.class public final Lcom/samsung/android/sconnect/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final Install_application:I = 0x7f090138

.field public static final accept:I = 0x7f090085

.field public static final action_connect:I = 0x7f090029

.field public static final action_contact_info:I = 0x7f090024

.field public static final action_control:I = 0x7f09001d

.field public static final action_create_meeting:I = 0x7f09002c

.field public static final action_disconnect:I = 0x7f09002a

.field public static final action_explore_content:I = 0x7f090025

.field public static final action_join:I = 0x7f09001c

.field public static final action_mirror_screen:I = 0x7f090027

.field public static final action_more_features:I = 0x7f090022

.field public static final action_open_together:I = 0x7f09002e

.field public static final action_play_content:I = 0x7f090026

.field public static final action_print:I = 0x7f090018

.field public static final action_print_images:I = 0x7f09001b

.field public static final action_print_other_content:I = 0x7f090019

.field public static final action_print_selected_content:I = 0x7f09001a

.field public static final action_remote_viewfinder:I = 0x7f090023

.field public static final action_search_on_web:I = 0x7f09002b

.field public static final action_send_content:I = 0x7f09001f

.field public static final action_send_other_contents:I = 0x7f090020

.field public static final action_send_selected_contents:I = 0x7f090021

.field public static final action_share_wifi_profile:I = 0x7f090030

.field public static final action_share_wifi_profile_overlay_chn:I = 0x7f09002f

.field public static final action_sidesync:I = 0x7f09002d

.field public static final action_start_group_play:I = 0x7f09001e

.field public static final action_stop_mirroring_screen:I = 0x7f090028

.field public static final action_will_be_supported_later:I = 0x7f0900fd

.field public static final add:I = 0x7f09018d

.field public static final add_to_favorite:I = 0x7f090189

.field public static final advanced:I = 0x7f09017e

.field public static final air_conditioner:I = 0x7f090179

.field public static final alltogether:I = 0x7f090194

.field public static final app_download_list_msg:I = 0x7f090192

.field public static final app_download_msg:I = 0x7f090191

.field public static final app_name:I = 0x7f090014

.field public static final ask_to_use_quick_connect:I = 0x7f09017f

.field public static final asks_your_permission_to_use_quick_connect:I = 0x7f090180

.field public static final audio_output_devices:I = 0x7f090165

.field public static final bd_player:I = 0x7f09016a

.field public static final bluetooth:I = 0x7f0900db

.field public static final bt_activate_msg:I = 0x7f0900bd

.field public static final camcorder:I = 0x7f09017b

.field public static final camera:I = 0x7f09016e

.field public static final cancel:I = 0x7f090089

.field public static final cannot_send_copyrighted_content:I = 0x7f090114

.field public static final change_icon:I = 0x7f09018c

.field public static final change_name:I = 0x7f09018b

.field public static final chromecast:I = 0x7f090175

.field public static final connect_to_ps:I = 0x7f090137

.field public static final connectable_device_info:I = 0x7f090104

.field public static final connectable_device_info_msg_1:I = 0x7f090106

.field public static final connectable_device_info_msg_10:I = 0x7f090111

.field public static final connectable_device_info_msg_11:I = 0x7f090112

.field public static final connectable_device_info_msg_1_overlay_chn:I = 0x7f090105

.field public static final connectable_device_info_msg_2:I = 0x7f090107

.field public static final connectable_device_info_msg_3:I = 0x7f090108

.field public static final connectable_device_info_msg_4:I = 0x7f090109

.field public static final connectable_device_info_msg_5:I = 0x7f09010a

.field public static final connectable_device_info_msg_6:I = 0x7f09010b

.field public static final connectable_device_info_msg_7:I = 0x7f09010d

.field public static final connectable_device_info_msg_7_overlay_chn:I = 0x7f09010c

.field public static final connectable_device_info_msg_8:I = 0x7f09010e

.field public static final connectable_device_info_msg_9:I = 0x7f090110

.field public static final connectable_device_info_msg_9_overlay_chn:I = 0x7f09010f

.field public static final connected_to_ps:I = 0x7f090031

.field public static final connecting:I = 0x7f0900d1

.field public static final connecting_to_ps:I = 0x7f0900d0

.field public static final connection_failed:I = 0x7f0900fb

.field public static final connection_method:I = 0x7f0900df

.field public static final content_sent:I = 0x7f0900b3

.field public static final dash:I = 0x7f09017c

.field public static final decline:I = 0x7f090084

.field public static final device_name:I = 0x7f0900de

.field public static final devices_not_supporting_multifile_sending_disappear:I = 0x7f090117

.field public static final disconnect_all_devices_from_their_wifi_direct_connection_and_try_again:I = 0x7f0900e8

.field public static final disconnect_all_devices_from_their_wifi_direct_connection_and_try_again_overlay_chn:I = 0x7f0900e7

.field public static final disconnect_msg:I = 0x7f090116

.field public static final disconnect_q:I = 0x7f090115

.field public static final dlna_audio_only_device:I = 0x7f09016d

.field public static final dlna_device:I = 0x7f09016c

.field public static final do_not_show_again:I = 0x7f0900cf

.field public static final do_not_show_again_for_this_device:I = 0x7f090139

.field public static final document_print_1_at_a_time_msg:I = 0x7f0901a0

.field public static final document_too_large_to_print_image:I = 0x7f0900d3

.field public static final document_too_large_to_print_pdf:I = 0x7f0900d4

.field public static final done:I = 0x7f09008c

.field public static final dvd_player:I = 0x7f090176

.field public static final emeeting:I = 0x7f0900cc

.field public static final emeeting_download_msg:I = 0x7f0900f9

.field public static final end:I = 0x7f09008b

.field public static final end_current_connection:I = 0x7f09011c

.field public static final end_current_session:I = 0x7f0900e0

.field public static final enter:I = 0x7f09017d

.field public static final error:I = 0x7f0900d5

.field public static final error_wifi_display_cause_group_play:I = 0x7f090103

.field public static final error_wifi_display_cause_hdmi:I = 0x7f090101

.field public static final error_wifi_display_cause_power_save:I = 0x7f090102

.field public static final failed_to_send_msg:I = 0x7f0900b4

.field public static final favorite:I = 0x7f090187

.field public static final general_device:I = 0x7f090173

.field public static final global_0:I = 0x7f09000a

.field public static final global_1:I = 0x7f09000b

.field public static final global_2:I = 0x7f09000c

.field public static final global_3:I = 0x7f09000d

.field public static final global_4:I = 0x7f09000e

.field public static final global_5:I = 0x7f09000f

.field public static final global_6:I = 0x7f090010

.field public static final global_7:I = 0x7f090011

.field public static final global_8:I = 0x7f090012

.field public static final global_9:I = 0x7f090013

.field public static final global_dash:I = 0x7f090000

.field public static final global_device_name_1:I = 0x7f090007

.field public static final global_device_name_2:I = 0x7f090008

.field public static final global_device_name_3:I = 0x7f090009

.field public static final global_dot:I = 0x7f090002

.field public static final global_num_1:I = 0x7f090003

.field public static final global_num_2:I = 0x7f090004

.field public static final global_num_3:I = 0x7f090005

.field public static final global_num_4:I = 0x7f090006

.field public static final global_star:I = 0x7f090001

.field public static final group_play:I = 0x7f0900c9

.field public static final group_play_download_msg:I = 0x7f0900f5

.field public static final help:I = 0x7f0900c4

.field public static final homesync:I = 0x7f090171

.field public static final hts:I = 0x7f09016b

.field public static final if_you_already_registered_this_tv_in_smart_remote_app:I = 0x7f0900ab

.field public static final if_you_already_registered_this_tv_in_watchon_app:I = 0x7f0900a9

.field public static final initiating:I = 0x7f0900b7

.field public static final input_device:I = 0x7f090172

.field public static final install_device_manager:I = 0x7f090134

.field public static final join_new_session:I = 0x7f0900a1

.field public static final keypad:I = 0x7f0900b1

.field public static final loading:I = 0x7f0900fc

.field public static final mac_address:I = 0x7f0900fa

.field public static final map_no_result:I = 0x7f09011d

.field public static final meeting_created:I = 0x7f090068

.field public static final mirroring_device:I = 0x7f090170

.field public static final mirroring_warning_dialog_msg:I = 0x7f0900f0

.field public static final mirroring_warning_dialog_title:I = 0x7f0900ef

.field public static final mobile:I = 0x7f090167

.field public static final more:I = 0x7f09018e

.field public static final multi_selection_limit_msg:I = 0x7f0900c6

.field public static final mute:I = 0x7f0900af

.field public static final my_location_option:I = 0x7f090120

.field public static final nearby:I = 0x7f090188

.field public static final network_permission_dialog_msg:I = 0x7f090182

.field public static final new_tv_is_searched:I = 0x7f0900a7

.field public static final new_tv_is_searched_smart_remote:I = 0x7f0900aa

.field public static final next:I = 0x7f09008d

.field public static final no:I = 0x7f090087

.field public static final no_devices_found:I = 0x7f090113

.field public static final no_network_connection:I = 0x7f090122

.field public static final not_found:I = 0x7f090186

.field public static final ok:I = 0x7f090088

.field public static final one_audio_file_selected:I = 0x7f09004a

.field public static final one_contact_selected:I = 0x7f09004b

.field public static final one_device_selected:I = 0x7f09008f

.field public static final one_document_selected:I = 0x7f09004c

.field public static final one_event_selected:I = 0x7f09004d

.field public static final one_file_selected:I = 0x7f09004e

.field public static final one_image_selected:I = 0x7f09004f

.field public static final one_location_selected:I = 0x7f090050

.field public static final one_memo_selected:I = 0x7f090051

.field public static final one_snote_selected:I = 0x7f090052

.field public static final one_task_selected:I = 0x7f090055

.field public static final one_video_selected:I = 0x7f090053

.field public static final one_webpage_selected:I = 0x7f090054

.field public static final pc:I = 0x7f090168

.field public static final pd_devices_selected:I = 0x7f09008e

.field public static final pd_files_will_be_shared_via_together:I = 0x7f09006b

.field public static final permdesc_accessBLEpackets:I = 0x7f0901a6

.field public static final permission_request:I = 0x7f090181

.field public static final permlab_accessSamsungBLE:I = 0x7f0901a5

.field public static final picker_audio:I = 0x7f090096

.field public static final picker_calendar:I = 0x7f09009f

.field public static final picker_capture_screen:I = 0x7f09009d

.field public static final picker_contacts:I = 0x7f09009b

.field public static final picker_doc:I = 0x7f09009e

.field public static final picker_files:I = 0x7f0900a0

.field public static final picker_image:I = 0x7f090090

.field public static final picker_location:I = 0x7f090091

.field public static final picker_memo:I = 0x7f090099

.field public static final picker_music:I = 0x7f090095

.field public static final picker_my_files:I = 0x7f09009c

.field public static final picker_record_audio:I = 0x7f090097

.field public static final picker_record_video:I = 0x7f090094

.field public static final picker_s_note:I = 0x7f090098

.field public static final picker_s_planner:I = 0x7f09009a

.field public static final picker_take_a_picture:I = 0x7f090092

.field public static final picker_video:I = 0x7f090093

.field public static final printer:I = 0x7f09016f

.field public static final ps_audio_files_selected:I = 0x7f090056

.field public static final ps_contacts_selected:I = 0x7f090057

.field public static final ps_documents_selected:I = 0x7f090058

.field public static final ps_events_selected:I = 0x7f090059

.field public static final ps_files_selected:I = 0x7f09005a

.field public static final ps_has_invited_you_to_join_a_together_room:I = 0x7f09006c

.field public static final ps_images_selected:I = 0x7f09005b

.field public static final ps_invites_you_to_group_play:I = 0x7f090064

.field public static final ps_invites_you_to_meeting:I = 0x7f090067

.field public static final ps_is_already_connected_to_another_device_via_wifi_direct:I = 0x7f0900f4

.field public static final ps_is_already_connected_to_another_device_via_wifi_direct_overlay_chn:I = 0x7f0900f3

.field public static final ps_memos_selected:I = 0x7f09005e

.field public static final ps_selected:I = 0x7f0900ac

.field public static final ps_sent:I = 0x7f0900ce

.field public static final ps_tasks_selected:I = 0x7f09005f

.field public static final ps_videos_selected:I = 0x7f09005c

.field public static final ps_wants_to_connect_to_your_device_via_sidesync:I = 0x7f09006d

.field public static final ps_wants_to_share_one_audio_file_with_you:I = 0x7f090039

.field public static final ps_wants_to_share_one_contact_with_you:I = 0x7f090035

.field public static final ps_wants_to_share_one_document_with_you:I = 0x7f090034

.field public static final ps_wants_to_share_one_event_with_you:I = 0x7f090036

.field public static final ps_wants_to_share_one_file_with_you:I = 0x7f090037

.field public static final ps_wants_to_share_one_image_with_you:I = 0x7f090038

.field public static final ps_wants_to_share_one_location_with_you:I = 0x7f090033

.field public static final ps_wants_to_share_one_memo_with_you:I = 0x7f09003d

.field public static final ps_wants_to_share_one_snote_with_you:I = 0x7f09003c

.field public static final ps_wants_to_share_one_task_with_you:I = 0x7f09003e

.field public static final ps_wants_to_share_one_video_with_you:I = 0x7f09003a

.field public static final ps_wants_to_share_one_webpage_with_you:I = 0x7f09003b

.field public static final ps_wants_to_share_ps_audio_files_with_you:I = 0x7f090044

.field public static final ps_wants_to_share_ps_contacts_with_you:I = 0x7f090040

.field public static final ps_wants_to_share_ps_documents_with_you:I = 0x7f09003f

.field public static final ps_wants_to_share_ps_events_with_you:I = 0x7f090041

.field public static final ps_wants_to_share_ps_files_with_you:I = 0x7f090042

.field public static final ps_wants_to_share_ps_images_with_you:I = 0x7f090043

.field public static final ps_wants_to_share_ps_memos_with_you:I = 0x7f090048

.field public static final ps_wants_to_share_ps_snotes_with_you:I = 0x7f090047

.field public static final ps_wants_to_share_ps_tasks_with_you:I = 0x7f090049

.field public static final ps_wants_to_share_ps_videos_with_you:I = 0x7f090045

.field public static final ps_wants_to_share_ps_webpages_with_you:I = 0x7f090046

.field public static final ps_webpages_selected:I = 0x7f09005d

.field public static final ps_wifi_will_be_turned_off:I = 0x7f0900a3

.field public static final ps_wifi_will_be_turned_off_overlay_chn:I = 0x7f0900a2

.field public static final ps_will_be_shared_via_emeeting:I = 0x7f090066

.field public static final ps_will_be_shared_via_group_play:I = 0x7f090063

.field public static final ps_will_be_shared_via_together:I = 0x7f09006a

.field public static final quick_connect_contacts_only:I = 0x7f090124

.field public static final quick_connect_contacts_only_msg:I = 0x7f090125

.field public static final quick_connect_settings_description1:I = 0x7f090126

.field public static final quick_connect_settings_description1_except_battery:I = 0x7f090127

.field public static final quick_connect_settings_description2:I = 0x7f090128

.field public static final quick_connect_settings_start:I = 0x7f090129

.field public static final quick_connect_settings_title:I = 0x7f090123

.field public static final refrigerator:I = 0x7f09017a

.field public static final rematch:I = 0x7f0900ae

.field public static final remove_from_favorite:I = 0x7f09018a

.field public static final rename_device:I = 0x7f0900c1

.field public static final rename_phone:I = 0x7f0900bf

.field public static final request_declined:I = 0x7f0900b5

.field public static final robot_vacuum:I = 0x7f090177

.field public static final rvf_download_msg:I = 0x7f0900f8

.field public static final samsung_link:I = 0x7f090195

.field public static final samsung_link_download_msg:I = 0x7f0900f7

.field public static final screen_mirroring_paused_msg:I = 0x7f09011a

.field public static final search_map:I = 0x7f09011e

.field public static final searching:I = 0x7f0900b8

.field public static final security_policy_bt_msg:I = 0x7f09019b

.field public static final security_policy_msg:I = 0x7f09019a

.field public static final security_policy_wifi_and_bt_msg:I = 0x7f09019f

.field public static final security_policy_wifi_and_bt_msg_overlay_chn:I = 0x7f09019e

.field public static final security_policy_wifi_msg:I = 0x7f09019d

.field public static final security_policy_wifi_msg_overlay_chn:I = 0x7f09019c

.field public static final select_location:I = 0x7f090121

.field public static final send:I = 0x7f090016

.field public static final send_one_audio_file:I = 0x7f090078

.field public static final send_one_contact:I = 0x7f090079

.field public static final send_one_document:I = 0x7f09007a

.field public static final send_one_event:I = 0x7f09007b

.field public static final send_one_file:I = 0x7f09007c

.field public static final send_one_image:I = 0x7f09007d

.field public static final send_one_location:I = 0x7f09007f

.field public static final send_one_memo:I = 0x7f090080

.field public static final send_one_snote:I = 0x7f090081

.field public static final send_one_task:I = 0x7f090083

.field public static final send_one_video:I = 0x7f09007e

.field public static final send_one_webpage:I = 0x7f090082

.field public static final send_ps_audio_files:I = 0x7f09006e

.field public static final send_ps_contacts:I = 0x7f09006f

.field public static final send_ps_documents:I = 0x7f090070

.field public static final send_ps_events:I = 0x7f090071

.field public static final send_ps_files:I = 0x7f090072

.field public static final send_ps_images:I = 0x7f090073

.field public static final send_ps_memos:I = 0x7f090075

.field public static final send_ps_tasks:I = 0x7f090077

.field public static final send_ps_videos:I = 0x7f090074

.field public static final send_ps_webpages:I = 0x7f090076

.field public static final send_via_bluetooth:I = 0x7f0900d8

.field public static final send_via_wifi:I = 0x7f0900d7

.field public static final send_via_wifi_overlay_chn:I = 0x7f0900d6

.field public static final sending_cancelled:I = 0x7f0900b2

.field public static final sending_to_ps:I = 0x7f0900cd

.field public static final set_device_name:I = 0x7f0900c0

.field public static final set_device_name_description:I = 0x7f0900c3

.field public static final set_device_name_description_overlay_chn:I = 0x7f0900c2

.field public static final set_my_device_visible:I = 0x7f0900be

.field public static final set_remote_control:I = 0x7f0900a8

.field public static final settings_description:I = 0x7f09012b

.field public static final settings_make_your_device_visible:I = 0x7f09012d

.field public static final settings_only_when_enabled:I = 0x7f090132

.field public static final settings_only_when_enabled_description:I = 0x7f090133

.field public static final settings_set_device_visibility:I = 0x7f09012a

.field public static final settings_to_all_devices:I = 0x7f09012e

.field public static final settings_to_all_devices_description:I = 0x7f09012f

.field public static final settings_to_contacts_only:I = 0x7f090130

.field public static final settings_to_contacts_only_description:I = 0x7f090131

.field public static final settings_visibility:I = 0x7f09012c

.field public static final share:I = 0x7f09008a

.field public static final share_via_emeeting:I = 0x7f090065

.field public static final share_via_group_play:I = 0x7f090062

.field public static final share_via_together:I = 0x7f090069

.field public static final sidesync:I = 0x7f090193

.field public static final smart_remote:I = 0x7f0900cb

.field public static final stms_appgroup:I = 0x7f0901aa

.field public static final stop:I = 0x7f090015

.field public static final stop_sending_msg:I = 0x7f0900b6

.field public static final suggested:I = 0x7f09018f

.field public static final suggested_header:I = 0x7f090190

.field public static final tablet:I = 0x7f090174

.field public static final tap_ok_to_connect:I = 0x7f090136

.field public static final tap_ok_to_download:I = 0x7f090135

.field public static final tb_button:I = 0x7f090160

.field public static final tb_channel_down:I = 0x7f090147

.field public static final tb_channel_up:I = 0x7f090146

.field public static final tb_checked:I = 0x7f090161

.field public static final tb_disable_quick_connect:I = 0x7f090149

.field public static final tb_double_tap_collapse:I = 0x7f090141

.field public static final tb_double_tap_expand:I = 0x7f09013f

.field public static final tb_double_tap_to_add_favorite:I = 0x7f09015b

.field public static final tb_double_tap_to_add_icon:I = 0x7f09015d

.field public static final tb_double_tap_to_change_icon_ps:I = 0x7f09015c

.field public static final tb_double_tap_to_change_or_remove_favorite:I = 0x7f09015a

.field public static final tb_double_tap_to_close:I = 0x7f09014a

.field public static final tb_double_tap_to_connect:I = 0x7f090157

.field public static final tb_double_tap_to_control:I = 0x7f090158

.field public static final tb_double_tap_to_disconnect:I = 0x7f090152

.field public static final tb_double_tap_to_explore_content:I = 0x7f090153

.field public static final tb_double_tap_to_join:I = 0x7f090150

.field public static final tb_double_tap_to_mirror_screen:I = 0x7f090155

.field public static final tb_double_tap_to_open_more_features:I = 0x7f090151

.field public static final tb_double_tap_to_pirnt_images:I = 0x7f090159

.field public static final tb_double_tap_to_play_content:I = 0x7f090154

.field public static final tb_double_tap_to_print:I = 0x7f090156

.field public static final tb_double_tap_to_send_content:I = 0x7f09014d

.field public static final tb_double_tap_to_send_more_content:I = 0x7f09014f

.field public static final tb_double_tap_to_send_selected_content:I = 0x7f09014e

.field public static final tb_double_tap_to_start_group_play:I = 0x7f09014b

.field public static final tb_double_tap_to_view_contact_info:I = 0x7f09014c

.field public static final tb_header:I = 0x7f09015f

.field public static final tb_hide_more_actions:I = 0x7f090140

.field public static final tb_more_button:I = 0x7f090164

.field public static final tb_moreoption:I = 0x7f09013d

.field public static final tb_navigate_up:I = 0x7f090142

.field public static final tb_not_checked:I = 0x7f090162

.field public static final tb_number_keypad:I = 0x7f090148

.field public static final tb_ps_already_set_as_device_icon:I = 0x7f09015e

.field public static final tb_refresh:I = 0x7f09013c

.field public static final tb_show_more_actions:I = 0x7f09013e

.field public static final tb_suggested_button:I = 0x7f090163

.field public static final tb_turn_off:I = 0x7f090145

.field public static final tb_volume_down:I = 0x7f090144

.field public static final tb_volume_up:I = 0x7f090143

.field public static final tethering_warning_dialog_msg:I = 0x7f0900ed

.field public static final tethering_warning_dialog_msg_overlay_chn:I = 0x7f0900eb

.field public static final tethering_warning_dialog_msg_overlay_tmo:I = 0x7f0900ec

.field public static final tethering_warning_dialog_title:I = 0x7f0900ea

.field public static final tethering_warning_dialog_title_overlay_chn:I = 0x7f0900e9

.field public static final the_current_screen_mirroring_will_end_msg:I = 0x7f09011b

.field public static final the_devices_are_already_connected_to_another_device_via_wifi_direct:I = 0x7f090061

.field public static final the_devices_are_already_connected_to_another_device_via_wifi_direct_overlay_chn:I = 0x7f090060

.field public static final to_connect_to_other_devices_msg:I = 0x7f0900e4

.field public static final to_connect_to_other_devices_msg_overlay_chn:I = 0x7f0900e3

.field public static final to_connect_to_ps_msg:I = 0x7f0900e2

.field public static final to_connect_to_ps_msg_overlay_chn:I = 0x7f0900e1

.field public static final tutorial_page1_description:I = 0x7f0900a4

.field public static final tutorial_page2_description:I = 0x7f0900a5

.field public static final tutorial_page3_description:I = 0x7f0900a6

.field public static final tv:I = 0x7f090169

.field public static final tv_list:I = 0x7f0900ad

.field public static final tv_source:I = 0x7f0900b0

.field public static final unable_to_add_dot:I = 0x7f0901a7

.field public static final unable_to_connect_to_device:I = 0x7f0900e5

.field public static final unable_to_find_location:I = 0x7f09011f

.field public static final unable_to_open_together:I = 0x7f0901a4

.field public static final unable_to_open_together_overlay_chn:I = 0x7f0901a3

.field public static final unable_to_print_file:I = 0x7f0901a2

.field public static final unable_to_print_images:I = 0x7f0901a1

.field public static final unable_to_run_quickconnect_while_oxygen:I = 0x7f0900ee

.field public static final unable_to_scan_mirroring_device_msg:I = 0x7f090119

.field public static final unable_to_scan_mirroring_device_msg_overlay_chn:I = 0x7f090118

.field public static final unable_to_send_audio_file:I = 0x7f0901a9

.field public static final unable_to_send_audio_files:I = 0x7f0901a8

.field public static final unable_to_send_drm_msg:I = 0x7f090198

.field public static final unable_to_send_drm_send_other_files_msg:I = 0x7f090199

.field public static final unable_to_send_files:I = 0x7f090197

.field public static final unable_to_send_maximum_number_of_content:I = 0x7f090196

.field public static final unexpected_error_msg:I = 0x7f0900e6

.field public static final unknown:I = 0x7f0900c7

.field public static final unknown_device:I = 0x7f0900c8

.field public static final unsupported_file_format:I = 0x7f0900d2

.field public static final update_dialog_body:I = 0x7f090185

.field public static final update_dialog_text:I = 0x7f090184

.field public static final use_bluetooth:I = 0x7f0900fe

.field public static final use_wifi:I = 0x7f090100

.field public static final use_wifi_overlay_chn:I = 0x7f0900ff

.field public static final userinfo:I = 0x7f090183

.field public static final view_or_play:I = 0x7f090017

.field public static final waiting_for_acceptance:I = 0x7f090032

.field public static final washing_machine:I = 0x7f090178

.field public static final watchon:I = 0x7f0900ca

.field public static final watchon_download_msg:I = 0x7f0900f6

.field public static final wearable_accessory:I = 0x7f090166

.field public static final wifi:I = 0x7f0900dd

.field public static final wifi_activate_msg:I = 0x7f0900bc

.field public static final wifi_activate_msg_overlay_chn:I = 0x7f0900bb

.field public static final wifi_bt_activate_msg:I = 0x7f0900ba

.field public static final wifi_bt_activate_msg_overlay_chn:I = 0x7f0900b9

.field public static final wifi_connection_wait_msg:I = 0x7f09013b

.field public static final wifi_connection_wait_msg_overlay_chn:I = 0x7f09013a

.field public static final wifi_direct:I = 0x7f0900da

.field public static final wifi_direct_overlay_chn:I = 0x7f0900d9

.field public static final wifi_overlay_chn:I = 0x7f0900dc

.field public static final wrong_input_toast:I = 0x7f0900c5

.field public static final yes:I = 0x7f090086

.field public static final you_cannot_connect_to_more_than_8:I = 0x7f0900f2

.field public static final you_cannot_connect_to_more_than_8_overlay_chn:I = 0x7f0900f1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
