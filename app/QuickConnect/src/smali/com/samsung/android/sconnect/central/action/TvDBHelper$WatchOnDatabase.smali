.class public Lcom/samsung/android/sconnect/central/action/TvDBHelper$WatchOnDatabase;
.super Ljava/lang/Object;
.source "TvDBHelper.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/TvDBHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WatchOnDatabase"
.end annotation


# static fields
.field public static final SQL_ALTER_V1_TO_V2:Ljava/lang/String; = "ALTER TABLE SConnect_WatchOn_TABLE ADD COLUMN IS_WATCHON text not null default \'true\';"

.field public static final _CREATE:Ljava/lang/String; = "create table SConnect_WatchOn_TABLE(_id integer primary key autoincrement, UUID text not null , P2PMAC text not null , BTMAC text not null , ROOM_ID text not null , ROOM_NAME text not null, D_TYPE text not null , D_BRAND text not null , IS_WATCHON text not null );"

.field public static final _TABLENAME:Ljava/lang/String; = "SConnect_WatchOn_TABLE"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
