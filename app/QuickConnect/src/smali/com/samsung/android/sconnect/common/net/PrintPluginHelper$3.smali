.class Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;
.super Landroid/os/Handler;
.source "PrintPluginHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 363
    const-string v0, "PrintPluginHelper"

    const-string v1, "handleMessage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 384
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 385
    return-void

    .line 367
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->startSearchViaPlugin()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$1000(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V

    goto :goto_0

    .line 371
    :pswitch_1
    const/16 v0, 0xbb8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;->removeMessages(I)V

    .line 372
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->stopSearchViaPlugin()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V

    goto :goto_0

    .line 376
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$100(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    # invokes: Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->startSearchViaPlugin()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->access$1000(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V

    goto :goto_0

    .line 365
    :pswitch_data_0
    .packed-switch 0xbb8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
