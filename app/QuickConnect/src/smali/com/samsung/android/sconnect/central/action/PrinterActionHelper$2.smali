.class Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "PrinterActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)V
    .locals 0

    .prologue
    .line 534
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 537
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 538
    .local v0, "action":Ljava/lang/String;
    const-string v4, "PrinterActionHelper"

    const-string v5, "mLocalReceiver: "

    invoke-static {v4, v5, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    const-string v4, "com.samsung.android.sconnect.central.PLUGIN_PRINTER_CONNECTED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 541
    const-string v4, "PrinterActionHelper"

    const-string v5, "mLocalReceiver"

    const-string v6, "PLUGIN PRINTER connected"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const-string v4, "PRINTER_INFO"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/print/PrinterInfo;

    .line 544
    .local v2, "printer":Landroid/print/PrinterInfo;
    const-string v4, "DEVICE"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 545
    .local v1, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v4, "URIS"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 546
    .local v3, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$2;->this$0:Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    # invokes: Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->startPluginPrint(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Landroid/print/PrinterInfo;)Z
    invoke-static {v4, v1, v3, v2}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->access$700(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Landroid/print/PrinterInfo;)Z

    .line 548
    .end local v1    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v2    # "printer":Landroid/print/PrinterInfo;
    .end local v3    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_0
    return-void
.end method
