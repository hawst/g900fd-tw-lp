.class Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser$1;
.super Landroid/bluetooth/le/AdvertiseCallback;
.source "BleAdvertiser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser$1;->this$0:Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;

    invoke-direct {p0}, Landroid/bluetooth/le/AdvertiseCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onStartFailure(I)V
    .locals 4
    .param p1, "errorCode"    # I

    .prologue
    .line 88
    const-string v0, "BleAdvertiser"

    const-string v1, "onStartFailure"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "errorCode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    return-void
.end method

.method public onStartSuccess(Landroid/bluetooth/le/AdvertiseSettings;)V
    .locals 0
    .param p1, "settingsInEffect"    # Landroid/bluetooth/le/AdvertiseSettings;

    .prologue
    .line 84
    return-void
.end method
