.class Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$4;
.super Landroid/widget/ArrayAdapter;
.source "QuickConnectSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->showVisibilityDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;Landroid/content/Context;ILjava/util/List;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I

    .prologue
    .line 178
    .local p4, "x2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$4;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 182
    if-nez p2, :cond_0

    .line 183
    iget-object v5, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$4;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 185
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030019

    invoke-virtual {v0, v5, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 189
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v5, 0x7f0c0069

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 190
    .local v2, "text1":Landroid/widget/TextView;
    const v5, 0x7f0c006a

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 191
    .local v3, "text2":Landroid/widget/TextView;
    const v5, 0x7f0c006b

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 193
    .local v1, "radioBtn":Landroid/widget/RadioButton;
    packed-switch p1, :pswitch_data_0

    .line 209
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings$4;->this$0:Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;

    # getter for: Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->mVisibility:I
    invoke-static {v5}, Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;->access$400(Lcom/samsung/android/sconnect/settings/ui/QuickConnectSettings;)I

    move-result v5

    if-ne v5, p1, :cond_1

    const/4 v4, 0x1

    :cond_1
    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 210
    return-object p2

    .line 195
    :pswitch_0
    const v5, 0x7f09012e

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 196
    const v5, 0x7f09012f

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 199
    :pswitch_1
    const v5, 0x7f090130

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 200
    const v5, 0x7f090131

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 203
    :pswitch_2
    const v5, 0x7f090132

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    .line 204
    const v5, 0x7f090133

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 193
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
