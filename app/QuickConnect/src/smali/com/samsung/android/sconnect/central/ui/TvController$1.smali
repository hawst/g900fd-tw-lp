.class Lcom/samsung/android/sconnect/central/ui/TvController$1;
.super Ljava/lang/Object;
.source "TvController.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/TvController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/TvController;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/TvController;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 364
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 366
    :sswitch_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    const/4 v3, 0x1

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->showTvList(Z)V
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$000(Lcom/samsung/android/sconnect/central/ui/TvController;Z)V

    goto :goto_0

    .line 369
    :sswitch_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    const/4 v3, 0x0

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->showTvList(Z)V
    invoke-static {v2, v3}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$000(Lcom/samsung/android/sconnect/central/ui/TvController;Z)V

    goto :goto_0

    .line 372
    :sswitch_2
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->goSetting()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$100(Lcom/samsung/android/sconnect/central/ui/TvController;)V

    goto :goto_0

    .line 375
    :sswitch_3
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishTvControl()V

    goto :goto_0

    .line 378
    :sswitch_4
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 379
    .local v0, "action":I
    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    .line 380
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # invokes: Lcom/samsung/android/sconnect/central/ui/TvController;->goMainHome()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$300(Lcom/samsung/android/sconnect/central/ui/TvController;)V

    goto :goto_0

    .line 382
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$400(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)V

    .line 383
    const/4 v1, 0x0

    .line 384
    .local v1, "isMirroring":Z
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$400(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getState()I

    move-result v2

    sget v3, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_MIRRORING:I

    and-int/2addr v2, v3

    if-lez v2, :cond_2

    .line 385
    const/4 v1, 0x1

    .line 387
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    if-eqz v1, :cond_0

    .line 388
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/TvController$1;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finishTvControl()V

    goto :goto_0

    .line 364
    :sswitch_data_0
    .sparse-switch
        0x7f0c0088 -> :sswitch_4
        0x7f0c009c -> :sswitch_3
        0x7f0c009f -> :sswitch_0
        0x7f0c00a4 -> :sswitch_2
        0x7f0c00a5 -> :sswitch_1
    .end sparse-switch
.end method
