.class public Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;
.super Ljava/lang/Object;
.source "RequestDeviceInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ACCPTED:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final DECLIEND:I = 0x2

.field public static final DONE:I = 0x3

.field public static final NOT_ANSWERED:I


# instance fields
.field public mDeviceNetType:I

.field public mDeviceType:I

.field public mIsAccepted:I

.field public mName:Ljava/lang/String;

.field public mNumber:Ljava/lang/String;

.field public mP2pMac:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo$1;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;I)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "action"    # I

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    .line 24
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mNumber:Ljava/lang/String;

    .line 27
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceType:I

    .line 28
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceNetType()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "mac"    # Ljava/lang/String;
    .param p3, "number"    # Ljava/lang/String;
    .param p4, "deviceType"    # I

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mIsAccepted:I

    .line 32
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mNumber:Ljava/lang/String;

    .line 35
    iput p4, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceType:I

    .line 36
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    .line 37
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mP2pMac:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 49
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 50
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/RequestDeviceInfo;->mDeviceNetType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 51
    return-void
.end method
