.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->askForEmeeting(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$action:I

.field final synthetic val$contentType:I

.field final synthetic val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

.field final synthetic val$selectedUris:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;I)V
    .locals 0

    .prologue
    .line 3237
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iput p3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->val$action:I

    iput-object p4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->val$selectedUris:Ljava/util/ArrayList;

    iput p5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->val$contentType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 3240
    const-string v0, "SconnectDisplay"

    const-string v1, "askForEmeeting"

    const-string v2, "onPositiveButton"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3241
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->val$device:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->val$action:I

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->val$selectedUris:Ljava/util/ArrayList;

    const-string v4, ""

    iget v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->val$contentType:I

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 3242
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$31;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    .line 3243
    return-void
.end method
