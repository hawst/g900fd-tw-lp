.class Lcom/samsung/android/sconnect/central/ui/ViewManager$12;
.super Ljava/lang/Object;
.source "ViewManager.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/ViewManager;->expandActionList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V
    .locals 0

    .prologue
    .line 608
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$12;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 611
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 612
    .local v0, "action":I
    const-string v1, "ViewManager"

    const-string v2, "mScrollView onTouch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    packed-switch v0, :pswitch_data_0

    .line 622
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v1, 0x1

    return v1

    .line 617
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$12;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$200(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 618
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$12;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    goto :goto_0

    .line 613
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
