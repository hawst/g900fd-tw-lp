.class public Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;
.super Landroid/app/Activity;
.source "MediaSourceSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector$4;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MediaSourceSelector"

.field private static mRequestCode:I


# instance fields
.field private final REQUEST_CODE_PICK_DRAWINGPAD:I

.field private final REQUEST_CODE_PICK_LOCATION:I

.field private final REQUEST_CODE_PICK_MEMO:I

.field private final REQUEST_CODE_PICK_MYFILES:I

.field private final REQUEST_CODE_PICK_PICTURE:I

.field private final REQUEST_CODE_PICK_SOUND:I

.field private final REQUEST_CODE_PICK_VCAL:I

.field private final REQUEST_CODE_PICK_VCARD:I

.field private final REQUEST_CODE_PICK_VIDEO:I

.field private final REQUEST_CODE_PICK_VNOTE:I

.field private final REQUEST_CODE_PICK_VTODO:I

.field private final REQUEST_CODE_RECORD_SOUND:I

.field private final REQUEST_CODE_TAKE_PICTURE:I

.field private final REQUEST_CODE_TAKE_VIDEO:I

.field private mGridView:Landroid/widget/GridView;

.field private mRequestedActivity:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestCode:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 56
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_PICTURE:I

    .line 57
    const/16 v0, 0xb

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_TAKE_PICTURE:I

    .line 58
    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_VIDEO:I

    .line 59
    const/16 v0, 0xd

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_TAKE_VIDEO:I

    .line 60
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_SOUND:I

    .line 61
    const/16 v0, 0xf

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_RECORD_SOUND:I

    .line 62
    const/16 v0, 0x10

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_VCARD:I

    .line 63
    const/16 v0, 0x11

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_VCAL:I

    .line 64
    const/16 v0, 0x12

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_VNOTE:I

    .line 65
    const/16 v0, 0x13

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_VTODO:I

    .line 66
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_LOCATION:I

    .line 67
    const/16 v0, 0x15

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_DRAWINGPAD:I

    .line 68
    const/16 v0, 0x16

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_MYFILES:I

    .line 69
    const/16 v0, 0x17

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->REQUEST_CODE_PICK_MEMO:I

    .line 74
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestedActivity:I

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method private finishWithResult(ILandroid/content/Intent;)V
    .locals 0
    .param p1, "resultCode"    # I
    .param p2, "data"    # Landroid/content/Intent;

    .prologue
    .line 77
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->setResult(ILandroid/content/Intent;)V

    .line 78
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->finish()V

    .line 79
    return-void
.end method


# virtual methods
.method public isInternetAvailable()Z
    .locals 4

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 428
    .local v0, "info":Landroid/net/NetworkInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-nez v1, :cond_1

    .line 429
    :cond_0
    const-string v1, "MediaSourceSelector"

    const-string v2, "isInternetAvailable"

    const-string v3, "Internet is not available"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const/4 v1, 0x0

    .line 433
    :goto_0
    return v1

    .line 432
    :cond_1
    const-string v1, "MediaSourceSelector"

    const-string v2, "isInternetAvailable"

    const-string v3, "Internet is available"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public launchAppInfo(Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)V
    .locals 4
    .param p1, "command"    # Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    .prologue
    .line 170
    const-string v1, ""

    .line 172
    .local v1, "pkg":Ljava/lang/String;
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->TAKE_PICTURE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-eq p1, v2, :cond_0

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->RECORD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_3

    .line 173
    :cond_0
    const-string v1, "com.sec.android.app.camera"

    .line 204
    :cond_1
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 205
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 206
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "package"

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 207
    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivity(Landroid/content/Intent;)V

    .line 209
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_2
    return-void

    .line 174
    :cond_3
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_DOC:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-eq p1, v2, :cond_4

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_MYFILES:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-eq p1, v2, :cond_4

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_PDF:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_5

    .line 176
    :cond_4
    const-string v1, "com.sec.android.app.myfiles"

    goto :goto_0

    .line 177
    :cond_5
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_6

    .line 178
    sget v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestCode:I

    sparse-switch v2, :sswitch_data_0

    .line 185
    const-string v1, "com.sec.android.app.myfiles"

    .line 186
    goto :goto_0

    .line 181
    :sswitch_0
    const-string v1, "com.sec.android.app.music"

    .line 182
    goto :goto_0

    .line 188
    :cond_6
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_IMAGE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-eq p1, v2, :cond_7

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VIDEO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_8

    .line 189
    :cond_7
    const-string v1, "com.sec.android.gallery3d"

    goto :goto_0

    .line 190
    :cond_8
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_MEMO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_9

    .line 191
    const-string v1, "com.samsung.android.app.memo"

    goto :goto_0

    .line 192
    :cond_9
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_LOCATION:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_a

    .line 193
    const-string v1, "com.google.android.apps.maps"

    goto :goto_0

    .line 194
    :cond_a
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_SNOTE:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_b

    .line 195
    const-string v1, "com.samsung.android.snote"

    goto :goto_0

    .line 196
    :cond_b
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VCARD:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_c

    .line 197
    const-string v1, "com.android.contacts"

    goto :goto_0

    .line 198
    :cond_c
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->RECORD_SOUND:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_d

    .line 199
    const-string v1, "com.sec.android.app.voicenote"

    goto :goto_0

    .line 200
    :cond_d
    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VTODO:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-eq p1, v2, :cond_e

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ADD_VCAL:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    if-ne p1, v2, :cond_1

    .line 201
    :cond_e
    const-string v1, "com.android.calendar"

    goto :goto_0

    .line 178
    :sswitch_data_0
    .sparse-switch
        0x3ea -> :sswitch_0
        0x3ef -> :sswitch_0
    .end sparse-switch
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 83
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 84
    const-string v0, "MediaSourceSelector"

    const-string v1, "onActivityResult"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resultCode :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", requestCode :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestedActivity:I

    if-ne v0, p1, :cond_0

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestedActivity:I

    .line 91
    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->finishWithResult(ILandroid/content/Intent;)V

    .line 92
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v12, 0x7f090018

    const v11, 0x7f090017

    const v10, 0x7f090016

    const/4 v9, 0x2

    .line 96
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 98
    .local v1, "intent":Landroid/content/Intent;
    if-nez v1, :cond_0

    .line 166
    :goto_0
    return-void

    .line 101
    :cond_0
    const-string v5, "request_code"

    const/16 v6, 0x3e9

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    sput v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestCode:I

    .line 102
    const-string v5, "MediaSourceSelector"

    const-string v6, "onCreate"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "requestCode: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget v8, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestCode:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v5, "layout_inflater"

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 105
    .local v0, "inflater":Landroid/view/LayoutInflater;
    if-nez v0, :cond_1

    .line 106
    const-string v5, "MediaSourceSelector"

    const-string v6, "onCreate"

    const-string v7, "mInflater is null"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    new-instance v5, Ljava/lang/NullPointerException;

    const-string v6, "MediaSourceSelector::MyAdapter mInflater is null"

    invoke-direct {v5, v6}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 110
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v9, v9}, Landroid/view/Window;->setFlags(II)V

    .line 112
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/high16 v6, 0x3f000000    # 0.5f

    invoke-virtual {v5, v6}, Landroid/view/Window;->setDimAmount(F)V

    .line 114
    new-instance v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;

    sget v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestCode:I

    invoke-direct {v2, p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;I)V

    .line 116
    .local v2, "mAdapter":Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;
    const v5, 0x7f03000f

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->setContentView(I)V

    .line 117
    const v5, 0x7f0c003f

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/GridView;

    iput-object v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mGridView:Landroid/widget/GridView;

    .line 118
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v5, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 120
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mGridView:Landroid/widget/GridView;

    new-instance v6, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector$1;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;)V

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 128
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mGridView:Landroid/widget/GridView;

    new-instance v6, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector$2;

    invoke-direct {v6, p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector$2;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;)V

    invoke-virtual {v5, v6}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 137
    const v5, 0x7f0c003d

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;

    .line 138
    .local v3, "rdl":Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;
    if-eqz v3, :cond_2

    .line 139
    new-instance v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector$3;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;)V

    invoke-virtual {v3, v5}, Lcom/samsung/android/sconnect/central/ui/picker/ResolverDrawerLayout;->setOnClickOutsideListener(Landroid/view/View$OnClickListener;)V

    .line 147
    :cond_2
    const v5, 0x7f0c003e

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 148
    .local v4, "titleView":Landroid/widget/TextView;
    sget v5, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestCode:I

    packed-switch v5, :pswitch_data_0

    .line 160
    :pswitch_0
    invoke-virtual {p0, v10}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->setTitle(Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(I)V

    .line 165
    :goto_1
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/samsung/android/sconnect/SconnectManager;->setDeviceSelectMode(Z)V

    goto/16 :goto_0

    .line 151
    :pswitch_1
    invoke-virtual {p0, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->setTitle(Ljava/lang/CharSequence;)V

    .line 152
    invoke-virtual {v4, v11}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 155
    :pswitch_2
    invoke-virtual {p0, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->setTitle(Ljava/lang/CharSequence;)V

    .line 156
    invoke-virtual {v4, v12}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x3ea
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 213
    const-string v0, "MediaSourceSelector"

    const-string v1, "onDestroy"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestedActivity:I

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "MediaSourceSelector"

    const-string v1, "onDestroy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "finishActivity : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestedActivity:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestedActivity:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->finishActivity(I)V

    .line 219
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 220
    return-void
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 449
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 450
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 444
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 445
    return-void
.end method

.method public showNoInternetDialog()V
    .locals 3

    .prologue
    .line 437
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0900d5

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090122

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f090088

    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 440
    return-void
.end method

.method public startActivityForAttachMedia(Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)V
    .locals 16
    .param p1, "type"    # Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    .prologue
    .line 223
    const-string v1, "audio/*"

    .line 224
    .local v1, "AUDIO_UNSPECIFIED":Ljava/lang/String;
    const-string v2, "image/*"

    .line 225
    .local v2, "IMAGE_UNSPECIFIED":Ljava/lang/String;
    const-string v5, "video/*"

    .line 226
    .local v5, "VIDEO_UNSPECIFIED":Ljava/lang/String;
    const-string v4, "Rcs_Camera_Request"

    .line 227
    .local v4, "RCS_CAMERA_VALUE":Ljava/lang/String;
    const-string v3, "Rcs_Camera_Key"

    .line 230
    .local v3, "RCS_CAMERA_KEY":Ljava/lang/String;
    const/4 v9, 0x0

    .line 231
    .local v9, "folderPath":Ljava/lang/String;
    const/4 v7, 0x0

    .line 232
    .local v7, "contentType":Ljava/lang/String;
    const/4 v11, 0x0

    .line 235
    .local v11, "requestCode":I
    :try_start_0
    sget-object v12, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector$4;->$SwitchMap$com$samsung$android$sconnect$central$ui$picker$MediaSourceSelectorAdapter$ActionType:[I

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;->ordinal()I

    move-result v13

    aget v12, v12, v13
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v12, :pswitch_data_0

    .line 421
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iput v11, v0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestedActivity:I

    .line 422
    return-void

    .line 237
    :pswitch_0
    :try_start_1
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 238
    .local v6, "captureIntent":Landroid/content/Intent;
    const-string v12, "CAPTURED_SCREEN"

    const/4 v13, 0x1

    invoke-virtual {v6, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 239
    const/4 v12, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v6}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->finishWithResult(ILandroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 417
    .end local v6    # "captureIntent":Landroid/content/Intent;
    :catch_0
    move-exception v8

    .line 418
    .local v8, "e":Ljava/lang/Exception;
    const-string v12, "MediaSourceSelector"

    const-string v13, "startActivityForAttachMedia"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "type: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "- exception :"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v8}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 243
    .end local v8    # "e":Ljava/lang/Exception;
    :pswitch_1
    :try_start_2
    const-string v7, "image/*"

    .line 244
    const/16 v11, 0xa

    .line 245
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 246
    .local v10, "intent":Landroid/content/Intent;
    const-string v12, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v10, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 247
    invoke-virtual {v10, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    const-string v12, "android.intent.extra.LOCAL_ONLY"

    const/4 v13, 0x1

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 249
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 253
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_2
    const/16 v11, 0xb

    .line 254
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 255
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "com.sec.android.app.camera"

    const-string v13, "com.sec.android.app.camera.Camera"

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 257
    const-string v12, "Rcs_Camera_Key"

    const-string v13, "Rcs_Camera_Request"

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 258
    const-string v12, "return-uri"

    const/4 v13, 0x1

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 259
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 263
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_3
    const-string v7, "video/*"

    .line 264
    const/16 v11, 0xc

    .line 265
    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    .line 266
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v10, v12}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    invoke-virtual {v10, v7}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    const-string v12, "android.intent.extra.LOCAL_ONLY"

    const/4 v13, 0x1

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 269
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 273
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_4
    const/16 v11, 0xd

    .line 274
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.media.action.VIDEO_CAPTURE"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 275
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "rcs_ft"

    const/4 v13, 0x1

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 276
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 280
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_5
    const/16 v11, 0xe

    .line 281
    sget v12, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestCode:I

    const/16 v13, 0x3e9

    if-ne v12, v13, :cond_2

    .line 282
    new-instance v10, Landroid/content/Intent;

    const-string v12, "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 283
    .restart local v10    # "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 284
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Sound"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 285
    const-string v12, "FOLDERPATH"

    invoke-virtual {v10, v12, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 286
    const-string v12, "CONTENT_TYPE"

    const-string v13, "audio/*"

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 290
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 288
    :cond_1
    const-string v12, "SELECTOR_CATEGORY_TYPE"

    const/4 v13, 0x4

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 291
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_2
    sget v12, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestCode:I

    const/16 v13, 0x3ea

    if-eq v12, v13, :cond_3

    sget v12, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->mRequestCode:I

    const/16 v13, 0x3ef

    if-ne v12, v13, :cond_0

    .line 293
    :cond_3
    new-instance v10, Landroid/content/Intent;

    const-string v12, "com.sec.android.music.intent.action.SOUND_PICKER"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 294
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "vnd.android.cursor.dir/audio"

    invoke-virtual {v10, v12}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 295
    const-string v12, "isMultiple"

    const/4 v13, 0x1

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 296
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 301
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_6
    const/16 v11, 0xf

    .line 302
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.provider.MediaStore.RECORD_SOUND"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 303
    .restart local v10    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 307
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_7
    const/16 v11, 0x10

    .line 308
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.intent.action.PICK"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 309
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "text/x-vcard"

    invoke-virtual {v10, v12}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    const-string v12, "maxRecipientCount"

    const/16 v13, 0xbb8

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 311
    const/16 v12, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 315
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_8
    const/16 v11, 0x11

    .line 316
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.intent.action.PICK"

    sget-object v13, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v10, v12, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 317
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "vnd.android.cursor.dir/vnd.samsung.calendar.*"

    invoke-virtual {v10, v12}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    const-string v12, "multiple_choice"

    const/4 v13, 0x1

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 319
    const-string v12, "choice_limit"

    const/16 v13, 0x12c

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 320
    const/16 v12, 0x11

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 324
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_9
    const/16 v11, 0x17

    .line 325
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.intent.action.PICK"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 326
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "com.samsung.android.app.memo"

    const-string v13, "com.samsung.android.app.memo.Main"

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 327
    const-string v12, "application/vnd.samsung.memo"

    invoke-virtual {v10, v12}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 328
    const/16 v12, 0x17

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 332
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_a
    const/16 v11, 0x12

    .line 333
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.intent.action.SNOTE_PICK"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 334
    .restart local v10    # "intent":Landroid/content/Intent;
    const/16 v12, 0x12

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 338
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_b
    const/16 v11, 0x13

    .line 339
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.intent.action.PICK"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 340
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "text/x-vtodo"

    invoke-virtual {v10, v12}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 341
    const/16 v12, 0x13

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 345
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->isInternetAvailable()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 346
    const/16 v11, 0x14

    .line 347
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.intent.action.GET_CONTENT"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 348
    .restart local v10    # "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isCHNDevice()Z

    move-result v12

    if-nez v12, :cond_4

    .line 349
    const-class v12, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    move-object/from16 v0, p0

    invoke-virtual {v10, v0, v12}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 353
    :goto_2
    const/16 v12, 0x14

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 351
    :cond_4
    const-class v12, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    move-object/from16 v0, p0

    invoke-virtual {v10, v0, v12}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    goto :goto_2

    .line 355
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->showNoInternetDialog()V

    goto/16 :goto_0

    .line 360
    :pswitch_d
    const/16 v11, 0x15

    .line 361
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.intent.action.PENMEMO_ATTACH"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 362
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "app_name"

    const-string v13, "com.samsung.android.sconnect"

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 363
    const/16 v12, 0x15

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 367
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_e
    const/16 v11, 0x15

    .line 368
    new-instance v10, Landroid/content/Intent;

    const-string v12, "android.intent.action.EDIT"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 369
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "application/vnd.penmemo.drawingpad"

    invoke-virtual {v10, v12}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    const/16 v12, 0x15

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 374
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_f
    const/16 v11, 0x16

    .line 375
    new-instance v10, Landroid/content/Intent;

    const-string v12, "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 376
    .restart local v10    # "intent":Landroid/content/Intent;
    const-string v12, "CONTENT_TYPE"

    const-string v13, "*/*"

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 377
    const-string v12, "JUST_SELECT_MODE"

    const/4 v13, 0x1

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 378
    const-string v12, "file_display"

    const-string v13, "forwardable"

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    const/16 v12, 0x16

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v12}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 383
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_10
    const/16 v11, 0xe

    .line 384
    new-instance v10, Landroid/content/Intent;

    const-string v12, "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 385
    .restart local v10    # "intent":Landroid/content/Intent;
    if-eqz v10, :cond_0

    .line 386
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 387
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Documents"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 389
    const-string v12, "FOLDERPATH"

    invoke-virtual {v10, v12, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 393
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 391
    :cond_6
    const-string v12, "SELECTOR_CATEGORY_TYPE"

    const/4 v13, 0x5

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_3

    .line 398
    .end local v10    # "intent":Landroid/content/Intent;
    :pswitch_11
    const/16 v11, 0xe

    .line 399
    new-instance v10, Landroid/content/Intent;

    const-string v12, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-direct {v10, v12}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 400
    .restart local v10    # "intent":Landroid/content/Intent;
    if-eqz v10, :cond_0

    .line 401
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v12

    if-eqz v12, :cond_7

    .line 402
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Documents"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 404
    const-string v12, "FOLDERPATH"

    invoke-virtual {v10, v12, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 410
    :goto_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v11}, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 406
    :cond_7
    const-string v12, "MediaSourceSelector"

    const-string v13, "ADD_PDF"

    const-string v14, "application/pdf"

    invoke-static {v12, v13, v14}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    const-string v12, "SELECTOR_CATEGORY_TYPE"

    const/4 v13, 0x5

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 408
    const-string v12, "CONTENT_TYPE"

    const-string v13, "application/pdf"

    invoke-virtual {v10, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_4

    .line 235
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch
.end method
