.class Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$3;
.super Ljava/lang/Object;
.source "FavoriteDeviceManager.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showIconDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V
    .locals 0

    .prologue
    .line 432
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$3;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 435
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$3;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconGridView:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$500(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;

    .line 437
    .local v0, "adapter":Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;
    invoke-virtual {v0, p3}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # setter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$602(I)I

    .line 438
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;->notifyDataSetChanged()V

    .line 439
    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$600()I

    move-result v1

    const/16 v2, 0x64

    if-ne v1, v2, :cond_0

    .line 440
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$3;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # invokes: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showAddIconDialog()V
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$700(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    .line 441
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$3;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$800(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 446
    :goto_0
    return-void

    .line 443
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$3;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I
    invoke-static {}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$600()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->changeIcon(I)V

    .line 444
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$3;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # getter for: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$800(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0
.end method
