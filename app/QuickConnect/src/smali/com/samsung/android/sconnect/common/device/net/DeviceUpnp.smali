.class public Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
.super Ljava/lang/Object;
.source "DeviceUpnp.java"


# static fields
.field public static final NIC_P2P:I = 0x2

.field public static final NIC_P2P_NAME:Ljava/lang/String; = "p2p-wlan0-0"

.field public static final NIC_WLAN:I = 0x1

.field public static final NIC_WLAN_NAME:Ljava/lang/String; = "wlan0"


# instance fields
.field public mDeviceType:I

.field public mP2pMac:Ljava/lang/String;

.field public mUpnpDeviceType:I

.field public mUpnpID:Ljava/lang/String;

.field public mUpnpIP:Ljava/lang/String;

.field public mUpnpNIC:I

.field public mUpnpName:Ljava/lang/String;

.field public mUpnpUUID:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "nic"    # Ljava/lang/String;
    .param p4, "ip"    # Ljava/lang/String;
    .param p5, "deviceType"    # I
    .param p6, "mac"    # Ljava/lang/String;

    .prologue
    const/16 v5, 0xd

    const/16 v4, 0xc

    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpName:Ljava/lang/String;

    .line 18
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpID:Ljava/lang/String;

    .line 19
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpUUID:Ljava/lang/String;

    .line 20
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpIP:Ljava/lang/String;

    .line 21
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mP2pMac:Ljava/lang/String;

    .line 22
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    .line 23
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    .line 24
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    .line 27
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpName:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpID:Ljava/lang/String;

    .line 30
    if-eqz p1, :cond_8

    .line 31
    const-string v1, "[TV]"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    const/4 v1, 0x6

    iput v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    .line 57
    :goto_0
    const-string v1, "+"

    invoke-virtual {p2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 58
    .local v0, "point":I
    if-lez v0, :cond_a

    .line 59
    invoke-virtual {p2, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpUUID:Ljava/lang/String;

    .line 63
    :goto_1
    iput-object p6, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mP2pMac:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpIP:Ljava/lang/String;

    .line 65
    invoke-direct {p0, p3}, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->addNIC(Ljava/lang/String;)V

    .line 67
    iput p5, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    .line 68
    return-void

    .line 33
    .end local v0    # "point":I
    :cond_0
    const-string v1, "[PC]"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Windows Media Player"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 34
    :cond_1
    iput v3, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    goto :goto_0

    .line 35
    :cond_2
    const-string v1, "[HomeSync]"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 36
    const/16 v1, 0xe

    iput v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    goto :goto_0

    .line 37
    :cond_3
    const-string v1, "[BD]"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 38
    const/16 v1, 0x11

    iput v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    goto :goto_0

    .line 39
    :cond_4
    const-string v1, "[HTS]"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 40
    const/16 v1, 0x12

    iput v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    goto :goto_0

    .line 41
    :cond_5
    if-ne p5, v3, :cond_6

    .line 42
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    goto :goto_0

    .line 43
    :cond_6
    and-int/lit8 v1, p5, 0x8

    if-lez v1, :cond_7

    .line 44
    iput v5, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    goto :goto_0

    .line 46
    :cond_7
    iput v4, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    goto :goto_0

    .line 49
    :cond_8
    and-int/lit8 v1, p5, 0x8

    if-lez v1, :cond_9

    .line 50
    iput v5, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    goto :goto_0

    .line 52
    :cond_9
    iput v4, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    goto :goto_0

    .line 61
    .restart local v0    # "point":I
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpID:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpUUID:Ljava/lang/String;

    goto :goto_1
.end method

.method private addNIC(Ljava/lang/String;)V
    .locals 2
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 105
    const-string v0, "wlan0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 106
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    const-string v0, "p2p-wlan0-0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 108
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    goto :goto_0
.end method

.method private removeNIC(Ljava/lang/String;)V
    .locals 2
    .param p1, "nic"    # Ljava/lang/String;

    .prologue
    .line 113
    const-string v0, "wlan0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 114
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 115
    :cond_1
    const-string v0, "p2p-wlan0-0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 116
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    goto :goto_0
.end method


# virtual methods
.method public DeleteUpnp(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)Z
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    .prologue
    .line 91
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    and-int/2addr v0, v1

    if-lez v0, :cond_0

    .line 92
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    .line 94
    :cond_0
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    and-int/2addr v0, v1

    if-lez v0, :cond_1

    .line 95
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    .line 98
    :cond_1
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    if-nez v0, :cond_2

    .line 99
    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 122
    if-nez p1, :cond_1

    .line 133
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 125
    check-cast v0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    .line 127
    .local v0, "dev":Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpName:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 128
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpUUID:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public updateUpnp(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    .prologue
    .line 77
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    and-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 78
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    .line 80
    :cond_0
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    and-int/2addr v0, v1

    if-nez v0, :cond_1

    .line 81
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    .line 84
    :cond_1
    return-void
.end method
