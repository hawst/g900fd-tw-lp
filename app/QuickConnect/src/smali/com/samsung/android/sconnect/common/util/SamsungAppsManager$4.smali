.class Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$4;
.super Ljava/lang/Object;
.source "SamsungAppsManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->checkDownloadables()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$4;->this$0:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    .line 122
    const-string v7, "SamsungAppsManager"

    const-string v8, "checkDownloadableThread"

    const-string v9, "run()"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$4;->this$0:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    # getter for: Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->access$000(Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "downloadable_status"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 125
    .local v5, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 126
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    # getter for: Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->DOWNLOADABLE_VERSION_CODE:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->access$100()Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 127
    .local v1, "element":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 128
    .local v3, "packageName":Ljava/lang/String;
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 129
    .local v6, "supportVersioncode":I
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$4;->this$0:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    # getter for: Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->access$000(Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;)Landroid/content/Context;

    move-result-object v7

    invoke-static {v7, v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppDownloadable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 130
    const-string v7, "SamsungAppsManager"

    const-string v8, "checkDownloadableThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " already AppDownloadable"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 134
    :cond_1
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker;->getDownloadableInfo(Ljava/lang/String;)Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;

    move-result-object v4

    .line 136
    .local v4, "result":Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;
    if-eqz v4, :cond_0

    .line 139
    const-string v7, "SamsungAppsManager"

    const-string v8, "checkDownloadableThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "[appId]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v4, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->appId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " [resultCode]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v4, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->resultCode:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " [resultMsg]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v4, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->resultMsg:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " [versionName]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v4, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->versionName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " [versionCode]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v4, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->versionCode:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget v7, v4, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;->versionCode:I

    if-lt v7, v6, :cond_2

    .line 144
    const-string v7, "SamsungAppsManager"

    const-string v8, "checkDownloadableThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " set AppDownloadable"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const/4 v7, 0x1

    invoke-interface {v0, v3, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto/16 :goto_0

    .line 148
    :cond_2
    const-string v7, "SamsungAppsManager"

    const-string v8, "checkDownloadableThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " not AppDownloadable"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 154
    .end local v1    # "element":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "result":Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker$StubUpdateCheckApiResult;
    .end local v6    # "supportVersioncode":I
    :cond_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 155
    return-void
.end method
