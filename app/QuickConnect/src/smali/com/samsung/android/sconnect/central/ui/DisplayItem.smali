.class public Lcom/samsung/android/sconnect/central/ui/DisplayItem;
.super Ljava/lang/Object;
.source "DisplayItem.java"


# instance fields
.field private mActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mDevice:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

.field private mDeviceView:Landroid/widget/LinearLayout;

.field private mPrimaryAction:I


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Lcom/samsung/android/sconnect/central/ActionFormatter;Z)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "actionFormatter"    # Lcom/samsung/android/sconnect/central/ActionFormatter;
    .param p3, "isContentRalatedMode"    # Z

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mActionList:Ljava/util/ArrayList;

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mPrimaryAction:I

    .line 15
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mDeviceView:Landroid/widget/LinearLayout;

    .line 19
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mDevice:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 20
    invoke-virtual {p2, p0, p3}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setActionList(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 21
    return-void
.end method


# virtual methods
.method public getActionList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mActionList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mDevice:Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    return-object v0
.end method

.method public getDeviceView()Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mDeviceView:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getPrimaryAction()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mPrimaryAction:I

    return v0
.end method

.method public listingActions(Lcom/samsung/android/sconnect/central/ActionFormatter;Z)V
    .locals 1
    .param p1, "actionFormatter"    # Lcom/samsung/android/sconnect/central/ActionFormatter;
    .param p2, "isContentRalatedMode"    # Z

    .prologue
    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mActionList:Ljava/util/ArrayList;

    .line 25
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mPrimaryAction:I

    .line 26
    invoke-virtual {p1, p0, p2}, Lcom/samsung/android/sconnect/central/ActionFormatter;->setActionList(Lcom/samsung/android/sconnect/central/ui/DisplayItem;Z)V

    .line 28
    return-void
.end method

.method public setActionList(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mActionList:Ljava/util/ArrayList;

    .line 32
    return-void
.end method

.method public setDeviceView(Landroid/widget/LinearLayout;)V
    .locals 0
    .param p1, "view"    # Landroid/widget/LinearLayout;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mDeviceView:Landroid/widget/LinearLayout;

    .line 56
    return-void
.end method

.method public setPrimaryAction(I)V
    .locals 0
    .param p1, "primaryAction"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mPrimaryAction:I

    .line 48
    return-void
.end method

.method public updateActionList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 36
    return-void
.end method
