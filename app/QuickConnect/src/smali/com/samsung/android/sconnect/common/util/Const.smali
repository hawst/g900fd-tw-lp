.class public Lcom/samsung/android/sconnect/common/util/Const;
.super Ljava/lang/Object;
.source "Const.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/common/util/Const$ContentsType;,
        Lcom/samsung/android/sconnect/common/util/Const$UpnpDeviceType;,
        Lcom/samsung/android/sconnect/common/util/Const$SconnectDeviceType;
    }
.end annotation


# static fields
.field public static final ACTION_ASK_TO_EMEET:I = 0x11

.field public static final ACTION_CANCEL:I = 0x63

.field public static final ACTION_CHROMECAST:I = 0x13

.field public static final ACTION_CONFIRM:I = 0x62

.field public static final ACTION_CONFIRM_EXTRA_REMOTE_BT_MAC:Ljava/lang/String; = "REMOTE_BT_MAC"

.field public static final ACTION_CONNECT:I = 0xc

.field public static final ACTION_EXPLORE_CONTENTS:I = 0x7

.field public static final ACTION_JOIN_TO_GROUP_PLAY:I = 0xe

.field public static final ACTION_MIRROR_SCREEN:I = 0x5

.field public static final ACTION_MORE_FEATURES:I = 0x3

.field public static final ACTION_P2P_PAIRING:I = 0xf

.field public static final ACTION_PLAY_CONTENT:I = 0xa

.field public static final ACTION_PRINT:I = 0xd

.field public static final ACTION_PRINT_OTHERS:I = 0x14

.field public static final ACTION_PROFILE_SHARE:I = 0x12

.field public static final ACTION_SEND_CONTENTS_MULTIPLE:I = 0x50

.field public static final ACTION_SEND_CURRENT_CONTENTS:I = 0x0

.field public static final ACTION_SEND_OTHER_CONTENTS:I = 0x1

.field public static final ACTION_SHARE_VIA_GROUP_PLAY:I = 0x2

.field public static final ACTION_SIDESYNC_CONTROL:I = 0x10

.field public static final ACTION_TOGETHER_CREATE_ROOM:I = 0x8

.field public static final ACTION_TOGETHER_JOIN_ROOM:I = 0x9

.field public static final ACTION_TV:I = 0xb

.field public static DEVICE_STATE_IDLE:I = 0x0

.field public static DEVICE_STATE_MIRRORING:I = 0x0

.field public static DEVICE_STATE_SENDED:I = 0x0

.field public static DEVICE_STATE_SENDING:I = 0x0

.field public static final DISCOVERY_TYPE_ALL:I = 0xff

.field public static final DISCOVERY_TYPE_BLE:I = 0x8

.field public static final DISCOVERY_TYPE_BLE_P2P:I = 0xa

.field public static final DISCOVERY_TYPE_BT:I = 0x4

.field public static final DISCOVERY_TYPE_CHROMECAST:I = 0x80

.field public static final DISCOVERY_TYPE_NONE:I = 0x0

.field public static final DISCOVERY_TYPE_P2P:I = 0x2

.field public static final DISCOVERY_TYPE_PRINTER:I = 0x20

.field public static final DISCOVERY_TYPE_SLINK:I = 0x40

.field public static final DISCOVERY_TYPE_UPNP:I = 0x10

.field public static final DISCOVERY_TYPE_WIFI:I = 0x1

.field public static final MSG_ADD_DEVICE:I = 0xbb8

.field public static final MSG_ADD_FAVORITE_DEVICE:I = 0xbca

.field public static final MSG_ANIMATE_SENDING_VIEW:I = 0xbc0

.field public static final MSG_ATTACH_SELECTOR:I = 0xbc6

.field public static final MSG_CANCEL_HOME_CHECKER:I = 0xbc9

.field public static final MSG_DISCOVERY_FINISHED:I = 0xbbc

.field public static final MSG_DISCOVERY_STARTED:I = 0xbbb

.field public static final MSG_FINISH_DISPLAY:I = 0xbc4

.field public static final MSG_HOME_RESUMED:I = 0xbc7

.field public static final MSG_PINCH_ZOOM:I = 0xbbd

.field public static final MSG_REMOVE_DEVICE:I = 0xbb9

.field public static final MSG_REMOVE_FAVORITE_DEVICE:I = 0xbcb

.field public static final MSG_REMOVE_SENDING_VIEW:I = 0xbc1

.field public static final MSG_SHOW_MENU:I = 0xbc5

.field public static final MSG_SHOW_SENDING_VIEW:I = 0xbbf

.field public static final MSG_START_HOME_CHECKER:I = 0xbc8

.field public static final MSG_START_SCONNECT_DISPLAY:I = 0xbc2

.field public static final MSG_START_SENDING_VIEW:I = 0xbbe

.field public static final MSG_UPDATE_DEVICE:I = 0xbba

.field public static final MSG_UPDATE_DEVICE_VIEW:I = 0xbcc

.field public static NETWORK_BT:B = 0x0t

.field public static NETWORK_P2P:B = 0x0t

.field public static NETWORK_WIFI:B = 0x0t

.field public static final RSSI_BLE_THRESHOLD:I = -0x32

.field public static final RSSI_BT_THRESHOLD:I = -0x41

.field public static final SCAN_BLE_P2P_PERIOD:J = 0x1b58L

.field public static final SCAN_BT_PERIOD:J = 0x32c8L

.field public static final SCAN_MARGIN_TIME:J = 0x64L

.field public static final SCAN_PARTIAL_AP_PERIOD:J = 0x3e8L

.field public static final SCAN_UPNP_PERIOD:J = 0x1f4L

.field public static SERVICE_EMEETING:I = 0x0

.field public static SERVICE_FILESHARE:I = 0x0

.field public static SERVICE_GROUPPLAY:I = 0x0

.field public static SERVICE_MIRRORING:I = 0x0

.field public static SERVICE_PROFILE_SHARE:I = 0x0

.field public static SERVICE_SIDESYNC_SOURCE:I = 0x0

.field public static SERVICE_SLINK_BROWSE_CONTENTS:I = 0x0

.field public static SERVICE_TOGETHER:I = 0x0

.field public static final SETTING_EVERY_ONE:I = 0x0

.field public static final SETTING_NOT_VISIBLE:I = 0x2

.field public static final SETTING_ONLY_CONTACTS:I = 0x1

.field public static final SUPPORT_WEARABLE_RSSI_CHECK:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 115
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_IDLE:I

    .line 116
    sput v1, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_SENDING:I

    .line 117
    sput v2, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_SENDED:I

    .line 118
    const/4 v0, 0x3

    sput v0, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_MIRRORING:I

    .line 123
    const/16 v0, 0x200

    sput v0, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_PROFILE_SHARE:I

    .line 124
    const/16 v0, 0x100

    sput v0, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_TOGETHER:I

    .line 125
    const/16 v0, 0x80

    sput v0, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_EMEETING:I

    .line 127
    const/16 v0, 0x20

    sput v0, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_SLINK_BROWSE_CONTENTS:I

    .line 128
    const/16 v0, 0x10

    sput v0, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    .line 130
    sput v3, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_FILESHARE:I

    .line 131
    sput v2, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_SIDESYNC_SOURCE:I

    .line 132
    sput v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_GROUPPLAY:I

    .line 134
    sput-byte v1, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_BT:B

    .line 135
    sput-byte v2, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_WIFI:B

    .line 136
    sput-byte v3, Lcom/samsung/android/sconnect/common/util/Const;->NETWORK_P2P:B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    return-void
.end method
