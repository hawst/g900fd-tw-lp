.class Lcom/samsung/android/sconnect/common/net/BleHelper$3;
.super Landroid/content/BroadcastReceiver;
.source "BleHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/BleHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/BleHelper;)V
    .locals 0

    .prologue
    .line 774
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 777
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 778
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 779
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    .line 780
    .local v1, "user":I
    const-string v2, "BleHelper"

    const-string v3, "mBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SCREEN_ON when user = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    if-nez v1, :cond_0

    .line 782
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    .line 795
    .end local v1    # "user":I
    :cond_0
    :goto_0
    return-void

    .line 784
    :cond_1
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 785
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    .line 786
    .restart local v1    # "user":I
    const-string v2, "BleHelper"

    const-string v3, "mBroadcastReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SCREEN_OFF when user = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 787
    if-nez v1, :cond_0

    .line 788
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isBleScanFilterSupported()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 789
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/net/BleHelper;->startScan(I)V

    goto :goto_0

    .line 791
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BleHelper$3;->this$0:Lcom/samsung/android/sconnect/common/net/BleHelper;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sconnect/common/net/BleHelper;->stopScan(Z)V

    goto :goto_0
.end method
