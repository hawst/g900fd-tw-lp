.class public Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;
.super Ljava/lang/Object;
.source "FavoriteDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;
    }
.end annotation


# static fields
.field private static final ADD_ICON:I = 0x64

.field private static final CUSTOM_ICON:I = 0x0

.field private static final RCS_CAMERA_KEY:Ljava/lang/String; = "Rcs_Camera_Key"

.field private static final RCS_CAMERA_VALUE:Ljava/lang/String; = "Rcs_Camera_Request"

.field private static final TAG:Ljava/lang/String; = "FavoriteDeviceManager"

.field private static mCheckedIcon:I

.field private static mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

.field private static mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;


# instance fields
.field private mAddIconDialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

.field private mFavoriteDialog:Landroid/app/AlertDialog;

.field private mGUIHandler:Landroid/os/Handler;

.field private mIconDialog:Landroid/app/AlertDialog;

.field private mIconDialogView:Landroid/view/View;

.field private mIconGridView:Landroid/widget/GridView;

.field private mNameDialog:Landroid/app/AlertDialog;

.field private mNameDialogView:Landroid/view/View;

.field private mNameEditText:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    sput-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .line 74
    sput-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 75
    const/4 v0, -0x1

    sput v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/samsung/android/sconnect/common/util/GUIUtil;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "guiHandler"    # Landroid/os/Handler;
    .param p3, "guiUtil"    # Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .prologue
    const/4 v0, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 64
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIHandler:Landroid/os/Handler;

    .line 66
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDialog:Landroid/app/AlertDialog;

    .line 67
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mAddIconDialog:Landroid/app/AlertDialog;

    .line 78
    const-string v0, "FavoriteDeviceManager"

    const-string v1, "FavoriteDeviceManager"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    .line 80
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIHandler:Landroid/os/Handler;

    .line 81
    sput-object p3, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    .line 82
    return-void
.end method

.method static synthetic access$000()Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 53
    sput-object p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object p0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->removeFavorite()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->changeName(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1300()Lcom/samsung/android/sconnect/common/util/GUIUtil;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->addFavorite()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showIconDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showNameDialog()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 53
    sget v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I

    return v0
.end method

.method static synthetic access$602(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 53
    sput p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I

    return p0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showAddIconDialog()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private addFavorite()V
    .locals 11

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    if-nez v0, :cond_0

    .line 234
    new-instance v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->open()Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 237
    sget-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v9

    .line 238
    .local v9, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v6

    .line 239
    .local v6, "name":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v10

    .line 240
    .local v10, "id":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    const-string v0, "FavoriteDeviceManager"

    const-string v1, "addFavorite"

    invoke-static {v0, v1, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-virtual {v9, v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setFavoriteName(Ljava/lang/String;)V

    .line 243
    const/4 v7, 0x0

    .line 244
    .local v7, "imagePath":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getContactImage()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 245
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".png"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 246
    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getContactImage()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0, v7}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->saveBitmapToFile(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 247
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 249
    :cond_1
    const-string v0, "FavoriteDeviceManager"

    const-string v1, "addFavorite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 252
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-object v2, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-object v3, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    iget-object v4, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v5, v10, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v8

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->insert(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 254
    const/16 v0, 0xbb9

    invoke-virtual {p0, v0, v9}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 255
    const/16 v0, 0xbca

    invoke-virtual {p0, v0, v9}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 256
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 258
    return-void
.end method

.method private changeIcon(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "iconOriginalImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 417
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".png"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 418
    .local v0, "iconPath":Ljava/lang/String;
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->saveBitmapToFile(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    .line 419
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 420
    sget-object v1, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setFavoriteImage(Landroid/graphics/Bitmap;)V

    .line 421
    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->updateIcon(Ljava/lang/String;)V

    .line 422
    return-void
.end method

.method private changeName(Ljava/lang/String;)V
    .locals 9
    .param p1, "newName"    # Ljava/lang/String;

    .prologue
    .line 503
    sget-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v7

    .line 504
    .local v7, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v0, "FavoriteDeviceManager"

    const-string v1, "changeName"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getFavoriteName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    invoke-virtual {v7, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setFavoriteName(Ljava/lang/String;)V

    .line 506
    invoke-virtual {v7}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v8

    .line 507
    .local v8, "id":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    if-nez v0, :cond_0

    .line 508
    new-instance v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->open()Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 511
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v2, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-object v3, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-object v4, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    iget-object v5, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v6, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->updateName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 514
    const/16 v0, 0xbcc

    sget-object v1, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 515
    return-void
.end method

.method private deleteFile()V
    .locals 11

    .prologue
    .line 328
    sget-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v8

    .line 329
    .local v8, "id":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->open()Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 331
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-object v2, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-object v3, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    iget-object v4, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v5, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->queryByAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 333
    .local v6, "cur":Landroid/database/Cursor;
    if-eqz v6, :cond_1

    .line 334
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    const-string v0, "IMAGE"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 337
    .local v10, "oldImagePath":Ljava/lang/String;
    if-eqz v10, :cond_0

    .line 339
    :try_start_0
    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    .end local v10    # "oldImagePath":Ljava/lang/String;
    :cond_0
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 350
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 351
    return-void

    .line 340
    .restart local v10    # "oldImagePath":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 341
    .local v7, "e":Ljava/lang/NumberFormatException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v10, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    .line 342
    .local v9, "localPath":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v9}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    goto :goto_0

    .line 348
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    .end local v9    # "localPath":Ljava/lang/String;
    .end local v10    # "oldImagePath":Ljava/lang/String;
    :cond_1
    const-string v0, "FavoriteDeviceManager"

    const-string v1, "deleteFile"

    const-string v2, "cursor is null!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private removeFavorite()V
    .locals 8

    .prologue
    .line 282
    sget-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    .line 283
    .local v6, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const-string v0, "FavoriteDeviceManager"

    const-string v1, "removeFavorite"

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    .line 286
    .local v7, "id":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    if-nez v0, :cond_0

    .line 287
    new-instance v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 289
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->deleteFile()V

    .line 290
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->open()Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 291
    const/4 v0, -0x1

    invoke-virtual {v6, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setImageType(I)V

    .line 292
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v1, 0x7f070021

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getContactImage()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->cropIcon(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setImage(Landroid/graphics/Bitmap;)V

    .line 294
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-object v2, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-object v3, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    iget-object v4, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v5, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->deleteDevice(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const/16 v0, 0xbcb

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 297
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 298
    return-void
.end method

.method private saveBitmapToFile(Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 261
    const/4 v1, 0x0

    .line 263
    .local v1, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, p2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    .line 265
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 266
    const-string v2, "FavoriteDeviceManager"

    const-string v3, "Capture"

    const-string v4, "bitmap.compress failed."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    :cond_0
    if-eqz v1, :cond_1

    .line 273
    :try_start_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 279
    :cond_1
    :goto_0
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 268
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 269
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 272
    if-eqz v1, :cond_1

    .line 273
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 275
    :catch_2
    move-exception v0

    .line 276
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 271
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 272
    if-eqz v1, :cond_2

    .line 273
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 277
    :cond_2
    :goto_1
    throw v2

    .line 275
    :catch_3
    move-exception v0

    .line 276
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private showAddIconDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 464
    const/4 v0, 0x0

    .line 466
    .local v0, "items":[Ljava/lang/String;
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    .line 467
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v3, 0x7f090092

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 468
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v2, 0x7f090090

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    .line 470
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mAddIconDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mAddIconDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 471
    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f09018d

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$4;-><init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mAddIconDialog:Landroid/app/AlertDialog;

    .line 497
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mAddIconDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 498
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mAddIconDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 500
    :cond_1
    return-void
.end method

.method private showIconDialog()V
    .locals 7

    .prologue
    .line 425
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03000c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconDialogView:Landroid/view/View;

    .line 427
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconDialogView:Landroid/view/View;

    const v3, 0x7f0c003a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconGridView:Landroid/widget/GridView;

    .line 429
    new-instance v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;-><init>(Landroid/content/Context;)V

    .line 430
    .local v0, "myAdapter":Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$FavoriteIconSelectorAdapter;
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 432
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconGridView:Landroid/widget/GridView;

    new-instance v3, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$3;-><init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 448
    const-string v1, ""

    .line 449
    .local v1, "typeString":Ljava/lang/String;
    sget v2, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I

    if-nez v2, :cond_0

    .line 450
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v3, 0x7f090090

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 454
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconGridView:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v4, 0x7f09015e

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 457
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconDialogView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f09018c

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconDialog:Landroid/app/AlertDialog;

    .line 460
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mIconDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 461
    return-void

    .line 452
    :cond_0
    sget-object v2, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIUtil:Lcom/samsung/android/sconnect/common/util/GUIUtil;

    sget v3, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mCheckedIcon:I

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getDeviceString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private showInputMethod()V
    .locals 4

    .prologue
    .line 579
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 581
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$8;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$8;-><init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 610
    :goto_0
    return-void

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$9;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$9;-><init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private showNameDialog()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 518
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03000d

    invoke-virtual {v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameDialogView:Landroid/view/View;

    .line 520
    sget-object v2, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v1

    .line 521
    .local v1, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameDialogView:Landroid/view/View;

    const v3, 0x7f0c003b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    .line 523
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 524
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 526
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 531
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 532
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->selectAll()V

    .line 533
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    new-instance v3, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$5;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$5;-><init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 543
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showInputMethod()V

    .line 545
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameDialogView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f09018b

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a

    new-instance v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$6;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$6;-><init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000

    invoke-virtual {v2, v3, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameDialog:Landroid/app/AlertDialog;

    .line 556
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameEditText:Landroid/widget/EditText;

    new-instance v3, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$7;

    invoke-direct {v3, p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$7;-><init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 575
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mNameDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 576
    return-void

    .line 527
    :catch_0
    move-exception v0

    .line 528
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v2, "FavoriteDeviceManager"

    const-string v3, "Caught Exception setSelection"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateIcon(Ljava/lang/String;)V
    .locals 8
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 316
    sget-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v7

    .line 317
    .local v7, "id":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    if-nez v0, :cond_0

    .line 318
    new-instance v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 320
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->deleteFile()V

    .line 321
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->open()Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 322
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-object v2, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-object v3, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    iget-object v4, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v5, v7, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->updateImagePath(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 325
    return-void
.end method


# virtual methods
.method public changeIcon(I)V
    .locals 10
    .param p1, "type"    # I

    .prologue
    const/4 v9, 0x0

    const/16 v8, 0xbcc

    .line 354
    const-string v4, "FavoriteDeviceManager"

    const-string v5, "changeIcon"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    if-nez p1, :cond_2

    .line 356
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImageType()I

    move-result v4

    if-lez v4, :cond_1

    .line 357
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setImageType(I)V

    .line 358
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getContactImage()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 359
    .local v0, "contactIcon":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 360
    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->changeIcon(Landroid/graphics/Bitmap;)V

    .line 362
    :cond_0
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {p0, v8, v4}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 384
    .end local v0    # "contactIcon":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-void

    .line 365
    :cond_2
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getFavoriteImage()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 366
    .local v2, "oldFavoriteIcon":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_3

    .line 367
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setFavoriteImage(Landroid/graphics/Bitmap;)V

    .line 368
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImage()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 369
    .local v3, "oldIcon":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_3

    .line 370
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getContactImage()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 371
    .local v1, "contactImage":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_4

    .line 372
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v6, 0x7f070021

    invoke-static {v5, v6, v1}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->cropIcon(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setImage(Landroid/graphics/Bitmap;)V

    .line 379
    .end local v1    # "contactImage":Landroid/graphics/Bitmap;
    .end local v3    # "oldIcon":Landroid/graphics/Bitmap;
    :cond_3
    :goto_1
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setImageType(I)V

    .line 380
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->updateIcon(Ljava/lang/String;)V

    .line 381
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {p0, v8, v4}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->notifyGuiMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 375
    .restart local v1    # "contactImage":Landroid/graphics/Bitmap;
    .restart local v3    # "oldIcon":Landroid/graphics/Bitmap;
    :cond_4
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4, v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setImage(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method

.method public changeIcon(Landroid/net/Uri;)V
    .locals 9
    .param p1, "imageUri"    # Landroid/net/Uri;

    .prologue
    const/4 v8, 0x0

    .line 387
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    if-nez v4, :cond_1

    .line 388
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v5, 0x7f0900e6

    invoke-static {v4, v5, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    if-eqz p1, :cond_2

    .line 392
    const-string v4, "FavoriteDeviceManager"

    const-string v5, "changeIcon"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setImageType(I)V

    .line 395
    const/4 v0, 0x0

    .line 397
    .local v0, "clsInputStream":Ljava/io/InputStream;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 401
    :goto_1
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 402
    .local v3, "image":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_0

    .line 403
    invoke-direct {p0, v3}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->changeIcon(Landroid/graphics/Bitmap;)V

    .line 404
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v5, 0x7f070021

    invoke-static {v4, v5, v3}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->cropIcon(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 405
    .local v2, "icon":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 406
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setImage(Landroid/graphics/Bitmap;)V

    .line 407
    const/16 v4, 0xbcc

    sget-object v5, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->notifyGuiMessage(ILjava/lang/Object;)V

    goto :goto_0

    .line 398
    .end local v2    # "icon":Landroid/graphics/Bitmap;
    .end local v3    # "image":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v1

    .line 399
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 411
    .end local v0    # "clsInputStream":Ljava/io/InputStream;
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :cond_2
    const-string v4, "FavoriteDeviceManager"

    const-string v5, "changeIcon"

    const-string v6, "imageUri is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public changeSelectedItem(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    .line 735
    const-string v0, "FavoriteDeviceManager"

    const-string v1, "changeSelectedItem"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " item is changed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    sput-object p1, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 737
    return-void
.end method

.method public getSelectedItem()Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .locals 1

    .prologue
    .line 731
    sget-object v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    return-object v0
.end method

.method public isFavorite()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 199
    sget-object v3, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    .line 201
    .local v1, "id":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    if-nez v3, :cond_0

    .line 202
    new-instance v3, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 204
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->open()Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 206
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    const-string v4, "P2PMAC"

    iget-object v5, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->query(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 207
    .local v0, "cur":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 208
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 209
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 229
    :goto_0
    return v2

    .line 212
    :cond_1
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 213
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    const-string v4, "BTMAC"

    iget-object v5, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->query(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 214
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 215
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 216
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 219
    :cond_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 220
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    const-string v4, "UUID"

    iget-object v5, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->query(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 221
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 222
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 223
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 226
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 227
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 229
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isFavoriteExist()Z
    .locals 6

    .prologue
    .line 139
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    if-nez v2, :cond_0

    .line 140
    new-instance v2, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 142
    :cond_0
    const/4 v0, 0x0

    .line 144
    .local v0, "bIsFavoriteExist":Z
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->open()Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 145
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->queryAll()Landroid/database/Cursor;

    move-result-object v1

    .line 146
    .local v1, "cur":Landroid/database/Cursor;
    if-eqz v1, :cond_2

    .line 147
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 148
    const/4 v0, 0x1

    .line 151
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 153
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 154
    const-string v2, "FavoriteDeviceManager"

    const-string v3, "isFavoriteExist"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    return v0
.end method

.method public notifyGuiMessage(I)V
    .locals 3
    .param p1, "message"    # I

    .prologue
    const/4 v2, 0x0

    .line 613
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 618
    :goto_0
    return-void

    .line 616
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 617
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public notifyGuiMessage(ILjava/lang/Object;)V
    .locals 3
    .param p1, "message"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v2, 0x0

    .line 621
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 627
    :goto_0
    return-void

    .line 624
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2, p1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 625
    .local v0, "msg":Landroid/os/Message;
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 626
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mGUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public queryFavoriteDevice()V
    .locals 14

    .prologue
    .line 159
    const-string v1, "FavoriteDeviceManager"

    const-string v11, "queryFavoriteDevice"

    const-string v12, ""

    invoke-static {v1, v11, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    if-nez v1, :cond_0

    .line 161
    new-instance v1, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v11, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v11}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->open()Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 165
    const/4 v2, 0x0

    .line 166
    .local v2, "uuid":Ljava/lang/String;
    const/4 v3, 0x0

    .line 167
    .local v3, "p2pMac":Ljava/lang/String;
    const/4 v4, 0x0

    .line 168
    .local v4, "wifiMac":Ljava/lang/String;
    const/4 v5, 0x0

    .line 169
    .local v5, "btMac":Ljava/lang/String;
    const/4 v6, 0x0

    .line 170
    .local v6, "bleMac":Ljava/lang/String;
    const/4 v7, 0x0

    .line 171
    .local v7, "name":Ljava/lang/String;
    const/4 v8, 0x0

    .line 172
    .local v8, "imagePath":Ljava/lang/String;
    const/4 v9, 0x0

    .line 174
    .local v9, "type":I
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->queryAll()Landroid/database/Cursor;

    move-result-object v10

    .line 175
    .local v10, "cur":Landroid/database/Cursor;
    if-eqz v10, :cond_2

    .line 176
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 177
    const-string v1, "UUID"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 178
    const-string v1, "P2PMAC"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 179
    const-string v1, "WIFIMAC"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 180
    const-string v1, "BTMAC"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 181
    const-string v1, "BLEMAC"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 182
    const-string v1, "NAME"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 183
    const-string v1, "IMAGE"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 184
    const-string v1, "TYPE"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 185
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct/range {v0 .. v9}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 187
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setFavorite(Z)V

    .line 188
    const-string v1, "FavoriteDeviceManager"

    const-string v11, "queryFavoriteDevice"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v1, v11, v12}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const/16 v1, 0xbca

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->notifyGuiMessage(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 192
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 194
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V

    .line 196
    return-void
.end method

.method public showFavoriteDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
    .locals 9
    .param p1, "selectedItem"    # Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 85
    const-string v4, "FavoriteDeviceManager"

    const-string v5, "showFavoriteDialog"

    const-string v6, ""

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    sput-object p1, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 87
    sget-object v4, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v0

    .line 88
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v1

    .line 90
    .local v1, "isFavorite":Z
    const/4 v2, 0x0

    .line 92
    .local v2, "items":[Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 93
    const/4 v4, 0x3

    new-array v2, v4, [Ljava/lang/String;

    .line 94
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v5, 0x7f09018a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v8

    .line 95
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v5, 0x7f09018c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v7

    .line 96
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v6, 0x7f09018b

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 102
    :goto_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDialog:Landroid/app/AlertDialog;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v4

    if-nez v4, :cond_1

    .line 103
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getVisibleName()Ljava/lang/String;

    move-result-object v3

    .line 104
    .local v3, "name":Ljava/lang/String;
    new-instance v4, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;

    invoke-direct {v5, p0, v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;-><init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;Z)V

    invoke-virtual {v4, v2, v5}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$1;

    invoke-direct {v5, p0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$1;-><init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDialog:Landroid/app/AlertDialog;

    .line 133
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 134
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDialog:Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->show()V

    .line 136
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    return-void

    .line 98
    :cond_2
    new-array v2, v7, [Ljava/lang/String;

    .line 99
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    const v5, 0x7f090189

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v8

    goto :goto_0
.end method

.method public updateFavorite(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 9
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    .line 301
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v8

    .line 302
    .local v8, "id":Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    if-nez v0, :cond_0

    .line 303
    new-instance v0, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 306
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->open()Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    .line 307
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    iget-object v1, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-object v2, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-object v3, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    iget-object v4, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v5, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v6

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->update(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 309
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->mFavoriteDBHelper:Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceDBHelper;->close()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :goto_0
    return-void

    .line 310
    :catch_0
    move-exception v7

    .line 311
    .local v7, "e":Landroid/database/SQLException;
    const-string v0, "FavoriteDeviceManager"

    const-string v1, "updateFavorite"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
