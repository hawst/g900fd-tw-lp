.class Lcom/samsung/android/sconnect/SconnectManager$2;
.super Landroid/content/BroadcastReceiver;
.source "SconnectManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/SconnectManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/SconnectManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/SconnectManager;)V
    .locals 0

    .prologue
    .line 695
    iput-object p1, p0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 18
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 698
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 699
    .local v1, "action":Ljava/lang/String;
    const-string v13, "com.samsung.android.sconnect.periph.ACCEPT_RESULT"

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 700
    const-string v13, "RES"

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 701
    .local v10, "result":Z
    const-string v13, "CMD"

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 702
    .local v6, "cmd":I
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ACTION_P_ACCEPT_RESULT :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "-"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    const/4 v14, 0x0

    # setter for: Lcom/samsung/android/sconnect/SconnectManager;->mIsAcceptDialogShowing:Z
    invoke-static {v13, v14}, Lcom/samsung/android/sconnect/SconnectManager;->access$502(Lcom/samsung/android/sconnect/SconnectManager;Z)Z

    .line 704
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 705
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopScanForPopup()V

    .line 708
    :cond_0
    const-string v13, "ADDRESS"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 709
    .local v3, "address":Ljava/lang/String;
    if-eqz v10, :cond_1

    .line 710
    const/16 v13, 0x11

    if-ne v6, v13, :cond_2

    .line 711
    const-string v13, "BT_MAC_ADDRESS"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 712
    .local v5, "btMacAddr":Ljava/lang/String;
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "accept the Emeeting invitation from "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static {v13, v14, v15, v5}, Lcom/samsung/android/sconnect/central/action/EmeetingActionHelper;->launchEmeeting(Landroid/content/Context;ZLjava/util/ArrayList;Ljava/lang/String;)V

    .line 799
    .end local v3    # "address":Ljava/lang/String;
    .end local v5    # "btMacAddr":Ljava/lang/String;
    .end local v6    # "cmd":I
    .end local v10    # "result":Z
    :cond_1
    :goto_0
    return-void

    .line 716
    .restart local v3    # "address":Ljava/lang/String;
    .restart local v6    # "cmd":I
    .restart local v10    # "result":Z
    :cond_2
    const/16 v13, 0xe

    if-ne v6, v13, :cond_6

    .line 717
    const-string v13, "BSSID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 718
    .local v4, "bssidAddr":Ljava/lang/String;
    const-string v13, "CONTENT_TYPE"

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 719
    .local v7, "contentType":I
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "accept the GroupPlay invitation from "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " [type]"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    const/4 v12, 0x0

    .line 722
    .local v12, "type":Ljava/lang/String;
    const/4 v13, 0x1

    if-ne v7, v13, :cond_4

    .line 723
    const-string v12, "video/*"

    .line 730
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v13, v4, v4, v14, v12}, Lcom/samsung/android/sconnect/central/action/GroupPlayActionHelper;->launchGroupPlayAsSlave(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0

    .line 724
    :cond_4
    const/4 v13, 0x2

    if-ne v7, v13, :cond_5

    .line 725
    const-string v12, "audio/*"

    goto :goto_1

    .line 726
    :cond_5
    const/4 v13, 0x3

    if-ne v7, v13, :cond_3

    .line 727
    const-string v12, "image/*"

    goto :goto_1

    .line 733
    .end local v4    # "bssidAddr":Ljava/lang/String;
    .end local v7    # "contentType":I
    .end local v12    # "type":Ljava/lang/String;
    :cond_6
    const/16 v13, 0x8

    if-ne v6, v13, :cond_7

    .line 734
    const-string v13, "BSSID"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 735
    .restart local v4    # "bssidAddr":Ljava/lang/String;
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "accept the TOGETHER invitation from "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v13

    const/4 v14, 0x0

    invoke-static {v13, v4, v14}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->startTogetherWithRoomId(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 739
    .end local v4    # "bssidAddr":Ljava/lang/String;
    :cond_7
    const/16 v13, 0x10

    if-ne v6, v13, :cond_8

    .line 740
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    const-string v15, "accept the SideSync control msg "

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mContext:Landroid/content/Context;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$300(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/content/Context;

    move-result-object v13

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x1

    invoke-static/range {v13 .. v17}, Lcom/samsung/android/sconnect/central/action/SideSyncActionHelper;->startSideSync(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 744
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v13

    invoke-virtual {v13, v3}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isConnectedWith(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_1

    .line 745
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    const-string v15, "ACTION_P_ACCEPT_RESULT : waitingPeer"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;

    move-result-object v13

    const/16 v14, 0x17

    invoke-virtual {v13, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 749
    .end local v3    # "address":Ljava/lang/String;
    .end local v6    # "cmd":I
    .end local v10    # "result":Z
    :cond_9
    const-string v13, "com.samsung.android.sconnect.central.DISCONNECT_P2P"

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_c

    .line 750
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v13

    if-eqz v13, :cond_1

    .line 751
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mWfdUtil:Lcom/samsung/android/sconnect/common/util/WfdUtil;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$800(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/util/WfdUtil;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/common/util/WfdUtil;->isWfdConnected()Z

    move-result v13

    if-eqz v13, :cond_a

    .line 752
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/SconnectManager;->stopMirroring()V

    goto/16 :goto_0

    .line 754
    :cond_a
    const-string v13, "FORCE"

    const/4 v14, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 755
    .local v8, "forceRemove":Z
    if-eqz v8, :cond_b

    .line 756
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    const-string v15, "ACTION_C_DISCONNECT_P2P : removeGroup"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeGroup(Z)V

    goto/16 :goto_0

    .line 758
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isGroupOwner()Z

    move-result v13

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->getClientCount()I

    move-result v13

    if-nez v13, :cond_1

    .line 759
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    const-string v15, "ACTION_C_DISCONNECT_P2P : removeGroup -dummy"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->removeGroup(Z)V

    goto/16 :goto_0

    .line 765
    .end local v8    # "forceRemove":Z
    :cond_c
    const-string v13, "com.samsung.android.sconnect.central.WAITING_CANCEL"

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_f

    .line 766
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    const-string v15, "ACTION_C_WAITING_CANCEL"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 767
    const-string v13, "ADDRESSLIST"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 768
    .local v2, "addrList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;

    move-result-object v13

    const/16 v14, 0x18

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeMessages(I)V

    .line 769
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/HashMap;->clear()V

    .line 770
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v13

    if-eqz v13, :cond_d

    .line 771
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopCentral()V

    .line 773
    :cond_d
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-eqz v13, :cond_1

    .line 774
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_e
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 775
    .local v11, "target":Ljava/lang/String;
    if-eqz v11, :cond_e

    .line 776
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v13

    invoke-virtual {v13, v11}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->cancelConnectPeer(Ljava/lang/String;)V

    .line 777
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v13

    invoke-virtual {v13, v11}, Lcom/samsung/android/sconnect/central/CentralActionManager;->removeHoldingIntent(Ljava/lang/String;)Z

    goto :goto_2

    .line 781
    .end local v2    # "addrList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v11    # "target":Ljava/lang/String;
    :cond_f
    const-string v13, "com.samsung.android.sconnect.central.PRINTER_WAITING_CANCEL"

    invoke-virtual {v13, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 782
    const-string v13, "SconnectManager"

    const-string v14, "mLocalReceiver"

    const-string v15, "ACTION_C_PRINTER_WAITING_CANCEL"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 783
    const-string v13, "ADDRESSLIST"

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 784
    .restart local v2    # "addrList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->ScanHandler:Landroid/os/Handler;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$700(Lcom/samsung/android/sconnect/SconnectManager;)Landroid/os/Handler;

    move-result-object v13

    const/16 v14, 0x18

    invoke-virtual {v13, v14}, Landroid/os/Handler;->removeMessages(I)V

    .line 785
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mRequest:Ljava/util/HashMap;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$400(Lcom/samsung/android/sconnect/SconnectManager;)Ljava/util/HashMap;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/HashMap;->clear()V

    .line 786
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v13

    if-eqz v13, :cond_10

    .line 787
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$600(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->stopCentral()V

    .line 789
    :cond_10
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-eqz v13, :cond_1

    .line 790
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .restart local v9    # "i$":Ljava/util/Iterator;
    :cond_11
    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 791
    .restart local v11    # "target":Ljava/lang/String;
    if-eqz v11, :cond_11

    .line 792
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v13

    invoke-virtual {v13, v11}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->cancelConnectPeer(Ljava/lang/String;)V

    .line 793
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mP2pHelper:Lcom/samsung/android/sconnect/common/net/P2pHelper;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$100(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->cancelP2pConnectRequest()V

    .line 794
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/sconnect/SconnectManager$2;->this$0:Lcom/samsung/android/sconnect/SconnectManager;

    # getter for: Lcom/samsung/android/sconnect/SconnectManager;->mCentralActionManager:Lcom/samsung/android/sconnect/central/CentralActionManager;
    invoke-static {v13}, Lcom/samsung/android/sconnect/SconnectManager;->access$200(Lcom/samsung/android/sconnect/SconnectManager;)Lcom/samsung/android/sconnect/central/CentralActionManager;

    move-result-object v13

    invoke-virtual {v13, v11}, Lcom/samsung/android/sconnect/central/CentralActionManager;->removeHoldingIntent(Ljava/lang/String;)Z

    goto :goto_3
.end method
