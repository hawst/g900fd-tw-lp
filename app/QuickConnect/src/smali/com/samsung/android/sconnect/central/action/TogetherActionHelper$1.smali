.class Lcom/samsung/android/sconnect/central/action/TogetherActionHelper$1;
.super Landroid/content/BroadcastReceiver;
.source "TogetherActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 140
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "action":Ljava/lang/String;
    const-string v3, "com.sec.android.app.alltogether.sconnect.started"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 143
    const-string v3, "BSSID"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "bssid":Ljava/lang/String;
    const-string v3, "TogetherActionHelper"

    const-string v4, "mReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TOGETHER_ACTION_STARTED - [BSSID]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getRequestId()Ljava/lang/Byte;

    move-result-object v2

    .line 147
    .local v2, "id":Ljava/lang/Byte;
    const-string v3, "TogetherActionHelper"

    const-string v4, "mReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "sendTogetherCommand - [ID]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " [P2pMac]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mP2pMac:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " [BSSID]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mPreDiscoveryHelper:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->access$100(Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    move-result-object v3

    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper$1;->this$0:Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;

    # getter for: Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->mP2pMac:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;->access$000(Lcom/samsung/android/sconnect/central/action/TogetherActionHelper;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->sendTogetherCommand(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .end local v1    # "bssid":Ljava/lang/String;
    .end local v2    # "id":Ljava/lang/Byte;
    :cond_0
    return-void
.end method
