.class Lcom/samsung/android/sconnect/central/ui/ViewManager$5;
.super Ljava/lang/Object;
.source "ViewManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/ViewManager;->refreshDeviceList()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/ViewManager;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 347
    const-string v3, "ViewManager"

    const-string v4, "refreshDeviceList"

    const-string v5, "mDeviceListLayout post, add device list"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 348
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$600(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$200(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->NONE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$200(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v3

    iget-object v3, v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->FINISHING:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-eq v3, v4, :cond_3

    .line 350
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$200(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getExpandedDevice()Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    .line 351
    .local v0, "expandedDevice":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    if-eqz v0, :cond_0

    .line 352
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->contractActionList()V

    .line 354
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$700(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 355
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$600(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 356
    .local v2, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 357
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mDeviceList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$600(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    invoke-virtual {v4, v3, v1}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addDeviceView(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)Z

    .line 356
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 359
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$200(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isDiscovering()Z

    move-result v3

    if-nez v3, :cond_2

    .line 360
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->addTroubleShootingButton()V

    .line 363
    :cond_2
    if-eqz v0, :cond_3

    .line 364
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$200(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setExpandedDevice(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V

    .line 365
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/ViewManager$5;->this$0:Lcom/samsung/android/sconnect/central/ui/ViewManager;

    # getter for: Lcom/samsung/android/sconnect/central/ui/ViewManager;->mNearbyListView:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/ViewManager;->access$700(Lcom/samsung/android/sconnect/central/ui/ViewManager;)Landroid/widget/LinearLayout;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/sconnect/central/ui/ViewManager$5$1;

    invoke-direct {v4, p0}, Lcom/samsung/android/sconnect/central/ui/ViewManager$5$1;-><init>(Lcom/samsung/android/sconnect/central/ui/ViewManager$5;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->post(Ljava/lang/Runnable;)Z

    .line 373
    .end local v0    # "expandedDevice":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    .end local v1    # "i":I
    .end local v2    # "size":I
    :cond_3
    return-void
.end method
