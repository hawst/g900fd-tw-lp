.class Lcom/samsung/android/sconnect/central/CentralManager$1;
.super Ljava/lang/Object;
.source "CentralManager.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/CentralManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/CentralManager;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/CentralManager;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/CentralManager$1;->this$0:Lcom/samsung/android/sconnect/central/CentralManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    .line 78
    const-string v0, "CentralManager"

    const-string v1, "mDeviceListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDeviceAdded: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager$1;->this$0:Lcom/samsung/android/sconnect/central/CentralManager;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/sconnect/central/CentralManager;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 80
    return-void
.end method

.method public onDeviceRemoved(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    .line 84
    const-string v0, "CentralManager"

    const-string v1, "mDeviceListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDeviceRemoved: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager$1;->this$0:Lcom/samsung/android/sconnect/central/CentralManager;

    const/16 v1, 0xbb9

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/sconnect/central/CentralManager;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 86
    return-void
.end method

.method public onDeviceUpdated(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 4
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    .line 90
    const-string v0, "CentralManager"

    const-string v1, "mDeviceListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDeviceUpdate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager$1;->this$0:Lcom/samsung/android/sconnect/central/CentralManager;

    const/16 v1, 0xbba

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/sconnect/central/CentralManager;->notifyGuiMessage(ILjava/lang/Object;)V

    .line 93
    return-void
.end method

.method public onDiscoveryFinished()V
    .locals 3

    .prologue
    .line 104
    const-string v0, "CentralManager"

    const-string v1, "mDeviceListener"

    const-string v2, "onDiscoveryFinished"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager$1;->this$0:Lcom/samsung/android/sconnect/central/CentralManager;

    const/16 v1, 0xbbc

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/CentralManager;->notifyGuiMessage(I)V

    .line 107
    return-void
.end method

.method public onDiscoveryStarted()V
    .locals 3

    .prologue
    .line 97
    const-string v0, "CentralManager"

    const-string v1, "mDeviceListener"

    const-string v2, "onDiscoveryStarted"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/CentralManager$1;->this$0:Lcom/samsung/android/sconnect/central/CentralManager;

    const/16 v1, 0xbbb

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/CentralManager;->notifyGuiMessage(I)V

    .line 100
    return-void
.end method
