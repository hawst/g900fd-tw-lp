.class Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;
.super Ljava/lang/Object;
.source "PreDiscoveryHelper.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/SconnListener$IPreDiscoveryPacketListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)V
    .locals 0

    .prologue
    .line 598
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceived(ILjava/lang/String;Ljava/lang/String;[BLjava/lang/String;)V
    .locals 7
    .param p1, "netType"    # I
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "addr"    # Ljava/lang/String;
    .param p4, "sConnectData"    # [B
    .param p5, "btMac"    # Ljava/lang/String;

    .prologue
    .line 602
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryReceiveHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v6

    .line 604
    .local v6, "msg":Landroid/os/Message;
    new-instance v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    .line 606
    .local v0, "packet":Lcom/samsung/android/sconnect/common/net/PreDiscoveryPacket;
    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 607
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mPreDiscoveryReceiveHandler:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;
    invoke-static {v1}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1100(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$PreDiscoveryReceiveHandler;->sendMessage(Landroid/os/Message;)Z

    .line 608
    return-void
.end method

.method public onWearableDeviceAdded(Landroid/bluetooth/BluetoothDevice;[BILjava/lang/String;)V
    .locals 22
    .param p1, "device"    # Landroid/bluetooth/BluetoothDevice;
    .param p2, "wearableData"    # [B
    .param p3, "rssi"    # I
    .param p4, "btMac"    # Ljava/lang/String;

    .prologue
    .line 613
    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    .line 615
    .local v2, "bdname":Ljava/lang/String;
    :goto_0
    const/4 v5, 0x0

    .line 616
    .local v5, "wearableType":I
    const/4 v7, 0x0

    .line 617
    .local v7, "packageName":Ljava/lang/String;
    const/4 v13, 0x7

    .line 618
    .local v13, "offset":I
    const/4 v10, 0x0

    .line 619
    .local v10, "findWearable":Z
    const/16 v16, 0x0

    .line 621
    .local v16, "rssiOffset":I
    const/4 v3, 0x7

    aget-byte v3, p2, v3

    if-nez v3, :cond_0

    .line 622
    const/16 v3, 0x8

    aget-byte v3, p2, v3

    if-nez v3, :cond_4

    .line 623
    const-string v3, "PreDiscoveryHelper"

    const-string v6, "onWearableDeviceAdded"

    const-string v8, "ignore...unknown Device Type"

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    :cond_0
    :goto_1
    if-eqz v10, :cond_2

    .line 651
    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 652
    .local v4, "bleMac":Ljava/lang/String;
    new-instance v1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;

    add-int v3, p3, v16

    int-to-short v6, v3

    move-object/from16 v3, p4

    move-object/from16 v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ISLjava/lang/String;[B)V

    .line 654
    .local v1, "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;
    new-instance v17, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 655
    .local v17, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;)V

    .line 656
    iget-short v3, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mRSSI:S

    const/16 v6, -0x32

    if-le v3, v6, :cond_1

    iget-short v3, v1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mRSSI:S

    if-gez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/Util;->isScreenLocked(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 659
    sget-object v3, Lcom/samsung/android/sconnect/periph/PeriphService;->mFoundDevice:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sconnect/common/util/HomeScreenChecker;->checkHomeScreenByProcess(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 661
    const-string v3, "PreDiscoveryHelper"

    const-string v6, "onWearableDeviceAdded"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "## Show - "

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    const/16 v18, 0x0

    .line 663
    .local v18, "type":Ljava/lang/String;
    const/16 v19, 0x0

    .line 665
    .local v19, "useType":I
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Wearable"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "%04d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/16 v20, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v8, v20

    invoke-static {v6, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 666
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v19

    .line 667
    const-string v3, "PreDiscoveryHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onWearableDeviceAdded##"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "KEY:"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v20, " :"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 674
    :goto_2
    if-nez v19, :cond_5

    .line 676
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v6, "com.samsung.android.app.watchmanagerstub"

    const/4 v8, 0x0

    invoke-virtual {v3, v6, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 678
    sget-object v3, Lcom/samsung/android/sconnect/periph/PeriphService;->mFoundDevice:Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 680
    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;

    move-result-object v3

    const-class v6, Lcom/samsung/android/sconnect/periph/ui/GearFoundDialog;

    invoke-direct {v12, v3, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 681
    .local v12, "intent":Landroid/content/Intent;
    const/high16 v3, 0x30800000

    invoke-virtual {v12, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 684
    const-string v3, "NAME"

    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 685
    const-string v3, "MAC"

    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v12, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 686
    const-string v3, "MANAGER"

    invoke-virtual {v12, v3, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 687
    const-string v3, "DATA"

    move-object/from16 v0, p2

    invoke-virtual {v12, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 688
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1200(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v12}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    .line 707
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v18    # "type":Ljava/lang/String;
    .end local v19    # "useType":I
    :cond_1
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 708
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper$5;->this$0:Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    invoke-static {v3}, Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;->access$1300(Lcom/samsung/android/sconnect/common/net/PreDiscoveryHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v3, v0}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 711
    .end local v1    # "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;
    .end local v4    # "bleMac":Ljava/lang/String;
    .end local v17    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_2
    return-void

    .line 613
    .end local v2    # "bdname":Ljava/lang/String;
    .end local v5    # "wearableType":I
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v10    # "findWearable":Z
    .end local v13    # "offset":I
    .end local v16    # "rssiOffset":I
    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 625
    .restart local v2    # "bdname":Ljava/lang/String;
    .restart local v5    # "wearableType":I
    .restart local v7    # "packageName":Ljava/lang/String;
    .restart local v10    # "findWearable":Z
    .restart local v13    # "offset":I
    .restart local v16    # "rssiOffset":I
    :cond_4
    const/16 v3, 0x9

    aget-byte v3, p2, v3

    const/4 v6, 0x1

    if-ne v3, v6, :cond_0

    .line 626
    const/16 v3, 0x8

    aget-byte v5, p2, v3

    .line 628
    const/16 v3, 0xa

    aget-byte v14, p2, v3

    .line 631
    .local v14, "packageLength":B
    :try_start_2
    new-array v11, v14, [B

    .line 632
    .local v11, "gearData":[B
    const/16 v3, 0xb

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v3, v11, v6, v14}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 633
    new-instance v15, Ljava/lang/String;

    invoke-direct {v15, v11}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_0

    .line 635
    .end local v7    # "packageName":Ljava/lang/String;
    .local v15, "packageName":Ljava/lang/String;
    :try_start_3
    move-object/from16 v0, p2

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-byte v16, p2, v3

    .line 637
    const/4 v10, 0x1

    .line 638
    const-string v3, "PreDiscoveryHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onWearableDeviceAdded##"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, " ,gearData size"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v20, v14, 0x2

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v20, " ,packageName:"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v20, ",rssiOffset:"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_3

    move-object v7, v15

    .line 644
    .end local v15    # "packageName":Ljava/lang/String;
    .restart local v7    # "packageName":Ljava/lang/String;
    goto/16 :goto_1

    .line 641
    .end local v11    # "gearData":[B
    :catch_0
    move-exception v9

    .line 642
    .local v9, "e":Ljava/lang/IndexOutOfBoundsException;
    :goto_4
    const-string v3, "PreDiscoveryHelper"

    const-string v6, "onWearableDeviceAdded"

    const-string v8, "IndexOutOfBoundsException: data is wrong"

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 669
    .end local v9    # "e":Ljava/lang/IndexOutOfBoundsException;
    .end local v14    # "packageLength":B
    .restart local v1    # "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;
    .restart local v4    # "bleMac":Ljava/lang/String;
    .restart local v17    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .restart local v18    # "type":Ljava/lang/String;
    .restart local v19    # "useType":I
    :catch_1
    move-exception v9

    .line 670
    .local v9, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v3, "PreDiscoveryHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onWearableDeviceAdded##"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "KEY:"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v20, "  ettingNotFoundException"

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 689
    .end local v9    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catch_2
    move-exception v9

    .line 690
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "PreDiscoveryHelper"

    const-string v6, "onWearableDeviceAdded"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "WearableInstaller is not preloaded! - "

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 695
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_5
    const-string v3, "PreDiscoveryHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onWearableDeviceAdded  ## This device type is already founded! - "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " useType:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, v19

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, ", Mac : "

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v3, v6, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 641
    .end local v1    # "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;
    .end local v4    # "bleMac":Ljava/lang/String;
    .end local v7    # "packageName":Ljava/lang/String;
    .end local v17    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v18    # "type":Ljava/lang/String;
    .end local v19    # "useType":I
    .restart local v11    # "gearData":[B
    .restart local v14    # "packageLength":B
    .restart local v15    # "packageName":Ljava/lang/String;
    :catch_3
    move-exception v9

    move-object v7, v15

    .end local v15    # "packageName":Ljava/lang/String;
    .restart local v7    # "packageName":Ljava/lang/String;
    goto/16 :goto_4
.end method
