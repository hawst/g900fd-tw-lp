.class public Lcom/samsung/android/sconnect/common/installer/InstallApkService;
.super Landroid/app/IntentService;
.source "InstallApkService.java"


# static fields
.field private static final ACTION_INSTALL_APK:Ljava/lang/String; = "com.samsung.android.sconnect.action.INSTALL_APK"

.field private static final EXTRA_APK_FILE_NAME:Ljava/lang/String; = "com.samsung.android.sconnect.extra.APK_FILE_NAME"

.field private static final TAG:Ljava/lang/String; = "InstallApkService"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    const-string v0, "InstallApkService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method private handleActionInstallApk(Ljava/lang/String;)V
    .locals 0
    .param p1, "apkFileName"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->installApk(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method private installApk(Ljava/lang/String;)V
    .locals 12
    .param p1, "apkFileName"    # Ljava/lang/String;

    .prologue
    .line 79
    const-string v8, "InstallApkService"

    const-string v9, "installApk"

    invoke-static {v8, v9, p1}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const/4 v5, 0x0

    .line 81
    .local v5, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 83
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v8

    invoke-virtual {v8, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 84
    const/4 v8, 0x1

    invoke-virtual {p0, p1, v8}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v4

    .line 86
    const/16 v8, 0x400

    new-array v1, v8, [B

    .line 87
    .local v1, "buffer":[B
    const/4 v2, 0x0

    .line 88
    .local v2, "byteRead":I
    :goto_0
    invoke-virtual {v5, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v8, -0x1

    if-eq v2, v8, :cond_2

    .line 89
    const/4 v8, 0x0

    invoke-virtual {v4, v1, v8, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 91
    .end local v1    # "buffer":[B
    .end local v2    # "byteRead":I
    :catch_0
    move-exception v3

    .line 92
    .local v3, "e":Ljava/io/FileNotFoundException;
    :try_start_1
    const-string v8, "InstallApkService"

    const-string v9, "installApk"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "FileNotFoundException : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    if-eqz v4, :cond_0

    .line 99
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 104
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :cond_0
    :goto_1
    if-eqz v5, :cond_1

    .line 106
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    .line 125
    :cond_1
    :goto_2
    return-void

    .line 97
    .restart local v1    # "buffer":[B
    .restart local v2    # "byteRead":I
    :cond_2
    if-eqz v4, :cond_3

    .line 99
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 104
    :cond_3
    :goto_3
    if-eqz v5, :cond_4

    .line 106
    :try_start_5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 113
    .end local v1    # "buffer":[B
    .end local v2    # "byteRead":I
    :cond_4
    :goto_4
    invoke-virtual {p0, p1}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 114
    .local v0, "apk":Ljava/io/File;
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    .line 115
    .local v7, "uri":Landroid/net/Uri;
    new-instance v6, Lcom/samsung/android/sconnect/common/installer/InstallApkService$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/sconnect/common/installer/InstallApkService$1;-><init>(Lcom/samsung/android/sconnect/common/installer/InstallApkService;)V

    .line 122
    .local v6, "packageInstallObserver":Landroid/content/pm/IPackageInstallObserver;
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v9, 0x82

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v7, v6, v9, v10}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    goto :goto_2

    .line 100
    .end local v0    # "apk":Ljava/io/File;
    .end local v6    # "packageInstallObserver":Landroid/content/pm/IPackageInstallObserver;
    .end local v7    # "uri":Landroid/net/Uri;
    .restart local v1    # "buffer":[B
    .restart local v2    # "byteRead":I
    :catch_1
    move-exception v3

    .line 101
    .local v3, "e":Ljava/io/IOException;
    const-string v8, "InstallApkService"

    const-string v9, "installApk"

    const-string v10, "fos.close() - IOException"

    invoke-static {v8, v9, v10, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_3

    .line 107
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 108
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v8, "InstallApkService"

    const-string v9, "installApk"

    const-string v10, "is.close() - IOException"

    invoke-static {v8, v9, v10, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 100
    .end local v1    # "buffer":[B
    .end local v2    # "byteRead":I
    .local v3, "e":Ljava/io/FileNotFoundException;
    :catch_3
    move-exception v3

    .line 101
    .local v3, "e":Ljava/io/IOException;
    const-string v8, "InstallApkService"

    const-string v9, "installApk"

    const-string v10, "fos.close() - IOException"

    invoke-static {v8, v9, v10, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 107
    .end local v3    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v3

    .line 108
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v8, "InstallApkService"

    const-string v9, "installApk"

    const-string v10, "is.close() - IOException"

    invoke-static {v8, v9, v10, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 94
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 95
    .restart local v3    # "e":Ljava/io/IOException;
    :try_start_6
    const-string v8, "InstallApkService"

    const-string v9, "installApk"

    const-string v10, "IOException"

    invoke-static {v8, v9, v10, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 97
    if-eqz v4, :cond_5

    .line 99
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    .line 104
    :cond_5
    :goto_5
    if-eqz v5, :cond_4

    .line 106
    :try_start_8
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    goto :goto_4

    .line 107
    :catch_6
    move-exception v3

    .line 108
    const-string v8, "InstallApkService"

    const-string v9, "installApk"

    const-string v10, "is.close() - IOException"

    invoke-static {v8, v9, v10, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 100
    :catch_7
    move-exception v3

    .line 101
    const-string v8, "InstallApkService"

    const-string v9, "installApk"

    const-string v10, "fos.close() - IOException"

    invoke-static {v8, v9, v10, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_5

    .line 97
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    if-eqz v4, :cond_6

    .line 99
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    .line 104
    :cond_6
    :goto_6
    if-eqz v5, :cond_7

    .line 106
    :try_start_a
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    .line 109
    :cond_7
    :goto_7
    throw v8

    .line 100
    :catch_8
    move-exception v3

    .line 101
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v9, "InstallApkService"

    const-string v10, "installApk"

    const-string v11, "fos.close() - IOException"

    invoke-static {v9, v10, v11, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_6

    .line 107
    .end local v3    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v3

    .line 108
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v9, "InstallApkService"

    const-string v10, "installApk"

    const-string v11, "is.close() - IOException"

    invoke-static {v9, v10, v11, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7
.end method

.method public static isBmVersionDifferent(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 39
    .local v2, "qcPackageInfo":Landroid/content/pm/PackageInfo;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.samsung.android.beaconmanager"

    const/4 v5, 0x4

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 41
    .local v0, "bmPackageInfo":Landroid/content/pm/PackageInfo;
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 42
    iget v3, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    iget v4, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v3, v4, :cond_0

    .line 43
    const/4 v3, 0x1

    .line 49
    .end local v0    # "bmPackageInfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "qcPackageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v3

    .line 46
    :catch_0
    move-exception v1

    .line 47
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "InstallApkService"

    const-string v4, "isBmVersionDifferent"

    const-string v5, "NameNotFoundException"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static startActionInstallApk(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "apkFileName"    # Ljava/lang/String;

    .prologue
    .line 53
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/sconnect/common/installer/InstallApkService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 54
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.android.sconnect.action.INSTALL_APK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const-string v1, "com.samsung.android.sconnect.extra.APK_FILE_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 57
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 65
    if-eqz p1, :cond_0

    .line 66
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 67
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.samsung.android.sconnect.action.INSTALL_APK"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 68
    const-string v2, "com.samsung.android.sconnect.extra.APK_FILE_NAME"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "apkFileName":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/samsung/android/sconnect/common/installer/InstallApkService;->handleActionInstallApk(Ljava/lang/String;)V

    .line 72
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "apkFileName":Ljava/lang/String;
    :cond_0
    return-void
.end method
