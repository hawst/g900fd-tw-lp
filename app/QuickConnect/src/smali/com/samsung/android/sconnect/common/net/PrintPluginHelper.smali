.class public Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
.super Ljava/lang/Object;
.source "PrintPluginHelper.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/IDiscoveryAction;


# static fields
.field private static final HANDLE_START_DISCOVERY:I = 0xbb8

.field private static final HANDLE_START_DISCOVERY_AGAIN:I = 0xbba

.field private static final HANDLE_STOP_DISCOVERY:I = 0xbb9

.field private static final TAG:Ljava/lang/String; = "PrintPluginHelper"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDiscoverySession:Landroid/print/PrinterDiscoverySession;

.field private mHandler:Landroid/os/Handler;

.field private mIsP2pDiscovery:Z

.field private mListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mP2pSubnetIP:Ljava/lang/String;

.field private mPluginBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mPrinterList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/print/PrinterInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;

    .line 41
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    .line 43
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mUris:Ljava/util/ArrayList;

    .line 53
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mIsP2pDiscovery:Z

    .line 298
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mP2pSubnetIP:Ljava/lang/String;

    .line 310
    new-instance v0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$2;-><init>(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPluginBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 360
    new-instance v0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$3;-><init>(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mHandler:Landroid/os/Handler;

    .line 58
    const-string v0, "PrintPluginHelper"

    const-string v1, "PrintPluginHelper"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mUris:Ljava/util/ArrayList;

    .line 62
    if-eqz p1, :cond_0

    .line 63
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;

    .line 66
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->setBroadcastReceiver()V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Landroid/print/PrinterDiscoverySession;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->startSearchViaPlugin()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->stopSearchViaPlugin()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->getIPAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mIsP2pDiscovery:Z

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mIsP2pDiscovery:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->isP2pAddress(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mUris:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mP2pSubnetIP:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mP2pSubnetIP:Ljava/lang/String;

    return-object p1
.end method

.method private getIPAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "localId"    # Ljava/lang/String;

    .prologue
    .line 286
    const-string v2, "[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+"

    invoke-static {v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 287
    .local v1, "pattern":Ljava/util/regex/Pattern;
    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 289
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 291
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object p1

    .line 295
    .end local p1    # "localId":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 294
    .restart local p1    # "localId":Ljava/lang/String;
    :cond_0
    const-string v2, "PrintPluginHelper"

    const-string v3, "getIPAddress"

    const-string v4, "IP NOT FOUND"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initPrinterList()V
    .locals 3

    .prologue
    .line 253
    const-string v0, "PrintPluginHelper"

    const-string v1, "initPrinterList"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 256
    const-string v0, "PrintPluginHelper"

    const-string v1, "initPrinterList"

    const-string v2, "create printer list"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    .line 263
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mUris:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mIsP2pDiscovery:Z

    if-nez v0, :cond_0

    .line 264
    const-string v0, "PrintPluginHelper"

    const-string v1, "initPrinterList"

    const-string v2, "init mUris"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mUris:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 267
    :cond_0
    return-void

    .line 259
    :cond_1
    const-string v0, "PrintPluginHelper"

    const-string v1, "initPrinterList"

    const-string v2, "remove all printer list"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method

.method private isP2pAddress(Ljava/lang/String;)Z
    .locals 4
    .param p1, "ip"    # Ljava/lang/String;

    .prologue
    .line 303
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mP2pSubnetIP:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mP2pSubnetIP:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    const-string v0, "PrintPluginHelper"

    const-string v1, "isP2pAddress"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "true: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const/4 v0, 0x1

    .line 307
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setBroadcastReceiver()V
    .locals 5

    .prologue
    .line 351
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPluginBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :goto_0
    return-void

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PrintPluginHelper"

    const-string v2, "setBroadcastReceiver"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 356
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private startSearchViaPlugin()V
    .locals 4

    .prologue
    .line 173
    const-string v1, "PrintPluginHelper"

    const-string v2, "searchPrintersViaPlugin"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->initPrinterList()V

    .line 176
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    if-nez v1, :cond_1

    .line 177
    const-string v1, "PrintPluginHelper"

    const-string v2, "searchPrintersViaPlugin"

    const-string v3, "createPrinterDiscoverySession"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;

    const-string v2, "print"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/print/PrintManager;

    .line 181
    .local v0, "printManager":Landroid/print/PrintManager;
    invoke-virtual {v0}, Landroid/print/PrintManager;->createPrinterDiscoverySession()Landroid/print/PrinterDiscoverySession;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    .line 182
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    new-instance v2, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper$1;-><init>(Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;)V

    invoke-virtual {v1, v2}, Landroid/print/PrinterDiscoverySession;->setOnPrintersChangeListener(Landroid/print/PrinterDiscoverySession$OnPrintersChangeListener;)V

    .line 248
    .end local v0    # "printManager":Landroid/print/PrintManager;
    :cond_0
    :goto_0
    const-string v1, "PrintPluginHelper"

    const-string v2, "searchPrintersViaPlugin"

    const-string v3, "start Current Discovery"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/print/PrinterDiscoverySession;->startPrinterDiscovery(Ljava/util/List;)V

    .line 250
    return-void

    .line 242
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v1}, Landroid/print/PrinterDiscoverySession;->isPrinterDiscoveryStarted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    const-string v1, "PrintPluginHelper"

    const-string v2, "searchPrintersViaPlugin"

    const-string v3, "stop Current Discovery"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v1}, Landroid/print/PrinterDiscoverySession;->stopPrinterDiscovery()V

    goto :goto_0
.end method

.method private stopSearchViaPlugin()V
    .locals 3

    .prologue
    .line 270
    const-string v0, "PrintPluginHelper"

    const-string v1, "stopSearchViaPlugin"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->isPrinterDiscoveryStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    const-string v0, "PrintPluginHelper"

    const-string v1, "stopSearchViaPlugin"

    const-string v2, "stop Current Discovery"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->stopPrinterDiscovery()V

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    invoke-virtual {v0}, Landroid/print/PrinterDiscoverySession;->destroy()V

    .line 278
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mDiscoverySession:Landroid/print/PrinterDiscoverySession;

    .line 283
    :goto_0
    return-void

    .line 280
    :cond_1
    const-string v0, "PrintPluginHelper"

    const-string v1, "stopSearchViaPlugin"

    const-string v2, "mDiscoverySession is already null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public checkEnabledNetwork()Z
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 129
    const-string v7, "PrintPluginHelper"

    const-string v8, "checkEnabledNetwork"

    const-string v9, ""

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;

    const-string v8, "connectivity"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 133
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 134
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    const/4 v4, 0x0

    .line 135
    .local v4, "wifiConnected":Z
    if-eqz v1, :cond_1

    .line 136
    const-string v7, "PrintPluginHelper"

    const-string v8, "checkEnabledNetwork"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "WIFI: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    sget-object v7, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v7, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    sget-object v7, Landroid/net/NetworkInfo$DetailedState;->CAPTIVE_PORTAL_CHECK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_0
    move v4, v6

    .line 142
    :cond_1
    :goto_0
    const/4 v2, 0x0

    .line 143
    .local v2, "p2pConnected":Z
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;

    const-string v8, "wifip2p"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/p2p/WifiP2pManager;

    .line 146
    .local v3, "p2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pManager;->isWifiP2pEnabled()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 147
    invoke-virtual {v3}, Landroid/net/wifi/p2p/WifiP2pManager;->isWifiP2pConnected()Z

    move-result v2

    .line 150
    :cond_2
    const-string v7, "PrintPluginHelper"

    const-string v8, "checkEnabledNetwork"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "WIFI-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", P2P-"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    if-nez v4, :cond_3

    if-eqz v2, :cond_4

    :cond_3
    move v5, v6

    .line 155
    :cond_4
    return v5

    .end local v2    # "p2pConnected":Z
    .end local v3    # "p2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    :cond_5
    move v4, v5

    .line 137
    goto :goto_0
.end method

.method public getDeviceList()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    const-string v4, "PrintPluginHelper"

    const-string v5, "getDeviceList"

    const-string v6, ""

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 106
    const-string v4, "PrintPluginHelper"

    const-string v5, "getDeviceList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "size:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v3, "sconnDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    monitor-enter v5

    .line 110
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 112
    .local v2, "itor":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/print/PrinterInfo;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrinterInfo;

    .line 114
    .local v1, "item":Landroid/print/PrinterInfo;
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 115
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    new-instance v4, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;

    invoke-virtual {v1}, Landroid/print/PrinterInfo;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Landroid/print/PrinterInfo;->getId()Landroid/print/PrinterId;

    move-result-object v7

    invoke-virtual {v7}, Landroid/print/PrinterId;->getLocalId()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->getIPAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;)V

    .line 117
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 119
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v1    # "item":Landroid/print/PrinterInfo;
    .end local v2    # "itor":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/print/PrinterInfo;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v2    # "itor":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/print/PrinterInfo;>;"
    :cond_0
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 124
    .end local v2    # "itor":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/print/PrinterInfo;>;"
    .end local v3    # "sconnDevices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    :goto_1
    return-object v3

    .line 122
    :cond_1
    const-string v4, "PrintPluginHelper"

    const-string v5, "getDeviceList"

    const-string v6, "mPrinterList is null or empty"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getPrinterInfo(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Landroid/print/PrinterInfo;
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    const/4 v1, 0x0

    .line 159
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDevicePlugin()Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;

    move-result-object v0

    .line 160
    .local v0, "pluginDevice":Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;
    if-eqz v0, :cond_1

    .line 161
    const-string v2, "PrintPluginHelper"

    const-string v3, "getPrinterInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "device: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;->mPluginName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;->mPluginIp:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 163
    const-string v2, "PrintPluginHelper"

    const-string v3, "getPrinterInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FAILED: Not in the List:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;->mPluginIp:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :goto_0
    return-object v1

    .line 166
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mPrinterList:Ljava/util/HashMap;

    iget-object v2, v0, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;->mPluginIp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrinterInfo;

    goto :goto_0

    .line 168
    :cond_1
    const-string v2, "PrintPluginHelper"

    const-string v3, "getPrinterInfo"

    const-string v4, "pluginDevice is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initiate()V
    .locals 3

    .prologue
    .line 70
    const-string v0, "PrintPluginHelper"

    const-string v1, "initiate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .prologue
    .line 79
    const-string v0, "PrintPluginHelper"

    const-string v1, "startDiscovery"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mIsP2pDiscovery:Z

    .line 82
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 83
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xbba

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 84
    return-void
.end method

.method public startP2pDiscovery(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 87
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v0, "PrintPluginHelper"

    const-string v1, "startP2pDiscovery"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mUris:Ljava/util/ArrayList;

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mIsP2pDiscovery:Z

    .line 90
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xbb8

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 91
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xbba

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 92
    return-void
.end method

.method public stopDiscovery()V
    .locals 3

    .prologue
    .line 96
    const-string v0, "PrintPluginHelper"

    const-string v1, "stopDiscovery"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xbb9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 99
    return-void
.end method

.method public terminate()V
    .locals 3

    .prologue
    .line 74
    const-string v0, "PrintPluginHelper"

    const-string v1, "terminate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    return-void
.end method
