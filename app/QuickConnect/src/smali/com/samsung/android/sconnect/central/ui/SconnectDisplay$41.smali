.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;
.super Landroid/content/BroadcastReceiver;
.source "SconnectDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 3488
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v5, 0x3e9

    const/4 v2, -0x1

    const/4 v4, 0x1

    .line 3491
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    .line 3492
    .local v6, "action":Ljava/lang/String;
    const-string v0, "com.samsung.android.sconnect.central.DISCONNECT_P2P"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 3493
    const-string v0, "ACT"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 3494
    .local v12, "service":I
    if-ne v12, v2, :cond_1

    .line 3495
    const-string v0, "SconnectDisplay"

    const-string v2, "mLocalReceiver"

    const-string v3, "DISCONNECT_P2P invalid action"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3571
    .end local v12    # "service":I
    :cond_0
    :goto_0
    return-void

    .line 3497
    .restart local v12    # "service":I
    :cond_1
    if-eqz v12, :cond_2

    if-ne v12, v4, :cond_7

    .line 3499
    :cond_2
    const-string v0, "SconnectDisplay"

    const-string v2, "mLocalReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DISCONNECT_P2P continue -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3500
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v0, v2, :cond_3

    .line 3501
    const-string v0, "SconnectDisplay"

    const-string v2, "mLocalReceiver"

    const-string v3, "stop discovery & invoke primary action"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3502
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/CentralManager;->stopDiscoveryDevice()V

    .line 3503
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;

    invoke-direct {v2, p0, v12}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$1;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;I)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 3517
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v0, v2, :cond_0

    .line 3518
    const-string v0, "SconnectDisplay"

    const-string v2, "mLocalReceiver"

    const-string v3, "choice mode is multiple"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3519
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->isContentSelected()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 3521
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3522
    .local v1, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 3523
    .local v11, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v11}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v7

    .line 3524
    .local v7, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 3535
    .end local v1    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    .end local v7    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :catch_0
    move-exception v8

    .line 3536
    .local v8, "e":Ljava/lang/Exception;
    const-string v0, "SconnectDisplay"

    const-string v2, "mLocalReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MULTIPLE_CHOICE_MODE Runnable Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3539
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 3526
    .restart local v1    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    .restart local v10    # "i$":Ljava/util/Iterator;
    :cond_5
    :try_start_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 3527
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 3528
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 3529
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v2, 0x0

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->setP2pConnected(Z)V
    invoke-static {v0, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$6100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)V

    .line 3530
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSharableContentsList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$6000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mMimeType:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContentsType:I
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Ljava/util/ArrayList;ILjava/util/ArrayList;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 3541
    .end local v1    # "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    .end local v10    # "i$":Ljava/util/Iterator;
    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v0, v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1102(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Lcom/samsung/android/sconnect/central/ui/DisplayItem;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 3542
    new-instance v9, Landroid/content/Intent;

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const-class v2, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelector;

    invoke-direct {v9, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 3544
    .local v9, "fileSharePickerIntent":Landroid/content/Intent;
    const-string v0, "request_code"

    invoke-virtual {v9, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 3545
    const/high16 v0, 0x24000000

    invoke-virtual {v9, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 3547
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0, v9, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 3550
    .end local v9    # "fileSharePickerIntent":Landroid/content/Intent;
    :cond_7
    const/16 v0, 0xd

    if-ne v12, v0, :cond_8

    .line 3551
    const-string v0, "SconnectDisplay"

    const-string v2, "mLocalReceiver"

    const-string v3, "DISCONNECT_P2P ACTION_PRINT "

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3552
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForDisconnect:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5502(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    goto/16 :goto_0

    .line 3554
    :cond_8
    const-string v0, "SconnectDisplay"

    const-string v2, "mLocalReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DISCONNECT_P2P continue -"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3555
    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$2;

    invoke-direct {v2, p0, v12}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41$2;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;I)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 3567
    .end local v12    # "service":I
    :cond_9
    const-string v0, "com.samsung.android.sconnect.central.PRINTER_FINISHED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3568
    const-string v0, "SconnectDisplay"

    const-string v2, "mLocalReceiver"

    const-string v3, "ACTION_C_PRINTER_FINISHED"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 3569
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$41;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForPrinting:Z
    invoke-static {v0, v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$6202(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    goto/16 :goto_0
.end method
