.class public Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;
.super Ljava/lang/Object;
.source "MimeTypeExtractor.java"


# static fields
.field public static final ASK_TO_EMEET:J = 0x20000L

.field public static final BASIC_ACTIONS:J = 0x20301L

.field public static final CHROMECAST:J = 0x80000L

.field public static final CONNECT:J = 0x1000L

.field public static final EXPLORE_CONTENTS:J = 0x80L

.field public static final JOIN_TO_GROUP_PLAY:J = 0x4000L

.field public static final MIRROR_SCREEN:J = 0x20L

.field public static final MORE_FEATURES:J = 0x8L

.field public static final P2P_PAIRING:J = 0x8000L

.field public static final PLAY_CONTENT:J = 0x400L

.field public static final PRINT:J = 0x2000L

.field public static final PRINT_OTHERS:J = 0x100000L

.field public static final PROFILE_SHARE:J = 0x40000L

.field public static final SEND_CURRENT_CONTENTS:J = 0x1L

.field public static final SEND_OTHER_CONTENTS:J = 0x2L

.field public static final SHARE_VIA_GROUP_PLAY:J = 0x4L

.field public static final SIDESYNC_CONTROL:J = 0x10000L

.field public static final TOGETHER_CREATE_ROOM:J = 0x100L

.field public static final TOGETHER_JOIN_ROOM:J = 0x200L

.field public static final TV:J = 0x800L

.field private static mContentsType:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static mMimeAction:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static mMimeType:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/16 v7, 0x11

    const/4 v2, 0x2

    const/4 v6, 0x3

    const-wide/32 v4, 0x84404

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeType:Ljava/util/HashMap;

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mContentsType:Ljava/util/HashMap;

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeAction:Ljava/util/HashMap;

    .line 58
    const-string v0, "EML"

    const-string v1, "message/rfc822"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    const-string v0, "MP3"

    const-string v1, "audio/mpeg"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 61
    const-string v0, "M4A"

    const-string v1, "audio/mp4"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 63
    const-string v0, "WAV"

    const-string v1, "audio/x-wav"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 65
    const-string v0, "AMR"

    const-string v1, "audio/amr"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 67
    const-string v0, "AWB"

    const-string v1, "audio/amr-wb"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 69
    const-string v0, "WMA"

    const-string v1, "audio/x-ms-wma"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 71
    const-string v0, "OGG"

    const-string v1, "audio/ogg"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 73
    const-string v0, "OGA"

    const-string v1, "audio/ogg"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 75
    const-string v0, "AAC"

    const-string v1, "audio/aac"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 77
    const-string v0, "3GA"

    const-string v1, "audio/3gpp"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 79
    const-string v0, "FLAC"

    const-string v1, "audio/flac"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 81
    const-string v0, "MPGA"

    const-string v1, "audio/mpeg"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 83
    const-string v0, "MP4_A"

    const-string v1, "audio/mp4"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 85
    const-string v0, "3GP_A"

    const-string v1, "audio/3gpp"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 87
    const-string v0, "3G2_A"

    const-string v1, "audio/3gpp2"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 89
    const-string v0, "ASF_A"

    const-string v1, "audio/x-ms-asf"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 91
    const-string v0, "3GPP_A"

    const-string v1, "audio/3gpp"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 93
    const-string v0, "MID"

    const-string v1, "audio/midi"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 95
    const-string v0, "MID_A"

    const-string v1, "audio/mid"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 97
    const-string v0, "XMF"

    const-string v1, "audio/midi"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 99
    const-string v0, "MXMF"

    const-string v1, "audio/midi"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 101
    const-string v0, "RTTTL"

    const-string v1, "audio/midi"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 103
    const-string v0, "SMF"

    const-string v1, "audio/sp-midi"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 105
    const-string v0, "IMY"

    const-string v1, "audio/imelody"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 107
    const-string v0, "MIDI"

    const-string v1, "audio/midi"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 109
    const-string v0, "RTX"

    const-string v1, "audio/midi"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 111
    const-string v0, "OTA"

    const-string v1, "audio/midi"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 113
    const-string v0, "PYA"

    const-string v1, "audio/vnd.ms-playready.media.pya"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 115
    const-string v0, "M4B"

    const-string v1, "audio/mp4"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 117
    const-string v0, "ISMA"

    const-string v1, "audio/isma"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 119
    const-string v0, "QCP"

    const-string v1, "audio/qcelp"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 121
    const-string v0, "MPEG"

    const-string v1, "video/mpeg"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 123
    const-string v0, "MPG"

    const-string v1, "video/mpeg"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 125
    const-string v0, "MP4"

    const-string v1, "video/mp4"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 127
    const-string v0, "M4V"

    const-string v1, "video/mp4"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 129
    const-string v0, "3GP"

    const-string v1, "video/3gpp"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 131
    const-string v0, "3GPP"

    const-string v1, "video/3gpp"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 133
    const-string v0, "3G2"

    const-string v1, "video/3gpp2"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 135
    const-string v0, "3GPP2"

    const-string v1, "video/3gpp2"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 137
    const-string v0, "WMV"

    const-string v1, "video/x-ms-wmv"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 139
    const-string v0, "ASF"

    const-string v1, "video/x-ms-asf"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 141
    const-string v0, "AVI"

    const-string v1, "video/avi"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 143
    const-string v0, "DIVX"

    const-string v1, "video/divx"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 145
    const-string v0, "FLV"

    const-string v1, "video/flv"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 147
    const-string v0, "MKV"

    const-string v1, "video/mkv"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 149
    const-string v0, "SDP"

    const-string v1, "application/sdp"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v0, "RM"

    const-string v1, "video/mp4"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 152
    const-string v0, "RMVB"

    const-string v1, "video/mp4"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 154
    const-string v0, "MOV"

    const-string v1, "video/quicktime"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 156
    const-string v0, "PYV"

    const-string v1, "video/vnd.ms-playready.media.pyv"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 158
    const-string v0, "ISMV"

    const-string v1, "video/ismv"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 160
    const-string v0, "SKM"

    const-string v1, "video/skm"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 162
    const-string v0, "K3G"

    const-string v1, "video/k3g"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 164
    const-string v0, "AK3G"

    const-string v1, "video/ak3g"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 166
    const-string v0, "WEBM"

    const-string v1, "video/webm"

    invoke-static {v0, v1, v2, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 168
    const-string v0, "JPG"

    const-string v1, "image/jpeg"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 170
    const-string v0, "JPEG"

    const-string v1, "image/jpeg"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 172
    const-string v0, "MY5"

    const-string v1, "image/vnd.tmo.my5"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 174
    const-string v0, "GIF"

    const-string v1, "image/gif"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 176
    const-string v0, "PNG"

    const-string v1, "image/png"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 178
    const-string v0, "BMP"

    const-string v1, "image/x-ms-bmp"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 180
    const-string v0, "WBMP"

    const-string v1, "image/vnd.wap.wbmp"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 182
    const-string v0, "GOLF"

    const-string v1, "image/golf"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 184
    const-string v0, "SRW"

    const-string v1, "image/srw"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 186
    const-string v0, "MPO"

    const-string v1, "image/mpo"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 188
    const-string v0, "M3U"

    const-string v1, "audio/x-mpegurl"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 190
    const-string v0, "PLS"

    const-string v1, "audio/x-scpls"

    invoke-static {v0, v1, v6, v4, v5}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 192
    const-string v0, "WPL"

    const-string v1, "application/vnd.ms-wpl"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v0, "PDF"

    const-string v1, "application/pdf"

    const-wide/16 v2, 0x2000

    invoke-static {v0, v1, v7, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 194
    const-string v0, "RTF"

    const-string v1, "application/msword"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 195
    const-string v0, "DOC"

    const-string v1, "application/msword"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 196
    const-string v0, "DOCX"

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 199
    const-string v0, "DOT"

    const-string v1, "application/msword"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 200
    const-string v0, "DOTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.wordprocessingml.template"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 203
    const-string v0, "CSV"

    const-string v1, "text/comma-separated-values"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 204
    const-string v0, "XLS"

    const-string v1, "application/vnd.ms-excel"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 205
    const-string v0, "XLSX"

    const-string v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 207
    const-string v0, "XLT"

    const-string v1, "application/vnd.ms-excel"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 208
    const-string v0, "XLTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.spreadsheetml.template"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 211
    const-string v0, "PPS"

    const-string v1, "application/vnd.ms-powerpoint"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 212
    const-string v0, "PPT"

    const-string v1, "application/vnd.ms-powerpoint"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 213
    const-string v0, "PPTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.presentationml.presentation"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 216
    const-string v0, "POT"

    const-string v1, "application/vnd.ms-powerpoint"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 217
    const-string v0, "POTX"

    const-string v1, "application/vnd.openxmlformats-officedocument.presentationml.template"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 220
    const-string v0, "PPSX"

    const-string v1, "application/vnd.openxmlformats-officedocument.presentationml.slideshow"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 223
    const-string v0, "ASC"

    const-string v1, "text/plain"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 224
    const-string v0, "TXT"

    const-string v1, "text/plain"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 225
    const-string v0, "GUL"

    const-string v1, "application/jungumword"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 226
    const-string v0, "EPUB"

    const-string v1, "application/epub+zip"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    const-string v0, "ACSM"

    const-string v1, "application/vnd.adobe.adept+xml"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string v0, "SWF"

    const-string v1, "application/x-shockwave-flash"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const-string v0, "SVG"

    const-string v1, "image/svg+xml"

    const-wide/32 v2, 0x86404

    invoke-static {v0, v1, v8, v2, v3}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V

    .line 231
    const-string v0, "DCF"

    const-string v1, "application/vnd.oma.drm.content"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const-string v0, "ODF"

    const-string v1, "application/vnd.oma.drm.content"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const-string v0, "APK"

    const-string v1, "application/apk"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const-string v0, "JAD"

    const-string v1, "text/vnd.sun.j2me.app-descriptor"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    const-string v0, "JAR"

    const-string v1, "application/java-archive"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    const-string v0, "VCS"

    const-string v1, "text/x-vCalendar"

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 237
    const-string v0, "ICS"

    const-string v1, "text/calendar"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v0, "VTS"

    const-string v1, "text/x-vtodo"

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 239
    const-string v0, "VCF"

    const-string v1, "text/x-vcard"

    const/16 v2, 0x8

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 240
    const-string v0, "VNT"

    const-string v1, "text/x-vnote"

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 241
    const-string v0, "HTML"

    const-string v1, "text/html"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    const-string v0, "HTM"

    const-string v1, "text/html"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v0, "XHTML"

    const-string v1, "text/html"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    const-string v0, "XML"

    const-string v1, "application/xhtml+xml"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const-string v0, "WGT"

    const-string v1, "application/vnd.samsung.widget"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v0, "HWP"

    const-string v1, "application/x-hwp"

    invoke-static {v0, v1, v7}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 247
    const-string v0, "SNB"

    const-string v1, "application/snb"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const-string v0, "SASF"

    const-string v1, "application/x-sasf"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v0, "ZIP"

    const-string v1, "application/x-zip-compressed"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, "SSF"

    const-string v1, "application/ssf"

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string v0, "SPD"

    const-string v1, "application/spd"

    const/16 v2, 0xb

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 252
    const-string v0, "MEMO"

    const-string v1, "application/vnd.samsung.android.memo"

    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->addContentType(Ljava/lang/String;Ljava/lang/String;I)V

    .line 254
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static addContentType(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "extension"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeType:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mContentsType:Ljava/util/HashMap;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeAction:Ljava/util/HashMap;

    const-wide/32 v2, 0x20301

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    return-void
.end method

.method static addContentType(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4
    .param p0, "extension"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "contentsType"    # I

    .prologue
    .line 46
    sget-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeType:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mContentsType:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeAction:Ljava/util/HashMap;

    const-wide/32 v2, 0x20301

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    return-void
.end method

.method static addContentType(Ljava/lang/String;Ljava/lang/String;IJ)V
    .locals 5
    .param p0, "extension"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "contentsType"    # I
    .param p3, "action"    # J

    .prologue
    .line 40
    sget-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeType:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mContentsType:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeAction:Ljava/util/HashMap;

    const-wide/32 v2, 0x20301

    or-long/2addr v2, p3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method

.method public static getContentsType(Ljava/lang/String;)I
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 321
    const-string v0, "null"

    invoke-static {p0, v0}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getContentsType(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0xd

    .line 284
    if-nez p1, :cond_0

    .line 285
    const-string p1, "null"

    .line 287
    :cond_0
    const-string v2, "content"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 288
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "audio"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 289
    :cond_1
    const/4 v2, 0x3

    .line 315
    :goto_0
    return v2

    .line 290
    :cond_2
    const-string v2, "video"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "video"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 291
    :cond_3
    const/4 v2, 0x2

    goto :goto_0

    .line 292
    :cond_4
    const-string v2, "image"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "image"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 293
    :cond_5
    const/4 v2, 0x1

    goto :goto_0

    .line 294
    :cond_6
    const-string v2, "com.android.contacts"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 295
    const/16 v2, 0x8

    goto :goto_0

    .line 296
    :cond_7
    const-string v2, "media/external/file/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v3

    .line 297
    goto :goto_0

    .line 299
    :cond_8
    const/4 v2, 0x0

    goto :goto_0

    .line 300
    :cond_9
    const-string v2, "http://maps.google.com/maps?"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 301
    const/4 v2, 0x7

    goto :goto_0

    .line 302
    :cond_a
    const-string v2, "http"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_b

    const-string v2, "text://http"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 303
    :cond_b
    const/16 v2, 0x9

    goto :goto_0

    .line 304
    :cond_c
    const-string v2, "text://"

    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 305
    const/16 v2, 0x11

    goto :goto_0

    .line 308
    :cond_d
    invoke-static {p0}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 309
    .local v1, "ext":Ljava/lang/String;
    if-nez v1, :cond_e

    move v2, v3

    .line 310
    goto :goto_0

    .line 313
    :cond_e
    :try_start_0
    sget-object v2, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mContentsType:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 314
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NullPointerException;
    move v2, v3

    .line 315
    goto :goto_0
.end method

.method private static getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 272
    if-nez p0, :cond_1

    .line 279
    :cond_0
    :goto_0
    return-object v1

    .line 275
    :cond_1
    const/16 v2, 0x2e

    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 276
    .local v0, "lastDot":I
    if-ltz v0, :cond_0

    .line 279
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getMimeTypeFromPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 257
    invoke-static {p0}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 258
    .local v1, "ext":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 259
    const/4 v2, 0x0

    .line 268
    :goto_0
    return-object v2

    .line 261
    :cond_0
    const/4 v2, 0x0

    .line 263
    .local v2, "mimetype":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeType:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "mimetype":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v2    # "mimetype":Ljava/lang/String;
    goto :goto_0

    .line 264
    .end local v2    # "mimetype":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 266
    const/4 v2, 0x0

    .restart local v2    # "mimetype":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getRelatedAction(Ljava/lang/String;)J
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 358
    const-string v0, "null"

    invoke-static {p0, v0}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getRelatedAction(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getRelatedAction(Ljava/lang/String;Ljava/lang/String;)J
    .locals 9
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    const-wide/32 v6, 0xa4705

    const-wide/16 v2, 0x1

    const-wide/32 v4, 0x20301

    .line 325
    if-nez p1, :cond_0

    .line 326
    const-string p1, "null"

    .line 328
    :cond_0
    const-string v8, "content"

    invoke-virtual {p0, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 329
    const-string v8, "audio"

    invoke-virtual {p0, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    const-string v8, "audio"

    invoke-virtual {p1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_1
    move-wide v2, v6

    .line 352
    :cond_2
    :goto_0
    return-wide v2

    .line 332
    :cond_3
    const-string v8, "video"

    invoke-virtual {p0, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "video"

    invoke-virtual {p1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_4
    move-wide v2, v6

    .line 333
    goto :goto_0

    .line 335
    :cond_5
    const-string v6, "image"

    invoke-virtual {p0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_6

    const-string v6, "image"

    invoke-virtual {p1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 336
    :cond_6
    const-wide/32 v2, 0xa6705

    goto :goto_0

    .line 338
    :cond_7
    const-string v6, "com.android.contacts"

    invoke-virtual {p0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    move-wide v2, v4

    .line 341
    goto :goto_0

    .line 342
    :cond_8
    const-string v6, "text://"

    invoke-virtual {p0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "http"

    invoke-virtual {p0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 345
    invoke-static {p0}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 346
    .local v1, "ext":Ljava/lang/String;
    if-nez v1, :cond_9

    move-wide v2, v4

    .line 347
    goto :goto_0

    .line 350
    :cond_9
    :try_start_0
    sget-object v2, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->mMimeAction:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    goto :goto_0

    .line 351
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NullPointerException;
    move-wide v2, v4

    .line 352
    goto :goto_0
.end method
