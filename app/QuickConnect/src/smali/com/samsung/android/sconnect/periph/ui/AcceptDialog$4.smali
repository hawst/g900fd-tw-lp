.class Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$4;
.super Landroid/content/BroadcastReceiver;
.source "AcceptDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$4;->this$0:Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 467
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 468
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 469
    const-string v1, "AcceptDialog"

    const-string v2, "mBroadcastReceiver"

    const-string v3, "ACTION_SCREEN_OFF : auto reject"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    iget-object v1, p0, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog$4;->this$0:Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/periph/ui/AcceptDialog;->finish()V

    .line 472
    :cond_0
    return-void
.end method
