.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$24;
.super Landroid/content/BroadcastReceiver;
.source "SconnectDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 2745
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$24;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2748
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2749
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2750
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    const/high16 v3, -0x80000000

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2752
    .local v1, "state":I
    const/16 v2, 0xa

    if-ne v1, v2, :cond_1

    .line 2753
    const-string v2, "SconnectDisplay"

    const-string v3, "ACTION_STATE_CHANGED"

    const-string v4, "STATE_OFF - redraw again"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2754
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$24;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->startSconnectDisplay()V
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    .line 2763
    .end local v1    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 2755
    .restart local v1    # "state":I
    :cond_1
    const/16 v2, 0xb

    if-ne v1, v2, :cond_2

    .line 2756
    const-string v2, "SconnectDisplay"

    const-string v3, "ACTION_STATE_CHANGED"

    const-string v4, "STATE_TURNING_ON"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2757
    :cond_2
    const/16 v2, 0xc

    if-ne v1, v2, :cond_3

    .line 2758
    const-string v2, "SconnectDisplay"

    const-string v3, "ACTION_STATE_CHANGED"

    const-string v4, "STATE_ON"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2759
    :cond_3
    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    .line 2760
    const-string v2, "SconnectDisplay"

    const-string v3, "ACTION_STATE_CHANGED"

    const-string v4, "STATE_TURNING_OFF"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
