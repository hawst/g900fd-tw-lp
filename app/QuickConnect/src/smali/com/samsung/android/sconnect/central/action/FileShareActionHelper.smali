.class public Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;
.super Ljava/lang/Object;
.source "FileShareActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;
    }
.end annotation


# static fields
.field private static final HANDLE_DELAY_SEND_FILE:I = 0x3e9

.field private static final HANDLE_UPDATE_STATUS:I = 0x3e8

.field private static final STATUS_COMPLETED:Ljava/lang/String; = "STATUS_COMPLETED"

.field private static final STATUS_FAILED:Ljava/lang/String; = "STATUS_FAILED"

.field private static final STATUS_PROGRESS:Ljava/lang/String; = "STATUS_PROGRESS"

.field private static final STATUS_STARTED:Ljava/lang/String; = "STATUS_STARTED"

.field private static final TAG:Ljava/lang/String; = "FileShareHelper"

.field private static final TIME_DELAY_UPDATE_STATUS:I = 0x3e8


# instance fields
.field mCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field mService:Lcom/samsung/android/sconnect/central/action/IFileShareService;

.field private mTransferList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .line 54
    new-instance v0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;-><init>(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    .line 161
    new-instance v0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;-><init>(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mHandler:Landroid/os/Handler;

    .line 265
    new-instance v0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;-><init>(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mConnection:Landroid/content/ServiceConnection;

    .line 43
    const-string v0, "FileShareHelper"

    const-string v1, "FileShareHelper"

    const-string v2, "EXEC"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .line 54
    new-instance v0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$1;-><init>(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mCallback:Lcom/samsung/android/sconnect/central/action/IFileShareServiceCallback;

    .line 161
    new-instance v0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$2;-><init>(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mHandler:Landroid/os/Handler;

    .line 265
    new-instance v0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$3;-><init>(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mConnection:Landroid/content/ServiceConnection;

    .line 47
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->updateCurrentTransferStatus()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->isP2pConnection()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method private isP2pConnection()Z
    .locals 3

    .prologue
    .line 236
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;

    const-string v2, "wifip2p"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    .line 239
    .local v0, "p2pManager":Landroid/net/wifi/p2p/WifiP2pManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pManager;->isWifiP2pEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 240
    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pManager;->isWifiP2pConnected()Z

    move-result v1

    .line 242
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private updateCurrentTransferStatus()V
    .locals 9

    .prologue
    .line 192
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    .line 193
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->isP2pConnection()Z

    move-result v4

    if-nez v4, :cond_1

    .line 194
    const-string v4, "FileShareHelper"

    const-string v5, "updateCurrentTransferStatus"

    const-string v6, "P2P is unable to use: init mTransferList & mUpdateListener!!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .line 197
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    monitor-enter v5

    .line 200
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 201
    .local v3, "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 202
    .local v2, "key":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;

    .line 203
    .local v1, "item":Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;
    const-string v4, "FileShareHelper"

    const-string v6, "updateCurrentTransferStatus"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "item: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->getDeviceName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;->getMacAddress()Ljava/lang/String;

    move-result-object v7

    const-string v8, "STATUS_PROGRESS"

    invoke-interface {v4, v6, v7, v8}, Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 207
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/samsung/android/sconnect/central/action/FileShareActionHelper$TransferItem;
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v3    # "keys":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public cancelTransfer(Ljava/lang/String;)V
    .locals 6
    .param p1, "mac"    # Ljava/lang/String;

    .prologue
    .line 246
    const-string v1, "FileShareHelper"

    const-string v2, "cancelTransfer"

    invoke-static {v1, v2, p1}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mService:Lcom/samsung/android/sconnect/central/action/IFileShareService;

    if-eqz v1, :cond_0

    .line 249
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mService:Lcom/samsung/android/sconnect/central/action/IFileShareService;

    invoke-interface {v1, p1}, Lcom/samsung/android/sconnect/central/action/IFileShareService;->cancelTransfer(Ljava/lang/String;)Z

    .line 251
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    monitor-enter v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 253
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mTransferList:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    :goto_0
    monitor-exit v2

    .line 262
    :cond_0
    :goto_1
    return-void

    .line 255
    :cond_1
    const-string v1, "FileShareHelper"

    const-string v3, "cancelTransfer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No matched mac in mTransferList: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 257
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public initiate()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public sendFile(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 213
    const-string v1, "FileShareHelper"

    const-string v2, "sendFile"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 218
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->isP2pConnection()Z

    move-result v1

    if-nez v1, :cond_1

    .line 219
    const-string v1, "FileShareHelper"

    const-string v2, "sendFile"

    const-string v3, "p2p is not connected. try it later"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 221
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x3e9

    iput v1, v0, Landroid/os/Message;->what:I

    .line 222
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 223
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 232
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    :goto_0
    return-void

    .line 227
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 228
    const-string v1, "FileShareHelper"

    const-string v2, "sendFile"

    const-string v3, "bindService"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method public setUpdateListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .prologue
    .line 153
    const-string v0, "FileShareHelper"

    const-string v1, "setUpdateListener"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mUpdateListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .line 156
    if-eqz p1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/FileShareActionHelper;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x3e8

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 159
    :cond_0
    return-void
.end method
