.class Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;
.super Ljava/lang/Object;
.source "SelectAutonaviMapActivity.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 6
    .param p1, "location"    # Landroid/location/Location;

    .prologue
    const/4 v5, 0x1

    .line 480
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, ""

    const-string v3, "New location found"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    if-eqz p1, :cond_0

    .line 483
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    .line 484
    .local v0, "currAccuracy":F
    const-string v1, "SelectAutonaviMapActivity"

    const-string v2, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onLocationChanged (N"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/O"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mPreAccuracy:F
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$700(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mPreAccuracy:F
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$700(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)F

    move-result v1

    const v2, 0x4479c000    # 999.0f

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    iget-boolean v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTaskEnabled:Z

    if-ne v1, v5, :cond_0

    .line 488
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mPreAccuracy:F
    invoke-static {v1, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$702(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;F)F

    .line 490
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLatE6:D

    .line 491
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mWgsLngE6:D

    .line 493
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->RemoveListener(Z)V
    invoke-static {v1, v5}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$800(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;Z)V

    .line 494
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$500(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Ljava/util/Timer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 495
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity$7;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->mTimer:Ljava/util/Timer;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;->access$500(Lcom/samsung/android/sconnect/central/ui/picker/SelectAutonaviMapActivity;)Ljava/util/Timer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    .line 499
    .end local v0    # "currAccuracy":F
    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 508
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 505
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 502
    return-void
.end method
