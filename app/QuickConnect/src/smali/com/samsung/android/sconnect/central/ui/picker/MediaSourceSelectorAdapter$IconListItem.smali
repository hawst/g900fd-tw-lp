.class Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;
.super Ljava/lang/Object;
.source "MediaSourceSelector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "IconListItem"
.end annotation


# instance fields
.field private mCommand:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

.field private final mIcon:Landroid/graphics/drawable/Drawable;

.field private final mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "appIcon"    # Landroid/graphics/drawable/Drawable;
    .param p3, "command"    # Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    .prologue
    .line 467
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 468
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->mIcon:Landroid/graphics/drawable/Drawable;

    .line 469
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->mTitle:Ljava/lang/String;

    .line 470
    iput-object p3, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->mCommand:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    .line 471
    return-void
.end method


# virtual methods
.method public getCommand()Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->mCommand:Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$ActionType;

    return-object v0
.end method

.method public getResource()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->mIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 474
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/MediaSourceSelectorAdapter$IconListItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method
