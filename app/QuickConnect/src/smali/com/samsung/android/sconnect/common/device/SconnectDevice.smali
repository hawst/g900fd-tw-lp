.class public Lcom/samsung/android/sconnect/common/device/SconnectDevice;
.super Ljava/lang/Object;
.source "SconnectDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation
.end field

.field static final TAG:Ljava/lang/String; = "SconnectDevice"


# instance fields
.field protected mBtRSSI:S

.field protected mContactName:Ljava/lang/String;

.field protected mContactUri:Landroid/net/Uri;

.field protected mContext:Landroid/content/Context;

.field protected mDeviceNetType:I

.field protected mFavoriteImage:Landroid/graphics/Bitmap;

.field protected mFavoriteName:Ljava/lang/String;

.field protected mHasContactInfo:Z

.field protected mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

.field protected mImage:Landroid/graphics/Bitmap;

.field protected mImageType:I

.field protected mIsFavorite:Z

.field protected mIsMyGroupDevice:Z

.field protected mIsPluginDevice:Z

.field protected mIsSearched:Z

.field protected mName:Ljava/lang/String;

.field protected mPhoneNumber:Ljava/lang/String;

.field protected mSconnectDeviceType:I

.field protected mServices:I

.field protected mSlinkId:J

.field protected mUpnpDeviceType:I

.field protected mUpnpNIC:I

.field protected mWearableData:[B

.field protected mWearableInfo:Ljava/lang/String;

.field public mWearableType:I

.field protected mWorkingServices:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 920
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$1;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    .line 42
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 43
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    .line 44
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    .line 45
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    .line 46
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mHasContactInfo:Z

    .line 47
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    .line 48
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    .line 49
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 50
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    .line 51
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSlinkId:J

    .line 54
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    .line 55
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    .line 56
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableType:I

    .line 57
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableInfo:Ljava/lang/String;

    .line 58
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableData:[B

    .line 59
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    .line 60
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteImage:Landroid/graphics/Bitmap;

    .line 61
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactUri:Landroid/net/Uri;

    .line 62
    const/16 v0, -0x12c

    iput-short v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    .line 63
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    .line 65
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsPluginDevice:Z

    .line 67
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsFavorite:Z

    .line 68
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImageType:I

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    .line 42
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 43
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    .line 44
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    .line 45
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    .line 46
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mHasContactInfo:Z

    .line 47
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    .line 48
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    .line 49
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 50
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    .line 51
    new-instance v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSlinkId:J

    .line 54
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    .line 55
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    .line 56
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableType:I

    .line 57
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableInfo:Ljava/lang/String;

    .line 58
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableData:[B

    .line 59
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    .line 60
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteImage:Landroid/graphics/Bitmap;

    .line 61
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactUri:Landroid/net/Uri;

    .line 62
    const/16 v0, -0x12c

    iput-short v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    .line 63
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    .line 65
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsPluginDevice:Z

    .line 67
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsFavorite:Z

    .line 68
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImageType:I

    .line 76
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uuid"    # Ljava/lang/String;
    .param p3, "p2pMac"    # Ljava/lang/String;
    .param p4, "wifiMac"    # Ljava/lang/String;
    .param p5, "btMac"    # Ljava/lang/String;
    .param p6, "bleMac"    # Ljava/lang/String;
    .param p7, "name"    # Ljava/lang/String;
    .param p8, "imagePath"    # Ljava/lang/String;
    .param p9, "type"    # I

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    .line 42
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 43
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    .line 44
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    .line 45
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    .line 46
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mHasContactInfo:Z

    .line 47
    iput v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    .line 48
    iput v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    .line 49
    iput v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 50
    iput v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    .line 51
    new-instance v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;-><init>(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    .line 53
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSlinkId:J

    .line 54
    iput v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    .line 55
    iput v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    .line 56
    iput v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableType:I

    .line 57
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableInfo:Ljava/lang/String;

    .line 58
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableData:[B

    .line 59
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    .line 60
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteImage:Landroid/graphics/Bitmap;

    .line 61
    iput-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactUri:Landroid/net/Uri;

    .line 62
    const/16 v1, -0x12c

    iput-short v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    .line 63
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    .line 65
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsPluginDevice:Z

    .line 67
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsFavorite:Z

    .line 68
    iput-boolean v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    .line 69
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImageType:I

    .line 90
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    .line 91
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iput-object p2, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    .line 92
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iput-object p3, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    .line 93
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iput-object p4, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    .line 94
    invoke-virtual {p0, p5}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setBtMac(Ljava/lang/String;)V

    .line 95
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iput-object p6, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    .line 96
    iput-object p7, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    .line 97
    iput p9, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 99
    :try_start_0
    invoke-static {p8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImageType:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v1, "SconnectDevice"

    const-string v2, "SconnectDevice "

    const-string v3, "NumberFormatException, Favorite Device has local icon."

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-direct {p0, p8}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->retrieveImage(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private addDeviceNetType(I)V
    .locals 1
    .param p1, "deviceNetType"    # I

    .prologue
    .line 480
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    .line 481
    return-void
.end method

.method private retrieveImage(Ljava/lang/String;)V
    .locals 3
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 800
    if-eqz p1, :cond_0

    .line 801
    invoke-static {p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteImage:Landroid/graphics/Bitmap;

    .line 802
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    const v1, 0x7f070021

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteImage:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->cropIcon(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    .line 807
    :goto_0
    return-void

    .line 804
    :cond_0
    const-string v0, "SconnectDevice"

    const-string v1, "retrieveImage"

    const-string v2, "image path is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateDeviceType(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    const/16 v5, 0xe

    const/4 v4, 0x4

    .line 522
    const-string v0, "SconnectDevice"

    const-string v1, "updateDeviceType "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSconnectDeviceType - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", device.mDeviceNetType - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    if-ne v0, v5, :cond_3

    .line 526
    iput v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 544
    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_2

    .line 545
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-eq v0, v4, :cond_1

    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 547
    :cond_1
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 551
    :cond_2
    const-string v0, "SconnectDevice"

    const-string v1, "updateDeviceType "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSconnectDeviceType - after : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    return-void

    .line 527
    :cond_3
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    const/4 v1, 0x1

    if-lt v0, v1, :cond_4

    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_4

    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_5

    .line 530
    :cond_4
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    goto :goto_0

    .line 531
    :cond_5
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v0, v0, 0x69

    if-nez v0, :cond_0

    .line 533
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v0, v0, 0x71

    if-eqz v0, :cond_7

    .line 535
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_6

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceType()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    .line 536
    :cond_6
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    goto :goto_0

    .line 538
    :cond_7
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-ne v0, v4, :cond_0

    .line 540
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    goto :goto_0
.end method

.method private updateMacFromIP()V
    .locals 12

    .prologue
    .line 555
    iget-object v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v8, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    if-nez v8, :cond_1

    .line 556
    const-string v8, "SconnectDevice"

    const-string v9, "updateMacFromIP "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": IP is null!  failed to udpate mac"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 558
    :cond_1
    const/4 v0, 0x0

    .line 559
    .local v0, "br":Ljava/io/BufferedReader;
    iget-object v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v8, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    const-string v9, "/"

    const-string v10, ""

    invoke-virtual {v8, v9, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 562
    .local v3, "ipAddr":Ljava/lang/String;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/FileReader;

    const-string v9, "/proc/net/arp"

    invoke-direct {v8, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 563
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    const/4 v4, 0x0

    .line 566
    .local v4, "line":Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 567
    if-nez v4, :cond_3

    .line 600
    if-eqz v1, :cond_0

    .line 602
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 603
    :catch_0
    move-exception v2

    .line 604
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "SconnectDevice"

    const-string v9, "updateMacFromIP br.close() IOE"

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 570
    .end local v2    # "e":Ljava/io/IOException;
    :cond_3
    :try_start_3
    const-string v8, " +"

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 571
    .local v7, "splitted":[Ljava/lang/String;
    if-eqz v7, :cond_2

    array-length v8, v7

    const/4 v9, 0x6

    if-lt v8, v9, :cond_2

    const/4 v8, 0x0

    aget-object v8, v7, v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 572
    const/4 v8, 0x3

    aget-object v5, v7, v8

    .line 573
    .local v5, "mac":Ljava/lang/String;
    const/4 v8, 0x5

    aget-object v6, v7, v8

    .line 574
    .local v6, "macInterface":Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/Util;->isMac(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 575
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 576
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 577
    :cond_4
    const-string v8, "SconnectDevice"

    const-string v9, "updateMacFromIP "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": Interface is empty! failed to udpate mac"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 597
    .end local v5    # "mac":Ljava/lang/String;
    .end local v6    # "macInterface":Ljava/lang/String;
    .end local v7    # "splitted":[Ljava/lang/String;
    :catch_1
    move-exception v2

    move-object v0, v1

    .line 598
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    .local v2, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    const-string v8, "SconnectDevice"

    const-string v9, "updateMacFromIP Exception"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 600
    if-eqz v0, :cond_0

    .line 602
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 603
    :catch_2
    move-exception v2

    .line 604
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "SconnectDevice"

    const-string v9, "updateMacFromIP br.close() IOE"

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 579
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "mac":Ljava/lang/String;
    .restart local v6    # "macInterface":Ljava/lang/String;
    .restart local v7    # "splitted":[Ljava/lang/String;
    :cond_5
    :try_start_6
    const-string v8, "p2p-wlan0-0"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v8, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v8, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 581
    :cond_6
    iget v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    const/4 v9, 0x1

    if-eq v8, v9, :cond_7

    iget v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_8

    .line 583
    :cond_7
    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/NetUtil;->getP2pMacFromConnectedMac(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 585
    :cond_8
    iget-object v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iput-object v5, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    .line 586
    const-string v8, "SconnectDevice"

    const-string v9, "updateMacFromIP "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": p2p mac updated: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_1

    .line 600
    .end local v5    # "mac":Ljava/lang/String;
    .end local v6    # "macInterface":Ljava/lang/String;
    .end local v7    # "splitted":[Ljava/lang/String;
    :catchall_0
    move-exception v8

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_3
    if-eqz v0, :cond_9

    .line 602
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    .line 607
    :cond_9
    :goto_4
    throw v8

    .line 588
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v4    # "line":Ljava/lang/String;
    .restart local v5    # "mac":Ljava/lang/String;
    .restart local v6    # "macInterface":Ljava/lang/String;
    .restart local v7    # "splitted":[Ljava/lang/String;
    :cond_a
    :try_start_8
    const-string v8, "wlan0"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v8, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    if-eqz v8, :cond_b

    iget-object v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v8, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 590
    :cond_b
    iget-object v8, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iput-object v5, v8, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    .line 591
    const-string v8, "SconnectDevice"

    const-string v9, "updateMacFromIP "

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": wifi mac updated: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    .line 605
    .end local v5    # "mac":Ljava/lang/String;
    .end local v6    # "macInterface":Ljava/lang/String;
    .end local v7    # "splitted":[Ljava/lang/String;
    :catch_3
    move-exception v2

    .line 606
    .local v2, "e":Ljava/lang/Exception;
    const-string v8, "SconnectDevice"

    const-string v9, "updateMacFromIP br.close() E"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 605
    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v4    # "line":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :catch_4
    move-exception v2

    .line 606
    const-string v8, "SconnectDevice"

    const-string v9, "updateMacFromIP br.close() E"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 603
    .end local v2    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v2

    .line 604
    .local v2, "e":Ljava/io/IOException;
    const-string v9, "SconnectDevice"

    const-string v10, "updateMacFromIP br.close() IOE"

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 605
    .end local v2    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v2

    .line 606
    .local v2, "e":Ljava/lang/Exception;
    const-string v9, "SconnectDevice"

    const-string v10, "updateMacFromIP br.close() E"

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 600
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v8

    goto :goto_3

    .line 597
    :catch_7
    move-exception v2

    goto/16 :goto_2
.end method


# virtual methods
.method public addState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 861
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    .line 862
    return-void
.end method

.method public contains(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    const/4 v0, 0x0

    .line 765
    if-nez p1, :cond_1

    .line 771
    :cond_0
    :goto_0
    return v0

    .line 768
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v2, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 769
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 903
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 870
    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .line 871
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    if-nez v2, :cond_1

    .line 885
    :cond_0
    :goto_0
    return v1

    .line 874
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 875
    iget v2, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isMyGroupDevice()Z

    move-result v2

    iget-boolean v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    if-ne v2, v3, :cond_0

    .line 879
    :cond_2
    iget v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    const/16 v3, 0x8

    if-eq v2, v3, :cond_3

    iget v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    iget v3, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-ne v2, v3, :cond_0

    .line 881
    :cond_3
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getBtRssi()S
    .locals 1

    .prologue
    .line 492
    iget-short v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    return v0
.end method

.method public getContactImage()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 841
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->getContactImage(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getContactName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 655
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    return-object v0
.end method

.method public getContactUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactUri:Landroid/net/Uri;

    return-object v0
.end method

.method public getDeviceBle()Lcom/samsung/android/sconnect/common/device/net/DeviceBle;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 675
    iget v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v1, v1, 0x8

    if-gez v1, :cond_1

    .line 685
    :cond_0
    :goto_0
    return-object v0

    .line 679
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 682
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 685
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v3, v3, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    iget v6, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    iget v7, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public getDeviceBt()Lcom/samsung/android/sconnect/common/device/net/DeviceBt;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 690
    iget v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v1, v1, 0x4

    if-gez v1, :cond_1

    .line 696
    :cond_0
    :goto_0
    return-object v0

    .line 693
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 696
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    iget v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    iget-short v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;-><init>(Ljava/lang/String;Ljava/lang/String;IS)V

    goto :goto_0
.end method

.method public getDeviceGroupPlay()Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 722
    iget v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v1, v1, 0x1

    if-gez v1, :cond_1

    .line 728
    :cond_0
    :goto_0
    return-object v0

    .line 725
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 728
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    return-object v0
.end method

.method public getDeviceNetType()I
    .locals 1

    .prologue
    .line 488
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    return v0
.end method

.method public getDeviceP2p()Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 700
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v0, v0, 0x2

    if-gez v0, :cond_1

    .line 710
    :cond_0
    :goto_0
    return-object v3

    .line 703
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 707
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    and-int/2addr v0, v1

    if-lez v0, :cond_2

    .line 708
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    const/4 v4, 0x1

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    move-object v3, v0

    goto :goto_0

    .line 710
    :cond_2
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    move-object v3, v0

    goto :goto_0
.end method

.method public getDevicePlugin()Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 755
    iget v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v1, v1, 0x10

    if-gez v1, :cond_1

    .line 761
    :cond_0
    :goto_0
    return-object v0

    .line 758
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 761
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 448
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    return v0
.end method

.method public getDeviceUpnp(I)Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;
    .locals 7
    .param p1, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 732
    const/4 v3, 0x0

    .line 733
    .local v3, "nid":Ljava/lang/String;
    iget v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    and-int/lit8 v1, v1, 0x1

    if-lez v1, :cond_1

    .line 734
    const-string v3, "wlan0"

    .line 741
    :goto_0
    iget v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v1, v1, 0x10

    if-gez v1, :cond_3

    .line 751
    :cond_0
    :goto_1
    return-object v0

    .line 735
    :cond_1
    iget v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    and-int/lit8 v1, v1, 0x2

    if-lez v1, :cond_2

    .line 736
    const-string v3, "p2p-wlan0-0"

    goto :goto_0

    .line 738
    :cond_2
    const-string v1, "SconnectDevice"

    const-string v2, "getDeviceUpnp"

    const-string v4, "NIC type error!"

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 744
    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 747
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, v1, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 751
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v2, v2, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v4, v4, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v6, v5, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    move v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_1
.end method

.method public getFavoriteImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getFavoriteName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    return-object v0
.end method

.method public getHasContactInfo()Z
    .locals 1

    .prologue
    .line 618
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mHasContactInfo:Z

    return v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getImageType()I
    .locals 1

    .prologue
    .line 460
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImageType:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 614
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getServices()I
    .locals 1

    .prologue
    .line 440
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    return v0
.end method

.method public getSlinkId()J
    .locals 2

    .prologue
    .line 857
    iget-wide v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSlinkId:J

    return-wide v0
.end method

.method public getState()I
    .locals 2

    .prologue
    .line 845
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_IDLE:I

    if-ne v0, v1, :cond_0

    .line 846
    sget v0, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_IDLE:I

    .line 853
    :goto_0
    return v0

    .line 847
    :cond_0
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_FILESHARE:I

    and-int/2addr v0, v1

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_FILESHARE:I

    if-ne v0, v1, :cond_1

    .line 848
    sget v0, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_SENDING:I

    goto :goto_0

    .line 849
    :cond_1
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    and-int/2addr v0, v1

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    if-ne v0, v1, :cond_2

    .line 850
    sget v0, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_MIRRORING:I

    goto :goto_0

    .line 853
    :cond_2
    sget v0, Lcom/samsung/android/sconnect/common/util/Const;->DEVICE_STATE_IDLE:I

    goto :goto_0
.end method

.method public getUpnpDeviceType()I
    .locals 1

    .prologue
    .line 464
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    return v0
.end method

.method public getVisibleName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 634
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    .line 641
    :goto_0
    return-object v0

    .line 636
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 637
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    goto :goto_0

    .line 638
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 639
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    goto :goto_0

    .line 641
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    const v1, 0x7f0900c8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getWearableData()[B
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableData:[B

    return-object v0
.end method

.method public getWearableInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableInfo:Ljava/lang/String;

    return-object v0
.end method

.method public getWearableType()I
    .locals 1

    .prologue
    .line 468
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableType:I

    return v0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 505
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsFavorite:Z

    return v0
.end method

.method public isMyGroupDevice()Z
    .locals 1

    .prologue
    .line 496
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    return v0
.end method

.method public isP2PDevice()Z
    .locals 1

    .prologue
    .line 714
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 715
    const/4 v0, 0x1

    .line 717
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSearched()Z
    .locals 1

    .prologue
    .line 513
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    return v0
.end method

.method public removeDeviceInfo(I)I
    .locals 5
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 397
    iget v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/2addr v3, p1

    if-nez v3, :cond_0

    .line 435
    :goto_0
    return v0

    .line 401
    :cond_0
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 403
    :sswitch_0
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 404
    invoke-virtual {p0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->removeDeviceNetType(I)V

    .line 432
    :goto_1
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-nez v0, :cond_1

    move v0, v1

    .line 433
    goto :goto_0

    .line 407
    :sswitch_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->removeDeviceNetType(I)V

    goto :goto_1

    .line 410
    :sswitch_2
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->removeDeviceNetType(I)V

    goto :goto_1

    .line 413
    :sswitch_3
    iput-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    .line 414
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    .line 415
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->removeDeviceNetType(I)V

    goto :goto_1

    .line 418
    :sswitch_4
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iput-object v4, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    .line 419
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->removeDeviceNetType(I)V

    .line 420
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    .line 421
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    goto :goto_1

    .line 424
    :sswitch_5
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iput-object v4, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    .line 425
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 426
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->removeDeviceNetType(I)V

    goto :goto_1

    :cond_1
    move v0, v2

    .line 435
    goto :goto_0

    .line 401
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
        0x20 -> :sswitch_5
    .end sparse-switch
.end method

.method public removeDeviceNetType(I)V
    .locals 2
    .param p1, "deviceNetType"    # I

    .prologue
    .line 484
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    .line 485
    return-void
.end method

.method public removeState(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 865
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    .line 866
    return-void
.end method

.method public retrieveContact()V
    .locals 8

    .prologue
    .line 810
    const/4 v7, 0x0

    .line 812
    .local v7, "phones":Landroid/database/Cursor;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 813
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 814
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 816
    .local v1, "uri":Landroid/net/Uri;
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "display_name"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 820
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 822
    const-string v2, "display_name"

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    .line 824
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mHasContactInfo:Z

    .line 825
    invoke-static {v0, v1}, Landroid/provider/ContactsContract$Contacts;->lookupContact(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactUri:Landroid/net/Uri;

    .line 826
    const-string v2, "SconnectDevice"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "retrieveContact - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mContactName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 828
    const-string v2, "SconnectDevice"

    const-string v3, "retrieveContact"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mContactUri: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactUri:Landroid/net/Uri;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 834
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    if-eqz v7, :cond_1

    .line 835
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 838
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    return-void

    .line 831
    :catch_0
    move-exception v6

    .line 832
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 834
    if-eqz v7, :cond_1

    .line 835
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 834
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    if-eqz v7, :cond_2

    .line 835
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v2
.end method

.method public sameNameCheck(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    const/16 v5, 0xa

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 775
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    if-nez v3, :cond_1

    :cond_0
    move v1, v2

    .line 796
    :goto_0
    return v1

    .line 778
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v4, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->exclusive(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 779
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iget-object v4, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 780
    const-string v2, "SconnectDevice"

    const-string v3, "equals device(only name)"

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 782
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    const-string v4, "[HomeSync]"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    const-string v4, "[HomeSync]"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 783
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 784
    .local v0, "tempName":Ljava/lang/String;
    iget-object v3, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 785
    const-string v2, "SconnectDevice"

    const-string v3, "equals device(only name) :case 2"

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 788
    .end local v0    # "tempName":Ljava/lang/String;
    :cond_3
    iget-object v3, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    const-string v4, "[HomeSync]"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    const-string v4, "[HomeSync]"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 789
    iget-object v3, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 790
    .restart local v0    # "tempName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 791
    const-string v2, "SconnectDevice"

    const-string v3, "equals device(only name) : case 3"

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "tempName":Ljava/lang/String;
    :cond_4
    move v1, v2

    .line 796
    goto :goto_0
.end method

.method public setBtMac(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 80
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v0, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p1, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBtMac:Ljava/lang/String;

    goto :goto_0
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceBle;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceBle;

    .prologue
    .line 208
    if-nez p1, :cond_0

    .line 209
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "DeviceBle is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :goto_0
    return-void

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 213
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;->mBleName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 215
    :cond_1
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;->mBleInfoNum:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    .line 216
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;->mBleServiceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    .line 217
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;->mBtMac:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setBtMac(Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;->mBleMac:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    .line 219
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;->mBleP2pMac:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    .line 220
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    .line 221
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBle;->mDeviceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 223
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 224
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->retrieveContact()V

    .line 225
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    .line 226
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContext:Landroid/content/Context;

    const v1, 0x7f070021

    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getContactImage()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/GUIUtil;->cropIcon(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    .line 230
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    goto :goto_0
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceBt;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    .prologue
    .line 260
    if-nez p1, :cond_0

    .line 261
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "DeviceBt is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :goto_0
    return-void

    .line 265
    :cond_0
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 266
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtMac:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setBtMac(Ljava/lang/String;)V

    .line 267
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    .line 268
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mDeviceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 277
    iget-short v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mRSSI:S

    iput-short v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    .line 279
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    goto :goto_0
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;

    .prologue
    .line 353
    if-nez p1, :cond_0

    .line 354
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "DeviceChromecast is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    :goto_0
    return-void

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 359
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mDeviceName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 361
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceChromecast;->mID:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    .line 362
    const/16 v0, 0x13

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 364
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    goto :goto_0
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;

    .prologue
    const/4 v2, 0x1

    .line 302
    if-nez p1, :cond_0

    .line 303
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "DeviceGroupPlay is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :goto_0
    return-void

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 308
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;->mGpName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;->mGpMac:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    .line 312
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 313
    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    .line 315
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    goto :goto_0
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;

    .prologue
    .line 283
    if-nez p1, :cond_0

    .line 284
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "DeviceP2p is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 289
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mP2pMac:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    .line 290
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    .line 291
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mDeviceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 292
    iget-boolean v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mIsMyGroup:Z

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    .line 294
    iget-boolean v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceP2p;->mMirror:Z

    if-eqz v0, :cond_1

    .line 295
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_MIRRORING:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    .line 298
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    goto :goto_0
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;

    .prologue
    const/4 v2, 0x1

    .line 343
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;->mPluginName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 344
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;->mPluginIp:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    .line 345
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->updateMacFromIP()V

    .line 346
    const/16 v0, 0x9

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 348
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsPluginDevice:Z

    .line 349
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    .line 350
    return-void
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;)V
    .locals 2
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;

    .prologue
    .line 368
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 369
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSlinkId:J

    .line 370
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->getDeviceType()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 371
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/net/DeviceSlink;->getIpAddr()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    .line 372
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->updateMacFromIP()V

    .line 373
    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    .line 374
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    sget v1, Lcom/samsung/android/sconnect/common/util/Const;->SERVICE_SLINK_BROWSE_CONTENTS:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    .line 376
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    .line 377
    return-void
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceTogether;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceTogether;

    .prologue
    const/4 v2, 0x1

    .line 380
    if-nez p1, :cond_0

    .line 381
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "DeviceTogether is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    :goto_0
    return-void

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 386
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/net/DeviceTogether;->getRoomName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 388
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/net/DeviceTogether;->getRoomId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mWifiMac:Ljava/lang/String;

    .line 390
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 391
    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    .line 393
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    goto :goto_0
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;

    .prologue
    .line 319
    if-nez p1, :cond_0

    .line 320
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "DeviceUpnp is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    :goto_0
    return-void

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 325
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpID:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mUpnpUUID:Ljava/lang/String;

    .line 328
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpIP:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mDeviceIP:Ljava/lang/String;

    .line 329
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mP2pMac:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 330
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mP2pMac:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mP2pMac:Ljava/lang/String;

    .line 334
    :goto_1
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    .line 335
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mDeviceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 336
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpDeviceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    .line 337
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceUpnp;->mUpnpNIC:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    .line 339
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    goto :goto_0

    .line 332
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->updateMacFromIP()V

    goto :goto_1
.end method

.method public setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;

    .prologue
    const/16 v2, 0x8

    .line 235
    if-nez p1, :cond_0

    .line 236
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "DeviceWearable is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    :goto_0
    return-void

    .line 240
    :cond_0
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 241
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableBtMac:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setBtMac(Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableBleMac:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->mBleMac:Ljava/lang/String;

    .line 243
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableBleMac:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 245
    invoke-direct {p0, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    .line 250
    :goto_1
    iput v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 251
    iget-short v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mRSSI:S

    iput-short v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    .line 252
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableDeviceID:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableType:I

    .line 253
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mWearableInfo:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableInfo:Ljava/lang/String;

    .line 254
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;->mData:[B

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableData:[B

    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    goto :goto_0

    .line 248
    :cond_1
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    goto :goto_1
.end method

.method public setDeviceType(I)V
    .locals 0
    .param p1, "deviceType"    # I

    .prologue
    .line 444
    iput p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    .line 445
    return-void
.end method

.method public setFavorite(Z)V
    .locals 0
    .param p1, "isFavorite"    # Z

    .prologue
    .line 509
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsFavorite:Z

    .line 510
    return-void
.end method

.method public setFavoriteImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "favoriteImage"    # Landroid/graphics/Bitmap;

    .prologue
    .line 667
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteImage:Landroid/graphics/Bitmap;

    .line 668
    return-void
.end method

.method public setFavoriteName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 626
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    .line 627
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "image"    # Landroid/graphics/Bitmap;

    .prologue
    .line 456
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    .line 457
    return-void
.end method

.method public setImageType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 452
    iput p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImageType:I

    .line 453
    return-void
.end method

.method public setIsMyGroupDevice(Z)V
    .locals 4
    .param p1, "set"    # Z

    .prologue
    .line 500
    const-string v0, "SconnectDevice"

    const-string v1, "setIsMyGroupDevice "

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "update - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    .line 502
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 651
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 652
    return-void
.end method

.method public setSearched(Z)V
    .locals 0
    .param p1, "isSerched"    # Z

    .prologue
    .line 517
    iput-boolean p1, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    .line 518
    return-void
.end method

.method public updateDeviceInfo(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    .line 107
    if-nez p1, :cond_0

    .line 108
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "SconnectDevice is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :goto_0
    return-void

    .line 112
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 113
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-ne v0, v4, :cond_e

    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/Util;->isMac(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 114
    const-string v0, "SconnectDevice"

    const-string v1, "setDeviceInfo "

    const-string v2, "ignore device name: this is mac"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 132
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 136
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    .line 139
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 140
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    .line 143
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mHasContactInfo:Z

    if-nez v0, :cond_5

    .line 144
    iget-boolean v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mHasContactInfo:Z

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mHasContactInfo:Z

    .line 147
    :cond_5
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    .line 148
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    .line 150
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->updateDeviceType(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 151
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->addDeviceNetType(I)V

    .line 152
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    iget-object v1, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->setValue(Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;)V

    .line 154
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    .line 155
    iget-boolean v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsMyGroupDevice:Z

    .line 158
    :cond_6
    iget-wide v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSlinkId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_7

    .line 159
    iget-wide v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSlinkId:J

    iput-wide v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSlinkId:J

    .line 162
    :cond_7
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    iget v1, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    .line 164
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    if-nez v0, :cond_8

    .line 165
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    .line 168
    :cond_8
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableType:I

    if-nez v0, :cond_9

    .line 169
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableType:I

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableType:I

    .line 172
    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableInfo:Ljava/lang/String;

    if-nez v0, :cond_a

    .line 173
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableInfo:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWearableInfo:Ljava/lang/String;

    .line 176
    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    if-nez v0, :cond_b

    .line 177
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    .line 180
    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactUri:Landroid/net/Uri;

    if-nez v0, :cond_c

    .line 181
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactUri:Landroid/net/Uri;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactUri:Landroid/net/Uri;

    .line 184
    :cond_c
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-ne v0, v5, :cond_12

    .line 185
    iget-short v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    iput-short v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    .line 192
    :cond_d
    :goto_2
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isFavorite()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsFavorite:Z

    .line 193
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->isSearched()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    goto/16 :goto_0

    .line 116
    :cond_e
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-ne v0, v5, :cond_f

    .line 117
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-ne v0, v4, :cond_10

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/sconnect/common/util/Util;->isMac(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 118
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    .line 123
    :cond_f
    :goto_3
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    .line 124
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    goto/16 :goto_1

    .line 119
    :cond_10
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-ne v0, v5, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 120
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    goto :goto_3

    .line 128
    :cond_11
    iget-object v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    goto/16 :goto_1

    .line 186
    :cond_12
    iget v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    if-ne v0, v4, :cond_d

    .line 187
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_d

    .line 188
    iget-short v0, p1, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    iput-short v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mBtRSSI:S

    goto :goto_2
.end method

.method public updateFavoriteDeviceInfo(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    const/4 v2, 0x1

    .line 197
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDeviceIDs()Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;->setValue(Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;)V

    .line 198
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getFavoriteName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteName:Ljava/lang/String;

    .line 199
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImage()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImage:Landroid/graphics/Bitmap;

    .line 200
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getFavoriteImage()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mFavoriteImage:Landroid/graphics/Bitmap;

    .line 201
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getImageType()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mImageType:I

    .line 203
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsFavorite:Z

    .line 204
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIsSearched:Z

    .line 205
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 908
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 909
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mContactName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 910
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 911
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mServices:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 912
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mWorkingServices:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 913
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mSconnectDeviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 914
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mDeviceNetType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 915
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpNIC:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 916
    iget v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mUpnpDeviceType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 917
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->mIDs:Lcom/samsung/android/sconnect/common/device/SconnectDevice$DeviceID;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 918
    return-void
.end method
