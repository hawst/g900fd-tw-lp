.class final Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile$A2dpServiceListener;
.super Ljava/lang/Object;
.source "A2dpProfile.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "A2dpServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile$A2dpServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;
    .param p2, "x1"    # Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile$1;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile$A2dpServiceListener;-><init>(Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 50
    # getter for: Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->V:Z
    invoke-static {}, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "A2dpProfile"

    const-string v1, "onServiceConnected"

    const-string v2, "Bluetooth service connected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile$A2dpServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    check-cast p2, Landroid/bluetooth/BluetoothA2dp;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->mService:Landroid/bluetooth/BluetoothA2dp;
    invoke-static {v0, p2}, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->access$102(Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;Landroid/bluetooth/BluetoothA2dp;)Landroid/bluetooth/BluetoothA2dp;

    .line 54
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile$A2dpServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->mIsProfileReady:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->access$202(Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;Z)Z

    .line 55
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "profile"    # I

    .prologue
    .line 59
    # getter for: Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->V:Z
    invoke-static {}, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "A2dpProfile"

    const-string v1, "onServiceConnected"

    const-string v2, "Bluetooth service disconnected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile$A2dpServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->mIsProfileReady:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->access$202(Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;Z)Z

    .line 63
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile$A2dpServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->mService:Landroid/bluetooth/BluetoothA2dp;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;->access$102(Lcom/samsung/android/sconnect/central/action/BtProfile/A2dpProfile;Landroid/bluetooth/BluetoothA2dp;)Landroid/bluetooth/BluetoothA2dp;

    .line 64
    return-void
.end method
