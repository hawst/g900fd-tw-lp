.class public Lcom/samsung/android/sconnect/common/net/WifiHelper;
.super Ljava/lang/Object;
.source "WifiHelper.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/IDiscoveryAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/common/net/WifiHelper$3;
    }
.end annotation


# static fields
.field private static final OXYGEN_NOT_SUPPORTED:I = -0x1

.field private static final OXYGEN_SUPPORTED:I

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mIsOxygenSupported:I

.field private mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

.field private mPartialScanChannels:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mPartialScanMessage:Landroid/os/Message;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRegisterIntent:Z

.field private mWiFiAPScanning:Z

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-string v0, "WifiHelper"

    sput-object v0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWiFiAPScanning:Z

    .line 41
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .line 43
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mRegisterIntent:Z

    .line 45
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    .line 49
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mIsOxygenSupported:I

    .line 51
    new-instance v1, Lcom/samsung/android/sconnect/common/net/WifiHelper$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/WifiHelper$1;-><init>(Lcom/samsung/android/sconnect/common/net/WifiHelper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mPartialScanChannels:Ljava/util/ArrayList;

    .line 61
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mPartialScanMessage:Landroid/os/Message;

    .line 315
    new-instance v1, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/net/WifiHelper$2;-><init>(Lcom/samsung/android/sconnect/common/net/WifiHelper;)V

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 112
    sget-object v1, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v2, "WifiHelper"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    .line 114
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 116
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    .line 117
    .local v0, "msg":Landroid/os/Message;
    const/16 v1, 0x3e

    iput v1, v0, Landroid/os/Message;->what:I

    .line 118
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1, v0}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mIsOxygenSupported:I

    .line 119
    sget-object v1, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v2, "WifiHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[mIsOxygenSupported]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mIsOxygenSupported:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/WifiHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/common/net/WifiHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/WifiHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static isEnableMobileAP(Landroid/content/Context;)Z
    .locals 7
    .param p0, "ct"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 199
    const/4 v1, 0x0

    .line 201
    .local v1, "m":Ljava/lang/reflect/Method;
    :try_start_0
    const-string v3, "wifi"

    invoke-virtual {p0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 202
    .local v2, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "isWifiApEnabled"

    const/4 v3, 0x0

    check-cast v3, [Ljava/lang/Class;

    invoke-virtual {v5, v6, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v1

    .line 204
    const/4 v3, 0x0

    :try_start_1
    check-cast v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-ne v4, v3, :cond_0

    .line 205
    sget-object v3, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v5, "isEnableMobileAP"

    const-string v6, "HOTSPOT Enabled"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2

    move v3, v4

    .line 220
    .end local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :goto_0
    return v3

    .line 208
    .restart local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    .line 218
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_0
    :goto_1
    sget-object v3, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v4, "isEnableMobileAP"

    const-string v5, "HOTSPOT Disabled"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const/4 v3, 0x0

    goto :goto_0

    .line 210
    .restart local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_1
    move-exception v0

    .line 211
    .local v0, "e":Ljava/lang/IllegalAccessException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 215
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    .end local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_2
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    sget-object v3, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v4, "isEnableMobileAP : NoSuchMethodException :"

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 212
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    .restart local v2    # "wifiManager":Landroid/net/wifi/WifiManager;
    :catch_3
    move-exception v0

    .line 213
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1
.end method

.method public static isOxygenEnabled(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 224
    const-string v1, "wifi"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 225
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiIBSSEnabled()Z

    move-result v1

    return v1
.end method

.method private setIntentFilter()V
    .locals 3

    .prologue
    .line 251
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mRegisterIntent:Z

    if-nez v1, :cond_0

    .line 252
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mRegisterIntent:Z

    .line 253
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 254
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 255
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 256
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 257
    const-string v1, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 258
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 259
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 262
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private setPartialScanMessage()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    .line 65
    iget v7, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mIsOxygenSupported:I

    if-nez v7, :cond_4

    .line 67
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 68
    .local v5, "scanningChannels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v7}, Landroid/net/wifi/WifiManager;->getSupportedChannels()Ljava/util/List;

    move-result-object v1

    .line 69
    .local v1, "deviceSupportedChannels":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiChannel;>;"
    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_2

    .line 70
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiChannel;

    .line 71
    .local v6, "supportedChannel":Landroid/net/wifi/WifiChannel;
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mPartialScanChannels:Ljava/util/ArrayList;

    iget v8, v6, Landroid/net/wifi/WifiChannel;->frequency:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 77
    iget v7, v6, Landroid/net/wifi/WifiChannel;->frequency:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 86
    .end local v6    # "supportedChannel":Landroid/net/wifi/WifiChannel;
    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    new-array v4, v7, [I

    .line 87
    .local v4, "scanningChannelArray":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_3

    .line 88
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v4, v2

    .line 87
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 91
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "scanningChannelArray":[I
    :cond_2
    sget-object v7, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v8, "setPartialScanMessage"

    const-string v9, "mDeviceSupportedChannels is null or size 0"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    new-array v4, v10, [I

    fill-array-data v4, :array_0

    .line 103
    .end local v1    # "deviceSupportedChannels":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/WifiChannel;>;"
    .end local v5    # "scanningChannels":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v4    # "scanningChannelArray":[I
    :cond_3
    :goto_2
    sget-object v7, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v8, "setPartialScanMessage"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SEC_COMMAND_ID_PARTIAL_SCAN : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v4}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mPartialScanMessage:Landroid/os/Message;

    const/16 v8, 0x24

    iput v8, v7, Landroid/os/Message;->what:I

    .line 106
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 107
    .local v0, "b":Landroid/os/Bundle;
    const-string v7, "channel"

    invoke-virtual {v0, v7, v4}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 108
    iget-object v7, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mPartialScanMessage:Landroid/os/Message;

    iput-object v0, v7, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 109
    return-void

    .line 97
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v4    # "scanningChannelArray":[I
    :cond_4
    sget-object v7, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v8, "setPartialScanMessage"

    const-string v9, "OXYGEN_NOT_SUPPORTED"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    new-array v4, v10, [I

    fill-array-data v4, :array_1

    .restart local v4    # "scanningChannelArray":[I
    goto :goto_2

    .line 92
    :array_0
    .array-data 4
        0x96c
        0x985
        0x99e
    .end array-data

    .line 98
    :array_1
    .array-data 4
        0x96c
        0x985
        0x99e
    .end array-data
.end method

.method private unregisterIntent()V
    .locals 2

    .prologue
    .line 265
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mRegisterIntent:Z

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mRegisterIntent:Z

    .line 267
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 271
    :cond_0
    return-void
.end method


# virtual methods
.method public checkConnectedAP(Landroid/content/Context;)Z
    .locals 6
    .param p1, "ct"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 183
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->checkEnabledNetwork()Z

    move-result v4

    if-nez v4, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v2

    .line 187
    :cond_1
    const-string v4, "connectivity"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 189
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 191
    .local v1, "netInfoWifi":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    sget-object v5, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v4, v5}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 192
    goto :goto_0
.end method

.method public checkEnabledNetwork()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 172
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 176
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 177
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public disableWifi(Landroid/content/Context;)V
    .locals 2
    .param p1, "ct"    # Landroid/content/Context;

    .prologue
    .line 229
    const-string v1, "wifi"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 230
    .local v0, "wifiManager":Landroid/net/wifi/WifiManager;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 231
    return-void
.end method

.method public getDeviceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getScanResult()V
    .locals 10

    .prologue
    .line 274
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v3

    .line 275
    .local v3, "results":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/ScanResult;>;"
    if-eqz v3, :cond_4

    .line 276
    sget-object v6, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v7, "getScanResult"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getScanResults size : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/ScanResult;

    .line 278
    .local v2, "result":Landroid/net/wifi/ScanResult;
    iget-object v6, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 279
    iget-object v6, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    const-string v7, "GroupPlay-"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v6, :cond_1

    .line 280
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    const-string v7, "com.samsung.groupcast"

    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 282
    sget-object v6, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v7, "getScanResult"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "WiFi GroupPlay AP name : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v2, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;

    iget-object v6, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget-object v7, v2, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-direct {v0, v6, v7}, Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;
    new-instance v5, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 286
    .local v5, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v5, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;)V

    .line 287
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v6, v5}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 288
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 293
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceGroupPlay;
    .end local v5    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_1
    :goto_1
    iget-object v6, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    const-string v7, "GT_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v6, :cond_0

    .line 294
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    const-string v7, "com.sec.android.app.alltogether"

    invoke-static {v6, v7}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 296
    sget-object v6, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v7, "getScanResult"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "WiFi Together AP name : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v2, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iget-object v6, v2, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 299
    .local v4, "roomName":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceTogether;

    iget-object v6, v2, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-direct {v0, v4, v6}, Lcom/samsung/android/sconnect/common/device/net/DeviceTogether;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    .local v0, "device":Lcom/samsung/android/sconnect/common/device/net/DeviceTogether;
    new-instance v5, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 301
    .restart local v5    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v5, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceTogether;)V

    .line 302
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v6, v5}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    .line 303
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 290
    .end local v0    # "device":Lcom/samsung/android/sconnect/common/device/net/DeviceTogether;
    .end local v4    # "roomName":Ljava/lang/String;
    .end local v5    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_2
    sget-object v6, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v7, "getScanResult"

    const-string v8, "GroupPlay not supported. Do not add it"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 305
    :cond_3
    sget-object v6, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v7, "getScanResult"

    const-string v8, "Together not supported. Do not add it"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 311
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "result":Landroid/net/wifi/ScanResult;
    :cond_4
    sget-object v6, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v7, "getScanResult"

    const-string v8, "getScanResults is null"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    :cond_5
    return-void
.end method

.method public initiate(Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .line 125
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWifiManager:Landroid/net/wifi/WifiManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 130
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->setIntentFilter()V

    .line 131
    return-void
.end method

.method public startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .prologue
    .line 147
    sget-object v0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v1, "startDiscovery"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDeviceList:Ljava/util/ArrayList;

    .line 149
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 151
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->setPartialScanMessage()V

    .line 152
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWifiManager:Landroid/net/wifi/WifiManager;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mPartialScanMessage:Landroid/os/Message;

    invoke-virtual {v0, v1}, Landroid/net/wifi/WifiManager;->callSECApi(Landroid/os/Message;)I

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWiFiAPScanning:Z

    .line 154
    return-void
.end method

.method public stopDiscovery()V
    .locals 3

    .prologue
    .line 158
    sget-object v0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v1, "stopDiscovery"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 163
    return-void
.end method

.method public terminate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 134
    sget-object v0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->TAG:Ljava/lang/String;

    const-string v1, "terminate"

    const-string v2, " -- "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->stopDiscovery()V

    .line 136
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/WifiHelper;->unregisterIntent()V

    .line 138
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWiFiAPScanning:Z

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mWiFiAPScanning:Z

    .line 141
    :cond_0
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 142
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/WifiHelper;->mDeviceList:Ljava/util/ArrayList;

    .line 143
    return-void
.end method
