.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->onPrintPickerResult(Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

.field final synthetic val$data:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2492
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iput-object p2, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->val$data:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    .line 2495
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v7, v8}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4802(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 2496
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsPrintable:Z
    invoke-static {v7, v10}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5202(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    .line 2497
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mUriProcessor:Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->val$data:Landroid/content/Intent;

    invoke-virtual {v7, v8}, Lcom/samsung/android/sconnect/central/ui/picker/UriProcessor;->process(Landroid/content/Intent;)[Landroid/net/Uri;

    move-result-object v6

    .line 2498
    .local v6, "uris":[Landroid/net/Uri;
    if-eqz v6, :cond_1

    .line 2500
    move-object v0, v6

    .local v0, "arr$":[Landroid/net/Uri;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v5, v0, v1

    .line 2501
    .local v5, "uri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2502
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2503
    .local v4, "path":Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getMimeTypeFromPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2504
    .local v3, "mimeType":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsPrintable:Z
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-static {v4}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;)I

    move-result v7

    if-eq v7, v10, :cond_2

    if-eqz v3, :cond_2

    const-string v7, "pdf"

    invoke-virtual {v3, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 2507
    const-string v7, "SconnectDisplay"

    const-string v8, "onPrintPickerResult"

    const-string v9, "NOT Supported file type"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2508
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v8, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsPrintable:Z
    invoke-static {v7, v8}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5202(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    .line 2513
    .end local v3    # "mimeType":Ljava/lang/String;
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/Util;->isPdfContents(Ljava/util/ArrayList;)Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-le v7, v10, :cond_3

    .line 2514
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v7, v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    if-eqz v7, :cond_1

    .line 2515
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v7, v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    new-instance v8, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$1;

    invoke-direct {v8, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$1;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2617
    .end local v0    # "arr$":[Landroid/net/Uri;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    :cond_1
    :goto_1
    return-void

    .line 2500
    .restart local v0    # "arr$":[Landroid/net/Uri;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v3    # "mimeType":Ljava/lang/String;
    .restart local v4    # "path":Ljava/lang/String;
    .restart local v5    # "uri":Landroid/net/Uri;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2550
    .end local v3    # "mimeType":Ljava/lang/String;
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/sconnect/common/util/Util;->isImageContents(Ljava/util/ArrayList;)Z

    move-result v7

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/16 v8, 0xa

    if-le v7, v8, :cond_4

    .line 2551
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v7, v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    if-eqz v7, :cond_1

    .line 2552
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v7, v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    new-instance v8, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$2;

    invoke-direct {v8, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$2;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 2577
    :cond_4
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v7, v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    if-eqz v7, :cond_1

    .line 2578
    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v7, v7, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mGUIHandler:Landroid/os/Handler;

    new-instance v8, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;

    invoke-direct {v8, p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21$3;-><init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$21;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method
