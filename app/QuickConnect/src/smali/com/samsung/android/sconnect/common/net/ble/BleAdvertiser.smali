.class public Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;
.super Ljava/lang/Object;
.source "BleAdvertiser.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BleAdvertiser"


# instance fields
.field private mAdvCallback:Landroid/bluetooth/le/AdvertiseCallback;

.field private mAdvertiseSettings:Landroid/bluetooth/le/AdvertiseSettings;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

.field private mIsAdvStarted:Z

.field private mIsSupporSingleAdv:I

.field private mIsSupportMultiAdv:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-boolean v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsAdvStarted:Z

    .line 15
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 17
    iput v1, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupportMultiAdv:I

    .line 18
    iput v1, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupporSingleAdv:I

    .line 19
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mAdvertiseSettings:Landroid/bluetooth/le/AdvertiseSettings;

    .line 20
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    .line 79
    new-instance v0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser$1;-><init>(Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mAdvCallback:Landroid/bluetooth/le/AdvertiseCallback;

    .line 23
    const-string v0, "BleAdvertiser"

    const-string v1, "BleAdvertiser"

    const-string v2, "Constructor"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 25
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_0

    .line 26
    const-string v0, "BleAdvertiser"

    const-string v1, "BleAdvertiser"

    const-string v2, "Unable to retrieve mBluetoothAdapter"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    :goto_0
    return-void

    .line 29
    :cond_0
    new-instance v0, Landroid/bluetooth/le/AdvertiseSettings$Builder;

    invoke-direct {v0}, Landroid/bluetooth/le/AdvertiseSettings$Builder;-><init>()V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/bluetooth/le/AdvertiseSettings$Builder;->setAdvertiseMode(I)Landroid/bluetooth/le/AdvertiseSettings$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/bluetooth/le/AdvertiseSettings$Builder;->setConnectable(Z)Landroid/bluetooth/le/AdvertiseSettings$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/le/AdvertiseSettings$Builder;->build()Landroid/bluetooth/le/AdvertiseSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mAdvertiseSettings:Landroid/bluetooth/le/AdvertiseSettings;

    .line 32
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->getBluetoothLeAdvertiser()V

    goto :goto_0
.end method

.method private getBluetoothLeAdvertiser()V
    .locals 3

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    .line 48
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 49
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->isSupportMultiAdv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeAdvertiser()Landroid/bluetooth/le/BluetoothLeAdvertiser;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    .line 57
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    if-nez v0, :cond_3

    .line 58
    const-string v0, "BleAdvertiser"

    const-string v1, "getBluetoothLeAdvertiser"

    const-string v2, "Was Unable to retrieve mBluetoothLeAdvertiser"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :goto_1
    return-void

    .line 51
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->isSupportSingleAdv()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBluetoothLeAdvertiserForSingle()Landroid/bluetooth/le/BluetoothLeAdvertiser;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    goto :goto_0

    .line 54
    :cond_2
    const-string v0, "BleAdvertiser"

    const-string v1, "getBluetoothLeAdvertiser"

    const-string v2, "Not Support Advertisement"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 60
    :cond_3
    const-string v0, "BleAdvertiser"

    const-string v1, "getBluetoothLeAdvertiser"

    const-string v2, "getBluetoothLeAdvertiser successed"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private isSupportMultiAdv()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 94
    iget v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupportMultiAdv:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 96
    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isMultipleAdvertisementSupported()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 97
    const-string v3, "BleAdvertiser"

    const-string v4, "isSupportMultiAdv"

    const-string v5, "isMultipleAdvertisementSupported : enabled"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const/4 v3, 0x1

    iput v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupportMultiAdv:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :cond_0
    :goto_0
    iget v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupportMultiAdv:I

    if-ne v3, v2, :cond_1

    move v1, v2

    .line 111
    :cond_1
    :goto_1
    return v1

    .line 100
    :cond_2
    :try_start_1
    const-string v3, "BleAdvertiser"

    const-string v4, "isSupportMultiAdv"

    const-string v5, "isMultipleAdvertisementSupported : disabled"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupportMultiAdv:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "BleAdvertiser"

    const-string v3, "isSupportMultiAdv"

    const-string v4, "NullPointerException"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private isSupportSingleAdv()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 115
    iget v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupporSingleAdv:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 117
    :try_start_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isPeripheralModeSupported()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 118
    const-string v3, "BleAdvertiser"

    const-string v4, "isSupportSingleAdv"

    const-string v5, "isPeripheralModeSupported : enabled"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    const/4 v3, 0x1

    iput v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupporSingleAdv:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :cond_0
    :goto_0
    iget v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupporSingleAdv:I

    if-ne v3, v2, :cond_1

    move v1, v2

    .line 132
    :cond_1
    :goto_1
    return v1

    .line 121
    :cond_2
    :try_start_1
    const-string v3, "BleAdvertiser"

    const-string v4, "isSupportSingleAdv"

    const-string v5, "isPeripheralModeSupported : disabled"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsSupporSingleAdv:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "BleAdvertiser"

    const-string v3, "isSupportSingleAdv"

    const-string v4, "NullPointerException"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public isEnableBluetoothLeAdvertiser()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    if-nez v0, :cond_0

    .line 74
    const/4 v0, 0x0

    .line 76
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public startBroadcast(I[B[B)V
    .locals 6
    .param p1, "manuID"    # I
    .param p2, "advData"    # [B
    .param p3, "respData"    # [B

    .prologue
    const/4 v5, 0x0

    .line 136
    const-string v2, "BleAdvertiser"

    const-string v3, "startBroadcast"

    const-string v4, "Start BLE advertising"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->isSupportMultiAdv()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->isSupportSingleAdv()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 139
    :cond_0
    new-instance v2, Landroid/bluetooth/le/AdvertiseData$Builder;

    invoke-direct {v2}, Landroid/bluetooth/le/AdvertiseData$Builder;-><init>()V

    invoke-virtual {v2, p1, p2}, Landroid/bluetooth/le/AdvertiseData$Builder;->addManufacturerData(I[B)Landroid/bluetooth/le/AdvertiseData$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/bluetooth/le/AdvertiseData$Builder;->setIncludeDeviceName(Z)Landroid/bluetooth/le/AdvertiseData$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/bluetooth/le/AdvertiseData$Builder;->setIncludeTxPowerLevel(Z)Landroid/bluetooth/le/AdvertiseData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/le/AdvertiseData$Builder;->build()Landroid/bluetooth/le/AdvertiseData;

    move-result-object v0

    .line 142
    .local v0, "advertiseData":Landroid/bluetooth/le/AdvertiseData;
    new-instance v2, Landroid/bluetooth/le/AdvertiseData$Builder;

    invoke-direct {v2}, Landroid/bluetooth/le/AdvertiseData$Builder;-><init>()V

    invoke-virtual {v2, p1, p3}, Landroid/bluetooth/le/AdvertiseData$Builder;->addManufacturerData(I[B)Landroid/bluetooth/le/AdvertiseData$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/bluetooth/le/AdvertiseData$Builder;->setIncludeDeviceName(Z)Landroid/bluetooth/le/AdvertiseData$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/bluetooth/le/AdvertiseData$Builder;->setIncludeTxPowerLevel(Z)Landroid/bluetooth/le/AdvertiseData$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/le/AdvertiseData$Builder;->build()Landroid/bluetooth/le/AdvertiseData;

    move-result-object v1

    .line 145
    .local v1, "scanResponseData":Landroid/bluetooth/le/AdvertiseData;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    if-eqz v2, :cond_1

    .line 147
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsAdvStarted:Z

    .line 148
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mAdvertiseSettings:Landroid/bluetooth/le/AdvertiseSettings;

    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mAdvCallback:Landroid/bluetooth/le/AdvertiseCallback;

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/bluetooth/le/BluetoothLeAdvertiser;->startAdvertising(Landroid/bluetooth/le/AdvertiseSettings;Landroid/bluetooth/le/AdvertiseData;Landroid/bluetooth/le/AdvertiseData;Landroid/bluetooth/le/AdvertiseCallback;)V

    .line 158
    .end local v0    # "advertiseData":Landroid/bluetooth/le/AdvertiseData;
    .end local v1    # "scanResponseData":Landroid/bluetooth/le/AdvertiseData;
    :goto_0
    return-void

    .line 151
    .restart local v0    # "advertiseData":Landroid/bluetooth/le/AdvertiseData;
    .restart local v1    # "scanResponseData":Landroid/bluetooth/le/AdvertiseData;
    :cond_1
    const-string v2, "BleAdvertiser"

    const-string v3, "startBroadcast"

    const-string v4, "mBluetoothLeAdvertiser is null - try getBluetoothLeAdvertiser"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->getBluetoothLeAdvertiser()V

    goto :goto_0

    .line 156
    .end local v0    # "advertiseData":Landroid/bluetooth/le/AdvertiseData;
    .end local v1    # "scanResponseData":Landroid/bluetooth/le/AdvertiseData;
    :cond_2
    const-string v2, "BleAdvertiser"

    const-string v3, "startBroadcast"

    const-string v4, "Not Support Advertisement"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopBroadcast()V
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    if-eqz v0, :cond_1

    .line 162
    const-string v0, "BleAdvertiser"

    const-string v1, "stopBroadcast"

    const-string v2, "Stop BLE advertising"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsAdvStarted:Z

    if-eqz v0, :cond_0

    .line 165
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsAdvStarted:Z

    .line 166
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mAdvCallback:Landroid/bluetooth/le/AdvertiseCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/le/BluetoothLeAdvertiser;->stopAdvertising(Landroid/bluetooth/le/AdvertiseCallback;)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 169
    :cond_1
    const-string v0, "BleAdvertiser"

    const-string v1, "stopBroadcast"

    const-string v2, "mBluetoothLeAdvertiser is null- try getBluetoothLeAdvertiser"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->getBluetoothLeAdvertiser()V

    goto :goto_0
.end method

.method public terminate()V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    if-eqz v0, :cond_0

    .line 37
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsAdvStarted:Z

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mIsAdvStarted:Z

    .line 39
    const-string v0, "BleAdvertiser"

    const-string v1, "terminate"

    const-string v2, "Stop BLE advertising"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mBluetoothLeAdvertiser:Landroid/bluetooth/le/BluetoothLeAdvertiser;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/ble/BleAdvertiser;->mAdvCallback:Landroid/bluetooth/le/AdvertiseCallback;

    invoke-virtual {v0, v1}, Landroid/bluetooth/le/BluetoothLeAdvertiser;->stopAdvertising(Landroid/bluetooth/le/AdvertiseCallback;)V

    .line 44
    :cond_0
    return-void
.end method
