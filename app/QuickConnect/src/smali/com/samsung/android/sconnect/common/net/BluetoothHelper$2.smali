.class Lcom/samsung/android/sconnect/common/net/BluetoothHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 179
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 181
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    const/high16 v3, -0x80000000

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 183
    .local v1, "state":I
    const/16 v2, 0xa

    if-ne v1, v2, :cond_1

    .line 184
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ACTION_STATE_CHANGED"

    const-string v4, "STATE_OFF"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    .end local v1    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 185
    .restart local v1    # "state":I
    :cond_1
    const/16 v2, 0xb

    if-ne v1, v2, :cond_2

    .line 186
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ACTION_STATE_CHANGED"

    const-string v4, "STATE_TURNING_ON"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 187
    :cond_2
    const/16 v2, 0xc

    if-ne v1, v2, :cond_3

    .line 188
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ACTION_STATE_CHANGED"

    const-string v4, "STATE_ON"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 189
    :cond_3
    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    .line 190
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$2;->this$0:Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    # getter for: Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "ACTION_STATE_CHANGED"

    const-string v4, "STATE_TURNING_OFF"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
