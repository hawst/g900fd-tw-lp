.class public Lcom/samsung/android/sconnect/common/device/net/DeviceBt;
.super Ljava/lang/Object;
.source "DeviceBt.java"


# static fields
.field private static final BluetoothClass_MAYBE_LEGACY_PRINTER:I = 0x680

.field static final TAG:Ljava/lang/String; = "DeviceBt"


# instance fields
.field public mBondState:I

.field public mBtMac:Ljava/lang/String;

.field public mBtName:Ljava/lang/String;

.field public mDeviceType:I

.field public mRSSI:S


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IS)V
    .locals 4
    .param p1, "displayname"    # Ljava/lang/String;
    .param p2, "btMac"    # Ljava/lang/String;
    .param p3, "btDeviceType"    # I
    .param p4, "RSSI"    # S

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtName:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtMac:Ljava/lang/String;

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mDeviceType:I

    .line 19
    const/16 v0, -0x12c

    iput-short v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mRSSI:S

    .line 20
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBondState:I

    .line 23
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtName:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtMac:Ljava/lang/String;

    .line 25
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtName:Ljava/lang/String;

    const-string v1, "HomeSync"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mDeviceType:I

    .line 30
    :goto_0
    iput-short p4, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mRSSI:S

    .line 31
    const-string v0, "DeviceBt"

    const-string v1, "Convert type"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mDeviceType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    return-void

    .line 28
    :cond_0
    invoke-virtual {p0, p3}, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->convertBTDeviceType(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mDeviceType:I

    goto :goto_0
.end method


# virtual methods
.method public final convertBTDeviceType(I)I
    .locals 4
    .param p1, "btDeviceType"    # I

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x4

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 114
    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1a

    :goto_0
    :sswitch_0
    return v1

    .line 40
    :sswitch_1
    const/16 v1, 0x9

    goto :goto_0

    .line 49
    :sswitch_2
    const/4 v1, 0x5

    goto :goto_0

    .line 56
    :sswitch_3
    const/4 v1, 0x1

    goto :goto_0

    .line 64
    :sswitch_4
    const/16 v1, 0xf

    goto :goto_0

    :sswitch_5
    move v1, v2

    .line 70
    goto :goto_0

    .line 72
    :sswitch_6
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtName:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 73
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;->mBtName:Ljava/lang/String;

    sget-object v3, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "name":Ljava/lang/String;
    const-string v1, "TVBLUETOOTH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "[TV]"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "BRAVIA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "SAMSUNG 3D TV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    :cond_0
    const/4 v1, 0x6

    goto :goto_0

    .end local v0    # "name":Ljava/lang/String;
    :cond_1
    move v1, v2

    .line 79
    goto :goto_0

    .line 86
    :sswitch_7
    const/16 v1, 0x8

    goto :goto_0

    .line 88
    :sswitch_8
    const/4 v1, 0x7

    goto :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x100 -> :sswitch_0
        0x104 -> :sswitch_0
        0x108 -> :sswitch_0
        0x10c -> :sswitch_2
        0x110 -> :sswitch_0
        0x114 -> :sswitch_0
        0x118 -> :sswitch_0
        0x200 -> :sswitch_3
        0x204 -> :sswitch_3
        0x208 -> :sswitch_3
        0x20c -> :sswitch_3
        0x210 -> :sswitch_3
        0x214 -> :sswitch_3
        0x404 -> :sswitch_5
        0x408 -> :sswitch_5
        0x41c -> :sswitch_5
        0x420 -> :sswitch_5
        0x428 -> :sswitch_5
        0x43c -> :sswitch_6
        0x500 -> :sswitch_4
        0x504 -> :sswitch_4
        0x508 -> :sswitch_4
        0x50c -> :sswitch_4
        0x540 -> :sswitch_4
        0x580 -> :sswitch_4
        0x5c0 -> :sswitch_4
        0x620 -> :sswitch_8
        0x680 -> :sswitch_1
        0x700 -> :sswitch_7
        0x704 -> :sswitch_7
        0x708 -> :sswitch_7
        0x70c -> :sswitch_7
        0x710 -> :sswitch_7
        0x714 -> :sswitch_7
    .end sparse-switch
.end method
