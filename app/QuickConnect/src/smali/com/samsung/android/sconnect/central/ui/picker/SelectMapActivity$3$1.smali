.class Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;
.super Ljava/lang/Object;
.source "SelectMapActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getMyLocation()Lcom/google/android/maps/GeoPoint;
    invoke-static {v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$500(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$402(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Lcom/google/android/maps/GeoPoint;

    .line 233
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$600(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    const-string v2, "start update location"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v1, v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v2, v2, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchPoint:Lcom/google/android/maps/GeoPoint;
    invoke-static {v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$400(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$800(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v1

    # setter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mLocation:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$702(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 236
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSelectMapHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$900(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->TAG:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$600(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    const-string v2, "search point is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getLocationServiceAvailable()Z
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$1000(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;->this$1:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1$1;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$3$1;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
