.class Lcom/samsung/android/sconnect/central/ui/TvController$2;
.super Ljava/lang/Object;
.source "TvController.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/TvController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/TvController;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/TvController;)V
    .locals 0

    .prologue
    .line 498
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/TvController$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 502
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 503
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/TvController$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mSconnectDisplay:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$200(Lcom/samsung/android/sconnect/central/ui/TvController;)Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 505
    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/TvController$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$500(Lcom/samsung/android/sconnect/central/ui/TvController;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {p1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v5, v6, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 508
    .local v2, "toast":Landroid/widget/Toast;
    const/4 v5, 0x2

    new-array v1, v5, [I

    .line 509
    .local v1, "pos":[I
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 510
    iget v5, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    aget v6, v1, v8

    sub-int/2addr v5, v6

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/TvController$2;->this$0:Lcom/samsung/android/sconnect/central/ui/TvController;

    # getter for: Lcom/samsung/android/sconnect/central/ui/TvController;->mResources:Landroid/content/res/Resources;
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/ui/TvController;->access$600(Lcom/samsung/android/sconnect/central/ui/TvController;)Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070040

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    sub-float/2addr v5, v6

    float-to-int v3, v5

    .line 512
    .local v3, "x":I
    aget v5, v1, v9

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int v4, v5, v6

    .line 514
    .local v4, "y":I
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/Util;->isLocaleRTL()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 515
    aget v3, v1, v8

    .line 518
    :cond_0
    const-string v5, "TvController"

    const-string v6, "onLongClick"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " x "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    const v5, 0x800035

    invoke-virtual {v2, v5, v3, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 520
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 521
    return v9
.end method
