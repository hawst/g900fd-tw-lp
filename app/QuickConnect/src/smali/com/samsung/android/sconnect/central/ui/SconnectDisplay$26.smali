.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;
.super Landroid/content/BroadcastReceiver;
.source "SconnectDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V
    .locals 0

    .prologue
    .line 2867
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    .line 2870
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2871
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->isInitialStickyBroadcast()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2948
    :cond_0
    :goto_0
    return-void

    .line 2875
    :cond_1
    const-string v4, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2876
    const-string v4, "wifi_state"

    const/4 v5, 0x4

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v6, :cond_0

    .line 2878
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "wifi"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2882
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    const-string v6, "WIFI_STATE_CHANGED_ACTION : WIFI_STATE_DISABLED"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2883
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto :goto_0

    .line 2885
    :cond_2
    const-string v4, "android.net.wifi.p2p.STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2886
    const-string v4, "wifi_p2p_state"

    const/4 v5, -0x1

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v6, :cond_0

    .line 2888
    :try_start_0
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/SconnectManager;->getP2pHelper()Lcom/samsung/android/sconnect/common/net/P2pHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/common/net/P2pHelper;->isP2pEnabled()Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_0

    .line 2895
    :goto_1
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    const-string v6, "WIFI_P2P_STATE_CHANGED_ACTION : WIFI_P2P_STATE_DISABLED"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2897
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto :goto_0

    .line 2899
    :cond_3
    const-string v4, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2900
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForDisconnect:Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2901
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isWaitForDisconnect:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForDisconnect:Z
    invoke-static {v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5500(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2902
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getPrimaryAction()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->invokeAction(Lcom/samsung/android/sconnect/central/ui/DisplayItem;I)V

    .line 2903
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/4 v5, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsWaitForDisconnect:Z
    invoke-static {v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5502(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;Z)Z

    goto/16 :goto_0

    .line 2905
    :cond_4
    const-string v4, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2906
    const-string v4, "android.bluetooth.adapter.extra.STATE"

    const/high16 v5, -0x80000000

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2908
    .local v1, "state":I
    const/16 v4, 0xd

    if-ne v1, v4, :cond_5

    .line 2909
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    const-string v6, "BT_ACTION_STATE_CHANGED : STATE_TURNING_OFF"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2910
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 2911
    :cond_5
    const/16 v4, 0xa

    if-ne v1, v4, :cond_0

    .line 2912
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    const-string v6, "BT_ACTION_STATE_CHANGED : STATE_OFF"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2914
    .end local v1    # "state":I
    :cond_6
    const-string v4, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2915
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1900(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/sconnect/common/util/Util;->getTopActivityName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 2916
    .local v2, "topActivity":Ljava/lang/String;
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SCREEN_ON, start discovery :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2917
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v4, v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v4, v5, :cond_7

    .line 2918
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->refreshDiscoveryDevice()V

    .line 2920
    :cond_7
    if-eqz v2, :cond_0

    const-class v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2921
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    const-string v6, "start home screen checker"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2922
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/16 v5, 0xbc8

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(I)V

    goto/16 :goto_0

    .line 2924
    .end local v2    # "topActivity":Ljava/lang/String;
    :cond_8
    const-string v4, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 2925
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    const-string v6, "SCREEN_OFF, stop discovery"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2926
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mIsDiscovering:Z
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 2927
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/CentralManager;->stopDiscoveryDevice()V

    .line 2929
    :cond_9
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v4, v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MAIN:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-eq v4, v5, :cond_a

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    iget-object v4, v4, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCurrentlayout:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    sget-object v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;->MULTIPLE_CHOICE_MODE:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$SconnectLayout;

    if-ne v4, v5, :cond_b

    .line 2931
    :cond_a
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const-wide/16 v6, -0x1

    # setter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLastTime:J
    invoke-static {v4, v6, v7}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5602(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;J)J

    .line 2933
    :cond_b
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/CentralManager;->resumeMirroring()V

    .line 2934
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    const/16 v5, 0xbc9

    invoke-virtual {v4, v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->notifyGuiMessage(I)V

    goto/16 :goto_0

    .line 2935
    :cond_c
    const-string v4, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2936
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    const-string v6, "HOME_RESUME"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2937
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-static {v4}, Lcom/samsung/android/sconnect/common/util/Util;->getTopActivityName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 2939
    .local v3, "topActivityName":Ljava/lang/String;
    sget-object v4, Lcom/samsung/android/sconnect/common/util/GUIUtil;->mActivityPackageList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2940
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    const-string v6, "HOME_RESUME - Quick Connect has top activity"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2942
    :cond_d
    const-string v4, "SconnectDisplay"

    const-string v5, "mReceiver"

    const-string v6, "HOME_RESUME - Quick Connect does not have top activity. Finish activity"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2944
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->terminateCentralManager()V
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4600(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    .line 2945
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$26;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v4}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto/16 :goto_0

    .line 2891
    .end local v3    # "topActivityName":Ljava/lang/String;
    :catch_0
    move-exception v4

    goto/16 :goto_1
.end method
