.class Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;
.super Lcom/google/android/maps/ItemizedOverlay;
.source "SelectMapActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectMapOverlay"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/maps/ItemizedOverlay",
        "<",
        "Lcom/google/android/maps/OverlayItem;",
        ">;"
    }
.end annotation


# instance fields
.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mOverlays:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/maps/OverlayItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "defaultMarker"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 996
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    .line 997
    invoke-static {p3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->boundCenterBottom(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/maps/ItemizedOverlay;-><init>(Landroid/graphics/drawable/Drawable;)V

    .line 992
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mOverlays:Ljava/util/ArrayList;

    .line 998
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay$MapGestureListener;-><init>(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;)V

    invoke-direct {v0, p2, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mGestureDetector:Landroid/view/GestureDetector;

    .line 999
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->populate()V

    .line 1000
    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;

    .prologue
    .line 988
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mOverlays:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public addOverlay(Lcom/google/android/maps/OverlayItem;)V
    .locals 1
    .param p1, "overlay"    # Lcom/google/android/maps/OverlayItem;

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1004
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->populate()V

    .line 1005
    return-void
.end method

.method public clearOverlay()V
    .locals 1

    .prologue
    .line 1008
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1009
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->populate()V

    .line 1010
    return-void
.end method

.method protected createItem(I)Lcom/google/android/maps/OverlayItem;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/maps/OverlayItem;

    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;Z)V
    .locals 1
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "v"    # Lcom/google/android/maps/MapView;
    .param p3, "b"    # Z

    .prologue
    .line 1014
    const/4 v0, 0x0

    invoke-super {p0, p1, p2, v0}, Lcom/google/android/maps/ItemizedOverlay;->draw(Landroid/graphics/Canvas;Lcom/google/android/maps/MapView;Z)V

    .line 1015
    return-void
.end method

.method protected onTap(I)Z
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 1029
    invoke-super {p0, p1}, Lcom/google/android/maps/ItemizedOverlay;->onTap(I)Z

    .line 1031
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/maps/OverlayItem;

    invoke-virtual {v3}, Lcom/google/android/maps/OverlayItem;->getPoint()Lcom/google/android/maps/GeoPoint;

    move-result-object v2

    .line 1032
    .local v2, "point":Lcom/google/android/maps/GeoPoint;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    iget-object v3, v3, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mMapView:Lcom/google/android/maps/MapView;

    invoke-virtual {v3}, Lcom/google/android/maps/MapView;->getMapCenter()Lcom/google/android/maps/GeoPoint;

    move-result-object v1

    .line 1034
    .local v1, "center":Lcom/google/android/maps/GeoPoint;
    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLatitudeE6()I

    move-result v4

    if-ne v3, v4, :cond_1

    invoke-virtual {v2}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/maps/GeoPoint;->getLongitudeE6()I

    move-result v4

    if-ne v3, v4, :cond_1

    .line 1036
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->updateLocation(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;
    invoke-static {v4, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$800(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1037
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mSearchText:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$300(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->length()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setSelection(I)V

    .line 1038
    const-string v0, ""

    .line 1039
    .local v0, "address":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # getter for: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->mPickImage:Z
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$1200(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1043
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->getURL(Lcom/google/android/maps/GeoPoint;)Ljava/lang/String;

    move-result-object v0

    .line 1049
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    invoke-virtual {v3, v0}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->doFinish(Ljava/lang/String;)V

    .line 1054
    .end local v0    # "address":Ljava/lang/String;
    :goto_0
    const/4 v3, 0x1

    return v3

    .line 1051
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->this$0:Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;

    # invokes: Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->moveToMarker(Lcom/google/android/maps/GeoPoint;)Z
    invoke-static {v3, v2}, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;->access$2000(Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity;Lcom/google/android/maps/GeoPoint;)Z

    goto :goto_0
.end method

.method public onTap(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Z
    .locals 3
    .param p1, "p"    # Lcom/google/android/maps/GeoPoint;
    .param p2, "mapView"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 1059
    const/4 v1, 0x0

    .line 1060
    .local v1, "ret":Z
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1062
    :try_start_0
    invoke-super {p0, p1, p2}, Lcom/google/android/maps/ItemizedOverlay;->onTap(Lcom/google/android/maps/GeoPoint;Lcom/google/android/maps/MapView;)Z
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1067
    :cond_0
    :goto_0
    return v1

    .line 1063
    :catch_0
    move-exception v0

    .line 1064
    .local v0, "oobe":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;Lcom/google/android/maps/MapView;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;
    .param p2, "mapView"    # Lcom/google/android/maps/MapView;

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1073
    const/4 v0, 0x0

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/picker/SelectMapActivity$SelectMapOverlay;->mOverlays:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
