.class public Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;
.super Ljava/lang/Object;
.source "BluetoothOppActionHelper.java"


# static fields
.field private static final ACTION_BT_OPP_TRANSFER_STARTED:Ljava/lang/String; = "android.btopp.intent.action.BT_OPP_TRANSFER_STARTED"

.field private static final ACTION_SCONNECT_HANDOVER_SEND:Ljava/lang/String; = "android.btopp.intent.action.SCONNECT_HANDOVER_SEND"

.field private static final ACTION_SCONNECT_HANDOVER_SEND_MULTIPLE:Ljava/lang/String; = "android.btopp.intent.action.SCONNECT_HANDOVER_SEND_MULTIPLE"

.field private static final ACTION_SCONNECT_HANDOVER_STOP:Ljava/lang/String; = "android.btopp.intent.action.SCONNECT_HANDOVER_STOP"

.field private static final CONTENT_URI:Landroid/net/Uri;

.field private static final DESTINATION:Ljava/lang/String; = "destination"

.field private static final DEVICE_NAME:Ljava/lang/String; = "device_name"

.field private static final MSG_OPP_SESSION_COMPLETE:Ljava/lang/String; = "android.btopp.intent.action.MSG_SESSION_COMPLETE"

.field private static final MSG_OPP_SESSION_ERROR:Ljava/lang/String; = "android.btopp.intent.action.MSG_SESSION_ERROR"

.field private static final TAG:Ljava/lang/String; = "BluetoothOppActionHelper"


# instance fields
.field private mBtOppBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

.field private mIsBtOppBroadcastReceiver:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "content://com.android.bluetooth.opp/btopp"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "appContext"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mContext:Landroid/content/Context;

    .line 195
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .line 215
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mIsBtOppBroadcastReceiver:Z

    .line 216
    new-instance v0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper$1;-><init>(Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mBtOppBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 52
    const-string v0, "BluetoothOppActionHelper"

    const-string v1, "BluetoothOppActionHelper"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mContext:Landroid/content/Context;

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->unregisterBtOppReceiver()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->isTherePendingItem(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private findOppIngDevice(Landroid/content/Context;)[Ljava/lang/String;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 135
    new-array v2, v10, [Ljava/lang/String;

    const-string v0, "destination"

    aput-object v0, v2, v8

    const-string v0, "device_name"

    aput-object v0, v2, v9

    .line 138
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "((status == \'192\') OR (status == \'190\')) AND (visibility IS NULL OR visibility == \'0\')"

    .line 139
    .local v3, "selection":Ljava/lang/String;
    const-string v5, "_id"

    .line 140
    .local v5, "order":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 143
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 144
    const-string v0, "BluetoothOppActionHelper"

    const-string v1, "findOppIngDevice"

    const-string v8, "no cursor"

    invoke-static {v0, v1, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :goto_0
    return-object v4

    .line 148
    :cond_0
    new-array v7, v10, [Ljava/lang/String;

    aput-object v4, v7, v8

    aput-object v4, v7, v9

    .line 151
    .local v7, "deviceString":[Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 152
    const-string v0, "destination"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v8

    .line 153
    const-string v0, "device_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v9

    .line 154
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 155
    const-string v0, "BluetoothOppActionHelper"

    const-string v1, "findOppIngDevice"

    aget-object v4, v7, v8

    invoke-static {v0, v1, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v7

    .line 156
    goto :goto_0

    .line 158
    :cond_1
    const-string v0, "BluetoothOppActionHelper"

    const-string v1, "findOppIngDevice"

    const-string v8, "no device"

    invoke-static {v0, v1, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private isTherePendingItem(Landroid/content/Context;)Z
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 171
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "destination"

    aput-object v0, v2, v7

    const-string v0, "device_name"

    aput-object v0, v2, v8

    .line 174
    .local v2, "projection":[Ljava/lang/String;
    const-string v3, "((status == \'192\') OR (status == \'190\')) AND (visibility IS NULL OR visibility == \'0\')"

    .line 175
    .local v3, "selection":Ljava/lang/String;
    const-string v5, "_id"

    .line 176
    .local v5, "order":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 179
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 180
    const-string v0, "BluetoothOppActionHelper"

    const-string v1, "isTherePendingItem"

    const-string v4, "no cursor"

    invoke-static {v0, v1, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    .line 191
    :goto_0
    return v0

    .line 184
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 185
    const-string v0, "BluetoothOppActionHelper"

    const-string v1, "isTherePendingItem"

    const-string v4, "oo"

    invoke-static {v0, v1, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v8

    .line 187
    goto :goto_0

    .line 189
    :cond_1
    const-string v0, "BluetoothOppActionHelper"

    const-string v1, "isTherePendingItem"

    const-string v4, "no item"

    invoke-static {v0, v1, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move v0, v7

    .line 191
    goto :goto_0
.end method

.method private registerBtOppReceiver()V
    .locals 6

    .prologue
    .line 279
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "registerBtOppReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIsBtOppBroadcastReceiver = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mIsBtOppBroadcastReceiver:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mIsBtOppBroadcastReceiver:Z

    if-nez v2, :cond_0

    .line 282
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 283
    .local v1, "oppIntentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.btopp.intent.action.BT_OPP_TRANSFER_STARTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 284
    const-string v2, "android.btopp.intent.action.MSG_SESSION_COMPLETE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 285
    const-string v2, "android.btopp.intent.action.MSG_SESSION_ERROR"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 287
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mBtOppBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 288
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mIsBtOppBroadcastReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    .end local v1    # "oppIntentFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 289
    .restart local v1    # "oppIntentFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v0

    .line 290
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "registerBtOppReceiver"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static sendFileSendIntent(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "btMacAddr"    # Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mime"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v5

    const/high16 v6, 0x100000

    invoke-virtual {v5, v6}, Landroid/bluetooth/BluetoothClass;->hasService(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 65
    const-string v5, "BluetoothOppActionHelper"

    const-string v6, "sendFileSendIntent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    .line 67
    .local v3, "testDevice":Landroid/bluetooth/BluetoothDevice;
    move-object v4, p2

    .line 68
    .local v4, "uriToPass":Landroid/net/Uri;
    move-object v1, p3

    .line 70
    .local v1, "mimeType":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 71
    .local v0, "intent":Landroid/content/Intent;
    const-string v5, "com.android.bluetooth"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const-string v5, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 74
    const-string v5, "android.btopp.intent.action.SCONNECT_HANDOVER_SEND"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const-string v5, "text/sconnect"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 76
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 77
    .local v2, "str":Ljava/lang/String;
    const-string v5, "text"

    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 78
    const/4 v5, 0x7

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 83
    :cond_0
    const-string v5, "text/plain"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 84
    const-string v5, "android.intent.extra.TEXT"

    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    .end local v2    # "str":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 93
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "mimeType":Ljava/lang/String;
    .end local v3    # "testDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v4    # "uriToPass":Landroid/net/Uri;
    :goto_1
    return-void

    .line 86
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "mimeType":Ljava/lang/String;
    .restart local v3    # "testDevice":Landroid/bluetooth/BluetoothDevice;
    .restart local v4    # "uriToPass":Landroid/net/Uri;
    :cond_1
    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0

    .line 91
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "mimeType":Ljava/lang/String;
    .end local v3    # "testDevice":Landroid/bluetooth/BluetoothDevice;
    .end local v4    # "uriToPass":Landroid/net/Uri;
    :cond_2
    const-string v5, "BluetoothOppActionHelper"

    const-string v6, "sendFileSendIntent"

    const-string v7, "remote BT device do not support OPP"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static sendMultiFileSendIntent(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "btMacAddr"    # Ljava/lang/String;
    .param p3, "mime"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 97
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v3

    const/high16 v4, 0x100000

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothClass;->hasService(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 99
    const-string v3, "BluetoothOppActionHelper"

    const-string v4, "sendMultiFileSendIntent"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    .line 101
    .local v2, "testDevice":Landroid/bluetooth/BluetoothDevice;
    move-object v1, p3

    .line 103
    .local v1, "mimeType":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 104
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "com.android.bluetooth"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 105
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 107
    const-string v3, "android.btopp.intent.action.SCONNECT_HANDOVER_SEND_MULTIPLE"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v0, v3, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 110
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 114
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "mimeType":Ljava/lang/String;
    .end local v2    # "testDevice":Landroid/bluetooth/BluetoothDevice;
    :goto_0
    return-void

    .line 112
    :cond_0
    const-string v3, "BluetoothOppActionHelper"

    const-string v4, "sendMultiFileSendIntent"

    const-string v5, "remote BT device do not support OPP"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static sendStopIntent(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "btMacAddr"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-static {p1}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v1

    .line 118
    .local v1, "testDevice":Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_0

    .line 119
    const-string v2, "BluetoothOppActionHelper"

    const-string v3, "sendStopIntent"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 121
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "com.android.bluetooth"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const-string v2, "android.btopp.intent.action.SCONNECT_HANDOVER_STOP"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 123
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 125
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 127
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private unregisterBtOppReceiver()V
    .locals 5

    .prologue
    .line 296
    const-string v1, "BluetoothOppActionHelper"

    const-string v2, "unregisterBtOppReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIsBtOppBroadcastReceiver = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mIsBtOppBroadcastReceiver:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mIsBtOppBroadcastReceiver:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 300
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mBtOppBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 301
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mIsBtOppBroadcastReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 302
    :catch_0
    move-exception v0

    .line 303
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "BluetoothOppActionHelper"

    const-string v2, "unregisterBtOppReceiver"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public setBtOppListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V
    .locals 7
    .param p1, "fileShareActionListener"    # Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 198
    const-string v1, "BluetoothOppActionHelper"

    const-string v2, "setBtOppListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fileShareActionListener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    .line 201
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    if-nez v1, :cond_0

    .line 213
    :goto_0
    return-void

    .line 204
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->findOppIngDevice(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    .line 205
    .local v0, "oppIngdevice":[Ljava/lang/String;
    if-eqz v0, :cond_1

    aget-object v1, v0, v5

    if-eqz v1, :cond_1

    aget-object v1, v0, v6

    if-eqz v1, :cond_1

    .line 206
    const-string v1, "BluetoothOppActionHelper"

    const-string v2, "setBtOppListener"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v0, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is STATUS_PROGRESS"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->mFileShareActionListener:Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;

    aget-object v2, v0, v6

    aget-object v3, v0, v5

    const-string v4, "STATUS_PROGRESS"

    invoke-interface {v1, v2, v3, v4}, Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;->stateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->registerBtOppReceiver()V

    goto :goto_0
.end method

.method public terminte()V
    .locals 3

    .prologue
    .line 57
    const-string v0, "BluetoothOppActionHelper"

    const-string v1, "terminte"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sconnect/central/action/BluetoothOppActionHelper;->setBtOppListener(Lcom/samsung/android/sconnect/central/action/IFileShareActionListener;)V

    .line 59
    return-void
.end method
