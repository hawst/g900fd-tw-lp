.class Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;
.super Ljava/lang/Object;
.source "FavoriteDeviceManager.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showFavoriteDialog(Lcom/samsung/android/sconnect/central/ui/DisplayItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

.field final synthetic val$isFavorite:Z


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;Z)V
    .locals 0

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    iput-boolean p2, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;->val$isFavorite:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 108
    packed-switch p2, :pswitch_data_0

    .line 126
    :goto_0
    return-void

    .line 110
    :pswitch_0
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;->val$isFavorite:Z

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # invokes: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->removeFavorite()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$100(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    goto :goto_0

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # invokes: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->addFavorite()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$200(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    goto :goto_0

    .line 117
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # invokes: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showIconDialog()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$300(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    goto :goto_0

    .line 120
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager$2;->this$0:Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;

    # invokes: Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->showNameDialog()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;->access$400(Lcom/samsung/android/sconnect/central/FavoriteDeviceManager;)V

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
