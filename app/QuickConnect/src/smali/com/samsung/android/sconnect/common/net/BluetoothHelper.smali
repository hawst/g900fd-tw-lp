.class public Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
.super Ljava/lang/Object;
.source "BluetoothHelper.java"

# interfaces
.implements Lcom/samsung/android/sconnect/common/net/IDiscoveryAction;


# instance fields
.field private TAG:Ljava/lang/String;

.field private mBTScanning:Z

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBluetoothAdapterReceiver:Landroid/content/BroadcastReceiver;

.field private mBondedDevices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

.field private mIsBluetoothAdapterReceiver:Z

.field private mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mRegisterIntent:Z

.field private mStopDiscovery:Landroid/os/Handler;

.field private mStopThead:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "BluetoothHelper"

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    .line 35
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    .line 37
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBondedDevices:Ljava/util/Set;

    .line 39
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .line 41
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBTScanning:Z

    .line 42
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mRegisterIntent:Z

    .line 44
    iput-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopDiscovery:Landroid/os/Handler;

    .line 136
    new-instance v0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$1;-><init>(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopThead:Ljava/lang/Runnable;

    .line 175
    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mIsBluetoothAdapterReceiver:Z

    .line 176
    new-instance v0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$2;-><init>(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapterReceiver:Landroid/content/BroadcastReceiver;

    .line 247
    new-instance v0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper$3;-><init>(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 47
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "BluetoothHelper"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    .line 49
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 50
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->registerBluetoothAdapterReceiver()V

    .line 51
    return-void
.end method

.method private GetGearData(Landroid/bluetooth/BluetoothDevice;)[B
    .locals 9
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v5, 0x0

    .line 469
    const/4 v4, 0x0

    .line 470
    .local v4, "manufacturerData":[B
    if-nez p1, :cond_0

    .line 471
    iget-object v6, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v7, "GetGearData"

    const-string v8, "Dev is NULL"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    :goto_0
    return-object v5

    .line 475
    :cond_0
    const/4 v3, 0x0

    .line 477
    .local v3, "m":Ljava/lang/reflect/Method;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "getGearManagerName"

    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/Class;

    invoke-virtual {v6, v7, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 478
    const/4 v5, 0x0

    check-cast v5, [Ljava/lang/Object;

    invoke-virtual {v3, p1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    move-object v0, v5

    check-cast v0, [B

    move-object v4, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 496
    :cond_1
    if-eqz v4, :cond_2

    .line 497
    iget-object v5, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v6, "GetGearData"

    const-string v7, "manufacturerData exists"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v5, v4

    .line 499
    goto :goto_0

    .line 479
    :catch_0
    move-exception v2

    .line 482
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 483
    .local v1, "deviceName":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 484
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 485
    const-string v5, "gear 2"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "gear 3"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "gear s"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 487
    :cond_3
    const-string v5, "17ff00750100020001010c77617463686d616e6167657200"

    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v5

    goto :goto_0

    .line 488
    :cond_4
    const-string v5, "gear fit"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 489
    const-string v5, "0eff007501000200020103776d7300"

    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v5

    goto :goto_0

    .line 490
    :cond_5
    const-string v5, "gear circle"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 491
    const-string v5, "16ff00750100020001010c77617463686d616e61676572"

    invoke-static {v5}, Lcom/samsung/android/sconnect/common/util/Util;->stringToByte(Ljava/lang/String;)[B

    move-result-object v5

    goto :goto_0
.end method

.method private GetGearDeviceID(Landroid/bluetooth/BluetoothDevice;)I
    .locals 6
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 450
    if-nez p1, :cond_1

    .line 451
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v3, "GetGearCategory"

    const-string v4, "Dev is NULL"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    :cond_0
    :goto_0
    return v0

    .line 455
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearData(Landroid/bluetooth/BluetoothDevice;)[B

    move-result-object v1

    .line 456
    .local v1, "manufacturerData":[B
    if-eqz v1, :cond_2

    array-length v2, v1

    if-lt v2, v3, :cond_2

    .line 457
    aget-byte v0, v1, v3

    .line 458
    .local v0, "deviceID":I
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v3, "GetGearDeviceID"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "deviceID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 462
    .end local v0    # "deviceID":I
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->isGear1(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 463
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private GetGearInfo(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 8
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v1, 0x0

    const/16 v5, 0xa

    .line 503
    if-nez p1, :cond_1

    .line 504
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v5, "GetGearCategory"

    const-string v6, "Dev is NULL"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 523
    :cond_0
    :goto_0
    return-object v1

    .line 508
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearData(Landroid/bluetooth/BluetoothDevice;)[B

    move-result-object v2

    .line 509
    .local v2, "manufacturerData":[B
    if-eqz v2, :cond_3

    array-length v4, v2

    if-le v4, v5, :cond_3

    .line 510
    new-instance v3, Ljava/lang/StringBuilder;

    aget-byte v4, v2, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 511
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "cnt":I
    :goto_1
    aget-byte v4, v2, v5

    if-ge v0, v4, :cond_2

    .line 512
    add-int/lit8 v4, v0, 0xb

    aget-byte v4, v2, v4

    int-to-char v4, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 511
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 514
    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 515
    .local v1, "gearInfo":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v5, "GetGearInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "gearInfo: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 519
    .end local v0    # "cnt":I
    .end local v1    # "gearInfo":Ljava/lang/String;
    .end local v3    # "stringBuilder":Ljava/lang/StringBuilder;
    :cond_3
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->isGear1(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 520
    iget-object v4, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v5, "GetGearInfo"

    const-string v6, "watchmanager"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    const-string v1, "watchmanager"

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;Landroid/bluetooth/BluetoothDevice;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearDeviceID(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearInfo(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;Landroid/bluetooth/BluetoothDevice;)[B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BluetoothHelper;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearData(Landroid/bluetooth/BluetoothDevice;)[B

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/common/net/BluetoothHelper;)Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/net/BluetoothHelper;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    return-object v0
.end method

.method private enableBluetoothAdapter()Z
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "enableBluetoothAdapter"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "enableBluetoothAdapter"

    const-string v2, "call actually"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    .line 156
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private findConnectedDevice()V
    .locals 12

    .prologue
    const/16 v5, -0x12c

    .line 351
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v3, "findConnectedDevice"

    const-string v4, " Caution: don\'t know Fit state..."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->hasConnectedDevice()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 353
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBondedDevices:Ljava/util/Set;

    if-nez v2, :cond_1

    .line 354
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v3, "findConnectedDevice"

    const-string v4, "getBondedDevices() is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBondedDevices:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/bluetooth/BluetoothDevice;

    .line 358
    .local v8, "device":Landroid/bluetooth/BluetoothDevice;
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getBluetoothActionHelper()Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->isConnected(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v10

    .line 360
    .local v10, "isConnected":Z
    if-eqz v10, :cond_2

    .line 361
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v1

    .line 362
    .local v1, "bdname":Ljava/lang/String;
    if-nez v1, :cond_3

    .line 363
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 365
    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v3, "findConnectedDevice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "[Name]"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", [Type]"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", [BondState]"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "[Address]"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "[Class]"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v6

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_4

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_7

    .line 372
    :cond_4
    new-instance v11, Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    invoke-direct {v11, v2}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;-><init>(Landroid/content/Context;)V

    .line 395
    .local v11, "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {p0, v8}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->isGearDevice(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 396
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {p0, v8}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearDeviceID(Landroid/bluetooth/BluetoothDevice;)I

    move-result v4

    invoke-direct {p0, v8}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearInfo(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v8}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearData(Landroid/bluetooth/BluetoothDevice;)[B

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ISLjava/lang/String;[B)V

    .line 399
    .local v0, "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;
    invoke-virtual {v11, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;)V

    .line 406
    .end local v0    # "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceWearable;
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDeviceList:Ljava/util/ArrayList;

    if-eqz v2, :cond_5

    .line 407
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    if-eqz v2, :cond_2

    .line 410
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    invoke-interface {v2, v11}, Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;->onDeviceAdded(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)V

    goto/16 :goto_1

    .line 401
    :cond_6
    new-instance v0, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    move-result-object v3

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    move-result v3

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/samsung/android/sconnect/common/device/net/DeviceBt;-><init>(Ljava/lang/String;Ljava/lang/String;IS)V

    .line 403
    .local v0, "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceBt;
    invoke-virtual {v11, v0}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->setDeviceInfo(Lcom/samsung/android/sconnect/common/device/net/DeviceBt;)V

    goto :goto_2

    .line 413
    .end local v0    # "deviceBt":Lcom/samsung/android/sconnect/common/device/net/DeviceBt;
    .end local v11    # "sconnDevice":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v3, "findConnectedDevice"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Don\'t add. not DEVICE_TYPE_CLASSIC - "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Landroid/bluetooth/BluetoothDevice;->getType()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 419
    .end local v1    # "bdname":Ljava/lang/String;
    .end local v8    # "device":Landroid/bluetooth/BluetoothDevice;
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "isConnected":Z
    :cond_8
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v3, "findConnectedDevice"

    const-string v4, "Don\'t have connected device"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private getBondedDevice()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBondedDevices:Ljava/util/Set;

    .line 173
    :cond_0
    return-void
.end method

.method private hasConnectedDevice()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 341
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getConnectionState()I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getConnectionState()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 343
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v2, "hasConnectedDevice"

    const-string v3, "true"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :goto_0
    return v0

    .line 346
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "hasConnectedDevice"

    const-string v2, "false"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isGear1(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 2
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    .line 532
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    :cond_0
    const/4 v0, 0x0

    .line 535
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GALAXY Gear ("

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private registerBluetoothAdapterReceiver()V
    .locals 6

    .prologue
    .line 197
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v3, "registerBluetoothAdapterReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mIsBluetoothAdapterReceiver = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mIsBluetoothAdapterReceiver:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mIsBluetoothAdapterReceiver:Z

    if-nez v2, :cond_0

    .line 200
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 201
    .local v1, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 203
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapterReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 204
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mIsBluetoothAdapterReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    .end local v1    # "intentFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 205
    .restart local v1    # "intentFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v0

    .line 206
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v3, "registerBluetoothAdapterReceiver"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private setIntentFilter()V
    .locals 3

    .prologue
    .line 225
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mRegisterIntent:Z

    if-nez v1, :cond_0

    .line 226
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 227
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.device.action.FOUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 231
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    .line 232
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 233
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mRegisterIntent:Z

    .line 236
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private unregisterBluetoothAdapterReceiver()V
    .locals 5

    .prologue
    .line 212
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v2, "unregisterBluetoothAdapterReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mIsBluetoothAdapterReceiver = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mIsBluetoothAdapterReceiver:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mIsBluetoothAdapterReceiver:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 216
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapterReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 217
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mIsBluetoothAdapterReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v2, "unregisterBluetoothAdapterReceiver"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private unregisterIntent()V
    .locals 2

    .prologue
    .line 239
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mRegisterIntent:Z

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 242
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mRegisterIntent:Z

    .line 245
    :cond_0
    return-void
.end method


# virtual methods
.method public checkEnabledNetwork()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x1

    .line 133
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public disableBluetoothAdapter()Z
    .locals 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "disableBluetoothAdapter"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->hasConnectedDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "disableBluetoothAdapter"

    const-string v2, "call actually"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    move-result v0

    .line 165
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected finalize()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "finalize"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->unregisterBluetoothAdapterReceiver()V

    .line 57
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 58
    return-void
.end method

.method public getBluetoothAdapterState()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v0

    .line 147
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getDeviceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDeviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public initiate(Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "initiate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .line 63
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->enableBluetoothAdapter()Z

    .line 64
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->setIntentFilter()V

    .line 66
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopDiscovery:Landroid/os/Handler;

    .line 67
    return-void
.end method

.method public isGear1(Ljava/lang/String;)Z
    .locals 2
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 527
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 528
    .local v0, "dev":Landroid/bluetooth/BluetoothDevice;
    invoke-direct {p0, v0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->isGear1(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    return v1
.end method

.method public isGearDevice(Landroid/bluetooth/BluetoothDevice;)Z
    .locals 4
    .param p1, "dev"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/4 v0, 0x0

    .line 424
    if-nez p1, :cond_1

    .line 425
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v2, "isGearDevice"

    const-string v3, "Dev is NULL"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_0
    :goto_0
    return v0

    .line 429
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->GetGearData(Landroid/bluetooth/BluetoothDevice;)[B

    move-result-object v1

    if-nez v1, :cond_2

    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->isGear1(Landroid/bluetooth/BluetoothDevice;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 430
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isGearDevice(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Z
    .locals 4
    .param p1, "dev"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    .prologue
    const/4 v0, 0x0

    .line 437
    if-nez p1, :cond_1

    .line 438
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v2, "isGearDevice"

    const-string v3, "Dev is NULL"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    :cond_0
    :goto_0
    return v0

    .line 442
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableData()[B

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getWearableInfo()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 443
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "isGearDevice"

    const-string v2, "Dev has wearable info"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public startDiscovery(Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "startDiscovery"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sconnect/central/action/BluetoothActionHelper;->mState:I

    .line 85
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopDiscovery:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopDiscovery:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopThead:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 89
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDeviceList:Ljava/util/ArrayList;

    .line 90
    iput-object p1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 91
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBTScanning:Z

    .line 92
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_2

    .line 93
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "startDiscovery"

    const-string v2, "call startDiscovery()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->startDiscovery()Z

    .line 95
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopDiscovery:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopDiscovery:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopThead:Ljava/lang/Runnable;

    const-wide/16 v2, 0x32c8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 98
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->getBondedDevice()V

    .line 99
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->findConnectedDevice()V

    .line 101
    :cond_2
    return-void
.end method

.method public stopDiscovery()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 105
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "stopDiscovery"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopDiscovery:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopDiscovery:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mStopThead:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 111
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBTScanning:Z

    if-eqz v0, :cond_2

    .line 112
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "stopDiscovery"

    const-string v2, "call cancelDiscovery()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    .line 116
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBTScanning:Z

    .line 119
    :cond_2
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBondedDevices:Ljava/util/Set;

    .line 120
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 121
    return-void
.end method

.method public terminate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 70
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->TAG:Ljava/lang/String;

    const-string v1, "terminate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->stopDiscovery()V

    .line 72
    invoke-direct {p0}, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->unregisterIntent()V

    .line 74
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mBondedDevices:Ljava/util/Set;

    .line 75
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDiscoveryListener:Lcom/samsung/android/sconnect/common/net/SconnListener$IDeviceDiscoveryListener;

    .line 76
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mDeviceList:Ljava/util/ArrayList;

    .line 77
    iput-object v3, p0, Lcom/samsung/android/sconnect/common/net/BluetoothHelper;->mNetworkStateListener:Lcom/samsung/android/sconnect/common/net/SconnListener$INetworkStateListener;

    .line 78
    return-void
.end method
