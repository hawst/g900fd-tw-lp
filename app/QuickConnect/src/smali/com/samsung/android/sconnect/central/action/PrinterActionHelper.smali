.class public Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;
.super Ljava/lang/Object;
.source "PrinterActionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;,
        Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintDocumentAdapter;
    }
.end annotation


# static fields
.field private static final ACTION_PRINTER_SELECTED:Ljava/lang/String; = "com.sec.android.app.mobileprint.ACTION_PRINTER_SELECTED"

.field private static final ACTION_SELECTED_PRINTER_SAVED:Ljava/lang/String; = "com.android.printspooler.ACTION_SELECTED_PRINTER_SAVED"

.field private static final EXTRA_SELECTED_PRINTER:Ljava/lang/String; = "selected_printer"

.field private static final PREFIX_CONTENT_URI:Ljava/lang/String; = "content://"

.field private static final PREFIX_FILE_URI:Ljava/lang/String; = "file://"

.field private static final TAG:Ljava/lang/String; = "PrinterActionHelper"


# instance fields
.field private isBroadcastReceiver:Z

.field private mActivityContext:Landroid/content/Context;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContentsType:I

.field private mContext:Landroid/content/Context;

.field private final mLocalReceiver:Landroid/content/BroadcastReceiver;

.field private mUris:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContext:Landroid/content/Context;

    .line 51
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mActivityContext:Landroid/content/Context;

    .line 55
    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mUris:Ljava/util/ArrayList;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->isBroadcastReceiver:Z

    .line 507
    new-instance v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$1;-><init>(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 534
    new-instance v0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$2;-><init>(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)V

    iput-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    .line 68
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContext:Landroid/content/Context;

    .line 69
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->setIntentFilter()V

    .line 70
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->releaseBroadcastReceiver()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mUris:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->getFilePathFromUri(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContentsType:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->printImage(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->printPdf(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Landroid/print/PrinterInfo;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;
    .param p1, "x1"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p2, "x2"    # Ljava/util/ArrayList;
    .param p3, "x3"    # Landroid/print/PrinterInfo;

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->startPluginPrint(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Landroid/print/PrinterInfo;)Z

    move-result v0

    return v0
.end method

.method private getFilePathFromUri(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    .local p1, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 162
    .local v2, "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 164
    .local v3, "uri":Landroid/net/Uri;
    const/4 v1, 0x0

    .line 166
    .local v1, "path":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/sconnect/common/util/Util;->isPdfContents(Ljava/util/ArrayList;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 168
    invoke-virtual {v3}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 169
    invoke-virtual {v3}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file://"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 184
    :cond_0
    :goto_1
    if-eqz v1, :cond_5

    .line 185
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 171
    :cond_1
    iget v4, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContentsType:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 173
    invoke-virtual {v3}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 174
    invoke-virtual {v3}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file://"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 175
    :cond_2
    invoke-virtual {v3}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "content://"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 176
    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->getImageFilePathFromURI(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 178
    :cond_3
    const-string v4, "PrinterActionHelper"

    const-string v5, "getFilePathFromUri"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "WRONG IMAGE URI:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v3}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 181
    :cond_4
    const-string v4, "PrinterActionHelper"

    const-string v5, "getFilePathFromUri"

    const-string v6, "NONE Support mimetype"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 187
    :cond_5
    const-string v4, "PrinterActionHelper"

    const-string v5, "getFilePathFromUri"

    const-string v6, "path is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 190
    .end local v1    # "path":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_6
    return-object v2
.end method

.method private getImageFilePathFromURI(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "contentUri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x0

    .line 194
    if-nez p2, :cond_1

    .line 195
    const-string v0, "PrinterActionHelper"

    const-string v1, "getRealFileNameFromURI"

    const-string v3, "contentUri is null"

    invoke-static {v0, v1, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_0
    :goto_0
    return-object v9

    .line 199
    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v9

    .line 201
    .local v9, "uri":Ljava/lang/String;
    const-string v0, "content://"

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    const/4 v7, 0x0

    .line 204
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 207
    .local v2, "proj":[Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 208
    if-eqz v7, :cond_2

    .line 209
    const-string v0, "_data"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    .line 210
    .local v6, "column_index":I
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 211
    invoke-interface {v7, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 217
    .end local v6    # "column_index":I
    :cond_2
    if-eqz v7, :cond_0

    .line 218
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 213
    .end local v2    # "proj":[Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 214
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "PrinterActionHelper"

    const-string v1, "getRealFileNameFromURI"

    const-string v3, "Exception"

    invoke-static {v0, v1, v3, v8}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 215
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 217
    if-eqz v7, :cond_0

    .line 218
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 217
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_3

    .line 218
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private printImage(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 240
    .local p1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "PrinterActionHelper"

    const-string v3, "printImage"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 243
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mActivityContext:Landroid/content/Context;

    if-eqz v2, :cond_1

    .line 244
    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mActivityContext:Landroid/content/Context;

    const-string v3, "print"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrintManager;

    .line 246
    .local v1, "printManager":Landroid/print/PrintManager;
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v3, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;

    iget-object v4, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mActivityContext:Landroid/content/Context;

    invoke-direct {v3, p1, v4}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintImageAdapter;-><init>(Ljava/util/ArrayList;Landroid/content/Context;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    .line 257
    .end local v1    # "printManager":Landroid/print/PrintManager;
    :cond_0
    :goto_0
    return-void

    .line 249
    :cond_1
    const-string v2, "PrinterActionHelper"

    const-string v3, "printBitmap"

    const-string v4, "mActivityContext is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "PrinterActionHelper"

    const-string v3, "printImage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private printPdf(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    .local p1, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v3, "PrinterActionHelper"

    const-string v4, "printPdf"

    const-string v5, ""

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mActivityContext:Landroid/content/Context;

    if-eqz v3, :cond_0

    .line 229
    iget-object v3, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mActivityContext:Landroid/content/Context;

    const-string v4, "print"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/print/PrintManager;

    .line 231
    .local v2, "printManager":Landroid/print/PrintManager;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 232
    .local v1, "path":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintDocumentAdapter;

    invoke-direct {v3, v1}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper$MyPrintDocumentAdapter;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    goto :goto_0

    .line 235
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "printManager":Landroid/print/PrintManager;
    :cond_0
    const-string v3, "PrinterActionHelper"

    const-string v4, "printPdf"

    const-string v5, "mActivityContext is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_1
    return-void
.end method

.method private releaseBroadcastReceiver()V
    .locals 2

    .prologue
    .line 503
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 504
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->isBroadcastReceiver:Z

    .line 505
    return-void
.end method

.method private setBroadcastReceiver()V
    .locals 4

    .prologue
    .line 497
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.android.printspooler.ACTION_SELECTED_PRINTER_SAVED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 499
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->isBroadcastReceiver:Z

    .line 500
    return-void
.end method

.method private setIntentFilter()V
    .locals 3

    .prologue
    .line 152
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 153
    .local v0, "localIntentFilter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.sconnect.central.PLUGIN_PRINTER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 154
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/support/v4/content/LocalBroadcastManager;->getInstance(Landroid/content/Context;)Landroid/support/v4/content/LocalBroadcastManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mLocalReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/content/LocalBroadcastManager;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 156
    return-void
.end method

.method private startPluginPrint(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Landroid/print/PrinterInfo;)Z
    .locals 6
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .param p3, "printer"    # Landroid/print/PrinterInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/print/PrinterInfo;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    .line 118
    const-string v2, "PrinterActionHelper"

    const-string v3, "startPluginPrint"

    const-string v4, ""

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getDevicePlugin()Lcom/samsung/android/sconnect/common/device/net/DevicePlugin;

    move-result-object v2

    if-nez v2, :cond_1

    .line 120
    :cond_0
    const-string v2, "PrinterActionHelper"

    const-string v3, "startPluginPrint"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FAILED:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :goto_0
    return v1

    .line 124
    :cond_1
    const-string v2, "PrinterActionHelper"

    const-string v3, "startPluginPrint"

    invoke-virtual {p1}, Lcom/samsung/android/sconnect/common/device/SconnectDevice;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 126
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toSafeString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/sconnect/common/util/MimeTypeExtractor;->getContentsType(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContentsType:I

    .line 127
    iput-object p2, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mUris:Ljava/util/ArrayList;

    .line 133
    iget-boolean v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->isBroadcastReceiver:Z

    if-nez v1, :cond_2

    .line 134
    invoke-direct {p0}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->setBroadcastReceiver()V

    .line 137
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.mobileprint.ACTION_PRINTER_SELECTED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 138
    .local v0, "printerIntent":Landroid/content/Intent;
    const-string v1, "selected_printer"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 139
    iget-object v1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 141
    const-string v1, "PrinterActionHelper"

    const-string v2, "startPluginPrint"

    const-string v3, "Broadcast Success!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const/4 v1, 0x1

    goto :goto_0

    .line 129
    .end local v0    # "printerIntent":Landroid/content/Intent;
    :cond_3
    const-string v2, "PrinterActionHelper"

    const-string v3, "startPluginPrint"

    const-string v4, "uri is empty"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public setPrinterContext(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 147
    const-string v0, "PrinterActionHelper"

    const-string v1, "setPrinterContext"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->mActivityContext:Landroid/content/Context;

    .line 149
    return-void
.end method

.method public startPrint(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "device"    # Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/sconnect/common/device/SconnectDevice;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v6, 0x0

    .line 73
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 74
    :cond_0
    const-string v3, "PrinterActionHelper"

    const-string v4, "startPrint"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid Param:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :goto_0
    return-void

    .line 78
    :cond_1
    invoke-static {}, Lcom/samsung/android/sconnect/SconnectManager;->getInstance()Lcom/samsung/android/sconnect/SconnectManager;

    move-result-object v2

    .line 80
    .local v2, "sconnectManager":Lcom/samsung/android/sconnect/SconnectManager;
    if-eqz v2, :cond_7

    .line 81
    invoke-virtual {v2}, Lcom/samsung/android/sconnect/SconnectManager;->getPrintPluginHelper()Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;

    move-result-object v1

    .line 83
    .local v1, "pluginHelper":Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    if-eqz v1, :cond_6

    .line 85
    invoke-static {p2}, Lcom/samsung/android/sconnect/common/util/Util;->isImageContents(Ljava/util/ArrayList;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 86
    const-string v3, "PrinterActionHelper"

    const-string v4, "startPrint"

    const-string v5, "Image: print by plugin"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    invoke-virtual {v2, v6}, Lcom/samsung/android/sconnect/SconnectManager;->setEnabledWifi(Z)V

    .line 89
    invoke-virtual {v1, p1}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->getPrinterInfo(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Landroid/print/PrinterInfo;

    move-result-object v0

    .line 90
    .local v0, "item":Landroid/print/PrinterInfo;
    if-eqz v0, :cond_2

    .line 91
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->startPluginPrint(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Landroid/print/PrinterInfo;)Z

    goto :goto_0

    .line 93
    :cond_2
    const-string v3, "PrinterActionHelper"

    const-string v4, "startPluginPrint"

    const-string v5, "item is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    .end local v0    # "item":Landroid/print/PrinterInfo;
    :cond_3
    invoke-static {p2}, Lcom/samsung/android/sconnect/common/util/Util;->isPdfContents(Ljava/util/ArrayList;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 96
    const-string v3, "PrinterActionHelper"

    const-string v4, "startPrint"

    const-string v5, "PDF: print by plugin"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-virtual {v2, v6}, Lcom/samsung/android/sconnect/SconnectManager;->setEnabledWifi(Z)V

    .line 99
    invoke-virtual {v1, p1}, Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;->getPrinterInfo(Lcom/samsung/android/sconnect/common/device/SconnectDevice;)Landroid/print/PrinterInfo;

    move-result-object v0

    .line 100
    .restart local v0    # "item":Landroid/print/PrinterInfo;
    if-eqz v0, :cond_4

    .line 101
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/sconnect/central/action/PrinterActionHelper;->startPluginPrint(Lcom/samsung/android/sconnect/common/device/SconnectDevice;Ljava/util/ArrayList;Landroid/print/PrinterInfo;)Z

    goto :goto_0

    .line 103
    :cond_4
    const-string v3, "PrinterActionHelper"

    const-string v4, "startPluginPrint"

    const-string v5, "item is null"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    .end local v0    # "item":Landroid/print/PrinterInfo;
    :cond_5
    const-string v3, "PrinterActionHelper"

    const-string v4, "startPrint"

    const-string v5, "Invalid URIs"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 109
    :cond_6
    const-string v3, "PrinterActionHelper"

    const-string v4, "startPrint"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid Helper:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 112
    .end local v1    # "pluginHelper":Lcom/samsung/android/sconnect/common/net/PrintPluginHelper;
    :cond_7
    const-string v3, "PrinterActionHelper"

    const-string v4, "startPrint"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid sconnectManager:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/sconnect/common/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
