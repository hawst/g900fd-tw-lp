.class Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;
.super Ljava/lang/Object;
.source "SconnectDisplay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;


# direct methods
.method constructor <init>(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;)V
    .locals 0

    .prologue
    .line 2384
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2387
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2388
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2389
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # invokes: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->showSendErrorDialog()V
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)V

    .line 2390
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->refreshDiscoveryDevice()V

    .line 2410
    :goto_0
    return-void

    .line 2392
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2393
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v3, v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedItem:Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1100(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v1

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v3, v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    const-string v4, ""

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v5, v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContentsType:I
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Lcom/samsung/android/sconnect/common/device/SconnectDevice;ILjava/util/ArrayList;Ljava/lang/String;I)Z

    .line 2408
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    invoke-virtual {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->finish()V

    goto :goto_0

    .line 2396
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2397
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2398
    .local v1, "deviceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sconnect/common/device/SconnectDevice;>;"
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mSelectedDeviceList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$2400(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/sconnect/central/ui/DisplayItem;

    .line 2399
    .local v8, "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    invoke-virtual {v8}, Lcom/samsung/android/sconnect/central/ui/DisplayItem;->getDevice()Lcom/samsung/android/sconnect/common/device/SconnectDevice;

    move-result-object v6

    .line 2400
    .local v6, "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2402
    .end local v6    # "device":Lcom/samsung/android/sconnect/common/device/SconnectDevice;
    .end local v8    # "item":Lcom/samsung/android/sconnect/central/ui/DisplayItem;
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 2403
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v0, v0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mCentralManager:Lcom/samsung/android/sconnect/central/CentralManager;
    invoke-static {v0}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$1200(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Lcom/samsung/android/sconnect/central/CentralManager;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v3, v3, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContents:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$4800(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)Ljava/util/ArrayList;

    move-result-object v3

    const-string v4, ""

    iget-object v5, p0, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18$1;->this$1:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;

    iget-object v5, v5, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay$18;->this$0:Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;

    # getter for: Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->mPickedContentsType:I
    invoke-static {v5}, Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;->access$5000(Lcom/samsung/android/sconnect/central/ui/SconnectDisplay;)I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sconnect/central/CentralManager;->doAction(Ljava/util/ArrayList;ILjava/util/ArrayList;Ljava/lang/String;I)V

    goto :goto_1
.end method
