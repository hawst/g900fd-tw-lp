.class final Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile$HeadsetServiceListener;
.super Ljava/lang/Object;
.source "HeadsetProfile.java"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HeadsetServiceListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile$HeadsetServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;
    .param p2, "x1"    # Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile$1;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile$HeadsetServiceListener;-><init>(Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;)V

    return-void
.end method


# virtual methods
.method public onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3
    .param p1, "profile"    # I
    .param p2, "proxy"    # Landroid/bluetooth/BluetoothProfile;

    .prologue
    .line 55
    # getter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->V:Z
    invoke-static {}, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "HeadsetProfile"

    const-string v1, "onServiceConnected"

    const-string v2, "Bluetooth service connected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile$HeadsetServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    .end local p2    # "proxy":Landroid/bluetooth/BluetoothProfile;
    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->mService:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v0, p2}, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->access$102(Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 59
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile$HeadsetServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->mIsProfileReady:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->access$202(Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;Z)Z

    .line 60
    return-void
.end method

.method public onServiceDisconnected(I)V
    .locals 3
    .param p1, "profile"    # I

    .prologue
    .line 64
    # getter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->V:Z
    invoke-static {}, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "HeadsetProfile"

    const-string v1, "onServiceDisconnected"

    const-string v2, "Bluetooth service disconnected"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sconnect/common/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile$HeadsetServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->mIsProfileReady:Z
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->access$202(Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;Z)Z

    .line 68
    iget-object v0, p0, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile$HeadsetServiceListener;->this$0:Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->mService:Landroid/bluetooth/BluetoothHeadset;
    invoke-static {v0, v1}, Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;->access$102(Lcom/samsung/android/sconnect/central/action/BtProfile/HeadsetProfile;Landroid/bluetooth/BluetoothHeadset;)Landroid/bluetooth/BluetoothHeadset;

    .line 69
    return-void
.end method
