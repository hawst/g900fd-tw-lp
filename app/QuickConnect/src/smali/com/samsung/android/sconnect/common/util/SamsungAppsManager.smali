.class public Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;
.super Ljava/lang/Object;
.source "SamsungAppsManager.java"


# static fields
.field public static final DOWNLOADABLE_NAME:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final DOWNLOADABLE_SVC:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final DOWNLOADABLE_VERSION_CODE:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final PACKAGE_NAME_EMEETING:Ljava/lang/String; = "com.sec.android.emeeting"

.field public static final PACKAGE_NAME_EMEETING_B2C:Ljava/lang/String; = "com.sec.android.emeeting.b2c.hancom"

.field public static final PACKAGE_NAME_GROUP_PLAY:Ljava/lang/String; = "com.samsung.groupcast"

.field public static final PACKAGE_NAME_HOME_SYNC:Ljava/lang/String; = "com.sec.android.homesync"

.field public static final PACKAGE_NAME_SAMSUNG_LINK:Ljava/lang/String; = "com.sec.pcw"

.field public static final PACKAGE_NAME_SIDESYNC:Ljava/lang/String; = "com.sec.android.sidesync30"

.field public static final PACKAGE_NAME_SMART_REMOTE:Ljava/lang/String; = "tv.peel.smartremote.for.note4"

.field public static final PACKAGE_NAME_TOGETHER:Ljava/lang/String; = "com.sec.android.app.alltogether"

.field public static final PACKAGE_NAME_WATCH_ON:Ljava/lang/String; = "com.sec.watchon.phone"

.field public static final PACKAGE_NAME_WATCH_ON_FOR_TAB:Ljava/lang/String; = "com.sec.yosemite.tab"

.field public static final PREF_DOWNLOADABLE:Ljava/lang/String; = "downloadable_status"

.field private static final TAG:Ljava/lang/String; = "SamsungAppsManager"

.field private static isProfileShareAvailable:Z

.field private static isSettingsDbRead:Z

.field private static mInstance:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    new-instance v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$1;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$1;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->DOWNLOADABLE_VERSION_CODE:Ljava/util/HashMap;

    .line 65
    new-instance v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$2;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$2;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->DOWNLOADABLE_NAME:Ljava/util/HashMap;

    .line 83
    new-instance v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$3;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$3;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->DOWNLOADABLE_SVC:Ljava/util/HashMap;

    .line 211
    sput-boolean v1, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isSettingsDbRead:Z

    .line 212
    sput-boolean v1, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isProfileShareAvailable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->DOWNLOADABLE_VERSION_CODE:Ljava/util/HashMap;

    return-object v0
.end method

.method public static getManager(Landroid/content/Context;)Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 106
    sget-object v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mInstance:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    invoke-direct {v0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;-><init>()V

    sput-object v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mInstance:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    .line 109
    :cond_0
    if-eqz p0, :cond_1

    .line 110
    sget-object v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mInstance:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    iput-object p0, v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mContext:Landroid/content/Context;

    .line 112
    :cond_1
    sget-object v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mInstance:Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;

    return-object v0
.end method

.method public static isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 195
    invoke-static {p0, p1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppDownloadable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isAppDownloadable(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 189
    const-string v1, "downloadable_status"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 191
    .local v0, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method public static isCompatibleAppInstalled(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 168
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 172
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v6, 0x1

    :try_start_0
    invoke-virtual {v3, p1, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 178
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    sget-object v6, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->DOWNLOADABLE_VERSION_CODE:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 179
    .local v0, "compatibleVersionCode":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    iget v6, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-lt v6, v7, :cond_0

    .line 185
    .end local v0    # "compatibleVersionCode":Ljava/lang/Integer;
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return v4

    .line 173
    :catch_0
    move-exception v1

    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    move v4, v5

    .line 175
    goto :goto_0

    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v0    # "compatibleVersionCode":Ljava/lang/Integer;
    .restart local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_0
    move v4, v5

    .line 185
    goto :goto_0
.end method

.method public static isProfileShareAvailable(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 215
    sget-boolean v2, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isSettingsDbRead:Z

    if-nez v2, :cond_1

    .line 216
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "wifi_share_profile"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    :cond_0
    sput-boolean v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isProfileShareAvailable:Z

    .line 218
    sput-boolean v1, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isSettingsDbRead:Z

    .line 221
    :cond_1
    sget-boolean v0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isProfileShareAvailable:Z

    return v0
.end method

.method public static isTvControllerAvailable(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 208
    invoke-static {p0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isWatchOnAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "tv.peel.smartremote.for.note4"

    invoke-static {p0, v0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWatchOnAvailable(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 200
    invoke-static {}, Lcom/samsung/android/sconnect/common/util/FeatureUtil;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    const-string v0, "com.sec.yosemite.tab"

    invoke-static {p0, v0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 203
    :goto_0
    return v0

    :cond_0
    const-string v0, "com.sec.watchon.phone"

    invoke-static {p0, v0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->isAppAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public checkDownloadables()V
    .locals 4

    .prologue
    .line 116
    const-string v1, "SamsungAppsManager"

    const-string v2, "checkDownloadables"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/sconnect/common/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/sconnect/common/util/SamsungAppsChecker;->setContext(Landroid/content/Context;)V

    .line 119
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sconnect/common/util/SamsungAppsManager$4;-><init>(Lcom/samsung/android/sconnect/common/util/SamsungAppsManager;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 158
    .local v0, "checkDownloadableThread":Ljava/lang/Thread;
    const-string v1, "checkDownloadableThread"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 159
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 160
    return-void
.end method
