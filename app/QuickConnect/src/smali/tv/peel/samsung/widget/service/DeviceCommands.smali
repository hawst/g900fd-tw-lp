.class public Ltv/peel/samsung/widget/service/DeviceCommands;
.super Ljava/lang/Object;
.source "DeviceCommands.java"


# static fields
.field public static final ACTIVE:Ljava/lang/String; = "Active"

.field public static final ADVANCE:Ljava/lang/String; = "advance"

.field public static final AUDIO:Ljava/lang/String; = "Audio"

.field public static final AdaptRmCorr:Ljava/lang/String; = "AdaptRmCorr"

.field public static final Angle:Ljava/lang/String; = "Angle"

.field public static final Antenna:Ljava/lang/String; = "Antenna"

.field public static final Aspect:Ljava/lang/String; = "Aspect"

.field public static final BACK:Ljava/lang/String; = "Back"

.field public static final BACKSPACE:Ljava/lang/String; = "Backspace"

.field public static final BLUE:Ljava/lang/String; = "Blue"

.field public static final BROWSER:Ljava/lang/String; = "Browser"

.field public static final BS:Ljava/lang/String; = "BS"

.field public static final BS1:Ljava/lang/String; = "BS1"

.field public static final BS10:Ljava/lang/String; = "BS10"

.field public static final BS11:Ljava/lang/String; = "BS11"

.field public static final BS12:Ljava/lang/String; = "BS12"

.field public static final BS2:Ljava/lang/String; = "BS2"

.field public static final BS3:Ljava/lang/String; = "BS3"

.field public static final BS4:Ljava/lang/String; = "BS4"

.field public static final BS5:Ljava/lang/String; = "BS5"

.field public static final BS6:Ljava/lang/String; = "BS6"

.field public static final BS7:Ljava/lang/String; = "BS7"

.field public static final BS8:Ljava/lang/String; = "BS8"

.field public static final BS9:Ljava/lang/String; = "BS9"

.field public static final CHANNEL_DOWN:Ljava/lang/String; = "Channel_Down"

.field public static final CHANNEL_UP:Ljava/lang/String; = "Channel_Up"

.field public static final CHLIST:Ljava/lang/String; = "CHList"

.field public static final CLEAR:Ljava/lang/String; = "clear"

.field public static final CS:Ljava/lang/String; = "CS"

.field public static final CS1:Ljava/lang/String; = "CS1"

.field public static final CS10:Ljava/lang/String; = "CS10"

.field public static final CS11:Ljava/lang/String; = "CS11"

.field public static final CS12:Ljava/lang/String; = "CS12"

.field public static final CS2:Ljava/lang/String; = "CS2"

.field public static final CS3:Ljava/lang/String; = "CS3"

.field public static final CS4:Ljava/lang/String; = "CS4"

.field public static final CS5:Ljava/lang/String; = "CS5"

.field public static final CS6:Ljava/lang/String; = "CS6"

.field public static final CS7:Ljava/lang/String; = "CS7"

.field public static final CS8:Ljava/lang/String; = "CS8"

.field public static final CS9:Ljava/lang/String; = "CS9"

.field public static final DATA:Ljava/lang/String; = "Data"

.field public static final DELAY:Ljava/lang/String; = "Delay"

.field public static final DELAY_20000:Ljava/lang/String; = "1,12000,1,12000,1,10600"

.field public static final DIGITS3:Ljava/lang/String; = "---"

.field public static final DISPLAY:Ljava/lang/String; = "Display"

.field public static final DOT:Ljava/lang/String; = "."

.field public static final DOT_DASH:Ljava/lang/String; = "Dot_DASh"

.field public static final DVD:Ljava/lang/String; = "DVD"

.field public static final DVDInput:Ljava/lang/String; = "DVDInput"

.field public static final DVR:Ljava/lang/String; = "DVR"

.field public static final EARLYDIGITS3:Ljava/lang/String; = "EarlyDigits3"

.field public static final EIGHT:Ljava/lang/String; = "8"

.field public static final ELEVEN:Ljava/lang/String; = "11"

.field public static final ENTER:Ljava/lang/String; = "Enter"

.field public static final EXIT:Ljava/lang/String; = "Exit"

.field public static final EarlyDigits:Ljava/lang/String; = "EarlyDigits3"

.field public static final Eject:Ljava/lang/String; = "Eject"

.field public static final FAN_AUTO:Ljava/lang/String; = "FAN_AUTO"

.field public static final FAN_HIGH:Ljava/lang/String; = "FAN_HIGH"

.field public static final FAN_LOW:Ljava/lang/String; = "FAN_LOW"

.field public static final FAN_MED:Ljava/lang/String; = "FAN_MED"

.field public static final FAST_FORWARD:Ljava/lang/String; = "Fast_Forward"

.field public static final FAVORITES:Ljava/lang/String; = "Favorites"

.field public static final FIVE:Ljava/lang/String; = "5"

.field public static final FLASH_BACK:Ljava/lang/String; = "FLASH BACK"

.field public static final FORMAT:Ljava/lang/String; = "format"

.field public static final FOUR:Ljava/lang/String; = "4"

.field public static final FrameAdvance:Ljava/lang/String; = "FrameAdvance"

.field public static final GAME:Ljava/lang/String; = "Game"

.field public static final GREEN:Ljava/lang/String; = "Green"

.field public static final GUIDE:Ljava/lang/String; = "Guide"

.field public static final Greater:Ljava/lang/String; = "Greater10"

.field public static final HOME:Ljava/lang/String; = "Home"

.field public static final ILINK:Ljava/lang/String; = "ILINK"

.field public static final INFO:Ljava/lang/String; = "Info"

.field public static final INPUT:Ljava/lang/String; = "Input"

.field public static final INPUT_COMPONENT1:Ljava/lang/String; = "Component1"

.field public static final INPUT_COMPONENT2:Ljava/lang/String; = "Component2"

.field public static final In_use:Ljava/lang/String; = "In use"

.field public static final InputAdapterPort:Ljava/lang/String; = "InputAdapterPort"

.field public static final InputHdmi:Ljava/lang/String; = "InputHdmi"

.field public static final InputHomeMedia:Ljava/lang/String; = "InputHomeMedia"

.field public static final JUMP:Ljava/lang/String; = "JUMP"

.field public static final LANDLINE_A:Ljava/lang/String; = "LLA"

.field public static final LANDLINE_D:Ljava/lang/String; = "LLD"

.field public static final LAST:Ljava/lang/String; = "Last"

.field public static final LIST:Ljava/lang/String; = "list"

.field public static final LIVETV:Ljava/lang/String; = "LiveTV"

.field public static final Language:Ljava/lang/String; = "Language"

.field public static final MARK:Ljava/lang/String; = "MARK"

.field public static final MENU:Ljava/lang/String; = "Menu"

.field public static final MODE_AUTO:Ljava/lang/String; = "Mode_Auto"

.field public static final MODE_COOL:Ljava/lang/String; = "Mode_Cool"

.field public static final MODE_DRY:Ljava/lang/String; = "Mode_Dry"

.field public static final MODE_FAN:Ljava/lang/String; = "Mode_Fan"

.field public static final MODE_HEAT:Ljava/lang/String; = "Mode_Heat"

.field public static final MUTE:Ljava/lang/String; = "Mute"

.field public static final Marker:Ljava/lang/String; = "Marker"

.field public static final Media:Ljava/lang/String; = "Media"

.field public static final NAVIGATE_DOWN:Ljava/lang/String; = "Navigate_Down"

.field public static final NAVIGATE_LEFT:Ljava/lang/String; = "Navigate_Left"

.field public static final NAVIGATE_RIGHT:Ljava/lang/String; = "Navigate_Right"

.field public static final NAVIGATE_RIGHT_OR_FORWARD:Ljava/lang/String; = "Right/>>"

.field public static final NAVIGATE_UP:Ljava/lang/String; = "Navigate_Up"

.field public static final NETFLIX:Ljava/lang/String; = "Netflix"

.field public static final NEW:Ljava/lang/String; = "New"

.field public static final NEXT:Ljava/lang/String; = "Next"

.field public static final NINE:Ljava/lang/String; = "9"

.field public static final ONDEMAND:Ljava/lang/String; = "OnDemand"

.field public static final ONE:Ljava/lang/String; = "1"

.field public static final OPTIONS:Ljava/lang/String; = "Options"

.field public static final PAUSE:Ljava/lang/String; = "Pause"

.field public static final PLAY:Ljava/lang/String; = "Play"

.field public static final PLAYLIST:Ljava/lang/String; = "Playlist"

.field public static final POPMENU:Ljava/lang/String; = "PopMenu"

.field public static final POWER:Ljava/lang/String; = "Power"

.field public static final POWEROFF:Ljava/lang/String; = "PowerOff"

.field public static final POWERON:Ljava/lang/String; = "PowerOn"

.field public static final PR:Ljava/lang/String; = "Pr"

.field public static final PREV:Ljava/lang/String; = "prev"

.field public static final PREVIOUS:Ljava/lang/String; = "Previous"

.field public static final PREVIOUS_CHANNEL:Ljava/lang/String; = "PreviousCh"

.field public static final Period:Ljava/lang/String; = "Period"

.field public static final RECORD:Ljava/lang/String; = "Record"

.field public static final RED:Ljava/lang/String; = "Red"

.field public static final REPEAT:Ljava/lang/String; = "Repeat"

.field public static final REPLAY:Ljava/lang/String; = "replay"

.field public static final REVERSE:Ljava/lang/String; = "reverse"

.field public static final REWIND:Ljava/lang/String; = "Rewind"

.field public static final SEARCH:Ljava/lang/String; = "Search"

.field public static final SELECT:Ljava/lang/String; = "Select"

.field public static final SETTINGS:Ljava/lang/String; = "Settings"

.field public static final SEVEN:Ljava/lang/String; = "7"

.field public static final SIX:Ljava/lang/String; = "6"

.field public static final SKIP:Ljava/lang/String; = "Skip"

.field public static final SKIP_BACK:Ljava/lang/String; = "Skip_Back"

.field public static final SKIP_FORWARD:Ljava/lang/String; = "Skip_Forward"

.field public static final SLEEP:Ljava/lang/String; = "Sleep"

.field public static final SLOW:Ljava/lang/String; = "slow"

.field public static final SMART_HUB:Ljava/lang/String; = "Smart_Hub"

.field public static final STB_REPLAY:Ljava/lang/String; = "Replay"

.field public static final STB_SLOW:Ljava/lang/String; = "Slow"

.field public static final STOP:Ljava/lang/String; = "Stop"

.field public static final STOP2:Ljava/lang/String; = "Stop2"

.field public static final SUBMENU:Ljava/lang/String; = "submenu"

.field public static final SUBTITLE:Ljava/lang/String; = "Subtitle"

.field public static final SWING_1:Ljava/lang/String; = "SWING 1"

.field public static final SWING_2:Ljava/lang/String; = "SWING 2"

.field public static final S_VIDEO:Ljava/lang/String; = "S-VIDEO"

.field public static final Setup:Ljava/lang/String; = "Setup"

.field public static final SlowReverse:Ljava/lang/String; = "SlowReverse"

.field public static final SubTitle:Ljava/lang/String; = "SubTitle"

.field public static final TEMP_DN:Ljava/lang/String; = "Down"

.field public static final TEMP_UP:Ljava/lang/String; = "UP"

.field public static final TEN:Ljava/lang/String; = "10"

.field public static final THEATER:Ljava/lang/String; = "Theater"

.field public static final THREE:Ljava/lang/String; = "3"

.field public static final THUMBDOWN:Ljava/lang/String; = "ThumbsDown"

.field public static final THUMBUP:Ljava/lang/String; = "ThumbsUp"

.field public static final TIMER:Ljava/lang/String; = "Timer"

.field public static final TIMESLIP:Ljava/lang/String; = "TimeSlip"

.field public static final TIVO:Ljava/lang/String; = "TIVO"

.field public static final TOOLS:Ljava/lang/String; = "Tools"

.field public static final TUNER:Ljava/lang/String; = "Tuner"

.field public static final TV:Ljava/lang/String; = "TV"

.field public static final TV_Video:Ljava/lang/String; = "TV/Video"

.field public static final TWELVE:Ljava/lang/String; = "12"

.field public static final TWO:Ljava/lang/String; = "2"

.field public static final UP_OR_PLAY:Ljava/lang/String; = "UP/PLAY"

.field public static final VANE:Ljava/lang/String; = "VANE"

.field public static final VOD:Ljava/lang/String; = "VOD"

.field public static final VOLUME_DOWN:Ljava/lang/String; = "Volume_Down"

.field public static final VOLUME_UP:Ljava/lang/String; = "Volume_Up"

.field public static final YELLOW:Ljava/lang/String; = "Yellow"

.field public static final ZERO:Ljava/lang/String; = "0"

.field public static final ZOOM:Ljava/lang/String; = "zoom"

.field public static final Zoom:Ljava/lang/String; = "Zoom"

.field public static final chapter_minus:Ljava/lang/String; = "Chapter-"

.field public static final chapter_plus:Ljava/lang/String; = "Chapter+"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
