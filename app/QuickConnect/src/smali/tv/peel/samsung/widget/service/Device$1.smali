.class Ltv/peel/samsung/widget/service/Device$1;
.super Ljava/lang/Object;
.source "Device.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/peel/samsung/widget/service/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Ltv/peel/samsung/widget/service/Device;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Ltv/peel/samsung/widget/service/Device$1;->createFromParcel(Landroid/os/Parcel;)Ltv/peel/samsung/widget/service/Device;

    move-result-object v0

    return-object v0
.end method

.method public createFromParcel(Landroid/os/Parcel;)Ltv/peel/samsung/widget/service/Device;
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 23
    new-instance v0, Ltv/peel/samsung/widget/service/Device;

    invoke-direct {v0, p1}, Ltv/peel/samsung/widget/service/Device;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Ltv/peel/samsung/widget/service/Device$1;->newArray(I)[Ltv/peel/samsung/widget/service/Device;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Ltv/peel/samsung/widget/service/Device;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 28
    new-array v0, p1, [Ltv/peel/samsung/widget/service/Device;

    return-object v0
.end method
