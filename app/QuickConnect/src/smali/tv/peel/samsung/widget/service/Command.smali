.class public Ltv/peel/samsung/widget/service/Command;
.super Ljava/lang/Object;
.source "Command.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ltv/peel/samsung/widget/service/Command;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private brandId:I

.field private brandName:Ljava/lang/String;

.field private codesetId:I

.field private commandName:Ljava/lang/String;

.field private deviceTypeId:I

.field private funId:I

.field private isInput:Z

.field private rank:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Ltv/peel/samsung/widget/service/Command$1;

    invoke-direct {v0}, Ltv/peel/samsung/widget/service/Command$1;-><init>()V

    sput-object v0, Ltv/peel/samsung/widget/service/Command;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ltv/peel/samsung/widget/service/Command;->commandName:Ljava/lang/String;

    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Ltv/peel/samsung/widget/service/Command;->rank:I

    .line 98
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Ltv/peel/samsung/widget/service/Command;->codesetId:I

    .line 99
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Ltv/peel/samsung/widget/service/Command;->deviceTypeId:I

    .line 100
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Ltv/peel/samsung/widget/service/Command;->brandId:I

    .line 101
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Ltv/peel/samsung/widget/service/Command;->brandName:Ljava/lang/String;

    .line 102
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Ltv/peel/samsung/widget/service/Command;->isInput:Z

    .line 103
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Ltv/peel/samsung/widget/service/Command;->funId:I

    .line 104
    return-void

    .line 102
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;IIIILjava/lang/String;ZI)V
    .locals 0
    .param p1, "commandName"    # Ljava/lang/String;
    .param p2, "rank"    # I
    .param p3, "codesetId"    # I
    .param p4, "deviceTypeId"    # I
    .param p5, "brandId"    # I
    .param p6, "brandName"    # Ljava/lang/String;
    .param p7, "isInput"    # Z
    .param p8, "funId"    # I

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Ltv/peel/samsung/widget/service/Command;->commandName:Ljava/lang/String;

    .line 21
    iput p2, p0, Ltv/peel/samsung/widget/service/Command;->rank:I

    .line 22
    iput p3, p0, Ltv/peel/samsung/widget/service/Command;->codesetId:I

    .line 23
    iput p4, p0, Ltv/peel/samsung/widget/service/Command;->deviceTypeId:I

    .line 24
    iput p5, p0, Ltv/peel/samsung/widget/service/Command;->brandId:I

    .line 25
    iput-object p6, p0, Ltv/peel/samsung/widget/service/Command;->brandName:Ljava/lang/String;

    .line 26
    iput-boolean p7, p0, Ltv/peel/samsung/widget/service/Command;->isInput:Z

    .line 27
    iput p8, p0, Ltv/peel/samsung/widget/service/Command;->funId:I

    .line 28
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public getBrandId()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->brandId:I

    return v0
.end method

.method public getBrandName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Command;->brandName:Ljava/lang/String;

    return-object v0
.end method

.method public getCodesetId()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->codesetId:I

    return v0
.end method

.method public getCommandName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Command;->commandName:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceTypeId()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->deviceTypeId:I

    return v0
.end method

.method public getFunId()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->funId:I

    return v0
.end method

.method public getRank()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->rank:I

    return v0
.end method

.method public isInput()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Ltv/peel/samsung/widget/service/Command;->isInput:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Ltv/peel/samsung/widget/service/Command;->commandName:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ltv/peel/samsung/widget/service/Command;->rank:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 73
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Command;->commandName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 74
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->rank:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 75
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->codesetId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 76
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->deviceTypeId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->brandId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 78
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Command;->brandName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget-boolean v0, p0, Ltv/peel/samsung/widget/service/Command;->isInput:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 80
    iget v0, p0, Ltv/peel/samsung/widget/service/Command;->funId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 81
    return-void

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
