.class public Ltv/peel/samsung/widget/service/Device;
.super Ljava/lang/Object;
.source "Device.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ltv/peel/samsung/widget/service/Device;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field brandName:Ljava/lang/String;

.field private commands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltv/peel/samsung/widget/service/Command;",
            ">;"
        }
    .end annotation
.end field

.field displayName:Ljava/lang/String;

.field id:Ljava/lang/String;

.field type:Ljava/lang/String;

.field typeId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ltv/peel/samsung/widget/service/Device$1;

    invoke-direct {v0}, Ltv/peel/samsung/widget/service/Device$1;-><init>()V

    sput-object v0, Ltv/peel/samsung/widget/service/Device;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/samsung/widget/service/Device;->id:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Ltv/peel/samsung/widget/service/Device;->typeId:I

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/samsung/widget/service/Device;->type:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/samsung/widget/service/Device;->brandName:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ltv/peel/samsung/widget/service/Device;->displayName:Ljava/lang/String;

    .line 38
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->commands:Ljava/util/List;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltv/peel/samsung/widget/service/Device;->commands:Ljava/util/List;

    .line 41
    :cond_0
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->commands:Ljava/util/List;

    sget-object v1, Ltv/peel/samsung/widget/service/Command;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "typeId"    # I
    .param p3, "type"    # Ljava/lang/String;
    .param p4, "brandName"    # Ljava/lang/String;
    .param p5, "displayName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ltv/peel/samsung/widget/service/Command;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p6, "commands":Ljava/util/List;, "Ljava/util/List<Ltv/peel/samsung/widget/service/Command;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Ltv/peel/samsung/widget/service/Device;->id:Ljava/lang/String;

    .line 46
    iput p2, p0, Ltv/peel/samsung/widget/service/Device;->typeId:I

    .line 47
    iput-object p3, p0, Ltv/peel/samsung/widget/service/Device;->type:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Ltv/peel/samsung/widget/service/Device;->brandName:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Ltv/peel/samsung/widget/service/Device;->displayName:Ljava/lang/String;

    .line 50
    iput-object p6, p0, Ltv/peel/samsung/widget/service/Device;->commands:Ljava/util/List;

    .line 51
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public getBrandName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->brandName:Ljava/lang/String;

    return-object v0
.end method

.method public getCommands()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ltv/peel/samsung/widget/service/Command;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->commands:Ljava/util/List;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getTypeId()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Ltv/peel/samsung/widget/service/Device;->typeId:I

    return v0
.end method

.method public setTypeId(I)V
    .locals 0
    .param p1, "typeId"    # I

    .prologue
    .line 62
    iput p1, p0, Ltv/peel/samsung/widget/service/Device;->typeId:I

    .line 63
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Ltv/peel/samsung/widget/service/Device;->id:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ltv/peel/samsung/widget/service/Device;->typeId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/peel/samsung/widget/service/Device;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/peel/samsung/widget/service/Device;->brandName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ltv/peel/samsung/widget/service/Device;->displayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 92
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 93
    iget v0, p0, Ltv/peel/samsung/widget/service/Device;->typeId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->brandName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 96
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->displayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Ltv/peel/samsung/widget/service/Device;->commands:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 98
    return-void
.end method
