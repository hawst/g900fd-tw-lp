.class public abstract Ltv/peel/samsung/widget/service/IQuickConnectService$Stub;
.super Landroid/os/Binder;
.source "IQuickConnectService.java"

# interfaces
.implements Ltv/peel/samsung/widget/service/IQuickConnectService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ltv/peel/samsung/widget/service/IQuickConnectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ltv/peel/samsung/widget/service/IQuickConnectService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "tv.peel.samsung.widget.service.IQuickConnectService"

.field static final TRANSACTION_getCurrentRoom:I = 0x1

.field static final TRANSACTION_getRemoteControlList:I = 0x2

.field static final TRANSACTION_getRoomList:I = 0x3

.field static final TRANSACTION_isRemoteControlSetuped:I = 0x5

.field static final TRANSACTION_sendRemocon:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-virtual {p0, p0, v0}, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Ltv/peel/samsung/widget/service/IQuickConnectService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Ltv/peel/samsung/widget/service/IQuickConnectService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Ltv/peel/samsung/widget/service/IQuickConnectService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 111
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 42
    :sswitch_0
    const-string v5, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v7, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub;->getCurrentRoom()Ltv/peel/samsung/widget/service/Room;

    move-result-object v3

    .line 49
    .local v3, "_result":Ltv/peel/samsung/widget/service/Room;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    if-eqz v3, :cond_0

    .line 51
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 52
    invoke-virtual {v3, p3, v6}, Ltv/peel/samsung/widget/service/Room;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 61
    .end local v3    # "_result":Ltv/peel/samsung/widget/service/Room;
    :sswitch_2
    const-string v5, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub;->getRemoteControlList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 65
    .local v4, "_result":Ljava/util/List;, "Ljava/util/List<Ltv/peel/samsung/widget/service/Device;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 66
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto :goto_0

    .line 71
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Ltv/peel/samsung/widget/service/Device;>;"
    :sswitch_3
    const-string v5, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub;->getRoomList()[Ltv/peel/samsung/widget/service/Room;

    move-result-object v3

    .line 73
    .local v3, "_result":[Ltv/peel/samsung/widget/service/Room;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 74
    invoke-virtual {p3, v3, v6}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    goto :goto_0

    .line 79
    .end local v3    # "_result":[Ltv/peel/samsung/widget/service/Room;
    :sswitch_4
    const-string v5, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 83
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_1

    .line 84
    sget-object v5, Ltv/peel/samsung/widget/service/Device;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltv/peel/samsung/widget/service/Device;

    .line 90
    .local v1, "_arg1":Ltv/peel/samsung/widget/service/Device;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    if-eqz v5, :cond_2

    .line 91
    sget-object v5, Ltv/peel/samsung/widget/service/Command;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ltv/peel/samsung/widget/service/Command;

    .line 96
    .local v2, "_arg2":Ltv/peel/samsung/widget/service/Command;
    :goto_2
    invoke-virtual {p0, v0, v1, v2}, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub;->sendRemocon(Ljava/lang/String;Ltv/peel/samsung/widget/service/Device;Ltv/peel/samsung/widget/service/Command;)V

    .line 97
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 87
    .end local v1    # "_arg1":Ltv/peel/samsung/widget/service/Device;
    .end local v2    # "_arg2":Ltv/peel/samsung/widget/service/Command;
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Ltv/peel/samsung/widget/service/Device;
    goto :goto_1

    .line 94
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg2":Ltv/peel/samsung/widget/service/Command;
    goto :goto_2

    .line 102
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ltv/peel/samsung/widget/service/Device;
    .end local v2    # "_arg2":Ltv/peel/samsung/widget/service/Command;
    :sswitch_5
    const-string v7, "tv.peel.samsung.widget.service.IQuickConnectService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 105
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Ltv/peel/samsung/widget/service/IQuickConnectService$Stub;->isRemoteControlSetuped(Ljava/lang/String;)Z

    move-result v3

    .line 106
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 107
    if-eqz v3, :cond_3

    move v5, v6

    :cond_3
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
