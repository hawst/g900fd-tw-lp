.class Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;
.super Ljava/lang/Object;
.source "ConfirmDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/useragreement/ConfirmDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;


# direct methods
.method constructor <init>(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v4, 0x7f060008

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 168
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ConfirmDialog.CheckBox.onClick()"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z
    invoke-static {v3}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$500(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Z

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 169
    iget-object v3, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$500(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    # setter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z
    invoke-static {v3, v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$502(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;Z)Z

    .line 171
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$500(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$600(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const v2, 0x7f020002

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 174
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$700(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 175
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$800(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02000b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$900(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 177
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$900(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-virtual {v1, v4}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 169
    goto :goto_0

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$600(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 182
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$700(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 183
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$800(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x7f02000a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 184
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$900(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "#888888"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 185
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$900(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-virtual {v1, v4}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
