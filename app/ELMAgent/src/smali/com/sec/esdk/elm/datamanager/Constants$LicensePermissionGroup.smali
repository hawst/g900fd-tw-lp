.class public final enum Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/datamanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LicensePermissionGroup"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

.field public static final enum KNOX_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

.field public static final enum SAFE_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

.field public static final enum UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;


# instance fields
.field private mPermGropName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 126
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    const-string v1, "UNKNOWN_GROUP"

    const-string v2, "UNKNOWN"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    const-string v1, "SAFE_GROUP"

    const-string v2, "SAFE"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->SAFE_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    const-string v1, "KNOX_GROUP"

    const-string v2, "KNOX"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->KNOX_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 125
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->SAFE_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->KNOX_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "GropuName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->mPermGropName:Ljava/lang/String;

    .line 131
    iput-object p3, p0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->mPermGropName:Ljava/lang/String;

    .line 132
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 125
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->mPermGropName:Ljava/lang/String;

    return-object v0
.end method
