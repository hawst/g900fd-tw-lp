.class public final enum Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/datamanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ServerConnectionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

.field public static final enum HTTPS:Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    const-string v1, "HTTPS"

    invoke-direct {v0, v1, v2}, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;->HTTPS:Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    .line 14
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;->HTTPS:Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 14
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    return-object v0
.end method
