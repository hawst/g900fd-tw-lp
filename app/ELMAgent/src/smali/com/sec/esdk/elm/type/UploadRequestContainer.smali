.class public Lcom/sec/esdk/elm/type/UploadRequestContainer;
.super Ljava/lang/Object;
.source "UploadRequestContainer.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private ApkHash:Ljava/lang/String;

.field private ClientTimezone:Ljava/lang/String;

.field private InstanceId:Ljava/lang/String;

.field private PackageName:Ljava/lang/String;

.field private PackageVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/sec/esdk/elm/type/UploadRequestContainer$1;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/UploadRequestContainer$1;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->InstanceId:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageName:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageVersion:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ApkHash:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ClientTimezone:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->InstanceId:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageName:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageVersion:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ApkHash:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ClientTimezone:Ljava/lang/String;

    .line 57
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->readFromParcel(Landroid/os/Parcel;)V

    .line 58
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public getApkHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ApkHash:Ljava/lang/String;

    return-object v0
.end method

.method public getInstanceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->InstanceId:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageVersion:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->InstanceId:Ljava/lang/String;

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageName:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageVersion:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ApkHash:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ClientTimezone:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setApkHash(Ljava/lang/String;)V
    .locals 0
    .param p1, "apkHash"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ApkHash:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public setClientTimezone(Ljava/lang/String;)V
    .locals 0
    .param p1, "clientTimezone"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ClientTimezone:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setInstanceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->InstanceId:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageName:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public setPackageVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageVersion"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageVersion:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->InstanceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->PackageVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ApkHash:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadRequestContainer;->ClientTimezone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 31
    return-void
.end method
