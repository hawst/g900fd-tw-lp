.class public Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
.super Ljava/lang/Object;
.source "RedirectionDataManager.java"


# static fields
.field private static CHINA_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

.field private static GLOBAL_REDIRECTION_SERVER_ADDR:Ljava/lang/String;


# instance fields
.field private HTTPS_Connection:Ljava/lang/String;

.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-string v0, "gslb.secb2b.com"

    sput-object v0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->GLOBAL_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    .line 27
    const-string v0, "china-gslb.secb2b.com.cn"

    sput-object v0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->CHINA_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-string v1, "https://"

    iput-object v1, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->HTTPS_Connection:Ljava/lang/String;

    .line 46
    iput-object p1, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->mContext:Landroid/content/Context;

    .line 47
    const/4 v0, 0x0

    .line 48
    .local v0, "isELMTested":Z
    const-string v1, "gslb.secb2b.com"

    sput-object v1, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->GLOBAL_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    .line 49
    const-string v1, "china-gslb.secb2b.com.cn"

    sput-object v1, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->CHINA_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    .line 51
    invoke-static {p1}, Lcom/sec/esdk/elm/utils/Utility;->isProduct_Ship(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    const-string v1, "stage-gslb.secb2b.com"

    sput-object v1, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->GLOBAL_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    .line 53
    const-string v1, "china-stage-gslb.secb2b.com.cn"

    sput-object v1, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->CHINA_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RedirectionDataManager.isProduct_Ship() is :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/sec/esdk/elm/utils/Utility;->isProduct_Ship(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". GSLB goes to Staging Server."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 70
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->checkTestApp(Landroid/content/Context;)Z

    move-result v0

    .line 58
    if-eqz v0, :cond_1

    .line 59
    const-string v1, "stage-gslb.secb2b.com"

    sput-object v1, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->GLOBAL_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    .line 60
    const-string v1, "china-stage-gslb.secb2b.com.cn"

    sput-object v1, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->CHINA_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RedirectionDataManager.isProduct_Ship() is :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/sec/esdk/elm/utils/Utility;->isProduct_Ship(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isTested:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". GSLB goes to Staging Server."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :cond_1
    const-string v1, "gslb.secb2b.com"

    sput-object v1, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->GLOBAL_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    .line 65
    const-string v1, "china-gslb.secb2b.com.cn"

    sput-object v1, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->CHINA_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    .line 66
    const-string v1, "RedirectionDataManager.isProduct_Ship() is true. GSLB goes to Production Server."

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkTestApp(Landroid/content/Context;)Z
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    const/4 v1, 0x0

    .line 75
    .local v1, "info":Landroid/content/pm/PackageInfo;
    const/4 v2, 0x0

    .line 76
    .local v2, "result1":Z
    const/4 v3, 0x0

    .line 80
    .local v3, "result2":Z
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.esdk.elmagenttest"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 81
    if-eqz v1, :cond_0

    .line 82
    const/4 v2, 0x1

    .line 88
    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.esdk.elmagenttest2"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 89
    if-eqz v1, :cond_1

    .line 90
    const/4 v3, 0x1

    .line 95
    :cond_1
    :goto_1
    or-int v4, v2, v3

    return v4

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0

    .line 91
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 92
    .restart local v0    # "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getELMServerAddr(Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;)Ljava/lang/String;
    .locals 13
    .param p1, "connectionType"    # Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    .prologue
    .line 193
    const-string v11, "RedirectionDataManager.getELMServerAddr()."

    invoke-static {v11}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x0

    .line 196
    .local v0, "Address":Ljava/lang/String;
    const/4 v7, 0x0

    .line 208
    .local v7, "port":Ljava/lang/String;
    :try_start_0
    iget-object v11, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v9

    .line 209
    .local v9, "redirectionDataUtil":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v9}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getElmServerAddr()Ljava/lang/String;

    move-result-object v10

    .line 210
    .local v10, "savedData":Ljava/lang/String;
    const/4 v2, 0x0

    .line 211
    .local v2, "ELMServerAddr":Lorg/json/JSONArray;
    if-nez v10, :cond_0

    .line 212
    new-instance v11, Ljava/lang/Exception;

    invoke-direct {v11}, Ljava/lang/Exception;-><init>()V

    throw v11
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    .end local v2    # "ELMServerAddr":Lorg/json/JSONArray;
    .end local v9    # "redirectionDataUtil":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .end local v10    # "savedData":Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 231
    .local v5, "e":Ljava/lang/Exception;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "RedirectionDataManager\'s getELMHttpServerAddr has Exception.\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 232
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 233
    const/4 v0, 0x0

    move-object v1, v0

    .line 237
    .end local v0    # "Address":Ljava/lang/String;
    .end local v5    # "e":Ljava/lang/Exception;
    .local v1, "Address":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 215
    .end local v1    # "Address":Ljava/lang/String;
    .restart local v0    # "Address":Ljava/lang/String;
    .restart local v2    # "ELMServerAddr":Lorg/json/JSONArray;
    .restart local v9    # "redirectionDataUtil":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .restart local v10    # "savedData":Ljava/lang/String;
    :cond_0
    :try_start_1
    new-instance v2, Lorg/json/JSONArray;

    .end local v2    # "ELMServerAddr":Lorg/json/JSONArray;
    invoke-direct {v2, v10}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 216
    .restart local v2    # "ELMServerAddr":Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 217
    .local v4, "ELMServerAddrSize":I
    const/4 v6, 0x0

    .local v6, "idx":I
    :goto_1
    if-ge v6, v4, :cond_1

    .line 218
    invoke-virtual {v2, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 219
    .local v3, "ELMServerAddrObject":Lorg/json/JSONObject;
    const-string v11, "protocol"

    invoke-virtual {v3, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 221
    .local v8, "protocol":Ljava/lang/String;
    sget-object v11, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;->HTTPS:Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    if-ne p1, v11, :cond_2

    const-string v11, "https"

    invoke-virtual {v8, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 222
    const-string v11, "url"

    invoke-virtual {v3, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 223
    const-string v11, "port"

    invoke-virtual {v3, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 224
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 228
    .end local v3    # "ELMServerAddrObject":Lorg/json/JSONObject;
    .end local v8    # "protocol":Ljava/lang/String;
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "RedirectionDataManager.getELMServerAddr() : Address="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-object v1, v0

    .line 237
    .end local v0    # "Address":Ljava/lang/String;
    .restart local v1    # "Address":Ljava/lang/String;
    goto :goto_0

    .line 217
    .end local v1    # "Address":Ljava/lang/String;
    .restart local v0    # "Address":Ljava/lang/String;
    .restart local v3    # "ELMServerAddrObject":Lorg/json/JSONObject;
    .restart local v8    # "protocol":Ljava/lang/String;
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method public isELMServerAddr()Z
    .locals 2

    .prologue
    .line 99
    iget-object v1, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 100
    .local v0, "redirectionDataUtil":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getElmServerAddr()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 101
    const/4 v1, 0x0

    .line 103
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setELMServerAddr()Z
    .locals 14

    .prologue
    .line 108
    const-string v11, "RedirectionDataManager.setELMServerAddr()."

    invoke-static {v11}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 110
    const/4 v6, 0x0

    .line 111
    .local v6, "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    const/4 v8, 0x0

    .line 112
    .local v8, "mGSLBManagerPolicy":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    const/4 v3, 0x0

    .line 113
    .local v3, "country":Ljava/lang/String;
    const/4 v2, 0x0

    .line 114
    .local v2, "SERVER_ADDRESS":Ljava/lang/String;
    const/4 v1, 0x0

    .line 116
    .local v1, "POLISY_SERVER_ADDRESS":Ljava/lang/String;
    new-instance v10, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;

    invoke-direct {v10}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;-><init>()V

    .line 117
    .local v10, "redirectionRequestInfo":Lcom/sec/esdk/elm/type/RedirectionRequestInfo;
    new-instance v4, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;

    iget-object v11, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->mContext:Landroid/content/Context;

    invoke-direct {v4, v11}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;-><init>(Landroid/content/Context;)V

    .line 119
    .local v4, "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    invoke-virtual {v4}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getDeviceSerial()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->setDeviceID(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v4}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getSalesCodeFromCSC()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->setSalesCode(Ljava/lang/String;)V

    .line 121
    invoke-virtual {v4}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getCountryISOFromCSC()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->setCountryISO(Ljava/lang/String;)V

    .line 122
    invoke-virtual {v4}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getMCC()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->setMCC(Ljava/lang/String;)V

    .line 124
    invoke-static {v10}, Lcom/sec/esdk/elm/utils/JSON;->compriseRedirectionRequestJSON(Lcom/sec/esdk/elm/type/RedirectionRequestInfo;)Lorg/json/JSONObject;

    move-result-object v9

    .line 126
    .local v9, "redirectionJSON":Lorg/json/JSONObject;
    iget-object v11, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->mContext:Landroid/content/Context;

    const-string v12, "ro.csc.country_code"

    invoke-static {v11, v12}, Lcom/sec/esdk/elm/utils/SystemPropertiesProxy;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 129
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 131
    .local v0, "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSURL()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_0

    .line 132
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSURL()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/KnoxGSLB/lookup/elm"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 133
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSURL()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/KnoxGSLB/policy"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 134
    new-instance v8, Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    .end local v8    # "mGSLBManagerPolicy":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->HTTPS_Connection:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->mContext:Landroid/content/Context;

    sget-object v13, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_POLICY:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    invoke-direct {v8, v11, v9, v12, v13}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Landroid/content/Context;Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;)V

    .line 141
    .restart local v8    # "mGSLBManagerPolicy":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    :goto_0
    new-instance v6, Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    .end local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->HTTPS_Connection:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->mContext:Landroid/content/Context;

    invoke-direct {v6, v11, v9, v12}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Landroid/content/Context;)V

    .line 144
    .restart local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    :try_start_0
    monitor-enter v6
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 145
    :try_start_1
    invoke-virtual {v6}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->start()V

    .line 146
    invoke-virtual {v6}, Ljava/lang/Object;->wait()V

    .line 149
    iget-boolean v11, v6, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->isSuccess:Z

    if-nez v11, :cond_3

    .line 150
    const-string v11, "RedirectionDataManager.setELMServerAddr() HTTPS fail. Try to HTTP connextion"

    invoke-static {v11}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 153
    new-instance v7, Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->HTTPS_Connection:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->mContext:Landroid/content/Context;

    invoke-direct {v7, v11, v9, v12}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 155
    .end local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    .local v7, "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    :try_start_2
    monitor-enter v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 156
    :try_start_3
    invoke-virtual {v7}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->start()V

    .line 157
    invoke-virtual {v7}, Ljava/lang/Object;->wait()V

    .line 158
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 160
    :goto_1
    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 163
    if-eqz v8, :cond_2

    .line 164
    :try_start_5
    monitor-enter v8
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0

    .line 165
    :try_start_6
    invoke-virtual {v8}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->start()V

    .line 166
    invoke-virtual {v8}, Ljava/lang/Object;->wait()V

    .line 167
    monitor-exit v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 170
    :try_start_7
    iget-boolean v11, v7, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->isSuccess:Z

    if-nez v11, :cond_2

    .line 171
    const-string v11, "RedirectionDataManager.setELMServerAddr() POLICY HTTPS fail. Try to HTTP connextion"

    invoke-static {v11}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 174
    new-instance v6, Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->HTTPS_Connection:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->mContext:Landroid/content/Context;

    sget-object v13, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_POLICY:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    invoke-direct {v6, v11, v9, v12, v13}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Landroid/content/Context;Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0

    .line 176
    .end local v7    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    .restart local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    :try_start_8
    monitor-enter v6
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_1

    .line 177
    :try_start_9
    invoke-virtual {v6}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->start()V

    .line 178
    invoke-virtual {v6}, Ljava/lang/Object;->wait()V

    .line 179
    monitor-exit v6
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    .line 188
    :goto_2
    const/4 v11, 0x1

    :goto_3
    return v11

    .line 135
    :cond_0
    if-eqz v3, :cond_1

    const-string v11, "China"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 136
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->CHINA_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/KnoxGSLB/lookup/elm"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 138
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->GLOBAL_REDIRECTION_SERVER_ADDR:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/KnoxGSLB/lookup/elm"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 158
    .end local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    .restart local v7    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    :catchall_0
    move-exception v11

    :try_start_a
    monitor-exit v7
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    throw v11

    .line 160
    :catchall_1
    move-exception v11

    :goto_4
    monitor-exit v6
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    throw v11
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_0

    .line 183
    :catch_0
    move-exception v5

    move-object v6, v7

    .line 184
    .end local v7    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    .local v5, "e":Ljava/lang/InterruptedException;
    .restart local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    :goto_5
    invoke-virtual {v5}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 185
    const/4 v11, 0x0

    goto :goto_3

    .line 167
    .end local v5    # "e":Ljava/lang/InterruptedException;
    .end local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    .restart local v7    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    :catchall_2
    move-exception v11

    :try_start_d
    monitor-exit v8
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    :try_start_e
    throw v11
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_0

    .line 179
    .end local v7    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    .restart local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    :catchall_3
    move-exception v11

    :try_start_f
    monitor-exit v6
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    :try_start_10
    throw v11
    :try_end_10
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_1

    .line 183
    :catch_1
    move-exception v5

    goto :goto_5

    .line 160
    :catchall_4
    move-exception v11

    move-object v7, v6

    .end local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    .restart local v7    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    goto :goto_4

    :cond_2
    move-object v6, v7

    .end local v7    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    .restart local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    goto :goto_2

    :cond_3
    move-object v7, v6

    .end local v6    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    .restart local v7    # "mGSLBManager":Lcom/sec/esdk/elm/networkmanager/GSLBManager;
    goto/16 :goto_1
.end method
