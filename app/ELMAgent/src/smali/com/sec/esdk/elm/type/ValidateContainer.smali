.class public Lcom/sec/esdk/elm/type/ValidateContainer;
.super Lcom/sec/esdk/elm/type/ELMContainer;
.source "ValidateContainer.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private mPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

.field private mValidateRequestContainer:Lcom/sec/esdk/elm/type/ValidateRequestContainer;

.field private mValidateResponseContainer:Lcom/sec/esdk/elm/type/ValidateResponseContainer;

.field private retryCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/sec/esdk/elm/type/ValidateContainer$1;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/ValidateContainer$1;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/type/ValidateContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Lcom/sec/esdk/elm/type/ELMContainer;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateRequestContainer:Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    .line 18
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateResponseContainer:Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    .line 20
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->retryCount:I

    .line 79
    new-instance v0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateRequestContainer:Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    .line 80
    new-instance v0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateResponseContainer:Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    .line 81
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->UN_KNOWN:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lcom/sec/esdk/elm/type/ELMContainer;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateRequestContainer:Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    .line 18
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateResponseContainer:Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    .line 20
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->retryCount:I

    .line 55
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->readFromParcel(Landroid/os/Parcel;)V

    .line 56
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public getPushPointOfValidation()Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    return-object v0
.end method

.method public getRetryCount()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->retryCount:I

    return v0
.end method

.method public getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateRequestContainer:Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    return-object v0
.end method

.method public getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateResponseContainer:Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    return-object v0
.end method

.method public increseRetryCount()V
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->retryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->retryCount:I

    .line 60
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 33
    const-class v0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateRequestContainer:Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    .line 34
    const-class v0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateResponseContainer:Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    .line 35
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->retryCount:I

    .line 38
    return-void
.end method

.method public setPushPointOfValidation(Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;)V
    .locals 0
    .param p1, "mPushPointOfValidation"    # Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .line 72
    return-void
.end method

.method public setRetryCount(I)V
    .locals 0
    .param p1, "nRetry"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->retryCount:I

    .line 68
    return-void
.end method

.method public setValidateResponseContainer(Lcom/sec/esdk/elm/type/ValidateResponseContainer;)V
    .locals 0
    .param p1, "validateResponseContainer"    # Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateResponseContainer:Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    .line 90
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateRequestContainer:Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 27
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mValidateResponseContainer:Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 28
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->mPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 29
    iget v0, p0, Lcom/sec/esdk/elm/type/ValidateContainer;->retryCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 30
    return-void
.end method
