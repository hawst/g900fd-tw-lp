.class public Lcom/sec/esdk/elm/useragreement/ConfirmDialog;
.super Landroid/app/Activity;
.source "ConfirmDialog.java"


# instance fields
.field private EULA_ServerAddr:Ljava/lang/String;

.field private IsUserSeleteButton:Z

.field private cb_agree:Landroid/widget/ImageView;

.field private check_confirm:Z

.field private extras:Landroid/os/Bundle;

.field private iv_agree:Landroid/widget/ImageView;

.field licenseKey:Ljava/lang/String;

.field private mAgreeBtnListener:Landroid/view/View$OnClickListener;

.field private mCheckBoxListener:Landroid/view/View$OnClickListener;

.field private mDisagreeBtnListener:Landroid/view/View$OnClickListener;

.field private mScrollView:Landroid/widget/ScrollView;

.field private msg:Landroid/os/Message;

.field packageName:Ljava/lang/String;

.field packageVersion:Ljava/lang/String;

.field private registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;

.field private sViewX:I

.field private sViewY:I

.field private tr_agree:Landroid/widget/LinearLayout;

.field private tv_agree:Landroid/widget/TextView;

.field private tv_agreeView:Landroid/widget/TextView;

.field private tv_layout_agree:Landroid/widget/LinearLayout;

.field private tv_layout_disagree:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 44
    iput-boolean v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z

    .line 45
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tr_agree:Landroid/widget/LinearLayout;

    .line 46
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    .line 47
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    .line 48
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;

    .line 49
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->msg:Landroid/os/Message;

    .line 50
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;

    .line 51
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    .line 52
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    .line 53
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    .line 54
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_disagree:Landroid/widget/LinearLayout;

    .line 55
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    .line 58
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->EULA_ServerAddr:Ljava/lang/String;

    .line 59
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    .line 60
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageVersion:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->licenseKey:Ljava/lang/String;

    .line 63
    iput-boolean v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->IsUserSeleteButton:Z

    .line 109
    new-instance v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;

    invoke-direct {v0, p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;-><init>(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)V

    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mAgreeBtnListener:Landroid/view/View$OnClickListener;

    .line 155
    new-instance v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$2;-><init>(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)V

    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mDisagreeBtnListener:Landroid/view/View$OnClickListener;

    .line 165
    new-instance v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;

    invoke-direct {v0, p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$3;-><init>(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)V

    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mCheckBoxListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->IsUserSeleteButton:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/os/Bundle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->sViewX:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->sViewY:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/os/Message;
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->msg:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;Landroid/os/Message;)Landroid/os/Message;
    .locals 0
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->msg:Landroid/os/Message;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Lcom/sec/esdk/elm/type/RegisterContainer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;Lcom/sec/esdk/elm/type/RegisterContainer;)Lcom/sec/esdk/elm/type/RegisterContainer;
    .locals 0
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;
    .param p1, "x1"    # Lcom/sec/esdk/elm/type/RegisterContainer;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->disagree()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z

    return v0
.end method

.method static synthetic access$502(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z

    return p1
.end method

.method static synthetic access$600(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    return-object v0
.end method

.method private disagree()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 275
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;

    const-string v1, "edm.intent.extra.license.data.pkgname"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    .line 276
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;

    const-string v1, "edm.intent.extra.license.data.pkgversion"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageVersion:Ljava/lang/String;

    .line 278
    new-instance v5, Landroid/app/enterprise/license/Error;

    sget-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v0

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    invoke-direct {v5, v0, v1, v3}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 279
    .local v5, "mError":Landroid/app/enterprise/license/Error;
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 280
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageVersion:Ljava/lang/String;

    const-string v2, "fail"

    sget-object v6, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v4, v3

    invoke-static/range {v0 .. v7}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    .line 282
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->disagree()V

    .line 241
    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->finish()V

    .line 242
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 67
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const/high16 v1, 0x7f030000

    invoke-virtual {p0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->setContentView(I)V

    .line 69
    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->setELMContext(Landroid/content/Context;)V

    .line 72
    const v1, 0x7f080002

    invoke-virtual {p0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    .line 73
    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;

    .line 76
    const v1, 0x7f080008

    invoke-virtual {p0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    .line 77
    const v1, 0x7f08000b

    invoke-virtual {p0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    .line 78
    const v1, 0x7f08000c

    invoke-virtual {p0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    .line 79
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 80
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mAgreeBtnListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v1, 0x7f080007

    invoke-virtual {p0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_disagree:Landroid/widget/LinearLayout;

    .line 84
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_disagree:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 85
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_disagree:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mDisagreeBtnListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    const v1, 0x7f080004

    invoke-virtual {p0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    .line 89
    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ro.csc.country_code"

    invoke-static {v1, v2}, Lcom/sec/esdk/elm/utils/SystemPropertiesProxy;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "country":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "Korea"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->EULA_ServerAddr:Ljava/lang/String;

    .line 93
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060004

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->EULA_ServerAddr:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->EULA_ServerAddr:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    :goto_0
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 101
    const v1, 0x7f080003

    invoke-virtual {p0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tr_agree:Landroid/widget/LinearLayout;

    .line 102
    const v1, 0x7f080006

    invoke-virtual {p0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    .line 103
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tr_agree:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mCheckBoxListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mCheckBoxListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    return-void

    .line 95
    :cond_0
    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->EULA_ServerAddr:Ljava/lang/String;

    .line 96
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agreeView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060002

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->EULA_ServerAddr:Ljava/lang/String;

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->EULA_ServerAddr:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x7f060008

    .line 206
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 207
    const-string v0, "ConfirmDialog.onRestoreInstanceState()"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 209
    const-string v0, "sViewX"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->sViewX:I

    .line 210
    const-string v0, "sViewY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->sViewY:I

    .line 211
    const-string v0, "check_confirm"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z

    .line 213
    iget-boolean v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    const v1, 0x7f020002

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 216
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 217
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    const v1, 0x7f02000b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 218
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    const-string v1, "#FFFFFF"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 219
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    :goto_0
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    new-instance v1, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$4;

    invoke-direct {v1, p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$4;-><init>(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 236
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->cb_agree:Landroid/widget/ImageView;

    const/high16 v1, 0x7f020000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 224
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_layout_agree:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 225
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->iv_agree:Landroid/widget/ImageView;

    const v1, 0x7f02000a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 226
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    const-string v1, "#888888"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 227
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->tv_agree:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 192
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 193
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 198
    const-string v0, "sViewX"

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollX()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 199
    const-string v0, "sViewY"

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->mScrollView:Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 200
    const-string v0, "check_confirm"

    iget-boolean v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->check_confirm:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 201
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 202
    return-void
.end method

.method protected onStop()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 246
    iget-boolean v9, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->IsUserSeleteButton:Z

    if-nez v9, :cond_1

    .line 247
    const-string v9, "activity"

    invoke-virtual {p0, v9}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 248
    .local v1, "am":Landroid/app/ActivityManager;
    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 251
    .local v7, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {v1, v11}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v8

    .line 252
    .local v8, "runningList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const-string v0, ""

    .line 253
    .local v0, "TopActivity":Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 254
    .local v6, "info":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v9, v6, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 255
    goto :goto_0

    .line 258
    .end local v6    # "info":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_0
    new-instance v9, Landroid/content/Intent;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v9, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v10, "android.intent.category.HOME"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 259
    .local v3, "homeIntent":Landroid/content/Intent;
    invoke-virtual {v7, v3, v11}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 261
    .local v2, "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v9

    if-ge v4, v9, :cond_1

    .line 262
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 263
    .local v6, "info":Landroid/content/pm/ResolveInfo;
    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 264
    invoke-direct {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->disagree()V

    .line 265
    invoke-virtual {p0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->finish()V

    .line 271
    .end local v0    # "TopActivity":Ljava/lang/String;
    .end local v1    # "am":Landroid/app/ActivityManager;
    .end local v2    # "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .end local v3    # "homeIntent":Landroid/content/Intent;
    .end local v4    # "i":I
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "info":Landroid/content/pm/ResolveInfo;
    .end local v7    # "pm":Landroid/content/pm/PackageManager;
    .end local v8    # "runningList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 272
    return-void

    .line 261
    .restart local v0    # "TopActivity":Ljava/lang/String;
    .restart local v1    # "am":Landroid/app/ActivityManager;
    .restart local v2    # "homeApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v3    # "homeIntent":Landroid/content/Intent;
    .restart local v4    # "i":I
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "info":Landroid/content/pm/ResolveInfo;
    .restart local v7    # "pm":Landroid/content/pm/PackageManager;
    .restart local v8    # "runningList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method
