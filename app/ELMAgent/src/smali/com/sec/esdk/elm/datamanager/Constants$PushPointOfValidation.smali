.class public final enum Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/datamanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PushPointOfValidation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

.field public static final enum INNER_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

.field public static final enum KLMS_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

.field public static final enum UN_KNOWN:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;


# instance fields
.field private mPushPoint:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 92
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    const-string v1, "KLMS_INTENT"

    const-string v2, "KLMS"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->KLMS_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    const-string v1, "INNER_INTENT"

    const-string v2, "INNER"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->INNER_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    const-string v1, "UN_KNOWN"

    const-string v2, "UN_KNOWN"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->UN_KNOWN:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .line 91
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->KLMS_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->INNER_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->UN_KNOWN:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "PushPointOfValidation"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->mPushPoint:Ljava/lang/String;

    .line 95
    iput-object p3, p0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->mPushPoint:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->mPushPoint:Ljava/lang/String;

    return-object v0
.end method
