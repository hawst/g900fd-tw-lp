.class public final enum Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;
.super Ljava/lang/Enum;
.source "ModuleErrorEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum AgentInternalError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum CommonNullParamError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum CommonUnknownError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum LicenseInvalidError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum LicenseRegisterError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum LicenseTerminatedError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum NetworkDisconnectedError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum NetworkGeneralError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum ServerInternalError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum ServerTimeMismatchError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum Success:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

.field public static final enum UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;


# instance fields
.field private mErrorCode:I

.field private mErrorDesc:Ljava/lang/String;

.field private mModuleCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/16 v9, 0xc8

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v1, "Success"

    const-string v5, "success"

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->Success:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 12
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "CommonNullParamError"

    const/16 v6, 0x64

    const/16 v7, 0x65

    const-string v8, "Null parameter"

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->CommonNullParamError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 13
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "CommonUnknownError"

    const/16 v6, 0x64

    const/16 v7, 0x66

    const-string v8, "Unknown error"

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->CommonUnknownError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 16
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "LicenseInvalidError"

    const/16 v7, 0xc9

    const-string v8, "Invalid license"

    move v5, v12

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->LicenseInvalidError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 17
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "LicenseRegisterError"

    const/4 v5, 0x4

    const/16 v7, 0xca

    const-string v8, "No more registration with this license"

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->LicenseRegisterError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 18
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "LicenseTerminatedError"

    const/4 v5, 0x5

    const/16 v7, 0xcb

    const-string v8, "License terminated"

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->LicenseTerminatedError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 19
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "ServerTimeMismatchError"

    const/4 v5, 0x6

    const/16 v7, 0xcd

    const-string v8, "Not current date"

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->ServerTimeMismatchError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 22
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "AgentInternalError"

    const/4 v5, 0x7

    const/16 v6, 0x12c

    const/16 v7, 0x12d

    const-string v8, "Internal error"

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->AgentInternalError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 25
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "ServerInternalError"

    const/16 v5, 0x8

    const/16 v6, 0x190

    const/16 v7, 0x191

    const-string v8, "Internal server error"

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->ServerInternalError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 28
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "NetworkDisconnectedError"

    const/16 v5, 0x9

    const/16 v6, 0x1f4

    const/16 v7, 0x1f5

    const-string v8, "Network disconnected"

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->NetworkDisconnectedError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 29
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "NetworkGeneralError"

    const/16 v5, 0xa

    const/16 v6, 0x1f4

    const/16 v7, 0x1f6

    const-string v8, "General network error"

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->NetworkGeneralError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 32
    new-instance v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    const-string v4, "UserDisagreeError"

    const/16 v5, 0xb

    const/16 v6, 0x258

    const/16 v7, 0x259

    const-string v8, "User Disagree Disclaimer Pop-up"

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .line 8
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->Success:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->CommonNullParamError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->CommonUnknownError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->LicenseInvalidError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->LicenseRegisterError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->LicenseTerminatedError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->ServerTimeMismatchError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->AgentInternalError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->ServerInternalError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->NetworkDisconnectedError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->NetworkGeneralError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->$VALUES:[Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILjava/lang/String;)V
    .locals 1
    .param p3, "moduleCode"    # I
    .param p4, "errorCode"    # I
    .param p5, "errorDesc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->mErrorDesc:Ljava/lang/String;

    .line 41
    iput p3, p0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->mModuleCode:I

    .line 42
    iput p4, p0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->mErrorCode:I

    .line 43
    iput-object p5, p0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->mErrorDesc:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->$VALUES:[Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    return-object v0
.end method


# virtual methods
.method public getModuleCode()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->mModuleCode:I

    return v0
.end method

.method public getModuleErrorCode()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->mErrorCode:I

    return v0
.end method

.method public getModuleErrorDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->mErrorDesc:Ljava/lang/String;

    return-object v0
.end method
