.class public Lcom/sec/esdk/elm/utils/Utility;
.super Ljava/lang/Object;
.source "Utility.java"


# static fields
.field static sPrefFileNameReadLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/utils/Utility;->sPrefFileNameReadLock:Ljava/lang/Object;

    return-void
.end method

.method public static IsPackageExistInDevice(Ljava/lang/String;)Z
    .locals 8
    .param p0, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 48
    const/4 v4, 0x0

    .line 50
    .local v4, "result":Z
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/16 v7, 0x2000

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v3

    .line 52
    .local v3, "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    if-nez v3, :cond_0

    .line 53
    const-string v6, "IsPackageExistInDevice(). mAppInfoList is Null"

    invoke-static {v6}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    move v5, v4

    .line 70
    .end local v3    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v4    # "result":Z
    .local v5, "result":I
    :goto_0
    return v5

    .line 57
    .end local v5    # "result":I
    .restart local v3    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v4    # "result":Z
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "IsPackageExistInDevice(). mAppInfoList:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 58
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 59
    .local v0, "appInfo":Landroid/content/pm/PackageInfo;
    iget-object v6, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 60
    const/4 v4, 0x1

    .line 61
    const-string v6, "IsPackageExistInDevice : exist "

    invoke-static {v6}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "appInfo":Landroid/content/pm/PackageInfo;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_2
    :goto_1
    move v5, v4

    .line 70
    .restart local v5    # "result":I
    goto :goto_0

    .line 66
    .end local v5    # "result":I
    :catch_0
    move-exception v1

    .line 67
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "IsPackageExistInDevice() has Exception."

    invoke-static {v6}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 68
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public static getRandomNumber()Ljava/lang/String;
    .locals 4

    .prologue
    .line 82
    const/4 v0, 0x0

    .line 83
    .local v0, "newAndroidIdValue":Ljava/lang/String;
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    .line 85
    .local v1, "random":Ljava/security/SecureRandom;
    if-eqz v1, :cond_0

    .line 86
    invoke-virtual {v1}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 88
    :cond_0
    return-object v0
.end method

.method public static isProduct_Ship(Landroid/content/Context;)Z
    .locals 3
    .param p0, "mContext"    # Landroid/content/Context;

    .prologue
    .line 92
    const/4 v0, 0x1

    .line 93
    .local v0, "Product_Ship":Z
    const-string v1, "ro.product_ship"

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Lcom/sec/esdk/elm/utils/SystemPropertiesProxy;->getBoolean(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 94
    return v0
.end method
