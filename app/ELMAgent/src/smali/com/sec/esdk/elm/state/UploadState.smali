.class public Lcom/sec/esdk/elm/state/UploadState;
.super Ljava/lang/Object;
.source "UploadState.java"


# direct methods
.method public static processUpload(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V
    .locals 3
    .param p0, "state"    # Lcom/sec/esdk/elm/type/States;
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 20
    :try_start_0
    sget-object v1, Lcom/sec/esdk/elm/type/States;->UPLOAD_REQUEST:Lcom/sec/esdk/elm/type/States;

    if-ne p0, v1, :cond_0

    .line 21
    const-string v1, "UploadState : processUpload() State is UPLOAD_REQUEST.processUploadRequest() Call."

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 22
    new-instance v2, Lcom/sec/esdk/elm/state/module/UploadRequestModule;

    invoke-direct {v2}, Lcom/sec/esdk/elm/state/module/UploadRequestModule;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/esdk/elm/type/UploadContainer;

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/state/module/UploadRequestModule;->processUploadRequest(Lcom/sec/esdk/elm/type/UploadContainer;)V

    .line 25
    :cond_0
    sget-object v1, Lcom/sec/esdk/elm/type/States;->UPLOAD_RESPONSE:Lcom/sec/esdk/elm/type/States;

    if-ne p0, v1, :cond_1

    .line 26
    const-string v1, "UploadState : processUpload() State is UPLOAD_RESPONSE. processUploadResponse() Call."

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 27
    new-instance v2, Lcom/sec/esdk/elm/state/module/UploadResponseModule;

    invoke-direct {v2}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/esdk/elm/type/UploadContainer;

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->processUploadResponse(Lcom/sec/esdk/elm/type/UploadContainer;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    :cond_1
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 30
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "UploadState : processUploadResponse() has NullPointException."

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 31
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
