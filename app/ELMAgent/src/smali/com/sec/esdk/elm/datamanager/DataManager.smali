.class public Lcom/sec/esdk/elm/datamanager/DataManager;
.super Ljava/lang/Object;
.source "DataManager.java"


# direct methods
.method public static compriseRegisterRequestData(Lcom/sec/esdk/elm/type/RegisterRequestContainer;Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;)Lorg/json/JSONObject;
    .locals 6
    .param p0, "registerRequestContainer"    # Lcom/sec/esdk/elm/type/RegisterRequestContainer;
    .param p1, "deviceInfoCollector"    # Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;

    .prologue
    const/4 v4, 0x0

    .line 24
    const-string v5, "DataManager.compriseRegisterRequestData()"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 28
    :try_start_0
    new-instance v0, Lcom/sec/esdk/elm/type/DeviceInfo;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/DeviceInfo;-><init>()V

    .line 29
    .local v0, "deviceInfo":Lcom/sec/esdk/elm/type/DeviceInfo;
    new-instance v2, Lcom/sec/esdk/elm/type/PackageInfo;

    invoke-direct {v2}, Lcom/sec/esdk/elm/type/PackageInfo;-><init>()V

    .line 31
    .local v2, "packageInfo":Lcom/sec/esdk/elm/type/PackageInfo;
    if-eqz p1, :cond_1

    .line 32
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getDeviceId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setIMEI(Ljava/lang/String;)V

    .line 33
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getModelName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setModel(Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getAndroidVersion()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setAndroidVersion(Ljava/lang/String;)V

    .line 35
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getBuildNumber()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setBuildNumber(Ljava/lang/String;)V

    .line 36
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getMCC()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setMCC(Ljava/lang/String;)V

    .line 37
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getEsdkVer()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setEsdkVer(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getDeviceCarrier()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setDeviceCarrier(Ljava/lang/String;)V

    .line 39
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getMNC1()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setSimCarrier1(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getMNC2()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setSimCarrier2(Ljava/lang/String;)V

    .line 41
    invoke-virtual {p1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getClientTimeZone()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/esdk/elm/type/DeviceInfo;->setCilentTimeZone(Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/sec/esdk/elm/type/PackageInfo;->setPackageName(Ljava/lang/String;)V

    .line 44
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageVer()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/sec/esdk/elm/type/PackageInfo;->setPackageVersion(Ljava/lang/String;)V

    .line 45
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getApkHash()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/sec/esdk/elm/type/PackageInfo;->setApkHash(Ljava/lang/String;)V

    .line 47
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getLicenseKey()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v0, v2}, Lcom/sec/esdk/elm/utils/JSON;->compriseRegisterRequestJSON(Ljava/lang/String;Lcom/sec/esdk/elm/type/DeviceInfo;Lcom/sec/esdk/elm/type/PackageInfo;)Lorg/json/JSONObject;

    move-result-object v3

    .line 48
    .local v3, "registerRequestJSON":Lorg/json/JSONObject;
    if-nez v3, :cond_0

    move-object v3, v4

    .line 62
    .end local v0    # "deviceInfo":Lcom/sec/esdk/elm/type/DeviceInfo;
    .end local v2    # "packageInfo":Lcom/sec/esdk/elm/type/PackageInfo;
    .end local v3    # "registerRequestJSON":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-object v3

    .line 53
    .restart local v0    # "deviceInfo":Lcom/sec/esdk/elm/type/DeviceInfo;
    .restart local v2    # "packageInfo":Lcom/sec/esdk/elm/type/PackageInfo;
    :cond_1
    const-string v5, "DataManager.compriseRegisterRequestData() Device Info Collecer is NULL"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v4

    .line 54
    goto :goto_0

    .line 56
    .end local v0    # "deviceInfo":Lcom/sec/esdk/elm/type/DeviceInfo;
    .end local v2    # "packageInfo":Lcom/sec/esdk/elm/type/PackageInfo;
    :catch_0
    move-exception v1

    .line 57
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "DataManager.compriseRegisterRequestData() : Exception was occurred."

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 58
    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    move-object v3, v4

    .line 59
    goto :goto_0
.end method

.method public static compriseUploadRequestData(Lcom/sec/esdk/elm/type/UploadRequestContainer;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 5
    .param p0, "uploadRequestContainer"    # Lcom/sec/esdk/elm/type/UploadRequestContainer;
    .param p1, "logData"    # Lorg/json/JSONObject;

    .prologue
    .line 83
    const-string v3, "DataManager.compriseUploadRequestData()"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 84
    new-instance v2, Lcom/sec/esdk/elm/type/PackageInfo;

    invoke-direct {v2}, Lcom/sec/esdk/elm/type/PackageInfo;-><init>()V

    .line 85
    .local v2, "packageInfo":Lcom/sec/esdk/elm/type/PackageInfo;
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/PackageInfo;->setPackageName(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/PackageInfo;->setPackageVersion(Ljava/lang/String;)V

    .line 87
    const/4 v0, 0x0

    .line 89
    .local v0, "apkHash":Ljava/lang/String;
    :try_start_0
    const-string v3, "apk_hash"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 90
    const-string v3, "apk_hash"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getApkHash()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    .line 96
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "compriseUploadRequestData : Package Removed."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p0, v0}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->setApkHash(Ljava/lang/String;)V

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getApkHash()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/PackageInfo;->setApkHash(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2, p1}, Lcom/sec/esdk/elm/utils/JSON;->compriseUploadRequestJSON(Ljava/lang/String;Lcom/sec/esdk/elm/type/PackageInfo;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v3

    return-object v3

    .line 91
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Lorg/json/JSONException;
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static compriseValidateRequestData(Lcom/sec/esdk/elm/type/ValidateRequestContainer;Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;)Lorg/json/JSONObject;
    .locals 4
    .param p0, "validateRequestContainer"    # Lcom/sec/esdk/elm/type/ValidateRequestContainer;
    .param p1, "mPushPoint"    # Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .prologue
    .line 66
    const-string v2, "DataManager.compriseValidateRequestData()"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 67
    const-string v1, ""

    .line 68
    .local v1, "validationPoint":Ljava/lang/String;
    new-instance v0, Lcom/sec/esdk/elm/type/PackageInfo;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/PackageInfo;-><init>()V

    .line 69
    .local v0, "packageInfo":Lcom/sec/esdk/elm/type/PackageInfo;
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/esdk/elm/type/PackageInfo;->setPackageName(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/esdk/elm/type/PackageInfo;->setPackageVersion(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getApkHash()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/esdk/elm/type/PackageInfo;->setApkHash(Ljava/lang/String;)V

    .line 73
    sget-object v2, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->KLMS_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    if-ne p1, v2, :cond_0

    .line 74
    invoke-virtual {p1}, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->toString()Ljava/lang/String;

    move-result-object v1

    .line 79
    :goto_0
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getClientTimezone()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v0, v3, v1}, Lcom/sec/esdk/elm/utils/JSON;->compriseValidateRequestJSON(Ljava/lang/String;Lcom/sec/esdk/elm/type/PackageInfo;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    return-object v2

    .line 76
    :cond_0
    const-string v1, ""

    goto :goto_0
.end method
