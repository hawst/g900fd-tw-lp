.class public Lcom/sec/esdk/elm/type/RegisterResponseContainer;
.super Ljava/lang/Object;
.source "RegisterResponseContainer.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private Error:Landroid/app/enterprise/license/Error;

.field private InstanceId:Ljava/lang/String;

.field private PackageName:Ljava/lang/String;

.field private PackageVer:Ljava/lang/String;

.field private RO:Landroid/app/enterprise/license/RightsObject;

.field private ServerTime:Ljava/lang/String;

.field private Status:Ljava/lang/String;

.field private mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/sec/esdk/elm/type/RegisterResponseContainer$1;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/RegisterResponseContainer$1;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->InstanceId:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageName:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageVer:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Status:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->ServerTime:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 29
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    .line 31
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 155
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 156
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->InstanceId:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageName:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageVer:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Status:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->ServerTime:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 29
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    .line 31
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 75
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->readFromParcel(Landroid/os/Parcel;)V

    .line 76
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method public getError()Landroid/app/enterprise/license/Error;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    return-object v0
.end method

.method public getInstanceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->InstanceId:Ljava/lang/String;

    return-object v0
.end method

.method public getLicensePermissionGroup()Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageVer:Ljava/lang/String;

    return-object v0
.end method

.method public getRO()Landroid/app/enterprise/license/RightsObject;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Status:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 47
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->InstanceId:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageName:Ljava/lang/String;

    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageVer:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Status:Ljava/lang/String;

    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->ServerTime:Ljava/lang/String;

    .line 53
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 54
    const-class v0, Landroid/app/enterprise/license/RightsObject;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/license/RightsObject;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    .line 56
    const-class v0, Landroid/app/enterprise/license/Error;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/license/Error;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 57
    return-void
.end method

.method public setError(Landroid/app/enterprise/license/Error;)V
    .locals 0
    .param p1, "error"    # Landroid/app/enterprise/license/Error;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 132
    return-void
.end method

.method public setInstanceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->InstanceId:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setLicensePermissionGroup(Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;)V
    .locals 0
    .param p1, "mLicensePermissionGroup"    # Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 140
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageName:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public setPackageVer(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageVer"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageVer:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setRO(Landroid/app/enterprise/license/RightsObject;)V
    .locals 0
    .param p1, "rO"    # Landroid/app/enterprise/license/RightsObject;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    .line 116
    return-void
.end method

.method public setServerTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverTime"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->ServerTime:Ljava/lang/String;

    .line 124
    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Status:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->InstanceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->PackageVer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Status:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->ServerTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 42
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 44
    return-void
.end method
