.class public final enum Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/datamanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MDMRequestPushPoint"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

.field public static final enum FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

.field public static final enum FROM_UMC:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;


# instance fields
.field private mPushPoint:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 108
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    const-string v1, "FROM_NORMAL"

    const-string v2, "FROM_NORMAL"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    const-string v1, "FROM_UMC"

    const-string v2, "FROM_UMC"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_UMC:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .line 107
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_UMC:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "pushPoint"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->mPushPoint:Ljava/lang/String;

    .line 113
    iput-object p3, p0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->mPushPoint:Ljava/lang/String;

    .line 114
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 107
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->mPushPoint:Ljava/lang/String;

    return-object v0
.end method
