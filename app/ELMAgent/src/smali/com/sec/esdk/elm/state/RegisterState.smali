.class public Lcom/sec/esdk/elm/state/RegisterState;
.super Ljava/lang/Object;
.source "RegisterState.java"


# direct methods
.method public static processRegister(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V
    .locals 3
    .param p0, "state"    # Lcom/sec/esdk/elm/type/States;
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 20
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processRegister:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/States;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 22
    :try_start_0
    sget-object v1, Lcom/sec/esdk/elm/type/States;->REGISTER_REQUEST:Lcom/sec/esdk/elm/type/States;

    if-ne p0, v1, :cond_1

    .line 23
    new-instance v2, Lcom/sec/esdk/elm/state/module/RegisterRequestModule;

    invoke-direct {v2}, Lcom/sec/esdk/elm/state/module/RegisterRequestModule;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/esdk/elm/type/RegisterContainer;

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/state/module/RegisterRequestModule;->processRegisterRequest(Lcom/sec/esdk/elm/type/RegisterContainer;)V

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    sget-object v1, Lcom/sec/esdk/elm/type/States;->REGISTER_RESPONSE:Lcom/sec/esdk/elm/type/States;

    if-ne p0, v1, :cond_2

    .line 26
    new-instance v2, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;

    invoke-direct {v2}, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/esdk/elm/type/RegisterContainer;

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;->processRegisterResponse(Lcom/sec/esdk/elm/type/RegisterContainer;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "RegisterState : processRegister() has NullPointException."

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 37
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 28
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_2
    :try_start_1
    sget-object v1, Lcom/sec/esdk/elm/type/States;->KLMS_REQUEST:Lcom/sec/esdk/elm/type/States;

    if-ne p0, v1, :cond_3

    .line 30
    new-instance v2, Lcom/sec/esdk/elm/state/module/KLMSRequestModule;

    invoke-direct {v2}, Lcom/sec/esdk/elm/state/module/KLMSRequestModule;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/state/module/KLMSRequestModule;->processKLMSRequest(Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 38
    :catch_1
    move-exception v0

    .line 39
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "RegisterState : processRegister() has Exception."

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 40
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 31
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_2
    sget-object v1, Lcom/sec/esdk/elm/type/States;->KLMS_RESPONSE:Lcom/sec/esdk/elm/type/States;

    if-ne p0, v1, :cond_0

    .line 32
    new-instance v2, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;

    invoke-direct {v2}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->processKLMSResponse(Landroid/os/Bundle;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0
.end method
