.class public Lcom/sec/esdk/elm/service/MDMBridge;
.super Ljava/lang/Object;
.source "MDMBridge.java"


# static fields
.field protected static mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    return-void
.end method

.method private static declared-synchronized RecoverySKeyStatus(Lcom/sec/esdk/elm/utils/RedirectionDataUtil;Z)V
    .locals 5
    .param p0, "GSLBInfoPref"    # Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .param p1, "IsRecovery"    # Z

    .prologue
    const/4 v4, 0x1

    .line 149
    const-class v3, Lcom/sec/esdk/elm/service/MDMBridge;

    monitor-enter v3

    :try_start_0
    const-string v2, "ModuleBase.RecoverySKeyStatus()"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 150
    if-ne p1, v4, :cond_0

    .line 151
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setSURL(Ljava/lang/String;)V

    .line 152
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setELMValidateOption(Z)V

    .line 153
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setSPDOption(Z)V

    .line 154
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setSELMActivated(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    :try_start_1
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 157
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v2, "elm_skey_activation"

    const/4 v4, 0x0

    invoke-static {v0, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 158
    const-string v2, "elm_skey_activation_state"

    const/4 v4, 0x2

    invoke-static {v0, v2, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 159
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MDMBridge.RecoverySKeyStatus : Get elm_skey_activation_state = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "elm_skey_activation_state"

    invoke-static {v0, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 160
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MDMBridge.RecoverySKeyStatus : Get elm_skey_activation = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "elm_skey_activation"

    invoke-static {v0, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 168
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :goto_0
    :try_start_2
    const-string v2, "ModuleBase.RecoverySKeyStatus : ELM S key doesn\'t be activated."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 170
    :cond_0
    monitor-exit v3

    return-void

    .line 161
    :catch_0
    move-exception v1

    .line 162
    .local v1, "e":Landroid/provider/Settings$SettingNotFoundException;
    :try_start_3
    const-string v2, "ModuleBase.RecoverySKeyStatus : SettingNotFoundException."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 163
    invoke-virtual {v1}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 149
    .end local v1    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 164
    :catch_1
    move-exception v1

    .line 165
    .local v1, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v2, "ModuleBase.RecoverySKeyStatus : Exception."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 166
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public static declared-synchronized RecoverySKeyStatusProcessing()Z
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 98
    const-class v9, Lcom/sec/esdk/elm/service/MDMBridge;

    monitor-enter v9

    :try_start_0
    const-string v8, "ModuleBase.RecoverySKeyStatusProcessing()."

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    const/4 v1, 0x0

    .line 101
    .local v1, "IsRecovery":Z
    :try_start_1
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 102
    .local v0, "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v2

    .line 103
    .local v2, "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    if-eqz v0, :cond_1

    .line 104
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSELMActivated()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 105
    const/4 v1, 0x1

    .line 106
    if-eqz v2, :cond_0

    .line 107
    const/4 v5, 0x0

    .local v5, "licenseCount":I
    :goto_0
    array-length v8, v2

    if-ge v5, v8, :cond_0

    .line 108
    const/4 v7, 0x0

    .line 109
    .local v7, "rightsObject":Landroid/app/enterprise/license/RightsObject;
    aget-object v8, v2, v5

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v4

    .line 110
    .local v4, "id":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v7

    .line 111
    if-eqz v7, :cond_2

    .line 112
    invoke-virtual {v7}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v8

    sget-object v10, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->LICENSE_TYPE_ONS_STR:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v10}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 113
    const-string v8, "MDMBridge.RecoverySKeyStatusProcessing(). ONS Exist."

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 114
    const/4 v1, 0x0

    .line 122
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "licenseCount":I
    .end local v7    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_0
    if-ne v1, v11, :cond_1

    .line 123
    invoke-static {v0, v1}, Lcom/sec/esdk/elm/service/MDMBridge;->RecoverySKeyStatus(Lcom/sec/esdk/elm/utils/RedirectionDataUtil;Z)V

    .line 125
    new-instance v6, Landroid/content/Intent;

    const-string v8, "com.sec.esdk.elm.skey.STATE"

    invoke-direct {v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 126
    .local v6, "mIntent":Landroid/content/Intent;
    const-string v8, "com.sec.knox.seandroid"

    invoke-virtual {v6, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    const-string v8, "IS_SELM_ACTIVATION"

    const/4 v10, 0x2

    invoke-virtual {v6, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 128
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 139
    .end local v0    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .end local v1    # "IsRecovery":Z
    .end local v2    # "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    .end local v6    # "mIntent":Landroid/content/Intent;
    :cond_1
    :goto_1
    monitor-exit v9

    return v1

    .line 107
    .restart local v0    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .restart local v1    # "IsRecovery":Z
    .restart local v2    # "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v5    # "licenseCount":I
    .restart local v7    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 130
    .end local v4    # "id":Ljava/lang/String;
    .end local v5    # "licenseCount":I
    .end local v7    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_3
    :try_start_2
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSURL()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 131
    const/4 v8, 0x1

    invoke-static {v0, v8}, Lcom/sec/esdk/elm/service/MDMBridge;->RecoverySKeyStatus(Lcom/sec/esdk/elm/utils/RedirectionDataUtil;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 135
    .end local v0    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .end local v2    # "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    :catch_0
    move-exception v3

    .line 136
    .local v3, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 137
    const/4 v1, 0x0

    goto :goto_1

    .line 98
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    monitor-exit v9

    throw v8
.end method

.method public static deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)V
    .locals 3
    .param p0, "status"    # Ljava/lang/String;
    .param p1, "instnceID"    # Ljava/lang/String;
    .param p2, "error"    # Landroid/app/enterprise/license/Error;

    .prologue
    .line 68
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MDMBridge.deleteApiCallData( status:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", instnceID:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ... )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 69
    invoke-static {p2, p0}, Lcom/sec/esdk/elm/service/MDMBridge;->getErrorMapping(Landroid/app/enterprise/license/Error;Ljava/lang/String;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    .line 70
    .local v0, "mErrorModule":Landroid/app/enterprise/license/Error;
    sget-object v1, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-virtual {v1, p0, p1, v0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)Z

    .line 71
    return-void
.end method

.method public static deleteLicense(Ljava/lang/String;)Z
    .locals 2
    .param p0, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MDMBridge.deleteLicense( instanceId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 213
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-virtual {v0, p0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->deleteLicense(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;
    .locals 1

    .prologue
    .line 50
    const-string v0, "MDMBridge.getAllLicenseInfoFromSDK()"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 51
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-virtual {v0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getAllLicenseInfo()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v0

    return-object v0
.end method

.method public static getApiCallData(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 2
    .param p0, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MDMBridge.getApiCallData( instanceId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 198
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-virtual {v0, p0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getApiCallData(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    return-object v0
.end method

.method public static getELMLicenseKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MDMBridge.getELMLicenseKey( packageName: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 218
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-virtual {v0, p0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getELMLicenseKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getErrorMapping(Landroid/app/enterprise/license/Error;Ljava/lang/String;)Landroid/app/enterprise/license/Error;
    .locals 4
    .param p0, "error"    # Landroid/app/enterprise/license/Error;
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "mErrorModule":Landroid/app/enterprise/license/Error;
    const-string v1, "success"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 230
    new-instance v1, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;

    invoke-direct {v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;-><init>()V

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerSuccess:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerSuccess:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getError(II)Landroid/app/enterprise/license/Error;

    move-result-object v0

    .line 241
    :goto_0
    return-object v0

    .line 233
    :cond_0
    if-eqz p0, :cond_1

    .line 234
    new-instance v1, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;

    invoke-direct {v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;-><init>()V

    invoke-virtual {p0}, Landroid/app/enterprise/license/Error;->getHttpResponseCode()I

    move-result v2

    invoke-virtual {p0}, Landroid/app/enterprise/license/Error;->getErrorCode()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getError(II)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0

    .line 236
    :cond_1
    new-instance v1, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;

    invoke-direct {v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;-><init>()V

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerNullInputParam:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerNullInputParam:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getError(II)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0
.end method

.method public static declared-synchronized getInstance()V
    .locals 2

    .prologue
    .line 31
    const-class v1, Lcom/sec/esdk/elm/service/MDMBridge;

    monitor-enter v1

    :try_start_0
    const-string v0, "MDMBridge.getInstance()"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 32
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    if-nez v0, :cond_0

    .line 33
    const-string v0, "MDMBridge.getInstance() mESDKLicenseManager is null"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 34
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-result-object v0

    sput-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_0
    monitor-exit v1

    return-void

    .line 31
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getLicenseInfoFromSDK(Ljava/lang/String;)Landroid/app/enterprise/license/LicenseInfo;
    .locals 2
    .param p0, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MDMBridge.getLicenseInfoFromSDK( instanceId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 59
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-virtual {v0, p0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getLicenseInfo(Ljava/lang/String;)Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v0

    return-object v0
.end method

.method public static getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;
    .locals 2
    .param p0, "instanceID"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MDMBridge.getRightsObject(  instnceID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 94
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-virtual {v0, p0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v0

    return-object v0
.end method

.method public static processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)V
    .locals 6
    .param p0, "status"    # Ljava/lang/String;
    .param p1, "instanceID"    # Ljava/lang/String;
    .param p2, "packagName"    # Ljava/lang/String;
    .param p3, "RO"    # Landroid/app/enterprise/license/RightsObject;
    .param p4, "error"    # Landroid/app/enterprise/license/Error;
    .param p5, "permGroup"    # Ljava/lang/String;

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MDMBridge.processLicenseValidationResult( status:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", instnceID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",permGroup: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "... )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 76
    invoke-static {p4, p0}, Lcom/sec/esdk/elm/service/MDMBridge;->getErrorMapping(Landroid/app/enterprise/license/Error;Ljava/lang/String;)Landroid/app/enterprise/license/Error;

    move-result-object v4

    .line 77
    .local v4, "mErrorModule":Landroid/app/enterprise/license/Error;
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z

    .line 78
    invoke-static {p2}, Lcom/sec/esdk/elm/service/MDMBridge;->sendIntenttoKLMS(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public static resetLicense(Ljava/lang/String;)Z
    .locals 2
    .param p0, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MDMBridge.resetLicense( instanceId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 208
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-virtual {v0, p0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->resetLicense(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static sendIntenttoKLMS(Ljava/lang/String;)V
    .locals 3
    .param p0, "packageName"    # Ljava/lang/String;

    .prologue
    .line 86
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.esdk.elm.action.NOTICE_PERMISSON_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 87
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v1, "com.samsung.klmsagent"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    const-string v1, "ELM_PACKAGE_NAME"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.sec.esdk.elm.permission.ELM_INTENT_RECEIVE"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 90
    return-void
.end method

.method public static setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z
    .locals 8
    .param p0, "packageName"    # Ljava/lang/String;
    .param p1, "packageVersion"    # Ljava/lang/String;
    .param p2, "status"    # Ljava/lang/String;
    .param p3, "instanceId"    # Ljava/lang/String;
    .param p4, "RO"    # Landroid/app/enterprise/license/RightsObject;
    .param p5, "error"    # Landroid/app/enterprise/license/Error;
    .param p6, "mMDMRequestPushPoint"    # Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;
    .param p7, "permGroup"    # Ljava/lang/String;

    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MDMBridge.setActivationResult(  packageName:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", packageVersion:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", status:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", instanceId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", permGroup:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ... )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 177
    invoke-static {p5, p2}, Lcom/sec/esdk/elm/service/MDMBridge;->getErrorMapping(Landroid/app/enterprise/license/Error;Ljava/lang/String;)Landroid/app/enterprise/license/Error;

    move-result-object v6

    .line 180
    .local v6, "mErrorModule":Landroid/app/enterprise/license/Error;
    const-string v0, "fail"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->RecoverySKeyStatusProcessing()Z

    .line 183
    :cond_0
    if-nez p6, :cond_1

    .line 184
    sget-object p6, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .line 186
    :cond_1
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_UMC:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    if-ne p6, v0, :cond_2

    .line 187
    const-string v0, "MDMBridge.setActivationResult( FROM_UMC )"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 188
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->processLicenseActivationResponseForUMC(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z

    move-result v0

    .line 192
    :goto_0
    return v0

    .line 191
    :cond_2
    const-string v0, "MDMBridge.setActivationResult( FROM_NORMAL )"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 192
    sget-object v0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->processLicenseActivationResponse(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static declared-synchronized setEnterpriseBridge(Landroid/app/enterprise/license/EnterpriseLicenseManager;)V
    .locals 2
    .param p0, "in_ESDK_LicenseManager"    # Landroid/app/enterprise/license/EnterpriseLicenseManager;

    .prologue
    .line 42
    const-class v1, Lcom/sec/esdk/elm/service/MDMBridge;

    monitor-enter v1

    :try_start_0
    const-string v0, "MDMBridge.setEnterpriseBridge()"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 43
    sput-object p0, Lcom/sec/esdk/elm/service/MDMBridge;->mESDKLicenseManager:Landroid/app/enterprise/license/EnterpriseLicenseManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    monitor-exit v1

    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
