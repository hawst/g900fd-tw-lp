.class public Lcom/sec/esdk/elm/utils/ELMFileUtil;
.super Ljava/lang/Object;
.source "ELMFileUtil.java"


# static fields
.field private static elmFileUtilFactory:Lcom/sec/esdk/elm/utils/ELMFileUtil;


# instance fields
.field private mPreferences:Lcom/sec/esdk/elm/utils/Preferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->elmFileUtilFactory:Lcom/sec/esdk/elm/utils/ELMFileUtil;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->mPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    return-void
.end method

.method private decryptAES([B)[B
    .locals 5
    .param p1, "encrypted"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 145
    const-string v3, "ELMFileUtil.decryptAES()."

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 146
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->mPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    .line 148
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v3, p0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->mPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/utils/Preferences;->getSessionSEED()[B

    move-result-object v3

    const-string v4, "AES"

    invoke-direct {v2, v3, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 149
    .local v2, "skeySpec":Ljavax/crypto/spec/SecretKeySpec;
    const-string v3, "AES"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 150
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 151
    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 152
    .local v1, "decrypted":[B
    return-object v1
.end method

.method private encryptAES([B)[B
    .locals 5
    .param p1, "clear"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 133
    const-string v3, "ELMFileUtil.encryptAES()."

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 134
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->mPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    .line 136
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    iget-object v3, p0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->mPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/utils/Preferences;->getSessionSEED()[B

    move-result-object v3

    const-string v4, "AES"

    invoke-direct {v2, v3, v4}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 137
    .local v2, "skeySpec":Ljavax/crypto/spec/SecretKeySpec;
    const-string v3, "AES"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 138
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 139
    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 141
    .local v1, "encrypted":[B
    return-object v1
.end method

.method public static getELMFileUtil()Lcom/sec/esdk/elm/utils/ELMFileUtil;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->elmFileUtilFactory:Lcom/sec/esdk/elm/utils/ELMFileUtil;

    if-nez v0, :cond_0

    .line 126
    new-instance v0, Lcom/sec/esdk/elm/utils/ELMFileUtil;

    invoke-direct {v0}, Lcom/sec/esdk/elm/utils/ELMFileUtil;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->elmFileUtilFactory:Lcom/sec/esdk/elm/utils/ELMFileUtil;

    .line 128
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->elmFileUtilFactory:Lcom/sec/esdk/elm/utils/ELMFileUtil;

    return-object v0
.end method


# virtual methods
.method public RemoveActInformation()V
    .locals 3

    .prologue
    .line 111
    :try_start_0
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/.ActInfo/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ActInfor.dat"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .local v0, "logFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 122
    .end local v0    # "logFile":Ljava/io/File;
    :goto_0
    return-void

    .line 115
    .restart local v0    # "logFile":Ljava/io/File;
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 116
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->mPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    .line 117
    iget-object v1, p0, Lcom/sec/esdk/elm/utils/ELMFileUtil;->mPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/utils/Preferences;->InitSessionSEED()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 118
    .end local v0    # "logFile":Ljava/io/File;
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getAPICallLogFromFile(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 18
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 156
    const-string v15, "ELMFileUtil.getAPICallLogFromFile()."

    invoke-static {v15}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 158
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 159
    :cond_0
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ELMFileUtil.getAPICallLogFromFile(). packageName:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", instanceId:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 160
    const/4 v14, 0x0

    .line 205
    :goto_0
    return-object v14

    .line 164
    :cond_1
    :try_start_0
    invoke-static/range {p2 .. p2}, Lcom/sec/esdk/elm/service/MDMBridge;->getApiCallData(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 166
    .local v3, "esdkAPICallLog":Lorg/json/JSONArray;
    if-eqz v3, :cond_2

    .line 167
    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    .line 168
    .local v11, "fileLogData":Lorg/json/JSONObject;
    new-instance v12, Lorg/json/JSONArray;

    invoke-direct {v12}, Lorg/json/JSONArray;-><init>()V

    .line 169
    .local v12, "fileLogDatesValue":Lorg/json/JSONArray;
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 170
    .local v9, "fileApiCall":Lorg/json/JSONObject;
    const/4 v13, 0x0

    .line 171
    .local v13, "pkgAPKHash":Ljava/lang/String;
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-static {v15, v0}, Lcom/sec/esdk/elm/utils/JSON;->getAPKHashString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 172
    const-string v15, "apk_hash"

    invoke-virtual {v11, v15, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 174
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "ELMFileUtil.getAPICallLogFromFile(). InstanceID : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ",pkgName:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ", hash:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 175
    const/4 v7, 0x0

    .local v7, "esdkCallLogIndex":I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v15

    if-ge v7, v15, :cond_5

    .line 176
    invoke-virtual {v3, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 177
    .local v8, "esdkLogSet":Lorg/json/JSONObject;
    const-string v15, "api_call"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 178
    .local v4, "esdkApiCall":Lorg/json/JSONObject;
    const-string v15, "log_date"

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v12, v7, v15}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    .line 179
    invoke-virtual {v4}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    .line 181
    .local v5, "esdkApiCallIter":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    .line 182
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 183
    .local v6, "esdkApiName":Ljava/lang/String;
    invoke-virtual {v9, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 184
    invoke-virtual {v9, v6}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    .line 185
    .local v10, "fileApiCallValue":Lorg/json/JSONArray;
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v10, v7, v0, v1}, Lorg/json/JSONArray;->put(IJ)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 199
    .end local v3    # "esdkAPICallLog":Lorg/json/JSONArray;
    .end local v4    # "esdkApiCall":Lorg/json/JSONObject;
    .end local v5    # "esdkApiCallIter":Ljava/util/Iterator;
    .end local v6    # "esdkApiName":Ljava/lang/String;
    .end local v7    # "esdkCallLogIndex":I
    .end local v8    # "esdkLogSet":Lorg/json/JSONObject;
    .end local v9    # "fileApiCall":Lorg/json/JSONObject;
    .end local v10    # "fileApiCallValue":Lorg/json/JSONArray;
    .end local v11    # "fileLogData":Lorg/json/JSONObject;
    .end local v12    # "fileLogDatesValue":Lorg/json/JSONArray;
    .end local v13    # "pkgAPKHash":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 200
    .local v2, "e":Lorg/json/JSONException;
    const-string v15, "success"

    const/16 v16, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-static {v15, v0, v1}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)V

    .line 201
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    .line 204
    .end local v2    # "e":Lorg/json/JSONException;
    :cond_2
    const-string v15, "ELMFileUtil.getAPICallLogFromFile(). return Null"

    invoke-static {v15}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 205
    const/4 v14, 0x0

    goto/16 :goto_0

    .line 187
    .restart local v3    # "esdkAPICallLog":Lorg/json/JSONArray;
    .restart local v4    # "esdkApiCall":Lorg/json/JSONObject;
    .restart local v5    # "esdkApiCallIter":Ljava/util/Iterator;
    .restart local v6    # "esdkApiName":Ljava/lang/String;
    .restart local v7    # "esdkCallLogIndex":I
    .restart local v8    # "esdkLogSet":Lorg/json/JSONObject;
    .restart local v9    # "fileApiCall":Lorg/json/JSONObject;
    .restart local v11    # "fileLogData":Lorg/json/JSONObject;
    .restart local v12    # "fileLogDatesValue":Lorg/json/JSONArray;
    .restart local v13    # "pkgAPKHash":Ljava/lang/String;
    :cond_3
    :try_start_1
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V

    .line 188
    .restart local v10    # "fileApiCallValue":Lorg/json/JSONArray;
    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v16

    move-wide/from16 v0, v16

    invoke-virtual {v10, v7, v0, v1}, Lorg/json/JSONArray;->put(IJ)Lorg/json/JSONArray;

    .line 189
    invoke-virtual {v9, v6, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 175
    .end local v6    # "esdkApiName":Ljava/lang/String;
    .end local v10    # "fileApiCallValue":Lorg/json/JSONArray;
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 194
    .end local v4    # "esdkApiCall":Lorg/json/JSONObject;
    .end local v5    # "esdkApiCallIter":Ljava/util/Iterator;
    .end local v8    # "esdkLogSet":Lorg/json/JSONObject;
    :cond_5
    const-string v15, "log_dates"

    invoke-virtual {v11, v15, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 195
    const-string v15, "api_call"

    invoke-virtual {v11, v15, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 196
    new-instance v14, Lorg/json/JSONObject;

    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v15

    const-string v16, "null"

    const-string v17, "0"

    invoke-virtual/range {v15 .. v17}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 197
    .local v14, "removeNullJSON":Lorg/json/JSONObject;
    goto/16 :goto_0
.end method

.method public getActInformation()Lorg/json/JSONArray;
    .locals 10

    .prologue
    .line 76
    const/4 v6, 0x0

    .line 78
    .local v6, "retObject":Lorg/json/JSONArray;
    :try_start_0
    new-instance v4, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/.ActInfo/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "ActInfor.dat"

    invoke-direct {v4, v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .local v4, "logFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v8

    if-nez v8, :cond_0

    .line 80
    const/4 v8, 0x0

    .line 106
    .end local v4    # "logFile":Ljava/io/File;
    :goto_0
    return-object v8

    .line 82
    .restart local v4    # "logFile":Ljava/io/File;
    :cond_0
    const/4 v2, 0x0

    .line 83
    .local v2, "fileInputStream":Ljava/io/FileInputStream;
    const/4 v1, 0x0

    .line 85
    .local v1, "encryptByte":[B
    :try_start_1
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v4}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 87
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .local v3, "fileInputStream":Ljava/io/FileInputStream;
    :try_start_2
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v8

    long-to-int v8, v8

    new-array v1, v8, [B

    .line 88
    invoke-virtual {v3, v1}, Ljava/io/FileInputStream;->read([B)I

    .line 89
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move-object v2, v3

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    move-object v7, v6

    .line 100
    .end local v6    # "retObject":Lorg/json/JSONArray;
    .local v7, "retObject":Ljava/lang/Object;
    :goto_1
    :try_start_3
    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->decryptAES([B)[B

    move-result-object v5

    .line 101
    .local v5, "plainByte":[B
    new-instance v6, Lorg/json/JSONArray;

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v5}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v6, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .end local v1    # "encryptByte":[B
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v4    # "logFile":Ljava/io/File;
    .end local v5    # "plainByte":[B
    .end local v7    # "retObject":Ljava/lang/Object;
    .restart local v6    # "retObject":Lorg/json/JSONArray;
    :goto_2
    move-object v8, v6

    .line 106
    goto :goto_0

    .line 90
    .restart local v1    # "encryptByte":[B
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "logFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_4
    const-string v8, "ELMFileUtil.getActInformation(). Exception."

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 92
    const/4 v6, 0x0

    .line 93
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 94
    if-eqz v2, :cond_1

    .line 95
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 96
    new-instance v8, Ljava/lang/Exception;

    invoke-direct {v8}, Ljava/lang/Exception;-><init>()V

    throw v8
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 102
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "encryptByte":[B
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .end local v4    # "logFile":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 103
    .end local v6    # "retObject":Lorg/json/JSONArray;
    .restart local v0    # "e":Ljava/lang/Exception;
    :goto_4
    const-string v8, "ELMFileUtil.getActInformation(). Exception."

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 104
    const/4 v6, 0x0

    .restart local v6    # "retObject":Lorg/json/JSONArray;
    goto :goto_2

    .line 102
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v6    # "retObject":Lorg/json/JSONArray;
    .restart local v1    # "encryptByte":[B
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v4    # "logFile":Ljava/io/File;
    .restart local v7    # "retObject":Ljava/lang/Object;
    :catch_2
    move-exception v0

    move-object v6, v7

    .end local v7    # "retObject":Ljava/lang/Object;
    .local v6, "retObject":Ljava/lang/Object;
    goto :goto_4

    .line 90
    .end local v2    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .local v6, "retObject":Lorg/json/JSONArray;
    :catch_3
    move-exception v0

    move-object v2, v3

    .end local v3    # "fileInputStream":Ljava/io/FileInputStream;
    .restart local v2    # "fileInputStream":Ljava/io/FileInputStream;
    goto :goto_3

    .restart local v0    # "e":Ljava/lang/Exception;
    :cond_1
    move-object v7, v6

    .restart local v7    # "retObject":Ljava/lang/Object;
    goto :goto_1
.end method

.method public setActInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 14
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "pkgVer"    # Ljava/lang/String;
    .param p3, "LicenseType"    # Ljava/lang/String;
    .param p4, "mRequestPushPoint"    # Ljava/lang/String;

    .prologue
    .line 29
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "ELMFileUtil.setActInformation(). pkgName:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", pkgVer:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "Type:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 30
    const/4 v11, 0x0

    .line 31
    .local v11, "result":Z
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 32
    .local v1, "InnerJSONObject":Lorg/json/JSONObject;
    const/4 v2, 0x0

    .line 35
    .local v2, "OuterJSONArray":Lorg/json/JSONArray;
    :try_start_0
    new-instance v9, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/.ActInfo/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const-string v13, "ActInfor.dat"

    invoke-direct {v9, v12, v13}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .local v9, "logFile":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_0

    .line 37
    new-instance v4, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "/.ActInfo/"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v4, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 38
    .local v4, "dir":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 40
    .end local v4    # "dir":Ljava/io/File;
    :cond_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 42
    .end local v2    # "OuterJSONArray":Lorg/json/JSONArray;
    .local v3, "OuterJSONArray":Lorg/json/JSONArray;
    :try_start_1
    const-string v12, "pkgName"

    invoke-virtual {v1, v12, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 43
    const-string v12, "pkgVer"

    move-object/from16 v0, p2

    invoke-virtual {v1, v12, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 44
    const-string v12, "type"

    move-object/from16 v0, p3

    invoke-virtual {v1, v12, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 45
    const-string v12, "MDMPushPoint"

    move-object/from16 v0, p4

    invoke-virtual {v1, v12, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 47
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v12

    invoke-virtual {v3, v12, v1}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    .line 48
    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 49
    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    .line 50
    .local v10, "plainByte":[B
    invoke-direct {p0, v10}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->encryptAES([B)[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    .line 51
    .local v6, "encryptByte":[B
    const/4 v7, 0x0

    .line 53
    .local v7, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v8, Ljava/io/FileOutputStream;

    const/4 v12, 0x0

    invoke-direct {v8, v9, v12}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 54
    .end local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    .local v8, "fileOutputStream":Ljava/io/FileOutputStream;
    :try_start_3
    invoke-virtual {v8, v6}, Ljava/io/FileOutputStream;->write([B)V

    .line 55
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->flush()V

    .line 56
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-object v7, v8

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    :cond_1
    move-object v2, v3

    .line 72
    .end local v3    # "OuterJSONArray":Lorg/json/JSONArray;
    .end local v6    # "encryptByte":[B
    .end local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v9    # "logFile":Ljava/io/File;
    .end local v10    # "plainByte":[B
    .restart local v2    # "OuterJSONArray":Lorg/json/JSONArray;
    :goto_0
    return v11

    .line 57
    .end local v2    # "OuterJSONArray":Lorg/json/JSONArray;
    .restart local v3    # "OuterJSONArray":Lorg/json/JSONArray;
    .restart local v6    # "encryptByte":[B
    .restart local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "logFile":Ljava/io/File;
    .restart local v10    # "plainByte":[B
    :catch_0
    move-exception v5

    .line 58
    .local v5, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_4
    const-string v12, "ELMFileUtil.setActInformation(). Exception."

    invoke-static {v12}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 59
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    .line 60
    if-eqz v7, :cond_1

    .line 61
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->flush()V

    .line 62
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    .line 63
    new-instance v12, Ljava/lang/Exception;

    invoke-direct {v12}, Ljava/lang/Exception;-><init>()V

    throw v12
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 67
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v6    # "encryptByte":[B
    .end local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v10    # "plainByte":[B
    :catch_1
    move-exception v5

    move-object v2, v3

    .line 68
    .end local v3    # "OuterJSONArray":Lorg/json/JSONArray;
    .end local v9    # "logFile":Ljava/io/File;
    .restart local v2    # "OuterJSONArray":Lorg/json/JSONArray;
    .restart local v5    # "e":Ljava/lang/Exception;
    :goto_2
    const-string v12, "ELMFileUtil.setActInformation(). Exception."

    invoke-static {v12}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 67
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v5

    goto :goto_2

    .line 57
    .end local v2    # "OuterJSONArray":Lorg/json/JSONArray;
    .restart local v3    # "OuterJSONArray":Lorg/json/JSONArray;
    .restart local v6    # "encryptByte":[B
    .restart local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "logFile":Ljava/io/File;
    .restart local v10    # "plainByte":[B
    :catch_3
    move-exception v5

    move-object v7, v8

    .end local v8    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "fileOutputStream":Ljava/io/FileOutputStream;
    goto :goto_1
.end method
