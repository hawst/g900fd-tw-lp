.class public Lcom/sec/esdk/elm/state/module/RegisterRequestModule;
.super Lcom/sec/esdk/elm/state/module/ModuleBase;
.source "RegisterRequestModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;-><init>()V

    return-void
.end method


# virtual methods
.method public processRegisterRequest(Lcom/sec/esdk/elm/type/RegisterContainer;)V
    .locals 18
    .param p1, "registerContainer"    # Lcom/sec/esdk/elm/type/RegisterContainer;

    .prologue
    .line 36
    const-string v2, "RegisterRequestModule.processRegisterRequest()."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 39
    const/16 v17, 0x0

    .line 40
    .local v17, "registerRequestJSON":Lorg/json/JSONObject;
    const/4 v11, 0x0

    .line 41
    .local v11, "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    const/4 v15, 0x0

    .line 42
    .local v15, "mLicenseKey":Ljava/lang/String;
    const/4 v7, 0x0

    .line 47
    .local v7, "mError":Landroid/app/enterprise/license/Error;
    :try_start_0
    new-instance v12, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v12, v2}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 48
    .end local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .local v12, "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getLicenseKey()Ljava/lang/String;

    move-result-object v15

    .line 50
    if-nez v15, :cond_0

    .line 51
    const-string v2, "RegisterRequestModule.processRegisterRequest(). Recevice LicenseKey is null"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 52
    new-instance v14, Landroid/app/enterprise/license/Error;

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v14, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    .line 54
    .end local v7    # "mError":Landroid/app/enterprise/license/Error;
    .local v14, "mError":Landroid/app/enterprise/license/Error;
    :try_start_2
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 93
    :catch_0
    move-exception v13

    move-object v7, v14

    .end local v14    # "mError":Landroid/app/enterprise/license/Error;
    .restart local v7    # "mError":Landroid/app/enterprise/license/Error;
    move-object v11, v12

    .line 94
    .end local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .local v13, "e":Ljava/lang/InterruptedException;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RegisterRequestModule.processRegisterRequest(). InterruptedException. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v13}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 96
    new-instance v7, Landroid/app/enterprise/license/Error;

    .end local v7    # "mError":Landroid/app/enterprise/license/Error;
    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v7, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 98
    .restart local v7    # "mError":Landroid/app/enterprise/license/Error;
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 99
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageVer()Ljava/lang/String;

    move-result-object v3

    const-string v4, "fail"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v8

    sget-object v9, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v2 .. v9}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    .line 113
    .end local v13    # "e":Ljava/lang/InterruptedException;
    :goto_1
    return-void

    .line 58
    .end local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    :cond_0
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v2

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/esdk/elm/utils/JSON;->getAPKHashString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->setApkHash(Ljava/lang/String;)V

    .line 60
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v2

    invoke-static {v2, v12}, Lcom/sec/esdk/elm/datamanager/DataManager;->compriseRegisterRequestData(Lcom/sec/esdk/elm/type/RegisterRequestContainer;Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;)Lorg/json/JSONObject;

    move-result-object v17

    .line 62
    if-eqz v17, :cond_4

    .line 63
    new-instance v16, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;-><init>(Landroid/content/Context;)V

    .line 67
    .local v16, "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/esdk/elm/state/module/RegisterRequestModule;->StartToGSLB()Z

    move-result v2

    if-nez v2, :cond_2

    .line 68
    const-string v2, "RegisterRequestModule.processRegisterRequest(). StartToGSLB() return False."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 69
    new-instance v14, Landroid/app/enterprise/license/Error;

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerGeneralNetworkException:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerGeneralNetworkException:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v14, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    .line 71
    .end local v7    # "mError":Landroid/app/enterprise/license/Error;
    .restart local v14    # "mError":Landroid/app/enterprise/license/Error;
    :try_start_4
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 102
    .end local v16    # "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    :catch_1
    move-exception v13

    move-object v7, v14

    .end local v14    # "mError":Landroid/app/enterprise/license/Error;
    .restart local v7    # "mError":Landroid/app/enterprise/license/Error;
    move-object v11, v12

    .line 103
    .end local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .local v13, "e":Ljava/lang/Exception;
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RegisterRequestModule.processRegisterRequest(). Exception. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 105
    if-nez v7, :cond_1

    .line 106
    new-instance v7, Landroid/app/enterprise/license/Error;

    .end local v7    # "mError":Landroid/app/enterprise/license/Error;
    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v7, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 108
    .restart local v7    # "mError":Landroid/app/enterprise/license/Error;
    :cond_1
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 109
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageVer()Ljava/lang/String;

    move-result-object v3

    const-string v4, "fail"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v8

    sget-object v9, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v2 .. v9}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_1

    .line 74
    .end local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v16    # "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    :cond_2
    :try_start_5
    sget-object v2, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;->HTTPS:Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->getELMServerAddr(Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;)Ljava/lang/String;

    move-result-object v10

    .line 75
    .local v10, "GSLBServerAddr":Ljava/lang/String;
    if-eqz v10, :cond_3

    .line 76
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RegisterRequestModule.processRegisterRequest(). ELM HTTPS Server Addr : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 77
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HTTPS://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/elm/license/register"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/esdk/elm/type/States;->REGISTER_REQUEST:Lcom/sec/esdk/elm/type/States;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-static {v2, v0, v1, v3}, Lcom/sec/esdk/elm/networkmanager/NetworkManager;->callRestAPI(Ljava/lang/String;Lorg/json/JSONObject;Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/type/States;)V

    move-object v11, v12

    .line 112
    .end local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    goto/16 :goto_1

    .line 81
    .end local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RegisterRequestModule.processRegisterRequest(). Exception. ELM HTTPS Server Addr :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 82
    new-instance v14, Landroid/app/enterprise/license/Error;

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerGeneralNetworkException:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerGeneralNetworkException:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v14, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 84
    .end local v7    # "mError":Landroid/app/enterprise/license/Error;
    .restart local v14    # "mError":Landroid/app/enterprise/license/Error;
    :try_start_6
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 88
    .end local v10    # "GSLBServerAddr":Ljava/lang/String;
    .end local v14    # "mError":Landroid/app/enterprise/license/Error;
    .end local v16    # "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    .restart local v7    # "mError":Landroid/app/enterprise/license/Error;
    :cond_4
    :try_start_7
    const-string v2, "RegisterRequestModule.processRegisterRequest(). Exception. DataManager.compriseRegisterRequestData return Null."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 89
    new-instance v14, Landroid/app/enterprise/license/Error;

    sget-object v2, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-direct {v14, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_5
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3

    .line 90
    .end local v7    # "mError":Landroid/app/enterprise/license/Error;
    .restart local v14    # "mError":Landroid/app/enterprise/license/Error;
    :try_start_8
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    .line 102
    .end local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .end local v14    # "mError":Landroid/app/enterprise/license/Error;
    .restart local v7    # "mError":Landroid/app/enterprise/license/Error;
    .restart local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    :catch_2
    move-exception v13

    goto/16 :goto_2

    .end local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    :catch_3
    move-exception v13

    move-object v11, v12

    .end local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    goto/16 :goto_2

    .line 93
    :catch_4
    move-exception v13

    goto/16 :goto_0

    .end local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    :catch_5
    move-exception v13

    move-object v11, v12

    .end local v12    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    .restart local v11    # "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    goto/16 :goto_0
.end method
