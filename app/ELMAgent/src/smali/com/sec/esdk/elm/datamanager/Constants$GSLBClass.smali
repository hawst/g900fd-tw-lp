.class public final enum Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/datamanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "GSLBClass"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

.field public static final enum GSLB_CLASS_POLICY:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

.field public static final enum GSLB_CLASS_REDIRECTION:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 88
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    const-string v1, "GSLB_CLASS_REDIRECTION"

    invoke-direct {v0, v1, v2}, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_REDIRECTION:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    const-string v1, "GSLB_CLASS_POLICY"

    invoke-direct {v0, v1, v3}, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_POLICY:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    .line 87
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_REDIRECTION:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_POLICY:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 87
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    return-object v0
.end method
