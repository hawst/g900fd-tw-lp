.class public Lcom/sec/esdk/elm/state/module/ModuleBase;
.super Ljava/lang/Object;
.source "ModuleBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/esdk/elm/state/module/ModuleBase$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    return-void
.end method

.method public static declared-synchronized ModifySetAlarm()I
    .locals 16

    .prologue
    .line 248
    const-class v13, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v13

    :try_start_0
    const-string v12, "ModuleBase.ModifySetAlarm()"

    invoke-static {v12}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 249
    const/4 v7, 0x0

    .line 250
    .local v7, "result":I
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getInstance()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :try_start_1
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v0

    .line 253
    .local v0, "MDMInfoArray":[Landroid/app/enterprise/license/LicenseInfo;
    if-eqz v0, :cond_2

    .line 254
    array-length v7, v0

    .line 255
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_2

    .line 256
    aget-object v12, v0, v2

    invoke-virtual {v12}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    .line 257
    .local v3, "instanceID":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v6

    .line 259
    .local v6, "mRO":Landroid/app/enterprise/license/RightsObject;
    if-eqz v6, :cond_0

    .line 260
    invoke-virtual {v6}, Landroid/app/enterprise/license/RightsObject;->getUploadTime()J

    move-result-wide v10

    .line 262
    .local v10, "upload_time":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    cmp-long v12, v14, v10

    if-gez v12, :cond_0

    .line 263
    invoke-virtual {v6}, Landroid/app/enterprise/license/RightsObject;->getUploadFrequencyTime()J

    move-result-wide v4

    .line 264
    .local v4, "intervalMilis":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    cmp-long v12, v14, v10

    if-lez v12, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 265
    .local v8, "startMilis":J
    :goto_1
    invoke-static {v3, v8, v9, v4, v5}, Lcom/sec/esdk/elm/state/module/ModuleBase;->setRepeatAlarm(Ljava/lang/String;JJ)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 255
    .end local v4    # "intervalMilis":J
    .end local v8    # "startMilis":J
    .end local v10    # "upload_time":J
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 264
    .restart local v4    # "intervalMilis":J
    .restart local v10    # "upload_time":J
    :cond_1
    sub-long v8, v10, v4

    goto :goto_1

    .line 270
    .end local v0    # "MDMInfoArray":[Landroid/app/enterprise/license/LicenseInfo;
    .end local v2    # "i":I
    .end local v3    # "instanceID":Ljava/lang/String;
    .end local v4    # "intervalMilis":J
    .end local v6    # "mRO":Landroid/app/enterprise/license/RightsObject;
    .end local v10    # "upload_time":J
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 273
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    monitor-exit v13

    return v7

    .line 248
    :catchall_0
    move-exception v12

    monitor-exit v13

    throw v12
.end method

.method protected static declared-synchronized SendMessageHandler(Lcom/sec/esdk/elm/type/ELMContainer;I)V
    .locals 4
    .param p0, "container"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p1, "what"    # I

    .prologue
    .line 240
    const-class v2, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v2

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ModuleBase.SendMessageHandler( container:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", what:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " )"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 241
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 242
    .local v0, "msg":Landroid/os/Message;
    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 243
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    monitor-exit v2

    return-void

    .line 240
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method protected static declared-synchronized SendRetryIntent(Lcom/sec/esdk/elm/type/ELMContainer;IJ)V
    .locals 8
    .param p0, "container"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p1, "what"    # I
    .param p2, "delayedTime"    # J

    .prologue
    .line 218
    const-class v5, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v5

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "ModuleBase.SendRetryIntent( container:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", what:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", delayedTime:"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, " )"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 219
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v4

    const-string v6, "alarm"

    invoke-virtual {v4, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 222
    .local v0, "am":Landroid/app/AlarmManager;
    const-string v2, "com.sec.esdk.elm.alarm.RetryIntent"

    .line 223
    .local v2, "intentAction":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 224
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "HANDLE_TYPE"

    invoke-virtual {v1, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 225
    const-string v4, "CONTAINER_OBJECT"

    invoke-virtual {v1, v4, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 228
    const-string v4, "com.sec.esdk.elm"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v4

    const/4 v6, 0x0

    const/high16 v7, 0x8000000

    invoke-static {v4, v6, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 230
    .local v3, "sender":Landroid/app/PendingIntent;
    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    add-long/2addr v6, p2

    invoke-virtual {v0, v4, v6, v7, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    monitor-exit v5

    return-void

    .line 218
    .end local v0    # "am":Landroid/app/AlarmManager;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "intentAction":Ljava/lang/String;
    .end local v3    # "sender":Landroid/app/PendingIntent;
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method private static declared-synchronized getIntegerFromString(Ljava/lang/String;)I
    .locals 5
    .param p0, "IntegerableString"    # Ljava/lang/String;

    .prologue
    .line 129
    const-class v3, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v3

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ModuleBase.getIntegerFromString( IntegerableString:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " )"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    const/4 v1, 0x0

    .line 132
    .local v1, "result":I
    :try_start_1
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 138
    :goto_0
    monitor-exit v3

    return v1

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ModuleBase.getIntegerFromString() has Exception."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 135
    const/4 v1, 0x0

    goto :goto_0

    .line 129
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v1    # "result":I
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method private static declared-synchronized getIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p0, "instanceID"    # Ljava/lang/String;

    .prologue
    .line 159
    const-class v4, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v4

    const/4 v2, 0x0

    .line 160
    .local v2, "sIntentName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 162
    .local v0, "intent":Landroid/content/Intent;
    :try_start_0
    const-string v2, "com.sec.esdk.elm.alarm.AlarmIntent"

    .line 163
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    .end local v0    # "intent":Landroid/content/Intent;
    .local v1, "intent":Landroid/content/Intent;
    :try_start_1
    const-string v3, "com.sec.esdk.elm"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    const-string v3, "INSTANCE_ID"

    invoke-virtual {v1, v3, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 167
    monitor-exit v4

    return-object v1

    .line 159
    .end local v1    # "intent":Landroid/content/Intent;
    .restart local v0    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v3

    :goto_0
    monitor-exit v4

    throw v3

    .end local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "intent":Landroid/content/Intent;
    :catchall_1
    move-exception v3

    move-object v0, v1

    .end local v1    # "intent":Landroid/content/Intent;
    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method

.method private static declared-synchronized getPendingIntent(Landroid/content/Intent;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 5
    .param p0, "intent"    # Landroid/content/Intent;
    .param p1, "instanceID"    # Ljava/lang/String;

    .prologue
    .line 143
    const-class v3, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v3

    const/4 v1, 0x0

    .line 144
    .local v1, "pIntent":Landroid/app/PendingIntent;
    :try_start_0
    invoke-static {p1}, Lcom/sec/esdk/elm/state/module/ModuleBase;->getIntegerFromString(Ljava/lang/String;)I

    move-result v0

    .line 145
    .local v0, "iInstanceID":I
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v4, 0x8000000

    invoke-static {v2, v0, p0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 146
    monitor-exit v3

    return-object v1

    .line 143
    .end local v0    # "iInstanceID":I
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method protected static declared-synchronized getResponseStatusVaule(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    .locals 4
    .param p0, "status"    # Ljava/lang/String;

    .prologue
    .line 204
    const-class v2, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v2

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ModuleBase.getResponseStatusVaule( status:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " )"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 205
    invoke-static {p0}, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->getStatus(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    move-result-object v0

    .line 206
    .local v0, "statusValue":Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    if-nez v0, :cond_0

    .line 207
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->FAIL:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    :cond_0
    monitor-exit v2

    return-object v0

    .line 204
    .end local v0    # "statusValue":Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized getTime(Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)J
    .locals 6
    .param p0, "in_freq"    # Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    .prologue
    .line 177
    const-class v2, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v2

    const v0, 0x36ee80

    .line 179
    .local v0, "days":I
    :try_start_0
    sget-object v1, Lcom/sec/esdk/elm/state/module/ModuleBase$1;->$SwitchMap$com$sec$esdk$elm$datamanager$Constants$FailFrequency:[I

    invoke-virtual {p0}, Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 190
    const v0, 0x36ee80

    .line 194
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ModuleBase.getTime(). Get Frequency Time.\t VALUE : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "sec"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    int-to-long v4, v0

    monitor-exit v2

    return-wide v4

    .line 181
    :pswitch_0
    const v0, 0x927c0

    .line 182
    goto :goto_0

    .line 184
    :pswitch_1
    const v0, 0x1b7740

    .line 185
    goto :goto_0

    .line 187
    :pswitch_2
    const v0, 0x36ee80

    .line 188
    goto :goto_0

    .line 177
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1

    .line 179
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected static declared-synchronized isONSELMActivated()Z
    .locals 5

    .prologue
    .line 100
    const-class v3, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v3

    const/4 v1, 0x0

    .line 102
    .local v1, "result":Z
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 103
    .local v0, "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSELMActivated()Z

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getELMValidateOption()Z

    move-result v2

    if-nez v2, :cond_0

    .line 104
    const-string v2, "ModuleBase.isONSELMActivated() : S is set"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    const/4 v1, 0x1

    .line 108
    :cond_0
    monitor-exit v3

    return v1

    .line 100
    .end local v0    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static declared-synchronized removeAlarm(Ljava/lang/String;)V
    .locals 6
    .param p0, "instnaceID"    # Ljava/lang/String;

    .prologue
    .line 57
    const-class v4, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v4

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ModuleBase.removeAlarm( instnaceID:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " )"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 58
    const/4 v1, 0x0

    .line 59
    .local v1, "intent":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 60
    .local v0, "am":Landroid/app/AlarmManager;
    const/4 v2, 0x0

    .line 62
    .local v2, "pIntent":Landroid/app/PendingIntent;
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "alarm"

    invoke-virtual {v3, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "am":Landroid/app/AlarmManager;
    check-cast v0, Landroid/app/AlarmManager;

    .line 63
    .restart local v0    # "am":Landroid/app/AlarmManager;
    invoke-static {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 64
    invoke-static {v1, p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;->getPendingIntent(Landroid/content/Intent;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 65
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    monitor-exit v4

    return-void

    .line 57
    .end local v0    # "am":Landroid/app/AlarmManager;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "pIntent":Landroid/app/PendingIntent;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method private static declared-synchronized setAlarm(JJLandroid/content/Intent;Ljava/lang/String;)V
    .locals 8
    .param p0, "startTime"    # J
    .param p2, "intervalMilis"    # J
    .param p4, "intent"    # Landroid/content/Intent;
    .param p5, "InstanceID"    # Ljava/lang/String;

    .prologue
    .line 119
    const-class v7, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v7

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ModuleBase.setAlarm( startTime: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", intervalMilis:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Intent:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 122
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "alarm"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 123
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {p4, p5}, Lcom/sec/esdk/elm/state/module/ModuleBase;->getPendingIntent(Landroid/content/Intent;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v6

    .line 124
    .local v6, "pIntent":Landroid/app/PendingIntent;
    const/4 v1, 0x0

    add-long v2, p0, p2

    move-wide v4, p2

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    monitor-exit v7

    return-void

    .line 119
    .end local v0    # "am":Landroid/app/AlarmManager;
    .end local v6    # "pIntent":Landroid/app/PendingIntent;
    :catchall_0
    move-exception v1

    monitor-exit v7

    throw v1
.end method

.method protected static declared-synchronized setRepeatAlarm(Ljava/lang/String;J)V
    .locals 5
    .param p0, "instanceID"    # Ljava/lang/String;
    .param p1, "intervalMilis"    # J

    .prologue
    .line 77
    const-class v1, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {p0, v2, v3, p1, p2}, Lcom/sec/esdk/elm/state/module/ModuleBase;->setRepeatAlarm(Ljava/lang/String;JJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit v1

    return-void

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized setRepeatAlarm(Ljava/lang/String;JJ)V
    .locals 7
    .param p0, "instanceID"    # Ljava/lang/String;
    .param p1, "startTime"    # J
    .param p3, "intervalMilis"    # J

    .prologue
    .line 81
    const-class v6, Lcom/sec/esdk/elm/state/module/ModuleBase;

    monitor-enter v6

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ModuleBase.setRepeatAlarm( instanceID:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", intervalMilis:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " )"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 83
    const/4 v4, 0x0

    .line 85
    .local v4, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/esdk/elm/state/module/ModuleBase;->isONSELMActivated()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    :goto_0
    monitor-exit v6

    return-void

    .line 93
    :cond_0
    :try_start_1
    invoke-static {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;->getIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    move-wide v0, p1

    move-wide v2, p3

    move-object v5, p0

    .line 96
    invoke-static/range {v0 .. v5}, Lcom/sec/esdk/elm/state/module/ModuleBase;->setAlarm(JJLandroid/content/Intent;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 81
    .end local v4    # "intent":Landroid/content/Intent;
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method


# virtual methods
.method protected StartToGSLB()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 32
    new-instance v0, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;-><init>(Landroid/content/Context;)V

    .line 34
    .local v0, "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->setELMServerAddr()Z

    move-result v2

    if-nez v2, :cond_0

    .line 42
    :goto_0
    return v1

    .line 37
    :cond_0
    invoke-virtual {v0}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->isELMServerAddr()Z

    move-result v2

    if-nez v2, :cond_1

    .line 38
    const-string v2, "ModuleBase.StartToGSLB(). isELMServerAddr() is false. return : false"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 42
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method
