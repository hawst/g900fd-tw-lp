.class public final enum Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/datamanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ResponseStatusVaule"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

.field public static final enum FAIL:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

.field public static final enum SUCCESS:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->SUCCESS:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    const-string v1, "FAIL"

    invoke-direct {v0, v1, v3}, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->FAIL:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    .line 42
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->SUCCESS:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->FAIL:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 42
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    return-object v0
.end method
