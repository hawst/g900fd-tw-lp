.class public Lcom/sec/esdk/elm/service/ElmAgentService;
.super Landroid/app/IntentService;
.source "ElmAgentService.java"


# static fields
.field private static mElmAgentService:Lcom/sec/esdk/elm/service/ElmAgentService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/esdk/elm/service/ElmAgentService;->mElmAgentService:Lcom/sec/esdk/elm/service/ElmAgentService;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "ELMAGENT"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 51
    sput-object p0, Lcom/sec/esdk/elm/service/ElmAgentService;->mElmAgentService:Lcom/sec/esdk/elm/service/ElmAgentService;

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 46
    sput-object p0, Lcom/sec/esdk/elm/service/ElmAgentService;->mElmAgentService:Lcom/sec/esdk/elm/service/ElmAgentService;

    .line 47
    return-void
.end method

.method public static getContext()Landroid/content/Context;
    .locals 2

    .prologue
    .line 55
    sget-object v0, Lcom/sec/esdk/elm/service/ElmAgentService;->mElmAgentService:Lcom/sec/esdk/elm/service/ElmAgentService;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/sec/esdk/elm/service/ElmAgentService;

    const-string v1, "ELMAGENT"

    invoke-direct {v0, v1}, Lcom/sec/esdk/elm/service/ElmAgentService;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/service/ElmAgentService;->mElmAgentService:Lcom/sec/esdk/elm/service/ElmAgentService;

    .line 58
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/service/ElmAgentService;->mElmAgentService:Lcom/sec/esdk/elm/service/ElmAgentService;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/service/ElmAgentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method private listeningToAlarm(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 288
    const-string v3, "InnerIntentReceiver.listeningToAlarm( context, intent )."

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 289
    if-eqz p2, :cond_0

    .line 291
    const-string v3, "INSTANCE_ID"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 292
    .local v2, "sInstanceId":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 303
    .end local v2    # "sInstanceId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 295
    .restart local v2    # "sInstanceId":Ljava/lang/String;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "InnerIntentReceiver.listeningToAlarm(). Validation InstanceId:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 296
    invoke-static {v2}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v1

    .line 297
    .local v1, "rightObject":Landroid/app/enterprise/license/RightsObject;
    if-eqz v1, :cond_0

    .line 300
    invoke-virtual {v1}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v0

    .line 301
    .local v0, "licenseType":Ljava/lang/String;
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->INNER_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    invoke-virtual {v3, v2, v0, v4}, Lcom/sec/esdk/elm/service/ELMEngine;->startValidation(Ljava/lang/String;Ljava/lang/String;Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;)V

    goto :goto_0
.end method

.method private listeningToKLMSIntent(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 150
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 151
    .local v2, "msg":Landroid/os/Message;
    const-string v3, "HANDLE_TYPE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 154
    .local v1, "mExtra":Landroid/os/Bundle;
    sget-object v3, Lcom/sec/esdk/elm/type/States;->KLMS_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v3

    iput v3, v2, Landroid/os/Message;->what:I

    .line 155
    iput-object v1, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 156
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    .end local v1    # "mExtra":Landroid/os/Bundle;
    .end local v2    # "msg":Landroid/os/Message;
    :goto_0
    return-void

    .line 157
    :catch_0
    move-exception v0

    .line 158
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private listeningToLicenseRegisterRequest(Landroid/content/Context;Landroid/content/Intent;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "mRequestPushPoint"    # Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .prologue
    .line 177
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MDMIntentReceiver.listeningToLicenseRegisterRequest( "

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v8, " )."

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 179
    const/16 v17, 0x0

    .line 180
    .local v17, "licenseKey":Ljava/lang/String;
    const/4 v6, 0x0

    .line 181
    .local v6, "packageName":Ljava/lang/String;
    const/4 v7, 0x0

    .line 182
    .local v7, "packageVer":Ljava/lang/String;
    const/4 v11, 0x0

    .line 183
    .local v11, "error":Landroid/app/enterprise/license/Error;
    const/16 v18, 0x0

    .line 186
    .local v18, "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v8, "edm.intent.extra.license.data.pkgname"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v20

    .line 187
    .end local v6    # "packageName":Ljava/lang/String;
    .local v20, "packageName":Ljava/lang/String;
    :try_start_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v8, "edm.intent.extra.license.data.pkgversion"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4

    move-result-object v21

    .line 188
    .end local v7    # "packageVer":Ljava/lang/String;
    .local v21, "packageVer":Ljava/lang/String;
    :try_start_2
    invoke-static/range {v20 .. v20}, Lcom/sec/esdk/elm/service/MDMBridge;->getELMLicenseKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 189
    new-instance v19, Lcom/sec/esdk/elm/utils/LicenseKeyUtil;

    invoke-direct/range {v19 .. v19}, Lcom/sec/esdk/elm/utils/LicenseKeyUtil;-><init>()V
    :try_end_2
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_5

    .line 191
    .end local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .local v19, "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    if-nez v17, :cond_0

    .line 192
    :try_start_3
    const-string v3, "MDMIntentReceiver.listeningToLicenseRegisterRequest : license is null"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 193
    new-instance v16, Landroid/app/enterprise/license/Error;

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v3

    sget-object v8, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v8}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v8

    const/4 v9, 0x0

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v8, v9}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V
    :try_end_3
    .catch Ljava/security/InvalidKeyException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6

    .line 194
    .end local v11    # "error":Landroid/app/enterprise/license/Error;
    .local v16, "error":Landroid/app/enterprise/license/Error;
    :try_start_4
    new-instance v3, Ljava/security/InvalidKeyException;

    invoke-direct {v3}, Ljava/security/InvalidKeyException;-><init>()V

    throw v3
    :try_end_4
    .catch Ljava/security/InvalidKeyException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 217
    :catch_0
    move-exception v15

    move-object/from16 v18, v19

    .end local v19    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    move-object/from16 v11, v16

    .end local v16    # "error":Landroid/app/enterprise/license/Error;
    .restart local v11    # "error":Landroid/app/enterprise/license/Error;
    move-object/from16 v7, v21

    .end local v21    # "packageVer":Ljava/lang/String;
    .restart local v7    # "packageVer":Ljava/lang/String;
    move-object/from16 v6, v20

    .line 218
    .end local v20    # "packageName":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    .local v15, "e":Ljava/security/InvalidKeyException;
    :goto_0
    const-string v3, "MDMIntentReceiver.listeningToLicenseRegisterRequest : InvalidKeyException is occurred"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 219
    const-string v8, "fail"

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v12, p3

    invoke-static/range {v6 .. v13}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    .line 221
    invoke-virtual {v15}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 232
    .end local v15    # "e":Ljava/security/InvalidKeyException;
    :goto_1
    return-void

    .line 198
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "packageVer":Ljava/lang/String;
    .end local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v19    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v20    # "packageName":Ljava/lang/String;
    .restart local v21    # "packageVer":Ljava/lang/String;
    :cond_0
    :try_start_5
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/utils/LicenseKeyUtil;->isSKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 199
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/utils/LicenseKeyUtil;->getURLFromPackedKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 200
    .local v5, "SURL":Ljava/lang/String;
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/utils/LicenseKeyUtil;->getLicenseKeyFromPackedKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 202
    .local v14, "DecodedLicenseKey":Ljava/lang/String;
    if-eqz v5, :cond_1

    if-nez v14, :cond_3

    .line 203
    :cond_1
    const-string v3, "MDMIntentReceiver.listeningToLicenseRegisterRequest : Decoded URL/Key is null"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 204
    new-instance v16, Landroid/app/enterprise/license/Error;

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v3

    sget-object v8, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v8}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v8

    const/4 v9, 0x0

    move-object/from16 v0, v16

    invoke-direct {v0, v3, v8, v9}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V
    :try_end_5
    .catch Ljava/security/InvalidKeyException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    .line 206
    .end local v11    # "error":Landroid/app/enterprise/license/Error;
    .restart local v16    # "error":Landroid/app/enterprise/license/Error;
    :try_start_6
    new-instance v3, Ljava/security/InvalidKeyException;

    invoke-direct {v3}, Ljava/security/InvalidKeyException;-><init>()V

    throw v3
    :try_end_6
    .catch Ljava/security/InvalidKeyException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    .line 222
    .end local v5    # "SURL":Ljava/lang/String;
    .end local v14    # "DecodedLicenseKey":Ljava/lang/String;
    :catch_1
    move-exception v15

    move-object/from16 v18, v19

    .end local v19    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    move-object/from16 v11, v16

    .end local v16    # "error":Landroid/app/enterprise/license/Error;
    .restart local v11    # "error":Landroid/app/enterprise/license/Error;
    move-object/from16 v7, v21

    .end local v21    # "packageVer":Ljava/lang/String;
    .restart local v7    # "packageVer":Ljava/lang/String;
    move-object/from16 v6, v20

    .line 223
    .end local v20    # "packageName":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    .local v15, "e":Ljava/lang/Exception;
    :goto_2
    const-string v3, "MDMIntentReceiver.listeningToLicenseRegisterRequest : exception is occurred"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 224
    if-nez v11, :cond_2

    .line 225
    new-instance v11, Landroid/app/enterprise/license/Error;

    .end local v11    # "error":Landroid/app/enterprise/license/Error;
    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v3

    sget-object v8, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v8}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v8

    const/4 v9, 0x0

    invoke-direct {v11, v3, v8, v9}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 227
    .restart local v11    # "error":Landroid/app/enterprise/license/Error;
    :cond_2
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 228
    const-string v8, "fail"

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v12, p3

    invoke-static/range {v6 .. v13}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    .line 230
    invoke-virtual {v15}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 209
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "packageVer":Ljava/lang/String;
    .end local v15    # "e":Ljava/lang/Exception;
    .end local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v5    # "SURL":Ljava/lang/String;
    .restart local v14    # "DecodedLicenseKey":Ljava/lang/String;
    .restart local v19    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v20    # "packageName":Ljava/lang/String;
    .restart local v21    # "packageVer":Ljava/lang/String;
    :cond_3
    :try_start_7
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v14, v0}, Lcom/sec/esdk/elm/service/ELMEngine;->setLicenseMap(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    invoke-static/range {p1 .. p1}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v4

    .local v4, "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    move-object/from16 v3, p0

    move-object/from16 v6, p2

    move-object/from16 v7, p1

    move-object/from16 v8, p3

    .line 211
    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/service/ElmAgentService;->processSKey(Lcom/sec/esdk/elm/utils/Preferences;Ljava/lang/String;Landroid/content/Intent;Landroid/content/Context;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V

    .end local v4    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    .end local v5    # "SURL":Ljava/lang/String;
    .end local v14    # "DecodedLicenseKey":Ljava/lang/String;
    :goto_3
    move-object/from16 v18, v19

    .end local v19    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    move-object/from16 v7, v21

    .end local v21    # "packageVer":Ljava/lang/String;
    .restart local v7    # "packageVer":Ljava/lang/String;
    move-object/from16 v6, v20

    .line 231
    .end local v20    # "packageName":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    goto/16 :goto_1

    .line 213
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "packageVer":Ljava/lang/String;
    .end local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v19    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v20    # "packageName":Ljava/lang/String;
    .restart local v21    # "packageVer":Ljava/lang/String;
    :cond_4
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v3, v0, v1}, Lcom/sec/esdk/elm/service/ELMEngine;->setLicenseMap(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/esdk/elm/service/ElmAgentService;->processKNOXKey(Landroid/content/Intent;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V
    :try_end_7
    .catch Ljava/security/InvalidKeyException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_6

    goto :goto_3

    .line 217
    :catch_2
    move-exception v15

    move-object/from16 v18, v19

    .end local v19    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    move-object/from16 v7, v21

    .end local v21    # "packageVer":Ljava/lang/String;
    .restart local v7    # "packageVer":Ljava/lang/String;
    move-object/from16 v6, v20

    .end local v20    # "packageName":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    goto/16 :goto_0

    .line 222
    :catch_3
    move-exception v15

    goto :goto_2

    .end local v6    # "packageName":Ljava/lang/String;
    .restart local v20    # "packageName":Ljava/lang/String;
    :catch_4
    move-exception v15

    move-object/from16 v6, v20

    .end local v20    # "packageName":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    goto :goto_2

    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "packageVer":Ljava/lang/String;
    .restart local v20    # "packageName":Ljava/lang/String;
    .restart local v21    # "packageVer":Ljava/lang/String;
    :catch_5
    move-exception v15

    move-object/from16 v7, v21

    .end local v21    # "packageVer":Ljava/lang/String;
    .restart local v7    # "packageVer":Ljava/lang/String;
    move-object/from16 v6, v20

    .end local v20    # "packageName":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    goto :goto_2

    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "packageVer":Ljava/lang/String;
    .end local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v19    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v20    # "packageName":Ljava/lang/String;
    .restart local v21    # "packageVer":Ljava/lang/String;
    :catch_6
    move-exception v15

    move-object/from16 v18, v19

    .end local v19    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    .restart local v18    # "licenseKeyGenerator":Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
    move-object/from16 v7, v21

    .end local v21    # "packageVer":Ljava/lang/String;
    .restart local v7    # "packageVer":Ljava/lang/String;
    move-object/from16 v6, v20

    .end local v20    # "packageName":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    goto/16 :goto_2

    .line 217
    :catch_7
    move-exception v15

    goto/16 :goto_0

    .end local v6    # "packageName":Ljava/lang/String;
    .restart local v20    # "packageName":Ljava/lang/String;
    :catch_8
    move-exception v15

    move-object/from16 v6, v20

    .end local v20    # "packageName":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    goto/16 :goto_0

    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "packageVer":Ljava/lang/String;
    .restart local v20    # "packageName":Ljava/lang/String;
    .restart local v21    # "packageVer":Ljava/lang/String;
    :catch_9
    move-exception v15

    move-object/from16 v7, v21

    .end local v21    # "packageVer":Ljava/lang/String;
    .restart local v7    # "packageVer":Ljava/lang/String;
    move-object/from16 v6, v20

    .end local v20    # "packageName":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method private listeningToPackageRemoved(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x1

    .line 319
    const-string v9, "MainReceiver.listeningToPackageRemoved()"

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 321
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_2

    .line 322
    :cond_0
    const-string v9, "MainReceiver.listeningToPackageRemoved() : parameter is null"

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 387
    :cond_1
    :goto_0
    return-void

    .line 326
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v9}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v5

    .line 327
    .local v5, "packageName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 328
    .local v0, "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getInstance()V

    .line 329
    const/4 v7, 0x0

    .line 331
    .local v7, "preONSCount":I
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 333
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v9, v0

    if-ge v2, v9, :cond_6

    .line 334
    aget-object v9, v0, v2

    invoke-virtual {v9}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-ne v9, v11, :cond_3

    .line 335
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MainReceiver.listeningToPackageRemoved(). Registered  getInstanceId:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v0, v2

    invoke-virtual {v10}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", getPackageName:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v0, v2

    invoke-virtual {v10}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", getPackageVersion:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    aget-object v10, v0, v2

    invoke-virtual {v10}, Landroid/app/enterprise/license/LicenseInfo;->getPackageVersion()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 343
    aget-object v9, v0, v2

    invoke-virtual {v9}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 344
    .local v6, "pkgName":Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/esdk/elm/utils/Utility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v9

    if-ne v9, v11, :cond_4

    .line 345
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MainReceiver.listeningToPackageRemoved(). "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " is exist."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 333
    .end local v6    # "pkgName":Ljava/lang/String;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 350
    .restart local v6    # "pkgName":Ljava/lang/String;
    :cond_4
    aget-object v9, v0, v2

    invoke-virtual {v9}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    .line 351
    .local v3, "instanceId":Ljava/lang/String;
    const/4 v8, 0x0

    .line 352
    .local v8, "rightsObject":Landroid/app/enterprise/license/RightsObject;
    invoke-static {v3}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v8

    .line 353
    if-eqz v8, :cond_5

    .line 354
    invoke-virtual {v8}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->LICENSE_TYPE_ONS_STR:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v10}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 355
    add-int/lit8 v7, v7, 0x1

    .line 359
    :cond_5
    invoke-static {v3}, Lcom/sec/esdk/elm/state/module/ModuleBase;->removeAlarm(Ljava/lang/String;)V

    .line 360
    invoke-static {v3}, Lcom/sec/esdk/elm/service/MDMBridge;->resetLicense(Ljava/lang/String;)Z

    .line 361
    const-string v9, "success"

    const/4 v10, 0x0

    invoke-static {v9, v3, v10}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)V

    .line 362
    invoke-static {v3}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteLicense(Ljava/lang/String;)Z

    .line 368
    .end local v2    # "i":I
    .end local v3    # "instanceId":Ljava/lang/String;
    .end local v6    # "pkgName":Ljava/lang/String;
    .end local v8    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_6
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v0

    .line 369
    if-nez v0, :cond_7

    .line 370
    invoke-static {}, Landroid/os/SELinux;->isSELinuxEnforced()Z

    move-result v9

    if-ne v9, v11, :cond_7

    invoke-static {p1}, Lcom/sec/esdk/elm/utils/Utility;->isProduct_Ship(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 371
    const-string v9, "enterprise_policy"

    invoke-static {v9}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v9

    invoke-static {v9}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v4

    .line 373
    .local v4, "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v4, :cond_7

    .line 374
    const/4 v9, 0x1

    invoke-interface {v4, v9}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setB2BMode(Z)I

    .line 379
    .end local v4    # "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :cond_7
    if-lez v7, :cond_1

    .line 380
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->RecoverySKeyStatusProcessing()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 383
    .end local v0    # "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    .end local v5    # "packageName":Ljava/lang/String;
    .end local v7    # "preONSCount":I
    :catch_0
    move-exception v1

    .line 384
    .local v1, "e":Ljava/lang/Exception;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MainReceiver.listeningToPackageRemoved(). Exception. "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 385
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private listeningToTimeDateChanges(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 306
    const-string v1, "ELMAgentService.listeningToTimeDateChanges( context, intent )."

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 307
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    .line 308
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v1

    sget-object v2, Lcom/sec/esdk/elm/type/States;->ALARM_MODIFY:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 309
    .local v0, "msg":Landroid/os/Message;
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z

    .line 310
    return-void
.end method

.method private processKNOXKey(Landroid/content/Intent;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "mRequestPushPoint"    # Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .prologue
    .line 239
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 240
    .local v0, "msg":Landroid/os/Message;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "processKNOXKey():"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 241
    const-string v1, "MDM_PUSH_POINT"

    invoke-virtual {p2}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 242
    sget-object v1, Lcom/sec/esdk/elm/type/States;->KLMS_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 243
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 244
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z

    .line 245
    return-void
.end method

.method private processSKey(Lcom/sec/esdk/elm/utils/Preferences;Ljava/lang/String;Landroid/content/Intent;Landroid/content/Context;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V
    .locals 12
    .param p1, "preferences"    # Lcom/sec/esdk/elm/utils/Preferences;
    .param p2, "targetUrl"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "context"    # Landroid/content/Context;
    .param p5, "mRequestPushPoint"    # Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .prologue
    .line 256
    const-string v3, "MDMIntentReceiver.processSKey : start S process"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 257
    const/4 v1, 0x0

    .line 258
    .local v1, "packageName":Ljava/lang/String;
    const/4 v2, 0x0

    .line 261
    .local v2, "packageVersion":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v9

    .line 263
    .local v9, "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v9, p2}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setSURL(Ljava/lang/String;)V

    .line 264
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v11

    .line 265
    .local v11, "extras":Landroid/os/Bundle;
    const-string v3, "edm.intent.extra.license.data.pkgname"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 266
    const-string v3, "edm.intent.extra.license.data.pkgversion"

    invoke-virtual {v11, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 267
    const-string v3, "LICENSE_TYPE"

    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_ONS1:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 268
    move-object/from16 v0, p5

    invoke-direct {p0, p3, v0}, Lcom/sec/esdk/elm/service/ElmAgentService;->processKNOXKey(Landroid/content/Intent;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    .end local v9    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .end local v11    # "extras":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 270
    :catch_0
    move-exception v10

    .line 271
    .local v10, "e":Ljava/lang/Exception;
    const-string v3, "MDMIntentReceiver.processSKey : Exception."

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 272
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    .line 273
    new-instance v6, Landroid/app/enterprise/license/Error;

    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v3

    sget-object v4, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v4

    const/4 v5, 0x0

    invoke-direct {v6, v3, v4, v5}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 275
    .local v6, "mError":Landroid/app/enterprise/license/Error;
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 276
    const-string v3, "fail"

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v7, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v7}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v7, p5

    invoke-static/range {v1 .. v8}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto :goto_0
.end method


# virtual methods
.method protected ReceiveRetryIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 136
    :try_start_0
    const-string v4, "HANDLE_TYPE"

    sget-object v5, Lcom/sec/esdk/elm/type/States;->UNKNOWN_STATE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v5

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 137
    .local v3, "what":I
    const-string v4, "CONTAINER_OBJECT"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/type/ELMContainer;

    .line 139
    .local v0, "container":Lcom/sec/esdk/elm/type/ELMContainer;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Service.ReceiveRetryIntent( container:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", what:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 140
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 141
    .local v2, "msg":Landroid/os/Message;
    iput-object v0, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 142
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    .end local v0    # "container":Lcom/sec/esdk/elm/type/ELMContainer;
    .end local v2    # "msg":Landroid/os/Message;
    .end local v3    # "what":I
    :goto_0
    return-void

    .line 143
    :catch_0
    move-exception v1

    .line 144
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 166
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 65
    const-string v0, "ElmAgentService : onCreate()"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 66
    sput-object p0, Lcom/sec/esdk/elm/service/ElmAgentService;->mElmAgentService:Lcom/sec/esdk/elm/service/ElmAgentService;

    .line 67
    invoke-virtual {p0}, Lcom/sec/esdk/elm/service/ElmAgentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->setELMContext(Landroid/content/Context;)V

    .line 68
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 171
    const-string v0, "ElmAgentService : onDestroy()."

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 172
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/esdk/elm/service/ElmAgentService;->mElmAgentService:Lcom/sec/esdk/elm/service/ElmAgentService;

    .line 173
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 174
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    .line 72
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ElmAgentService : onHandleIntent() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p0}, Lcom/sec/esdk/elm/service/ElmAgentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->setELMContext(Landroid/content/Context;)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/esdk/elm/service/ElmAgentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 75
    .local v1, "context":Landroid/content/Context;
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 81
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    sget-object v4, Lcom/sec/esdk/elm/type/States;->BOOT_COMPLETED:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 82
    .local v2, "msg":Landroid/os/Message;
    const/4 v3, 0x2

    iput v3, v2, Landroid/os/Message;->arg1:I

    .line 83
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z

    goto :goto_0

    .line 86
    .end local v2    # "msg":Landroid/os/Message;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 87
    const-string v3, "android.intent.extra.REPLACING"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_3

    .line 88
    invoke-direct {p0, v1, p1}, Lcom/sec/esdk/elm/service/ElmAgentService;->listeningToPackageRemoved(Landroid/content/Context;Landroid/content/Intent;)V

    .line 107
    :cond_3
    :goto_1
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 108
    .local v0, "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getELMValidateOption()Z

    move-result v3

    if-nez v3, :cond_8

    .line 109
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ELMAgentService.onStartCommand() : ValidateOption is false :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 90
    .end local v0    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "edm.intent.action.license.registration.internal"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v5, :cond_5

    .line 93
    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    invoke-direct {p0, v1, p1, v3}, Lcom/sec/esdk/elm/service/ElmAgentService;->listeningToLicenseRegisterRequest(Landroid/content/Context;Landroid/content/Intent;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V

    goto :goto_1

    .line 95
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "edm.intent.action.license.registration.internal_umc"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v5, :cond_6

    .line 96
    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_UMC:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    invoke-direct {p0, v1, p1, v3}, Lcom/sec/esdk/elm/service/ElmAgentService;->listeningToLicenseRegisterRequest(Landroid/content/Context;Landroid/content/Intent;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V

    goto :goto_1

    .line 98
    :cond_6
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.samsung.klmsagent.action.KLMS_ACTIVE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eq v3, v5, :cond_7

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.samsung.klmsagent.action.KLMS_STATUS_CHANGED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eq v3, v5, :cond_7

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.samsung.klmsagent.action.KLMS_RESPONSE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v5, :cond_3

    .line 104
    :cond_7
    invoke-direct {p0, p1}, Lcom/sec/esdk/elm/service/ElmAgentService;->listeningToKLMSIntent(Landroid/content/Intent;)V

    goto :goto_1

    .line 113
    .restart local v0    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.esdk.elm.alarm.RetryIntent"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 114
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/service/ElmAgentService;->ReceiveRetryIntent(Landroid/content/Intent;)V

    .line 124
    :cond_9
    :goto_2
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSELMActivated()Z

    move-result v3

    if-ne v3, v5, :cond_0

    .line 125
    const-string v3, "ELMAgentService.onStartCommand() : S is set"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 116
    :cond_a
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.sec.esdk.elm.alarm.AlarmIntent"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 117
    invoke-direct {p0, v1, p1}, Lcom/sec/esdk/elm/service/ElmAgentService;->listeningToAlarm(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_2

    .line 119
    :cond_b
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 121
    invoke-direct {p0, v1, p1}, Lcom/sec/esdk/elm/service/ElmAgentService;->listeningToTimeDateChanges(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_2
.end method
