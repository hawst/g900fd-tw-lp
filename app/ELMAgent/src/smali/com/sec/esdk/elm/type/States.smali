.class public final enum Lcom/sec/esdk/elm/type/States;
.super Ljava/lang/Enum;
.source "States.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/type/States;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/type/States;

.field public static final enum ALARM_MODIFY:Lcom/sec/esdk/elm/type/States;

.field public static final enum BOOT_COMPLETED:Lcom/sec/esdk/elm/type/States;

.field public static final enum KLMS_REQUEST:Lcom/sec/esdk/elm/type/States;

.field public static final enum KLMS_RESPONSE:Lcom/sec/esdk/elm/type/States;

.field public static final enum REGISTER_REQUEST:Lcom/sec/esdk/elm/type/States;

.field public static final enum REGISTER_RESPONSE:Lcom/sec/esdk/elm/type/States;

.field public static final enum UNKNOWN_STATE:Lcom/sec/esdk/elm/type/States;

.field public static final enum UPLOAD_REQUEST:Lcom/sec/esdk/elm/type/States;

.field public static final enum UPLOAD_RESPONSE:Lcom/sec/esdk/elm/type/States;

.field public static final enum VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

.field public static final enum VALIDATE_RESPONSE:Lcom/sec/esdk/elm/type/States;


# instance fields
.field private mStates:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 10
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "BOOT_COMPLETED"

    const-string v2, "BOOT_COMPLETED"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->BOOT_COMPLETED:Lcom/sec/esdk/elm/type/States;

    .line 11
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "ALARM_MODIFY"

    const-string v2, "ALARM_MODIFY"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->ALARM_MODIFY:Lcom/sec/esdk/elm/type/States;

    .line 12
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "KLMS_REQUEST"

    const-string v2, "KLMS_REQUEST"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->KLMS_REQUEST:Lcom/sec/esdk/elm/type/States;

    .line 13
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "KLMS_RESPONSE"

    const-string v2, "KLMS_RESPONSE"

    invoke-direct {v0, v1, v7, v2}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->KLMS_RESPONSE:Lcom/sec/esdk/elm/type/States;

    .line 14
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "REGISTER_REQUEST"

    const-string v2, "REGISTER_REQUEST"

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->REGISTER_REQUEST:Lcom/sec/esdk/elm/type/States;

    .line 15
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "REGISTER_RESPONSE"

    const/4 v2, 0x5

    const-string v3, "REGISTER_RESPONSE"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->REGISTER_RESPONSE:Lcom/sec/esdk/elm/type/States;

    .line 16
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "VALIDATE_REQUEST"

    const/4 v2, 0x6

    const-string v3, "VALIDATE_REQUEST"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    .line 17
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "VALIDATE_RESPONSE"

    const/4 v2, 0x7

    const-string v3, "VALIDATE_RESPONSE"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->VALIDATE_RESPONSE:Lcom/sec/esdk/elm/type/States;

    .line 18
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "UPLOAD_REQUEST"

    const/16 v2, 0x8

    const-string v3, "UPLOAD_REQUEST"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->UPLOAD_REQUEST:Lcom/sec/esdk/elm/type/States;

    .line 19
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "UPLOAD_RESPONSE"

    const/16 v2, 0x9

    const-string v3, "UPLOAD_RESPONSE"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->UPLOAD_RESPONSE:Lcom/sec/esdk/elm/type/States;

    .line 20
    new-instance v0, Lcom/sec/esdk/elm/type/States;

    const-string v1, "UNKNOWN_STATE"

    const/16 v2, 0xa

    const-string v3, "UNKNOWN_STATE"

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/esdk/elm/type/States;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/States;->UNKNOWN_STATE:Lcom/sec/esdk/elm/type/States;

    .line 9
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/sec/esdk/elm/type/States;

    sget-object v1, Lcom/sec/esdk/elm/type/States;->BOOT_COMPLETED:Lcom/sec/esdk/elm/type/States;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/esdk/elm/type/States;->ALARM_MODIFY:Lcom/sec/esdk/elm/type/States;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/esdk/elm/type/States;->KLMS_REQUEST:Lcom/sec/esdk/elm/type/States;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/esdk/elm/type/States;->KLMS_RESPONSE:Lcom/sec/esdk/elm/type/States;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/esdk/elm/type/States;->REGISTER_REQUEST:Lcom/sec/esdk/elm/type/States;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/esdk/elm/type/States;->REGISTER_RESPONSE:Lcom/sec/esdk/elm/type/States;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/esdk/elm/type/States;->VALIDATE_RESPONSE:Lcom/sec/esdk/elm/type/States;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/esdk/elm/type/States;->UPLOAD_REQUEST:Lcom/sec/esdk/elm/type/States;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/esdk/elm/type/States;->UPLOAD_RESPONSE:Lcom/sec/esdk/elm/type/States;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/esdk/elm/type/States;->UNKNOWN_STATE:Lcom/sec/esdk/elm/type/States;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/esdk/elm/type/States;->$VALUES:[Lcom/sec/esdk/elm/type/States;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "states"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/States;->mStates:Ljava/lang/String;

    .line 25
    iput-object p3, p0, Lcom/sec/esdk/elm/type/States;->mStates:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/type/States;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/sec/esdk/elm/type/States;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/type/States;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/type/States;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/sec/esdk/elm/type/States;->$VALUES:[Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/type/States;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/type/States;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/esdk/elm/type/States;->mStates:Ljava/lang/String;

    return-object v0
.end method
