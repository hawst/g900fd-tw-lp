.class Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;
.super Ljava/lang/Object;
.source "ConfirmDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/useragreement/ConfirmDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;


# direct methods
.method constructor <init>(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 112
    const-string v0, "ConfirmDialog.agree.onClick()"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    const/4 v1, 0x1

    # setter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->IsUserSeleteButton:Z
    invoke-static {v0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$002(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;Z)Z

    .line 114
    const/4 v9, 0x0

    .line 116
    .local v9, "licenseType":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$100(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "edm.intent.extra.license.data.pkgname"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    .line 117
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;
    invoke-static {v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$100(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "edm.intent.extra.license.data.pkgversion"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageVersion:Ljava/lang/String;

    .line 118
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->extras:Landroid/os/Bundle;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$100(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "LICENSE_TYPE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 119
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v2, v2, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/esdk/elm/service/ELMEngine;->getLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->licenseKey:Ljava/lang/String;

    .line 121
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v0, v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->licenseKey:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v0, v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v0, v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageVersion:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 122
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    .line 123
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v1

    sget-object v2, Lcom/sec/esdk/elm/type/States;->REGISTER_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage(I)Landroid/os/Message;

    move-result-object v1

    # setter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->msg:Landroid/os/Message;
    invoke-static {v0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$202(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;Landroid/os/Message;)Landroid/os/Message;

    .line 125
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    new-instance v1, Lcom/sec/esdk/elm/type/RegisterContainer;

    invoke-direct {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;-><init>()V

    # setter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-static {v0, v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$302(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;Lcom/sec/esdk/elm/type/RegisterContainer;)Lcom/sec/esdk/elm/type/RegisterContainer;

    .line 126
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$300(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Lcom/sec/esdk/elm/type/RegisterContainer;

    move-result-object v0

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->setMDMRequestPushPoint(Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V

    .line 127
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$300(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Lcom/sec/esdk/elm/type/RegisterContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v1, v1, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->licenseKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->setLicenseKey(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$300(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Lcom/sec/esdk/elm/type/RegisterContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v1, v1, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->setPakcageName(Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$300(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Lcom/sec/esdk/elm/type/RegisterContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v1, v1, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->setPakcageVer(Ljava/lang/String;)V

    .line 130
    if-eqz v9, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$300(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Lcom/sec/esdk/elm/type/RegisterContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v0

    invoke-virtual {v0, v9}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->setLicenseKeyType(Ljava/lang/String;)V

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->msg:Landroid/os/Message;
    invoke-static {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$200(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->registerContainer:Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-static {v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$300(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Lcom/sec/esdk/elm/type/RegisterContainer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 135
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    # getter for: Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->msg:Landroid/os/Message;
    invoke-static {v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->access$200(Lcom/sec/esdk/elm/useragreement/ConfirmDialog;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    const-string v0, "ConfirmDialog.agree.setOnClickListener().finally."

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->finish()V

    .line 151
    :goto_0
    return-void

    .line 137
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 139
    :catch_0
    move-exception v8

    .line 140
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ConfirmDialog.agree.setOnClickListener().Exception : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 141
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 142
    new-instance v5, Landroid/app/enterprise/license/Error;

    sget-object v0, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v0

    sget-object v1, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v1

    sget-object v2, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorDesc()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 144
    .local v5, "mError":Landroid/app/enterprise/license/Error;
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v1, v1, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 145
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v0, v0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageName:Ljava/lang/String;

    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    iget-object v1, v1, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->packageVersion:Ljava/lang/String;

    const-string v2, "fail"

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v6, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    sget-object v7, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v7}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v0 .. v7}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 148
    const-string v0, "ConfirmDialog.agree.setOnClickListener().finally."

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->finish()V

    goto :goto_0

    .line 148
    .end local v5    # "mError":Landroid/app/enterprise/license/Error;
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    const-string v1, "ConfirmDialog.agree.setOnClickListener().finally."

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/sec/esdk/elm/useragreement/ConfirmDialog$1;->this$0:Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;->finish()V

    throw v0
.end method
