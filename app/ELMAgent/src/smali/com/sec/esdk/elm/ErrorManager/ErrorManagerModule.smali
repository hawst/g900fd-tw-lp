.class public Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;
.super Ljava/lang/Object;
.source "ErrorManagerModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getHttpResCode1000ToModuleError(I)Landroid/app/enterprise/license/Error;
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 86
    .local v0, "mResultError":Landroid/app/enterprise/license/Error;
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerNullInputParam:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    if-ne p1, v1, :cond_1

    .line 88
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->CommonNullParamError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    .line 97
    :cond_0
    :goto_0
    return-object v0

    .line 90
    :cond_1
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerNetworkDisabled:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    if-ne p1, v1, :cond_2

    .line 92
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->NetworkDisconnectedError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_2
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerGeneralNetworkException:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 95
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->NetworkGeneralError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0
.end method

.method private getHttpResCode400ToModuleError(I)Landroid/app/enterprise/license/Error;
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 55
    .local v0, "mResultError":Landroid/app/enterprise/license/Error;
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    if-ne p1, v1, :cond_1

    .line 57
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->LicenseInvalidError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    .line 81
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :cond_1
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseStateError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    if-ne p1, v1, :cond_2

    .line 61
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->LicenseRegisterError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0

    .line 63
    :cond_2
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerTerminated:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    if-ne p1, v1, :cond_3

    .line 65
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->LicenseTerminatedError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0

    .line 67
    :cond_3
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerTimeMismatchError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    if-ne p1, v1, :cond_4

    .line 69
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->ServerTimeMismatchError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0

    .line 71
    :cond_4
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    if-ne p1, v1, :cond_5

    .line 73
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->AgentInternalError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_5
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerSignatureError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 77
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->AgentInternalError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0
.end method

.method private getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;
    .locals 4
    .param p1, "mModuleErrorEnum"    # Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    .prologue
    .line 101
    const/4 v0, 0x0

    .line 102
    .local v0, "mResultError":Landroid/app/enterprise/license/Error;
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "mResultError":Landroid/app/enterprise/license/Error;
    invoke-virtual {p1}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->getModuleCode()I

    move-result v1

    invoke-virtual {p1}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->getModuleErrorCode()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->getModuleErrorDesc()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 103
    .restart local v0    # "mResultError":Landroid/app/enterprise/license/Error;
    return-object v0
.end method


# virtual methods
.method public getError(II)Landroid/app/enterprise/license/Error;
    .locals 3
    .param p1, "httpResponseCode"    # I
    .param p2, "errorCode"    # I

    .prologue
    .line 15
    const/4 v0, 0x0

    .line 16
    .local v0, "mResultError":Landroid/app/enterprise/license/Error;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorManagerModule.getError( httpResponseCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", errorCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 18
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerSuccess:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v1

    if-ne p1, v1, :cond_2

    .line 19
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->Success:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    .line 41
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 44
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->CommonUnknownError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    .line 46
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorManagerModule.getError() return :  getHttpResponseCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/enterprise/license/Error;->getHttpResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " getErrorCode:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/enterprise/license/Error;->getErrorCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " getErrorDescription:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/enterprise/license/Error;->getErrorDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 49
    return-object v0

    .line 21
    :cond_2
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerNullInputParam:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v1

    if-ne p1, v1, :cond_3

    .line 23
    invoke-direct {p0, p2}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getHttpResCode1000ToModuleError(I)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0

    .line 25
    :cond_3
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v1

    if-eq p1, v1, :cond_4

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseStateError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v1

    if-eq p1, v1, :cond_4

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerSignatureError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v1

    if-ne p1, v1, :cond_5

    .line 29
    :cond_4
    invoke-direct {p0, p2}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getHttpResCode400ToModuleError(I)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0

    .line 31
    :cond_5
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerInternalServerError1:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v1

    if-ne p1, v1, :cond_6

    .line 33
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->ServerInternalError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto :goto_0

    .line 35
    :cond_6
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v1

    if-ne p1, v1, :cond_0

    .line 37
    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;->UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/ErrorManager/ErrorManagerModule;->getModuleError(Lcom/sec/esdk/elm/ErrorManager/ModuleErrorEnum;)Landroid/app/enterprise/license/Error;

    move-result-object v0

    goto/16 :goto_0
.end method
