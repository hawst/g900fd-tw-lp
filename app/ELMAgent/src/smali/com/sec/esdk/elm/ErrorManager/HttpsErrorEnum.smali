.class public final enum Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;
.super Ljava/lang/Enum;
.source "HttpsErrorEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerGeneralNetworkException:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerInternalServerError1:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerInternalServerError2:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerInternalServerError3:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerInternalServerError4:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerLicenseStateError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerNetworkDisabled:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerNullInputParam:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerSignatureError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerSuccess:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerTerminated:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum ServerTimeMismatchError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

.field public static final enum UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;


# instance fields
.field private mErrorCode:I

.field private mHttpResponseCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/16 v8, 0x3e8

    const/16 v7, 0x190

    const/16 v6, 0x1f4

    const/4 v5, 0x0

    .line 10
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerSuccess"

    invoke-direct {v0, v1, v5, v5, v5}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerSuccess:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 13
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerNullInputParam"

    const/16 v2, 0x2712

    invoke-direct {v0, v1, v9, v8, v2}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerNullInputParam:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 16
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerLicenseInvalid"

    const/4 v2, 0x2

    const/16 v3, 0x193

    const/16 v4, 0xbcc

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 19
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerLicenseStateError"

    const/4 v2, 0x3

    const/16 v3, 0xbbe

    invoke-direct {v0, v1, v2, v7, v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseStateError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 22
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerTerminated"

    const/4 v2, 0x4

    const/16 v3, 0xc1b

    invoke-direct {v0, v1, v2, v7, v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerTerminated:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 25
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerRequestEmpty"

    const/4 v2, 0x5

    const/16 v3, 0xbb9

    invoke-direct {v0, v1, v2, v7, v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 26
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerSignatureError"

    const/4 v2, 0x6

    const/16 v3, 0x191

    const/16 v4, 0xbbc

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerSignatureError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 27
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerTimeMismatchError"

    const/4 v2, 0x7

    const/16 v3, 0x191

    const/16 v4, 0xbbf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerTimeMismatchError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 30
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerInternalServerError1"

    const/16 v2, 0x8

    const/16 v3, 0xbba

    invoke-direct {v0, v1, v2, v6, v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerInternalServerError1:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 31
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerInternalServerError2"

    const/16 v2, 0x9

    const/16 v3, 0xbbb

    invoke-direct {v0, v1, v2, v6, v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerInternalServerError2:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 32
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerInternalServerError3"

    const/16 v2, 0xa

    const/16 v3, 0xbbc

    invoke-direct {v0, v1, v2, v6, v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerInternalServerError3:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 33
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerInternalServerError4"

    const/16 v2, 0xb

    const/16 v3, 0xbbf

    invoke-direct {v0, v1, v2, v6, v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerInternalServerError4:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 36
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerNetworkDisabled"

    const/16 v2, 0xc

    const/16 v3, 0x2710

    invoke-direct {v0, v1, v2, v8, v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerNetworkDisabled:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 38
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "ServerGeneralNetworkException"

    const/16 v2, 0xd

    const/16 v3, 0x2711

    invoke-direct {v0, v1, v2, v8, v3}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerGeneralNetworkException:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 41
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    const-string v1, "UserDisagreeError"

    const/16 v2, 0xe

    const/16 v3, 0x258

    const/16 v4, 0x259

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    .line 8
    const/16 v0, 0xf

    new-array v0, v0, [Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerSuccess:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerNullInputParam:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v1, v0, v9

    const/4 v1, 0x2

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseInvalid:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerLicenseStateError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerTerminated:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerSignatureError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerTimeMismatchError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerInternalServerError1:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerInternalServerError2:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerInternalServerError3:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerInternalServerError4:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerNetworkDisabled:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerGeneralNetworkException:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->UserDisagreeError:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->$VALUES:[Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p3, "httpResponseCode"    # I
    .param p4, "errorCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput p3, p0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->mHttpResponseCode:I

    .line 49
    iput p4, p0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->mErrorCode:I

    .line 50
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->$VALUES:[Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    return-object v0
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->mErrorCode:I

    return v0
.end method

.method public getHttpResponseCode()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->mHttpResponseCode:I

    return v0
.end method
