.class public Lcom/sec/esdk/elm/networkmanager/HttpsManager;
.super Ljava/lang/Thread;
.source "HttpsManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/esdk/elm/networkmanager/HttpsManager$1;
    }
.end annotation


# instance fields
.field private final ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

.field private InitVector:Ljavax/crypto/spec/IvParameterSpec;

.field private conn:Ljava/net/HttpURLConnection;

.field private httpHost:Lorg/apache/http/HttpHost;

.field private mElmContainer:Lcom/sec/esdk/elm/type/ELMContainer;

.field private mRequestData:Lorg/json/JSONObject;

.field private mSecureRandom:Ljava/security/SecureRandom;

.field private mServerURI:Ljava/lang/String;

.field private mStates:Lcom/sec/esdk/elm/type/States;

.field private proxy:Ljava/net/Proxy;

.field private secretKey:Ljavax/crypto/SecretKey;

.field private secureDataGenerator:Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/json/JSONObject;Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/type/States;)V
    .locals 5
    .param p1, "serverURI"    # Ljava/lang/String;
    .param p2, "requestData"    # Lorg/json/JSONObject;
    .param p3, "elmContainer"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p4, "states"    # Lcom/sec/esdk/elm/type/States;

    .prologue
    const/4 v4, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " Thread ID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    .line 63
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mSecureRandom:Ljava/security/SecureRandom;

    .line 64
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secretKey:Ljavax/crypto/SecretKey;

    .line 65
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    .line 68
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    .line 69
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->proxy:Ljava/net/Proxy;

    .line 70
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    .line 71
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secureDataGenerator:Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    .line 74
    iput-object p1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mRequestData:Lorg/json/JSONObject;

    .line 76
    iput-object p3, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mElmContainer:Lcom/sec/esdk/elm/type/ELMContainer;

    .line 77
    iput-object p4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mStates:Lcom/sec/esdk/elm/type/States;

    .line 78
    return-void
.end method

.method private CheckProxyEnable()V
    .locals 3

    .prologue
    .line 110
    const/4 v0, 0x0

    .line 112
    .local v0, "inetSocketAddress":Ljava/net/InetSocketAddress;
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Proxy;->getPreferredHttpHost(Landroid/content/Context;Ljava/lang/String;)Lorg/apache/http/HttpHost;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    .line 113
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    if-eqz v1, :cond_0

    .line 114
    new-instance v0, Ljava/net/InetSocketAddress;

    .end local v0    # "inetSocketAddress":Ljava/net/InetSocketAddress;
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    invoke-virtual {v1}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->httpHost:Lorg/apache/http/HttpHost;

    invoke-virtual {v2}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 115
    .restart local v0    # "inetSocketAddress":Ljava/net/InetSocketAddress;
    new-instance v1, Ljava/net/Proxy;

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    invoke-direct {v1, v2, v0}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    iput-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->proxy:Ljava/net/Proxy;

    .line 116
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpsManager.run : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Set the httpHost Proxy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 121
    :goto_0
    return-void

    .line 118
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpsManager.run : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " httpHost Proxy is NULL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private CheckResponseSign(Lorg/json/JSONObject;)Z
    .locals 6
    .param p1, "res"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 309
    const/4 v1, 0x0

    .line 310
    .local v1, "mResult":Z
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mRequestData:Lorg/json/JSONObject;

    const-string v5, "signature"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 311
    .local v2, "matchData":Ljava/lang/String;
    const-string v4, "signature"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 312
    .local v3, "server_Response":Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/esdk/elm/utils/JSON;->getHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 313
    .local v0, "OriginlaSignEncode":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 314
    const/4 v1, 0x1

    .line 317
    :cond_0
    return v1
.end method

.method private NetworkEnableCheck()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 103
    invoke-static {}, Lcom/sec/esdk/elm/networkmanager/NetworkManager;->isPossibleNetwork()Z

    move-result v0

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HttpsManager.run : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Network is disabled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 105
    new-instance v0, Landroid/accounts/NetworkErrorException;

    const-string v1, "Network is disabled"

    invoke-direct {v0, v1}, Landroid/accounts/NetworkErrorException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    return-void
.end method

.method private NetworkResponseProcess(Landroid/os/Message;Lcom/sec/esdk/elm/type/ELMContainer;ILorg/json/JSONObject;)Landroid/os/Message;
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;
    .param p2, "mElmContainer"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p3, "responseCode"    # I
    .param p4, "responseJSONData"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 384
    invoke-static {}, Lcom/sec/esdk/elm/type/States;->values()[Lcom/sec/esdk/elm/type/States;

    move-result-object v2

    .line 385
    .local v2, "states":[Lcom/sec/esdk/elm/type/States;
    move-object v0, p1

    .line 386
    .local v0, "mResult":Landroid/os/Message;
    sget-object v5, Lcom/sec/esdk/elm/networkmanager/HttpsManager$1;->$SwitchMap$com$sec$esdk$elm$type$States:[I

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mStates:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v6

    aget-object v6, v2, v6

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 411
    :goto_0
    return-object v0

    .line 388
    :pswitch_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Process Response of REGISTER_REQUEST"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 389
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getRegisterResponseContainer(Lcom/sec/esdk/elm/type/ELMContainer;ILorg/json/JSONObject;)Lcom/sec/esdk/elm/type/RegisterContainer;

    move-result-object v1

    .line 390
    .local v1, "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    sget-object v5, Lcom/sec/esdk/elm/type/States;->REGISTER_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v5

    iput v5, v0, Landroid/os/Message;->what:I

    .line 391
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0

    .line 395
    .end local v1    # "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    :pswitch_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Process Response of VALIDATE_REQUEST"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 396
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getValidateResponseContainer(Lcom/sec/esdk/elm/type/ELMContainer;ILorg/json/JSONObject;)Lcom/sec/esdk/elm/type/ValidateContainer;

    move-result-object v4

    .line 397
    .local v4, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    sget-object v5, Lcom/sec/esdk/elm/type/States;->VALIDATE_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v5

    iput v5, v0, Landroid/os/Message;->what:I

    .line 398
    iput-object v4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0

    .line 402
    .end local v4    # "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    :pswitch_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Process Response of UPLOAD_REQUEST"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 403
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getUploadResponseContainer(Lcom/sec/esdk/elm/type/ELMContainer;ILorg/json/JSONObject;)Lcom/sec/esdk/elm/type/UploadContainer;

    move-result-object v3

    .line 404
    .local v3, "uploadContainer":Lcom/sec/esdk/elm/type/UploadContainer;
    sget-object v5, Lcom/sec/esdk/elm/type/States;->UPLOAD_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v5

    iput v5, v0, Landroid/os/Message;->what:I

    .line 405
    iput-object v3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto/16 :goto_0

    .line 386
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private NullParameterCheck()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mRequestData:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mElmContainer:Lcom/sec/esdk/elm/type/ELMContainer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mStates:Lcom/sec/esdk/elm/type/States;

    if-nez v0, :cond_1

    .line 96
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HttpsManager.run : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Null Paramter was input"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 97
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Null Parameter"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :cond_1
    return-void
.end method

.method private ReadDataFromBufferedReader(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 4
    .param p1, "br"    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 267
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HttpsManager.run : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ReadDataFromBufferedReader()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 268
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 269
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 271
    .local v0, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 272
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 274
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private SendNetworkRequestPacket()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 233
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "HttpsManager.run : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SendNetworkRequestPacket()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 234
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getPUDSerialNumber()Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    move-result-object v1

    .line 236
    .local v1, "XSN":Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v5, "X-SS"

    const-string v6, "rna1.2"

    invoke-virtual {v4, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v5, "X-SN"

    invoke-virtual {v1}, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->getSerialNumebr()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v5, "X-SK"

    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getEncryptedAESSessionKey(Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const-string v5, "Content-Type"

    const-string v6, "application/knox-crypto-stream"

    invoke-virtual {v4, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const/16 v5, 0x2710

    invoke-virtual {v4, v5}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 241
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 243
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 246
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mRequestData:Lorg/json/JSONObject;

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getEncryptedBody(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "EncryptedBody":Ljava/lang/String;
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v2, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 248
    .local v2, "os":Ljava/io/ByteArrayOutputStream;
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 249
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 251
    new-instance v3, Ljava/io/DataOutputStream;

    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 252
    .local v3, "wr":Ljava/io/DataOutputStream;
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/DataOutputStream;->write([B)V

    .line 253
    invoke-virtual {v3}, Ljava/io/DataOutputStream;->flush()V

    .line 254
    return-void
.end method

.method private getDecryptIV()Ljavax/crypto/spec/IvParameterSpec;
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x0

    .line 175
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    if-eqz v4, :cond_1

    .line 176
    const/4 v2, 0x0

    .line 178
    .local v2, "mResult":Ljavax/crypto/spec/IvParameterSpec;
    new-instance v4, Ljava/math/BigInteger;

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    invoke-virtual {v5}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/math/BigInteger;-><init>([B)V

    new-instance v5, Ljava/math/BigInteger;

    const-string v6, "1"

    invoke-direct {v5, v6}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    .line 179
    .local v1, "mBigInteger":Ljava/math/BigInteger;
    invoke-virtual {v1}, Ljava/math/BigInteger;->toByteArray()[B

    move-result-object v4

    invoke-static {v4, v8}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    .line 180
    .local v3, "src":[B
    new-array v0, v8, [B

    .line 182
    .local v0, "dst":[B
    array-length v4, v3

    array-length v5, v0

    if-gt v4, v5, :cond_0

    .line 183
    array-length v4, v0

    array-length v5, v3

    sub-int/2addr v4, v5

    array-length v5, v3

    invoke-static {v3, v7, v0, v4, v5}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 187
    :goto_0
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    .end local v2    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    invoke-direct {v2, v0}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 193
    .end local v0    # "dst":[B
    .end local v1    # "mBigInteger":Ljava/math/BigInteger;
    .end local v3    # "src":[B
    :goto_1
    return-object v2

    .line 185
    .restart local v0    # "dst":[B
    .restart local v1    # "mBigInteger":Ljava/math/BigInteger;
    .restart local v2    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    .restart local v3    # "src":[B
    :cond_0
    invoke-static {v3, v7, v0, v7, v8}, Ljava/lang/System;->arraycopy([BI[BII)V

    goto :goto_0

    .line 192
    .end local v0    # "dst":[B
    .end local v1    # "mBigInteger":Ljava/math/BigInteger;
    .end local v2    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    .end local v3    # "src":[B
    :cond_1
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secureDataGenerator:Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getInitialVector()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    .line 193
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    goto :goto_1
.end method

.method private getDecryptedBody(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "cipherText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 221
    const/4 v0, 0x0

    .line 222
    .local v0, "DecryptedMessage":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secureDataGenerator:Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getSessionKey()Ljava/security/Key;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getDecryptIV()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getDecryptedNetworkData(Ljava/lang/String;Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;)Ljava/lang/String;

    move-result-object v0

    .line 223
    if-nez v0, :cond_0

    .line 224
    new-instance v1, Lorg/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HttpsManager.run : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " DecryptedMessage is Null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 226
    :cond_0
    return-object v0
.end method

.method private getEncryptIV()Ljavax/crypto/spec/IvParameterSpec;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    .line 170
    :goto_0
    return-object v0

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secureDataGenerator:Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getInitialVector()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    .line 170
    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->InitVector:Ljavax/crypto/spec/IvParameterSpec;

    goto :goto_0
.end method

.method private getEncryptedAESSessionKey(Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;)Ljava/lang/String;
    .locals 4
    .param p1, "mPUBSerialNumber"    # Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    .prologue
    .line 197
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secureDataGenerator:Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getSessionKey()Ljava/security/Key;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getEncryptIV()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p1}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getEncryptedAESSessionKey(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;)Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "mResult":Ljava/lang/String;
    return-object v0
.end method

.method private getEncryptedBody(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "plainText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 212
    const/4 v0, 0x0

    .line 213
    .local v0, "encryptedMessage":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secureDataGenerator:Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getSessionKey()Ljava/security/Key;

    move-result-object v2

    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getEncryptIV()Ljavax/crypto/spec/IvParameterSpec;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getEncryptedNetworkData(Ljava/lang/String;Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;)Ljava/lang/String;

    move-result-object v0

    .line 214
    if-nez v0, :cond_0

    .line 215
    new-instance v1, Lorg/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HttpsManager.run : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " CompriseEncryptedMessage is Null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 217
    :cond_0
    return-object v0
.end method

.method private getPUDSerialNumber()Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;
    .locals 4

    .prologue
    .line 202
    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->NORMAL_SN:Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    .line 203
    .local v1, "mResult":Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 204
    .local v0, "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSELMActivated()Z

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSURL()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 205
    :cond_0
    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->NO_HOME_CALLING_SN:Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    .line 207
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HttpsManager.run : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ALIAS : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->getAlias()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 208
    return-object v1
.end method

.method private getRegisterExceptionErrorContainer(Lcom/sec/esdk/elm/type/HttpsManagerError;Lcom/sec/esdk/elm/type/ELMContainer;)Lcom/sec/esdk/elm/type/RegisterContainer;
    .locals 6
    .param p1, "httpsManagerError"    # Lcom/sec/esdk/elm/type/HttpsManagerError;
    .param p2, "elmContainer"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 573
    move-object v0, p2

    check-cast v0, Lcom/sec/esdk/elm/type/RegisterContainer;

    .line 574
    .local v0, "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    if-nez v0, :cond_0

    .line 575
    new-instance v0, Lcom/sec/esdk/elm/type/RegisterContainer;

    .end local v0    # "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-direct {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;-><init>()V

    .line 577
    .restart local v0    # "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    :cond_0
    new-instance v1, Landroid/app/enterprise/license/Error;

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorDesc()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 579
    .local v1, "registerError":Landroid/app/enterprise/license/Error;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setError(Landroid/app/enterprise/license/Error;)V

    .line 580
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setInstanceId(Ljava/lang/String;)V

    .line 581
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setRO(Landroid/app/enterprise/license/RightsObject;)V

    .line 582
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setServerTime(Ljava/lang/String;)V

    .line 583
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v2

    const-string v3, "fail"

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setStatus(Ljava/lang/String;)V

    .line 584
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setPackageName(Ljava/lang/String;)V

    .line 585
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageVer()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setPackageVer(Ljava/lang/String;)V

    .line 587
    return-object v0
.end method

.method private getRegisterResponseContainer(Lcom/sec/esdk/elm/type/ELMContainer;ILorg/json/JSONObject;)Lcom/sec/esdk/elm/type/RegisterContainer;
    .locals 7
    .param p1, "mElmContainer"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p2, "responseCode"    # I
    .param p3, "responseJSONData"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 416
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v0

    .local v0, "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    move-object v1, p1

    .line 417
    check-cast v1, Lcom/sec/esdk/elm/type/RegisterContainer;

    .line 418
    .local v1, "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setPackageName(Ljava/lang/String;)V

    .line 419
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v5

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getPakcageVer()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setPackageVer(Ljava/lang/String;)V

    .line 420
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v5

    const-string v6, "status"

    invoke-virtual {p3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setStatus(Ljava/lang/String;)V

    .line 422
    const-string v5, "status"

    invoke-virtual {p3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "success"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 423
    invoke-direct {p0, p3}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->CheckResponseSign(Lorg/json/JSONObject;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 424
    new-instance v5, Ljava/lang/Exception;

    const-string v6, "Invalid Response signature"

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 426
    :cond_0
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v5

    const-string v6, "instance_id"

    invoke-virtual {p3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setInstanceId(Ljava/lang/String;)V

    .line 427
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v5

    const-string v6, "servertime"

    invoke-virtual {p3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setServerTime(Ljava/lang/String;)V

    .line 431
    const/4 v4, 0x0

    .line 432
    .local v4, "rightsObject":Landroid/app/enterprise/license/RightsObject;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/Preferences;->getKLMSStatus()Z

    move-result v5

    if-nez v5, :cond_1

    .line 433
    const-string v5, "HttpsManager.run BRO"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 434
    const-string v5, "BRO"

    invoke-virtual {p3, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getRightObject(Lorg/json/JSONObject;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v4

    .line 435
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v5

    sget-object v6, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->SAFE_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setLicensePermissionGroup(Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;)V

    .line 442
    :goto_0
    invoke-virtual {v4}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_DEV:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v6}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 443
    sget-object v5, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_DEV:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v5}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/enterprise/license/RightsObject;->setLicenseType(Ljava/lang/String;)V

    .line 447
    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TYPE:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 449
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setRO(Landroid/app/enterprise/license/RightsObject;)V

    .line 456
    .end local v4    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :goto_2
    return-object v1

    .line 437
    .restart local v4    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_1
    const-string v5, "HttpsManager.run RO"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 438
    const-string v5, "RO"

    invoke-virtual {p3, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getRightObject(Lorg/json/JSONObject;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v4

    .line 439
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v5

    sget-object v6, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->KNOX_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setLicensePermissionGroup(Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;)V

    goto :goto_0

    .line 445
    :cond_2
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->getLicenseKeyType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/enterprise/license/RightsObject;->setLicenseType(Ljava/lang/String;)V

    goto :goto_1

    .line 451
    .end local v4    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_3
    const-string v5, "error"

    invoke-virtual {p3, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 452
    .local v3, "responseErrorJSON":Lorg/json/JSONObject;
    new-instance v2, Landroid/app/enterprise/license/Error;

    const-string v5, "error_code"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v6, "error_description"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, p2, v5, v6}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 453
    .local v2, "responseError":Landroid/app/enterprise/license/Error;
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->setError(Landroid/app/enterprise/license/Error;)V

    goto :goto_2
.end method

.method private getRightObject(Lorg/json/JSONObject;)Landroid/app/enterprise/license/RightsObject;
    .locals 6
    .param p1, "responseRO"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 622
    new-instance v3, Landroid/app/enterprise/license/RightsObject;

    invoke-direct {v3}, Landroid/app/enterprise/license/RightsObject;-><init>()V

    .line 624
    .local v3, "rightsObject":Landroid/app/enterprise/license/RightsObject;
    const-string v4, "instance_id"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/enterprise/license/RightsObject;->setInstanceId(Ljava/lang/String;)V

    .line 625
    const-string v4, "state"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/enterprise/license/RightsObject;->setState(Ljava/lang/String;)V

    .line 626
    const-string v4, "issue_date"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/app/enterprise/license/RightsObject;->setIssueDate(J)V

    .line 627
    const-string v4, "expire_date"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/app/enterprise/license/RightsObject;->setExpiryDate(J)V

    .line 628
    const-string v4, "license_type"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/enterprise/license/RightsObject;->setLicenseType(Ljava/lang/String;)V

    .line 629
    const-string v4, "upload_frequency"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/app/enterprise/license/RightsObject;->setUploadFrequencyTime(J)V

    .line 630
    const-string v4, "upload_time"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/app/enterprise/license/RightsObject;->setUploadTime(J)V

    .line 632
    const-string v4, "permissions"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 633
    .local v1, "permissionsJSON":Lorg/json/JSONArray;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 635
    .local v2, "permissionsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 636
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 635
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 638
    :cond_0
    invoke-virtual {v3, v2}, Landroid/app/enterprise/license/RightsObject;->setPermissions(Ljava/util/List;)V

    .line 640
    return-object v3
.end method

.method private getSessionKey()Ljava/security/Key;
    .locals 4

    .prologue
    .line 152
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secretKey:Ljavax/crypto/SecretKey;

    if-eqz v2, :cond_0

    .line 153
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secretKey:Ljavax/crypto/SecretKey;

    .line 163
    :goto_0
    return-object v2

    .line 155
    :cond_0
    const/4 v1, 0x0

    .line 157
    .local v1, "keyGen":Ljavax/crypto/KeyGenerator;
    :try_start_0
    const-string v2, "AES"

    invoke-static {v2}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v1

    .line 158
    const/16 v2, 0x100

    iget-object v3, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mSecureRandom:Ljava/security/SecureRandom;

    invoke-virtual {v1, v2, v3}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 159
    invoke-virtual {v1}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secretKey:Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_1
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secretKey:Ljavax/crypto/SecretKey;

    goto :goto_0

    .line 160
    :catch_0
    move-exception v0

    .line 161
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_1
.end method

.method private getUploadExceptionErrorContainer(Lcom/sec/esdk/elm/type/HttpsManagerError;Lcom/sec/esdk/elm/type/ELMContainer;)Lcom/sec/esdk/elm/type/UploadContainer;
    .locals 5
    .param p1, "httpsManagerError"    # Lcom/sec/esdk/elm/type/HttpsManagerError;
    .param p2, "elmContainer"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 606
    move-object v0, p2

    check-cast v0, Lcom/sec/esdk/elm/type/UploadContainer;

    .line 607
    .local v0, "uploadContainer":Lcom/sec/esdk/elm/type/UploadContainer;
    if-nez v0, :cond_0

    .line 608
    new-instance v0, Lcom/sec/esdk/elm/type/UploadContainer;

    .end local v0    # "uploadContainer":Lcom/sec/esdk/elm/type/UploadContainer;
    invoke-direct {v0}, Lcom/sec/esdk/elm/type/UploadContainer;-><init>()V

    .line 610
    .restart local v0    # "uploadContainer":Lcom/sec/esdk/elm/type/UploadContainer;
    :cond_0
    new-instance v1, Landroid/app/enterprise/license/Error;

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorDesc()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 611
    .local v1, "uploadError":Landroid/app/enterprise/license/Error;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->setError(Landroid/app/enterprise/license/Error;)V

    .line 612
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->setInstanceId(Ljava/lang/String;)V

    .line 613
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->setServerTime(Ljava/lang/String;)V

    .line 614
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v2

    const-string v3, "fail"

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->setStatus(Ljava/lang/String;)V

    .line 615
    return-object v0
.end method

.method private getUploadResponseContainer(Lcom/sec/esdk/elm/type/ELMContainer;ILorg/json/JSONObject;)Lcom/sec/esdk/elm/type/UploadContainer;
    .locals 5
    .param p1, "mElmContainer"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p2, "responseCode"    # I
    .param p3, "responseJSONData"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 506
    move-object v2, p1

    check-cast v2, Lcom/sec/esdk/elm/type/UploadContainer;

    .line 507
    .local v2, "uploadContainer":Lcom/sec/esdk/elm/type/UploadContainer;
    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->setInstanceId(Ljava/lang/String;)V

    .line 508
    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v3

    const-string v4, "status"

    invoke-virtual {p3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->setStatus(Ljava/lang/String;)V

    .line 510
    const-string v3, "status"

    invoke-virtual {p3, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "success"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 511
    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v3

    const-string v4, "servertime"

    invoke-virtual {p3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->setServerTime(Ljava/lang/String;)V

    .line 517
    :goto_0
    return-object v2

    .line 513
    :cond_0
    const-string v3, "error"

    invoke-virtual {p3, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 514
    .local v1, "responseErrorJSON":Lorg/json/JSONObject;
    new-instance v0, Landroid/app/enterprise/license/Error;

    const-string v3, "error_code"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v4, "error_description"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, p2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 515
    .local v0, "responseError":Landroid/app/enterprise/license/Error;
    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->setError(Landroid/app/enterprise/license/Error;)V

    goto :goto_0
.end method

.method private getValidateExceptionErrorContainer(Lcom/sec/esdk/elm/type/HttpsManagerError;Lcom/sec/esdk/elm/type/ELMContainer;)Lcom/sec/esdk/elm/type/ValidateContainer;
    .locals 6
    .param p1, "httpsManagerError"    # Lcom/sec/esdk/elm/type/HttpsManagerError;
    .param p2, "elmContainer"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 591
    move-object v0, p2

    check-cast v0, Lcom/sec/esdk/elm/type/ValidateContainer;

    .line 592
    .local v0, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    if-nez v0, :cond_0

    .line 593
    new-instance v0, Lcom/sec/esdk/elm/type/ValidateContainer;

    .end local v0    # "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    invoke-direct {v0}, Lcom/sec/esdk/elm/type/ValidateContainer;-><init>()V

    .line 595
    .restart local v0    # "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    :cond_0
    new-instance v1, Landroid/app/enterprise/license/Error;

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v2

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v3

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorDesc()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 597
    .local v1, "validateError":Landroid/app/enterprise/license/Error;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setError(Landroid/app/enterprise/license/Error;)V

    .line 598
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setRO(Landroid/app/enterprise/license/RightsObject;)V

    .line 599
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setServerTime(Ljava/lang/String;)V

    .line 600
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v2

    const-string v3, "fail"

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setStatus(Ljava/lang/String;)V

    .line 601
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setInstanceId(Ljava/lang/String;)V

    .line 602
    return-object v0
.end method

.method private getValidateResponseContainer(Lcom/sec/esdk/elm/type/ELMContainer;ILorg/json/JSONObject;)Lcom/sec/esdk/elm/type/ValidateContainer;
    .locals 7
    .param p1, "mElmContainer"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p2, "responseCode"    # I
    .param p3, "responseJSONData"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 461
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v0

    .local v0, "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    move-object v4, p1

    .line 462
    check-cast v4, Lcom/sec/esdk/elm/type/ValidateContainer;

    .line 463
    .local v4, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    new-instance v5, Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    invoke-direct {v5}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;-><init>()V

    invoke-virtual {v4, v5}, Lcom/sec/esdk/elm/type/ValidateContainer;->setValidateResponseContainer(Lcom/sec/esdk/elm/type/ValidateResponseContainer;)V

    .line 464
    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v5

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setInstanceId(Ljava/lang/String;)V

    .line 465
    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v5

    const-string v6, "status"

    invoke-virtual {p3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setStatus(Ljava/lang/String;)V

    .line 467
    const-string v5, "status"

    invoke-virtual {p3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "success"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 468
    invoke-direct {p0, p3}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->CheckResponseSign(Lorg/json/JSONObject;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 469
    new-instance v5, Ljava/lang/Exception;

    const-string v6, "Invalid Response signature"

    invoke-direct {v5, v6}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v5

    .line 471
    :cond_0
    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v5

    const-string v6, "servertime"

    invoke-virtual {p3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setServerTime(Ljava/lang/String;)V

    .line 472
    const-string v5, "RO"

    invoke-virtual {p3, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 476
    const/4 v3, 0x0

    .line 478
    .local v3, "rightsObject":Landroid/app/enterprise/license/RightsObject;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/Preferences;->getKLMSStatus()Z

    move-result v5

    if-nez v5, :cond_2

    .line 479
    const-string v5, "HttpsManager.run BRO"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 480
    const-string v5, "BRO"

    invoke-virtual {p3, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getRightObject(Lorg/json/JSONObject;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v3

    .line 481
    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v5

    sget-object v6, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->SAFE_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setLicensePermissionGroup(Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;)V

    .line 488
    :goto_0
    invoke-virtual {v3}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_DEV:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v6}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 489
    sget-object v5, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_DEV:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v5}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/enterprise/license/RightsObject;->setLicenseType(Ljava/lang/String;)V

    .line 493
    :goto_1
    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setRO(Landroid/app/enterprise/license/RightsObject;)V

    .line 501
    .end local v3    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_1
    :goto_2
    return-object v4

    .line 483
    .restart local v3    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_2
    const-string v5, "HttpsManager.run RO"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 484
    const-string v5, "RO"

    invoke-virtual {p3, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getRightObject(Lorg/json/JSONObject;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v3

    .line 485
    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v5

    sget-object v6, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->KNOX_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v5, v6}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setLicensePermissionGroup(Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;)V

    goto :goto_0

    .line 491
    :cond_3
    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getLicenseKeyType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/app/enterprise/license/RightsObject;->setLicenseType(Ljava/lang/String;)V

    goto :goto_1

    .line 496
    .end local v3    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_4
    const-string v5, "error"

    invoke-virtual {p3, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 497
    .local v2, "responseErrorJSON":Lorg/json/JSONObject;
    new-instance v1, Landroid/app/enterprise/license/Error;

    const-string v5, "error_code"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v6, "error_description"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, p2, v5, v6}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 498
    .local v1, "responseError":Landroid/app/enterprise/license/Error;
    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->setError(Landroid/app/enterprise/license/Error;)V

    goto :goto_2
.end method

.method private processExceptionError(Lcom/sec/esdk/elm/type/HttpsManagerError;)V
    .locals 8
    .param p1, "httpsManagerError"    # Lcom/sec/esdk/elm/type/HttpsManagerError;

    .prologue
    .line 525
    invoke-static {}, Lcom/sec/esdk/elm/type/States;->values()[Lcom/sec/esdk/elm/type/States;

    move-result-object v3

    .line 526
    .local v3, "states":[Lcom/sec/esdk/elm/type/States;
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 533
    .local v1, "msg":Landroid/os/Message;
    :try_start_0
    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mStates:Lcom/sec/esdk/elm/type/States;

    if-nez v6, :cond_0

    .line 534
    sget-object v6, Lcom/sec/esdk/elm/type/States;->UNKNOWN_STATE:Lcom/sec/esdk/elm/type/States;

    iput-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mStates:Lcom/sec/esdk/elm/type/States;

    .line 536
    :cond_0
    sget-object v6, Lcom/sec/esdk/elm/networkmanager/HttpsManager$1;->$SwitchMap$com$sec$esdk$elm$type$States:[I

    iget-object v7, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mStates:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v7

    aget-object v7, v3, v7

    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 556
    sget-object v6, Lcom/sec/esdk/elm/type/States;->UNKNOWN_STATE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v6

    iput v6, v1, Landroid/os/Message;->what:I

    .line 560
    :goto_0
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z

    .line 567
    :goto_1
    return-void

    .line 538
    :pswitch_0
    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mElmContainer:Lcom/sec/esdk/elm/type/ELMContainer;

    invoke-direct {p0, p1, v6}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getRegisterExceptionErrorContainer(Lcom/sec/esdk/elm/type/HttpsManagerError;Lcom/sec/esdk/elm/type/ELMContainer;)Lcom/sec/esdk/elm/type/RegisterContainer;

    move-result-object v2

    .line 539
    .local v2, "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    sget-object v6, Lcom/sec/esdk/elm/type/States;->REGISTER_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v6

    iput v6, v1, Landroid/os/Message;->what:I

    .line 540
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 561
    .end local v2    # "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    :catch_0
    move-exception v0

    .line 562
    .local v0, "e":Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "HttpsManager : processHttpsManagerError() "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "Exception was occurred"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 563
    sget-object v6, Lcom/sec/esdk/elm/type/States;->UNKNOWN_STATE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v6

    iput v6, v1, Landroid/os/Message;->what:I

    .line 564
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z

    .line 565
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 544
    .end local v0    # "e":Ljava/lang/Exception;
    :pswitch_1
    :try_start_1
    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mElmContainer:Lcom/sec/esdk/elm/type/ELMContainer;

    invoke-direct {p0, p1, v6}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getValidateExceptionErrorContainer(Lcom/sec/esdk/elm/type/HttpsManagerError;Lcom/sec/esdk/elm/type/ELMContainer;)Lcom/sec/esdk/elm/type/ValidateContainer;

    move-result-object v5

    .line 545
    .local v5, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    sget-object v6, Lcom/sec/esdk/elm/type/States;->VALIDATE_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v6

    iput v6, v1, Landroid/os/Message;->what:I

    .line 546
    iput-object v5, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_0

    .line 550
    .end local v5    # "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    :pswitch_2
    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mElmContainer:Lcom/sec/esdk/elm/type/ELMContainer;

    invoke-direct {p0, p1, v6}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getUploadExceptionErrorContainer(Lcom/sec/esdk/elm/type/HttpsManagerError;Lcom/sec/esdk/elm/type/ELMContainer;)Lcom/sec/esdk/elm/type/UploadContainer;

    move-result-object v4

    .line 551
    .local v4, "uploadContainer":Lcom/sec/esdk/elm/type/UploadContainer;
    sget-object v6, Lcom/sec/esdk/elm/type/States;->UPLOAD_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v6

    iput v6, v1, Landroid/os/Message;->what:I

    .line 552
    iput-object v4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 536
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private readResponseData(I)Lorg/json/JSONObject;
    .locals 7
    .param p1, "responseCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;,
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 278
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "readResponseData("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 279
    const/4 v3, 0x0

    .line 280
    .local v3, "mResult":Lorg/json/JSONObject;
    const/4 v0, 0x0

    .line 283
    .local v0, "br":Ljava/io/BufferedReader;
    sparse-switch p1, :sswitch_data_0

    .line 290
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    move-object v0, v1

    .line 292
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ReadDataFromBufferedReader(Ljava/io/BufferedReader;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->getDecryptedBody(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 293
    .local v4, "plainText":Ljava/lang/String;
    new-instance v3, Lorg/json/JSONObject;

    .end local v3    # "mResult":Lorg/json/JSONObject;
    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 301
    .restart local v3    # "mResult":Lorg/json/JSONObject;
    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 305
    :cond_0
    return-object v3

    .line 287
    .end local v4    # "plainText":Ljava/lang/String;
    :sswitch_0
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v0, v1

    .line 288
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 294
    .end local v3    # "mResult":Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 295
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 301
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    if-eqz v0, :cond_1

    .line 302
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    :cond_1
    throw v5

    .line 296
    :catch_1
    move-exception v2

    .line 297
    .local v2, "e":Ljava/lang/NullPointerException;
    :try_start_3
    throw v2

    .line 298
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v2

    .line 299
    .local v2, "e":Lorg/json/JSONException;
    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 283
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x12d -> :sswitch_0
        0x12e -> :sswitch_0
    .end sparse-switch
.end method

.method private receiverNetworkResponseCode()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 260
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 262
    .local v0, "mResult":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpsManager.run : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ResponseCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 263
    return v0
.end method

.method private setNetworkConnection(Ljava/net/URL;)V
    .locals 3
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 127
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HttpsManager.run : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " setNetworkConnection()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 130
    invoke-static {}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->trustCheckHosts()V

    .line 134
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->proxy:Ljava/net/Proxy;

    if-nez v1, :cond_0

    .line 135
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .line 143
    .local v0, "https":Ljavax/net/ssl/HttpsURLConnection;
    :goto_0
    const-string v1, "POST"

    invoke-virtual {v0, v1}, Ljavax/net/ssl/HttpsURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 144
    iput-object v0, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    .line 149
    return-void

    .line 137
    .end local v0    # "https":Ljavax/net/ssl/HttpsURLConnection;
    :cond_0
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->proxy:Ljava/net/Proxy;

    invoke-virtual {p1, v1}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .restart local v0    # "https":Ljavax/net/ssl/HttpsURLConnection;
    goto :goto_0

    .line 146
    .end local v0    # "https":Ljavax/net/ssl/HttpsURLConnection;
    :cond_1
    new-instance v1, Ljava/io/IOException;

    const-string v2, "URL is not a HTTPS Host."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static trustCheckHosts()V
    .locals 2

    .prologue
    .line 86
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-static {v1}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 87
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v1

    invoke-static {v1}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 88
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_0
    move-exception v0

    .line 89
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v1, "HttpsManager.run : trustCheckHosts has Exception()"

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 90
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 327
    :try_start_0
    new-instance v5, Ljava/security/SecureRandom;

    invoke-direct {v5}, Ljava/security/SecureRandom;-><init>()V

    iput-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mSecureRandom:Ljava/security/SecureRandom;

    .line 328
    new-instance v5, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-direct {v5}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;-><init>()V

    iput-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->secureDataGenerator:Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    .line 330
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->NullParameterCheck()V

    .line 331
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->NetworkEnableCheck()V

    .line 333
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 334
    .local v1, "msg":Landroid/os/Message;
    new-instance v4, Ljava/net/URL;

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 335
    .local v4, "url":Ljava/net/URL;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : START"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "States = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mStates:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/States;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 336
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "Server URI : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mServerURI:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 337
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->CheckProxyEnable()V

    .line 338
    invoke-direct {p0, v4}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->setNetworkConnection(Ljava/net/URL;)V

    .line 340
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_0

    .line 341
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->SendNetworkRequestPacket()V

    .line 342
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->receiverNetworkResponseCode()I

    move-result v2

    .line 343
    .local v2, "responseCode":I
    invoke-direct {p0, v2}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->readResponseData(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 344
    .local v3, "responseJSONData":Lorg/json/JSONObject;
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->mElmContainer:Lcom/sec/esdk/elm/type/ELMContainer;

    invoke-direct {p0, v1, v5, v2, v3}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->NetworkResponseProcess(Landroid/os/Message;Lcom/sec/esdk/elm/type/ELMContainer;ILorg/json/JSONObject;)Landroid/os/Message;

    .line 346
    .end local v2    # "responseCode":I
    .end local v3    # "responseJSONData":Lorg/json/JSONObject;
    :cond_0
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z
    :try_end_0
    .catch Landroid/accounts/NetworkErrorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidParameterException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 373
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_1

    .line 374
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 377
    .end local v1    # "msg":Landroid/os/Message;
    .end local v4    # "url":Ljava/net/URL;
    :cond_1
    :goto_0
    return-void

    .line 348
    :catch_0
    move-exception v0

    .line 349
    .local v0, "e":Landroid/accounts/NetworkErrorException;
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "NetworkErrorException was occurred : \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 350
    invoke-virtual {v0}, Landroid/accounts/NetworkErrorException;->printStackTrace()V

    .line 351
    sget-object v5, Lcom/sec/esdk/elm/type/HttpsManagerError;->NetworkDisabled:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->processExceptionError(Lcom/sec/esdk/elm/type/HttpsManagerError;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 373
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_1

    .line 374
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 353
    .end local v0    # "e":Landroid/accounts/NetworkErrorException;
    :catch_1
    move-exception v0

    .line 354
    .local v0, "e":Ljava/security/InvalidParameterException;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "InvalidParameterException was occurred : \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 355
    invoke-virtual {v0}, Ljava/security/InvalidParameterException;->printStackTrace()V

    .line 356
    sget-object v5, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->processExceptionError(Lcom/sec/esdk/elm/type/HttpsManagerError;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 373
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_1

    .line 374
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_0

    .line 358
    .end local v0    # "e":Ljava/security/InvalidParameterException;
    :catch_2
    move-exception v0

    .line 359
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "NullPointerException was occurred : \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 360
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 361
    sget-object v5, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->processExceptionError(Lcom/sec/esdk/elm/type/HttpsManagerError;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 373
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_1

    .line 374
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    .line 363
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_3
    move-exception v0

    .line 364
    .local v0, "e":Lorg/json/JSONException;
    :try_start_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "JSONException was occurred : \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 365
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    .line 366
    sget-object v5, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->processExceptionError(Lcom/sec/esdk/elm/type/HttpsManagerError;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 373
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_1

    .line 374
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    .line 368
    .end local v0    # "e":Lorg/json/JSONException;
    :catch_4
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/Exception;
    :try_start_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "HttpsManager.run : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->ELM_HTTPS_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "General Exception was occurred \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 370
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 371
    sget-object v5, Lcom/sec/esdk/elm/type/HttpsManagerError;->GeneralNetworkException:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-direct {p0, v5}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->processExceptionError(Lcom/sec/esdk/elm/type/HttpsManagerError;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 373
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_1

    .line 374
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    .line 373
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v6, :cond_2

    .line 374
    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v5
.end method
