.class public Lcom/sec/esdk/elm/type/RegisterRequestContainer;
.super Ljava/lang/Object;
.source "RegisterRequestContainer.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private ApkHash:Ljava/lang/String;

.field private LicenseKey:Ljava/lang/String;

.field private LicenseKeyType:Ljava/lang/String;

.field private PakcageName:Ljava/lang/String;

.field private PakcageVer:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/sec/esdk/elm/type/RegisterRequestContainer$1;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/RegisterRequestContainer$1;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKeyType:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKey:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageName:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageVer:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->ApkHash:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKeyType:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKey:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageName:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageVer:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->ApkHash:Ljava/lang/String;

    .line 56
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->readFromParcel(Landroid/os/Parcel;)V

    .line 57
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    return v0
.end method

.method public getApkHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->ApkHash:Ljava/lang/String;

    return-object v0
.end method

.method public getLicenseKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKey:Ljava/lang/String;

    return-object v0
.end method

.method public getLicenseKeyType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKeyType:Ljava/lang/String;

    return-object v0
.end method

.method public getPakcageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPakcageVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageVer:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKeyType:Ljava/lang/String;

    .line 35
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKey:Ljava/lang/String;

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageName:Ljava/lang/String;

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageVer:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->ApkHash:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public setApkHash(Ljava/lang/String;)V
    .locals 0
    .param p1, "apkHash"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->ApkHash:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setLicenseKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "licenseKey"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKey:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setLicenseKeyType(Ljava/lang/String;)V
    .locals 0
    .param p1, "iLicenseKeyType"    # Ljava/lang/String;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKeyType:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public setPakcageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "pakcageName"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageName:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setPakcageVer(Ljava/lang/String;)V
    .locals 0
    .param p1, "pakcageVer"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageVer:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKeyType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->LicenseKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->PakcageVer:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->ApkHash:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 31
    return-void
.end method
