.class public Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
.super Ljava/lang/Object;
.source "DeviceInfoCollector.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTelephonyMgr:Landroid/telephony/TelephonyManager;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    .line 25
    iput-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    .line 27
    iput-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 30
    iput-object p1, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    .line 31
    return-void
.end method

.method private getTelephonyManager()Landroid/telephony/TelephonyManager;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mTelephonyMgr:Landroid/telephony/TelephonyManager;

    return-object v0
.end method

.method private getWifiManager()Landroid/net/wifi/WifiManager;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mWifiManager:Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mWifiManager:Landroid/net/wifi/WifiManager;

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mWifiManager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method


# virtual methods
.method public getAndroidVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method public getBuildNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Landroid/os/Build;->DISPLAY:Ljava/lang/String;

    return-object v0
.end method

.method public getClientTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCountryISOFromCSC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 175
    const/4 v0, 0x0

    .line 177
    .local v0, "countryISO":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    const-string v2, "ro.csc.countryiso_code"

    invoke-static {v1, v2}, Lcom/sec/esdk/elm/utils/SystemPropertiesProxy;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 179
    :cond_0
    const-string v1, "Failed to get the CISO"

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 180
    const/4 v0, 0x0

    .line 183
    .end local v0    # "countryISO":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public getDeviceCarrier()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 135
    .local v0, "deviceCarrier":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "samsung"

    if-ne v0, v1, :cond_1

    .line 136
    :cond_0
    const/4 v0, 0x0

    .line 138
    .end local v0    # "deviceCarrier":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 7

    .prologue
    .line 54
    const/4 v3, 0x0

    .line 55
    .local v3, "result":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getIMEI()Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "imei":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getSerialFromRil()Ljava/lang/String;

    move-result-object v0

    .line 57
    .local v0, "SerialNumber":Ljava/lang/String;
    invoke-direct {p0}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "mac":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v5, "0"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    .line 60
    move-object v3, v1

    .line 67
    :goto_0
    return-object v3

    .line 61
    :cond_0
    if-eqz v0, :cond_1

    .line 62
    move-object v3, v0

    goto :goto_0

    .line 64
    :cond_1
    const-string v5, ":"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 65
    .local v4, "tmp":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/esdk/elm/utils/JSON;->getHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getDeviceSerial()Ljava/lang/String;
    .locals 3

    .prologue
    .line 147
    const/4 v0, 0x0

    .line 148
    .local v0, "androidID":Ljava/lang/String;
    const/4 v1, 0x0

    .line 151
    .local v1, "serialNO":Ljava/lang/String;
    invoke-static {}, Lcom/sec/esdk/elm/utils/Utility;->getRandomNumber()Ljava/lang/String;

    move-result-object v0

    .line 153
    if-nez v0, :cond_0

    .line 154
    const-string v2, "ro.serial"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 159
    .end local v0    # "androidID":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getEsdkVer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    iget-object v1, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;)V

    .line 124
    .local v0, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getEnterpriseSdkVer()Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getIMEI()Ljava/lang/String;
    .locals 4

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getTelephonyManager()Landroid/telephony/TelephonyManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 189
    .local v1, "imei":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 193
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 194
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-eq v2, v3, :cond_0

    .line 195
    invoke-static {v1}, Lcom/sec/esdk/elm/utils/JSON;->getHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 198
    .end local v0    # "i":I
    :goto_1
    return-object v2

    .line 193
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    .end local v0    # "i":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public getMAC()Ljava/lang/String;
    .locals 5

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getWifiManager()Landroid/net/wifi/WifiManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 220
    .local v0, "mac":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getSerialFromRil()Ljava/lang/String;

    move-result-object v2

    .line 221
    .local v2, "serialNO":Ljava/lang/String;
    const/4 v1, 0x0

    .line 223
    .local v1, "result":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 224
    move-object v1, v2

    .line 230
    :goto_0
    return-object v1

    .line 225
    :cond_0
    if-eqz v0, :cond_1

    .line 226
    const-string v3, ":"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/JSON;->getHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 228
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 79
    const/4 v1, 0x0

    .line 80
    .local v1, "mccMnc":Ljava/lang/String;
    const/4 v0, 0x0

    .line 82
    .local v0, "mcc":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    const-string v3, "gsm.sim.operator.numeric"

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/SystemPropertiesProxy;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v4, :cond_0

    .line 84
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 88
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getMNC1()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 93
    const/4 v0, 0x0

    .line 94
    .local v0, "mccMnc":Ljava/lang/String;
    const/4 v1, 0x0

    .line 96
    .local v1, "mnc":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    const-string v3, "gsm.sim.operator.numeric"

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/SystemPropertiesProxy;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v4, :cond_0

    .line 99
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 102
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getMNC2()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "mccMnc":Ljava/lang/String;
    const/4 v1, 0x0

    .line 110
    .local v1, "mnc":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    const-string v3, "gsm.sim.operator.numeric2"

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/SystemPropertiesProxy;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v2, v4, :cond_0

    .line 113
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 117
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method public getSalesCodeFromCSC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 163
    const/4 v0, 0x0

    .line 165
    .local v0, "country":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->mContext:Landroid/content/Context;

    const-string v2, "ro.csc.sales_code"

    invoke-static {v1, v2}, Lcom/sec/esdk/elm/utils/SystemPropertiesProxy;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 166
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 167
    :cond_0
    const-string v1, "Failed to get the SC"

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 168
    const/4 v0, 0x0

    .line 171
    .end local v0    # "country":Ljava/lang/String;
    :cond_1
    return-object v0
.end method

.method public getSerialFromRil()Ljava/lang/String;
    .locals 4

    .prologue
    .line 202
    const/4 v1, 0x0

    .line 203
    .local v1, "serialNum":Ljava/lang/String;
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "ril.serialnumber"

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/SystemPropertiesProxy;->get(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 205
    if-eqz v1, :cond_1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 209
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 210
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x30

    if-eq v2, v3, :cond_0

    .line 211
    invoke-static {v1}, Lcom/sec/esdk/elm/utils/JSON;->getHash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 215
    .end local v0    # "i":I
    :goto_1
    return-object v2

    .line 209
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    .end local v0    # "i":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
