.class public Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
.super Ljava/lang/Object;
.source "RedirectionDataUtil.java"


# static fields
.field private static sPreferences:Lcom/sec/esdk/elm/utils/RedirectionDataUtil;


# instance fields
.field final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "GSLBInfo"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 34
    return-void
.end method

.method public static declared-synchronized getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    const-class v1, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->sPreferences:Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    invoke-direct {v0, p0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->sPreferences:Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    .line 40
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->sPreferences:Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 37
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public getELMValidateOption()Z
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "S_ELM_VALIDATE"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getElmServerAddr()Ljava/lang/String;
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "ELM_SERVER_ADDR"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSELMActivated()Z
    .locals 3

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "S_ELM_ACTIVATED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getSPDOption()Z
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "S_ELM_SPD"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getSURL()Ljava/lang/String;
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "S_ELM_URL"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setELMValidateOption(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GSLBInfo.setELMValidateOption : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "S_ELM_VALIDATE"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 63
    return-void
.end method

.method public setElmServerAddr(Lorg/json/JSONArray;)V
    .locals 3
    .param p1, "value"    # Lorg/json/JSONArray;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ELM_SERVER_ADDR"

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 45
    return-void
.end method

.method public setSELMActivated(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GSLBInfo.setSELMActivated : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "S_ELM_ACTIVATED"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 81
    return-void
.end method

.method public setSPDOption(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GSLBInfo.setSPDOption : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "S_ELM_SPD"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 72
    return-void
.end method

.method public setSURL(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 52
    const-string v0, "GSLBInfo.setURUL is called"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "S_ELM_URL"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 54
    return-void
.end method
