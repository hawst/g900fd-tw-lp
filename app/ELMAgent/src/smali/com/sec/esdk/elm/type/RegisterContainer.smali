.class public Lcom/sec/esdk/elm/type/RegisterContainer;
.super Lcom/sec/esdk/elm/type/ELMContainer;
.source "RegisterContainer.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private mMDMRequestPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

.field private mRegisterRequestContainer:Lcom/sec/esdk/elm/type/RegisterRequestContainer;

.field private mRegisterResponseContainer:Lcom/sec/esdk/elm/type/RegisterResponseContainer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/sec/esdk/elm/type/RegisterContainer$1;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/RegisterContainer$1;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/type/RegisterContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Lcom/sec/esdk/elm/type/ELMContainer;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterRequestContainer:Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    .line 19
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterResponseContainer:Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    .line 21
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mMDMRequestPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .line 57
    new-instance v0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterRequestContainer:Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    .line 58
    new-instance v0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterResponseContainer:Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    .line 59
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mMDMRequestPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Lcom/sec/esdk/elm/type/ELMContainer;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterRequestContainer:Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    .line 19
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterResponseContainer:Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    .line 21
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mMDMRequestPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .line 53
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->readFromParcel(Landroid/os/Parcel;)V

    .line 54
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    return v0
.end method

.method public getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mMDMRequestPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    return-object v0
.end method

.method public getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterRequestContainer:Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    return-object v0
.end method

.method public getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterResponseContainer:Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterRequestContainer:Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    .line 33
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterResponseContainer:Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    .line 34
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mMDMRequestPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .line 36
    return-void
.end method

.method public setMDMRequestPushPoint(Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V
    .locals 0
    .param p1, "PushPoint"    # Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mMDMRequestPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .line 84
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterRequestContainer:Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 26
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mRegisterResponseContainer:Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 27
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RegisterContainer;->mMDMRequestPushPoint:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 29
    return-void
.end method
