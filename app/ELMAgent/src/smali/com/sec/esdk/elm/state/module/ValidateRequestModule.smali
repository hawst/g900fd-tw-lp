.class public Lcom/sec/esdk/elm/state/module/ValidateRequestModule;
.super Lcom/sec/esdk/elm/state/module/ModuleBase;
.source "ValidateRequestModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;-><init>()V

    return-void
.end method

.method private failProcess(Lcom/sec/esdk/elm/type/ValidateContainer;)V
    .locals 4
    .param p1, "mValidateContainer"    # Lcom/sec/esdk/elm/type/ValidateContainer;

    .prologue
    .line 170
    const-string v0, "ValidateRequestModule.failProcess()."

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 172
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageVersion()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getRetryCount()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 176
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->increseRetryCount()V

    .line 177
    sget-object v0, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v0

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;->ONE_HOUR:Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    invoke-static {v1}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;->getTime(Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)J

    move-result-wide v2

    invoke-static {p1, v0, v2, v3}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;->SendRetryIntent(Lcom/sec/esdk/elm/type/ELMContainer;IJ)V

    .line 181
    :goto_0
    return-void

    .line 179
    :cond_0
    invoke-virtual {p0}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;->StartToGSLB()Z

    goto :goto_0
.end method

.method private isDeletedPackage(Lcom/sec/esdk/elm/type/ELMContainer;)Z
    .locals 11
    .param p1, "container"    # Lcom/sec/esdk/elm/type/ELMContainer;

    .prologue
    const/4 v7, 0x1

    .line 114
    const-string v8, "ValidateRequestModule.isDeletedPackage()."

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 115
    const/4 v3, 0x0

    .local v3, "instanceID":Ljava/lang/String;
    move-object v6, p1

    .line 116
    check-cast v6, Lcom/sec/esdk/elm/type/ValidateContainer;

    .line 117
    .local v6, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    .line 119
    if-eqz v3, :cond_1

    .line 120
    const-string v8, "ValidateRequestModule.isDeletedPackage(). null != instanceID"

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 128
    :try_start_0
    invoke-virtual {v6}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 129
    .local v5, "pkgName":Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/esdk/elm/utils/Utility;->IsPackageExistInDevice(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 130
    new-instance v8, Landroid/content/pm/PackageManager$NameNotFoundException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "is deleted."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/content/pm/PackageManager$NameNotFoundException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 133
    .end local v5    # "pkgName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 134
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ValidateRequestModule : NameNotFoundException. :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 137
    invoke-static {v3}, Lcom/sec/esdk/elm/state/module/ModuleBase;->removeAlarm(Ljava/lang/String;)V

    .line 140
    invoke-static {v3}, Lcom/sec/esdk/elm/service/MDMBridge;->resetLicense(Ljava/lang/String;)Z

    .line 141
    const-string v8, "success"

    const/4 v9, 0x0

    invoke-static {v8, v3, v9}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)V

    .line 142
    invoke-static {v3}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteLicense(Ljava/lang/String;)Z

    .line 145
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->RecoverySKeyStatusProcessing()Z

    .line 146
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v0

    .line 147
    .local v0, "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    if-nez v0, :cond_0

    .line 148
    invoke-static {}, Landroid/os/SELinux;->isSELinuxEnforced()Z

    move-result v8

    if-ne v8, v7, :cond_0

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/Utility;->isProduct_Ship(Landroid/content/Context;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 149
    const-string v8, "enterprise_policy"

    invoke-static {v8}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v8

    invoke-static {v8}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v4

    .line 151
    .local v4, "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v4, :cond_0

    .line 153
    const/4 v8, 0x1

    :try_start_1
    invoke-interface {v4, v8}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setB2BMode(Z)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 166
    .end local v0    # "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v4    # "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :cond_0
    :goto_0
    return v7

    .line 154
    .restart local v0    # "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    .restart local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :catch_1
    move-exception v2

    .line 155
    .local v2, "e1":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 160
    .end local v0    # "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v2    # "e1":Landroid/os/RemoteException;
    .end local v4    # "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :catch_2
    move-exception v1

    .line 161
    .local v1, "e":Ljava/lang/Exception;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ValidateRequestModule.isDeletedPackage(). Exception. "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 166
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method


# virtual methods
.method public processValidateRequest(Lcom/sec/esdk/elm/type/ValidateContainer;)V
    .locals 10
    .param p1, "validateContainer"    # Lcom/sec/esdk/elm/type/ValidateContainer;

    .prologue
    .line 42
    const-string v7, "ValidateRequestModule.processValidateRequest()."

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 44
    const/4 v6, 0x0

    .line 45
    .local v6, "validateRequestJSON":Lorg/json/JSONObject;
    const/4 v2, 0x0

    .line 46
    .local v2, "instanceId":Ljava/lang/String;
    const/4 v5, 0x0

    .line 47
    .local v5, "rightObject":Landroid/app/enterprise/license/RightsObject;
    const/4 v3, 0x0

    .line 50
    .local v3, "mPushPoint":Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v2

    .line 51
    if-nez v2, :cond_0

    .line 52
    new-instance v7, Ljava/lang/Exception;

    invoke-direct {v7}, Ljava/lang/Exception;-><init>()V

    throw v7
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 103
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ValidateRequestModule.processValidateRequest(). NameNotFoundException. Package is deleted."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 111
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_0
    return-void

    .line 54
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getPushPointOfValidation()Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    move-result-object v3

    .line 55
    invoke-static {}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;->isONSELMActivated()Z

    move-result v7

    if-eqz v7, :cond_1

    sget-object v7, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->KLMS_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    if-eq v3, v7, :cond_1

    .line 56
    const-string v7, "success"

    const/4 v8, 0x0

    invoke-static {v7, v2, v8}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 106
    :catch_1
    move-exception v1

    .line 107
    .local v1, "e":Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ValidateRequestModule.processValidateRequest(). Exception."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 108
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 109
    invoke-direct {p0, p1}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;->failProcess(Lcom/sec/esdk/elm/type/ValidateContainer;)V

    goto :goto_0

    .line 60
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_2
    invoke-direct {p0, p1}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;->isDeletedPackage(Lcom/sec/esdk/elm/type/ELMContainer;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 61
    new-instance v7, Landroid/content/pm/PackageManager$NameNotFoundException;

    invoke-direct {v7}, Landroid/content/pm/PackageManager$NameNotFoundException;-><init>()V

    throw v7

    .line 64
    :cond_2
    invoke-static {v2}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v5

    .line 66
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_4

    .line 67
    :cond_3
    new-instance v7, Ljava/lang/Exception;

    invoke-direct {v7}, Ljava/lang/Exception;-><init>()V

    throw v7

    .line 69
    :cond_4
    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getUploadFrequencyTime()J

    move-result-wide v8

    invoke-static {v2, v8, v9}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;->setRepeatAlarm(Ljava/lang/String;J)V

    .line 71
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v7

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/esdk/elm/utils/JSON;->getAPKHashString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setApkHash(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v7

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getPushPointOfValidation()Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/esdk/elm/datamanager/DataManager;->compriseValidateRequestData(Lcom/sec/esdk/elm/type/ValidateRequestContainer;Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;)Lorg/json/JSONObject;

    move-result-object v6

    .line 77
    if-eqz v6, :cond_7

    .line 78
    new-instance v4, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v4, v7}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;-><init>(Landroid/content/Context;)V

    .line 80
    .local v4, "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    invoke-virtual {v4}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->isELMServerAddr()Z

    move-result v7

    if-nez v7, :cond_5

    .line 83
    invoke-virtual {p0}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;->StartToGSLB()Z

    move-result v7

    if-nez v7, :cond_5

    .line 84
    new-instance v7, Ljava/lang/Exception;

    invoke-direct {v7}, Ljava/lang/Exception;-><init>()V

    throw v7

    .line 88
    :cond_5
    sget-object v7, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;->HTTPS:Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    invoke-virtual {v4, v7}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->getELMServerAddr(Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "GSLBServerAddr":Ljava/lang/String;
    if-eqz v0, :cond_6

    .line 90
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ValidateRequestModule.processValidateRequest(). ELM HTTPS Server Addr : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 91
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "HTTPS://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/elm/license/validate"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-static {v7, v6, p1, v8}, Lcom/sec/esdk/elm/networkmanager/NetworkManager;->callRestAPI(Ljava/lang/String;Lorg/json/JSONObject;Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/type/States;)V

    goto/16 :goto_0

    .line 94
    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ValidateRequestModule.processValidateRequest(). Exception. ELM HTTPS Server Addr :"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 95
    new-instance v7, Ljava/lang/Exception;

    invoke-direct {v7}, Ljava/lang/Exception;-><init>()V

    throw v7

    .line 99
    .end local v0    # "GSLBServerAddr":Ljava/lang/String;
    .end local v4    # "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    :cond_7
    const-string v7, "ValidateRequestModule.processValidateRequest(). DataManager.compriseValidateRequestData() return Null."

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 100
    new-instance v7, Ljava/lang/Exception;

    invoke-direct {v7}, Ljava/lang/Exception;-><init>()V

    throw v7
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
.end method
