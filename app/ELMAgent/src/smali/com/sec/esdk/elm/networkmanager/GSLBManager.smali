.class public Lcom/sec/esdk/elm/networkmanager/GSLBManager;
.super Ljava/lang/Thread;
.source "GSLBManager.java"


# instance fields
.field private final ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

.field private conn:Ljava/net/HttpURLConnection;

.field private httpHost:Lorg/apache/http/HttpHost;

.field public isSuccess:Z

.field private mContext:Landroid/content/Context;

.field private mGSLBClass:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

.field private mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

.field private mRequestData:Lorg/json/JSONObject;

.field private mServerURI:Ljava/lang/String;

.field private proxy:Ljava/net/Proxy;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/json/JSONObject;Landroid/content/Context;)V
    .locals 5
    .param p1, "serverURI"    # Ljava/lang/String;
    .param p2, "requestData"    # Lorg/json/JSONObject;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " Thread ID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->isSuccess:Z

    .line 42
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    .line 45
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    .line 46
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->proxy:Ljava/net/Proxy;

    .line 47
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->httpHost:Lorg/apache/http/HttpHost;

    .line 50
    iput-object p1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mServerURI:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mRequestData:Lorg/json/JSONObject;

    .line 52
    iput-object p3, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mContext:Landroid/content/Context;

    .line 53
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_REDIRECTION:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    iput-object v0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBClass:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    .line 54
    iput-object p0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lorg/json/JSONObject;Landroid/content/Context;Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;)V
    .locals 5
    .param p1, "serverURI"    # Ljava/lang/String;
    .param p2, "requestData"    # Lorg/json/JSONObject;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "gslbClass"    # Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    .prologue
    const/4 v4, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " Thread ID : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->getId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->isSuccess:Z

    .line 42
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    .line 45
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    .line 46
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->proxy:Ljava/net/Proxy;

    .line 47
    iput-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->httpHost:Lorg/apache/http/HttpHost;

    .line 58
    iput-object p1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mServerURI:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mRequestData:Lorg/json/JSONObject;

    .line 60
    iput-object p3, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mContext:Landroid/content/Context;

    .line 61
    iput-object p4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBClass:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    .line 62
    iput-object p0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    .line 63
    return-void
.end method

.method private CheckProxyEnable()V
    .locals 3

    .prologue
    .line 84
    const/4 v0, 0x0

    .line 86
    .local v0, "inetSocketAddress":Ljava/net/InetSocketAddress;
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mServerURI:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/net/Proxy;->getPreferredHttpHost(Landroid/content/Context;Ljava/lang/String;)Lorg/apache/http/HttpHost;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->httpHost:Lorg/apache/http/HttpHost;

    .line 87
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->httpHost:Lorg/apache/http/HttpHost;

    if-eqz v1, :cond_0

    .line 88
    new-instance v0, Ljava/net/InetSocketAddress;

    .end local v0    # "inetSocketAddress":Ljava/net/InetSocketAddress;
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->httpHost:Lorg/apache/http/HttpHost;

    invoke-virtual {v1}, Lorg/apache/http/HttpHost;->getHostName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->httpHost:Lorg/apache/http/HttpHost;

    invoke-virtual {v2}, Lorg/apache/http/HttpHost;->getPort()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 89
    .restart local v0    # "inetSocketAddress":Ljava/net/InetSocketAddress;
    new-instance v1, Ljava/net/Proxy;

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    invoke-direct {v1, v2, v0}, Ljava/net/Proxy;-><init>(Ljava/net/Proxy$Type;Ljava/net/SocketAddress;)V

    iput-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->proxy:Ljava/net/Proxy;

    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GSLBManager.run : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Set the httpHost Proxy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 95
    :goto_0
    return-void

    .line 92
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GSLBManager.run : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " httpHost Proxy is NULL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private NetworkEnableCheck()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/accounts/NetworkErrorException;
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {}, Lcom/sec/esdk/elm/networkmanager/NetworkManager;->isPossibleNetwork()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GSLBManager.run : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Network is disabled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 79
    new-instance v0, Landroid/accounts/NetworkErrorException;

    const-string v1, "Network is disabled"

    invoke-direct {v0, v1}, Landroid/accounts/NetworkErrorException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    return-void
.end method

.method private NullParameterCheck()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidParameterException;
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mServerURI:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mRequestData:Lorg/json/JSONObject;

    if-nez v0, :cond_1

    .line 70
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GSLBManager.run : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Null Paramter was input"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 71
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "Null Parameter"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    return-void
.end method

.method private ReadDataFromBufferedReader(Ljava/io/BufferedReader;)Ljava/lang/String;
    .locals 4
    .param p1, "br"    # Ljava/io/BufferedReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 164
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GSLBManager.run : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ReadDataFromBufferedReader()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 166
    .local v1, "sb":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .line 168
    .local v0, "line":Ljava/lang/String;
    :goto_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 171
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private SendNetworkRequestPacket()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 136
    const-string v2, "GSLBManager.run : SendNetworkRequestPacket()"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 137
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    const/16 v3, 0x2710

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 138
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 139
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBClass:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_POLICY:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    if-ne v2, v3, :cond_0

    .line 140
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    const-string v3, "Content-Type"

    const-string v4, "application/json"

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :goto_0
    return-void

    .line 142
    :cond_0
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    const-string v3, "POST"

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 143
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    const-string v3, "Content-Type"

    const-string v4, "application/json"

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 147
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mRequestData:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->length()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 148
    .local v0, "os":Ljava/io/ByteArrayOutputStream;
    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mRequestData:Lorg/json/JSONObject;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 149
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 151
    new-instance v1, Ljava/io/DataOutputStream;

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 152
    .local v1, "wr":Ljava/io/DataOutputStream;
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/DataOutputStream;->write([B)V

    .line 153
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    goto :goto_0
.end method

.method private readResponseData(I)Ljava/lang/String;
    .locals 6
    .param p1, "responseCode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 175
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GSLBManager.run : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "readResponseData( "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " )"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 176
    const/4 v3, 0x0

    .line 177
    .local v3, "mResult":Ljava/lang/String;
    const/4 v0, 0x0

    .line 180
    .local v0, "br":Ljava/io/BufferedReader;
    sparse-switch p1, :sswitch_data_0

    .line 188
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    move-object v0, v1

    .line 191
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ReadDataFromBufferedReader(Ljava/io/BufferedReader;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 195
    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 199
    :cond_0
    return-object v3

    .line 184
    :sswitch_0
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    move-object v0, v1

    .line 185
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_0

    .line 192
    :catch_0
    move-exception v2

    .line 193
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 195
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    if-eqz v0, :cond_1

    .line 196
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    :cond_1
    throw v4

    .line 180
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x12d -> :sswitch_0
        0x12e -> :sswitch_0
    .end sparse-switch
.end method

.method private receiverNetworkResponseCode()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 158
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v0

    .line 159
    .local v0, "mResult":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GSLBManager.run : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ResponseCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 160
    return v0
.end method

.method private saveResponseJSONData(Ljava/lang/String;)V
    .locals 5
    .param p1, "in_responseData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 247
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v1

    .line 248
    .local v1, "redirectionDataUtil":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 249
    .local v2, "responseJSONData":Lorg/json/JSONObject;
    const-string v4, "service"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 250
    .local v3, "service":Ljava/lang/String;
    const-string v4, "endpoint"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 252
    .local v0, "endpoint":Lorg/json/JSONArray;
    const-string v4, "elm"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 253
    invoke-virtual {v1, v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setElmServerAddr(Lorg/json/JSONArray;)V

    .line 255
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->isSuccess:Z

    .line 256
    return-void
.end method

.method private saveResponsePolicyJSONData(Ljava/lang/String;)V
    .locals 8
    .param p1, "in_responseData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 259
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 260
    .local v2, "responseJSONData":Lorg/json/JSONObject;
    const-string v5, "elm"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 261
    .local v1, "elm":Lorg/json/JSONObject;
    const-string v5, "validate"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 262
    .local v4, "validate":Ljava/lang/String;
    const-string v5, "spd"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 263
    .local v3, "spd":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 265
    .local v0, "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    const-string v5, "on"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 266
    invoke-virtual {v0, v6}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setELMValidateOption(Z)V

    .line 271
    :goto_0
    const-string v5, "on"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 272
    invoke-virtual {v0, v6}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setSPDOption(Z)V

    .line 276
    :goto_1
    iput-boolean v6, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->isSuccess:Z

    .line 277
    return-void

    .line 268
    :cond_0
    invoke-virtual {v0, v7}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setELMValidateOption(Z)V

    goto :goto_0

    .line 274
    :cond_1
    invoke-virtual {v0, v7}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setSPDOption(Z)V

    goto :goto_1
.end method

.method private setNetworkConnection(Ljava/net/URL;)V
    .locals 3
    .param p1, "url"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 113
    const-string v1, "GSLBManager.run : setNetworkConnection()"

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 116
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->trustCheckHosts()V

    .line 120
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->proxy:Ljava/net/Proxy;

    if-nez v1, :cond_0

    .line 121
    invoke-virtual {p1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .line 129
    .local v0, "https":Ljavax/net/ssl/HttpsURLConnection;
    :goto_0
    iput-object v0, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    .line 133
    return-void

    .line 123
    .end local v0    # "https":Ljavax/net/ssl/HttpsURLConnection;
    :cond_0
    iget-object v1, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->proxy:Ljava/net/Proxy;

    invoke-virtual {p1, v1}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    .restart local v0    # "https":Ljavax/net/ssl/HttpsURLConnection;
    goto :goto_0

    .line 131
    .end local v0    # "https":Ljavax/net/ssl/HttpsURLConnection;
    :cond_1
    new-instance v1, Ljava/io/IOException;

    const-string v2, "URL is not a HTTPS Host."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private trustCheckHosts()V
    .locals 2

    .prologue
    .line 100
    :try_start_0
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-static {v1}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 101
    invoke-static {}, Ljavax/net/ssl/HttpsURLConnection;->getDefaultHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v1

    invoke-static {v1}, Ljavax/net/ssl/HttpsURLConnection;->setDefaultHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "GSLBManager.run : trustCheckHosts has Exception()"

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 104
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 206
    :try_start_0
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->NullParameterCheck()V

    .line 207
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->NetworkEnableCheck()V

    .line 208
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GSLBManager.run : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "Server URI : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mServerURI:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 209
    new-instance v3, Ljava/net/URL;

    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mServerURI:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 210
    .local v3, "url":Ljava/net/URL;
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->CheckProxyEnable()V

    .line 213
    invoke-direct {p0, v3}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->setNetworkConnection(Ljava/net/URL;)V

    .line 215
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v4, :cond_0

    .line 216
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->SendNetworkRequestPacket()V

    .line 217
    invoke-direct {p0}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->receiverNetworkResponseCode()I

    move-result v1

    .line 218
    .local v1, "responseCode":I
    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->readResponseData(I)Ljava/lang/String;

    move-result-object v2

    .line 220
    .local v2, "responseStringData":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBClass:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    sget-object v5, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_REDIRECTION:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    if-ne v4, v5, :cond_2

    .line 221
    invoke-direct {p0, v2}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->saveResponseJSONData(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/NetworkErrorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidParameterException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_4

    .line 237
    .end local v1    # "responseCode":I
    .end local v2    # "responseStringData":Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v4, :cond_1

    .line 238
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 240
    :cond_1
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    monitor-enter v5

    .line 241
    :try_start_1
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 242
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 244
    .end local v3    # "url":Ljava/net/URL;
    :goto_1
    return-void

    .line 222
    .restart local v1    # "responseCode":I
    .restart local v2    # "responseStringData":Ljava/lang/String;
    .restart local v3    # "url":Ljava/net/URL;
    :cond_2
    :try_start_2
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBClass:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    sget-object v5, Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;->GSLB_CLASS_POLICY:Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;

    if-ne v4, v5, :cond_0

    .line 223
    invoke-direct {p0, v2}, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->saveResponsePolicyJSONData(Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/accounts/NetworkErrorException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/InvalidParameterException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    goto :goto_0

    .line 227
    .end local v1    # "responseCode":I
    .end local v2    # "responseStringData":Ljava/lang/String;
    .end local v3    # "url":Ljava/net/URL;
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e":Landroid/accounts/NetworkErrorException;
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GSLBManager.run : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "NetworkErrorException was occurred : \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/accounts/NetworkErrorException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 229
    invoke-virtual {v0}, Landroid/accounts/NetworkErrorException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    .line 237
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v4, :cond_3

    .line 238
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 240
    :cond_3
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    monitor-enter v5

    .line 241
    :try_start_4
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 242
    monitor-exit v5

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .end local v0    # "e":Landroid/accounts/NetworkErrorException;
    .restart local v3    # "url":Ljava/net/URL;
    :catchall_1
    move-exception v4

    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v4

    .line 230
    .end local v3    # "url":Ljava/net/URL;
    :catch_1
    move-exception v0

    .line 231
    .local v0, "e":Ljava/security/InvalidParameterException;
    :try_start_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GSLBManager.run : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "InvalidParameterException was occurred : \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/security/InvalidParameterException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 232
    invoke-virtual {v0}, Ljava/security/InvalidParameterException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 237
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v4, :cond_4

    .line 238
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 240
    :cond_4
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    monitor-enter v5

    .line 241
    :try_start_7
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 242
    monitor-exit v5

    goto/16 :goto_1

    :catchall_2
    move-exception v4

    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v4

    .line 233
    .end local v0    # "e":Ljava/security/InvalidParameterException;
    :catch_2
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/Exception;
    :try_start_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "GSLBManager.run : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->ELM_GSLB_MANAGER_THREAD_ID:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "General Exception was occurred \""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 235
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 237
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v4, :cond_5

    .line 238
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 240
    :cond_5
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    monitor-enter v5

    .line 241
    :try_start_9
    iget-object v4, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 242
    monitor-exit v5

    goto/16 :goto_1

    :catchall_3
    move-exception v4

    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    throw v4

    .line 237
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_4
    move-exception v4

    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    if-eqz v5, :cond_6

    .line 238
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->conn:Ljava/net/HttpURLConnection;

    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 240
    :cond_6
    iget-object v5, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    monitor-enter v5

    .line 241
    :try_start_a
    iget-object v6, p0, Lcom/sec/esdk/elm/networkmanager/GSLBManager;->mGSLBManager:Lcom/sec/esdk/elm/networkmanager/GSLBManager;

    invoke-virtual {v6}, Ljava/lang/Object;->notifyAll()V

    .line 242
    monitor-exit v5
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    throw v4

    :catchall_5
    move-exception v4

    :try_start_b
    monitor-exit v5
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    throw v4
.end method
