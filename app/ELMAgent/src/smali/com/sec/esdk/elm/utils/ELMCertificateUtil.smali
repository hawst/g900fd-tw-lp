.class public Lcom/sec/esdk/elm/utils/ELMCertificateUtil;
.super Ljava/lang/Object;
.source "ELMCertificateUtil.java"


# static fields
.field private static certResContext:Landroid/content/Context;

.field private static elmCertificateFactory:Lcom/sec/esdk/elm/utils/ELMCertificateUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    sput-object v0, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->elmCertificateFactory:Lcom/sec/esdk/elm/utils/ELMCertificateUtil;

    .line 29
    sput-object v0, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->certResContext:Landroid/content/Context;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method

.method public static getELMCertificateUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/ELMCertificateUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    sget-object v0, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->elmCertificateFactory:Lcom/sec/esdk/elm/utils/ELMCertificateUtil;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;

    invoke-direct {v0}, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->elmCertificateFactory:Lcom/sec/esdk/elm/utils/ELMCertificateUtil;

    .line 50
    :cond_0
    sput-object p0, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->certResContext:Landroid/content/Context;

    .line 51
    sget-object v0, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->elmCertificateFactory:Lcom/sec/esdk/elm/utils/ELMCertificateUtil;

    return-object v0
.end method


# virtual methods
.method public getElmKeyDecryptPubllicKey(Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;)Ljava/security/PublicKey;
    .locals 11
    .param p1, "mPUBSerialNumber"    # Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    .prologue
    .line 55
    const/4 v6, 0x0

    .line 56
    .local v6, "elmKeyDecryptPubllicKey":Ljava/security/PublicKey;
    const/4 v7, 0x0

    .line 57
    .local v7, "fInStream":Ljava/io/InputStream;
    const/4 v1, 0x0

    .line 59
    .local v1, "bin":Ljava/io/BufferedInputStream;
    sget-object v8, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->certResContext:Landroid/content/Context;

    if-nez v8, :cond_0

    .line 60
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    sput-object v8, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->certResContext:Landroid/content/Context;

    .line 63
    :cond_0
    :try_start_0
    sget-object v8, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->certResContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 64
    .local v0, "am":Landroid/content/res/AssetManager;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "certificates/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->getAlias()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v7

    .line 65
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .local v2, "bin":Ljava/io/BufferedInputStream;
    :try_start_1
    const-string v8, "X.509"

    invoke-static {v8}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v4

    .line 68
    .local v4, "cf":Ljava/security/cert/CertificateFactory;
    const/4 v3, 0x0

    .line 69
    .local v3, "cert":Ljava/security/cert/Certificate;
    :goto_0
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->available()I

    move-result v8

    if-lez v8, :cond_1

    .line 70
    invoke-virtual {v4, v2}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v3

    goto :goto_0

    .line 72
    :cond_1
    invoke-virtual {v3}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v6

    .line 84
    if-eqz v7, :cond_2

    .line 86
    :try_start_2
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 92
    :cond_2
    :goto_1
    if-eqz v2, :cond_9

    .line 94
    :try_start_3
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v1, v2

    .line 101
    .end local v0    # "am":Landroid/content/res/AssetManager;
    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .end local v3    # "cert":Ljava/security/cert/Certificate;
    .end local v4    # "cf":Ljava/security/cert/CertificateFactory;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    :cond_3
    :goto_2
    return-object v6

    .line 87
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .restart local v0    # "am":Landroid/content/res/AssetManager;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v3    # "cert":Ljava/security/cert/Certificate;
    .restart local v4    # "cf":Ljava/security/cert/CertificateFactory;
    :catch_0
    move-exception v5

    .line 88
    .local v5, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 95
    .end local v5    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 96
    .restart local v5    # "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    move-object v1, v2

    .line 97
    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto :goto_2

    .line 74
    .end local v0    # "am":Landroid/content/res/AssetManager;
    .end local v3    # "cert":Ljava/security/cert/Certificate;
    .end local v4    # "cf":Ljava/security/cert/CertificateFactory;
    .end local v5    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v5

    .line 75
    .local v5, "e":Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : FileNotFoundException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 76
    invoke-virtual {v5}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 84
    if-eqz v7, :cond_4

    .line 86
    :try_start_5
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 92
    .end local v5    # "e":Ljava/io/FileNotFoundException;
    :cond_4
    :goto_4
    if-eqz v1, :cond_3

    .line 94
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_2

    .line 95
    :catch_3
    move-exception v5

    .line 96
    .local v5, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    goto :goto_2

    .line 87
    .local v5, "e":Ljava/io/FileNotFoundException;
    :catch_4
    move-exception v5

    .line 88
    .local v5, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    goto :goto_4

    .line 77
    .end local v5    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v5

    .line 78
    .restart local v5    # "e":Ljava/io/IOException;
    :goto_5
    :try_start_7
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 79
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 84
    if-eqz v7, :cond_5

    .line 86
    :try_start_8
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 92
    :cond_5
    :goto_6
    if-eqz v1, :cond_3

    .line 94
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto/16 :goto_2

    .line 95
    :catch_6
    move-exception v5

    .line 96
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 87
    :catch_7
    move-exception v5

    .line 88
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    goto :goto_6

    .line 80
    .end local v5    # "e":Ljava/io/IOException;
    :catch_8
    move-exception v5

    .line 81
    .local v5, "e":Ljava/lang/Exception;
    :goto_7
    :try_start_a
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : Exception"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 84
    if-eqz v7, :cond_6

    .line 86
    :try_start_b
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a

    .line 92
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_6
    :goto_8
    if-eqz v1, :cond_3

    .line 94
    :try_start_c
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    goto/16 :goto_2

    .line 95
    :catch_9
    move-exception v5

    .line 96
    .local v5, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 87
    .local v5, "e":Ljava/lang/Exception;
    :catch_a
    move-exception v5

    .line 88
    .local v5, "e":Ljava/io/IOException;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Certificate : IOException"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    goto :goto_8

    .line 84
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_9
    if-eqz v7, :cond_7

    .line 86
    :try_start_d
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_b

    .line 92
    :cond_7
    :goto_a
    if-eqz v1, :cond_8

    .line 94
    :try_start_e
    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_c

    .line 97
    :cond_8
    :goto_b
    throw v8

    .line 87
    :catch_b
    move-exception v5

    .line 88
    .restart local v5    # "e":Ljava/io/IOException;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Certificate : IOException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    goto :goto_a

    .line 95
    .end local v5    # "e":Ljava/io/IOException;
    :catch_c
    move-exception v5

    .line 96
    .restart local v5    # "e":Ljava/io/IOException;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Certificate : IOException"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    goto :goto_b

    .line 84
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v0    # "am":Landroid/content/res/AssetManager;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto :goto_9

    .line 80
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    :catch_d
    move-exception v5

    move-object v1, v2

    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto/16 :goto_7

    .line 77
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    :catch_e
    move-exception v5

    move-object v1, v2

    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto/16 :goto_5

    .line 74
    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    :catch_f
    move-exception v5

    move-object v1, v2

    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto/16 :goto_3

    .end local v1    # "bin":Ljava/io/BufferedInputStream;
    .restart local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v3    # "cert":Ljava/security/cert/Certificate;
    .restart local v4    # "cf":Ljava/security/cert/CertificateFactory;
    :cond_9
    move-object v1, v2

    .end local v2    # "bin":Ljava/io/BufferedInputStream;
    .restart local v1    # "bin":Ljava/io/BufferedInputStream;
    goto/16 :goto_2
.end method
