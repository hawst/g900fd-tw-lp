.class public Lcom/sec/esdk/elm/state/module/KLMSRequestModule;
.super Lcom/sec/esdk/elm/state/module/ModuleBase;
.source "KLMSRequestModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;-><init>()V

    return-void
.end method


# virtual methods
.method public processKLMSRequest(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "extra"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 27
    const-string v2, "KLMSRequestModule.processKLMSRequest()."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 28
    const/4 v0, 0x0

    .line 29
    .local v0, "packageName":Ljava/lang/String;
    const/4 v1, 0x0

    .line 30
    .local v1, "packageVer":Ljava/lang/String;
    const/4 v6, 0x0

    .line 31
    .local v6, "mRequestPushPoint":Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;
    const/4 v5, 0x0

    .line 32
    .local v5, "error":Landroid/app/enterprise/license/Error;
    const/4 v8, 0x0

    .line 34
    .local v8, "LicenseType":Ljava/lang/String;
    if-nez p1, :cond_1

    .line 35
    :try_start_0
    new-instance v2, Ljava/lang/Exception;

    const-string v4, "Extra is Null"

    invoke-direct {v2, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :catch_0
    move-exception v10

    .line 57
    .local v10, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "KLMSRequestModule.processKLMSRequest(). Exception. : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 58
    if-nez v5, :cond_0

    .line 59
    new-instance v5, Landroid/app/enterprise/license/Error;

    .end local v5    # "error":Landroid/app/enterprise/license/Error;
    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v2

    sget-object v4, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerRequestEmpty:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v4

    invoke-direct {v5, v2, v4, v3}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 61
    .restart local v5    # "error":Landroid/app/enterprise/license/Error;
    :cond_0
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 62
    const-string v2, "fail"

    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v4, v3

    invoke-static/range {v0 .. v7}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    .line 65
    .end local v10    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 38
    :cond_1
    :try_start_1
    const-string v2, "edm.intent.extra.license.data.pkgname"

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    const-string v2, "edm.intent.extra.license.data.pkgversion"

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 40
    const-string v2, "LICENSE_TYPE"

    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_PROD:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 41
    const-string v2, "MDM_PUSH_POINT"

    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 42
    .local v9, "RegiterPushPoint":Ljava/lang/String;
    invoke-static {v9}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v6

    .line 45
    invoke-static {}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->getELMFileUtil()Lcom/sec/esdk/elm/utils/ELMFileUtil;

    move-result-object v2

    invoke-virtual {v6}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v1, v8, v4}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->setActInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 46
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2

    .line 50
    :cond_2
    new-instance v11, Landroid/content/Intent;

    const-string v2, "com.sec.esdk.elm.action.REQUEST_KLMS_STATUS"

    invoke-direct {v11, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 51
    .local v11, "mIntent":Landroid/content/Intent;
    const-string v2, "com.samsung.klmsagent"

    invoke-virtual {v11, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 52
    const-string v2, "MDM_PUSH_POINT"

    invoke-virtual {v6}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    const-string v4, "com.sec.esdk.elm.permission.ELM_INTENT_RECEIVE"

    invoke-virtual {v2, v11, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 55
    const-string v2, "KLMSRequestModule.processKLMSRequest(). END"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
