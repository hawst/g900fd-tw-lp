.class public final enum Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;
.super Ljava/lang/Enum;
.source "KLMSErrorEnum.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

.field public static final enum KLMS_EULA_DISAGREE:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

.field public static final enum KLMS_LICENSEKEY_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

.field public static final enum KLMS_LICENSEKEY_VAILD:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

.field public static final enum KLMS_NETWORK_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

.field public static final enum KLMS_SERVER_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

.field public static final enum KLMS_SERVER_LOCK_CONTAINER:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;


# instance fields
.field private mErrorCode:I

.field private mErrorDesc:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 10
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    const-string v1, "KLMS_LICENSEKEY_ERROR"

    const/16 v2, 0xa

    const-string v3, "License key is wrong"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_LICENSEKEY_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    .line 11
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    const-string v1, "KLMS_NETWORK_ERROR"

    const/16 v2, 0x14

    const-string v3, "Network is not available.(Wi-Fi, 3G, LTE etc)"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_NETWORK_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    .line 12
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    const-string v1, "KLMS_SERVER_ERROR"

    const/16 v2, 0x1e

    const-string v3, "Server send Error."

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_SERVER_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    .line 13
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    const-string v1, "KLMS_EULA_DISAGREE"

    const/16 v2, 0x28

    const-string v3, "User disagree EULA."

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_EULA_DISAGREE:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    .line 14
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    const-string v1, "KLMS_SERVER_LOCK_CONTAINER"

    const/16 v2, 0x32

    const-string v3, "License key is expired or de-activated."

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_SERVER_LOCK_CONTAINER:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    .line 17
    new-instance v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    const-string v1, "KLMS_LICENSEKEY_VAILD"

    const/4 v2, 0x5

    const/16 v3, 0x5a

    const-string v4, "License key is valid"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_LICENSEKEY_VAILD:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    .line 8
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_LICENSEKEY_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_NETWORK_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_SERVER_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_EULA_DISAGREE:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_SERVER_LOCK_CONTAINER:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_LICENSEKEY_VAILD:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->$VALUES:[Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 1
    .param p3, "errorCode"    # I
    .param p4, "errorDesc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->mErrorDesc:Ljava/lang/String;

    .line 24
    iput p3, p0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->mErrorCode:I

    .line 25
    iput-object p4, p0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->mErrorDesc:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->$VALUES:[Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    return-object v0
.end method


# virtual methods
.method public getKLMSErrorCode()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->mErrorCode:I

    return v0
.end method
