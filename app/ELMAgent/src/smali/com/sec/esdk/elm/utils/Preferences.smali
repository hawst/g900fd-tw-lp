.class public Lcom/sec/esdk/elm/utils/Preferences;
.super Ljava/lang/Object;
.source "Preferences.java"


# static fields
.field private static sPreferences:Lcom/sec/esdk/elm/utils/Preferences;


# instance fields
.field final mSharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "AndroidMail.Main"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/utils/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    .line 35
    return-void
.end method

.method public static getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    sget-object v0, Lcom/sec/esdk/elm/utils/Preferences;->sPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    if-nez v0, :cond_0

    .line 40
    if-nez p0, :cond_1

    .line 41
    new-instance v0, Lcom/sec/esdk/elm/utils/Preferences;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/esdk/elm/utils/Preferences;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/esdk/elm/utils/Preferences;->sPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    .line 45
    :cond_0
    :goto_0
    sget-object v0, Lcom/sec/esdk/elm/utils/Preferences;->sPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    return-object v0

    .line 43
    :cond_1
    new-instance v0, Lcom/sec/esdk/elm/utils/Preferences;

    invoke-direct {v0, p0}, Lcom/sec/esdk/elm/utils/Preferences;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/esdk/elm/utils/Preferences;->sPreferences:Lcom/sec/esdk/elm/utils/Preferences;

    goto :goto_0
.end method


# virtual methods
.method public InitSessionSEED()V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "SESSION_SEED"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 84
    return-void
.end method

.method public getAgentVersion()I
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "AGENT_VERSION"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getKLMSStatus()Z
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "KLMS_STATUS"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public getSessionSEED()[B
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 87
    iget-object v5, p0, Lcom/sec/esdk/elm/utils/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    const-string v6, "SESSION_SEED"

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "EncodedResult":Ljava/lang/String;
    const/4 v3, 0x0

    .line 89
    .local v3, "mResult":[B
    if-nez v0, :cond_1

    .line 90
    const/4 v2, 0x0

    .line 91
    .local v2, "keyGen":Ljavax/crypto/KeyGenerator;
    const/4 v4, 0x0

    .line 93
    .local v4, "secretKey":Ljavax/crypto/SecretKey;
    :try_start_0
    const-string v5, "AES"

    invoke-static {v5}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v2

    .line 94
    const/16 v5, 0x100

    new-instance v6, Ljava/security/SecureRandom;

    invoke-direct {v6}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v2, v5, v6}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    .line 95
    invoke-virtual {v2}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 100
    :goto_0
    if-eqz v4, :cond_0

    .line 101
    invoke-interface {v4}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v5

    invoke-static {v5, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 103
    :cond_0
    iget-object v5, p0, Lcom/sec/esdk/elm/utils/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "SESSION_SEED"

    invoke-interface {v5, v6, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 105
    .end local v2    # "keyGen":Ljavax/crypto/KeyGenerator;
    .end local v4    # "secretKey":Ljavax/crypto/SecretKey;
    :cond_1
    invoke-static {v0, v8}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    .line 106
    return-object v3

    .line 96
    .restart local v2    # "keyGen":Ljavax/crypto/KeyGenerator;
    .restart local v4    # "secretKey":Ljavax/crypto/SecretKey;
    :catch_0
    move-exception v1

    .line 97
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAgentVersion(I)V
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 71
    if-gez p1, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AGENT_VERSION"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "AGENT_VERSION"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public setKLMSStatus(Z)V
    .locals 2
    .param p1, "value"    # Z

    .prologue
    .line 50
    iget-object v0, p0, Lcom/sec/esdk/elm/utils/Preferences;->mSharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "KLMS_STATUS"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 51
    return-void
.end method
