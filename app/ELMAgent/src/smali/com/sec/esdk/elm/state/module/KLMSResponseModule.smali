.class public Lcom/sec/esdk/elm/state/module/KLMSResponseModule;
.super Lcom/sec/esdk/elm/state/module/ModuleBase;
.source "KLMSResponseModule.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;-><init>()V

    return-void
.end method

.method private IsPackageExist(Ljava/lang/String;)Z
    .locals 7
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 321
    const/4 v5, 0x0

    .line 322
    .local v5, "mResult":Z
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v4

    .line 323
    .local v4, "mLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    if-eqz v4, :cond_1

    .line 324
    move-object v0, v4

    .local v0, "arr$":[Landroid/app/enterprise/license/LicenseInfo;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 325
    .local v2, "info":Landroid/app/enterprise/license/LicenseInfo;
    invoke-virtual {v2}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 326
    const/4 v5, 0x1

    .line 324
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 330
    .end local v0    # "arr$":[Landroid/app/enterprise/license/LicenseInfo;
    .end local v1    # "i$":I
    .end local v2    # "info":Landroid/app/enterprise/license/LicenseInfo;
    .end local v3    # "len$":I
    :cond_1
    return v5
.end method

.method private KLM_Active_Process(ZILjava/lang/String;)V
    .locals 4
    .param p1, "isActived"    # Z
    .param p2, "iErrorNum"    # I
    .param p3, "KLMKeyType"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 72
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v1

    .line 73
    .local v1, "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    if-ne p1, v3, :cond_1

    .line 75
    if-eqz v1, :cond_0

    .line 76
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/esdk/elm/utils/Preferences;->setKLMSStatus(Z)V

    .line 77
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->KNOXKeyValidation()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    .end local v1    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :cond_0
    :goto_0
    const-string v2, "KLM_Active_Process END."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 89
    return-void

    .line 83
    .restart local v1    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :cond_1
    :try_start_1
    new-instance v2, Ljava/lang/Exception;

    const-string v3, "Un-Expected Intent From KLM. KLM Activate Fail."

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 85
    .end local v1    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private KLM_Response_Process(ZILjava/lang/String;)V
    .locals 5
    .param p1, "isActived"    # Z
    .param p2, "iErrorNum"    # I
    .param p3, "KLMKeyType"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 100
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v2

    .line 101
    .local v2, "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    invoke-static {}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->getELMFileUtil()Lcom/sec/esdk/elm/utils/ELMFileUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->getActInformation()Lorg/json/JSONArray;

    move-result-object v1

    .line 102
    .local v1, "mSavedInfo":Lorg/json/JSONArray;
    invoke-static {}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->getELMFileUtil()Lcom/sec/esdk/elm/utils/ELMFileUtil;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->RemoveActInformation()V

    .line 104
    if-ne p1, v4, :cond_2

    .line 106
    if-eqz v2, :cond_0

    .line 107
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/utils/Preferences;->setKLMSStatus(Z)V

    .line 111
    :cond_0
    if-eqz p3, :cond_1

    const-string v3, "ON_PREMISE"

    invoke-virtual {p3, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 112
    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicense(Lorg/json/JSONArray;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    .end local v1    # "mSavedInfo":Lorg/json/JSONArray;
    .end local v2    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :goto_0
    const-string v3, "KLM_Response_Process END."

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 144
    return-void

    .line 115
    .restart local v1    # "mSavedInfo":Lorg/json/JSONArray;
    .restart local v2    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-direct {p0, v1, v3}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicense(Lorg/json/JSONArray;Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 140
    .end local v1    # "mSavedInfo":Lorg/json/JSONArray;
    .end local v2    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :catch_0
    move-exception v0

    .line 141
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 122
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "mSavedInfo":Lorg/json/JSONArray;
    .restart local v2    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :cond_2
    if-eqz v2, :cond_3

    .line 123
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/utils/Preferences;->setKLMSStatus(Z)V

    .line 126
    :cond_3
    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_SERVER_LOCK_CONTAINER:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->getKLMSErrorCode()I

    move-result v3

    if-ne p2, v3, :cond_4

    .line 128
    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicense(Lorg/json/JSONArray;Z)V

    goto :goto_0

    .line 130
    :cond_4
    sget-object v3, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_LICENSEKEY_ERROR:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->getKLMSErrorCode()I

    move-result v3

    if-ne p2, v3, :cond_5

    .line 132
    invoke-direct {p0, v1}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicense(Lorg/json/JSONArray;)V

    goto :goto_0

    .line 136
    :cond_5
    const/4 v3, 0x0

    invoke-direct {p0, v1, v3}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicense(Lorg/json/JSONArray;Z)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method private KLM_Status_Change_Process(ZILjava/lang/String;)V
    .locals 4
    .param p1, "isActived"    # Z
    .param p2, "iErrorNum"    # I
    .param p3, "KLMKeyType"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 148
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v1

    .line 150
    .local v1, "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    if-ne p1, v3, :cond_1

    .line 152
    if-eqz v1, :cond_0

    .line 153
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/esdk/elm/utils/Preferences;->setKLMSStatus(Z)V

    .line 155
    :cond_0
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->KNOXKeyValidation()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    .end local v1    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :goto_0
    const-string v2, "KLM_Status_Change_Process END."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 181
    return-void

    .line 161
    .restart local v1    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :cond_1
    if-eqz v1, :cond_2

    .line 162
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v1, v2}, Lcom/sec/esdk/elm/utils/Preferences;->setKLMSStatus(Z)V

    .line 165
    :cond_2
    sget-object v2, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->KLMS_SERVER_LOCK_CONTAINER:Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/ErrorManager/KLMSErrorEnum;->getKLMSErrorCode()I

    move-result v2

    if-ne p2, v2, :cond_3

    .line 167
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->KNOXKeyValidation()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 177
    .end local v1    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :catch_0
    move-exception v0

    .line 178
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 171
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "preferences":Lcom/sec/esdk/elm/utils/Preferences;
    :cond_3
    :try_start_2
    const-string v2, "KLM_Status_Change_Process. Un Expected Error."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 172
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->KNOXKeyValidation()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method private KNOXKeyValidation()V
    .locals 7

    .prologue
    .line 184
    const-string v5, "processKLMSResponse.KNOXKeyValidation()"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 186
    const/4 v0, 0x0

    .line 187
    .local v0, "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    sget-object v5, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v5}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v5

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v6

    if-eq v5, v6, :cond_1

    .line 210
    :cond_0
    return-void

    .line 191
    :cond_1
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_0

    .line 193
    const/4 v4, 0x0

    .line 194
    .local v4, "sInstanceId":Ljava/lang/String;
    const/4 v2, 0x0

    .line 195
    .local v2, "licenseType":Ljava/lang/String;
    const/4 v3, 0x0

    .line 197
    .local v3, "rightObject":Landroid/app/enterprise/license/RightsObject;
    aget-object v5, v0, v1

    invoke-virtual {v5}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 198
    invoke-static {v4}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v3

    .line 199
    if-eqz v3, :cond_2

    .line 200
    invoke-virtual {v3}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v2

    .line 201
    if-eqz v2, :cond_2

    .line 202
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v5

    sget-object v6, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->KLMS_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    invoke-virtual {v5, v4, v2, v6}, Lcom/sec/esdk/elm/service/ELMEngine;->startValidation(Ljava/lang/String;Ljava/lang/String;Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;)V

    .line 192
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private activeLicense(Lorg/json/JSONArray;)V
    .locals 3
    .param p1, "SavedInfo"    # Lorg/json/JSONArray;

    .prologue
    const/4 v2, 0x1

    .line 213
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 214
    .local v0, "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSELMActivated()Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 215
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicense(Lorg/json/JSONArray;Z)V

    .line 219
    :goto_0
    return-void

    .line 217
    :cond_0
    invoke-direct {p0, p1, v2}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicense(Lorg/json/JSONArray;Z)V

    goto :goto_0
.end method

.method private activeLicense(Lorg/json/JSONArray;Z)V
    .locals 19
    .param p1, "SavedInfo"    # Lorg/json/JSONArray;
    .param p2, "withEULA"    # Z

    .prologue
    .line 222
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processKLMSResponse.activeLicense("

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, ")"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 223
    if-nez p1, :cond_1

    .line 278
    :cond_0
    return-void

    .line 226
    :cond_1
    const/4 v3, 0x0

    .line 227
    .local v3, "app_string":Ljava/lang/String;
    const/4 v4, 0x0

    .line 228
    .local v4, "pkgName":Ljava/lang/String;
    const/4 v5, 0x0

    .line 229
    .local v5, "pkgVer":Ljava/lang/String;
    const/4 v6, 0x0

    .line 230
    .local v6, "type":Ljava/lang/String;
    const/4 v7, 0x0

    .line 231
    .local v7, "mRequestPushPoint":Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;
    const/16 v18, 0x0

    .line 233
    .local v18, "mObject":Lorg/json/JSONObject;
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    move/from16 v0, v17

    if-ge v0, v2, :cond_0

    .line 235
    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v18

    .line 236
    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 237
    const-string v2, "pkgName"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 238
    const-string v2, "pkgVer"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 239
    const-string v2, "type"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 240
    const-string v2, "MDMPushPoint"

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v7

    .line 241
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/esdk/elm/service/ELMEngine;->getLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 243
    const/4 v2, 0x1

    move/from16 v0, p2

    if-ne v0, v2, :cond_5

    sget-object v2, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_PROD:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 246
    sget-object v2, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_UMC:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    if-eq v7, v2, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->IsPackageExist(Ljava/lang/String;)Z

    move-result v2

    const/4 v8, 0x1

    if-ne v2, v8, :cond_4

    :cond_2
    move-object/from16 v2, p0

    .line 248
    invoke-direct/range {v2 .. v7}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicenseWithoutEULA(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 249
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    :catch_0
    move-exception v16

    .line 267
    .local v16, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processKLMSResponse.onReceive() Exception : "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 268
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V

    .line 269
    new-instance v13, Landroid/app/enterprise/license/Error;

    sget-object v2, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v2

    sget-object v8, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v8}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v8

    sget-object v9, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorDesc()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v13, v2, v8, v9}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 271
    .local v13, "mError":Landroid/app/enterprise/license/Error;
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 272
    const-string v10, "fail"

    const/4 v11, 0x0

    const/4 v12, 0x0

    sget-object v2, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v15

    move-object v8, v4

    move-object v9, v5

    move-object v14, v7

    invoke-static/range {v8 .. v15}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    .line 233
    .end local v13    # "mError":Landroid/app/enterprise/license/Error;
    .end local v16    # "e":Ljava/lang/Exception;
    :cond_3
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_0

    .line 254
    :cond_4
    :try_start_1
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v6}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicenseWithEULA(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 255
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2

    :cond_5
    move-object/from16 v2, p0

    .line 262
    invoke-direct/range {v2 .. v7}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->activeLicenseWithoutEULA(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 263
    new-instance v2, Ljava/lang/Exception;

    invoke-direct {v2}, Ljava/lang/Exception;-><init>()V

    throw v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private activeLicenseWithEULA(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "pkgVer"    # Ljava/lang/String;
    .param p3, "type"    # Ljava/lang/String;

    .prologue
    .line 305
    const-string v1, "processKLMSResponse.activeLicenseWithEULA()"

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 306
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 307
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/esdk/elm/useragreement/ConfirmDialog;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 308
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 309
    const-string v1, "edm.intent.extra.license.data.pkgname"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 310
    const-string v1, "edm.intent.extra.license.data.pkgversion"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 311
    const-string v1, "LICENSE_TYPE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 312
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 316
    const/4 v1, 0x1

    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return v1

    .line 314
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private activeLicenseWithoutEULA(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)Z
    .locals 4
    .param p1, "app_string"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "pkgVer"    # Ljava/lang/String;
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "mMDMRequestPushPoint"    # Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    .prologue
    .line 281
    const-string v2, "processKLMSResponse.activeLicenseWithoutEULA()"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 282
    const/4 v0, 0x0

    .line 283
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    .line 285
    .local v1, "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 286
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    .line 287
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v2

    sget-object v3, Lcom/sec/esdk/elm/type/States;->REGISTER_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 289
    new-instance v1, Lcom/sec/esdk/elm/type/RegisterContainer;

    .end local v1    # "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-direct {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;-><init>()V

    .line 290
    .restart local v1    # "registerContainer":Lcom/sec/esdk/elm/type/RegisterContainer;
    invoke-virtual {v1, p5}, Lcom/sec/esdk/elm/type/RegisterContainer;->setMDMRequestPushPoint(Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;)V

    .line 291
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->setLicenseKey(Ljava/lang/String;)V

    .line 292
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->setPakcageName(Ljava/lang/String;)V

    .line 293
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->setPakcageVer(Ljava/lang/String;)V

    .line 294
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterRequestContainer()Lcom/sec/esdk/elm/type/RegisterRequestContainer;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/sec/esdk/elm/type/RegisterRequestContainer;->setLicenseKeyType(Ljava/lang/String;)V

    .line 296
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 297
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z

    .line 301
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 299
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public processKLMSResponse(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "mExtra"    # Landroid/os/Bundle;

    .prologue
    .line 35
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "processKLMSResponse(Bundle) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "HANDLE_TYPE"

    invoke-virtual {p1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 37
    const/4 v3, 0x0

    .line 38
    .local v3, "KLMStatus":Z
    const/4 v1, 0x0

    .line 39
    .local v1, "KLMErrorNum":I
    const/4 v2, 0x0

    .line 40
    .local v2, "KLMKeyType":Ljava/lang/String;
    const/4 v0, 0x0

    .line 42
    .local v0, "KLMAction":Ljava/lang/String;
    const-string v4, "KLMS_KEY_STATUS"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 43
    const-string v4, "KLMS_ERROR_NUMBER"

    const/4 v5, -0x1

    invoke-virtual {p1, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 44
    const-string v4, "KLMS_KEY_TYPE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 45
    const-string v4, "HANDLE_TYPE"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "processKLMSResponse : Status:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",ErrNum:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",Type:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 48
    const-string v4, "com.samsung.klmsagent.action.KLMS_ACTIVE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 49
    invoke-direct {p0, v3, v1, v2}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->KLM_Active_Process(ZILjava/lang/String;)V

    .line 58
    :cond_0
    :goto_0
    const-string v4, "processKLMSResponse END."

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 59
    return-void

    .line 51
    :cond_1
    const-string v4, "com.samsung.klmsagent.action.KLMS_STATUS_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 52
    invoke-direct {p0, v3, v1, v2}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->KLM_Status_Change_Process(ZILjava/lang/String;)V

    goto :goto_0

    .line 54
    :cond_2
    const-string v4, "com.samsung.klmsagent.action.KLMS_RESPONSE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 55
    invoke-direct {p0, v3, v1, v2}, Lcom/sec/esdk/elm/state/module/KLMSResponseModule;->KLM_Response_Process(ZILjava/lang/String;)V

    goto :goto_0
.end method
