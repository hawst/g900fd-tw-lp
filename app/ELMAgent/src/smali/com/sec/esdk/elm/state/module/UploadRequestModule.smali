.class public Lcom/sec/esdk/elm/state/module/UploadRequestModule;
.super Lcom/sec/esdk/elm/state/module/ModuleBase;
.source "UploadRequestModule.java"


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/esdk/elm/state/module/UploadRequestModule;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/esdk/elm/state/module/UploadRequestModule;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;-><init>()V

    return-void
.end method

.method private failProcess(Lcom/sec/esdk/elm/type/UploadContainer;)V
    .locals 6
    .param p1, "mUploadContainer"    # Lcom/sec/esdk/elm/type/UploadContainer;

    .prologue
    .line 113
    const-string v3, "UploadRequestModule.failProcess()."

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageVersion()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getRetryCount()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_1

    .line 121
    :try_start_0
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v1

    .line 123
    .local v1, "mRo":Landroid/app/enterprise/license/RightsObject;
    sget-boolean v3, Lcom/sec/esdk/elm/state/module/UploadRequestModule;->$assertionsDisabled:Z

    if-nez v3, :cond_0

    if-nez v1, :cond_0

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 133
    .end local v1    # "mRo":Landroid/app/enterprise/license/RightsObject;
    :catch_0
    move-exception v0

    .line 134
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 141
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :goto_0
    return-void

    .line 124
    .restart local v1    # "mRo":Landroid/app/enterprise/license/RightsObject;
    :cond_0
    :try_start_1
    new-instance v2, Lcom/sec/esdk/elm/type/ValidateContainer;

    invoke-direct {v2}, Lcom/sec/esdk/elm/type/ValidateContainer;-><init>()V

    .line 125
    .local v2, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getRetryCount()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/ValidateContainer;->setRetryCount(I)V

    .line 126
    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/ValidateContainer;->increseRetryCount()V

    .line 127
    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setInstanceId(Ljava/lang/String;)V

    .line 128
    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setPackageName(Ljava/lang/String;)V

    .line 129
    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageVersion()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setPackageVersion(Ljava/lang/String;)V

    .line 130
    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v3

    invoke-virtual {v1}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setLicenseKeyType(Ljava/lang/String;)V

    .line 132
    sget-object v3, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v3

    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;->ONE_HOUR:Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    invoke-static {v4}, Lcom/sec/esdk/elm/state/module/UploadRequestModule;->getTime(Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Lcom/sec/esdk/elm/state/module/UploadRequestModule;->SendRetryIntent(Lcom/sec/esdk/elm/type/ELMContainer;IJ)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 135
    .end local v1    # "mRo":Landroid/app/enterprise/license/RightsObject;
    .end local v2    # "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    :catch_1
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 139
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/esdk/elm/state/module/UploadRequestModule;->StartToGSLB()Z

    goto :goto_0
.end method


# virtual methods
.method public processUploadRequest(Lcom/sec/esdk/elm/type/UploadContainer;)V
    .locals 11
    .param p1, "uploadContainer"    # Lcom/sec/esdk/elm/type/UploadContainer;

    .prologue
    .line 36
    const/4 v8, 0x0

    .line 37
    .local v8, "uploadRequestJSON":Lorg/json/JSONObject;
    const/4 v5, 0x0

    .line 40
    .local v5, "logData":Lorg/json/JSONObject;
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/state/module/UploadRequestModule;->isONSELMActivated()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "UploadRequestModule.processUploadRequest()., "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getRetryCount()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageVersion()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageVersion()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    .line 53
    invoke-static {}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->getELMFileUtil()Lcom/sec/esdk/elm/utils/ELMFileUtil;

    move-result-object v3

    .line 55
    .local v3, "elmFileUtil":Lcom/sec/esdk/elm/utils/ELMFileUtil;
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 56
    .local v6, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v4

    .line 57
    .local v4, "instanceId":Ljava/lang/String;
    invoke-virtual {v3, v6, v4}, Lcom/sec/esdk/elm/utils/ELMFileUtil;->getAPICallLogFromFile(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 59
    if-eqz v5, :cond_6

    .line 60
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/sec/esdk/elm/utils/JSON;->getAPKHashString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "apk_hash":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 63
    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    :cond_2
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v9

    invoke-virtual {v9, v1}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->setApkHash(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v9

    invoke-static {v9, v5}, Lcom/sec/esdk/elm/datamanager/DataManager;->compriseUploadRequestData(Lcom/sec/esdk/elm/type/UploadRequestContainer;Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v8

    .line 69
    if-eqz v8, :cond_5

    .line 70
    new-instance v7, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v7, v9}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;-><init>(Landroid/content/Context;)V

    .line 72
    .local v7, "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    invoke-virtual {v7}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->isELMServerAddr()Z

    move-result v9

    if-nez v9, :cond_3

    .line 75
    invoke-virtual {p0}, Lcom/sec/esdk/elm/state/module/UploadRequestModule;->StartToGSLB()Z

    move-result v9

    if-nez v9, :cond_3

    .line 76
    new-instance v9, Ljava/lang/Exception;

    invoke-direct {v9}, Ljava/lang/Exception;-><init>()V

    throw v9
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    .end local v1    # "apk_hash":Ljava/lang/String;
    .end local v3    # "elmFileUtil":Lcom/sec/esdk/elm/utils/ELMFileUtil;
    .end local v4    # "instanceId":Ljava/lang/String;
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v7    # "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    :catch_0
    move-exception v2

    .line 105
    .local v2, "e":Ljava/lang/Exception;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "UploadRequestModule.processUploadRequest(). Exception."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 106
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 107
    invoke-direct {p0, p1}, Lcom/sec/esdk/elm/state/module/UploadRequestModule;->failProcess(Lcom/sec/esdk/elm/type/UploadContainer;)V

    goto/16 :goto_0

    .line 80
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "apk_hash":Ljava/lang/String;
    .restart local v3    # "elmFileUtil":Lcom/sec/esdk/elm/utils/ELMFileUtil;
    .restart local v4    # "instanceId":Ljava/lang/String;
    .restart local v6    # "packageName":Ljava/lang/String;
    .restart local v7    # "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    :cond_3
    :try_start_1
    invoke-virtual {v7}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->isELMServerAddr()Z

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_0

    .line 81
    sget-object v9, Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;->HTTPS:Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;

    invoke-virtual {v7, v9}, Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;->getELMServerAddr(Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;)Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "GSLBServerAddr":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 83
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "UploadRequestModule.processUploadRequest(). ELM HTTPS Server Addr : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 84
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "HTTPS://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/elm/logs/upload"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    sget-object v10, Lcom/sec/esdk/elm/type/States;->UPLOAD_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-static {v9, v8, p1, v10}, Lcom/sec/esdk/elm/networkmanager/NetworkManager;->callRestAPI(Ljava/lang/String;Lorg/json/JSONObject;Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/type/States;)V

    goto/16 :goto_0

    .line 87
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "UploadRequestModule.processUploadRequest(). Exception. ELM HTTPS Server Addr :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 88
    new-instance v9, Ljava/lang/Exception;

    const-string v10, "Can\'t get Server Address."

    invoke-direct {v9, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v9

    .line 92
    .end local v0    # "GSLBServerAddr":Ljava/lang/String;
    .end local v7    # "redirectionDataManager":Lcom/sec/esdk/elm/RedirectionManager/RedirectionDataManager;
    :cond_5
    const-string v9, "UploadRequestModule.processUploadRequest(). DataManager.compriseUploadRequestData() return null."

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 93
    new-instance v9, Ljava/lang/Exception;

    invoke-direct {v9}, Ljava/lang/Exception;-><init>()V

    throw v9

    .line 96
    .end local v1    # "apk_hash":Ljava/lang/String;
    :cond_6
    const-string v9, "UploadRequestModule.processUploadRequest(). logData is null."

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 100
    .end local v3    # "elmFileUtil":Lcom/sec/esdk/elm/utils/ELMFileUtil;
    .end local v4    # "instanceId":Ljava/lang/String;
    .end local v6    # "packageName":Ljava/lang/String;
    :cond_7
    const-string v9, "UploadRequestModule.processUploadRequest(). Has Null."

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 101
    new-instance v9, Ljava/lang/Exception;

    invoke-direct {v9}, Ljava/lang/Exception;-><init>()V

    throw v9
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
.end method
