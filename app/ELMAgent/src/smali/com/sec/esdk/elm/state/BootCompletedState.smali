.class public Lcom/sec/esdk/elm/state/BootCompletedState;
.super Ljava/lang/Object;
.source "BootCompletedState.java"


# direct methods
.method public static processBootCompleted(I)V
    .locals 1
    .param p0, "isRestart"    # I

    .prologue
    .line 17
    const-string v0, "BootCompletedState : startBootCompleted() call"

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 18
    new-instance v0, Lcom/sec/esdk/elm/state/module/BootCompletedModule;

    invoke-direct {v0}, Lcom/sec/esdk/elm/state/module/BootCompletedModule;-><init>()V

    invoke-virtual {v0, p0}, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->startBootCompleted(I)V

    .line 19
    return-void
.end method
