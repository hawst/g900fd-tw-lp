.class public Lcom/sec/esdk/elm/utils/LicenseKeyUtil;
.super Ljava/lang/Object;
.source "LicenseKeyUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getLicenseKeyFromPackedKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "packedKey"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 17
    :try_start_0
    const-string v3, "=1"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 24
    :goto_0
    return-object v2

    .line 20
    :cond_0
    new-instance v1, Ljava/lang/String;

    const-string v3, "=1"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([B)V

    .line 21
    .local v1, "tmp":Ljava/lang/String;
    const/4 v3, 0x0

    const-string v4, "!"

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 22
    .end local v1    # "tmp":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 23
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getURLFromPackedKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "packedKey"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 30
    :try_start_0
    const-string v3, "=1"

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 37
    :goto_0
    return-object v2

    .line 33
    :cond_0
    new-instance v1, Ljava/lang/String;

    const-string v3, "=1"

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([B)V

    .line 34
    .local v1, "tmp":Ljava/lang/String;
    const-string v3, "!"

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 35
    .end local v1    # "tmp":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 36
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isSKey(Ljava/lang/String;)Z
    .locals 1
    .param p1, "licenseKey"    # Ljava/lang/String;

    .prologue
    .line 42
    const-string v0, "=1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
