.class public Lcom/sec/esdk/elm/state/module/BootCompletedModule;
.super Lcom/sec/esdk/elm/state/module/ModuleBase;
.source "BootCompletedModule.java"


# static fields
.field private static arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;-><init>()V

    return-void
.end method

.method private IsAgentUpgrade()Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 35
    const-string v7, "IsAgentUpgrade()"

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 36
    const/4 v6, 0x0

    .line 38
    .local v6, "result":Z
    const/4 v5, 0x0

    .line 39
    .local v5, "previousVersion":I
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/utils/ELMLog;->getVersionName()I

    move-result v2

    .line 40
    .local v2, "currentVersion":I
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/Preferences;->getPreferences(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/Preferences;

    move-result-object v4

    .line 41
    .local v4, "mPreferences":Lcom/sec/esdk/elm/utils/Preferences;
    invoke-virtual {v4}, Lcom/sec/esdk/elm/utils/Preferences;->getAgentVersion()I

    move-result v5

    .line 42
    if-le v2, v5, :cond_2

    .line 43
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Agent has Upgrade. "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " -> "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 44
    const/4 v6, 0x1

    .line 45
    invoke-virtual {v4, v2}, Lcom/sec/esdk/elm/utils/Preferences;->setAgentVersion(I)V

    .line 53
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 54
    .local v0, "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSELMActivated()Z

    move-result v7

    if-ne v7, v9, :cond_0

    .line 56
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 58
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v7, "elm_skey_activation_state"

    const/4 v8, 0x1

    invoke-static {v1, v7, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 59
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSPDOption()Z

    move-result v7

    if-ne v7, v9, :cond_1

    .line 60
    const-string v7, "elm_skey_activation"

    const/4 v8, 0x0

    invoke-static {v1, v7, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 74
    .end local v0    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "currentVersion":I
    .end local v4    # "mPreferences":Lcom/sec/esdk/elm/utils/Preferences;
    :cond_0
    :goto_0
    return v6

    .line 62
    .restart local v0    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .restart local v1    # "cr":Landroid/content/ContentResolver;
    .restart local v2    # "currentVersion":I
    .restart local v4    # "mPreferences":Lcom/sec/esdk/elm/utils/Preferences;
    :cond_1
    const-string v7, "elm_skey_activation"

    const/4 v8, 0x1

    invoke-static {v1, v7, v8}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 70
    .end local v0    # "GSLBInfoPref":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "currentVersion":I
    .end local v4    # "mPreferences":Lcom/sec/esdk/elm/utils/Preferences;
    :catch_0
    move-exception v3

    .line 71
    .local v3, "e":Ljava/lang/Exception;
    const/4 v6, 0x0

    .line 72
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 66
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v2    # "currentVersion":I
    .restart local v4    # "mPreferences":Lcom/sec/esdk/elm/utils/Preferences;
    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method


# virtual methods
.method public startBootCompleted(I)V
    .locals 11
    .param p1, "isRestart"    # I

    .prologue
    .line 79
    invoke-static {}, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->isONSELMActivated()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    :try_start_0
    sget-object v8, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v8}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v8

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v9

    if-ne v8, v9, :cond_0

    .line 88
    const/4 v8, 0x2

    if-ne p1, v8, :cond_6

    .line 90
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v8

    sput-object v8, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    if-eqz v8, :cond_5

    .line 92
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 93
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v6

    .line 94
    .local v6, "strClientTimezone":Ljava/lang/String;
    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;->INNER_INTENT:Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .line 95
    .local v4, "mPushPoint":Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->IsAgentUpgrade()Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    .line 96
    const-string v8, "BootCompletedModule.startBootCompleted(). IsAgentUpgrade is true. Send KLMS Request Intent."

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 98
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.sec.esdk.elm.action.REQUEST_KLMS_STATUS"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 99
    .local v3, "mIntent":Landroid/content/Intent;
    const-string v8, "com.samsung.klmsagent"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    const-string v8, "MDM_PUSH_POINT"

    sget-object v9, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->FROM_NORMAL:Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "com.sec.esdk.elm.permission.ELM_INTENT_RECEIVE"

    invoke-virtual {v8, v3, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v3    # "mIntent":Landroid/content/Intent;
    .end local v4    # "mPushPoint":Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;
    .end local v6    # "strClientTimezone":Ljava/lang/String;
    :goto_1
    const/4 v8, 0x0

    sput-object v8, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    goto :goto_0

    .line 104
    .restart local v0    # "calendar":Ljava/util/Calendar;
    .restart local v4    # "mPushPoint":Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;
    .restart local v6    # "strClientTimezone":Ljava/lang/String;
    :cond_2
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    :try_start_1
    sget-object v8, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    array-length v8, v8

    if-ge v2, v8, :cond_4

    .line 105
    sget-object v8, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    aget-object v8, v8, v2

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    sget-object v8, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    aget-object v8, v8, v2

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    sget-object v8, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    aget-object v8, v8, v2

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getPackageVersion()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 107
    sget-object v8, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    aget-object v8, v8, v2

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v5

    .line 108
    .local v5, "mRo":Landroid/app/enterprise/license/RightsObject;
    if-eqz v5, :cond_3

    .line 109
    new-instance v7, Lcom/sec/esdk/elm/type/ValidateContainer;

    invoke-direct {v7}, Lcom/sec/esdk/elm/type/ValidateContainer;-><init>()V

    .line 110
    .local v7, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    invoke-virtual {v7, v4}, Lcom/sec/esdk/elm/type/ValidateContainer;->setPushPointOfValidation(Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;)V

    .line 111
    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v8

    sget-object v9, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    aget-object v9, v9, v2

    invoke-virtual {v9}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setInstanceId(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v8

    sget-object v9, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    aget-object v9, v9, v2

    invoke-virtual {v9}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setPackageName(Ljava/lang/String;)V

    .line 113
    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v8

    sget-object v9, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    aget-object v9, v9, v2

    invoke-virtual {v9}, Landroid/app/enterprise/license/LicenseInfo;->getPackageVersion()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setPackageVersion(Ljava/lang/String;)V

    .line 114
    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setClientTimezone(Ljava/lang/String;)V

    .line 115
    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v8

    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setLicenseKeyType(Ljava/lang/String;)V

    .line 116
    sget-object v8, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v8}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v8

    invoke-static {v7, v8}, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->SendMessageHandler(Lcom/sec/esdk/elm/type/ELMContainer;I)V

    .line 104
    .end local v5    # "mRo":Landroid/app/enterprise/license/RightsObject;
    .end local v7    # "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    .line 120
    :cond_4
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Get License : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/esdk/elm/state/module/BootCompletedModule;->arrayLicenseInfo:[Landroid/app/enterprise/license/LicenseInfo;

    array-length v10, v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 129
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v2    # "i":I
    .end local v4    # "mPushPoint":Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;
    .end local v6    # "strClientTimezone":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 130
    .local v1, "e":Ljava/lang/Exception;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "BootCompletedModule.startBootCompleted().Exception : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    .line 124
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    :try_start_2
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    const-string v9, "Get License : 0"

    invoke-static {v8, v9}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 127
    :cond_6
    const-string v8, "BootCompletedModule.startBootCompleted( true ). Nothing to do here."

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1
.end method
