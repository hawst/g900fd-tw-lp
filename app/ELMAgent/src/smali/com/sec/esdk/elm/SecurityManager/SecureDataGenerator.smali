.class public Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;
.super Ljava/lang/Object;
.source "SecureDataGenerator.java"


# static fields
.field private static final cipherSuites:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 49
    .local v0, "tmpMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Byte;Ljava/lang/String;>;"
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "AES/CBC/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "AES/ECB/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "DESede/CBC/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    const-string v2, "DESede/ECB/PKCS5Padding"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    sput-object v1, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->cipherSuites:Ljava/util/Map;

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private compriseSignatureData(Ljava/lang/String;BJ)Ljava/lang/String;
    .locals 5
    .param p1, "hash"    # Ljava/lang/String;
    .param p2, "dataFormatId"    # B
    .param p3, "timestamp"    # J

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 331
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x18

    if-ge v0, v1, :cond_1

    .line 332
    :cond_0
    const/4 v0, 0x0

    .line 335
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "%s%02d%s%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->xor(Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private decrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B
    .locals 6
    .param p1, "inKey"    # Ljava/security/Key;
    .param p2, "ivParameterSpec"    # Ljavax/crypto/spec/IvParameterSpec;
    .param p3, "cipherData"    # [B

    .prologue
    .line 436
    const/4 v3, 0x0

    .line 437
    .local v3, "plainData":[B
    const/4 v0, 0x0

    .line 439
    .local v0, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v4

    invoke-interface {p1}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 440
    .local v2, "ks":Ljavax/crypto/spec/SecretKeySpec;
    const-string v4, "AES/CBC/PKCS7Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 441
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v2, p2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 442
    invoke-virtual {v0, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v3

    .line 462
    .end local v2    # "ks":Ljavax/crypto/spec/SecretKeySpec;
    :goto_0
    return-object v3

    .line 443
    :catch_0
    move-exception v1

    .line 444
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 445
    const/4 v3, 0x0

    .line 461
    goto :goto_0

    .line 446
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v1

    .line 447
    .local v1, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v1}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 448
    const/4 v3, 0x0

    .line 461
    goto :goto_0

    .line 449
    .end local v1    # "e":Ljava/security/InvalidKeyException;
    :catch_2
    move-exception v1

    .line 450
    .local v1, "e":Ljava/security/InvalidAlgorithmParameterException;
    invoke-virtual {v1}, Ljava/security/InvalidAlgorithmParameterException;->printStackTrace()V

    .line 451
    const/4 v3, 0x0

    .line 461
    goto :goto_0

    .line 452
    .end local v1    # "e":Ljava/security/InvalidAlgorithmParameterException;
    :catch_3
    move-exception v1

    .line 453
    .local v1, "e":Ljavax/crypto/IllegalBlockSizeException;
    invoke-virtual {v1}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    .line 454
    const/4 v3, 0x0

    .line 461
    goto :goto_0

    .line 455
    .end local v1    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_4
    move-exception v1

    .line 456
    .local v1, "e":Ljavax/crypto/BadPaddingException;
    invoke-virtual {v1}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    .line 457
    const/4 v3, 0x0

    .line 461
    goto :goto_0

    .line 458
    .end local v1    # "e":Ljavax/crypto/BadPaddingException;
    :catch_5
    move-exception v1

    .line 459
    .local v1, "e":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v1}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    .line 460
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private encrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B
    .locals 6
    .param p1, "inKey"    # Ljava/security/Key;
    .param p2, "ivParameterSpec"    # Ljavax/crypto/spec/IvParameterSpec;
    .param p3, "plainData"    # [B

    .prologue
    .line 405
    const/4 v1, 0x0

    .line 406
    .local v1, "ciphertext":[B
    const/4 v0, 0x0

    .line 408
    .local v0, "cipher":Ljavax/crypto/Cipher;
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v4

    invoke-interface {p1}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 409
    .local v3, "ks":Ljavax/crypto/spec/SecretKeySpec;
    const-string v4, "AES/CBC/PKCS7Padding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 410
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3, p2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 411
    invoke-virtual {v0, p3}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_5

    move-result-object v1

    .line 431
    .end local v3    # "ks":Ljavax/crypto/spec/SecretKeySpec;
    :goto_0
    return-object v1

    .line 412
    :catch_0
    move-exception v2

    .line 413
    .local v2, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v2}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 414
    const/4 v1, 0x0

    .line 430
    goto :goto_0

    .line 415
    .end local v2    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v2

    .line 416
    .local v2, "e":Ljava/security/InvalidKeyException;
    invoke-virtual {v2}, Ljava/security/InvalidKeyException;->printStackTrace()V

    .line 417
    const/4 v1, 0x0

    .line 430
    goto :goto_0

    .line 418
    .end local v2    # "e":Ljava/security/InvalidKeyException;
    :catch_2
    move-exception v2

    .line 419
    .local v2, "e":Ljava/security/InvalidAlgorithmParameterException;
    invoke-virtual {v2}, Ljava/security/InvalidAlgorithmParameterException;->printStackTrace()V

    .line 420
    const/4 v1, 0x0

    .line 430
    goto :goto_0

    .line 421
    .end local v2    # "e":Ljava/security/InvalidAlgorithmParameterException;
    :catch_3
    move-exception v2

    .line 422
    .local v2, "e":Ljavax/crypto/IllegalBlockSizeException;
    invoke-virtual {v2}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V

    .line 423
    const/4 v1, 0x0

    .line 430
    goto :goto_0

    .line 424
    .end local v2    # "e":Ljavax/crypto/IllegalBlockSizeException;
    :catch_4
    move-exception v2

    .line 425
    .local v2, "e":Ljavax/crypto/BadPaddingException;
    invoke-virtual {v2}, Ljavax/crypto/BadPaddingException;->printStackTrace()V

    .line 426
    const/4 v1, 0x0

    .line 430
    goto :goto_0

    .line 427
    .end local v2    # "e":Ljavax/crypto/BadPaddingException;
    :catch_5
    move-exception v2

    .line 428
    .local v2, "e":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v2}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    .line 429
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getCiphersuite(Ljava/lang/String;Ljava/lang/String;)B
    .locals 4
    .param p1, "serviceName"    # Ljava/lang/String;
    .param p2, "deviceId"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x1

    .line 268
    const/4 v0, -0x1

    .line 270
    .local v0, "dataFormatID":B
    if-nez p1, :cond_1

    .line 271
    const-string v2, "getCiphersuite : serviceName is null"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 292
    :cond_0
    :goto_0
    return v1

    .line 275
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getDataFormatID(Ljava/lang/String;)B

    move-result v0

    .line 276
    if-ne v0, v1, :cond_2

    .line 277
    const-string v2, "getCiphersuite : dataFormatId is null"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 282
    :cond_2
    if-nez p2, :cond_3

    .line 283
    const-string v2, "getCiphersuite : deviceId is null"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 287
    :cond_3
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v2, v3

    rem-int/lit8 v2, v2, 0x4

    int-to-byte v1, v2

    .line 289
    .local v1, "num":B
    if-gez v1, :cond_0

    .line 290
    mul-int/lit8 v2, v1, -0x1

    int-to-byte v1, v2

    goto :goto_0
.end method

.method private getDataFormatID(Ljava/lang/String;)B
    .locals 5
    .param p1, "serviceName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 192
    new-instance v2, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;-><init>(Landroid/content/Context;)V

    .line 193
    .local v2, "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    const/4 v0, 0x0

    .line 195
    .local v0, "IMEI":Ljava/lang/String;
    const/4 v1, -0x1

    .line 197
    .local v1, "dataFormatID":B
    if-nez p1, :cond_0

    .line 198
    const-string v3, "getDataFormatID : serviceName is null"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 199
    const/4 v3, -0x1

    .line 222
    :goto_0
    return v3

    .line 202
    :cond_0
    invoke-virtual {v2}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getIMEI()Ljava/lang/String;

    move-result-object v0

    .line 205
    const-string v3, "ELM"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v4, :cond_3

    .line 207
    if-eqz v0, :cond_2

    .line 208
    const/4 v1, 0x0

    :cond_1
    :goto_1
    move v3, v1

    .line 222
    goto :goto_0

    .line 210
    :cond_2
    const/4 v1, 0x1

    goto :goto_1

    .line 213
    :cond_3
    const-string v3, "GSLB"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v4, :cond_1

    .line 214
    if-eqz v0, :cond_4

    .line 215
    const/4 v1, 0x2

    goto :goto_1

    .line 217
    :cond_4
    const/4 v1, 0x3

    goto :goto_1
.end method

.method private getDeviceID(B)Ljava/lang/String;
    .locals 3
    .param p1, "dataFormatID"    # B

    .prologue
    .line 143
    const/4 v0, 0x0

    .line 144
    .local v0, "deviceId":Ljava/lang/String;
    new-instance v1, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;-><init>(Landroid/content/Context;)V

    .line 146
    .local v1, "deviceInfoCollector":Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;
    packed-switch p1, :pswitch_data_0

    .line 158
    const/4 v0, 0x0

    .line 162
    :goto_0
    return-object v0

    .line 149
    :pswitch_0
    invoke-virtual {v1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getIMEI()Ljava/lang/String;

    move-result-object v0

    .line 150
    goto :goto_0

    .line 154
    :pswitch_1
    invoke-virtual {v1}, Lcom/sec/esdk/elm/deviceinfo/DeviceInfoCollector;->getMAC()Ljava/lang/String;

    move-result-object v0

    .line 155
    goto :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getDeviceID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "serviceName"    # Ljava/lang/String;
    .param p2, "baseData"    # Ljava/lang/String;

    .prologue
    .line 168
    const/4 v1, 0x0

    .line 170
    .local v1, "deviceId":Ljava/lang/String;
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 171
    :cond_0
    const-string v3, "getDeviceID : serviceName or baseData is null"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 172
    const/4 v3, 0x0

    .line 188
    :goto_0
    return-object v3

    .line 176
    :cond_1
    :try_start_0
    const-string v3, "GSLB"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 177
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 178
    .local v0, "baseJson":Lorg/json/JSONObject;
    const-string v3, "deviceid"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .end local v0    # "baseJson":Lorg/json/JSONObject;
    :cond_2
    :goto_1
    move-object v3, v1

    .line 188
    goto :goto_0

    .line 184
    :catch_0
    move-exception v2

    .line 185
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private getEnc1Data([BLjava/lang/String;[B)Ljava/lang/String;
    .locals 10
    .param p1, "data"    # [B
    .param p2, "cipher"    # Ljava/lang/String;
    .param p3, "F2Value"    # [B

    .prologue
    const/16 v9, 0x8

    const/4 v4, 0x0

    const/4 v8, 0x0

    const/16 v7, 0x10

    .line 297
    sget-object v5, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->cipherSuites:Ljava/util/Map;

    invoke-interface {v5, p2}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 327
    :goto_0
    return-object v4

    .line 301
    :cond_0
    if-eqz p1, :cond_1

    if-nez p3, :cond_2

    .line 302
    :cond_1
    const-string v5, "getEnc1Data : data or F2Value is null"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 308
    :cond_2
    const-string v5, "AES"

    invoke-virtual {p2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 309
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, p3, v6, v7}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    const-string v6, "AES"

    invoke-direct {v3, v5, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 310
    .local v3, "sKey":Ljava/security/Key;
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-static {v7}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5, p3, v8, v7}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v2, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 316
    .local v2, "iv":Ljavax/crypto/spec/IvParameterSpec;
    :goto_1
    :try_start_0
    invoke-static {p2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 317
    .local v0, "c":Ljavax/crypto/Cipher;
    const-string v5, "CBC"

    invoke-virtual {p2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 318
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 322
    :goto_2
    new-instance v5, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v6

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v4, v5

    goto :goto_0

    .line 312
    .end local v0    # "c":Ljavax/crypto/Cipher;
    .end local v2    # "iv":Ljavax/crypto/spec/IvParameterSpec;
    .end local v3    # "sKey":Ljava/security/Key;
    :cond_3
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const/16 v5, 0x18

    invoke-static {v5}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    const/16 v6, 0x18

    invoke-virtual {v5, p3, v8, v6}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    const-string v6, "DESede"

    invoke-direct {v3, v5, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 313
    .restart local v3    # "sKey":Ljava/security/Key;
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-static {v9}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5, p3, v8, v9}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-direct {v2, v5}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .restart local v2    # "iv":Ljavax/crypto/spec/IvParameterSpec;
    goto :goto_1

    .line 320
    .restart local v0    # "c":Ljavax/crypto/Cipher;
    :cond_4
    const/4 v5, 0x1

    :try_start_1
    invoke-virtual {v0, v5, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 324
    .end local v0    # "c":Ljavax/crypto/Cipher;
    :catch_0
    move-exception v1

    .line 325
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private getF2(BLjava/lang/String;Ljava/lang/String;J)[B
    .locals 8
    .param p1, "dataFormatId"    # B
    .param p2, "IMEIorMAC"    # Ljava/lang/String;
    .param p3, "serviceName"    # Ljava/lang/String;
    .param p4, "timestamp"    # J

    .prologue
    const/4 v5, 0x0

    .line 234
    if-eqz p3, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    if-eqz p2, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-gez v0, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-object v5

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    move-object v1, p0

    move v2, p1

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->md5bytes(B[B[B[BJ)[B

    move-result-object v5

    goto :goto_0
.end method

.method private getF2(BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)[B
    .locals 9
    .param p1, "dataFormatId"    # B
    .param p2, "IMEIorMAC"    # Ljava/lang/String;
    .param p3, "serviceName"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;
    .param p5, "timestamp"    # J

    .prologue
    .line 226
    if-eqz p3, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    if-eqz p2, :cond_0

    if-eqz p4, :cond_0

    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-gez v0, :cond_1

    .line 227
    :cond_0
    const/4 v0, 0x0

    .line 230
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {p4}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    move-object v1, p0

    move v2, p1

    move-wide v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->md5bytes(B[B[B[BJ)[B

    move-result-object v0

    goto :goto_0
.end method

.method private md5bytes(B[B[B[BJ)[B
    .locals 7
    .param p1, "b0"    # B
    .param p2, "b1"    # [B
    .param p3, "b2"    # [B
    .param p4, "b3"    # [B
    .param p5, "b4"    # J

    .prologue
    const/4 v4, 0x0

    .line 242
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-object v4

    .line 246
    :cond_1
    array-length v5, p2

    array-length v6, p3

    add-int/2addr v5, v6

    add-int/lit8 v3, v5, 0x9

    .line 247
    .local v3, "size":I
    if-eqz p4, :cond_2

    .line 248
    array-length v5, p4

    add-int/2addr v3, v5

    .line 249
    :cond_2
    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 250
    .local v0, "bf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 251
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 252
    invoke-virtual {v0, p3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 253
    if-eqz p4, :cond_3

    .line 254
    invoke-virtual {v0, p4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 255
    :cond_3
    invoke-virtual {v0, p5, p6}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 257
    :try_start_0
    const-string v5, "SHA-256"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 258
    .local v2, "md":Ljava/security/MessageDigest;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/security/MessageDigest;->update([B)V

    .line 259
    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_0

    .line 260
    .end local v2    # "md":Ljava/security/MessageDigest;
    :catch_0
    move-exception v1

    .line 261
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0
.end method

.method private xor(Ljava/lang/Long;)Ljava/lang/String;
    .locals 6
    .param p1, "orig"    # Ljava/lang/Long;

    .prologue
    .line 339
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide v4, -0x410efd670d2ddc6L    # -9.459999887958783E288

    xor-long v0, v2, v4

    .line 340
    .local v0, "xor":J
    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public getDecryptedNetworkData(Ljava/lang/String;Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;)Ljava/lang/String;
    .locals 6
    .param p1, "cryptoData"    # Ljava/lang/String;
    .param p2, "inKey"    # Ljava/security/Key;
    .param p3, "InitVector"    # Ljavax/crypto/spec/IvParameterSpec;

    .prologue
    .line 385
    move-object v1, p1

    .line 387
    .local v1, "mResult":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v4, v5}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v3

    .line 388
    .local v3, "receivedData":[B
    invoke-direct {p0, p2, p3, v3}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->decrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B

    move-result-object v2

    .line 389
    .local v2, "msgArr":[B
    new-instance v1, Ljava/lang/String;

    .end local v1    # "mResult":Ljava/lang/String;
    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 394
    .end local v2    # "msgArr":[B
    .end local v3    # "receivedData":[B
    .restart local v1    # "mResult":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 390
    .end local v1    # "mResult":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 392
    .restart local v1    # "mResult":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getEncryptedAESSessionKey(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;)Ljava/lang/String;
    .locals 10
    .param p1, "inKey"    # Ljava/security/Key;
    .param p2, "InitialVector"    # Ljavax/crypto/spec/IvParameterSpec;
    .param p3, "mPUBSerialNumber"    # Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    .prologue
    .line 344
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->getELMCertificateUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/ELMCertificateUtil;

    move-result-object v2

    .line 345
    .local v2, "eLMCertificateUtil":Lcom/sec/esdk/elm/utils/ELMCertificateUtil;
    const/4 v4, 0x0

    .line 347
    .local v4, "mResult":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v2, p3}, Lcom/sec/esdk/elm/utils/ELMCertificateUtil;->getElmKeyDecryptPubllicKey(Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;)Ljava/security/PublicKey;

    move-result-object v6

    .line 348
    .local v6, "publicKey":Ljava/security/PublicKey;
    if-nez v6, :cond_0

    .line 349
    const-string v8, "getEncryptedAESSessionKey : publicKey is null"

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 350
    const/4 v8, 0x0

    .line 369
    .end local v6    # "publicKey":Ljava/security/PublicKey;
    :goto_0
    return-object v8

    .line 353
    .restart local v6    # "publicKey":Ljava/security/PublicKey;
    :cond_0
    const-string v8, "RSA/ECB/PKCS1Padding"

    invoke-static {v8}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v7

    .line 354
    .local v7, "rsaCipher":Ljavax/crypto/Cipher;
    const/4 v8, 0x1

    invoke-virtual {v7, v8, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 355
    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v8

    array-length v8, v8

    invoke-virtual {p2}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v9

    array-length v9, v9

    add-int/2addr v8, v9

    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 356
    .local v5, "plainText":Ljava/nio/ByteBuffer;
    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 357
    invoke-virtual {p2}, Ljavax/crypto/spec/IvParameterSpec;->getIV()[B

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 359
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    invoke-virtual {v7, v8}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v3

    .line 361
    .local v3, "encryptedPassword":[B
    array-length v8, v3

    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 362
    .local v0, "bf":Ljava/nio/ByteBuffer;
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 363
    new-instance v4, Ljava/lang/String;

    .end local v4    # "mResult":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v8

    const/4 v9, 0x2

    invoke-static {v8, v9}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "bf":Ljava/nio/ByteBuffer;
    .end local v3    # "encryptedPassword":[B
    .end local v5    # "plainText":Ljava/nio/ByteBuffer;
    .end local v6    # "publicKey":Ljava/security/PublicKey;
    .end local v7    # "rsaCipher":Ljavax/crypto/Cipher;
    .restart local v4    # "mResult":Ljava/lang/String;
    :goto_1
    move-object v8, v4

    .line 369
    goto :goto_0

    .line 365
    .end local v4    # "mResult":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 366
    .local v1, "e":Ljava/lang/Exception;
    const/4 v4, 0x0

    .line 367
    .restart local v4    # "mResult":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getEncryptedNetworkData(Ljava/lang/String;Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;)Ljava/lang/String;
    .locals 4
    .param p1, "plainData"    # Ljava/lang/String;
    .param p2, "inKey"    # Ljava/security/Key;
    .param p3, "InitVector"    # Ljavax/crypto/spec/IvParameterSpec;

    .prologue
    .line 373
    move-object v1, p1

    .line 375
    .local v1, "mResult":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-direct {p0, p2, p3, v3}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->encrypt256(Ljava/security/Key;Ljavax/crypto/spec/IvParameterSpec;[B)[B

    move-result-object v2

    .line 376
    .local v2, "msgArr":[B
    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 381
    .end local v2    # "msgArr":[B
    :goto_0
    return-object v1

    .line 377
    :catch_0
    move-exception v0

    .line 378
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 379
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getInitialVector()Ljavax/crypto/spec/IvParameterSpec;
    .locals 3

    .prologue
    .line 398
    const/4 v0, 0x0

    .line 399
    .local v0, "mResult":Ljavax/crypto/spec/IvParameterSpec;
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    .line 400
    .local v1, "mSecureRandom":Ljava/security/SecureRandom;
    new-instance v0, Ljavax/crypto/spec/IvParameterSpec;

    .end local v0    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    const/16 v2, 0x10

    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->generateSeed(I)[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 401
    .restart local v0    # "mResult":Ljavax/crypto/spec/IvParameterSpec;
    return-object v0
.end method

.method public getSignatureData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;
    .locals 26
    .param p1, "origData"    # Ljava/lang/String;
    .param p2, "serviceName"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "optTimestamp"    # J

    .prologue
    .line 58
    const/4 v7, 0x0

    .line 60
    .local v7, "deviceId":Ljava/lang/String;
    const/16 v21, 0x0

    .line 61
    .local v21, "enc1Data":Ljava/lang/String;
    const/16 v23, 0x0

    .line 63
    .local v23, "signatureData":Ljava/lang/String;
    const/4 v6, -0x1

    .line 65
    .local v6, "dataFormatID":B
    const/16 v19, -0x1

    .line 67
    .local v19, "ciphersuite":B
    const-wide/16 v10, -0x1

    .line 69
    .local v10, "timestamp":J
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 70
    :cond_0
    const/4 v5, 0x0

    .line 136
    :goto_0
    return-object v5

    .line 73
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getDataFormatID(Ljava/lang/String;)B

    move-result v6

    .line 74
    const/4 v5, -0x1

    if-ne v6, v5, :cond_2

    .line 75
    const-string v5, "getSignatureData : dataFormatId is null"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 76
    const/4 v5, 0x0

    goto :goto_0

    .line 79
    :cond_2
    const-string v5, "ELM"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 80
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getDeviceID(B)Ljava/lang/String;

    move-result-object v7

    .line 85
    :cond_3
    :goto_1
    if-nez v7, :cond_5

    .line 86
    const-string v5, "getSignatureData : deviceId is null"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 87
    const/4 v5, 0x0

    goto :goto_0

    .line 81
    :cond_4
    const-string v5, "GSLB"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 82
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v0, v1, v2}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getDeviceID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 90
    :cond_5
    const-wide/16 v8, 0x0

    cmp-long v5, p4, v8

    if-nez v5, :cond_6

    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 96
    :goto_2
    if-eqz p3, :cond_7

    move-object/from16 v5, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    .line 97
    invoke-direct/range {v5 .. v11}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getF2(BLjava/lang/String;Ljava/lang/String;Ljava/lang/String;J)[B

    move-result-object v4

    .line 102
    .local v4, "F2Value":[B
    :goto_3
    if-nez v4, :cond_8

    .line 103
    const-string v5, "getSignatureData : F2Value is null"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 104
    const/4 v5, 0x0

    goto :goto_0

    .line 93
    .end local v4    # "F2Value":[B
    :cond_6
    move-wide/from16 v10, p4

    goto :goto_2

    :cond_7
    move-object/from16 v12, p0

    move v13, v6

    move-object v14, v7

    move-object/from16 v15, p2

    move-wide/from16 v16, v10

    .line 99
    invoke-direct/range {v12 .. v17}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getF2(BLjava/lang/String;Ljava/lang/String;J)[B

    move-result-object v4

    .restart local v4    # "F2Value":[B
    goto :goto_3

    .line 107
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v7}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getCiphersuite(Ljava/lang/String;Ljava/lang/String;)B

    move-result v19

    .line 108
    if-ltz v19, :cond_9

    const/4 v5, 0x4

    move/from16 v0, v19

    if-lt v0, v5, :cond_a

    .line 109
    :cond_9
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSignatureData : ciphersuite("

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ") is invalid."

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 110
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 113
    :cond_a
    sget-object v5, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->cipherSuites:Ljava/util/Map;

    invoke-static/range {v19 .. v19}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    .line 118
    .local v24, "strCiphersuite":Ljava/lang/String;
    :try_start_0
    const-string v5, "SHA-256"

    invoke-static {v5}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v22

    .line 119
    .local v22, "md":Ljava/security/MessageDigest;
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v5}, Ljava/security/MessageDigest;->update([B)V

    .line 120
    invoke-virtual/range {v22 .. v22}, Ljava/security/MessageDigest;->digest()[B
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v18

    .line 127
    .local v18, "MD5Data":[B
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getEnc1Data([BLjava/lang/String;[B)Ljava/lang/String;

    move-result-object v21

    .line 129
    if-nez v21, :cond_b

    .line 130
    const-string v5, "getSignatureData : enc1Data is null"

    invoke-static {v5}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 131
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 122
    .end local v18    # "MD5Data":[B
    .end local v22    # "md":Ljava/security/MessageDigest;
    :catch_0
    move-exception v20

    .line 123
    .local v20, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual/range {v20 .. v20}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 124
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 134
    .end local v20    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v18    # "MD5Data":[B
    .restart local v22    # "md":Ljava/security/MessageDigest;
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v1, v6, v10, v11}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->compriseSignatureData(Ljava/lang/String;BJ)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v5, v23

    .line 136
    goto/16 :goto_0
.end method
