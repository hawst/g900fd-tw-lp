.class public Lcom/sec/esdk/elm/state/ValidateState;
.super Ljava/lang/Object;
.source "ValidateState.java"


# direct methods
.method public static processValidate(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V
    .locals 3
    .param p0, "state"    # Lcom/sec/esdk/elm/type/States;
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 20
    :try_start_0
    sget-object v1, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    if-ne p0, v1, :cond_0

    .line 21
    const-string v1, "ValidateState : processValidate() State is VALIDATE_REQUEST. processValidateRequest() Call"

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 22
    new-instance v2, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;

    invoke-direct {v2}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/esdk/elm/type/ValidateContainer;

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/state/module/ValidateRequestModule;->processValidateRequest(Lcom/sec/esdk/elm/type/ValidateContainer;)V

    .line 25
    :cond_0
    sget-object v1, Lcom/sec/esdk/elm/type/States;->VALIDATE_RESPONSE:Lcom/sec/esdk/elm/type/States;

    if-ne p0, v1, :cond_1

    .line 26
    const-string v1, "ValidateState : processValidate() State is VALIDATE_RESPONSE. processValidateResponse() Call"

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 27
    new-instance v2, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;

    invoke-direct {v2}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;-><init>()V

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/sec/esdk/elm/type/ValidateContainer;

    invoke-virtual {v2, v1}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->processValidateResponse(Lcom/sec/esdk/elm/type/ValidateContainer;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    :cond_1
    :goto_0
    return-void

    .line 30
    :catch_0
    move-exception v0

    .line 31
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v1, "ValidateState : processValidate() has NullPointException."

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 32
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method
