.class public Lcom/sec/esdk/elm/state/module/UploadResponseModule;
.super Lcom/sec/esdk/elm/state/module/ModuleBase;
.source "UploadResponseModule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/esdk/elm/state/module/UploadResponseModule$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;-><init>()V

    .line 51
    return-void
.end method


# virtual methods
.method public failProcessing(Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)V
    .locals 6
    .param p1, "container"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p2, "in_freq"    # Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    .prologue
    const/4 v5, 0x2

    .line 72
    const-string v2, "UploadResponseModule.failProcessing()"

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    move-object v1, p1

    .line 74
    check-cast v1, Lcom/sec/esdk/elm/type/UploadContainer;

    .line 75
    .local v1, "uploadContainer":Lcom/sec/esdk/elm/type/UploadContainer;
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->getError()Landroid/app/enterprise/license/Error;

    move-result-object v0

    .line 77
    .local v0, "error":Landroid/app/enterprise/license/Error;
    if-nez v0, :cond_0

    .line 78
    const-string v2, "UploadResponseModule.failProcessing(). Error is null. change to NullInputParam."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 79
    new-instance v0, Landroid/app/enterprise/license/Error;

    .end local v0    # "error":Landroid/app/enterprise/license/Error;
    sget-object v2, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v3

    sget-object v4, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorDesc()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 83
    .restart local v0    # "error":Landroid/app/enterprise/license/Error;
    :cond_0
    invoke-virtual {v0}, Landroid/app/enterprise/license/Error;->getHttpResponseCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 120
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Fail to Upload Response. CODE : ???"

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    .line 123
    :cond_1
    :goto_0
    return-void

    .line 85
    :sswitch_0
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Fail to Upload Response. CODE : 200"

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :sswitch_1
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Fail to Upload Response. CODE : 300"

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :sswitch_2
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Fail to Upload Response. CODE : 301"

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 97
    :sswitch_3
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Fail to Upload Response. CODE : 400"

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :sswitch_4
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/UploadContainer;->getRetryCount()I

    move-result v2

    if-ge v2, v5, :cond_1

    .line 102
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/UploadContainer;->increseRetryCount()V

    .line 103
    sget-object v2, Lcom/sec/esdk/elm/type/States;->UPLOAD_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v2

    invoke-static {p2}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->getTime(Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)J

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->SendRetryIntent(Lcom/sec/esdk/elm/type/ELMContainer;IJ)V

    .line 104
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to Upload Response. CODE : 500 Retry "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/UploadContainer;->getRetryCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 110
    :sswitch_5
    invoke-virtual {v0}, Landroid/app/enterprise/license/Error;->getErrorCode()I

    move-result v2

    sget-object v3, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v3

    if-eq v2, v3, :cond_1

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/UploadContainer;->getRetryCount()I

    move-result v2

    if-ge v2, v5, :cond_1

    .line 112
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/UploadContainer;->increseRetryCount()V

    .line 113
    sget-object v2, Lcom/sec/esdk/elm/type/States;->UPLOAD_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v2

    invoke-static {p2}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->getTime(Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)J

    move-result-wide v4

    invoke-static {v1, v2, v4, v5}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->SendRetryIntent(Lcom/sec/esdk/elm/type/ELMContainer;IJ)V

    .line 114
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Fail to Upload Response. CODE : 1000 Retry "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/UploadContainer;->getRetryCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 83
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x12c -> :sswitch_1
        0x12d -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
        0x3e8 -> :sswitch_5
    .end sparse-switch
.end method

.method public processUploadResponse(Lcom/sec/esdk/elm/type/UploadContainer;)V
    .locals 6
    .param p1, "uploadContainer"    # Lcom/sec/esdk/elm/type/UploadContainer;

    .prologue
    .line 23
    const/4 v3, 0x0

    .line 24
    .local v3, "statusValue":Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    const/4 v1, 0x0

    .line 25
    .local v1, "instanceID":Ljava/lang/String;
    const/4 v2, 0x0

    .line 28
    .local v2, "status":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UploadResponseModule.processUploadResponse()., "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->getServerTime()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->getStatus()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->getError()Landroid/app/enterprise/license/Error;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->getStatus()Ljava/lang/String;

    move-result-object v2

    .line 33
    invoke-static {v2}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->getResponseStatusVaule(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    move-result-object v3

    .line 34
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    .line 36
    if-nez v1, :cond_0

    .line 37
    const-string v4, "UploadResponseModule.processUploadResponse(). Instance ID is Null. Status is changing to fail."

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 38
    const-string v4, "fail"

    invoke-static {v4}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->getResponseStatusVaule(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    move-result-object v3

    .line 41
    :cond_0
    sget-object v4, Lcom/sec/esdk/elm/state/module/UploadResponseModule$1;->$SwitchMap$com$sec$esdk$elm$datamanager$Constants$ResponseStatusVaule:[I

    invoke-virtual {v3}, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 63
    :goto_0
    return-void

    .line 43
    :pswitch_0
    const-string v4, "UploadResponseModule.processUploadResponse(). Success to Upload Process"

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 44
    const-string v4, "success"

    const/4 v5, 0x0

    invoke-static {v4, v1, v5}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)V

    .line 45
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->successProcessing(Lcom/sec/esdk/elm/type/ELMContainer;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UploadResponseModule.processUploadResponse()() has NullPointException."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 56
    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;->ONE_HOUR:Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    invoke-virtual {p0, p1, v4}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->failProcessing(Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 60
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v4

    throw v4

    .line 49
    :pswitch_1
    :try_start_2
    const-string v4, "UploadResponseModule.processUploadResponse(). Fail to Upload Response"

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 50
    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;->ONE_HOUR:Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    invoke-virtual {p0, p1, v4}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->failProcessing(Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)V
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 57
    :catch_1
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UploadResponseModule.processUploadResponse()() has Exception."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 59
    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;->ONE_HOUR:Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    invoke-virtual {p0, p1, v4}, Lcom/sec/esdk/elm/state/module/UploadResponseModule;->failProcessing(Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public successProcessing(Lcom/sec/esdk/elm/type/ELMContainer;)V
    .locals 1
    .param p1, "container"    # Lcom/sec/esdk/elm/type/ELMContainer;

    .prologue
    .line 67
    const-string v0, "UploadResponseModule.successProcessing()."

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 68
    return-void
.end method
