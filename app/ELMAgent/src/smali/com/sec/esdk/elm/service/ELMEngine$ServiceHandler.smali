.class Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;
.super Landroid/os/Handler;
.source "ELMEngine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/service/ELMEngine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ServiceHandler"
.end annotation


# instance fields
.field states:[Lcom/sec/esdk/elm/type/States;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 186
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 187
    invoke-static {}, Lcom/sec/esdk/elm/type/States;->values()[Lcom/sec/esdk/elm/type/States;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;->states:[Lcom/sec/esdk/elm/type/States;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 192
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine$1;->$SwitchMap$com$sec$esdk$elm$type$States:[I

    iget-object v1, p0, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;->states:[Lcom/sec/esdk/elm/type/States;

    iget v2, p1, Landroid/os/Message;->what:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 248
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( DEFAULT )."

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 252
    :goto_0
    return-void

    .line 194
    :pswitch_0
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( ALARM_MODIFY ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 195
    invoke-static {}, Lcom/sec/esdk/elm/state/module/ModuleBase;->ModifySetAlarm()I

    goto :goto_0

    .line 199
    :pswitch_1
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( BOOT_COMPLETED ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 200
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0}, Lcom/sec/esdk/elm/state/BootCompletedState;->processBootCompleted(I)V

    goto :goto_0

    .line 204
    :pswitch_2
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( REGISTER_REQUEST ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 205
    sget-object v0, Lcom/sec/esdk/elm/type/States;->REGISTER_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-static {v0, p1}, Lcom/sec/esdk/elm/state/RegisterState;->processRegister(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V

    goto :goto_0

    .line 209
    :pswitch_3
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( KLMS_REQUEST ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 210
    sget-object v0, Lcom/sec/esdk/elm/type/States;->KLMS_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-static {v0, p1}, Lcom/sec/esdk/elm/state/RegisterState;->processRegister(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V

    goto :goto_0

    .line 214
    :pswitch_4
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( KLMS_RESPONSE ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 215
    sget-object v0, Lcom/sec/esdk/elm/type/States;->KLMS_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-static {v0, p1}, Lcom/sec/esdk/elm/state/RegisterState;->processRegister(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V

    goto :goto_0

    .line 219
    :pswitch_5
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( REGISTER_RESPONSE ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 220
    sget-object v0, Lcom/sec/esdk/elm/type/States;->REGISTER_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-static {v0, p1}, Lcom/sec/esdk/elm/state/RegisterState;->processRegister(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V

    goto :goto_0

    .line 224
    :pswitch_6
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( VALIDATE_REQUEST ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 225
    sget-object v0, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-static {v0, p1}, Lcom/sec/esdk/elm/state/ValidateState;->processValidate(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V

    goto :goto_0

    .line 229
    :pswitch_7
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( VALIDATE_RESPONSE ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 230
    sget-object v0, Lcom/sec/esdk/elm/type/States;->VALIDATE_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-static {v0, p1}, Lcom/sec/esdk/elm/state/ValidateState;->processValidate(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V

    goto :goto_0

    .line 234
    :pswitch_8
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( UPLOAD_REQUEST ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 235
    sget-object v0, Lcom/sec/esdk/elm/type/States;->UPLOAD_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-static {v0, p1}, Lcom/sec/esdk/elm/state/UploadState;->processUpload(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V

    goto :goto_0

    .line 239
    :pswitch_9
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( UPLOAD_RESPONSE ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 240
    sget-object v0, Lcom/sec/esdk/elm/type/States;->UPLOAD_RESPONSE:Lcom/sec/esdk/elm/type/States;

    invoke-static {v0, p1}, Lcom/sec/esdk/elm/state/UploadState;->processUpload(Lcom/sec/esdk/elm/type/States;Landroid/os/Message;)V

    goto :goto_0

    .line 244
    :pswitch_a
    const-string v0, "ELMEngine.ServiceHandler.handleMessage( UNKNOWN_STATE ). "

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
