.class public final enum Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/datamanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PUBSerialNumber"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

.field public static final enum NORMAL_SN:Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

.field public static final enum NO_HOME_CALLING_SN:Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;


# instance fields
.field private alias:Ljava/lang/String;

.field private mPUBSerialNumber:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 165
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    const-string v1, "NORMAL_SN"

    const-string v2, "elmkeyv1"

    const-string v3, "elm-cloud"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->NORMAL_SN:Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    .line 166
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    const-string v1, "NO_HOME_CALLING_SN"

    const-string v2, "elm_onprem_29812"

    const-string v3, "elm-onprem"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->NO_HOME_CALLING_SN:Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    .line 164
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->NORMAL_SN:Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->NO_HOME_CALLING_SN:Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3, "SerialNumber"    # Ljava/lang/String;
    .param p4, "mAlias"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 171
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 168
    iput-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->mPUBSerialNumber:Ljava/lang/String;

    .line 169
    iput-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->alias:Ljava/lang/String;

    .line 172
    iput-object p3, p0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->mPUBSerialNumber:Ljava/lang/String;

    .line 173
    iput-object p4, p0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->alias:Ljava/lang/String;

    .line 174
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 164
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;

    return-object v0
.end method


# virtual methods
.method public getAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->alias:Ljava/lang/String;

    return-object v0
.end method

.method public getSerialNumebr()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;->mPUBSerialNumber:Ljava/lang/String;

    return-object v0
.end method
