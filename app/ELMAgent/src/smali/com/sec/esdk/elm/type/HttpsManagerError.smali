.class public final enum Lcom/sec/esdk/elm/type/HttpsManagerError;
.super Ljava/lang/Enum;
.source "HttpsManagerError.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/type/HttpsManagerError;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/type/HttpsManagerError;

.field public static final enum GeneralNetworkException:Lcom/sec/esdk/elm/type/HttpsManagerError;

.field public static final enum NetworkDisabled:Lcom/sec/esdk/elm/type/HttpsManagerError;

.field public static final enum NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

.field public static final enum RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

.field public static final enum Terminated:Lcom/sec/esdk/elm/type/HttpsManagerError;


# instance fields
.field private mErrorCode:I

.field private mErrorDesc:Ljava/lang/String;

.field private mHttpResponseCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    const/16 v9, 0x3e8

    .line 10
    new-instance v0, Lcom/sec/esdk/elm/type/HttpsManagerError;

    const-string v1, "RequestEmpty"

    const/16 v3, 0x190

    const/16 v4, 0xbb9

    const-string v5, "Request is Empty"

    invoke-direct/range {v0 .. v5}, Lcom/sec/esdk/elm/type/HttpsManagerError;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    .line 11
    new-instance v3, Lcom/sec/esdk/elm/type/HttpsManagerError;

    const-string v4, "Terminated"

    const/16 v6, 0x190

    const/16 v7, 0xc1b

    const-string v8, "terminated"

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/type/HttpsManagerError;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/type/HttpsManagerError;->Terminated:Lcom/sec/esdk/elm/type/HttpsManagerError;

    .line 13
    new-instance v3, Lcom/sec/esdk/elm/type/HttpsManagerError;

    const-string v4, "NetworkDisabled"

    const/16 v7, 0x2710

    const-string v8, "Network is Disabled"

    move v5, v11

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/type/HttpsManagerError;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/type/HttpsManagerError;->NetworkDisabled:Lcom/sec/esdk/elm/type/HttpsManagerError;

    .line 14
    new-instance v3, Lcom/sec/esdk/elm/type/HttpsManagerError;

    const-string v4, "GeneralNetworkException"

    const/16 v7, 0x2711

    const-string v8, "General Network Error was occurred"

    move v5, v12

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/type/HttpsManagerError;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/type/HttpsManagerError;->GeneralNetworkException:Lcom/sec/esdk/elm/type/HttpsManagerError;

    .line 15
    new-instance v3, Lcom/sec/esdk/elm/type/HttpsManagerError;

    const-string v4, "NullInputParam"

    const/4 v5, 0x4

    const/16 v7, 0x2712

    const-string v8, "Null Paramter was input"

    move v6, v9

    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/type/HttpsManagerError;-><init>(Ljava/lang/String;IIILjava/lang/String;)V

    sput-object v3, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    .line 8
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/sec/esdk/elm/type/HttpsManagerError;

    sget-object v1, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/esdk/elm/type/HttpsManagerError;->Terminated:Lcom/sec/esdk/elm/type/HttpsManagerError;

    aput-object v1, v0, v10

    sget-object v1, Lcom/sec/esdk/elm/type/HttpsManagerError;->NetworkDisabled:Lcom/sec/esdk/elm/type/HttpsManagerError;

    aput-object v1, v0, v11

    sget-object v1, Lcom/sec/esdk/elm/type/HttpsManagerError;->GeneralNetworkException:Lcom/sec/esdk/elm/type/HttpsManagerError;

    aput-object v1, v0, v12

    const/4 v1, 0x4

    sget-object v2, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/esdk/elm/type/HttpsManagerError;->$VALUES:[Lcom/sec/esdk/elm/type/HttpsManagerError;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIILjava/lang/String;)V
    .locals 1
    .param p3, "httpResponseCode"    # I
    .param p4, "errorCode"    # I
    .param p5, "errorDesc"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/HttpsManagerError;->mErrorDesc:Ljava/lang/String;

    .line 24
    iput p3, p0, Lcom/sec/esdk/elm/type/HttpsManagerError;->mHttpResponseCode:I

    .line 25
    iput p4, p0, Lcom/sec/esdk/elm/type/HttpsManagerError;->mErrorCode:I

    .line 26
    iput-object p5, p0, Lcom/sec/esdk/elm/type/HttpsManagerError;->mErrorDesc:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/type/HttpsManagerError;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/type/HttpsManagerError;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/type/HttpsManagerError;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/sec/esdk/elm/type/HttpsManagerError;->$VALUES:[Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/type/HttpsManagerError;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/type/HttpsManagerError;

    return-object v0
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/sec/esdk/elm/type/HttpsManagerError;->mErrorCode:I

    return v0
.end method

.method public getErrorDesc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/esdk/elm/type/HttpsManagerError;->mErrorDesc:Ljava/lang/String;

    return-object v0
.end method

.method public getHttpResponseCode()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/sec/esdk/elm/type/HttpsManagerError;->mHttpResponseCode:I

    return v0
.end method
