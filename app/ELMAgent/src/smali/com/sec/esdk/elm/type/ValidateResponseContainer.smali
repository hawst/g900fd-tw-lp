.class public Lcom/sec/esdk/elm/type/ValidateResponseContainer;
.super Ljava/lang/Object;
.source "ValidateResponseContainer.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private Error:Landroid/app/enterprise/license/Error;

.field private InstanceId:Ljava/lang/String;

.field private RO:Landroid/app/enterprise/license/RightsObject;

.field private ServerTime:Ljava/lang/String;

.field private Status:Ljava/lang/String;

.field private mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/sec/esdk/elm/type/ValidateResponseContainer$1;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/ValidateResponseContainer$1;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->InstanceId:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Status:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->ServerTime:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 25
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    .line 27
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 127
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 128
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->InstanceId:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Status:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->ServerTime:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 25
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    .line 27
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 65
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->readFromParcel(Landroid/os/Parcel;)V

    .line 66
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method public getError()Landroid/app/enterprise/license/Error;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    return-object v0
.end method

.method public getInstanceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->InstanceId:Ljava/lang/String;

    return-object v0
.end method

.method public getLicensePermissionGroup()Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    return-object v0
.end method

.method public getRO()Landroid/app/enterprise/license/RightsObject;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    return-object v0
.end method

.method public getServerTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->ServerTime:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Status:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->InstanceId:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Status:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->ServerTime:Ljava/lang/String;

    .line 43
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 44
    const-class v0, Landroid/app/enterprise/license/RightsObject;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/license/RightsObject;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    .line 46
    const-class v0, Landroid/app/enterprise/license/Error;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/license/Error;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 47
    return-void
.end method

.method public setError(Landroid/app/enterprise/license/Error;)V
    .locals 0
    .param p1, "error"    # Landroid/app/enterprise/license/Error;

    .prologue
    .line 106
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 107
    return-void
.end method

.method public setInstanceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->InstanceId:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public setLicensePermissionGroup(Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;)V
    .locals 0
    .param p1, "mLicensePermissionGroup"    # Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    .line 115
    return-void
.end method

.method public setRO(Landroid/app/enterprise/license/RightsObject;)V
    .locals 0
    .param p1, "rO"    # Landroid/app/enterprise/license/RightsObject;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    .line 99
    return-void
.end method

.method public setServerTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverTime"    # Ljava/lang/String;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->ServerTime:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Status:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->InstanceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Status:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->ServerTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->mPermissionGroup:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 35
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->RO:Landroid/app/enterprise/license/RightsObject;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 37
    return-void
.end method
