.class public Lcom/sec/esdk/elm/utils/JSON;
.super Ljava/lang/Object;
.source "JSON.java"


# direct methods
.method public static compriseRedirectionRequestJSON(Lcom/sec/esdk/elm/type/RedirectionRequestInfo;)Lorg/json/JSONObject;
    .locals 10
    .param p0, "redirectionRequestInfo"    # Lcom/sec/esdk/elm/type/RedirectionRequestInfo;

    .prologue
    .line 292
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 294
    .local v8, "redirectionRequestJSON":Lorg/json/JSONObject;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 295
    .local v6, "dataJSON":Lorg/json/JSONObject;
    const/4 v9, 0x0

    .line 299
    .local v9, "signedData":Ljava/lang/String;
    :try_start_0
    const-string v1, "deviceid"

    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->getDeviceID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 301
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->getCountryISO()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 302
    const-string v1, "country_iso"

    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->getCountryISO()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 305
    :cond_0
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->getSalesCode()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 306
    const-string v1, "csc"

    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->getSalesCode()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 309
    :cond_1
    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->getMCC()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 310
    const-string v1, "mcc"

    invoke-virtual {p0}, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 313
    :cond_2
    new-instance v0, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-direct {v0}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;-><init>()V

    .line 314
    .local v0, "secureDataGenerator":Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;
    invoke-virtual {v8}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "GSLB"

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getSignatureData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v9

    .line 316
    const-string v1, "data"

    invoke-virtual {v6, v1, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 317
    const-string v1, "signature"

    invoke-virtual {v6, v1, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 333
    .end local v0    # "secureDataGenerator":Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;
    :goto_0
    return-object v6

    .line 319
    :catch_0
    move-exception v7

    .line 320
    .local v7, "e":Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseRedirectionRequestJSON : JSONException was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 321
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    .line 323
    const/4 v8, 0x0

    .line 324
    const/4 v6, 0x0

    .line 331
    goto :goto_0

    .line 325
    .end local v7    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v7

    .line 326
    .local v7, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseRedirectionRequestJSON : Exception was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 327
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 329
    const/4 v8, 0x0

    .line 330
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static compriseRegisterRequestJSON(Ljava/lang/String;Lcom/sec/esdk/elm/type/DeviceInfo;Lcom/sec/esdk/elm/type/PackageInfo;)Lorg/json/JSONObject;
    .locals 12
    .param p0, "licenseKey"    # Ljava/lang/String;
    .param p1, "deviceInfo"    # Lcom/sec/esdk/elm/type/DeviceInfo;
    .param p2, "packageInfo"    # Lcom/sec/esdk/elm/type/PackageInfo;

    .prologue
    .line 32
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 33
    .local v10, "registerRequestJSON":Lorg/json/JSONObject;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 34
    .local v6, "DataJSON":Lorg/json/JSONObject;
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7}, Lorg/json/JSONObject;-><init>()V

    .line 35
    .local v7, "deviceInfoJSON":Lorg/json/JSONObject;
    new-instance v9, Lorg/json/JSONObject;

    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 36
    .local v9, "packageInfoJSON":Lorg/json/JSONObject;
    const/4 v11, 0x0

    .line 39
    .local v11, "signedData":Ljava/lang/String;
    :try_start_0
    const-string v1, "client_timezone"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getCilentTimeZone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 41
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getSimCarrier2()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 42
    const-string v1, "sim_carrier_2"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getSimCarrier2()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 45
    :cond_0
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getSimCarrier1()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 46
    const-string v1, "sim_carrier_1"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getSimCarrier1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 49
    :cond_1
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getDeviceCarrier()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 50
    const-string v1, "device_carrier"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getDeviceCarrier()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 53
    :cond_2
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getEsdkVer()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 54
    const-string v1, "esdk_version"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getEsdkVer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 57
    :cond_3
    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getMCC()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 58
    const-string v1, "mcc"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getMCC()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 61
    :cond_4
    const-string v1, "build_number"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getBuildNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 62
    const-string v1, "android_version"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getAndroidVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 63
    const-string v1, "model"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getModel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 64
    const-string v1, "imei"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/DeviceInfo;->getIMEI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 66
    const-string v1, "package_version"

    invoke-virtual {p2}, Lcom/sec/esdk/elm/type/PackageInfo;->getPackageVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 67
    const-string v1, "package_name"

    invoke-virtual {p2}, Lcom/sec/esdk/elm/type/PackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 68
    const-string v1, "apk_hash"

    invoke-virtual {p2}, Lcom/sec/esdk/elm/type/PackageInfo;->getApkHash()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 70
    const-string v1, "package_info"

    invoke-virtual {v6, v1, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 71
    const-string v1, "device_info"

    invoke-virtual {v6, v1, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 72
    const-string v1, "license_key"

    invoke-virtual {v6, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 73
    const-string v1, "RequestID"

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 75
    const-string v1, "data"

    invoke-virtual {v10, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 77
    new-instance v0, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-direct {v0}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;-><init>()V

    .line 78
    .local v0, "secureDataGenerator":Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ELM"

    invoke-virtual {p2}, Lcom/sec/esdk/elm/type/PackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getSignatureData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v11

    .line 81
    const-string v1, "signature"

    invoke-virtual {v10, v1, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 99
    .end local v0    # "secureDataGenerator":Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;
    :goto_0
    return-object v10

    .line 83
    :catch_0
    move-exception v8

    .line 84
    .local v8, "e":Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseRegisterRequestJSON : JSONException was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v8}, Lorg/json/JSONException;->printStackTrace()V

    .line 87
    const/4 v9, 0x0

    .line 88
    const/4 v7, 0x0

    .line 89
    const/4 v10, 0x0

    .line 97
    goto :goto_0

    .line 90
    .end local v8    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v8

    .line 91
    .local v8, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseRegisterRequestJSON : Exception was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 92
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 94
    const/4 v9, 0x0

    .line 95
    const/4 v7, 0x0

    .line 96
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public static compriseUploadRequestJSON(Ljava/lang/String;Lcom/sec/esdk/elm/type/PackageInfo;Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 11
    .param p0, "instanceId"    # Ljava/lang/String;
    .param p1, "packageInfo"    # Lcom/sec/esdk/elm/type/PackageInfo;
    .param p2, "logData"    # Lorg/json/JSONObject;

    .prologue
    .line 152
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 153
    .local v10, "uploadRequestJSON":Lorg/json/JSONObject;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 154
    .local v6, "DataJSON":Lorg/json/JSONObject;
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 156
    .local v8, "packageInfoJSON":Lorg/json/JSONObject;
    const/4 v9, 0x0

    .line 159
    .local v9, "signedData":Ljava/lang/String;
    :try_start_0
    const-string v1, "package_version"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/PackageInfo;->getPackageVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 160
    const-string v1, "package_name"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/PackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 161
    const-string v1, "apk_hash"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/PackageInfo;->getApkHash()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 163
    if-eqz p2, :cond_0

    .line 164
    const-string v1, "instance_id"

    invoke-virtual {v6, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 165
    const-string v1, "package_info"

    invoke-virtual {v6, v1, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 166
    const-string v1, "log_data"

    invoke-virtual {v6, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 168
    const-string v1, "data"

    invoke-virtual {v10, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 170
    new-instance v0, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-direct {v0}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;-><init>()V

    .line 171
    .local v0, "secureDataGenerator":Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ELM"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/PackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getSignatureData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v9

    .line 174
    const-string v1, "signature"

    invoke-virtual {v10, v1, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .end local v0    # "secureDataGenerator":Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;
    :goto_0
    move-object v1, v10

    .line 196
    :goto_1
    return-object v1

    .line 177
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 179
    :catch_0
    move-exception v7

    .line 180
    .local v7, "e":Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseUploadRequestJSON : JSONException was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 181
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    .line 183
    const/4 v10, 0x0

    .line 184
    const/4 v8, 0x0

    .line 185
    const/4 v6, 0x0

    .line 194
    goto :goto_0

    .line 187
    .end local v7    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v7

    .line 188
    .local v7, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseUploadRequestJSON : Exception was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 189
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 191
    const/4 v10, 0x0

    .line 192
    const/4 v8, 0x0

    .line 193
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static compriseValidateRequestJSON(Ljava/lang/String;Lcom/sec/esdk/elm/type/PackageInfo;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 11
    .param p0, "instanceId"    # Ljava/lang/String;
    .param p1, "packageInfo"    # Lcom/sec/esdk/elm/type/PackageInfo;
    .param p2, "timezone"    # Ljava/lang/String;
    .param p3, "mPushPoint"    # Ljava/lang/String;

    .prologue
    .line 105
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    .line 106
    .local v10, "validateRequestJSON":Lorg/json/JSONObject;
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6}, Lorg/json/JSONObject;-><init>()V

    .line 107
    .local v6, "DataJSON":Lorg/json/JSONObject;
    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 109
    .local v8, "packageInfoJSON":Lorg/json/JSONObject;
    const/4 v9, 0x0

    .line 113
    .local v9, "signedData":Ljava/lang/String;
    :try_start_0
    const-string v1, "package_version"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/PackageInfo;->getPackageVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 114
    const-string v1, "package_name"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/PackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 115
    const-string v1, "apk_hash"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/PackageInfo;->getApkHash()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 117
    const-string v1, "instance_id"

    invoke-virtual {v6, v1, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 118
    const-string v1, "package_info"

    invoke-virtual {v6, v1, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 119
    const-string v1, "client_timezone"

    invoke-virtual {v6, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 120
    const-string v1, "klm_active"

    invoke-virtual {v6, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 121
    const-string v1, "RequestID"

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 123
    const-string v1, "data"

    invoke-virtual {v10, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 125
    new-instance v0, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;

    invoke-direct {v0}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;-><init>()V

    .line 126
    .local v0, "secureDataGenerator":Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;
    invoke-virtual {v6}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ELM"

    invoke-virtual {p1}, Lcom/sec/esdk/elm/type/PackageInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;->getSignatureData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v9

    .line 129
    const-string v1, "signature"

    invoke-virtual {v10, v1, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 147
    .end local v0    # "secureDataGenerator":Lcom/sec/esdk/elm/SecurityManager/SecureDataGenerator;
    :goto_0
    return-object v10

    .line 131
    :catch_0
    move-exception v7

    .line 132
    .local v7, "e":Lorg/json/JSONException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseValidateRequestJSON : JSONException was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 133
    invoke-virtual {v7}, Lorg/json/JSONException;->printStackTrace()V

    .line 135
    const/4 v10, 0x0

    .line 136
    const/4 v8, 0x0

    .line 137
    const/4 v6, 0x0

    .line 145
    goto :goto_0

    .line 138
    .end local v7    # "e":Lorg/json/JSONException;
    :catch_1
    move-exception v7

    .line 139
    .local v7, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "compriseValidateRequestJSON : Exception was occurred : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 140
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 142
    const/4 v10, 0x0

    .line 143
    const/4 v8, 0x0

    .line 144
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static declared-synchronized getAPKHashString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 201
    const-class v9, Lcom/sec/esdk/elm/utils/JSON;

    monitor-enter v9

    const/4 v4, 0x0

    .line 202
    .local v4, "hash":Ljava/lang/String;
    const/4 v0, 0x0

    .line 205
    .local v0, "apkPath":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const/16 v10, 0x2000

    invoke-virtual {v8, v10}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v7

    .line 206
    .local v7, "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    if-nez v7, :cond_0

    .line 207
    const-string v8, "getAPKHashString(). mAppInfoList is Null"

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    const/4 v8, 0x0

    .line 235
    .end local v7    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :goto_0
    monitor-exit v9

    return-object v8

    .line 211
    .restart local v7    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_0
    :try_start_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getAPKHashString(). mAppInfoList:"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 212
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/PackageInfo;

    .line 213
    .local v1, "appInfo":Landroid/content/pm/PackageInfo;
    iget-object v8, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 214
    iget-object v8, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v8, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 215
    const-string v8, "MD5"

    invoke-static {v0, v8}, Lcom/sec/esdk/elm/utils/JSON;->getApkHash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 220
    .end local v1    # "appInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    if-nez v4, :cond_3

    .line 222
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    const-string v10, "com.sec.enterprise.knox.cloudmdm.smdms"

    const/4 v11, 0x0

    invoke-virtual {v8, v10, v11}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v6

    .line 223
    .local v6, "info":Landroid/content/pm/PackageInfo;
    iget-object v8, v6, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v8, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 224
    const-string v8, "MD5"

    invoke-static {v0, v8}, Lcom/sec/esdk/elm/utils/JSON;->getApkHash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 234
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "info":Landroid/content/pm/PackageInfo;
    .end local v7    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_3
    :goto_1
    :try_start_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "JSON : getAPKHashString() PackageName : "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, " HASH : "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-object v8, v4

    .line 235
    goto :goto_0

    .line 225
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v7    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :catch_0
    move-exception v3

    .line 226
    .local v3, "e2":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_4
    const-string v8, "JSON : getAPKHashString() NameNotFoundException Exception"

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 227
    const/4 v4, 0x0

    goto :goto_1

    .line 230
    .end local v3    # "e2":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v7    # "mPackageInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :catch_1
    move-exception v2

    .line 231
    .local v2, "e":Ljava/lang/Exception;
    :try_start_5
    const-string v8, "getAPKHashString() has Exception."

    invoke-static {v8}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 201
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    monitor-exit v9

    throw v8
.end method

.method private static getApkHash(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "algorithm"    # Ljava/lang/String;

    .prologue
    .line 257
    const/4 v2, 0x0

    .line 258
    .local v2, "fis":Ljava/io/FileInputStream;
    const/4 v5, 0x0

    .line 261
    .local v5, "result":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .local v3, "fis":Ljava/io/FileInputStream;
    const/16 v6, 0x2800

    :try_start_1
    new-array v0, v6, [B

    .line 263
    .local v0, "buffer":[B
    invoke-static {p1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4

    .line 264
    .local v4, "md":Ljava/security/MessageDigest;
    invoke-virtual {v4}, Ljava/security/MessageDigest;->reset()V

    .line 266
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_1

    .line 267
    invoke-virtual {v4, v0}, Ljava/security/MessageDigest;->update([B)V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 271
    .end local v0    # "buffer":[B
    .end local v4    # "md":Ljava/security/MessageDigest;
    :catch_0
    move-exception v1

    move-object v2, v3

    .line 272
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    :goto_1
    :try_start_2
    const-string v6, "error - NoSuchAlgorithmException"

    invoke-static {v6}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 273
    const/4 v5, 0x0

    .line 278
    if-eqz v2, :cond_0

    .line 280
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 287
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :cond_0
    :goto_2
    return-object v5

    .line 270
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "buffer":[B
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "md":Ljava/security/MessageDigest;
    :cond_1
    :try_start_4
    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    const/4 v7, 0x2

    invoke-static {v6, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_4
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v5

    .line 278
    if-eqz v3, :cond_3

    .line 280
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    move-object v2, v3

    .line 283
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 281
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_1
    move-exception v1

    .line 282
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v2, v3

    .line 283
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_2

    .line 281
    .end local v0    # "buffer":[B
    .end local v4    # "md":Ljava/security/MessageDigest;
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    :catch_2
    move-exception v1

    .line 282
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 274
    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v1

    .line 275
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_6
    const-string v6, "getApkHash() failed"

    invoke-static {v6}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 276
    const/4 v5, 0x0

    .line 278
    if-eqz v2, :cond_0

    .line 280
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_2

    .line 281
    :catch_4
    move-exception v1

    .line 282
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 278
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v2, :cond_2

    .line 280
    :try_start_8
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 283
    :cond_2
    :goto_5
    throw v6

    .line 281
    :catch_5
    move-exception v1

    .line 282
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 278
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_4

    .line 274
    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :catch_6
    move-exception v1

    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_3

    .line 271
    :catch_7
    move-exception v1

    goto :goto_1

    .end local v2    # "fis":Ljava/io/FileInputStream;
    .restart local v0    # "buffer":[B
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v4    # "md":Ljava/security/MessageDigest;
    :cond_3
    move-object v2, v3

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "fis":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public static getHash(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 239
    const/4 v2, 0x0

    .line 241
    .local v2, "result":Ljava/lang/String;
    :try_start_0
    const-string v3, "SHA-256"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 242
    .local v1, "md":Ljava/security/MessageDigest;
    invoke-virtual {v1}, Ljava/security/MessageDigest;->reset()V

    .line 243
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 244
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 253
    .end local v1    # "md":Ljava/security/MessageDigest;
    :goto_0
    return-object v2

    .line 245
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    const-string v3, "error - NoSuchAlgorithmException"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 247
    const/4 v2, 0x0

    .line 251
    goto :goto_0

    .line 248
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 249
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "getApkHash() failed"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 250
    const/4 v2, 0x0

    goto :goto_0
.end method
