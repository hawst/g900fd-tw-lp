.class public Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;
.super Ljava/lang/Object;
.source "ELMContextManager.java"


# static fields
.field private static elmContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->elmContext:Landroid/content/Context;

    return-void
.end method

.method public static getELMContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->elmContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 17
    invoke-static {}, Lcom/sec/esdk/elm/service/ElmAgentService;->getContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->elmContext:Landroid/content/Context;

    .line 19
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->elmContext:Landroid/content/Context;

    return-object v0
.end method

.method public static setELMContext(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    sput-object p0, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->elmContext:Landroid/content/Context;

    .line 24
    return-void
.end method
