.class public Lcom/sec/esdk/elm/networkmanager/NetworkManager;
.super Ljava/lang/Object;
.source "NetworkManager.java"


# direct methods
.method public static callRestAPI(Ljava/lang/String;Lorg/json/JSONObject;Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/type/States;)V
    .locals 1
    .param p0, "serverURI"    # Ljava/lang/String;
    .param p1, "jsonData"    # Lorg/json/JSONObject;
    .param p2, "elmContainer"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p3, "states"    # Lcom/sec/esdk/elm/type/States;

    .prologue
    .line 21
    new-instance v0, Lcom/sec/esdk/elm/networkmanager/HttpsManager;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;-><init>(Ljava/lang/String;Lorg/json/JSONObject;Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/type/States;)V

    invoke-virtual {v0}, Lcom/sec/esdk/elm/networkmanager/HttpsManager;->start()V

    .line 22
    return-void
.end method

.method public static isPSAvailable()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 62
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    .line 63
    .local v3, "m_NetConnectMgr":Landroid/net/ConnectivityManager;
    const/4 v0, 0x0

    .line 65
    .local v0, "bConnect":Z
    if-nez v3, :cond_0

    .line 73
    :goto_0
    return v4

    .line 67
    :cond_0
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v3, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 69
    .local v2, "info":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v4, v0

    .line 73
    goto :goto_0

    .line 70
    .end local v2    # "info":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v1

    .line 71
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static isPossibleNetwork()Z
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/sec/esdk/elm/networkmanager/NetworkManager;->isPSAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/esdk/elm/networkmanager/NetworkManager;->isWifiAvailable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 26
    :cond_0
    const/4 v0, 0x1

    .line 28
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWifiAvailable()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 39
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "connectivity"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    .line 40
    .local v3, "m_NetConnectMgr":Landroid/net/ConnectivityManager;
    const/4 v0, 0x0

    .line 42
    .local v0, "bConnect":Z
    if-nez v3, :cond_0

    .line 52
    :goto_0
    return v4

    .line 45
    :cond_0
    const/4 v5, 0x1

    :try_start_0
    invoke-virtual {v3, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    .line 47
    .local v2, "info":Landroid/net/NetworkInfo;
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v4, v0

    .line 52
    goto :goto_0

    .line 49
    .end local v2    # "info":Landroid/net/NetworkInfo;
    :catch_0
    move-exception v1

    .line 50
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method
