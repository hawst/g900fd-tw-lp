.class public Lcom/sec/esdk/elm/service/ELMEngine;
.super Ljava/lang/Object;
.source "ELMEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/esdk/elm/service/ELMEngine$1;,
        Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;
    }
.end annotation


# static fields
.field private static mContext:Landroid/content/Context;

.field private static mELMEngine:Lcom/sec/esdk/elm/service/ELMEngine;

.field private static mELMKeyMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mJobHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    sput-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mContext:Landroid/content/Context;

    .line 36
    sput-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMEngine:Lcom/sec/esdk/elm/service/ELMEngine;

    .line 40
    new-instance v0, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;

    invoke-direct {v0}, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-string v0, "ELMEngine.ELMEngine( context )."

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 56
    sput-object p0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMEngine:Lcom/sec/esdk/elm/service/ELMEngine;

    .line 57
    sput-object p1, Lcom/sec/esdk/elm/service/ELMEngine;->mContext:Landroid/content/Context;

    .line 59
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/service/MDMBridge;->setEnterpriseBridge(Landroid/app/enterprise/license/EnterpriseLicenseManager;)V

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMKeyMap:Ljava/util/Map;

    .line 61
    return-void
.end method

.method public static getInstance()Lcom/sec/esdk/elm/service/ELMEngine;
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMEngine:Lcom/sec/esdk/elm/service/ELMEngine;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    .line 44
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 45
    new-instance v0, Lcom/sec/esdk/elm/service/ELMEngine;

    sget-object v1, Lcom/sec/esdk/elm/service/ELMEngine;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/esdk/elm/service/ELMEngine;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMEngine:Lcom/sec/esdk/elm/service/ELMEngine;

    .line 50
    :cond_1
    :goto_0
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMEngine:Lcom/sec/esdk/elm/service/ELMEngine;

    return-object v0

    .line 47
    :cond_2
    new-instance v0, Lcom/sec/esdk/elm/service/ELMEngine;

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/esdk/elm/service/ELMEngine;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMEngine:Lcom/sec/esdk/elm/service/ELMEngine;

    goto :goto_0
.end method


# virtual methods
.method public RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 95
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 96
    :cond_0
    const/4 v2, 0x0

    .line 105
    :goto_0
    return-object v2

    .line 98
    :cond_1
    const/4 v2, 0x0

    .line 100
    .local v2, "elmKey":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/sec/esdk/elm/service/ELMEngine;->mELMKeyMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 101
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "RemoveLicenseMap() failed"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public SendMessageHandler(Landroid/os/Message;)Z
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 170
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 171
    new-instance v0, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;

    invoke-direct {v0}, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    .line 173
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    move-result v0

    return v0
.end method

.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 65
    const-string v0, "ELMEngine.finalize()."

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 66
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMKeyMap:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMKeyMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 69
    :cond_0
    sput-object v1, Lcom/sec/esdk/elm/service/ELMEngine;->mContext:Landroid/content/Context;

    .line 70
    sput-object v1, Lcom/sec/esdk/elm/service/ELMEngine;->mELMEngine:Lcom/sec/esdk/elm/service/ELMEngine;

    .line 71
    sput-object v1, Lcom/sec/esdk/elm/service/ELMEngine;->mELMKeyMap:Ljava/util/Map;

    .line 72
    sput-object v1, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    .line 73
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 74
    return-void
.end method

.method public getLicenseMap(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 81
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 82
    :cond_0
    const/4 v2, 0x0

    .line 91
    :goto_0
    return-object v2

    .line 84
    :cond_1
    const/4 v2, 0x0

    .line 86
    .local v2, "elmKey":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/sec/esdk/elm/service/ELMEngine;->mELMKeyMap:Ljava/util/Map;

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "getELMLicenseKey() failed"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getObtainMessage()Landroid/os/Message;
    .locals 1

    .prologue
    .line 162
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;

    invoke-direct {v0}, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    .line 165
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method public getObtainMessage(I)Landroid/os/Message;
    .locals 1
    .param p1, "what"    # I

    .prologue
    .line 155
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 156
    new-instance v0, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;

    invoke-direct {v0}, Lcom/sec/esdk/elm/service/ELMEngine$ServiceHandler;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    .line 158
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mJobHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method public setLicenseMap(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "LicenseKey"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 77
    sget-object v0, Lcom/sec/esdk/elm/service/ELMEngine;->mELMKeyMap:Ljava/util/Map;

    invoke-interface {v0, p2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    return-void
.end method

.method public startValidation(Ljava/lang/String;Ljava/lang/String;Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;)V
    .locals 12
    .param p1, "sInstanceId"    # Ljava/lang/String;
    .param p2, "LicenseType"    # Ljava/lang/String;
    .param p3, "mPushPoint"    # Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;

    .prologue
    .line 110
    :try_start_0
    invoke-static {p1}, Lcom/sec/esdk/elm/service/MDMBridge;->getLicenseInfoFromSDK(Ljava/lang/String;)Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v8

    .line 111
    .local v8, "licenseInfo":Landroid/app/enterprise/license/LicenseInfo;
    new-instance v6, Ljava/util/GregorianCalendar;

    invoke-direct {v6}, Ljava/util/GregorianCalendar;-><init>()V

    .line 112
    .local v6, "calendar":Ljava/util/Calendar;
    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v10

    .line 114
    .local v10, "strClientTimezone":Ljava/lang/String;
    if-eqz v8, :cond_1

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getPackageVersion()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 116
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ELMEngine.startValidation().:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 123
    sget-object v0, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v0}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v0

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 151
    .end local v6    # "calendar":Ljava/util/Calendar;
    .end local v8    # "licenseInfo":Landroid/app/enterprise/license/LicenseInfo;
    .end local v10    # "strClientTimezone":Ljava/lang/String;
    :goto_0
    return-void

    .line 127
    .restart local v6    # "calendar":Ljava/util/Calendar;
    .restart local v8    # "licenseInfo":Landroid/app/enterprise/license/LicenseInfo;
    .restart local v10    # "strClientTimezone":Ljava/lang/String;
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/esdk/elm/service/ELMEngine;->getObtainMessage(I)Landroid/os/Message;

    move-result-object v9

    .line 129
    .local v9, "msg":Landroid/os/Message;
    new-instance v11, Lcom/sec/esdk/elm/type/ValidateContainer;

    invoke-direct {v11}, Lcom/sec/esdk/elm/type/ValidateContainer;-><init>()V

    .line 130
    .local v11, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    invoke-virtual {v11, p3}, Lcom/sec/esdk/elm/type/ValidateContainer;->setPushPointOfValidation(Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;)V

    .line 131
    invoke-virtual {v11}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v0

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setInstanceId(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v11}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v0

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setPackageName(Ljava/lang/String;)V

    .line 133
    invoke-virtual {v11}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v0

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getPackageVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setPackageVersion(Ljava/lang/String;)V

    .line 134
    invoke-virtual {v11}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setClientTimezone(Ljava/lang/String;)V

    .line 135
    invoke-virtual {v11}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->setLicenseKeyType(Ljava/lang/String;)V

    .line 137
    iput-object v11, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 138
    invoke-virtual {p0, v9}, Lcom/sec/esdk/elm/service/ELMEngine;->SendMessageHandler(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 147
    .end local v6    # "calendar":Ljava/util/Calendar;
    .end local v8    # "licenseInfo":Landroid/app/enterprise/license/LicenseInfo;
    .end local v9    # "msg":Landroid/os/Message;
    .end local v10    # "strClientTimezone":Ljava/lang/String;
    .end local v11    # "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    :catch_0
    move-exception v7

    .line 148
    .local v7, "e":Ljava/lang/NullPointerException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ELMEngine.startValidation(). NullPointException."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 149
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 141
    .end local v7    # "e":Ljava/lang/NullPointerException;
    .restart local v6    # "calendar":Ljava/util/Calendar;
    .restart local v8    # "licenseInfo":Landroid/app/enterprise/license/LicenseInfo;
    .restart local v10    # "strClientTimezone":Ljava/lang/String;
    :cond_1
    :try_start_1
    new-instance v4, Landroid/app/enterprise/license/Error;

    sget-object v0, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v0

    sget-object v1, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v1

    sget-object v2, Lcom/sec/esdk/elm/type/HttpsManagerError;->RequestEmpty:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorDesc()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v1, v2}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 144
    .local v4, "mError":Landroid/app/enterprise/license/Error;
    const-string v0, "fail"

    invoke-virtual {v8}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v1}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/esdk/elm/service/MDMBridge;->processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
