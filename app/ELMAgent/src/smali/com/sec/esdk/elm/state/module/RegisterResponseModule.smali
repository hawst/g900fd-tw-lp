.class public Lcom/sec/esdk/elm/state/module/RegisterResponseModule;
.super Lcom/sec/esdk/elm/state/module/ModuleBase;
.source "RegisterResponseModule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/esdk/elm/state/module/RegisterResponseModule$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;-><init>()V

    .line 144
    return-void
.end method

.method private processSuccess(Ljava/lang/String;Ljava/lang/String;JLandroid/app/enterprise/license/RightsObject;)V
    .locals 9
    .param p1, "instanceId"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "uploadFreq"    # J
    .param p5, "RO"    # Landroid/app/enterprise/license/RightsObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 172
    invoke-static {}, Landroid/os/SELinux;->isSELinuxEnforced()Z

    move-result v4

    if-ne v4, v6, :cond_0

    .line 173
    const-string v4, "enterprise_policy"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v3

    .line 175
    .local v3, "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v3, :cond_0

    .line 176
    invoke-interface {v3, v7}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setB2BMode(Z)I

    .line 180
    .end local v3    # "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RO.getLicenseType():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p5}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->i(Ljava/lang/String;)V

    .line 181
    invoke-static {p1, p3, p4}, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;->setRepeatAlarm(Ljava/lang/String;J)V

    .line 183
    invoke-virtual {p5}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ON"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 184
    const-string v4, "RegisterResponseModule.processRegisterResponse() : S is activated"

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 185
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getRedirectionDataUtil(Landroid/content/Context;)Lcom/sec/esdk/elm/utils/RedirectionDataUtil;

    move-result-object v0

    .line 186
    .local v0, "GSLBInfoPrefSuccess":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    invoke-virtual {v0, v6}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->setSELMActivated(Z)V

    .line 188
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 190
    .local v1, "cr":Landroid/content/ContentResolver;
    const-string v4, "elm_skey_activation_state"

    invoke-static {v1, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 191
    invoke-virtual {v0}, Lcom/sec/esdk/elm/utils/RedirectionDataUtil;->getSPDOption()Z

    move-result v4

    if-ne v4, v6, :cond_2

    .line 192
    const-string v4, "elm_skey_activation"

    invoke-static {v1, v4, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 196
    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RegisterResponseModule.processRegisterResponse : Get elm_skey_activation_state = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "elm_skey_activation_state"

    invoke-static {v1, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 197
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RegisterResponseModule.processRegisterResponse : Get elm_skey_activation = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "elm_skey_activation"

    invoke-static {v1, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 199
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.esdk.elm.skey.STATE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 200
    .local v2, "mIntent":Landroid/content/Intent;
    const-string v4, "com.sec.knox.seandroid"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    const-string v4, "IS_SELM_ACTIVATION"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 202
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 205
    .end local v0    # "GSLBInfoPrefSuccess":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "mIntent":Landroid/content/Intent;
    :cond_1
    return-void

    .line 194
    .restart local v0    # "GSLBInfoPrefSuccess":Lcom/sec/esdk/elm/utils/RedirectionDataUtil;
    .restart local v1    # "cr":Landroid/content/ContentResolver;
    :cond_2
    const-string v4, "elm_skey_activation"

    invoke-static {v1, v4, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method


# virtual methods
.method public processRegisterResponse(Lcom/sec/esdk/elm/type/RegisterContainer;)V
    .locals 28
    .param p1, "registerContainer"    # Lcom/sec/esdk/elm/type/RegisterContainer;

    .prologue
    .line 39
    const/16 v25, 0x0

    .line 40
    .local v25, "statusValue":Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    const/4 v5, 0x0

    .local v5, "packageName":Ljava/lang/String;
    const/4 v10, 0x0

    .local v10, "packageVer":Ljava/lang/String;
    const/4 v4, 0x0

    .local v4, "instanceId":Ljava/lang/String;
    const/4 v11, 0x0

    .line 41
    .local v11, "status":Ljava/lang/String;
    const/4 v14, 0x0

    .line 42
    .local v14, "error":Landroid/app/enterprise/license/Error;
    const/4 v8, 0x0

    .line 43
    .local v8, "RO":Landroid/app/enterprise/license/RightsObject;
    const-wide/16 v6, 0x0

    .line 44
    .local v6, "uploadFreq":J
    const/16 v21, 0x0

    .line 46
    .local v21, "mLicensePermissionGroup":Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v4

    .line 47
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->getPackageName()Ljava/lang/String;

    move-result-object v5

    .line 48
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->getPackageVer()Ljava/lang/String;

    move-result-object v10

    .line 49
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->getError()Landroid/app/enterprise/license/Error;

    move-result-object v14

    .line 50
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->getRO()Landroid/app/enterprise/license/RightsObject;

    move-result-object v8

    .line 51
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->getStatus()Ljava/lang/String;

    move-result-object v11

    .line 52
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getRegisterResponseContainer()Lcom/sec/esdk/elm/type/RegisterResponseContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/RegisterResponseContainer;->getLicensePermissionGroup()Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    move-result-object v21

    .line 54
    if-nez v14, :cond_0

    .line 55
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RegisterResponseModule.processRegisterResponse(). Error:Null. Status:"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 56
    new-instance v18, Landroid/app/enterprise/license/Error;

    sget-object v3, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v3

    sget-object v9, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v9

    sget-object v12, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v12}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorDesc()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v9, v12}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .end local v14    # "error":Landroid/app/enterprise/license/Error;
    .local v18, "error":Landroid/app/enterprise/license/Error;
    move-object/from16 v14, v18

    .line 60
    .end local v18    # "error":Landroid/app/enterprise/license/Error;
    .restart local v14    # "error":Landroid/app/enterprise/license/Error;
    :cond_0
    if-eqz v8, :cond_4

    if-eqz v4, :cond_4

    if-eqz v5, :cond_4

    if-eqz v10, :cond_4

    if-eqz v11, :cond_4

    .line 61
    invoke-virtual {v8}, Landroid/app/enterprise/license/RightsObject;->getExpiryDate()J

    move-result-wide v12

    const-wide/16 v26, 0x0

    cmp-long v3, v12, v26

    if-lez v3, :cond_1

    invoke-virtual {v8}, Landroid/app/enterprise/license/RightsObject;->getIssueDate()J

    move-result-wide v12

    const-wide/16 v26, 0x0

    cmp-long v3, v12, v26

    if-lez v3, :cond_1

    invoke-virtual {v8}, Landroid/app/enterprise/license/RightsObject;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v8}, Landroid/app/enterprise/license/RightsObject;->getPermissions()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v8}, Landroid/app/enterprise/license/RightsObject;->getState()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v8}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v8}, Landroid/app/enterprise/license/RightsObject;->getUploadFrequencyTime()J

    move-result-wide v12

    const-wide/16 v26, 0x0

    cmp-long v3, v12, v26

    if-gez v3, :cond_2

    .line 63
    :cond_1
    const-string v3, "RegisterResponseModule.processRegisterResponse(). RO has Null. Throw Exception()"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 64
    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    :catch_0
    move-exception v17

    .line 148
    .local v17, "e":Ljava/lang/NullPointerException;
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RegisterResponseModule.processRegisterResponse().  NullPointException. "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 149
    invoke-virtual/range {v17 .. v17}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 154
    const-string v3, "RegisterResponseModule.processRegisterResponse().  finally."

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 156
    const-string v3, "fail"

    invoke-virtual {v11, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 157
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    .line 169
    .end local v17    # "e":Ljava/lang/NullPointerException;
    :goto_0
    return-void

    .line 66
    :cond_2
    :try_start_2
    invoke-static {v11}, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;->getResponseStatusVaule(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    move-result-object v25

    .line 67
    invoke-virtual {v8}, Landroid/app/enterprise/license/RightsObject;->getUploadFrequencyTime()J

    move-result-wide v6

    .line 75
    :goto_1
    sget-object v3, Lcom/sec/esdk/elm/state/module/RegisterResponseModule$1;->$SwitchMap$com$sec$esdk$elm$datamanager$Constants$ResponseStatusVaule:[I

    invoke-virtual/range {v25 .. v25}, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->ordinal()I

    move-result v9

    aget v3, v3, v9
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    packed-switch v3, :pswitch_data_0

    .line 154
    :cond_3
    :goto_2
    const-string v3, "RegisterResponseModule.processRegisterResponse().  finally."

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 156
    const-string v3, "fail"

    invoke-virtual {v11, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 157
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto :goto_0

    .line 70
    :cond_4
    :try_start_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RegisterResponseModule.processRegisterResponse(). Some value is null. Status change to "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 71
    const-string v11, "fail"

    .line 72
    invoke-static {v11}, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;->getResponseStatusVaule(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    move-result-object v25

    goto :goto_1

    .line 77
    :pswitch_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RegisterResponseModule.processRegisterResponse(). SUCCESS switch. Status:"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 79
    sget-boolean v3, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;->$assertionsDisabled:Z

    if-nez v3, :cond_5

    if-nez v8, :cond_5

    new-instance v3, Ljava/lang/AssertionError;

    invoke-direct {v3}, Ljava/lang/AssertionError;-><init>()V

    throw v3
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 150
    :catch_1
    move-exception v17

    .line 151
    .local v17, "e":Ljava/lang/Exception;
    :try_start_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RegisterResponseModule.processRegisterResponse().  Exception. "

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 152
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 154
    const-string v3, "RegisterResponseModule.processRegisterResponse().  finally."

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 156
    const-string v3, "fail"

    invoke-virtual {v11, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 157
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_0

    .end local v17    # "e":Ljava/lang/Exception;
    :cond_5
    move-object/from16 v3, p0

    .line 80
    :try_start_5
    invoke-direct/range {v3 .. v8}, Lcom/sec/esdk/elm/state/module/RegisterResponseModule;->processSuccess(Ljava/lang/String;Ljava/lang/String;JLandroid/app/enterprise/license/RightsObject;)V

    .line 81
    const-string v3, "Success to Register Process"

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    .line 154
    :catchall_0
    move-exception v3

    const-string v9, "RegisterResponseModule.processRegisterResponse().  finally."

    invoke-static {v9}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 155
    invoke-static {}, Lcom/sec/esdk/elm/service/ELMEngine;->getInstance()Lcom/sec/esdk/elm/service/ELMEngine;

    move-result-object v9

    invoke-virtual {v9, v5}, Lcom/sec/esdk/elm/service/ELMEngine;->RemoveLicenseMap(Ljava/lang/String;)Ljava/lang/String;

    .line 156
    const-string v9, "fail"

    invoke-virtual {v11, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_11

    .line 157
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    sget-object v9, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    .line 164
    :goto_3
    throw v3

    .line 85
    :pswitch_1
    :try_start_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "RegisterResponseModule.processRegisterResponse(). FAIL switch. Status:"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v14}, Landroid/app/enterprise/license/Error;->getHttpResponseCode()I

    move-result v3

    sget-object v9, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerTerminated:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getHttpResponseCode()I

    move-result v9

    if-ne v3, v9, :cond_3

    invoke-virtual {v14}, Landroid/app/enterprise/license/Error;->getErrorCode()I

    move-result v3

    sget-object v9, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->ServerTerminated:Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/ErrorManager/HttpsErrorEnum;->getErrorCode()I

    move-result v9

    if-ne v3, v9, :cond_3

    .line 91
    const/4 v2, 0x0

    .line 92
    .local v2, "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getInstance()V

    .line 93
    const/16 v23, 0x0

    .line 95
    .local v23, "preONSCount":I
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 96
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_4
    array-length v3, v2

    move/from16 v0, v19

    if-ge v0, v3, :cond_9

    .line 97
    aget-object v3, v2, v19

    invoke-virtual {v3}, Landroid/app/enterprise/license/LicenseInfo;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    const/4 v9, 0x1

    if-ne v3, v9, :cond_6

    .line 106
    sget-object v3, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v3}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v3

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v9

    if-eq v3, v9, :cond_7

    .line 96
    :cond_6
    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    .line 111
    :cond_7
    aget-object v3, v2, v19

    invoke-virtual {v3}, Landroid/app/enterprise/license/LicenseInfo;->getInstanceId()Ljava/lang/String;

    move-result-object v20

    .line 112
    .local v20, "mInstanceId":Ljava/lang/String;
    const/16 v24, 0x0

    .line 113
    .local v24, "rightsObject":Landroid/app/enterprise/license/RightsObject;
    invoke-static/range {v20 .. v20}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v24

    .line 114
    if-eqz v24, :cond_8

    .line 115
    invoke-virtual/range {v24 .. v24}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v3

    sget-object v9, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->LICENSE_TYPE_ONS_STR:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 116
    add-int/lit8 v23, v23, 0x1

    .line 120
    :cond_8
    invoke-static/range {v20 .. v20}, Lcom/sec/esdk/elm/state/module/ModuleBase;->removeAlarm(Ljava/lang/String;)V

    .line 121
    invoke-static/range {v20 .. v20}, Lcom/sec/esdk/elm/service/MDMBridge;->resetLicense(Ljava/lang/String;)Z

    .line 122
    const-string v3, "success"

    const/4 v9, 0x0

    move-object/from16 v0, v20

    invoke-static {v3, v0, v9}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)V

    .line 123
    invoke-static/range {v20 .. v20}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteLicense(Ljava/lang/String;)Z

    .line 129
    .end local v19    # "i":I
    .end local v20    # "mInstanceId":Ljava/lang/String;
    .end local v24    # "rightsObject":Landroid/app/enterprise/license/RightsObject;
    :cond_9
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v2

    .line 130
    if-nez v2, :cond_a

    .line 131
    invoke-static {}, Landroid/os/SELinux;->isSELinuxEnforced()Z

    move-result v3

    const/4 v9, 0x1

    if-ne v3, v9, :cond_a

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/esdk/elm/utils/Utility;->isProduct_Ship(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 132
    const-string v3, "enterprise_policy"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v22

    .line 134
    .local v22, "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v22, :cond_a

    .line 135
    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setB2BMode(Z)I

    .line 139
    .end local v22    # "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :cond_a
    if-lez v23, :cond_3

    .line 140
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->RecoverySKeyStatusProcessing()Z
    :try_end_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2

    .line 160
    .end local v2    # "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    .end local v23    # "preONSCount":I
    :cond_b
    if-eqz v21, :cond_c

    .line 161
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    invoke-virtual/range {v21 .. v21}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 164
    :cond_c
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 160
    .local v17, "e":Ljava/lang/NullPointerException;
    :cond_d
    if-eqz v21, :cond_e

    .line 161
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    invoke-virtual/range {v21 .. v21}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 164
    :cond_e
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 160
    .local v17, "e":Ljava/lang/Exception;
    :cond_f
    if-eqz v21, :cond_10

    .line 161
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    invoke-virtual/range {v21 .. v21}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 164
    :cond_10
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    sget-object v3, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v3}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 160
    .end local v17    # "e":Ljava/lang/Exception;
    :cond_11
    if-eqz v21, :cond_12

    .line 161
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    invoke-virtual/range {v21 .. v21}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 164
    :cond_12
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/RegisterContainer;->getMDMRequestPushPoint()Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;

    move-result-object v15

    sget-object v9, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v9}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v16

    move-object v9, v5

    move-object v12, v4

    move-object v13, v8

    invoke-static/range {v9 .. v16}, Lcom/sec/esdk/elm/service/MDMBridge;->setActivationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;Ljava/lang/String;)Z

    goto/16 :goto_3

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
