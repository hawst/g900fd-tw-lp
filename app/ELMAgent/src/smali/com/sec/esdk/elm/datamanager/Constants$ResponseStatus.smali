.class public Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/datamanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResponseStatus"
.end annotation


# static fields
.field private static mResponseStatusMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;",
            ">;"
        }
    .end annotation
.end field

.field private static mResponseStatusVaule:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusVaule:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;
    .locals 3

    .prologue
    .line 71
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusVaule:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    invoke-direct {v0}, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusVaule:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    .line 73
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusMap:Ljava/util/Map;

    const-string v1, "success"

    sget-object v2, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->SUCCESS:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusMap:Ljava/util/Map;

    const-string v1, "fail"

    sget-object v2, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->FAIL:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusVaule:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    return-object v0
.end method

.method public static getStatus(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    .locals 1
    .param p0, "Status"    # Ljava/lang/String;

    .prologue
    .line 80
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusVaule:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    if-nez v0, :cond_0

    .line 81
    invoke-static {}, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->getInstance()Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    move-result-object v0

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusVaule:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    .line 83
    :cond_0
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->mResponseStatusMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    return-object v0
.end method
