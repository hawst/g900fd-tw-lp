.class public Lcom/sec/esdk/elm/type/UploadContainer;
.super Lcom/sec/esdk/elm/type/ELMContainer;
.source "UploadContainer.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private mUploadResponseContainer:Lcom/sec/esdk/elm/type/UploadResponseContainer;

.field private mUploadrRequestContainer:Lcom/sec/esdk/elm/type/UploadRequestContainer;

.field private retryCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/sec/esdk/elm/type/UploadContainer$1;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/UploadContainer$1;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/type/UploadContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Lcom/sec/esdk/elm/type/ELMContainer;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadrRequestContainer:Lcom/sec/esdk/elm/type/UploadRequestContainer;

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadResponseContainer:Lcom/sec/esdk/elm/type/UploadResponseContainer;

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->retryCount:I

    .line 65
    new-instance v0, Lcom/sec/esdk/elm/type/UploadRequestContainer;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/UploadRequestContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadrRequestContainer:Lcom/sec/esdk/elm/type/UploadRequestContainer;

    .line 66
    new-instance v0, Lcom/sec/esdk/elm/type/UploadResponseContainer;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/UploadResponseContainer;-><init>()V

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadResponseContainer:Lcom/sec/esdk/elm/type/UploadResponseContainer;

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Lcom/sec/esdk/elm/type/ELMContainer;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadrRequestContainer:Lcom/sec/esdk/elm/type/UploadRequestContainer;

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadResponseContainer:Lcom/sec/esdk/elm/type/UploadResponseContainer;

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->retryCount:I

    .line 49
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/type/UploadContainer;->readFromParcel(Landroid/os/Parcel;)V

    .line 50
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public getRetryCount()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->retryCount:I

    return v0
.end method

.method public getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadrRequestContainer:Lcom/sec/esdk/elm/type/UploadRequestContainer;

    return-object v0
.end method

.method public getUploadResponseContainer()Lcom/sec/esdk/elm/type/UploadResponseContainer;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadResponseContainer:Lcom/sec/esdk/elm/type/UploadResponseContainer;

    return-object v0
.end method

.method public increseRetryCount()V
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->retryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->retryCount:I

    .line 54
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/type/UploadRequestContainer;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadrRequestContainer:Lcom/sec/esdk/elm/type/UploadRequestContainer;

    .line 30
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/type/UploadResponseContainer;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadResponseContainer:Lcom/sec/esdk/elm/type/UploadResponseContainer;

    .line 31
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->retryCount:I

    .line 32
    return-void
.end method

.method public setRetryCount(I)V
    .locals 0
    .param p1, "nRetry"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/sec/esdk/elm/type/UploadContainer;->retryCount:I

    .line 62
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadrRequestContainer:Lcom/sec/esdk/elm/type/UploadRequestContainer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 24
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->mUploadResponseContainer:Lcom/sec/esdk/elm/type/UploadResponseContainer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 25
    iget v0, p0, Lcom/sec/esdk/elm/type/UploadContainer;->retryCount:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 26
    return-void
.end method
