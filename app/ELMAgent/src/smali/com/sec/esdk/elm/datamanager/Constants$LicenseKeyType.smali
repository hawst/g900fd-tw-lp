.class public final enum Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;
.super Ljava/lang/Enum;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/esdk/elm/datamanager/Constants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LicenseKeyType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

.field public static final enum KEY_TYPE_DEV:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

.field public static final enum KEY_TYPE_ONS1:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

.field public static final enum KEY_TYPE_PROD:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

.field public static final enum LICENSE_TYPE_ONS_STR:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;


# instance fields
.field private KEY_TYPE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 144
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    const-string v1, "KEY_TYPE_DEV"

    const-string v2, "DEV"

    invoke-direct {v0, v1, v3, v2}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_DEV:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    .line 145
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    const-string v1, "KEY_TYPE_PROD"

    const-string v2, "PRD"

    invoke-direct {v0, v1, v4, v2}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_PROD:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    .line 146
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    const-string v1, "KEY_TYPE_ONS1"

    const-string v2, "ON1"

    invoke-direct {v0, v1, v5, v2}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_ONS1:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    .line 147
    new-instance v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    const-string v1, "LICENSE_TYPE_ONS_STR"

    const-string v2, "ON"

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->LICENSE_TYPE_ONS_STR:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    .line 143
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_DEV:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_PROD:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_ONS1:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->LICENSE_TYPE_ONS_STR:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p3, "mKEY_TYPE"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE:Ljava/lang/String;

    .line 152
    iput-object p3, p0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE:Ljava/lang/String;

    .line 153
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 143
    const-class v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    return-object v0
.end method

.method public static values()[Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->$VALUES:[Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v0}, [Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE:Ljava/lang/String;

    return-object v0
.end method
