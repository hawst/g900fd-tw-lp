.class public interface abstract Lcom/sec/esdk/elm/datamanager/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/esdk/elm/datamanager/Constants$PUBSerialNumber;,
        Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;,
        Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;,
        Lcom/sec/esdk/elm/datamanager/Constants$MDMRequestPushPoint;,
        Lcom/sec/esdk/elm/datamanager/Constants$PushPointOfValidation;,
        Lcom/sec/esdk/elm/datamanager/Constants$GSLBClass;,
        Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;,
        Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;,
        Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;,
        Lcom/sec/esdk/elm/datamanager/Constants$ServerConnectionType;
    }
.end annotation


# static fields
.field public static final RESPONSESTATUS_INSTANCE:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;->getInstance()Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    move-result-object v0

    sput-object v0, Lcom/sec/esdk/elm/datamanager/Constants;->RESPONSESTATUS_INSTANCE:Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatus;

    return-void
.end method
