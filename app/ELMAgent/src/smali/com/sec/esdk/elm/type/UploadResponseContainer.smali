.class public Lcom/sec/esdk/elm/type/UploadResponseContainer;
.super Ljava/lang/Object;
.source "UploadResponseContainer.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private Error:Landroid/app/enterprise/license/Error;

.field private InstanceId:Ljava/lang/String;

.field private ServerTime:Ljava/lang/String;

.field private Status:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/sec/esdk/elm/type/UploadResponseContainer$1;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/UploadResponseContainer$1;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->InstanceId:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Status:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->ServerTime:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->InstanceId:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Status:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->ServerTime:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 55
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/type/UploadResponseContainer;->readFromParcel(Landroid/os/Parcel;)V

    .line 56
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public getError()Landroid/app/enterprise/license/Error;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    return-object v0
.end method

.method public getInstanceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->InstanceId:Ljava/lang/String;

    return-object v0
.end method

.method public getServerTime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->ServerTime:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Status:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->InstanceId:Ljava/lang/String;

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Status:Ljava/lang/String;

    .line 34
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->ServerTime:Ljava/lang/String;

    .line 36
    const-class v0, Landroid/app/enterprise/license/Error;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/license/Error;

    iput-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 37
    return-void
.end method

.method public setError(Landroid/app/enterprise/license/Error;)V
    .locals 0
    .param p1, "error"    # Landroid/app/enterprise/license/Error;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    .line 89
    return-void
.end method

.method public setInstanceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 64
    iput-object p1, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->InstanceId:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public setServerTime(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverTime"    # Ljava/lang/String;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->ServerTime:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public setStatus(Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Status:Ljava/lang/String;

    .line 73
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->InstanceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 26
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Status:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->ServerTime:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 28
    iget-object v0, p0, Lcom/sec/esdk/elm/type/UploadResponseContainer;->Error:Landroid/app/enterprise/license/Error;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 29
    return-void
.end method
