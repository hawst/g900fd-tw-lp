.class public Lcom/sec/esdk/elm/state/module/ValidateResponseModule;
.super Lcom/sec/esdk/elm/state/module/ModuleBase;
.source "ValidateResponseModule.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/esdk/elm/state/module/ValidateResponseModule$1;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/esdk/elm/state/module/ModuleBase;-><init>()V

    .line 103
    return-void
.end method


# virtual methods
.method public failProcessing(Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)V
    .locals 12
    .param p1, "container"    # Lcom/sec/esdk/elm/type/ELMContainer;
    .param p2, "in_freq"    # Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    .prologue
    const/4 v3, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x1

    .line 113
    const-string v0, "ValidateResponseModule.failProcessing()."

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 115
    const/4 v1, 0x0

    .local v1, "instanceId":Ljava/lang/String;
    move-object v9, p1

    .line 116
    check-cast v9, Lcom/sec/esdk/elm/type/ValidateContainer;

    .line 117
    .local v9, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getError()Landroid/app/enterprise/license/Error;

    move-result-object v4

    .line 118
    .local v4, "error":Landroid/app/enterprise/license/Error;
    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    .line 120
    if-nez v4, :cond_0

    .line 121
    new-instance v4, Landroid/app/enterprise/license/Error;

    .end local v4    # "error":Landroid/app/enterprise/license/Error;
    sget-object v0, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getHttpResponseCode()I

    move-result v0

    sget-object v2, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v2

    sget-object v5, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v5}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorDesc()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v2, v5}, Landroid/app/enterprise/license/Error;-><init>(IILjava/lang/String;)V

    .line 123
    .restart local v4    # "error":Landroid/app/enterprise/license/Error;
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ResponseCode : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Landroid/app/enterprise/license/Error;->getHttpResponseCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ErrorCode :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4}, Landroid/app/enterprise/license/Error;->getErrorCode()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/ELMLog;->e(Ljava/lang/String;)V

    .line 125
    invoke-virtual {v4}, Landroid/app/enterprise/license/Error;->getHttpResponseCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 181
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to Validate Response. CODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v4}, Landroid/app/enterprise/license/Error;->getHttpResponseCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    .line 185
    :cond_1
    :goto_0
    :sswitch_0
    return-void

    .line 133
    :sswitch_1
    invoke-virtual {v4}, Landroid/app/enterprise/license/Error;->getErrorCode()I

    move-result v0

    sget-object v2, Lcom/sec/esdk/elm/type/HttpsManagerError;->Terminated:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v2

    if-ne v0, v2, :cond_2

    .line 134
    const-string v0, "fail"

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v5}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/sec/esdk/elm/service/MDMBridge;->processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)V

    .line 137
    invoke-static {v1}, Lcom/sec/esdk/elm/state/module/ModuleBase;->removeAlarm(Ljava/lang/String;)V

    .line 139
    invoke-static {v1}, Lcom/sec/esdk/elm/service/MDMBridge;->resetLicense(Ljava/lang/String;)Z

    .line 140
    const-string v0, "success"

    invoke-static {v0, v1, v3}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)V

    .line 141
    invoke-static {v1}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteLicense(Ljava/lang/String;)Z

    .line 145
    :cond_2
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->RecoverySKeyStatusProcessing()Z

    .line 146
    invoke-static {}, Lcom/sec/esdk/elm/service/MDMBridge;->getAllLicenseInfoFromSDK()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v6

    .line 147
    .local v6, "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    if-nez v6, :cond_1

    .line 148
    invoke-static {}, Landroid/os/SELinux;->isSELinuxEnforced()Z

    move-result v0

    if-ne v0, v10, :cond_1

    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/esdk/elm/utils/Utility;->isProduct_Ship(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    const-string v0, "enterprise_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v8

    .line 151
    .local v8, "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    if-eqz v8, :cond_1

    .line 153
    const/4 v0, 0x1

    :try_start_0
    invoke-interface {v8, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setB2BMode(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 154
    :catch_0
    move-exception v7

    .line 155
    .local v7, "e1":Landroid/os/RemoteException;
    invoke-virtual {v7}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 162
    .end local v6    # "arrayLicenseInfo":[Landroid/app/enterprise/license/LicenseInfo;
    .end local v7    # "e1":Landroid/os/RemoteException;
    .end local v8    # "mService":Landroid/app/enterprise/IEnterpriseDeviceManager;
    :sswitch_2
    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateContainer;->getRetryCount()I

    move-result v0

    if-ge v0, v11, :cond_1

    .line 163
    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateContainer;->increseRetryCount()V

    .line 164
    sget-object v0, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v0

    invoke-static {p2}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->getTime(Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)J

    move-result-wide v2

    invoke-static {v9, v0, v2, v3}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->SendRetryIntent(Lcom/sec/esdk/elm/type/ELMContainer;IJ)V

    .line 165
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to Validate Response. CODE : 500 Retry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateContainer;->getRetryCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 171
    :sswitch_3
    invoke-virtual {v4}, Landroid/app/enterprise/license/Error;->getErrorCode()I

    move-result v0

    sget-object v2, Lcom/sec/esdk/elm/type/HttpsManagerError;->NullInputParam:Lcom/sec/esdk/elm/type/HttpsManagerError;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/HttpsManagerError;->getErrorCode()I

    move-result v2

    if-eq v0, v2, :cond_1

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateContainer;->getRetryCount()I

    move-result v0

    if-ge v0, v11, :cond_1

    .line 173
    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateContainer;->increseRetryCount()V

    .line 174
    invoke-static {}, Lcom/sec/esdk/elm/ELMContextManager/ELMContextManager;->getELMContext()Landroid/content/Context;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Fail to Validate Response. CODE : 1000 Retry "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v9}, Lcom/sec/esdk/elm/type/ValidateContainer;->getRetryCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/esdk/elm/utils/ELMLog;->popup(Landroid/content/Context;Ljava/lang/String;)V

    .line 176
    sget-object v0, Lcom/sec/esdk/elm/type/States;->VALIDATE_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v0

    invoke-static {p2}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->getTime(Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)J

    move-result-wide v2

    invoke-static {v9, v0, v2, v3}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->SendRetryIntent(Lcom/sec/esdk/elm/type/ELMContainer;IJ)V

    goto/16 :goto_0

    .line 125
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x12c -> :sswitch_0
        0x12d -> :sswitch_0
        0x190 -> :sswitch_1
        0x1f4 -> :sswitch_2
        0x3e8 -> :sswitch_3
    .end sparse-switch
.end method

.method public processValidateResponse(Lcom/sec/esdk/elm/type/ValidateContainer;)V
    .locals 18
    .param p1, "validateContainer"    # Lcom/sec/esdk/elm/type/ValidateContainer;

    .prologue
    .line 35
    const/4 v3, 0x0

    .line 36
    .local v3, "instanceId":Ljava/lang/String;
    const/4 v2, 0x0

    .line 37
    .local v2, "status":Ljava/lang/String;
    const/4 v10, 0x0

    .line 38
    .local v10, "statusValue":Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;
    const-wide/16 v12, 0x0

    .line 39
    .local v12, "uploadFrequencyValue":J
    const/4 v5, 0x0

    .line 40
    .local v5, "RO":Landroid/app/enterprise/license/RightsObject;
    const/4 v9, 0x0

    .line 43
    .local v9, "mLicensePermissionGroup":Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ValidateResponseModule.processValidateResponse()."

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getServerTime()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getStatus()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getRO()Landroid/app/enterprise/license/RightsObject;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getError()Landroid/app/enterprise/license/Error;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, ", "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getLicensePermissionGroup()Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 49
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    .line 50
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getStatus()Ljava/lang/String;

    move-result-object v2

    .line 51
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getRO()Landroid/app/enterprise/license/RightsObject;

    move-result-object v5

    .line 52
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getError()Landroid/app/enterprise/license/Error;

    move-result-object v6

    .line 53
    .local v6, "error":Landroid/app/enterprise/license/Error;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getLicensePermissionGroup()Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    move-result-object v9

    .line 56
    if-nez v5, :cond_0

    .line 57
    invoke-static {v3}, Lcom/sec/esdk/elm/service/MDMBridge;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v5

    .line 59
    :cond_0
    if-eqz v5, :cond_3

    if-eqz v3, :cond_3

    if-eqz v2, :cond_3

    .line 60
    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getExpiryDate()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v4, v14, v16

    if-lez v4, :cond_1

    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getIssueDate()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v4, v14, v16

    if-lez v4, :cond_1

    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getInstanceId()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getPermissions()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getState()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getUploadFrequencyTime()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v4, v14, v16

    if-gez v4, :cond_2

    .line 62
    :cond_1
    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    .end local v6    # "error":Landroid/app/enterprise/license/Error;
    :catch_0
    move-exception v8

    .line 107
    .local v8, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;->ONE_HOUR:Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->failProcessing(Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)V

    .line 109
    .end local v8    # "e":Ljava/lang/Exception;
    :goto_0
    return-void

    .line 65
    .restart local v6    # "error":Landroid/app/enterprise/license/Error;
    :cond_2
    :try_start_1
    invoke-static {v2}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->getResponseStatusVaule(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    move-result-object v10

    .line 66
    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getUploadFrequencyTime()J

    move-result-wide v12

    .line 73
    :goto_1
    sget-object v4, Lcom/sec/esdk/elm/state/module/ValidateResponseModule$1;->$SwitchMap$com$sec$esdk$elm$datamanager$Constants$ResponseStatusVaule:[I

    invoke-virtual {v10}, Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;->ordinal()I

    move-result v7

    aget v4, v4, v7

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 75
    :pswitch_0
    const-string v4, "ValidateResponseModule.processValidateResponse(). SUCCESS"

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 77
    sget-boolean v4, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->$assertionsDisabled:Z

    if-nez v4, :cond_4

    if-nez v5, :cond_4

    new-instance v4, Ljava/lang/AssertionError;

    invoke-direct {v4}, Ljava/lang/AssertionError;-><init>()V

    throw v4

    .line 68
    :cond_3
    const-string v4, "ValidateResponseModule.processValidateResponse(). RO|instanceId|status is null. status change null."

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 69
    const-string v2, "fail"

    .line 70
    const-string v4, "fail"

    invoke-static {v4}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->getResponseStatusVaule(Ljava/lang/String;)Lcom/sec/esdk/elm/datamanager/Constants$ResponseStatusVaule;

    move-result-object v10

    goto :goto_1

    .line 78
    :cond_4
    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getUploadFrequencyTime()J

    move-result-wide v12

    .line 79
    invoke-static {v3, v12, v13}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->setRepeatAlarm(Ljava/lang/String;J)V

    .line 80
    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v4

    sget-object v7, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->LICENSE_TYPE_ONS_STR:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v7}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v5}, Landroid/app/enterprise/license/RightsObject;->getLicenseType()Ljava/lang/String;

    move-result-object v4

    sget-object v7, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->KEY_TYPE_DEV:Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;

    invoke-virtual {v7}, Lcom/sec/esdk/elm/datamanager/Constants$LicenseKeyType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 83
    :cond_5
    const-string v4, "success"

    const/4 v7, 0x0

    invoke-static {v4, v3, v7}, Lcom/sec/esdk/elm/service/MDMBridge;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)V

    .line 89
    :goto_2
    const-string v4, "fail"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 90
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v4

    sget-object v7, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v7}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/sec/esdk/elm/service/MDMBridge;->processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)V

    goto :goto_0

    .line 85
    :cond_6
    invoke-virtual/range {p0 .. p1}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->successProcessing(Lcom/sec/esdk/elm/type/ELMContainer;)V

    goto :goto_2

    .line 92
    :cond_7
    if-eqz v9, :cond_8

    .line 93
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v9}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/sec/esdk/elm/service/MDMBridge;->processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 95
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v4

    sget-object v7, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->UNKNOWN_GROUP:Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;

    invoke-virtual {v7}, Lcom/sec/esdk/elm/datamanager/Constants$LicensePermissionGroup;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/sec/esdk/elm/service/MDMBridge;->processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 101
    :pswitch_1
    const-string v4, "ValidateResponseModule.processValidateResponse(). FAIL"

    invoke-static {v4}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    .line 102
    sget-object v4, Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;->ONE_HOUR:Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->failProcessing(Lcom/sec/esdk/elm/type/ELMContainer;Lcom/sec/esdk/elm/datamanager/Constants$FailFrequency;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public successProcessing(Lcom/sec/esdk/elm/type/ELMContainer;)V
    .locals 4
    .param p1, "container"    # Lcom/sec/esdk/elm/type/ELMContainer;

    .prologue
    .line 189
    const-string v2, "ValidateResponseModule.successProcessing()."

    invoke-static {v2}, Lcom/sec/esdk/elm/utils/ELMLog;->d(Ljava/lang/String;)V

    move-object v1, p1

    .line 190
    check-cast v1, Lcom/sec/esdk/elm/type/ValidateContainer;

    .line 193
    .local v1, "validateContainer":Lcom/sec/esdk/elm/type/ValidateContainer;
    new-instance v0, Lcom/sec/esdk/elm/type/UploadContainer;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/UploadContainer;-><init>()V

    .line 194
    .local v0, "uploadContainer":Lcom/sec/esdk/elm/type/UploadContainer;
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateResponseContainer()Lcom/sec/esdk/elm/type/ValidateResponseContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/ValidateResponseContainer;->getInstanceId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->setInstanceId(Ljava/lang/String;)V

    .line 195
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->setPackageName(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getPackageVersion()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->setPackageVersion(Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0}, Lcom/sec/esdk/elm/type/UploadContainer;->getUploadRequestContainer()Lcom/sec/esdk/elm/type/UploadRequestContainer;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getValidateRequestContainer()Lcom/sec/esdk/elm/type/ValidateRequestContainer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->getClientTimezone()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/esdk/elm/type/UploadRequestContainer;->setClientTimezone(Ljava/lang/String;)V

    .line 199
    invoke-virtual {v1}, Lcom/sec/esdk/elm/type/ValidateContainer;->getRetryCount()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/sec/esdk/elm/type/UploadContainer;->setRetryCount(I)V

    .line 201
    sget-object v2, Lcom/sec/esdk/elm/type/States;->UPLOAD_REQUEST:Lcom/sec/esdk/elm/type/States;

    invoke-virtual {v2}, Lcom/sec/esdk/elm/type/States;->ordinal()I

    move-result v2

    invoke-static {v0, v2}, Lcom/sec/esdk/elm/state/module/ValidateResponseModule;->SendMessageHandler(Lcom/sec/esdk/elm/type/ELMContainer;I)V

    .line 202
    return-void
.end method
