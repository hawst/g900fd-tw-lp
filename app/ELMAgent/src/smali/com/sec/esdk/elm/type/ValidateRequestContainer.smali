.class public Lcom/sec/esdk/elm/type/ValidateRequestContainer;
.super Ljava/lang/Object;
.source "ValidateRequestContainer.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private ApkHash:Ljava/lang/String;

.field private ClientTimezone:Ljava/lang/String;

.field private InstanceId:Ljava/lang/String;

.field private LicenseKeyType:Ljava/lang/String;

.field private PackageName:Ljava/lang/String;

.field private PackageVersion:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/sec/esdk/elm/type/ValidateRequestContainer$1;

    invoke-direct {v0}, Lcom/sec/esdk/elm/type/ValidateRequestContainer$1;-><init>()V

    sput-object v0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->LicenseKeyType:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->InstanceId:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageName:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageVersion:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ApkHash:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ClientTimezone:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->LicenseKeyType:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->InstanceId:Ljava/lang/String;

    .line 18
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageName:Ljava/lang/String;

    .line 20
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageVersion:Ljava/lang/String;

    .line 22
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ApkHash:Ljava/lang/String;

    .line 24
    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ClientTimezone:Ljava/lang/String;

    .line 64
    invoke-virtual {p0, p1}, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->readFromParcel(Landroid/os/Parcel;)V

    .line 65
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public getApkHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ApkHash:Ljava/lang/String;

    return-object v0
.end method

.method public getClientTimezone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ClientTimezone:Ljava/lang/String;

    return-object v0
.end method

.method public getInstanceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->InstanceId:Ljava/lang/String;

    return-object v0
.end method

.method public getLicenseKeyType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->LicenseKeyType:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageVersion:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->LicenseKeyType:Ljava/lang/String;

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->InstanceId:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageName:Ljava/lang/String;

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageVersion:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ApkHash:Ljava/lang/String;

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ClientTimezone:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setApkHash(Ljava/lang/String;)V
    .locals 0
    .param p1, "apkHash"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ApkHash:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setClientTimezone(Ljava/lang/String;)V
    .locals 0
    .param p1, "clientTimezone"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ClientTimezone:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setInstanceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->InstanceId:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setLicenseKeyType(Ljava/lang/String;)V
    .locals 0
    .param p1, "iLicenseKeyType"    # Ljava/lang/String;

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->LicenseKeyType:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageName:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setPackageVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageVersion"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageVersion:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->LicenseKeyType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->InstanceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 30
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->PackageVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 32
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ApkHash:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/sec/esdk/elm/type/ValidateRequestContainer;->ClientTimezone:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 34
    return-void
.end method
