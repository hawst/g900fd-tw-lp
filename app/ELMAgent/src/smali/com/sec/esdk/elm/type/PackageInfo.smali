.class public Lcom/sec/esdk/elm/type/PackageInfo;
.super Ljava/lang/Object;
.source "PackageInfo.java"


# instance fields
.field private mApkHash:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mPackageVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput-object v0, p0, Lcom/sec/esdk/elm/type/PackageInfo;->mPackageName:Ljava/lang/String;

    .line 12
    iput-object v0, p0, Lcom/sec/esdk/elm/type/PackageInfo;->mPackageVersion:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/sec/esdk/elm/type/PackageInfo;->mApkHash:Ljava/lang/String;

    .line 18
    return-void
.end method


# virtual methods
.method public getApkHash()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/esdk/elm/type/PackageInfo;->mApkHash:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/esdk/elm/type/PackageInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/esdk/elm/type/PackageInfo;->mPackageVersion:Ljava/lang/String;

    return-object v0
.end method

.method public setApkHash(Ljava/lang/String;)V
    .locals 0
    .param p1, "apkHash"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/esdk/elm/type/PackageInfo;->mApkHash:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setPackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/esdk/elm/type/PackageInfo;->mPackageName:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setPackageVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageVersion"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/sec/esdk/elm/type/PackageInfo;->mPackageVersion:Ljava/lang/String;

    .line 32
    return-void
.end method
