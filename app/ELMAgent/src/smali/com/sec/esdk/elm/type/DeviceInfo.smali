.class public Lcom/sec/esdk/elm/type/DeviceInfo;
.super Ljava/lang/Object;
.source "DeviceInfo.java"


# instance fields
.field private mAndroidVersion:Ljava/lang/String;

.field private mBuildNumber:Ljava/lang/String;

.field private mClientTimeZone:Ljava/lang/String;

.field private mDeviceCarrier:Ljava/lang/String;

.field private mEsdkVer:Ljava/lang/String;

.field private mIMEI:Ljava/lang/String;

.field private mMCC:Ljava/lang/String;

.field private mModel:Ljava/lang/String;

.field private mSimCarrier1:Ljava/lang/String;

.field private mSimCarrier2:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mIMEI:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mModel:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mAndroidVersion:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mBuildNumber:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mMCC:Ljava/lang/String;

    .line 19
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mEsdkVer:Ljava/lang/String;

    .line 21
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mDeviceCarrier:Ljava/lang/String;

    .line 23
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mSimCarrier1:Ljava/lang/String;

    .line 25
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mSimCarrier2:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mClientTimeZone:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public getAndroidVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mAndroidVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getBuildNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mBuildNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getCilentTimeZone()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mClientTimeZone:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceCarrier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mDeviceCarrier:Ljava/lang/String;

    return-object v0
.end method

.method public getEsdkVer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mEsdkVer:Ljava/lang/String;

    return-object v0
.end method

.method public getIMEI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mIMEI:Ljava/lang/String;

    return-object v0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mMCC:Ljava/lang/String;

    return-object v0
.end method

.method public getModel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mModel:Ljava/lang/String;

    return-object v0
.end method

.method public getSimCarrier1()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mSimCarrier1:Ljava/lang/String;

    return-object v0
.end method

.method public getSimCarrier2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mSimCarrier2:Ljava/lang/String;

    return-object v0
.end method

.method public setAndroidVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "androidVersion"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mAndroidVersion:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setBuildNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "buildNumber"    # Ljava/lang/String;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mBuildNumber:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public setCilentTimeZone(Ljava/lang/String;)V
    .locals 0
    .param p1, "cilentTimeZone"    # Ljava/lang/String;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mClientTimeZone:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public setDeviceCarrier(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceCarrier"    # Ljava/lang/String;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mDeviceCarrier:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public setEsdkVer(Ljava/lang/String;)V
    .locals 0
    .param p1, "esdkVer"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mEsdkVer:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setIMEI(Ljava/lang/String;)V
    .locals 0
    .param p1, "IMEI"    # Ljava/lang/String;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mIMEI:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public setMCC(Ljava/lang/String;)V
    .locals 0
    .param p1, "MCC"    # Ljava/lang/String;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mMCC:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public setModel(Ljava/lang/String;)V
    .locals 0
    .param p1, "model"    # Ljava/lang/String;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mModel:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public setSimCarrier1(Ljava/lang/String;)V
    .locals 0
    .param p1, "simCarrier"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mSimCarrier1:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public setSimCarrier2(Ljava/lang/String;)V
    .locals 0
    .param p1, "simCarrier"    # Ljava/lang/String;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/esdk/elm/type/DeviceInfo;->mSimCarrier2:Ljava/lang/String;

    .line 88
    return-void
.end method
