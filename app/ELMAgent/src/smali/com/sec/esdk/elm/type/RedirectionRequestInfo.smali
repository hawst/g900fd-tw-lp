.class public Lcom/sec/esdk/elm/type/RedirectionRequestInfo;
.super Ljava/lang/Object;
.source "RedirectionRequestInfo.java"


# instance fields
.field private mCountryISO:Ljava/lang/String;

.field private mDeviceID:Ljava/lang/String;

.field private mMCC:Ljava/lang/String;

.field private mSalesCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mDeviceID:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mCountryISO:Ljava/lang/String;

    .line 13
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mSalesCode:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mMCC:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public getCountryISO()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mCountryISO:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mDeviceID:Ljava/lang/String;

    return-object v0
.end method

.method public getMCC()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mMCC:Ljava/lang/String;

    return-object v0
.end method

.method public getSalesCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mSalesCode:Ljava/lang/String;

    return-object v0
.end method

.method public setCountryISO(Ljava/lang/String;)V
    .locals 0
    .param p1, "countryISO"    # Ljava/lang/String;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mCountryISO:Ljava/lang/String;

    .line 34
    return-void
.end method

.method public setDeviceID(Ljava/lang/String;)V
    .locals 0
    .param p1, "deviceID"    # Ljava/lang/String;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mDeviceID:Ljava/lang/String;

    .line 30
    return-void
.end method

.method public setMCC(Ljava/lang/String;)V
    .locals 0
    .param p1, "MCC"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mMCC:Ljava/lang/String;

    .line 42
    return-void
.end method

.method public setSalesCode(Ljava/lang/String;)V
    .locals 0
    .param p1, "salesCode"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/esdk/elm/type/RedirectionRequestInfo;->mSalesCode:Ljava/lang/String;

    .line 38
    return-void
.end method
