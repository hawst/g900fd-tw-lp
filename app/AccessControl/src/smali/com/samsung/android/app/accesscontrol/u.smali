.class public Lcom/samsung/android/app/accesscontrol/u;
.super Landroid/widget/RelativeLayout;


# instance fields
.field final synthetic a:Lcom/samsung/android/app/accesscontrol/t;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/accesscontrol/t;Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/u;->a:Lcom/samsung/android/app/accesscontrol/t;

    invoke-direct {p0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/u;->b:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/u;->c:I

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 3

    const-string v0, "FakeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[c] onLayout()+"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/u;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/u;->b:I

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/u;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/u;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/u;->b:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/u;->a:Lcom/samsung/android/app/accesscontrol/t;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/t;->a(Lcom/samsung/android/app/accesscontrol/t;)Landroid/graphics/Point;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/u;->b:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/u;->c:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/u;->a:Lcom/samsung/android/app/accesscontrol/t;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/t;->b(Lcom/samsung/android/app/accesscontrol/t;)Lcom/samsung/android/app/accesscontrol/v;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/u;->c:I

    invoke-interface {v0, v1}, Lcom/samsung/android/app/accesscontrol/v;->a(I)V

    :cond_0
    return-void
.end method
