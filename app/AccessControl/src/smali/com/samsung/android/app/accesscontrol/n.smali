.class public Lcom/samsung/android/app/accesscontrol/n;
.super Landroid/view/View;

# interfaces
.implements Lcom/samsung/android/app/accesscontrol/z;


# instance fields
.field private final A:I

.field private B:I

.field private C:Lcom/samsung/android/app/accesscontrol/q;

.field private D:F

.field private E:F

.field private F:F

.field private G:F

.field private H:I

.field private I:I

.field private J:I

.field private K:I

.field private L:I

.field private M:I

.field private N:Z

.field private O:Landroid/view/GestureDetector;

.field private P:Lcom/samsung/android/app/accesscontrol/s;

.field private Q:Z

.field private R:Z

.field private S:Landroid/widget/ImageView;

.field private T:Landroid/view/ViewGroup;

.field private U:Landroid/content/res/Resources;

.field private final a:Ljava/lang/String;

.field private final b:I

.field private c:I

.field private d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private h:Landroid/content/Context;

.field private i:Landroid/graphics/Paint;

.field private j:Lcom/samsung/android/app/accesscontrol/RectAnimator;

.field private k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

.field private l:Ljava/util/ArrayList;

.field private m:Ljava/util/ArrayList;

.field private n:Landroid/graphics/Bitmap;

.field private o:Landroid/graphics/Bitmap;

.field private p:Landroid/graphics/Bitmap;

.field private q:I

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Landroid/graphics/Rect;

.field private v:I

.field private w:I

.field private final x:I

.field private final y:I

.field private final z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const-string v0, "AccessControlView"

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->a:Ljava/lang/String;

    iput v4, p0, Lcom/samsung/android/app/accesscontrol/n;->b:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->d:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->e:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->f:I

    iput v5, p0, Lcom/samsung/android/app/accesscontrol/n;->g:I

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->h:Landroid/content/Context;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->j:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->n:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    iput v5, p0, Lcom/samsung/android/app/accesscontrol/n;->q:I

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/n;->s:Z

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/n;->t:Z

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iput v4, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    iput v4, p0, Lcom/samsung/android/app/accesscontrol/n;->w:I

    sget v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    sget v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    sget v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->e:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    sget v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->f:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->B:I

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    iput v3, p0, Lcom/samsung/android/app/accesscontrol/n;->D:F

    iput v3, p0, Lcom/samsung/android/app/accesscontrol/n;->E:F

    iput v3, p0, Lcom/samsung/android/app/accesscontrol/n;->F:F

    iput v3, p0, Lcom/samsung/android/app/accesscontrol/n;->G:F

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->H:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->I:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->L:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->M:I

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->O:Landroid/view/GestureDetector;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->P:Lcom/samsung/android/app/accesscontrol/s;

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/n;->Q:Z

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/n;->R:Z

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->T:Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/n;->h:Landroid/content/Context;

    iput-object p2, p0, Lcom/samsung/android/app/accesscontrol/n;->T:Landroid/view/ViewGroup;

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->T:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/samsung/android/app/accesscontrol/RectAnimator;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;-><init>(Lcom/samsung/android/app/accesscontrol/z;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->j:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/app/accesscontrol/s;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->P:Lcom/samsung/android/app/accesscontrol/s;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/n;->f()V

    return-void
.end method

.method private a(II)I
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/l;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/n;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    return v0
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/n;I)I
    .locals 0

    iput p1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    return p1
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/n;Lcom/samsung/android/app/accesscontrol/q;)Lcom/samsung/android/app/accesscontrol/q;
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    return-object p1
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V
    .locals 5

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->b:Lcom/samsung/android/app/accesscontrol/q;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->L:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->M:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->c:Lcom/samsung/android/app/accesscontrol/q;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->L:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->M:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->d:Lcom/samsung/android/app/accesscontrol/q;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->L:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->M:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->g:Lcom/samsung/android/app/accesscontrol/q;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->h:Lcom/samsung/android/app/accesscontrol/q;

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_9

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->e:Lcom/samsung/android/app/accesscontrol/q;

    if-eq v0, v1, :cond_a

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_b

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->f:Lcom/samsung/android/app/accesscontrol/q;

    if-eq v0, v1, :cond_c

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_d

    :cond_c
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_d
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->a:Lcom/samsung/android/app/accesscontrol/q;

    if-eq v0, v1, :cond_e

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_f

    :cond_e
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->n:Landroid/graphics/Bitmap;

    iget v1, p2, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->H:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->I:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget v0, p2, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->H:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iget v1, p2, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->I:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/accesscontrol/n;->c(II)V

    :cond_f
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/n;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/app/accesscontrol/n;->R:Z

    return p1
.end method

.method static synthetic b(Lcom/samsung/android/app/accesscontrol/n;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    return v0
.end method

.method private b(II)V
    .locals 5

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->i:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/l;->a:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->b:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    goto :goto_0

    :cond_2
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->c:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    goto :goto_0

    :cond_3
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->H:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->I:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->H:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->I:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->a:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    goto/16 :goto_0

    :cond_4
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->d:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-le v2, v3, :cond_7

    iget v2, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->g:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    goto/16 :goto_0

    :cond_6
    iget v2, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->h:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-le v2, v3, :cond_0

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->e:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    goto/16 :goto_0

    :cond_8
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->top:I

    iget v0, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    iget v0, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->f:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/samsung/android/app/accesscontrol/n;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/app/accesscontrol/n;->Q:Z

    return p1
.end method

.method static synthetic c(Lcom/samsung/android/app/accesscontrol/n;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    return v0
.end method

.method private c(II)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setX(F)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setY(F)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/samsung/android/app/accesscontrol/n;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    return v0
.end method

.method static synthetic e(Lcom/samsung/android/app/accesscontrol/n;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    return v0
.end method

.method static synthetic f(Lcom/samsung/android/app/accesscontrol/n;)Lcom/samsung/android/app/accesscontrol/s;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->P:Lcom/samsung/android/app/accesscontrol/s;

    return-object v0
.end method

.method private f()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "AccessControlView"

    const-string v1, "Init()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    const v1, -0xff5a3c

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->h:Landroid/content/Context;

    new-instance v2, Lcom/samsung/android/app/accesscontrol/r;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/accesscontrol/r;-><init>(Lcom/samsung/android/app/accesscontrol/n;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->O:Landroid/view/GestureDetector;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->d:I

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->d:I

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v1, "save_cbselectall_port_preference"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    const v1, 0x7f07000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->P:Lcom/samsung/android/app/accesscontrol/s;

    iget-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    invoke-interface {v0, v1}, Lcom/samsung/android/app/accesscontrol/s;->a(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/n;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f020000

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->n:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->n:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->H:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->n:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->I:I

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/n;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020002

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->J:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->K:I

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/n;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020001

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->L:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->M:I

    iput-boolean v4, p0, Lcom/samsung/android/app/accesscontrol/n;->r:Z

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->h:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->I:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->H:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    const-string v1, "Remove Button"

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    new-instance v1, Lcom/samsung/android/app/accesscontrol/o;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/accesscontrol/o;-><init>(Lcom/samsung/android/app/accesscontrol/n;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->T:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/n;->g()V

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/n;->h()V

    return-void

    :cond_0
    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v1, "save_cbselectall_land_preference"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    const v1, 0x7f070002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    goto/16 :goto_0
.end method

.method static synthetic g(Lcom/samsung/android/app/accesscontrol/n;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    return v0
.end method

.method private g()V
    .locals 6

    const-string v0, "AccessControlView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GetRectData, IsRectDataEmpty : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v2}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->h()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->h()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    new-instance v4, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v5, p0, Lcom/samsung/android/app/accesscontrol/n;->h:Landroid/content/Context;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-direct {v4, v5, v0}, Lcom/samsung/android/app/accesscontrol/l;-><init>(Landroid/content/Context;Landroid/graphics/Rect;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    :cond_1
    return-void
.end method

.method static synthetic h(Lcom/samsung/android/app/accesscontrol/n;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method private h()V
    .locals 8

    const v7, 0x7f07000b

    const/4 v6, 0x1

    const-string v0, "AccessControlView"

    const-string v1, "SetStatusBarHeight()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->l()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->B:I

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->B:I

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d()I

    move-result v2

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tblte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "tbelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SC-01G"

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "SCL24"

    const-string v3, "ro.product.name"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->h:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d()I

    move-result v0

    if-ne v0, v6, :cond_1

    iget v0, v4, Landroid/graphics/Point;->y:I

    iget v1, v3, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    move v1, v0

    :cond_1
    if-eqz v2, :cond_2

    const/4 v0, 0x2

    if-ne v2, v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    const v2, 0x7f07000d

    invoke-virtual {v0, v2, v6, v6}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    invoke-virtual {v2, v7, v6, v6}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v2

    div-float/2addr v0, v2

    move v5, v0

    :goto_0
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    int-to-float v1, v1

    mul-float/2addr v1, v5

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int v3, v0, v1

    iget-object v6, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->h:Landroid/content/Context;

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget v7, p0, Lcom/samsung/android/app/accesscontrol/n;->B:I

    int-to-float v7, v7

    mul-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    add-int/2addr v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/accesscontrol/l;-><init>(Landroid/content/Context;IIII)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    :cond_3
    return-void

    :cond_4
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "tblte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "tbelte"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "SC-01G"

    const-string v2, "ro.product.name"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "SCL24"

    const-string v2, "ro.product.name"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    const v2, 0x7f070004

    invoke-virtual {v0, v2, v6, v6}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    invoke-virtual {v2, v7, v6, v6}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v2

    div-float/2addr v0, v2

    move v5, v0

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    const v2, 0x7f070003

    invoke-virtual {v0, v2, v6, v6}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->U:Landroid/content/res/Resources;

    invoke-virtual {v2, v7, v6, v6}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v2

    div-float/2addr v0, v2

    move v5, v0

    goto/16 :goto_0
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/l;->a()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/l;->a()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->n:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->n:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->n:Landroid/graphics/Bitmap;

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->o:Landroid/graphics/Bitmap;

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->p:Landroid/graphics/Bitmap;

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->j:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    if-eqz v0, :cond_5

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->j:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->T:Landroid/view/ViewGroup;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->T:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->T:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->T:Landroid/view/ViewGroup;

    :cond_6
    return-void
.end method

.method public a(Z)V
    .locals 7

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/n;->i()V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    iget-object v6, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->h:Landroid/content/Context;

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget v5, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/accesscontrol/l;-><init>(Landroid/content/Context;IIII)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/n;->invalidate()V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    goto :goto_0
.end method

.method public a(ZFFFF)V
    .locals 7

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz p1, :cond_0

    iget-object v6, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->h:Landroid/content/Context;

    float-to-int v2, p2

    float-to-int v3, p3

    float-to-int v4, p4

    float-to-int v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/app/accesscontrol/l;-><init>(Landroid/content/Context;IIII)V

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->r:Z

    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->q:I

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/n;->invalidate()V

    return-void
.end method

.method public b()V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/n;->i()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->f()V

    return-void
.end method

.method public c()V
    .locals 4

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/l;->a:Landroid/graphics/Rect;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->d:I

    if-nez v1, :cond_1

    const-string v1, "save_cbselectall_port_preference"

    iget-boolean v3, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :goto_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a(Ljava/util/ArrayList;)V

    return-void

    :cond_1
    const-string v1, "save_cbselectall_land_preference"

    iget-boolean v3, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    goto :goto_1
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->r:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    return-void
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->q:I

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/n;->invalidate()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->q:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/l;->a:Landroid/graphics/Rect;

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/app/accesscontrol/n;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;)V

    :goto_0
    return-void

    :pswitch_0
    move v1, v6

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/l;->a:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    :goto_2
    if-ge v6, v7, :cond_0

    if-nez v6, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v1, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    :goto_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    add-int/lit8 v1, v6, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v1, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    add-int/lit8 v2, v6, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v2, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v3, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v4, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iget-object v5, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_3

    :pswitch_1
    move v1, v6

    :goto_4
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/l;->a:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v7, v6

    :goto_5
    if-ge v7, v8, :cond_0

    add-int/lit8 v0, v8, -0x1

    if-ne v7, v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v1, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v2, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v3, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v4, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iget-object v5, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :goto_6
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_5

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v1, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v2, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    add-int/lit8 v3, v7, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v3, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    add-int/lit8 v4, v7, 0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v4, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iget-object v5, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_6

    :goto_7
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/l;->a:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_7

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->S:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v4, 0x2

    const/4 v3, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->O:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->k:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->r:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->N:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v7

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    move v0, v7

    goto :goto_0

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->D:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->E:F

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->D:F

    float-to-int v0, v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->E:F

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/accesscontrol/n;->b(II)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/n;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->i:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_6

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->D:F

    float-to-int v0, v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->E:F

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/accesscontrol/n;->a(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->w:I

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->w:I

    if-eq v0, v3, :cond_5

    iput-boolean v8, p0, Lcom/samsung/android/app/accesscontrol/n;->s:Z

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->w:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    if-ne v0, v1, :cond_2

    iput-boolean v7, p0, Lcom/samsung/android/app/accesscontrol/n;->s:Z

    iput v4, p0, Lcom/samsung/android/app/accesscontrol/n;->q:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/l;->a:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    if-le v0, v1, :cond_3

    iput-boolean v7, p0, Lcom/samsung/android/app/accesscontrol/n;->Q:Z

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    if-le v0, v1, :cond_4

    iput-boolean v7, p0, Lcom/samsung/android/app/accesscontrol/n;->R:Z

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->F:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->G:F

    goto :goto_1

    :cond_5
    iput-boolean v8, p0, Lcom/samsung/android/app/accesscontrol/n;->s:Z

    iput v8, p0, Lcom/samsung/android/app/accesscontrol/n;->q:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/android/app/accesscontrol/Vertex;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_6
    const-string v0, "AccessControlView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " mBtnActionMode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    invoke-virtual {v2}, Lcom/samsung/android/app/accesscontrol/q;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->a:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_7

    iput-boolean v7, p0, Lcom/samsung/android/app/accesscontrol/n;->s:Z

    move v0, v7

    goto/16 :goto_0

    :cond_7
    iput-boolean v7, p0, Lcom/samsung/android/app/accesscontrol/n;->s:Z

    iput v4, p0, Lcom/samsung/android/app/accesscontrol/n;->q:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/l;->a:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->F:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->G:F

    goto/16 :goto_1

    :pswitch_1
    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->s:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->a:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_8

    move v0, v8

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/n;->s:Z

    if-eqz v2, :cond_29

    sget-object v2, Lcom/samsung/android/app/accesscontrol/p;->a:[I

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    invoke-virtual {v3}, Lcom/samsung/android/app/accesscontrol/q;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    :cond_9
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/n;->invalidate()V

    goto/16 :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    float-to-int v3, v0

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    float-to-int v4, v1

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v2, v4, :cond_c

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    sub-int/2addr v2, v4

    iput v2, v0, Landroid/graphics/Rect;->left:I

    :cond_a
    :goto_3
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v3, v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    :cond_b
    :goto_4
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    goto :goto_2

    :cond_c
    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    if-ge v0, v2, :cond_a

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iput v2, v0, Landroid/graphics/Rect;->left:I

    goto :goto_3

    :cond_d
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    if-ge v0, v1, :cond_b

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto :goto_4

    :pswitch_3
    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    float-to-int v3, v0

    sub-int/2addr v2, v3

    float-to-int v3, v1

    iget-object v4, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v2, v4, :cond_10

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    sub-int/2addr v2, v4

    iput v2, v0, Landroid/graphics/Rect;->left:I

    :cond_e
    :goto_5
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v3, v0, :cond_11

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :cond_f
    :goto_6
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    :cond_10
    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    if-ge v0, v2, :cond_e

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iput v2, v0, Landroid/graphics/Rect;->left:I

    goto :goto_5

    :cond_11
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    if-le v0, v1, :cond_f

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_6

    :pswitch_4
    float-to-int v2, v0

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->F:F

    float-to-int v3, v3

    sub-int/2addr v2, v3

    float-to-int v3, v1

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->G:F

    float-to-int v4, v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v2, v4, :cond_14

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    add-int/2addr v2, v4

    iput v2, v0, Landroid/graphics/Rect;->right:I

    :cond_12
    :goto_7
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v3, v0, :cond_15

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :cond_13
    :goto_8
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    :cond_14
    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    if-le v0, v2, :cond_12

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iput v2, v0, Landroid/graphics/Rect;->right:I

    goto :goto_7

    :cond_15
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    if-le v0, v1, :cond_13

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_8

    :pswitch_5
    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    float-to-int v2, v0

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v1, v2, :cond_17

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    :cond_16
    :goto_9
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    :cond_17
    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v0, v0

    iput v0, v1, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    if-ge v0, v1, :cond_16

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    goto :goto_9

    :pswitch_6
    float-to-int v1, v0

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->F:F

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v1, v2, :cond_19

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    :cond_18
    :goto_a
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    :cond_19
    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v0, v0

    iput v0, v1, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    if-le v0, v1, :cond_18

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_a

    :pswitch_7
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    float-to-int v2, v1

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v0, v2, :cond_1b

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    :cond_1a
    :goto_b
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    :cond_1b
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    if-ge v0, v1, :cond_1a

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto :goto_b

    :pswitch_8
    float-to-int v0, v1

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->G:F

    float-to-int v2, v2

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    if-ge v0, v2, :cond_1d

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :cond_1c
    :goto_c
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    :cond_1d
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    if-le v0, v1, :cond_1c

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_c

    :pswitch_9
    float-to-int v2, v0

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->D:F

    float-to-int v3, v3

    sub-int/2addr v2, v3

    float-to-int v3, v1

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->E:F

    float-to-int v4, v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v5, v2

    iput v5, v4, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v3

    iput v5, v4, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v5

    iput v2, v4, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->D:F

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/n;->E:F

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    if-ge v0, v1, :cond_1f

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->Q:Z

    if-eqz v0, :cond_25

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_1e

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    :cond_1e
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    :cond_1f
    :goto_d
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    if-ge v0, v1, :cond_21

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->R:Z

    if-eqz v0, :cond_26

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_20

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :cond_20
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    :cond_21
    :goto_e
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    if-le v0, v1, :cond_23

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->Q:Z

    if-eqz v0, :cond_27

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_22

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    :cond_22
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    :cond_23
    :goto_f
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    if-le v0, v1, :cond_9

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->R:Z

    if-eqz v0, :cond_28

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_24

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    :cond_24
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->m:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/l;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/l;->a(Landroid/graphics/Rect;)V

    goto/16 :goto_2

    :cond_25
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    goto/16 :goto_d

    :cond_26
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto/16 :goto_e

    :cond_27
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    goto :goto_f

    :cond_28
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->u:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_2

    :cond_29
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->w:I

    if-ne v0, v3, :cond_9

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/android/app/accesscontrol/Vertex;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :pswitch_a
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/q;->a:Lcom/samsung/android/app/accesscontrol/q;

    if-ne v0, v1, :cond_2b

    const-string v0, "AccessControlView"

    const-string v1, "ACTION_UP, BTN_ACTION_CLOSE_MODE"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2a
    :goto_10
    sget-object v0, Lcom/samsung/android/app/accesscontrol/q;->j:Lcom/samsung/android/app/accesscontrol/q;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->C:Lcom/samsung/android/app/accesscontrol/q;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/n;->invalidate()V

    iput-boolean v8, p0, Lcom/samsung/android/app/accesscontrol/n;->R:Z

    iput-boolean v8, p0, Lcom/samsung/android/app/accesscontrol/n;->Q:Z

    goto/16 :goto_1

    :cond_2b
    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/n;->s:Z

    if-nez v0, :cond_2a

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->w:I

    if-eq v0, v3, :cond_2c

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/n;->w:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/n;->v:I

    :cond_2c
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/n;->j:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/n;->l:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/n;->c:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/n;->x:I

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/n;->y:I

    iget v5, p0, Lcom/samsung/android/app/accesscontrol/n;->z:I

    iget v6, p0, Lcom/samsung/android/app/accesscontrol/n;->A:I

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->StartAnimation(Ljava/util/ArrayList;IIIII)V

    goto :goto_10

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_a
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
