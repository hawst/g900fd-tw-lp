.class Lcom/samsung/android/app/accesscontrol/k;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/samsung/android/app/accesscontrol/j;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/accesscontrol/j;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/k;->a:Lcom/samsung/android/app/accesscontrol/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/k;->a:Lcom/samsung/android/app/accesscontrol/j;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/samsung/android/app/accesscontrol/j;->a(Lcom/samsung/android/app/accesscontrol/j;F)F

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/k;->a:Lcom/samsung/android/app/accesscontrol/j;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/j;->a(Lcom/samsung/android/app/accesscontrol/j;)F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/k;->a:Lcom/samsung/android/app/accesscontrol/j;

    invoke-static {v1}, Lcom/samsung/android/app/accesscontrol/j;->b(Lcom/samsung/android/app/accesscontrol/j;)F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/k;->a:Lcom/samsung/android/app/accesscontrol/j;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/j;->c(Lcom/samsung/android/app/accesscontrol/j;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "statusbar"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapsePanels()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
