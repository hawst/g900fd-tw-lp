.class Lcom/samsung/android/app/accesscontrol/g;
.super Landroid/os/Handler;


# instance fields
.field final synthetic a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/g;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    const-string v0, "AccessControlMainService"

    const-string v1, "Unkown message !!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    const-class v2, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/g;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v1, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    const-string v0, "AccessControlMainService"

    const-string v1, "RECEIVE_MESSAGE - MSG_CREATE_GUIDED_VIEW"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/g;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
