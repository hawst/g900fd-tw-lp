.class public Lcom/samsung/android/app/accesscontrol/RectAnimator;
.super Ljava/lang/Object;


# instance fields
.field private final ANIMATION_DRAW:I

.field private final ANIMATION_TIME:I

.field private final RECTANGLE_LINE_BOTTOM:I

.field private final RECTANGLE_LINE_LEFT:I

.field private final RECTANGLE_LINE_RIGHT:I

.field private final RECTANGLE_LINE_TOP:I

.field private final STARTING_QUADRANT_FOUR:I

.field private final STARTING_QUADRANT_ONE:I

.field private final STARTING_QUADRANT_THREE:I

.field private final STARTING_QUADRANT_TWO:I

.field private final mAnimationHandler:Landroid/os/Handler;

.field private mBottom:F

.field private mCaptureViewBottom:I

.field private mCaptureViewLeft:I

.field private mCaptureViewRight:I

.field private mCaptureViewTop:I

.field private mIsClockwise:Z

.field private mIsCompleteAniX:Z

.field private mIsCompleteAniY:Z

.field private mLeft:F

.field private mListener:Lcom/samsung/android/app/accesscontrol/z;

.field private mMinSize:I

.field private mPointAnimationEnd:I

.field private mQuadrant:I

.field private mRectPositionList:Ljava/util/ArrayList;

.field private mRight:F

.field private mTop:F


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/accesscontrol/z;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->ANIMATION_TIME:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->ANIMATION_DRAW:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->STARTING_QUADRANT_ONE:I

    iput v2, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->STARTING_QUADRANT_TWO:I

    iput v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->STARTING_QUADRANT_THREE:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->STARTING_QUADRANT_FOUR:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->RECTANGLE_LINE_TOP:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->RECTANGLE_LINE_RIGHT:I

    iput v2, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->RECTANGLE_LINE_BOTTOM:I

    iput v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->RECTANGLE_LINE_LEFT:I

    iput-object v4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mListener:Lcom/samsung/android/app/accesscontrol/z;

    new-instance v0, Lcom/samsung/android/app/accesscontrol/w;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/accesscontrol/w;-><init>(Lcom/samsung/android/app/accesscontrol/RectAnimator;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mAnimationHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mListener:Lcom/samsung/android/app/accesscontrol/z;

    return-void
.end method

.method private DetectDrawStyle(Ljava/util/ArrayList;)V
    .locals 10

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    add-float/2addr v0, v3

    div-float v3, v0, v5

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    add-float/2addr v0, v4

    div-float v4, v0, v5

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    cmpg-float v0, v3, v0

    if-gez v0, :cond_2

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    cmpl-float v0, v4, v0

    if-lez v0, :cond_1

    iput v7, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mQuadrant:I

    :goto_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v0, v2

    :goto_1
    if-ge v0, v1, :cond_7

    if-nez v0, :cond_4

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x4

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mQuadrant:I

    goto :goto_0

    :cond_2
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    cmpl-float v0, v4, v0

    if-lez v0, :cond_3

    iput v8, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mQuadrant:I

    goto :goto_0

    :cond_3
    iput v9, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mQuadrant:I

    goto :goto_0

    :cond_4
    div-int/lit8 v3, v1, 0x4

    if-ne v0, v3, :cond_5

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    div-int/lit8 v3, v1, 0x4

    mul-int/lit8 v3, v3, 0x2

    if-ne v0, v3, :cond_6

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    div-int/lit8 v3, v1, 0x4

    mul-int/lit8 v3, v3, 0x3

    if-ne v0, v3, :cond_0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    move v1, v2

    move v3, v2

    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_e

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v5, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    invoke-direct {p0, v5, v0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->DetectNearbyLine(FF)I

    move-result v5

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_9

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v6, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    invoke-direct {p0, v6, v0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->DetectNearbyLine(FF)I

    move-result v0

    :goto_4
    packed-switch v5, :pswitch_data_0

    :cond_8
    :goto_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_9
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v6, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    invoke-direct {p0, v6, v0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->DetectNearbyLine(FF)I

    move-result v0

    goto :goto_4

    :pswitch_0
    if-ne v0, v7, :cond_a

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_a
    if-ne v0, v9, :cond_8

    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    :pswitch_1
    if-ne v0, v8, :cond_b

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_b
    if-nez v0, :cond_8

    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    :pswitch_2
    if-ne v0, v9, :cond_c

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_c
    if-ne v0, v7, :cond_8

    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    :pswitch_3
    if-nez v0, :cond_d

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_d
    if-ne v0, v8, :cond_8

    add-int/lit8 v3, v3, -0x1

    goto :goto_5

    :cond_e
    if-ltz v3, :cond_f

    iput-boolean v7, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsClockwise:Z

    :goto_6
    return-void

    :cond_f
    iput-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsClockwise:Z

    goto :goto_6

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private DetectNearbyLine(FF)I
    .locals 7

    const/4 v2, 0x0

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    sub-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v0, v5, v4

    if-lez v0, :cond_6

    move v0, v4

    :goto_0
    cmpl-float v6, v0, v3

    if-lez v6, :cond_0

    move v0, v3

    :cond_0
    cmpl-float v6, v0, v1

    if-lez v6, :cond_1

    move v0, v1

    :cond_1
    invoke-direct {p0, v0, v5}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->EqualFloat(FF)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v2

    :goto_1
    return v0

    :cond_2
    invoke-direct {p0, v0, v4}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->EqualFloat(FF)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    invoke-direct {p0, v0, v3}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->EqualFloat(FF)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v0, 0x2

    goto :goto_1

    :cond_4
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->EqualFloat(FF)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    move v0, v5

    goto :goto_0
.end method

.method private EqualFloat(FF)Z
    .locals 2

    sub-float v0, p1, p2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x358637bd    # 1.0E-6f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private FinishAnimation()V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mListener:Lcom/samsung/android/app/accesscontrol/z;

    const/4 v1, 0x1

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v5, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/app/accesscontrol/z;->a(ZFFFF)V

    return-void
.end method

.method private SetRectanglePoint(Ljava/util/ArrayList;)V
    .locals 15

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v5, 0x0

    div-int/lit8 v1, v7, 0x4

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    sub-float v8, v0, v2

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    sub-float v9, v0, v2

    rem-int/lit8 v0, v7, 0x4

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    add-int/lit8 v0, v1, 0x1

    move v2, v1

    move v3, v0

    move v0, v1

    :goto_0
    const/4 v4, 0x0

    move v6, v4

    :goto_1
    if-ge v6, v7, :cond_1c

    iget-boolean v4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsClockwise:Z

    if-eqz v4, :cond_f

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mQuadrant:I

    packed-switch v4, :pswitch_data_0

    :goto_2
    move v4, v5

    :cond_0
    :goto_3
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v4

    goto :goto_1

    :cond_1
    rem-int/lit8 v0, v7, 0x4

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v0, v1, 0x1

    move v3, v2

    move v2, v0

    move v0, v1

    goto :goto_0

    :cond_2
    rem-int/lit8 v0, v7, 0x4

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1d

    add-int/lit8 v3, v1, 0x1

    add-int/lit8 v2, v1, 0x1

    add-int/lit8 v0, v1, 0x1

    goto :goto_0

    :pswitch_0
    if-ge v6, v3, :cond_3

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v3

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    add-float/2addr v5, v13

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v3, v4, :cond_0

    const/4 v4, 0x0

    goto :goto_3

    :cond_3
    add-int v4, v3, v2

    if-ge v6, v4, :cond_4

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v2

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    sub-float v5, v12, v5

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v2, v4, :cond_0

    const/4 v4, 0x0

    goto :goto_3

    :cond_4
    add-int v4, v3, v2

    add-int/2addr v4, v0

    if-ge v6, v4, :cond_5

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v0

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    sub-float v5, v13, v5

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v4, :cond_0

    const/4 v4, 0x0

    goto :goto_3

    :cond_5
    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v1

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    add-float/2addr v5, v12

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :pswitch_1
    if-ge v6, v3, :cond_6

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v3

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    add-float/2addr v5, v12

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v3, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_6
    add-int v4, v3, v2

    if-ge v6, v4, :cond_7

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v2

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    add-float/2addr v5, v13

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v2, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_7
    add-int v4, v3, v2

    add-int/2addr v4, v0

    if-ge v6, v4, :cond_8

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v0

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    sub-float v5, v12, v5

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_8
    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v1

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    sub-float v5, v13, v5

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :pswitch_2
    if-ge v6, v3, :cond_9

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v3

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    sub-float v5, v13, v5

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v3, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_9
    add-int v4, v3, v2

    if-ge v6, v4, :cond_a

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v2

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    add-float/2addr v5, v12

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v2, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_a
    add-int v4, v3, v2

    add-int/2addr v4, v0

    if-ge v6, v4, :cond_b

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v0

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    add-float/2addr v5, v13

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_b
    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v1

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    sub-float v5, v12, v5

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :pswitch_3
    if-ge v6, v3, :cond_c

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v3

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    sub-float v5, v12, v5

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v3, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_c
    add-int v4, v3, v2

    if-ge v6, v4, :cond_d

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v2

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    sub-float v5, v13, v5

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v2, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_d
    add-int v4, v3, v2

    add-int/2addr v4, v0

    if-ge v6, v4, :cond_e

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v0

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    add-float/2addr v5, v12

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_e
    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v1

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    add-float/2addr v5, v13

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_f
    iget v4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mQuadrant:I

    packed-switch v4, :pswitch_data_1

    goto/16 :goto_2

    :pswitch_4
    if-ge v6, v3, :cond_10

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v3

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    sub-float v5, v12, v5

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v3, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_10
    add-int v4, v3, v2

    if-ge v6, v4, :cond_11

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v2

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    add-float/2addr v5, v13

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v2, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_11
    add-int v4, v3, v2

    add-int/2addr v4, v0

    if-ge v6, v4, :cond_12

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v0

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    add-float/2addr v5, v12

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_12
    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v1

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    sub-float v5, v13, v5

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :pswitch_5
    if-ge v6, v3, :cond_13

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v3

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    add-float/2addr v5, v13

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v3, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_13
    add-int v4, v3, v2

    if-ge v6, v4, :cond_14

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v2

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    add-float/2addr v5, v12

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v2, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_14
    add-int v4, v3, v2

    add-int/2addr v4, v0

    if-ge v6, v4, :cond_15

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v0

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    sub-float v5, v13, v5

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_15
    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v1

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    sub-float v5, v12, v5

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :pswitch_6
    if-ge v6, v3, :cond_16

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v3

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    add-float/2addr v5, v12

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v3, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_16
    add-int v4, v3, v2

    if-ge v6, v4, :cond_17

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v2

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    sub-float v5, v13, v5

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v2, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_17
    add-int v4, v3, v2

    add-int/2addr v4, v0

    if-ge v6, v4, :cond_18

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v0

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    sub-float v5, v12, v5

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_18
    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v1

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    add-float/2addr v5, v13

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :pswitch_7
    if-ge v6, v3, :cond_19

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v3

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    sub-float v5, v13, v5

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v3, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_19
    add-int v4, v3, v2

    if-ge v6, v4, :cond_1a

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v2

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    sub-float v5, v12, v5

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v2, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_1a
    add-int v4, v3, v2

    add-int/2addr v4, v0

    if-ge v6, v4, :cond_1b

    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v13, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v14, v0

    div-float v14, v9, v14

    mul-float/2addr v5, v14

    add-float/2addr v5, v13

    invoke-direct {v11, v12, v5}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v4, :cond_0

    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_1b
    iget-object v10, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    new-instance v11, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    add-int/lit8 v4, v5, 0x1

    int-to-float v5, v5

    int-to-float v13, v1

    div-float v13, v8, v13

    mul-float/2addr v5, v13

    add-float/2addr v5, v12

    iget v12, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-direct {v11, v5, v12}, Lcom/samsung/android/app/accesscontrol/Vertex;-><init>(FF)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_1c
    return-void

    :cond_1d
    move v0, v1

    move v2, v1

    move v3, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private SetRectangleTwoPoint(Ljava/util/ArrayList;)Z
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x4

    if-ge v4, v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_5

    iget v5, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    cmpl-float v0, v5, v0

    if-lez v0, :cond_3

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    :cond_1
    :goto_2
    iget v5, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    cmpl-float v0, v5, v0

    if-lez v0, :cond_4

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    :cond_2
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    iget v5, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    cmpg-float v0, v5, v0

    if-gez v0, :cond_1

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    goto :goto_2

    :cond_4
    iget v5, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    cmpg-float v0, v5, v0

    if-gez v0, :cond_2

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    goto :goto_3

    :cond_5
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewLeft:I

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_6

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewLeft:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    :cond_6
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewTop:I

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_7

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewTop:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    :cond_7
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewRight:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_8

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewRight:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    :cond_8
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewBottom:I

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-lez v0, :cond_9

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewBottom:I

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    :cond_9
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mMinSize:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRight:F

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mLeft:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    if-gt v0, v3, :cond_a

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mMinSize:I

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mBottom:F

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mTop:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    if-le v0, v3, :cond_b

    :cond_a
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    move v0, v1

    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto/16 :goto_0
.end method

.method private ShapeTweeningAnimation(Ljava/util/ArrayList;)V
    .locals 14

    const-wide/16 v12, 0x1f4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    mul-int/lit8 v0, v3, 0x2

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mPointAnimationEnd:I

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mListener:Lcom/samsung/android/app/accesscontrol/z;

    invoke-interface {v0}, Lcom/samsung/android/app/accesscontrol/z;->d()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mAnimationHandler:Landroid/os/Handler;

    invoke-virtual {v0, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iput-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsCompleteAniX:Z

    iput-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsCompleteAniY:Z

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    const-string v7, "X"

    new-array v8, v10, [F

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    aput v0, v8, v2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    aput v0, v8, v9

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    new-instance v6, Lcom/samsung/android/app/accesscontrol/x;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/accesscontrol/x;-><init>(Lcom/samsung/android/app/accesscontrol/RectAnimator;)V

    invoke-virtual {v0, v6}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    const-string v7, "Y"

    new-array v8, v10, [F

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    aput v0, v8, v2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/Vertex;

    iget v0, v0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    aput v0, v8, v9

    invoke-static {v6, v7, v8}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0, v12, v13}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    new-instance v6, Lcom/samsung/android/app/accesscontrol/y;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/accesscontrol/y;-><init>(Lcom/samsung/android/app/accesscontrol/RectAnimator;)V

    invoke-virtual {v0, v6}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/accesscontrol/RectAnimator;)Lcom/samsung/android/app/accesscontrol/z;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mListener:Lcom/samsung/android/app/accesscontrol/z;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/accesscontrol/RectAnimator;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mAnimationHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/accesscontrol/RectAnimator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsCompleteAniX:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/app/accesscontrol/RectAnimator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsCompleteAniX:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/accesscontrol/RectAnimator;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mPointAnimationEnd:I

    return v0
.end method

.method static synthetic access$310(Lcom/samsung/android/app/accesscontrol/RectAnimator;)I
    .locals 2

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mPointAnimationEnd:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mPointAnimationEnd:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/accesscontrol/RectAnimator;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsCompleteAniY:Z

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/android/app/accesscontrol/RectAnimator;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsCompleteAniY:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/android/app/accesscontrol/RectAnimator;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->FinishAnimation()V

    return-void
.end method


# virtual methods
.method public StartAnimation(Ljava/util/ArrayList;IIIII)V
    .locals 6

    const/4 v2, 0x0

    iput p2, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mMinSize:I

    iput p3, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewLeft:I

    iput p4, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewTop:I

    iput p5, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewRight:I

    iput p6, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mCaptureViewBottom:I

    invoke-direct {p0, p1}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->SetRectangleTwoPoint(Ljava/util/ArrayList;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mRectPositionList:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->DetectDrawStyle(Ljava/util/ArrayList;)V

    invoke-direct {p0, p1}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->SetRectanglePoint(Ljava/util/ArrayList;)V

    invoke-direct {p0, p1}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->ShapeTweeningAnimation(Ljava/util/ArrayList;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/RectAnimator;->mListener:Lcom/samsung/android/app/accesscontrol/z;

    const/4 v1, 0x0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/app/accesscontrol/z;->a(ZFFFF)V

    goto :goto_0
.end method
