.class public Lcom/samsung/android/app/accesscontrol/AccessControlMainService;
.super Landroid/app/Service;

# interfaces
.implements Lcom/samsung/android/app/accesscontrol/v;


# static fields
.field public static a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

.field public static b:Landroid/content/SharedPreferences;

.field public static c:I

.field public static d:I

.field public static e:I

.field public static f:I


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private D:I

.field private E:I

.field private F:I

.field private G:Lcom/samsung/android/app/accesscontrol/i;

.field private H:Landroid/graphics/Bitmap;

.field private I:I

.field private J:I

.field private final K:I

.field private final L:I

.field private M:I

.field private N:Landroid/os/Handler;

.field private final O:Landroid/content/BroadcastReceiver;

.field g:Landroid/content/Context;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:I

.field private final k:I

.field private l:Lcom/samsung/android/app/accesscontrol/t;

.field private m:Ljava/util/ArrayList;

.field private n:Ljava/util/ArrayList;

.field private o:Landroid/app/StatusBarManager;

.field private p:Landroid/app/NotificationManager;

.field private q:Landroid/app/KeyguardManager;

.field private r:Lcom/samsung/android/app/accesscontrol/j;

.field private s:I

.field private t:Landroid/os/PowerManager$WakeLock;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    sput v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c:I

    sput v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d:I

    sput v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->e:I

    sput v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->f:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const-string v0, "AccessControlMainService"

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->h:Ljava/lang/String;

    const-string v0, "coverOpen"

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->i:Ljava/lang/String;

    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->j:I

    iput v3, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->k:I

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->l:Lcom/samsung/android/app/accesscontrol/t;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->r:Lcom/samsung/android/app/accesscontrol/j;

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->s:I

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->t:Landroid/os/PowerManager$WakeLock;

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->u:Z

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->v:Z

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->w:Z

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->x:Z

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->y:Z

    iput-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->z:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->B:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->C:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->D:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->F:I

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->G:Lcom/samsung/android/app/accesscontrol/i;

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->I:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->J:I

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->K:I

    iput v3, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->L:I

    iput v3, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->M:I

    new-instance v0, Lcom/samsung/android/app/accesscontrol/g;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/accesscontrol/g;-><init>(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->N:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/app/accesscontrol/h;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/accesscontrol/h;-><init>(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->O:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(II)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/app/accesscontrol/a;->a(II)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->q()V

    return-void
.end method

.method static synthetic b(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)Lcom/samsung/android/app/accesscontrol/j;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->r:Lcom/samsung/android/app/accesscontrol/j;

    return-object v0
.end method

.method private b(I)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "air_motion_engine"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.gesture.AIR_MOTION_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-ne p1, v2, :cond_0

    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string v1, "isEnable"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private b(Ljava/util/ArrayList;)V
    .locals 8

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_4

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    sget v6, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c:I

    if-ge v1, v6, :cond_5

    sget v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    move v1, v2

    :goto_1
    iget v6, v0, Landroid/graphics/Rect;->top:I

    sget v7, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d:I

    if-ge v6, v7, :cond_0

    sget v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    move v1, v2

    :cond_0
    iget v6, v0, Landroid/graphics/Rect;->right:I

    sget v7, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->e:I

    if-le v6, v7, :cond_1

    sget v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->e:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    move v1, v2

    :cond_1
    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    sget v7, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->f:I

    if-le v6, v7, :cond_2

    sget v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->f:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    move v1, v2

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p1, v4, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const-string v0, "AccessControlMainService"

    const-string v1, "CheckValidData, invalid data is update!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_4
    return-void

    :cond_5
    move v1, v3

    goto :goto_1
.end method

.method static synthetic c(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)Landroid/app/KeyguardManager;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->q:Landroid/app/KeyguardManager;

    return-object v0
.end method

.method private c(I)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "master_motion"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v1, "motion_engine"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.motions.MOTIONS_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-ne p1, v2, :cond_0

    const-string v1, "isEnable"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string v1, "isEnable"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic d(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    return v0
.end method

.method private d(I)V
    .locals 2

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "surface_motion_engine"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "motion_overturn"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "surface_palm_touch"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "surface_palm_swipe"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void
.end method

.method private m()V
    .locals 6

    const/16 v5, 0xff

    const/4 v3, 0x1

    const/4 v4, 0x0

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v2, "use_motions_preference"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    if-ne v0, v5, :cond_0

    iput v4, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    const-string v0, "use_motions_preference"

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/aa;->a(Landroid/content/ContentResolver;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->v:Z

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/aa;->b(Landroid/content/ContentResolver;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->w:Z

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "master_motion"

    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->B:I

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "air_motion_engine"

    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->C:I

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "surface_motion_engine"

    invoke-static {v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->D:I

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "accessibility_display_magnification_enabled"

    invoke-static {v0, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->z:Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->v:Z

    if-eqz v0, :cond_2

    const-string v0, "use_auto_rotate_screen_preference"

    iget-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->v:Z

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/samsung/android/app/accesscontrol/aa;->a(Landroid/content/ContentResolver;Z)V

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->w:Z

    if-eqz v0, :cond_3

    const-string v0, "use_auto_rotate_screen_second_preference"

    iget-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->w:Z

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/samsung/android/app/accesscontrol/aa;->b(Landroid/content/ContentResolver;Z)V

    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->z:Z

    if-eqz v0, :cond_4

    const-string v0, "accessibility_display_magnification_enabled"

    iget-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->z:Z

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "accessibility_display_magnification_enabled"

    invoke-static {v0, v2, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    :cond_4
    const-string v0, "master_motion"

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->B:I

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v0, "air_motion_engine"

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->C:I

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v0, "surface_motion_engine"

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->D:I

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    invoke-static {v0, v1}, Lcom/samsung/android/app/accesscontrol/aa;->a(Landroid/content/ContentResolver;I)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->N:Landroid/os/Handler;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private n()V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->q()V

    return-void
.end method

.method private o()V
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    const-string v0, "AccessControlMainService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "GetBackGroundScreen(), mOrientation = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    if-nez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->I:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->J:I

    invoke-static {v0, v1}, Landroid/view/SurfaceControl;->screenshot(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->J:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->I:I

    invoke-static {v0, v2}, Landroid/view/SurfaceControl;->screenshot(II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    if-ne v0, v6, :cond_3

    const/high16 v0, -0x3d4c0000    # -90.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4

    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_4
    const/high16 v0, 0x43340000    # 180.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->postRotate(F)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private p()I
    .locals 5

    const/4 v1, 0x2

    const/4 v2, 0x1

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    if-eqz v0, :cond_0

    if-ne v0, v1, :cond_1

    :cond_0
    iget v4, v3, Landroid/content/res/Configuration;->orientation:I

    if-eq v4, v1, :cond_3

    :cond_1
    if-eq v0, v2, :cond_2

    const/4 v4, 0x3

    if-ne v0, v4, :cond_4

    :cond_2
    iget v0, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v2, :cond_4

    :cond_3
    move v0, v1

    :goto_0
    return v0

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method private q()V
    .locals 7

    const v6, 0x7f080013

    const/4 v2, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->i()V

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v0, "last_svc_mode"

    const/4 v1, 0x2

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v0, "AccessControlMainService"

    const-string v1, "Savet last mode As Block Mode!!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    new-instance v4, Lcom/samsung/android/app/accesscontrol/a;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    invoke-direct {v4, p0, v0}, Lcom/samsung/android/app/accesscontrol/a;-><init>(Landroid/content/Context;Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "access_control_enabled"

    invoke-static {v0, v1, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iput-boolean v5, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->y:Z

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v1, "use_motions_preference"

    const/16 v4, 0xff

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    invoke-direct {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c(I)V

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    invoke-direct {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b(I)V

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    invoke-direct {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d(I)V

    const-string v0, "TEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "View Create : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->E:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/app/Notification$Builder;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-direct {v0, v1}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f020004

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v4, 0x7f080000

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {p0, v5, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->startForeground(ILandroid/app/Notification;)V

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "multi_window_enabled"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    if-ne v0, v5, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->x:Z
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->v:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->w:Z

    if-eqz v1, :cond_7

    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->z:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    :goto_3
    iget-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->x:Z

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "multi_window_enabled"

    invoke-static {v1, v4, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v1, "multi_window_preference"

    iget-boolean v4, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->x:Z

    invoke-interface {v3, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_5
    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->g:Landroid/content/Context;

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_7
    iget-boolean v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->z:Z

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method

.method private r()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->G:Lcom/samsung/android/app/accesscontrol/i;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/i;->a(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private s()V
    .locals 3

    const-string v1, "AccessControlMainService"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LoadRectDataFromFile, mRectDataList is null : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->G:Lcom/samsung/android/app/accesscontrol/i;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/i;->b(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/a;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/accesscontrol/a;->a(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->s:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->s:I

    :cond_0
    return-void
.end method

.method public a(IIII)V
    .locals 3

    sput p1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c:I

    sput p2, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d:I

    sput p3, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->e:I

    sput p4, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->f:I

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->s()V

    const-string v0, "AccessControlMainService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CaptureView point left : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", top : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", right : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bottom : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->r()V

    return-void
.end method

.method public a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->o:Landroid/app/StatusBarManager;

    const/high16 v1, 0x10000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->o:Landroid/app/StatusBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/a;

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/accesscontrol/a;->a(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c()Landroid/graphics/Bitmap;
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "AccessControlMainService"

    const-string v1, "mBGImageBitmap is null!!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    return v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->y:Z

    return v0
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public g()Ljava/util/ArrayList;
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "AccessControlMainService"

    const-string v1, "GetRectDataList, mRectDataList is null!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()V
    .locals 3

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v1, "use_screen_timeout_preference"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    const-string v0, "AccessControlMainService"

    const-string v1, "Lock LCD Timeout"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->u:Z

    :cond_0
    return-void
.end method

.method public j()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v1, "use_screen_timeout_preference"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    const-string v0, "AccessControlMainService"

    const-string v1, "unLock LCD Timeout"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->t:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->u:Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V

    goto :goto_0
.end method

.method public k()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->N:Landroid/os/Handler;

    return-object v0
.end method

.method public l()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->s:I

    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    rem-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->I:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->J:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->J:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->I:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a(II)V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->g:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->g:Landroid/content/Context;

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->g:Landroid/content/Context;

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->y:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->y:Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->g:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080012

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->stopForeground(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->j()V

    :cond_0
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "access_control_enabled"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->l:Lcom/samsung/android/app/accesscontrol/t;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->l:Lcom/samsung/android/app/accesscontrol/t;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/t;->a()V

    iput-object v5, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->l:Lcom/samsung/android/app/accesscontrol/t;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/a;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/a;->a()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v5, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->r:Lcom/samsung/android/app/accesscontrol/j;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->r:Lcom/samsung/android/app/accesscontrol/j;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/j;->a()V

    iput-object v5, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->r:Lcom/samsung/android/app/accesscontrol/j;

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v5, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v5, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->H:Landroid/graphics/Bitmap;

    :cond_5
    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlActivity;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->finish()V

    :cond_6
    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->finish()V

    :cond_7
    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v3, "use_auto_rotate_screen_preference"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/samsung/android/app/accesscontrol/aa;->a(Landroid/content/ContentResolver;Z)V

    const-string v1, "use_auto_rotate_screen_preference"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :goto_1
    sget-object v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v3, "use_auto_rotate_screen_second_preference"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/samsung/android/app/accesscontrol/aa;->b(Landroid/content/ContentResolver;Z)V

    const-string v1, "use_auto_rotate_screen_second_preference"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_8
    sget-object v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v3, "accessibility_display_magnification_enabled"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "accessibility_display_magnification_enabled"

    invoke-static {v1, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v1, "accessibility_display_magnification_enabled"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_9
    sget-object v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v3, "multi_window_preference"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "multi_window_enabled"

    invoke-static {v1, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v1, "multi_window_preference"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_a
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->B:I

    invoke-direct {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c(I)V

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->C:I

    invoke-direct {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b(I)V

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->D:I

    invoke-direct {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d(I)V

    const-string v0, "TEST"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SVC End : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->C:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->D:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->B:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->O:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const-string v0, "AccessControlMainService"

    const-string v1, "Savet last mode As Setting Mode!!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    sput-object v5, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    return-void

    :cond_b
    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->p()I

    move-result v1

    if-ne v1, v4, :cond_c

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v2}, Lcom/samsung/android/app/accesscontrol/aa;->a(Landroid/content/ContentResolver;I)V

    goto/16 :goto_1

    :cond_c
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/samsung/android/app/accesscontrol/aa;->a(Landroid/content/ContentResolver;I)V

    goto/16 :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    const/16 v4, 0xff

    const/4 v2, 0x0

    sput-object p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/android/app/accesscontrol/i;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/accesscontrol/i;-><init>(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->G:Lcom/samsung/android/app/accesscontrol/i;

    new-instance v0, Lcom/samsung/android/app/accesscontrol/t;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/accesscontrol/t;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->l:Lcom/samsung/android/app/accesscontrol/t;

    const-string v0, "AccessControlPreference"

    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v1, "last_svc_mode"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->M:I

    const-string v0, "statusbar"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->o:Landroid/app/StatusBarManager;

    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->p:Landroid/app/NotificationManager;

    const-string v0, "keyguard"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->q:Landroid/app/KeyguardManager;

    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v1, 0x6

    const-string v2, "My Tag"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->t:Landroid/os/PowerManager$WakeLock;

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->A:I

    iget v0, v2, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->I:I

    iget v0, v2, Landroid/graphics/Point;->y:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->J:I

    const-string v0, "AccessControlMainService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "LCD width : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->I:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", height : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->J:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v2, "save_settings_preference"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->F:I

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->F:I

    if-ne v0, v4, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->F:I

    const-string v0, "save_settings_preference"

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->F:I

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    new-instance v0, Lcom/samsung/android/app/accesscontrol/j;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/accesscontrol/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->r:Lcom/samsung/android/app/accesscontrol/j;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->r:Lcom/samsung/android/app/accesscontrol/j;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/j;->a(I)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->o:Landroid/app/StatusBarManager;

    invoke-virtual {v0}, Landroid/app/StatusBarManager;->getPanelExpandState()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->o:Landroid/app/StatusBarManager;

    invoke-virtual {v0}, Landroid/app/StatusBarManager;->collapsePanels()V

    const-wide/16 v0, 0x258

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->M:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const-string v0, "AccessControlMainService"

    const-string v1, "Start as Block Mode!!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->n()V

    :goto_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.systemui.statusbar.EXPANDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.samsung.cover.OPEN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.sec.android.app.camera.ACTION_CAMERA_FINISH"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.sec.android.app.camera.ACTION_STOP_CAMERA"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.VIDEOPLAYER_AUTO_OFF_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->O:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_2
    const-string v0, "AccessControlMainService"

    const-string v1, "Start as Setting Mode!!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->o()V

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->m()V

    goto :goto_1
.end method
