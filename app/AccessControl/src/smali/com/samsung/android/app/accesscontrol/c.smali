.class public Lcom/samsung/android/app/accesscontrol/c;
.super Landroid/widget/BaseAdapter;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Landroid/content/res/Resources;

.field private c:Ljava/util/List;

.field private d:Landroid/view/LayoutInflater;

.field private e:I

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const-string v1, "AccessControlListAdapter"

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/c;->a:Ljava/lang/String;

    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/c;->e:I

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/c;->f:Landroid/content/Context;

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/c;->f:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/c;->b:Landroid/content/res/Resources;

    iput p2, p0, Lcom/samsung/android/app/accesscontrol/c;->e:I

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/c;->b:Landroid/content/res/Resources;

    const/high16 v1, 0x7f040000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/c;->c:Ljava/util/List;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/c;->d:Landroid/view/LayoutInflater;

    return-void

    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/c;->b:Landroid/content/res/Resources;

    const v1, 0x7f040001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v1, "AccessControlListAdapter"

    const-string v2, "Mode error"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/c;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/c;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f030002

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    new-instance v1, Lcom/samsung/android/app/accesscontrol/d;

    invoke-direct {v1}, Lcom/samsung/android/app/accesscontrol/d;-><init>()V

    const v0, 0x7f0b0006

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->a:Landroid/widget/TextView;

    const v0, 0x7f0b0007

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->b:Landroid/widget/TextView;

    const v0, 0x7f0b0005

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    iget-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setId(I)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/c;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, v1, Lcom/samsung/android/app/accesscontrol/d;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/c;->e:I

    if-nez v0, :cond_3

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_1
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/d;

    move-object v1, v0

    goto :goto_0

    :pswitch_0
    iget-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/c;->b:Landroid/content/res/Resources;

    const v3, 0x7f080023

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    sget-object v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v2, "use_screen_timeout_preference"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :pswitch_1
    iget-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/c;->b:Landroid/content/res/Resources;

    const v3, 0x7f080021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v2, "use_motions_preference"

    const/16 v3, 0xff

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v5, :cond_2

    iget-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_2
    iget-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/c;->e:I

    if-ne v0, v5, :cond_0

    iget-object v0, v1, Lcom/samsung/android/app/accesscontrol/d;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
