.class Lcom/samsung/android/app/accesscontrol/m;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/m;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/app/accesscontrol/d;

    if-nez p3, :cond_2

    iget-object v1, v0, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/m;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    invoke-static {v0, v3}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a(Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;Z)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/m;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    invoke-static {v0, v2}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a(Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;Z)Z

    goto :goto_0

    :cond_2
    if-ne p3, v2, :cond_0

    iget-object v1, v0, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/m;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    invoke-static {v0, v3}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a(Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;I)I

    goto :goto_0

    :cond_3
    iget-object v0, v0, Lcom/samsung/android/app/accesscontrol/d;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/m;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    invoke-static {v0, v2}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a(Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;I)I

    goto :goto_0
.end method
