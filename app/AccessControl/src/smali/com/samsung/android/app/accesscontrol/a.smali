.class public Lcom/samsung/android/app/accesscontrol/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/view/WindowManager;

.field private b:Landroid/view/WindowManager$LayoutParams;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/content/res/Resources;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:F

.field private j:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Rect;)V
    .locals 6

    const v4, 0x7f07000b

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->a:Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->d:Landroid/content/res/Resources;

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->e:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->f:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->g:I

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->h:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->i:F

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->j:Landroid/content/Context;

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/a;->j:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    const v1, 0x7f020007

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const-string v0, "AccessControlImageView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AccessControlImageView viewRect : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->d:Landroid/content/res/Resources;

    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->a:Landroid/view/WindowManager;

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, v4, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->d:Landroid/content/res/Resources;

    const v2, 0x7f07000d

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->i:F

    :goto_0
    sget v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c:I

    sget v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->d:I

    iget v2, p2, Landroid/graphics/Rect;->left:I

    sub-int v0, v2, v0

    iget v2, p2, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    iget v2, p2, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    iget v4, p2, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    int-to-float v0, v0

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/a;->i:F

    mul-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->e:I

    int-to-float v0, v1

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->i:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->f:I

    int-to-float v0, v2

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->i:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->g:I

    int-to-float v0, v3

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->i:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->h:I

    const-string v0, "Test"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GuidedView(rear) Left : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/a;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Top : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/a;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Width : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/a;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/a;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0x7d5

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->g:I

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/a;->h:I

    const v4, 0x10128

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->e:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->f:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->a:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "SC-01G"

    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "SCL24"

    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, v4, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->d:Landroid/content/res/Resources;

    const v2, 0x7f070004

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->i:F

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->d:Landroid/content/res/Resources;

    invoke-virtual {v0, v4, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->d:Landroid/content/res/Resources;

    const v2, 0x7f070003

    invoke-virtual {v1, v2, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/a;->i:F

    goto/16 :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->a:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public a(II)V
    .locals 3

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/a;->g:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/a;->h:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->a:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/a;->e:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->g:I

    add-int/2addr v0, v1

    if-le v0, p1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->e:I

    sub-int v1, p1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    :goto_1
    iget v0, p0, Lcom/samsung/android/app/accesscontrol/a;->f:I

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->h:I

    add-int/2addr v0, v1

    if-le v0, p2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->f:I

    sub-int v1, p2, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->g:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->b:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/a;->h:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->c:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/a;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "air_motion_engine"

    invoke-static {v0, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/a;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "surface_motion_engine"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/a;->j:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "master_motion"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    const-string v3, "TEST"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "OnClick "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
