.class Lcom/samsung/android/app/accesscontrol/x;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic a:Lcom/samsung/android/app/accesscontrol/RectAnimator;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/accesscontrol/RectAnimator;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/x;->a:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/x;->a:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    # getter for: Lcom/samsung/android/app/accesscontrol/RectAnimator;->mAnimationHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->access$100(Lcom/samsung/android/app/accesscontrol/RectAnimator;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/x;->a:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    # setter for: Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsCompleteAniX:Z
    invoke-static {v0, v1}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->access$202(Lcom/samsung/android/app/accesscontrol/RectAnimator;Z)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/x;->a:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    # operator-- for: Lcom/samsung/android/app/accesscontrol/RectAnimator;->mPointAnimationEnd:I
    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->access$310(Lcom/samsung/android/app/accesscontrol/RectAnimator;)I

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/x;->a:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    # getter for: Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsCompleteAniX:Z
    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->access$200(Lcom/samsung/android/app/accesscontrol/RectAnimator;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/x;->a:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    # getter for: Lcom/samsung/android/app/accesscontrol/RectAnimator;->mIsCompleteAniY:Z
    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->access$400(Lcom/samsung/android/app/accesscontrol/RectAnimator;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/x;->a:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    # getter for: Lcom/samsung/android/app/accesscontrol/RectAnimator;->mPointAnimationEnd:I
    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->access$300(Lcom/samsung/android/app/accesscontrol/RectAnimator;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/x;->a:Lcom/samsung/android/app/accesscontrol/RectAnimator;

    # invokes: Lcom/samsung/android/app/accesscontrol/RectAnimator;->FinishAnimation()V
    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/RectAnimator;->access$500(Lcom/samsung/android/app/accesscontrol/RectAnimator;)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method
