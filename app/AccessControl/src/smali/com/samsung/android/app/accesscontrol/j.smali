.class public Lcom/samsung/android/app/accesscontrol/j;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/view/WindowManager;

.field private b:Landroid/content/Context;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:F

.field private g:F

.field private h:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->a:Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->c:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->d:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->e:Landroid/widget/ImageView;

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/j;->f:F

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/j;->g:F

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->h:Landroid/graphics/Point;

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    :cond_0
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->a:Landroid/view/WindowManager;

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->h:Landroid/graphics/Point;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->a:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->h:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/j;->b()V

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/j;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/j;->f:F

    return v0
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/j;F)F
    .locals 0

    iput p1, p0, Lcom/samsung/android/app/accesscontrol/j;->f:F

    return p1
.end method

.method static synthetic b(Lcom/samsung/android/app/accesscontrol/j;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/j;->g:F

    return v0
.end method

.method private b()V
    .locals 10

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    iput v1, p0, Lcom/samsung/android/app/accesscontrol/j;->g:F

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->c:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->d:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->e:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->e:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->e:Landroid/widget/ImageView;

    new-instance v2, Lcom/samsung/android/app/accesscontrol/k;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/accesscontrol/k;-><init>(Lcom/samsung/android/app/accesscontrol/j;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const v1, 0x7f070009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const v2, 0x7f070008

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    const v2, 0x7f070007

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    const v2, 0x7f070005

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/j;->h:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    float-to-int v1, v1

    sub-int v1, v2, v1

    float-to-int v2, v8

    const/16 v3, 0x7d5

    const/16 v4, 0x128

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    const/4 v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/j;->h:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    float-to-int v3, v7

    const/16 v4, 0x7d5

    const/16 v5, 0x128

    const/4 v6, -0x3

    invoke-direct/range {v1 .. v6}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/16 v2, 0x33

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v2, 0x0

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/j;->h:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    add-float v3, v7, v9

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/j;->h:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/samsung/android/app/accesscontrol/j;->h:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    add-float v5, v8, v9

    add-float/2addr v5, v7

    float-to-int v5, v5

    sub-int/2addr v4, v5

    const/16 v5, 0x7d5

    const/16 v6, 0x128

    const/4 v7, -0x3

    invoke-direct/range {v2 .. v7}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    const/16 v3, 0x33

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/4 v3, 0x0

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    float-to-int v3, v8

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/j;->a:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/samsung/android/app/accesscontrol/j;->c:Landroid/widget/ImageView;

    invoke-interface {v3, v4, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/j;->a:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/j;->d:Landroid/widget/ImageView;

    invoke-interface {v2, v3, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->a:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/j;->e:Landroid/widget/ImageView;

    invoke-interface {v0, v2, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic c(Lcom/samsung/android/app/accesscontrol/j;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->a:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->d:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->a:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->c:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->a:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->e:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/j;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08001d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method
