.class public Lcom/samsung/android/app/accesscontrol/t;
.super Ljava/lang/Object;


# instance fields
.field private a:Lcom/samsung/android/app/accesscontrol/u;

.field private b:Lcom/samsung/android/app/accesscontrol/v;

.field private c:Landroid/view/WindowManager$LayoutParams;

.field private d:Landroid/content/Context;

.field private e:Landroid/graphics/Point;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->a:Lcom/samsung/android/app/accesscontrol/u;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->b:Lcom/samsung/android/app/accesscontrol/v;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->c:Landroid/view/WindowManager$LayoutParams;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->d:Landroid/content/Context;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->e:Landroid/graphics/Point;

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/t;->d:Landroid/content/Context;

    check-cast p1, Lcom/samsung/android/app/accesscontrol/v;

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/t;->b:Lcom/samsung/android/app/accesscontrol/v;

    new-instance v0, Lcom/samsung/android/app/accesscontrol/u;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/t;->d:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/accesscontrol/u;-><init>(Lcom/samsung/android/app/accesscontrol/t;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->a:Lcom/samsung/android/app/accesscontrol/u;

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    const/16 v3, 0x7d5

    const/16 v4, 0x28

    const/4 v5, -0x3

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->c:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->d:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/t;->a:Lcom/samsung/android/app/accesscontrol/u;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/t;->c:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/t;->e:Landroid/graphics/Point;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/t;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/t;)Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->e:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic b(Lcom/samsung/android/app/accesscontrol/t;)Lcom/samsung/android/app/accesscontrol/v;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->b:Lcom/samsung/android/app/accesscontrol/v;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/t;->d:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/t;->a:Lcom/samsung/android/app/accesscontrol/u;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    return-void
.end method
