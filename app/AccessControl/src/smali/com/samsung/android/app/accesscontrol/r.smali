.class Lcom/samsung/android/app/accesscontrol/r;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;


# instance fields
.field final synthetic a:Lcom/samsung/android/app/accesscontrol/n;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/accesscontrol/n;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v3, 0x1

    const-string v0, "AccessControlView"

    const-string v1, "onDoubleTap()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    invoke-static {v2}, Lcom/samsung/android/app/accesscontrol/n;->a(Lcom/samsung/android/app/accesscontrol/n;)I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    invoke-static {v2}, Lcom/samsung/android/app/accesscontrol/n;->b(Lcom/samsung/android/app/accesscontrol/n;)I

    move-result v2

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/n;->c(Lcom/samsung/android/app/accesscontrol/n;)I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v1, v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/n;->d(Lcom/samsung/android/app/accesscontrol/n;)I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/n;->e(Lcom/samsung/android/app/accesscontrol/n;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/n;->a(Z)V

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/n;->f(Lcom/samsung/android/app/accesscontrol/n;)Lcom/samsung/android/app/accesscontrol/s;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    invoke-static {v1}, Lcom/samsung/android/app/accesscontrol/n;->e(Lcom/samsung/android/app/accesscontrol/n;)Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/samsung/android/app/accesscontrol/s;->a(Z)V

    :cond_0
    return v3

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/r;->a:Lcom/samsung/android/app/accesscontrol/n;

    invoke-virtual {v0, v3}, Lcom/samsung/android/app/accesscontrol/n;->a(Z)V

    goto :goto_0
.end method
