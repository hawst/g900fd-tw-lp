.class public Lcom/samsung/android/app/accesscontrol/AccessControlActivity;
.super Landroid/app/Activity;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/samsung/android/app/accesscontrol/s;


# static fields
.field public static a:Lcom/samsung/android/app/accesscontrol/AccessControlActivity;


# instance fields
.field b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

.field private i:Landroid/widget/RelativeLayout;

.field private j:Lcom/samsung/android/app/accesscontrol/n;

.field private k:Landroid/widget/ImageView;

.field private l:Landroid/widget/CheckBox;

.field private m:Z

.field private n:Lcom/samsung/android/app/accesscontrol/b;

.field private o:Landroid/view/View;

.field private p:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlActivity;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "AccessControlActivity"

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->c:Ljava/lang/String;

    iput v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->e:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->f:I

    const/16 v0, 0x12c

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->g:I

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->i:Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->k:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->l:Landroid/widget/CheckBox;

    iput-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->m:Z

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->b:Landroid/content/Context;

    return-void
.end method

.method private a()V
    .locals 9

    const/4 v8, 0x2

    const/4 v3, 0x1

    const-string v0, "AccessControlActivity"

    const-string v1, "Init()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v2

    const v0, 0x7f0b0002

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->i:Landroid/widget/RelativeLayout;

    const/high16 v0, 0x7f0b0000

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->k:Landroid/widget/ImageView;

    const v0, 0x7f0b0001

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->l:Landroid/widget/CheckBox;

    if-eqz v2, :cond_0

    if-ne v2, v8, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07000d

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->p:F

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f07000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    if-nez v2, :cond_5

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->p:F

    mul-float/2addr v0, v1

    float-to-int v1, v0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->p:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    move v4, v0

    :goto_1
    const-string v0, "AccessControlActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "scale = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->p:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", W = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", H = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/secutil/Log;->secI(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-static {v3, v1, v4, v0}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->k:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    if-eqz v2, :cond_1

    if-ne v2, v8, :cond_6

    :cond_1
    iget v0, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f070001

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v5

    add-int/lit8 v2, v0, 0x1

    add-int/2addr v1, v3

    add-int v0, v2, v4

    :goto_2
    iget-object v4, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v4, v3, v2, v1, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a(IIII)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/samsung/android/app/accesscontrol/n;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->i:Landroid/widget/RelativeLayout;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/app/accesscontrol/n;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    :goto_3
    return-void

    :cond_2
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "tblte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "tbelte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "SC-01G"

    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "SCL24"

    const-string v1, "ro.product.name"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070004

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->p:F

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070003

    invoke-virtual {v0, v1, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->p:F

    goto/16 :goto_0

    :cond_5
    :try_start_1
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->p:F

    mul-float/2addr v0, v1

    float-to-int v1, v0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v4, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->p:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    move v4, v0

    goto/16 :goto_1

    :cond_6
    iget v0, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f070000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v0

    add-int/2addr v0, v5

    add-int/lit8 v2, v0, 0x1

    add-int/2addr v1, v3

    add-int v0, v2, v4

    goto/16 :goto_2

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->b:Landroid/content/Context;

    const v1, 0x7f080016

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->stopSelf()V

    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->finish()V

    goto/16 :goto_3
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    const-string v0, "statusbar"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    if-eqz v0, :cond_0

    const/high16 v1, 0x1a70000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    const-string v0, "statusbar"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    const/4 v4, 0x1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->k()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/n;->c()V

    const-string v1, "AccessControlActivity"

    const-string v2, "SEND_MESSAGE - MSG_CREATE_GUIDED_VIEW"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    if-ne p2, v4, :cond_0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/n;->b()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->stopSelf()V

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->l:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/app/accesscontrol/n;->a(Z)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->onBackPressed()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->k()Landroid/os/Handler;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "use_screen_timeout_preference"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/n;->c()V

    const-string v1, "AccessControlActivity"

    const-string v2, "SEND_MESSAGE - MSG_CREATE_GUIDED_VIEW"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->finish()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0b0001 -> :sswitch_0
        0x7f0b0008 -> :sswitch_1
        0x7f0b0009 -> :sswitch_2
    .end sparse-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sput-object p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlActivity;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->b:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f050000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->b:Landroid/content/Context;

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->b:Landroid/content/Context;

    :cond_0
    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "access_control_enabled"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const/high16 v0, 0x7f030000

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->setContentView(I)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030003

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->o:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->o:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->o:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->o:Landroid/view/View;

    const v1, 0x7f0b0008

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->o:Landroid/view/View;

    const v1, 0x7f0b0009

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/samsung/android/app/accesscontrol/b;

    invoke-direct {v0}, Lcom/samsung/android/app/accesscontrol/b;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->n:Lcom/samsung/android/app/accesscontrol/b;

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->a()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/n;->a()V

    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    :cond_0
    iput-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    sput-object v1, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlActivity;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/16 v0, 0x105

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    iget-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->m:Z

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->onBackPressed()V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->h:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->k()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->j:Lcom/samsung/android/app/accesscontrol/n;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/n;->c()V

    const-string v1, "AccessControlActivity"

    const-string v2, "SEND_MESSAGE - MSG_CREATE_GUIDED_VIEW"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->finish()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0b000c -> :sswitch_0
        0x7f0b000d -> :sswitch_1
        0x7f0b000e -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->m:Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->n:Lcom/samsung/android/app/accesscontrol/b;

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/accesscontrol/b;->a(IZLandroid/content/ComponentName;)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->n:Lcom/samsung/android/app/accesscontrol/b;

    const/16 v1, 0x105

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/accesscontrol/b;->a(IZLandroid/content/ComponentName;)Z

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_CAMERA_ONLY_MODEL"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->b(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->m:Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->n:Lcom/samsung/android/app/accesscontrol/b;

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/accesscontrol/b;->a(IZLandroid/content/ComponentName;)Z

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->n:Lcom/samsung/android/app/accesscontrol/b;

    const/16 v1, 0x105

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/app/accesscontrol/b;->a(IZLandroid/content/ComponentName;)Z

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_CAMERA_ONLY_MODEL"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlActivity;->a(Landroid/content/Context;)V

    :cond_0
    return-void
.end method
