.class public Lcom/samsung/android/app/accesscontrol/Vertex;
.super Ljava/lang/Object;


# instance fields
.field mPointX:F

.field mPointY:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    iput p2, p0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    return-void
.end method


# virtual methods
.method public getX()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    return v0
.end method

.method public getY()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    return v0
.end method

.method public setX(F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointX:F

    return-void
.end method

.method public setY(F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/app/accesscontrol/Vertex;->mPointY:F

    return-void
.end method
