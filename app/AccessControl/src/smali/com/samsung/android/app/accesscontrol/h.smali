.class Lcom/samsung/android/app/accesscontrol/h;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->e()Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "com.sec.android.app.camera.ACTION_CAMERA_FINISH"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.sec.android.app.camera.ACTION_STOP_CAMERA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->stopSelf()V

    :cond_1
    const-string v1, "android.intent.action.VIDEOPLAYER_AUTO_OFF_COMPLETE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->stopSelf()V

    :cond_2
    const-string v1, "com.sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->stopSelf()V

    :cond_3
    const-string v1, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "easymode"

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->stopSelf()V

    :cond_4
    const-string v1, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-static {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)Lcom/samsung/android/app/accesscontrol/j;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/samsung/android/app/accesscontrol/j;->a(I)V

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->a()V

    :cond_5
    const-string v1, "com.android.systemui.statusbar.EXPANDED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-static {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)Landroid/app/KeyguardManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-static {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)Lcom/samsung/android/app/accesscontrol/j;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/accesscontrol/j;->a(I)V

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b()V

    :cond_6
    const-string v1, "com.samsung.cover.OPEN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "coverOpen"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-virtual {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->stopSelf()V

    :cond_7
    :goto_0
    return-void

    :cond_8
    const-string v1, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-static {v1}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)Lcom/samsung/android/app/accesscontrol/j;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/samsung/android/app/accesscontrol/j;->a(I)V

    :cond_9
    const-string v1, "com.android.systemui.statusbar.EXPANDED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->c(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)Landroid/app/KeyguardManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/h;->a:Lcom/samsung/android/app/accesscontrol/AccessControlMainService;

    invoke-static {v0}, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b(Lcom/samsung/android/app/accesscontrol/AccessControlMainService;)Lcom/samsung/android/app/accesscontrol/j;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/app/accesscontrol/j;->a(I)V

    goto :goto_0
.end method
