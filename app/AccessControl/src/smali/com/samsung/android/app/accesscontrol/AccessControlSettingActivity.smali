.class public Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;
.super Landroid/app/Activity;


# static fields
.field public static a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;


# instance fields
.field private b:Landroid/widget/ListView;

.field private c:Lcom/samsung/android/app/accesscontrol/c;

.field private d:Z

.field private e:I

.field private f:Lcom/samsung/android/app/accesscontrol/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->b:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->c:Lcom/samsung/android/app/accesscontrol/c;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->e:I

    return-void
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;I)I
    .locals 0

    iput p1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->e:I

    return p1
.end method

.method private a()V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v1, "use_screen_timeout_preference"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->d:Z

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    const-string v1, "use_motions_preference"

    const/16 v2, 0xff

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->e:I

    const v0, 0x7f0b0004

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->b:Landroid/widget/ListView;

    new-instance v0, Lcom/samsung/android/app/accesscontrol/c;

    invoke-direct {v0, p0, v3}, Lcom/samsung/android/app/accesscontrol/c;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->c:Lcom/samsung/android/app/accesscontrol/c;

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->c:Lcom/samsung/android/app/accesscontrol/c;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/samsung/android/app/accesscontrol/m;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/accesscontrol/m;-><init>(Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    const-string v0, "statusbar"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    if-eqz v0, :cond_0

    const/high16 v1, 0x1a70000

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->d:Z

    return p1
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    const-string v0, "statusbar"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->setResult(I)V

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    sput-object p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    const v0, 0x7f030001

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x7f080000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/samsung/android/app/accesscontrol/b;

    invoke-direct {v0}, Lcom/samsung/android/app/accesscontrol/b;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->f:Lcom/samsung/android/app/accesscontrol/b;

    invoke-direct {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5

    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a0000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v0, 0x7f0b000c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    const v1, 0x7f0b000e

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f0b000d

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    :cond_0
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a:Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->onBackPressed()V

    goto :goto_0

    :sswitch_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->setResult(I)V

    sget-object v0, Lcom/samsung/android/app/accesscontrol/AccessControlMainService;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "use_screen_timeout_preference"

    iget-boolean v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->d:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v1, "use_motions_preference"

    iget v2, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->e:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0b000c -> :sswitch_0
        0x7f0b000e -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->f:Lcom/samsung/android/app/accesscontrol/b;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/accesscontrol/b;->a(IZLandroid/content/ComponentName;)Z

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_CAMERA_ONLY_MODEL"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->b(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 4

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->f:Lcom/samsung/android/app/accesscontrol/b;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/app/accesscontrol/b;->a(IZLandroid/content/ComponentName;)Z

    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    const-string v1, "SEC_FLOATING_FEATURE_SETTINGS_CAMERA_ONLY_MODEL"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/samsung/android/app/accesscontrol/AccessControlSettingActivity;->a(Landroid/content/Context;)V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
