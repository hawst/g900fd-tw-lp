.class Lcom/android/stk2/StkDialogActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "StkDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/stk2/StkDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/stk2/StkDialogActivity;


# direct methods
.method constructor <init>(Lcom/android/stk2/StkDialogActivity;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/android/stk2/StkDialogActivity$2;->this$0:Lcom/android/stk2/StkDialogActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v7, 0xd

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 348
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.android.samsungtest.EVENTHANDLE_BUTTON"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 349
    const-string v2, ""

    .line 350
    .local v2, "inputString":Ljava/lang/String;
    const/4 v1, -0x1

    .line 351
    .local v1, "button":I
    const-string v3, "SIM2"

    const-string v4, "got intent"

    invoke-static {v3, p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 352
    const-string v3, "Button"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 354
    if-nez v1, :cond_1

    .line 355
    iget-object v3, p0, Lcom/android/stk2/StkDialogActivity$2;->this$0:Lcom/android/stk2/StkDialogActivity;

    const/16 v4, 0x15

    # invokes: Lcom/android/stk2/StkDialogActivity;->sendResponse(I)V
    invoke-static {v3, v4}, Lcom/android/stk2/StkDialogActivity;->access$000(Lcom/android/stk2/StkDialogActivity;I)V

    .line 356
    iget-object v3, p0, Lcom/android/stk2/StkDialogActivity$2;->this$0:Lcom/android/stk2/StkDialogActivity;

    invoke-virtual {v3}, Lcom/android/stk2/StkDialogActivity;->finish()V

    .line 372
    .end local v1    # "button":I
    .end local v2    # "inputString":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 358
    .restart local v1    # "button":I
    .restart local v2    # "inputString":Ljava/lang/String;
    :cond_1
    if-ne v1, v6, :cond_2

    .line 359
    iget-object v3, p0, Lcom/android/stk2/StkDialogActivity$2;->this$0:Lcom/android/stk2/StkDialogActivity;

    # invokes: Lcom/android/stk2/StkDialogActivity;->sendResponse(IZ)V
    invoke-static {v3, v7, v6}, Lcom/android/stk2/StkDialogActivity;->access$100(Lcom/android/stk2/StkDialogActivity;IZ)V

    .line 360
    iget-object v3, p0, Lcom/android/stk2/StkDialogActivity$2;->this$0:Lcom/android/stk2/StkDialogActivity;

    invoke-virtual {v3}, Lcom/android/stk2/StkDialogActivity;->finish()V

    goto :goto_0

    .line 362
    :cond_2
    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 363
    iget-object v3, p0, Lcom/android/stk2/StkDialogActivity$2;->this$0:Lcom/android/stk2/StkDialogActivity;

    # invokes: Lcom/android/stk2/StkDialogActivity;->sendResponse(IZ)V
    invoke-static {v3, v7, v5}, Lcom/android/stk2/StkDialogActivity;->access$100(Lcom/android/stk2/StkDialogActivity;IZ)V

    .line 364
    iget-object v3, p0, Lcom/android/stk2/StkDialogActivity$2;->this$0:Lcom/android/stk2/StkDialogActivity;

    invoke-virtual {v3}, Lcom/android/stk2/StkDialogActivity;->finish()V

    goto :goto_0

    .line 366
    .end local v1    # "button":I
    .end local v2    # "inputString":Ljava/lang/String;
    :cond_3
    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 367
    iget-object v3, p0, Lcom/android/stk2/StkDialogActivity$2;->this$0:Lcom/android/stk2/StkDialogActivity;

    # getter for: Lcom/android/stk2/StkDialogActivity;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/stk2/StkDialogActivity;->access$200(Lcom/android/stk2/StkDialogActivity;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "airplane_mode_on"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 368
    .local v0, "airPlaneEnabled":I
    const-string v3, "SIM2"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received  ACTION_AIRPLANE_MODE_CHANGED = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 369
    if-ne v0, v6, :cond_0

    .line 370
    iget-object v3, p0, Lcom/android/stk2/StkDialogActivity$2;->this$0:Lcom/android/stk2/StkDialogActivity;

    invoke-virtual {v3}, Lcom/android/stk2/StkDialogActivity;->finish()V

    goto :goto_0
.end method
