.class public Lcom/android/stk2/StkDialogActivity;
.super Landroid/app/Activity;
.source "StkDialogActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field appService:Lcom/android/stk2/StkAppService;

.field private mContext:Landroid/content/Context;

.field mPauseRelease:Z

.field mSentTerminalResponse:Z

.field private final mStateReceiver:Landroid/content/BroadcastReceiver;

.field mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

.field mTimeoutHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 57
    invoke-static {}, Lcom/android/stk2/StkAppService;->getInstance()Lcom/android/stk2/StkAppService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    .line 64
    new-instance v0, Lcom/android/stk2/StkDialogActivity$1;

    invoke-direct {v0, p0}, Lcom/android/stk2/StkDialogActivity$1;-><init>(Lcom/android/stk2/StkDialogActivity;)V

    iput-object v0, p0, Lcom/android/stk2/StkDialogActivity;->mTimeoutHandler:Landroid/os/Handler;

    .line 345
    new-instance v0, Lcom/android/stk2/StkDialogActivity$2;

    invoke-direct {v0, p0}, Lcom/android/stk2/StkDialogActivity$2;-><init>(Lcom/android/stk2/StkDialogActivity;)V

    iput-object v0, p0, Lcom/android/stk2/StkDialogActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/android/stk2/StkDialogActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkDialogActivity;
    .param p1, "x1"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/android/stk2/StkDialogActivity;->sendResponse(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/android/stk2/StkDialogActivity;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkDialogActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/android/stk2/StkDialogActivity;->sendResponse(IZ)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/stk2/StkDialogActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk2/StkDialogActivity;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/stk2/StkDialogActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private cancelTimeOut()V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/android/stk2/StkDialogActivity;->mTimeoutHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 313
    return-void
.end method

.method private initFromIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 304
    if-eqz p1, :cond_0

    .line 305
    const-string v0, "TEXT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/cat/TextMessage;

    iput-object v0, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    .line 309
    :goto_0
    return-void

    .line 307
    :cond_0
    invoke-virtual {p0}, Lcom/android/stk2/StkDialogActivity;->finish()V

    goto :goto_0
.end method

.method private sendResponse(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 300
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/android/stk2/StkDialogActivity;->sendResponse(IZ)V

    .line 301
    return-void
.end method

.method private sendResponse(IZ)V
    .locals 3
    .param p1, "resId"    # I
    .param p2, "confirmed"    # Z

    .prologue
    .line 291
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 292
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "op"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 293
    const-string v1, "response id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 294
    const-string v1, "confirm"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 295
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/stk2/StkAppService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/stk2/StkDialogActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 296
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/stk2/StkDialogActivity;->mSentTerminalResponse:Z

    .line 297
    return-void
.end method

.method private startPauseTimeOut()V
    .locals 4

    .prologue
    .line 341
    iget-object v0, p0, Lcom/android/stk2/StkDialogActivity;->mTimeoutHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mTimeoutHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 343
    return-void
.end method

.method private startTimeOut()V
    .locals 6

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/android/stk2/StkDialogActivity;->cancelTimeOut()V

    .line 318
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v1, v1, Lcom/android/internal/telephony/cat/TextMessage;->duration:Lcom/android/internal/telephony/cat/Duration;

    invoke-static {v1}, Lcom/android/stk2/StkApp;->calculateDurationInMilis(Lcom/android/internal/telephony/cat/Duration;)I

    move-result v0

    .line 319
    .local v0, "dialogDuration":I
    const-string v1, "SIM2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "raw data dialogDuration = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 328
    if-nez v0, :cond_0

    .line 329
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-boolean v1, v1, Lcom/android/internal/telephony/cat/TextMessage;->userClear:Z

    if-nez v1, :cond_1

    .line 330
    const/16 v0, 0x1388

    .line 335
    :cond_0
    :goto_0
    const-string v1, "SIM2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "final dialogDuration = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 336
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mTimeoutHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/stk2/StkDialogActivity;->mTimeoutHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 338
    return-void

    .line 332
    :cond_1
    const v0, 0x9c40

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 265
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/android/stk2/StkDialogActivity;->overridePendingTransition(II)V

    .line 266
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 267
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/16 v1, 0xd

    .line 183
    packed-switch p2, :pswitch_data_0

    .line 191
    :goto_0
    invoke-virtual {p0}, Lcom/android/stk2/StkDialogActivity;->finish()V

    .line 192
    return-void

    .line 185
    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/android/stk2/StkDialogActivity;->sendResponse(IZ)V

    goto :goto_0

    .line 188
    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/android/stk2/StkDialogActivity;->sendResponse(IZ)V

    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 272
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 273
    const-string v0, "SIM2"

    const-string v1, "onConfigurationChanged"

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 98
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-object v1, v1, Lcom/android/stk2/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-object v1, v1, Lcom/android/stk2/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    invoke-virtual {v1}, Lcom/android/stk2/StkAppService;->disableKeyguard()V

    .line 104
    iput-boolean v5, p0, Lcom/android/stk2/StkDialogActivity;->mSentTerminalResponse:Z

    .line 105
    invoke-virtual {p0}, Lcom/android/stk2/StkDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/stk2/StkDialogActivity;->initFromIntent(Landroid/content/Intent;)V

    .line 106
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    if-nez v1, :cond_1

    .line 107
    invoke-virtual {p0}, Lcom/android/stk2/StkDialogActivity;->finish()V

    .line 160
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v2, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    const-string v3, "\r\n"

    const-string v4, "\n"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    .line 138
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v2, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    const-string v3, "\r"

    const-string v4, "\n"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    .line 153
    invoke-virtual {p0, v5}, Lcom/android/stk2/StkDialogActivity;->showDialog(I)V

    .line 154
    invoke-virtual {p0}, Lcom/android/stk2/StkDialogActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mContext:Landroid/content/Context;

    .line 155
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.android.samsungtest.EVENTHANDLE_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 156
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 157
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/stk2/StkDialogActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 158
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/stk2/StkDialogActivity;->mPauseRelease:Z

    .line 159
    invoke-direct {p0}, Lcom/android/stk2/StkDialogActivity;->startPauseTimeOut()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 163
    packed-switch p1, :pswitch_data_0

    .line 179
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 167
    :pswitch_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    const/4 v2, 0x5

    invoke-direct {v1, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 171
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v2, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 172
    iget-object v2, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 173
    const v2, 0x104000a

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 174
    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 175
    invoke-virtual {v1, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 176
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 177
    .local v0, "alert":Landroid/app/AlertDialog;
    goto :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 198
    const-string v0, "SIM2"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyDown : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    packed-switch p2, :pswitch_data_0

    .line 206
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 202
    :pswitch_0
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/android/stk2/StkDialogActivity;->sendResponse(I)V

    .line 203
    invoke-virtual {p0}, Lcom/android/stk2/StkDialogActivity;->finish()V

    goto :goto_0

    .line 200
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 218
    const-string v1, "SIM2"

    const-string v2, "onPause time out cancel"

    invoke-static {v1, p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 219
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 220
    iget-boolean v1, p0, Lcom/android/stk2/StkDialogActivity;->mPauseRelease:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/stk2/StkDialogActivity;->mSentTerminalResponse:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-object v1, v1, Lcom/android/stk2/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 223
    :try_start_0
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-object v1, v1, Lcom/android/stk2/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 224
    const-string v1, "SIM2"

    const-string v2, "before release wakeup"

    invoke-static {v1, p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 225
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-object v1, v1, Lcom/android/stk2/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    :cond_1
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-object v1, v1, Lcom/android/stk2/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 231
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    invoke-virtual {v1}, Lcom/android/stk2/StkAppService;->enableKeyguard()V

    .line 234
    const-string v1, "KDI"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 235
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mTextMsg.responseNeeded : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-boolean v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->responseNeeded:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-boolean v1, v1, Lcom/android/internal/telephony/cat/TextMessage;->responseNeeded:Z

    if-nez v1, :cond_3

    .line 254
    :cond_2
    :goto_0
    return-void

    .line 228
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/android/stk2/StkDialogActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-object v2, v2, Lcom/android/stk2/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1

    .line 241
    :cond_3
    iget-boolean v1, p0, Lcom/android/stk2/StkDialogActivity;->mSentTerminalResponse:Z

    if-nez v1, :cond_4

    .line 242
    const/16 v1, 0x16

    invoke-direct {p0, v1}, Lcom/android/stk2/StkDialogActivity;->sendResponse(I)V

    .line 244
    :cond_4
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_5

    .line 246
    :try_start_1
    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/stk2/StkDialogActivity;->mStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 251
    :cond_5
    :goto_1
    invoke-direct {p0}, Lcom/android/stk2/StkDialogActivity;->cancelTimeOut()V

    .line 252
    invoke-virtual {p0}, Lcom/android/stk2/StkDialogActivity;->finish()V

    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "Failed unregisterReceiver"

    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 285
    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 287
    const-string v0, "text"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/cat/TextMessage;

    iput-object v0, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    .line 288
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 211
    const-string v0, "SIM2"

    const-string v1, "onResume startTimeOut"

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 213
    invoke-direct {p0}, Lcom/android/stk2/StkDialogActivity;->startTimeOut()V

    .line 214
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 278
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 280
    const-string v0, "text"

    iget-object v1, p0, Lcom/android/stk2/StkDialogActivity;->mTextMsg:Lcom/android/internal/telephony/cat/TextMessage;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 281
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 258
    const-string v0, "SIM2"

    const-string v1, "onStop finish dialog and send TR as END SESSION"

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 259
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 260
    return-void
.end method
