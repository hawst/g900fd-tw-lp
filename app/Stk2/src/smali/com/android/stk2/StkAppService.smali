.class public Lcom/android/stk2/StkAppService;
.super Landroid/app/Service;
.source "StkAppService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/stk2/StkAppService$2;,
        Lcom/android/stk2/StkAppService$ServiceHandler;,
        Lcom/android/stk2/StkAppService$MessageHandler;,
        Lcom/android/stk2/StkAppService$DelayedCmd;,
        Lcom/android/stk2/StkAppService$InitiatedByUserAction;
    }
.end annotation


# static fields
.field static sInstance:Lcom/android/stk2/StkAppService;


# instance fields
.field private final DELAY_TIME_FOR_RESET:I

.field private lastSelectedItem:Ljava/lang/String;

.field private launchBrowser:Z

.field public final lock:Ljava/util/concurrent/locks/ReentrantLock;

.field mBackupProxy:Landroid/net/ProxyInfo;

.field private mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

.field private mCmdInProgress:Z

.field private mCmdsQ:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/android/stk2/StkAppService$DelayedCmd;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

.field private mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

.field public mIsMainMenu:Z

.field mIsProxyChanged:Z

.field private mIsStartedByUser:Z

.field mIsSystemShutdown:Z

.field private mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

.field private mKeyguardManager:Landroid/app/KeyguardManager;

.field private mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

.field private mMenuIsVisibile:Z

.field public mMenuItemBlock:Z

.field private mMessageHandler:Landroid/os/Handler;

.field private mNotificationManager:Landroid/app/NotificationManager;

.field private volatile mServiceHandler:Lcom/android/stk2/StkAppService$ServiceHandler;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mSetEventList:[Z

.field private mStkService:Lcom/android/internal/telephony/cat/AppInterface;

.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private responseNeeded:Z

.field private salesCode:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    sput-object v0, Lcom/android/stk2/StkAppService;->sInstance:Lcom/android/stk2/StkAppService;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 93
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 99
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    .line 100
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 101
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 102
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 103
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->lastSelectedItem:Ljava/lang/String;

    .line 104
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->mMenuIsVisibile:Z

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/stk2/StkAppService;->responseNeeded:Z

    .line 106
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->mCmdInProgress:Z

    .line 107
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 108
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    .line 109
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->launchBrowser:Z

    .line 110
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    .line 112
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    .line 118
    iput-object v1, p0, Lcom/android/stk2/StkAppService;->mMessageHandler:Landroid/os/Handler;

    .line 120
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    .line 121
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->mIsMainMenu:Z

    .line 125
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->mIsStartedByUser:Z

    .line 131
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->mIsProxyChanged:Z

    .line 133
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->mIsSystemShutdown:Z

    .line 184
    const/16 v0, 0x1388

    iput v0, p0, Lcom/android/stk2/StkAppService;->DELAY_TIME_FOR_RESET:I

    .line 197
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/android/stk2/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    .line 377
    return-void
.end method

.method static synthetic access$1000(Lcom/android/stk2/StkAppService;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;
    .param p1, "x1"    # Landroid/os/Bundle;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/stk2/StkAppService;->handleCmdResponse(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/android/stk2/StkAppService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->callDelayedMsg()V

    return-void
.end method

.method static synthetic access$1200(Lcom/android/stk2/StkAppService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->handleSessionEnd()V

    return-void
.end method

.method static synthetic access$1300(Lcom/android/stk2/StkAppService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/stk2/StkAppService;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->handleDelayedCmd()V

    return-void
.end method

.method static synthetic access$1500(Lcom/android/stk2/StkAppService;Lcom/android/internal/telephony/cat/CatEventDownload;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/CatEventDownload;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/stk2/StkAppService;->handleEvent(Lcom/android/internal/telephony/cat/CatEventDownload;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/stk2/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/stk2/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/stk2/StkAppService;->launchBrowser(Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/stk2/StkAppService;)Lcom/android/internal/telephony/cat/CatCmdMessage;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/stk2/StkAppService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;
    .param p1, "x1"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/android/stk2/StkAppService;->mIsStartedByUser:Z

    return p1
.end method

.method static synthetic access$400(Lcom/android/stk2/StkAppService;Lcom/android/internal/telephony/cat/Menu;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/Menu;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/stk2/StkAppService;->launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/stk2/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/stk2/StkAppService;->isCmdInteractive(Lcom/android/internal/telephony/cat/CatCmdMessage;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/stk2/StkAppService;Lcom/android/internal/telephony/cat/CatCmdMessage;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;
    .param p1, "x1"    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Lcom/android/stk2/StkAppService;->handleCmd(Lcom/android/internal/telephony/cat/CatCmdMessage;)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/stk2/StkAppService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/android/stk2/StkAppService;->mCmdInProgress:Z

    return v0
.end method

.method static synthetic access$702(Lcom/android/stk2/StkAppService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;
    .param p1, "x1"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/android/stk2/StkAppService;->mCmdInProgress:Z

    return p1
.end method

.method static synthetic access$800(Lcom/android/stk2/StkAppService;)Ljava/util/LinkedList;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/android/stk2/StkAppService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/android/stk2/StkAppService;->responseNeeded:Z

    return v0
.end method

.method static synthetic access$902(Lcom/android/stk2/StkAppService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkAppService;
    .param p1, "x1"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/android/stk2/StkAppService;->responseNeeded:Z

    return p1
.end method

.method private callDelayedMsg()V
    .locals 2

    .prologue
    .line 638
    iget-object v1, p0, Lcom/android/stk2/StkAppService;->mServiceHandler:Lcom/android/stk2/StkAppService$ServiceHandler;

    invoke-virtual {v1}, Lcom/android/stk2/StkAppService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 639
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 640
    iget-object v1, p0, Lcom/android/stk2/StkAppService;->mServiceHandler:Lcom/android/stk2/StkAppService$ServiceHandler;

    invoke-virtual {v1, v0}, Lcom/android/stk2/StkAppService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 641
    return-void
.end method

.method private canLaunchBrowser()Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 940
    const-string v8, "canLaunchBrowser"

    invoke-static {p0, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 949
    invoke-virtual {p0}, Lcom/android/stk2/StkAppService;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 950
    .local v1, "iContext":Landroid/content/Context;
    const/4 v3, 0x0

    .line 951
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 955
    .local v0, "am":Landroid/app/ActivityManager;
    const-string v8, "activity"

    invoke-virtual {v1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "am":Landroid/app/ActivityManager;
    check-cast v0, Landroid/app/ActivityManager;

    .line 956
    .restart local v0    # "am":Landroid/app/ActivityManager;
    if-nez v0, :cond_1

    .line 957
    const-string v7, "Activity Manager is NULL"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 981
    :cond_0
    :goto_0
    return v6

    .line 961
    :cond_1
    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v5

    .line 962
    .local v5, "runningTaskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v5, :cond_2

    .line 964
    const-string v7, "runningTaskInfo == null"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 968
    :cond_2
    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 969
    .local v4, "runInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    const-string v8, "Getting first Running task info"

    invoke-static {p0, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 972
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 973
    .local v2, "packName":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Value of package name"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p0, v8}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 974
    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.android.browser"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.sec.android.app.sbrowser"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 976
    :cond_3
    const-string v6, "Package Name matches"

    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move v6, v7

    .line 977
    goto :goto_0
.end method

.method private canLaunchDisptextDialog()Z
    .locals 12

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 884
    const-string v10, "canLaunchDisptextDialog"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 885
    invoke-virtual {p0}, Lcom/android/stk2/StkAppService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 886
    .local v2, "iContext":Landroid/content/Context;
    const/4 v5, 0x0

    .line 887
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/4 v0, 0x0

    .line 892
    .local v0, "am":Landroid/app/ActivityManager;
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 893
    if-nez v5, :cond_0

    .line 894
    const-string v9, "Package Manager is NULL"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 934
    :goto_0
    return v8

    .line 899
    :cond_0
    const-string v10, "activity"

    invoke-virtual {v2, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "am":Landroid/app/ActivityManager;
    check-cast v0, Landroid/app/ActivityManager;

    .line 900
    .restart local v0    # "am":Landroid/app/ActivityManager;
    if-nez v0, :cond_1

    .line 901
    const-string v9, "Activity Manager is NULL"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 905
    :cond_1
    new-instance v10, Landroid/content/Intent;

    const-string v11, "android.intent.action.MAIN"

    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v11, "android.intent.category.HOME"

    invoke-virtual {v10, v11}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v5, v10, v9}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 907
    .local v1, "homeInfo":Landroid/content/pm/ResolveInfo;
    if-nez v1, :cond_2

    .line 908
    const-string v9, "homdInfo is NULL"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 911
    :cond_2
    iget-object v10, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 912
    .local v3, "origPackName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Value of original Packgage name"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 914
    invoke-virtual {v0, v8}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v7

    .line 915
    .local v7, "runningTaskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v7, :cond_3

    .line 917
    const-string v9, "runningTaskInfo == null"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 921
    :cond_3
    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 922
    .local v6, "runInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    const-string v10, "Getting first Running task info"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 925
    iget-object v10, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 926
    .local v4, "packName":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Value of package name"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 927
    iget-object v10, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    iget-object v11, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    iget-object v10, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    const-string v11, "com.android.stk2"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 929
    :cond_4
    const-string v9, "Package Name matches"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v8, v9

    .line 934
    goto/16 :goto_0
.end method

.method private distroyMenuActivity()V
    .locals 4

    .prologue
    .line 1206
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1207
    .local v1, "newIntent":Landroid/content/Intent;
    const-string v2, "com.android.stk2"

    const-string v3, "com.android.stk2.StkMenuActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1208
    const/high16 v0, 0x14000000

    .line 1211
    .local v0, "intentFlags":I
    sget-object v2, Lcom/android/stk2/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk2/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk2/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk2/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v0, v2

    .line 1213
    const-string v2, "STATE"

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1214
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1215
    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1216
    return-void
.end method

.method private getFlagActivityNoUserAction(Lcom/android/stk2/StkAppService$InitiatedByUserAction;)I
    .locals 3
    .param p1, "userAction"    # Lcom/android/stk2/StkAppService$InitiatedByUserAction;

    .prologue
    const/4 v1, 0x0

    .line 1181
    sget-object v0, Lcom/android/stk2/StkAppService$InitiatedByUserAction;->yes:Lcom/android/stk2/StkAppService$InitiatedByUserAction;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-boolean v2, p0, Lcom/android/stk2/StkAppService;->mMenuIsVisibile:Z

    or-int/2addr v0, v2

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/high16 v1, 0x40000

    goto :goto_1
.end method

.method static getInstance()Lcom/android/stk2/StkAppService;
    .locals 1

    .prologue
    .line 345
    sget-object v0, Lcom/android/stk2/StkAppService;->sInstance:Lcom/android/stk2/StkAppService;

    return-object v0
.end method

.method private getItemName(I)Ljava/lang/String;
    .locals 5
    .param p1, "itemId"    # I

    .prologue
    const/4 v3, 0x0

    .line 1598
    iget-object v4, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v4}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v2

    .line 1599
    .local v2, "menu":Lcom/android/internal/telephony/cat/Menu;
    if-nez v2, :cond_1

    .line 1607
    :cond_0
    :goto_0
    return-object v3

    .line 1602
    :cond_1
    iget-object v4, v2, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/internal/telephony/cat/Item;

    .line 1603
    .local v1, "item":Lcom/android/internal/telephony/cat/Item;
    iget v4, v1, Lcom/android/internal/telephony/cat/Item;->id:I

    if-ne v4, p1, :cond_2

    .line 1604
    iget-object v3, v1, Lcom/android/internal/telephony/cat/Item;->text:Ljava/lang/String;

    goto :goto_0
.end method

.method private handleCmd(Lcom/android/internal/telephony/cat/CatCmdMessage;)V
    .locals 14
    .param p1, "cmdMsg"    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    const/16 v11, 0x18

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 689
    if-nez p1, :cond_1

    .line 873
    :cond_0
    :goto_0
    return-void

    .line 693
    :cond_1
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-nez v7, :cond_2

    .line 694
    invoke-static {v9}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 695
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-nez v7, :cond_2

    .line 696
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 697
    .local v0, "args":Landroid/os/Bundle;
    const-string v7, "op"

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 698
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->TERMINAL_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    .line 699
    .local v5, "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v7, "response id"

    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 700
    const-string v7, "error code"

    invoke-virtual {v5}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 701
    const-string v7, "additional info"

    invoke-virtual {v0, v7, v12}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 702
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk2/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk2/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 707
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    :cond_2
    iput-object p1, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 708
    const/4 v6, 0x1

    .line 711
    .local v6, "waitForUsersResponse":Z
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 712
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v7

    sget-object v8, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SET_UP_MENU:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    if-eq v7, v8, :cond_3

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v7

    sget-object v8, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->SELECT_ITEM:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    if-ne v7, v8, :cond_5

    .line 714
    :cond_3
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->unlockMenuActivityBlock()V

    .line 719
    :goto_1
    sget-object v7, Lcom/android/stk2/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 866
    :cond_4
    :goto_2
    if-nez v6, :cond_0

    .line 867
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-eqz v7, :cond_10

    .line 868
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->callDelayedMsg()V

    goto :goto_0

    .line 716
    :cond_5
    invoke-virtual {p0}, Lcom/android/stk2/StkAppService;->lockMenuActivityBlock()V

    goto :goto_1

    .line 721
    :pswitch_0
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v4

    .line 722
    .local v4, "msg":Lcom/android/internal/telephony/cat/TextMessage;
    iget-boolean v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->responseNeeded:Z

    iput-boolean v7, p0, Lcom/android/stk2/StkAppService;->responseNeeded:Z

    .line 723
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->lastSelectedItem:Ljava/lang/String;

    if-eqz v7, :cond_6

    .line 724
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->lastSelectedItem:Ljava/lang/String;

    iput-object v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    .line 733
    :goto_3
    iget-boolean v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->isHighPriority:Z

    if-eqz v7, :cond_8

    .line 734
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->launchTextDialog()V

    goto :goto_2

    .line 725
    :cond_6
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-eqz v7, :cond_7

    .line 726
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/Menu;->title:Ljava/lang/String;

    iput-object v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    goto :goto_3

    .line 729
    :cond_7
    const-string v7, ""

    iput-object v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    goto :goto_3

    .line 735
    :cond_8
    iget-boolean v7, v4, Lcom/android/internal/telephony/cat/TextMessage;->isHighPriority:Z

    if-nez v7, :cond_4

    .line 736
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->canLaunchDisptextDialog()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 737
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->launchTextDialog()V

    goto :goto_2

    .line 740
    :cond_9
    const-string v7, "Can not display Normal Priority text"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 741
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 742
    .restart local v0    # "args":Landroid/os/Bundle;
    const-string v7, "op"

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 743
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->TERMINAL_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    .line 744
    .restart local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v7, "response id"

    invoke-virtual {v0, v7, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 745
    const-string v7, "error code"

    invoke-virtual {v5}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 746
    const-string v7, "additional info"

    invoke-virtual {v0, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 747
    const-string v7, "additional info data"

    invoke-virtual {v0, v7, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 748
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk2/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk2/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_2

    .line 754
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v4    # "msg":Lcom/android/internal/telephony/cat/TextMessage;
    .end local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    :pswitch_1
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 755
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/stk2/StkAppService;->launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V

    goto/16 :goto_2

    .line 758
    :pswitch_2
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    iput-object v7, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 759
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 761
    invoke-static {v9}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v7

    iput-object v7, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 763
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->removeMenu()Z

    move-result v7

    if-eqz v7, :cond_c

    .line 764
    const-string v7, "Uninstall App"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 765
    iput-object v13, p0, Lcom/android/stk2/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 766
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/stk2/StkAppInstaller;->unInstall(Landroid/content/Context;)V

    .line 781
    :cond_a
    :goto_4
    iget-boolean v7, p0, Lcom/android/stk2/StkAppService;->mMenuIsVisibile:Z

    if-eqz v7, :cond_b

    .line 782
    invoke-direct {p0, v13}, Lcom/android/stk2/StkAppService;->launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V

    .line 785
    :cond_b
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 786
    .restart local v0    # "args":Landroid/os/Bundle;
    const-string v7, "op"

    invoke-virtual {v0, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 787
    const-string v7, "response id"

    const/16 v8, 0xe

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 788
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk2/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk2/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 789
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-eqz v7, :cond_4

    iget-boolean v7, p0, Lcom/android/stk2/StkAppService;->mIsSystemShutdown:Z

    if-nez v7, :cond_4

    .line 790
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v7, v9}, Lcom/android/internal/telephony/cat/AppInterface;->sentTerminalResponseForSetupMenu(Z)V

    goto/16 :goto_2

    .line 768
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_c
    const-string v7, "Install App"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 770
    const-string v7, "CHU"

    iget-object v8, p0, Lcom/android/stk2/StkAppService;->salesCode:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 771
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const-string v8, "phone2_on"

    invoke-static {v7, v8, v9}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 772
    .local v2, "mSimActive":I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "[STK2]DB in Settings, mSimActive="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 773
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-eqz v7, :cond_a

    if-ne v9, v2, :cond_a

    .line 774
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/stk2/StkAppInstaller;->install(Landroid/content/Context;)V

    goto :goto_4

    .line 778
    .end local v2    # "mSimActive":I
    :cond_d
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/android/stk2/StkAppInstaller;->install(Landroid/content/Context;)V

    goto :goto_4

    .line 794
    :pswitch_3
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->launchInputActivity()V

    goto/16 :goto_2

    .line 797
    :pswitch_4
    const/4 v6, 0x0

    .line 799
    goto/16 :goto_2

    .line 804
    :pswitch_5
    const/4 v6, 0x0

    .line 805
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->launchEventMessage()V

    goto/16 :goto_2

    .line 808
    :pswitch_6
    const/4 v6, 0x0

    .line 809
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v8, "CscFeature_RIL_RemoveToastDuringStkRefresh"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 810
    const-string v7, "Do not display a toast for SIM Refresh"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 813
    :pswitch_7
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getBrowserSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->mode:Lcom/android/internal/telephony/cat/LaunchBrowserMode;

    sget-object v8, Lcom/android/internal/telephony/cat/LaunchBrowserMode;->LAUNCH_IF_NOT_ALREADY_LAUNCHED:Lcom/android/internal/telephony/cat/LaunchBrowserMode;

    if-ne v7, v8, :cond_e

    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->canLaunchBrowser()Z

    move-result v7

    if-nez v7, :cond_e

    .line 814
    const-string v7, "Launch Browser mode is LAUNCH_IF_NOT_ALREADY_LAUNCHED and browser is already launched"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 815
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 816
    .local v1, "argsBrowser":Landroid/os/Bundle;
    const-string v7, "op"

    invoke-virtual {v1, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 817
    sget-object v5, Lcom/android/internal/telephony/cat/ResultCode;->LAUNCH_BROWSER_ERROR:Lcom/android/internal/telephony/cat/ResultCode;

    .line 818
    .restart local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v7, "response id"

    invoke-virtual {v1, v7, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 819
    const-string v7, "error code"

    invoke-virtual {v5}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 820
    const-string v7, "additional info"

    invoke-virtual {v1, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 821
    const-string v7, "additional info data"

    invoke-virtual {v1, v7, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 822
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk2/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk2/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_2

    .line 825
    .end local v1    # "argsBrowser":Landroid/os/Bundle;
    .end local v5    # "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    :cond_e
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v7

    iput-boolean v9, v7, Lcom/android/internal/telephony/cat/TextMessage;->userClear:Z

    .line 826
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/stk2/StkAppService;->launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V

    goto/16 :goto_2

    .line 830
    :pswitch_8
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCallSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    iget-object v7, v7, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-nez v7, :cond_f

    .line 832
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    const v8, 0x7f06000d

    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 833
    .local v3, "message":Ljava/lang/CharSequence;
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCallSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    .line 836
    .end local v3    # "message":Ljava/lang/CharSequence;
    :cond_f
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCallSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;

    move-result-object v7

    iget-object v7, v7, Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;->confirmMsg:Lcom/android/internal/telephony/cat/TextMessage;

    invoke-direct {p0, v7}, Lcom/android/stk2/StkAppService;->launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V

    goto/16 :goto_2

    .line 839
    :pswitch_9
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->launchToneDialog()V

    goto/16 :goto_2

    .line 842
    :pswitch_a
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getNumberOfEventList()I

    move-result v7

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getEventList()[I

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/android/stk2/StkAppService;->processSetEventList(I[I)V

    goto/16 :goto_2

    .line 845
    :pswitch_b
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getinitLanguage()Z

    move-result v8

    invoke-direct {p0, v7, v8}, Lcom/android/stk2/StkAppService;->processLanguageNotification(Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 848
    :pswitch_c
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/stk2/StkAppService;->launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V

    .line 849
    const-string v7, "OPEN CHANNEL"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 852
    :pswitch_d
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/android/stk2/StkAppService;->launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V

    .line 853
    const-string v7, "CLOSE CHANNEL"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 858
    :pswitch_e
    const/4 v6, 0x0

    .line 859
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->launchEventMessage()V

    .line 860
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 870
    :cond_10
    iput-boolean v12, p0, Lcom/android/stk2/StkAppService;->mCmdInProgress:Z

    goto/16 :goto_0

    .line 719
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_2
        :pswitch_6
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_e
    .end packed-switch
.end method

.method private handleCmdResponse(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x1

    .line 1024
    iget-object v10, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-nez v10, :cond_1

    .line 1167
    :cond_0
    :goto_0
    return-void

    .line 1027
    :cond_1
    new-instance v8, Lcom/android/internal/telephony/cat/CatResponseMessage;

    iget-object v10, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-direct {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;-><init>(Lcom/android/internal/telephony/cat/CatCmdMessage;)V

    .line 1030
    .local v8, "resMsg":Lcom/android/internal/telephony/cat/CatResponseMessage;
    const-string v10, "help"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 1032
    .local v5, "helpRequired":Z
    const-string v10, "response id"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    packed-switch v10, :pswitch_data_0

    .line 1157
    :pswitch_0
    const-string v10, "Unknown result id"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1034
    :pswitch_1
    const-string v10, "RES_ID_MENU_SELECTION"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1035
    const-string v10, "menu selection"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 1036
    .local v7, "menuSelection":I
    sget-object v10, Lcom/android/stk2/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    iget-object v11, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_1

    .line 1161
    .end local v7    # "menuSelection":I
    :cond_2
    :goto_1
    :pswitch_2
    iget-object v10, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-nez v10, :cond_3

    .line 1162
    invoke-static {v12}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v10

    iput-object v10, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 1164
    :cond_3
    iget-object v10, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-eqz v10, :cond_0

    .line 1165
    iget-object v10, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v10, v8}, Lcom/android/internal/telephony/cat/AppInterface;->onCmdResponse(Lcom/android/internal/telephony/cat/CatResponseMessage;)V

    goto :goto_0

    .line 1039
    .restart local v7    # "menuSelection":I
    :pswitch_3
    invoke-direct {p0, v7}, Lcom/android/stk2/StkAppService;->getItemName(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/android/stk2/StkAppService;->lastSelectedItem:Ljava/lang/String;

    .line 1040
    if-eqz v5, :cond_4

    .line 1041
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->HELP_INFO_REQUIRED:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1045
    :goto_2
    invoke-virtual {v8, v7}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setMenuSelection(I)V

    goto :goto_1

    .line 1043
    :cond_4
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto :goto_2

    .line 1052
    .end local v7    # "menuSelection":I
    :pswitch_4
    const-string v10, "RES_ID_INPUT"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1053
    const-string v10, "input"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1054
    .local v6, "input":Ljava/lang/String;
    iget-object v10, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geInput()Lcom/android/internal/telephony/cat/Input;

    move-result-object v2

    .line 1055
    .local v2, "cmdInput":Lcom/android/internal/telephony/cat/Input;
    if-eqz v2, :cond_5

    iget-boolean v10, v2, Lcom/android/internal/telephony/cat/Input;->yesNo:Z

    if-eqz v10, :cond_5

    .line 1056
    const-string v10, "YES"

    invoke-virtual {v6, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    .line 1058
    .local v9, "yesNoSelection":Z
    invoke-virtual {v8, v9}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setYesNo(Z)V

    goto :goto_1

    .line 1060
    .end local v9    # "yesNoSelection":Z
    :cond_5
    if-eqz v5, :cond_6

    .line 1061
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->HELP_INFO_REQUIRED:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto :goto_1

    .line 1063
    :cond_6
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1064
    invoke-virtual {v8, v6}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setInput(Ljava/lang/String;)V

    goto :goto_1

    .line 1069
    .end local v2    # "cmdInput":Lcom/android/internal/telephony/cat/Input;
    .end local v6    # "input":Ljava/lang/String;
    :pswitch_5
    const-string v10, "RES_ID_CONFIRM"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1070
    const-string v10, "confirm"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 1071
    .local v3, "confirmed":Z
    sget-object v10, Lcom/android/stk2/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    iget-object v11, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v11

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_2

    :pswitch_6
    goto :goto_1

    .line 1073
    :pswitch_7
    if-eqz v3, :cond_7

    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    :goto_3
    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    :cond_7
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    goto :goto_3

    .line 1077
    :pswitch_8
    if-eqz v3, :cond_8

    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    :goto_4
    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1079
    if-eqz v3, :cond_2

    .line 1080
    iput-boolean v12, p0, Lcom/android/stk2/StkAppService;->launchBrowser:Z

    .line 1081
    iget-object v10, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getBrowserSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    move-result-object v10

    iput-object v10, p0, Lcom/android/stk2/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    goto/16 :goto_1

    .line 1077
    :cond_8
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    goto :goto_4

    .line 1085
    :pswitch_9
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1086
    invoke-virtual {v8, v3}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setConfirmation(Z)V

    .line 1087
    if-eqz v3, :cond_2

    .line 1088
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->launchCallMsg()V

    goto/16 :goto_1

    .line 1092
    :pswitch_a
    const-string v10, "Open Channel Command response"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1093
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1094
    invoke-virtual {v8, v3}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setConfirmation(Z)V

    .line 1095
    if-eqz v3, :cond_9

    .line 1096
    const-string v10, "User Confirmed"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1097
    const-string v10, "OPEN CHANNEL"

    invoke-direct {p0, v10}, Lcom/android/stk2/StkAppService;->launchBIPChannel(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1099
    :cond_9
    const-string v10, "User did not Confirm!"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1103
    :pswitch_b
    const-string v10, "Close Channel Command response"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1104
    if-eqz v3, :cond_a

    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    :goto_5
    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1106
    invoke-virtual {v8, v3}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setConfirmation(Z)V

    .line 1107
    if-eqz v3, :cond_b

    .line 1108
    const-string v10, "User Confirmed"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1109
    const-string v10, "CLOSE CHANNEL"

    invoke-direct {p0, v10}, Lcom/android/stk2/StkAppService;->launchBIPChannel(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1104
    :cond_a
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    goto :goto_5

    .line 1111
    :cond_b
    const-string v10, "User did not Confirm!"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1119
    .end local v3    # "confirmed":Z
    :pswitch_c
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1122
    :pswitch_d
    const-string v10, "RES_ID_BACKWARD"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1123
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->BACKWARD_MOVE_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1126
    :pswitch_e
    const-string v10, "RES_ID_END_SESSION"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1127
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->UICC_SESSION_TERM_BY_USER:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1130
    :pswitch_f
    const-string v10, "RES_ID_TIMEOUT"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1135
    iget-object v10, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v10

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->value()I

    move-result v10

    sget-object v11, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->DISPLAY_TEXT:Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    invoke-virtual {v11}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->value()I

    move-result v11

    if-ne v10, v11, :cond_c

    iget-object v10, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v10

    iget-boolean v10, v10, Lcom/android/internal/telephony/cat/TextMessage;->userClear:Z

    if-nez v10, :cond_c

    .line 1138
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->OK:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1140
    :cond_c
    sget-object v10, Lcom/android/internal/telephony/cat/ResultCode;->NO_RESPONSE_FROM_USER:Lcom/android/internal/telephony/cat/ResultCode;

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    goto/16 :goto_1

    .line 1144
    :pswitch_10
    const-string v10, "RES_ID_GENERAL_ERROR"

    invoke-static {p0, v10}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1145
    const-string v10, "error code"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 1146
    .local v4, "errorCode":I
    const-string v10, "additional info"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1149
    .local v0, "additionalInfo":Z
    invoke-static {v4}, Lcom/android/internal/telephony/cat/ResultCode;->fromInt(I)Lcom/android/internal/telephony/cat/ResultCode;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setResultCode(Lcom/android/internal/telephony/cat/ResultCode;)V

    .line 1150
    invoke-virtual {v8, v0}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setAdditionalInfo(Z)V

    .line 1151
    if-ne v0, v12, :cond_2

    .line 1152
    const-string v10, "additional info data"

    invoke-virtual {p1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1153
    .local v1, "additionalInfoData":I
    invoke-virtual {v8, v1}, Lcom/android/internal/telephony/cat/CatResponseMessage;->setAdditionalInfoData(I)V

    goto/16 :goto_1

    .line 1032
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_c
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_10
    .end packed-switch

    .line 1036
    :pswitch_data_1
    .packed-switch 0x6
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 1071
    :pswitch_data_2
    .packed-switch 0x8
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method private handleDelayedCmd()V
    .locals 2

    .prologue
    .line 564
    iget-object v1, p0, Lcom/android/stk2/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 565
    iget-object v1, p0, Lcom/android/stk2/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/stk2/StkAppService$DelayedCmd;

    .line 566
    .local v0, "cmd":Lcom/android/stk2/StkAppService$DelayedCmd;
    iget v1, v0, Lcom/android/stk2/StkAppService$DelayedCmd;->id:I

    packed-switch v1, :pswitch_data_0

    .line 577
    .end local v0    # "cmd":Lcom/android/stk2/StkAppService$DelayedCmd;
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 568
    .restart local v0    # "cmd":Lcom/android/stk2/StkAppService$DelayedCmd;
    :pswitch_1
    iget-object v1, v0, Lcom/android/stk2/StkAppService$DelayedCmd;->msg:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-direct {p0, v1}, Lcom/android/stk2/StkAppService;->handleCmd(Lcom/android/internal/telephony/cat/CatCmdMessage;)V

    goto :goto_0

    .line 571
    :pswitch_2
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->handleSessionEnd()V

    goto :goto_0

    .line 566
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private handleEvent(Lcom/android/internal/telephony/cat/CatEventDownload;)V
    .locals 10
    .param p1, "event"    # Lcom/android/internal/telephony/cat/CatEventDownload;

    .prologue
    const/16 v9, 0x82

    const/16 v8, 0x81

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 581
    const/4 v0, 0x0

    .line 583
    .local v0, "additionalInfo":[B
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    iget-boolean v3, p0, Lcom/android/stk2/StkAppService;->mIsProxyChanged:Z

    if-ne v3, v6, :cond_0

    .line 585
    const-string v3, "received browser termination event and proxy was chaned, restore proxy"

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 586
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 587
    .local v1, "cm":Landroid/net/ConnectivityManager;
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mBackupProxy:Landroid/net/ProxyInfo;

    invoke-virtual {v1, v3}, Landroid/net/ConnectivityManager;->setGlobalProxy(Landroid/net/ProxyInfo;)V

    .line 588
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->mBackupProxy:Landroid/net/ProxyInfo;

    .line 589
    iput-boolean v5, p0, Lcom/android/stk2/StkAppService;->mIsProxyChanged:Z

    .line 592
    .end local v1    # "cm":Landroid/net/ConnectivityManager;
    :cond_0
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v4

    aget-boolean v3, v3, v4

    if-ne v3, v6, :cond_1

    .line 593
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 635
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 595
    :pswitch_1
    const-string v3, "send user activity event to RIL"

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 596
    new-instance v2, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-direct {v2, v3, v9, v8, v4}, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;-><init>(III[B)V

    .line 598
    .local v2, "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v3, v2}, Lcom/android/internal/telephony/cat/AppInterface;->onEventDownload(Lcom/android/internal/telephony/cat/CatEnvelopeMessage;)V

    .line 599
    const-string v3, "gsm.sim.userEvent"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 600
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v4

    aput-boolean v5, v3, v4

    goto :goto_0

    .line 603
    .end local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    :pswitch_2
    const-string v3, "send Idle screen event to RIL"

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 604
    new-instance v2, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;

    const/4 v3, 0x5

    const/4 v4, 0x0

    invoke-direct {v2, v3, v7, v8, v4}, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;-><init>(III[B)V

    .line 606
    .restart local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v3, v2}, Lcom/android/internal/telephony/cat/AppInterface;->onEventDownload(Lcom/android/internal/telephony/cat/CatEnvelopeMessage;)V

    .line 607
    const-string v3, "gsm.sim.screenEvent"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getEvent()I

    move-result v4

    aput-boolean v5, v3, v4

    goto :goto_0

    .line 611
    .end local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    :pswitch_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "send setting language event to RIL, language = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 612
    const/4 v3, 0x4

    new-array v0, v3, [B

    .line 613
    const/16 v3, 0x2d

    aput-byte v3, v0, v5

    .line 614
    aput-byte v7, v0, v6

    .line 615
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v7

    .line 616
    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 617
    new-instance v2, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;

    const/4 v3, 0x7

    invoke-direct {v2, v3, v9, v8, v0}, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;-><init>(III[B)V

    .line 619
    .restart local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v3, v2}, Lcom/android/internal/telephony/cat/AppInterface;->onEventDownload(Lcom/android/internal/telephony/cat/CatEnvelopeMessage;)V

    goto/16 :goto_0

    .line 622
    .end local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    :pswitch_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "browser termination event to RIL, cause = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getBrowserTerminationCause()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 623
    const/4 v3, 0x3

    new-array v0, v3, [B

    .line 624
    const/16 v3, -0x4c

    aput-byte v3, v0, v5

    .line 625
    aput-byte v6, v0, v6

    .line 626
    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatEventDownload;->getBrowserTerminationCause()I

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, v0, v7

    .line 627
    new-instance v2, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;

    const/16 v3, 0x8

    invoke-direct {v2, v3, v9, v8, v0}, Lcom/android/internal/telephony/cat/CatEnvelopeMessage;-><init>(III[B)V

    .line 629
    .restart local v2    # "envelope":Lcom/android/internal/telephony/cat/CatEnvelopeMessage;
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v3, v2}, Lcom/android/internal/telephony/cat/AppInterface;->onEventDownload(Lcom/android/internal/telephony/cat/CatEnvelopeMessage;)V

    goto/16 :goto_0

    .line 593
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private handleSessionEnd()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 644
    const-string v0, "handleSessionEnd()"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 645
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    iput-object v0, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    .line 646
    iput-object v3, p0, Lcom/android/stk2/StkAppService;->lastSelectedItem:Ljava/lang/String;

    .line 647
    const-string v0, "unlockMenuActivityBlock()"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 648
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->unlockMenuActivityBlock()V

    .line 652
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v0}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk2/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 654
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->mIsMainMenu:Z

    .line 657
    :cond_0
    iget-boolean v0, p0, Lcom/android/stk2/StkAppService;->mIsStartedByUser:Z

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->isRunningStk()Z

    move-result v0

    if-ne v0, v2, :cond_4

    .line 658
    const-string v0, "distroyMenuActivity"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 659
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->distroyMenuActivity()V

    .line 664
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-eqz v0, :cond_5

    .line 665
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->callDelayedMsg()V

    .line 670
    :goto_1
    iget-boolean v0, p0, Lcom/android/stk2/StkAppService;->launchBrowser:Z

    if-eqz v0, :cond_2

    .line 671
    iput-boolean v1, p0, Lcom/android/stk2/StkAppService;->launchBrowser:Z

    .line 672
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mBrowserSettings:Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    invoke-direct {p0, v0}, Lcom/android/stk2/StkAppService;->launchBrowser(Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V

    .line 675
    :cond_2
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 677
    :try_start_0
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 678
    const-string v0, "before release wakeup"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 679
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 682
    :cond_3
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 685
    invoke-virtual {p0}, Lcom/android/stk2/StkAppService;->enableKeyguard()V

    .line 686
    return-void

    .line 660
    :cond_4
    iget-boolean v0, p0, Lcom/android/stk2/StkAppService;->mMenuIsVisibile:Z

    if-eqz v0, :cond_1

    .line 661
    invoke-direct {p0, v3}, Lcom/android/stk2/StkAppService;->launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V

    goto :goto_0

    .line 667
    :cond_5
    iput-boolean v1, p0, Lcom/android/stk2/StkAppService;->mCmdInProgress:Z

    goto :goto_1

    .line 682
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/stk2/StkAppService;->lock:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private isCmdInteractive(Lcom/android/internal/telephony/cat/CatCmdMessage;)Z
    .locals 2
    .param p1, "cmd"    # Lcom/android/internal/telephony/cat/CatCmdMessage;

    .prologue
    .line 548
    sget-object v0, Lcom/android/stk2/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    invoke-virtual {p1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 559
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 556
    :pswitch_0
    const-string v0, "Command is Informative!"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 557
    const/4 v0, 0x0

    goto :goto_0

    .line 548
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isRunningStk()Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 987
    const-string v7, "isRunningStk"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 988
    invoke-virtual {p0}, Lcom/android/stk2/StkAppService;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    .line 989
    .local v2, "iContext":Landroid/content/Context;
    const/4 v0, 0x0

    .line 994
    .local v0, "am":Landroid/app/ActivityManager;
    const-string v7, "activity"

    invoke-virtual {v2, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "am":Landroid/app/ActivityManager;
    check-cast v0, Landroid/app/ActivityManager;

    .line 995
    .restart local v0    # "am":Landroid/app/ActivityManager;
    if-nez v0, :cond_1

    .line 996
    const-string v6, "Activity Manager is NULL"

    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1019
    :cond_0
    :goto_0
    return v5

    .line 1000
    :cond_1
    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v4

    .line 1001
    .local v4, "runningTaskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-nez v4, :cond_2

    .line 1003
    const-string v6, "runningTaskInfo == null"

    invoke-static {p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 1007
    :cond_2
    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1008
    .local v3, "runInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    const-string v7, "Getting first Running task info"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1010
    iget-object v7, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 1011
    .local v1, "className":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Value of class name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1012
    iget-object v7, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "com.android.stk2.StkMenuActivity"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1014
    const-string v5, "Class Name matches"

    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move v5, v6

    .line 1015
    goto :goto_0
.end method

.method private launchBIPChannel(Ljava/lang/String;)V
    .locals 5
    .param p1, "Str"    # Ljava/lang/String;

    .prologue
    .line 1662
    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v0

    .line 1663
    .local v0, "msg":Lcom/android/internal/telephony/cat/TextMessage;
    iget-object v2, v0, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 1664
    :cond_0
    const-string v2, "Sending..."

    iput-object v2, v0, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    .line 1667
    :cond_1
    iput-object p1, v0, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    .line 1668
    const-string v2, "Launch BIP Channel"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1669
    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v0, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 1671
    .local v1, "toast":Landroid/widget/Toast;
    const/16 v2, 0x50

    const/4 v3, 0x0

    const/16 v4, 0x32

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/Toast;->setGravity(III)V

    .line 1672
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1673
    return-void
.end method

.method private launchBrowser(Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;)V
    .locals 12
    .param p1, "settings"    # Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;

    .prologue
    const/high16 v11, 0x4000000

    .line 1310
    if-nez p1, :cond_0

    .line 1376
    :goto_0
    return-void

    .line 1314
    :cond_0
    const/4 v4, 0x0

    .line 1316
    .local v4, "intent":Landroid/content/Intent;
    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    if-nez v9, :cond_1

    .line 1317
    const-string v9, "url is null, so try to get default url from browser"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1318
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v9, "android.intent.action.STK_BROWSER_GET_HOMEPAGE"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1319
    .restart local v4    # "intent":Landroid/content/Intent;
    iget-object v9, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 1323
    :cond_1
    iget-object v3, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->gatewayProxy:Ljava/lang/String;

    .line 1324
    .local v3, "gatewayProxy":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 1325
    const-string v9, "gateway/proxy informaion is in launch browser cmd, change proxy"

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1326
    iget-object v9, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    const-string v10, "connectivity"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 1327
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getGlobalProxy()Landroid/net/ProxyInfo;

    move-result-object v9

    iput-object v9, p0, Lcom/android/stk2/StkAppService;->mBackupProxy:Landroid/net/ProxyInfo;

    .line 1328
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getProxy()Landroid/net/ProxyInfo;

    move-result-object v8

    .line 1329
    .local v8, "tempProxy":Landroid/net/ProxyInfo;
    const/16 v6, 0x50

    .line 1330
    .local v6, "port":I
    const-string v2, ""

    .line 1332
    .local v2, "exclusionList":Ljava/lang/String;
    if-eqz v8, :cond_2

    .line 1333
    invoke-virtual {v8}, Landroid/net/ProxyInfo;->getPort()I

    move-result v6

    .line 1334
    invoke-virtual {v8}, Landroid/net/ProxyInfo;->getExclusionListAsString()Ljava/lang/String;

    move-result-object v2

    .line 1336
    :cond_2
    new-instance v7, Landroid/net/ProxyInfo;

    invoke-direct {v7, v3, v6, v2}, Landroid/net/ProxyInfo;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 1337
    .local v7, "proxy":Landroid/net/ProxyInfo;
    invoke-virtual {v0, v7}, Landroid/net/ConnectivityManager;->setGlobalProxy(Landroid/net/ProxyInfo;)V

    .line 1338
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/android/stk2/StkAppService;->mIsProxyChanged:Z

    .line 1339
    const-string v9, "gsm.sim.browserEvent"

    const-string v10, "1"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1342
    .end local v0    # "cm":Landroid/net/ConnectivityManager;
    .end local v2    # "exclusionList":Ljava/lang/String;
    .end local v6    # "port":I
    .end local v7    # "proxy":Landroid/net/ProxyInfo;
    .end local v8    # "tempProxy":Landroid/net/ProxyInfo;
    :cond_3
    new-instance v4, Landroid/content/Intent;

    .end local v4    # "intent":Landroid/content/Intent;
    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1344
    .restart local v4    # "intent":Landroid/content/Intent;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "settings.url = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1345
    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    const-string v10, "http://"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    const-string v10, "https://"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    const-string v10, "dss://"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1346
    :cond_4
    iget-object v9, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1352
    .local v1, "data":Landroid/net/Uri;
    :goto_1
    invoke-virtual {v4, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1354
    const/high16 v9, 0x10000000

    invoke-virtual {v4, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1355
    sget-object v9, Lcom/android/stk2/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$LaunchBrowserMode:[I

    iget-object v10, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->mode:Lcom/android/internal/telephony/cat/LaunchBrowserMode;

    invoke-virtual {v10}, Lcom/android/internal/telephony/cat/LaunchBrowserMode;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 1369
    :goto_2
    invoke-virtual {p0, v4}, Lcom/android/stk2/StkAppService;->startActivity(Landroid/content/Intent;)V

    .line 1374
    const-wide/16 v10, 0x2710

    :try_start_0
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1375
    :catch_0
    move-exception v9

    goto/16 :goto_0

    .line 1348
    .end local v1    # "data":Landroid/net/Uri;
    :cond_5
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "http://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p1, Lcom/android/internal/telephony/cat/CatCmdMessage$BrowserSettings;->url:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1349
    .local v5, "modifiedUrl":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "modifiedUrl = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1350
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .restart local v1    # "data":Landroid/net/Uri;
    goto :goto_1

    .line 1357
    .end local v5    # "modifiedUrl":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {v4, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_2

    .line 1360
    :pswitch_1
    const/high16 v9, 0x8000000

    invoke-virtual {v4, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_2

    .line 1363
    :pswitch_2
    invoke-virtual {v4, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_2

    .line 1355
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private launchCallMsg()V
    .locals 6

    .prologue
    .line 1379
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v3}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCallSettings()Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;

    move-result-object v3

    iget-object v1, v3, Lcom/android/internal/telephony/cat/CatCmdMessage$CallSettings;->callMsg:Lcom/android/internal/telephony/cat/TextMessage;

    .line 1382
    .local v1, "msg":Lcom/android/internal/telephony/cat/TextMessage;
    iget-object v3, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 1383
    :cond_0
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    const v4, 0x7f06000d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1384
    .local v0, "message":Ljava/lang/CharSequence;
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    .line 1386
    .end local v0    # "message":Ljava/lang/CharSequence;
    :cond_1
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->lastSelectedItem:Ljava/lang/String;

    iput-object v3, v1, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    .line 1388
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v1, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    .line 1390
    .local v2, "toast":Landroid/widget/Toast;
    const/16 v3, 0x50

    const/4 v4, 0x0

    const/16 v5, 0x32

    invoke-virtual {v2, v3, v4, v5}, Landroid/widget/Toast;->setGravity(III)V

    .line 1391
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 1392
    return-void
.end method

.method private launchConfirmationDialog(Lcom/android/internal/telephony/cat/TextMessage;)V
    .locals 3
    .param p1, "msg"    # Lcom/android/internal/telephony/cat/TextMessage;

    .prologue
    .line 1288
    sget-object v1, Lcom/android/stk2/StkAppService$2;->$SwitchMap$com$android$internal$telephony$cat$AppInterface$CommandType:[I

    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getCmdType()Lcom/android/internal/telephony/cat/AppInterface$CommandType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/AppInterface$CommandType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1296
    iget-object v1, p0, Lcom/android/stk2/StkAppService;->lastSelectedItem:Ljava/lang/String;

    iput-object v1, p1, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    .line 1299
    :goto_0
    const-string v1, "Launch Dialog"

    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1300
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/stk2/StkDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1301
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v1, 0x50800000

    sget-object v2, Lcom/android/stk2/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk2/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk2/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk2/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1305
    const-string v1, "TEXT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1306
    invoke-virtual {p0, v0}, Lcom/android/stk2/StkAppService;->startActivity(Landroid/content/Intent;)V

    .line 1307
    return-void

    .line 1290
    .end local v0    # "newIntent":Landroid/content/Intent;
    :pswitch_0
    const-string v1, "OPEN CHANNEL"

    iput-object v1, p1, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    goto :goto_0

    .line 1293
    :pswitch_1
    const-string v1, "CLOSE CHANNEL"

    iput-object v1, p1, Lcom/android/internal/telephony/cat/TextMessage;->title:Ljava/lang/String;

    goto :goto_0

    .line 1288
    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private launchEventMessage()V
    .locals 11

    .prologue
    const/16 v10, 0x8

    .line 1248
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v7}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v2

    .line 1249
    .local v2, "msg":Lcom/android/internal/telephony/cat/TextMessage;
    if-eqz v2, :cond_0

    iget-object v7, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    if-nez v7, :cond_1

    .line 1285
    :cond_0
    :goto_0
    return-void

    .line 1252
    :cond_1
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1253
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v7}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 1254
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mServiceHandler:Lcom/android/stk2/StkAppService$ServiceHandler;

    invoke-virtual {v7}, Lcom/android/stk2/StkAppService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v6

    .line 1255
    .local v6, "wakelockMsg":Landroid/os/Message;
    iput v10, v6, Landroid/os/Message;->arg1:I

    .line 1256
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mServiceHandler:Lcom/android/stk2/StkAppService$ServiceHandler;

    const-wide/16 v8, 0x2710

    invoke-virtual {v7, v6, v8, v9}, Lcom/android/stk2/StkAppService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1258
    .end local v6    # "wakelockMsg":Landroid/os/Message;
    :cond_2
    invoke-virtual {p0}, Lcom/android/stk2/StkAppService;->disableKeyguard()V

    .line 1260
    new-instance v3, Landroid/widget/Toast;

    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v3, v7}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 1261
    .local v3, "toast":Landroid/widget/Toast;
    iget-object v7, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 1263
    .local v0, "inflate":Landroid/view/LayoutInflater;
    const/high16 v7, 0x7f030000

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1264
    .local v5, "v":Landroid/view/View;
    const v7, 0x102000b

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1266
    .local v4, "tv":Landroid/widget/TextView;
    const v7, 0x1020006

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1268
    .local v1, "iv":Landroid/widget/ImageView;
    iget-object v7, v2, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    if-eqz v7, :cond_3

    .line 1269
    iget-object v7, v2, Lcom/android/internal/telephony/cat/TextMessage;->icon:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1279
    :goto_1
    iget-object v7, v2, Lcom/android/internal/telephony/cat/TextMessage;->text:Ljava/lang/String;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1281
    invoke-virtual {v3, v5}, Landroid/widget/Toast;->setView(Landroid/view/View;)V

    .line 1282
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/widget/Toast;->setDuration(I)V

    .line 1283
    const/16 v7, 0x50

    const/4 v8, 0x0

    const/16 v9, 0x32

    invoke-virtual {v3, v7, v8, v9}, Landroid/widget/Toast;->setGravity(III)V

    .line 1284
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1271
    :cond_3
    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private launchInputActivity()V
    .locals 3

    .prologue
    .line 1219
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1220
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    sget-object v2, Lcom/android/stk2/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk2/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk2/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk2/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1222
    const-string v1, "com.android.stk2"

    const-string v2, "com.android.stk2.StkInputActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1223
    const-string v1, "INPUT"

    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geInput()Lcom/android/internal/telephony/cat/Input;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1224
    iget-object v1, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1225
    return-void
.end method

.method private launchMenuActivity(Lcom/android/internal/telephony/cat/Menu;)V
    .locals 4
    .param p1, "menu"    # Lcom/android/internal/telephony/cat/Menu;

    .prologue
    .line 1186
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1187
    .local v1, "newIntent":Landroid/content/Intent;
    const-string v2, "com.android.stk2"

    const-string v3, "com.android.stk2.StkMenuActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1188
    const/high16 v0, 0x14000000

    .line 1190
    .local v0, "intentFlags":I
    if-nez p1, :cond_0

    .line 1192
    sget-object v2, Lcom/android/stk2/StkAppService$InitiatedByUserAction;->yes:Lcom/android/stk2/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk2/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk2/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v0, v2

    .line 1194
    const-string v2, "STATE"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1201
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1202
    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1203
    return-void

    .line 1197
    :cond_0
    sget-object v2, Lcom/android/stk2/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk2/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk2/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk2/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v0, v2

    .line 1199
    const-string v2, "STATE"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private launchTextDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1228
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/android/stk2/StkDialogActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1229
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v2, 0x58800000    # 1.12589991E15f

    sget-object v3, Lcom/android/stk2/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk2/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v3}, Lcom/android/stk2/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk2/StkAppService$InitiatedByUserAction;)I

    move-result v3

    or-int/2addr v2, v3

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1234
    const-string v2, "TEXT"

    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v3}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1236
    new-instance v1, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-direct {v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>()V

    .line 1237
    .local v1, "style":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    invoke-virtual {v1, v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 1238
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1240
    invoke-virtual {p0, v0}, Lcom/android/stk2/StkAppService;->startActivity(Landroid/content/Intent;)V

    .line 1241
    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v2

    iget-object v2, v2, Lcom/android/internal/telephony/cat/TextMessage;->duration:Lcom/android/internal/telephony/cat/Duration;

    invoke-static {v2}, Lcom/android/stk2/StkApp;->calculateDurationInMilis(Lcom/android/internal/telephony/cat/Duration;)I

    move-result v2

    const v3, 0x5b8d80

    if-ne v2, v3, :cond_0

    .line 1242
    const-string v2, "sustained text. It\'ll be delete with new display text"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1243
    iput-boolean v4, p0, Lcom/android/stk2/StkAppService;->mCmdInProgress:Z

    .line 1245
    :cond_0
    return-void
.end method

.method private launchToneDialog()V
    .locals 3

    .prologue
    .line 1587
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/android/stk2/ToneDialog;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1588
    .local v0, "newIntent":Landroid/content/Intent;
    const/high16 v1, 0x50800000

    sget-object v2, Lcom/android/stk2/StkAppService$InitiatedByUserAction;->unknown:Lcom/android/stk2/StkAppService$InitiatedByUserAction;

    invoke-direct {p0, v2}, Lcom/android/stk2/StkAppService;->getFlagActivityNoUserAction(Lcom/android/stk2/StkAppService$InitiatedByUserAction;)I

    move-result v2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1592
    const-string v1, "TEXT"

    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->geTextMessage()Lcom/android/internal/telephony/cat/TextMessage;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1593
    const-string v1, "TONE"

    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mCurrentCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getToneSettings()Lcom/android/internal/telephony/cat/ToneSettings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1594
    invoke-virtual {p0, v0}, Lcom/android/stk2/StkAppService;->startActivity(Landroid/content/Intent;)V

    .line 1595
    return-void
.end method

.method private processLanguageNotification(Ljava/lang/String;Z)V
    .locals 11
    .param p1, "language"    # Ljava/lang/String;
    .param p2, "initLanguage"    # Z

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1519
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "processLanguageNotification language = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", init = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1520
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 1521
    .local v4, "handled":Ljava/lang/Boolean;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1522
    .local v1, "args":Landroid/os/Bundle;
    const-string v7, "op"

    const/4 v8, 0x2

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1523
    if-ne p2, v10, :cond_0

    .line 1525
    const-string v7, "Language info is null, and init language"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1526
    const-string p1, "en"

    .line 1528
    :cond_0
    if-nez p2, :cond_1

    if-nez p1, :cond_1

    .line 1530
    const-string v7, "Language info is null, and not init language"

    invoke-static {p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1561
    :goto_0
    return-void

    .line 1534
    :cond_1
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 1535
    .local v0, "am":Landroid/app/IActivityManager;
    if-nez v0, :cond_2

    .line 1536
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 1552
    .end local v0    # "am":Landroid/app/IActivityManager;
    :goto_1
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1553
    const-string v7, "response id"

    const/16 v8, 0xe

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1560
    :goto_2
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/android/stk2/StkAppService;

    invoke-direct {v7, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v7, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/stk2/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 1538
    .restart local v0    # "am":Landroid/app/IActivityManager;
    :cond_2
    :try_start_1
    invoke-interface {v0}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 1539
    .local v2, "config":Landroid/content/res/Configuration;
    new-instance v5, Ljava/util/Locale;

    invoke-direct {v5, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    .line 1540
    .local v5, "locale":Ljava/util/Locale;
    if-eqz v2, :cond_3

    if-nez v5, :cond_4

    .line 1541
    :cond_3
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_1

    .line 1543
    :cond_4
    iput-object v5, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 1544
    const/4 v7, 0x1

    iput-boolean v7, v2, Landroid/content/res/Configuration;->userSetLocale:Z

    .line 1545
    invoke-interface {v0, v2}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1548
    .end local v0    # "am":Landroid/app/IActivityManager;
    .end local v2    # "config":Landroid/content/res/Configuration;
    .end local v5    # "locale":Ljava/util/Locale;
    :catch_0
    move-exception v3

    .line 1549
    .local v3, "e":Landroid/os/RemoteException;
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_1

    .line 1555
    .end local v3    # "e":Landroid/os/RemoteException;
    :cond_5
    sget-object v6, Lcom/android/internal/telephony/cat/ResultCode;->TERMINAL_CRNTLY_UNABLE_TO_PROCESS:Lcom/android/internal/telephony/cat/ResultCode;

    .line 1556
    .local v6, "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v7, "response id"

    const/16 v8, 0x18

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1557
    const-string v7, "error code"

    invoke-virtual {v6}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1558
    const-string v7, "additional info"

    invoke-virtual {v1, v7, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_2
.end method

.method private processSetEventList(I[I)V
    .locals 12
    .param p1, "numberOfEvents"    # I
    .param p2, "events"    # [I

    .prologue
    const/4 v11, 0x5

    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1441
    const-string v5, "processSetEventList"

    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1442
    const/4 v0, 0x1

    .line 1443
    .local v0, "allHandledEvent":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_2

    .line 1444
    aget v5, p2, v2

    sparse-switch v5, :sswitch_data_0

    .line 1443
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1446
    :sswitch_0
    const-string v5, "gsm.sim.userEvent"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1447
    iget-object v5, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    aput-boolean v7, v5, v10

    goto :goto_1

    .line 1450
    :sswitch_1
    const-string v5, "gsm.sim.screenEvent"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    iget-object v5, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    aput-boolean v7, v5, v11

    goto :goto_1

    .line 1454
    :sswitch_2
    const-string v5, "gsm.sim.lenguageEvent"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1455
    iget-object v5, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    const/4 v6, 0x7

    aput-boolean v7, v5, v6

    goto :goto_1

    .line 1458
    :sswitch_3
    const-string v5, "gsm.sim.browserEvent"

    const-string v6, "1"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1459
    iget-object v5, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    const/16 v6, 0x8

    aput-boolean v7, v5, v6

    goto :goto_1

    .line 1462
    :sswitch_4
    const-string v5, "processSetEventList data"

    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1463
    iget-object v5, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-eqz v5, :cond_0

    .line 1464
    iget-object v5, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v5, v7}, Lcom/android/internal/telephony/cat/AppInterface;->setEventListDataAvailable(Z)V

    goto :goto_1

    .line 1468
    :sswitch_5
    const-string v5, "processSetEventList channel"

    invoke-static {p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1469
    iget-object v5, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    if-eqz v5, :cond_0

    .line 1470
    iget-object v5, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    invoke-interface {v5, v7}, Lcom/android/internal/telephony/cat/AppInterface;->setEventListChannelStatus(Z)V

    goto :goto_1

    .line 1474
    :sswitch_6
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    const/16 v5, 0x14

    if-ge v3, v5, :cond_1

    .line 1475
    iget-object v5, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    aput-boolean v8, v5, v3

    .line 1474
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1476
    :cond_1
    const-string v5, "gsm.sim.userEvent"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1477
    const-string v5, "gsm.sim.screenEvent"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1478
    const-string v5, "gsm.sim.lenguageEvent"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 1479
    const-string v5, "gsm.sim.browserEvent"

    const-string v6, "0"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1486
    .end local v3    # "j":I
    :cond_2
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1487
    .local v1, "args":Landroid/os/Bundle;
    const-string v5, "op"

    invoke-virtual {v1, v5, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1488
    const/4 v2, 0x0

    :goto_3
    if-ge v2, p1, :cond_3

    .line 1489
    aget v5, p2, v2

    if-eqz v5, :cond_4

    aget v5, p2, v2

    if-eq v5, v7, :cond_4

    aget v5, p2, v2

    if-eq v5, v9, :cond_4

    aget v5, p2, v2

    const/4 v6, 0x3

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    if-eq v5, v10, :cond_4

    aget v5, p2, v2

    if-eq v5, v11, :cond_4

    aget v5, p2, v2

    const/4 v6, 0x7

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0x8

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0xb

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0x9

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0xa

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0xff

    if-eq v5, v6, :cond_4

    aget v5, p2, v2

    const/16 v6, 0x12

    if-eq v5, v6, :cond_4

    .line 1502
    const/4 v0, 0x0

    .line 1507
    :cond_3
    if-eqz v0, :cond_5

    .line 1508
    const-string v5, "response id"

    const/16 v6, 0xe

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1515
    :goto_4
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/android/stk2/StkAppService;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/android/stk2/StkAppService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1516
    return-void

    .line 1488
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1510
    :cond_5
    sget-object v4, Lcom/android/internal/telephony/cat/ResultCode;->BEYOND_TERMINAL_CAPABILITY:Lcom/android/internal/telephony/cat/ResultCode;

    .line 1511
    .local v4, "resCode":Lcom/android/internal/telephony/cat/ResultCode;
    const-string v5, "response id"

    const/16 v6, 0x18

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1512
    const-string v5, "error code"

    invoke-virtual {v4}, Lcom/android/internal/telephony/cat/ResultCode;->value()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1513
    const-string v5, "additional info"

    invoke-virtual {v1, v5, v8}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_4

    .line 1444
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x7 -> :sswitch_2
        0x8 -> :sswitch_3
        0x9 -> :sswitch_4
        0xa -> :sswitch_5
        0xff -> :sswitch_6
    .end sparse-switch
.end method

.method private removeMenu()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1612
    :try_start_0
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v3, v3, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v1, :cond_0

    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v3, v3, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    if-nez v3, :cond_0

    .line 1620
    :goto_0
    return v1

    .line 1616
    :catch_0
    move-exception v0

    .line 1617
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "Unable to get Menu\'s items size"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_0
    move v1, v2

    .line 1620
    goto :goto_0
.end method

.method private unlockMenuActivityBlock()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1638
    iget-boolean v0, p0, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    if-nez v0, :cond_0

    .line 1647
    :goto_0
    return-void

    .line 1642
    :cond_0
    const-string v0, "unlockMenuActivityBlock"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1644
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    .line 1645
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1646
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "has message! : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/stk2/StkAppService;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private waitForLooper()V
    .locals 2

    .prologue
    .line 349
    :goto_0
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mServiceHandler:Lcom/android/stk2/StkAppService$ServiceHandler;

    if-nez v0, :cond_0

    .line 350
    monitor-enter p0

    .line 352
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 355
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 353
    :catch_0
    move-exception v0

    goto :goto_1

    .line 357
    :cond_0
    return-void
.end method


# virtual methods
.method public clearmIsStartedByUser()V
    .locals 1

    .prologue
    .line 1676
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/stk2/StkAppService;->mIsStartedByUser:Z

    .line 1677
    return-void
.end method

.method declared-synchronized disableKeyguard()V
    .locals 2

    .prologue
    .line 1631
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    if-nez v0, :cond_0

    .line 1632
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mKeyguardManager:Landroid/app/KeyguardManager;

    const-string v1, "STK"

    invoke-virtual {v0, v1}, Landroid/app/KeyguardManager;->newKeyguardLock(Ljava/lang/String;)Landroid/app/KeyguardManager$KeyguardLock;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk2/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    .line 1633
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->disableKeyguard()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1635
    :cond_0
    monitor-exit p0

    return-void

    .line 1631
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized enableKeyguard()V
    .locals 1

    .prologue
    .line 1624
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    if-eqz v0, :cond_0

    .line 1625
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;

    invoke-virtual {v0}, Landroid/app/KeyguardManager$KeyguardLock;->reenableKeyguard()V

    .line 1626
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/stk2/StkAppService;->mKeyguardLock:Landroid/app/KeyguardManager$KeyguardLock;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1628
    :cond_0
    monitor-exit p0

    return-void

    .line 1624
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method getMainMenu()Lcom/android/internal/telephony/cat/Menu;
    .locals 2

    .prologue
    .line 333
    const/4 v0, 0x0

    .line 334
    .local v0, "mainMenu":Lcom/android/internal/telephony/cat/Menu;
    iget-object v1, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    if-eqz v1, :cond_0

    .line 335
    iget-object v1, p0, Lcom/android/stk2/StkAppService;->mMainCmd:Lcom/android/internal/telephony/cat/CatCmdMessage;

    invoke-virtual {v1}, Lcom/android/internal/telephony/cat/CatCmdMessage;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v0

    .line 337
    :cond_0
    return-object v0
.end method

.method getMenu()Lcom/android/internal/telephony/cat/Menu;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mCurrentMenu:Lcom/android/internal/telephony/cat/Menu;

    return-object v0
.end method

.method indicateMenuVisibility(Z)V
    .locals 0
    .param p1, "visibility"    # Z

    .prologue
    .line 322
    iput-boolean p1, p0, Lcom/android/stk2/StkAppService;->mMenuIsVisibile:Z

    .line 323
    return-void
.end method

.method lockMenuActivityBlock()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1650
    iget-boolean v0, p0, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    if-ne v0, v2, :cond_0

    .line 1659
    :goto_0
    return-void

    .line 1654
    :cond_0
    const-string v0, "lockMenuActivityBlock"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1656
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1657
    iput-boolean v2, p0, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    .line 1658
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mMessageHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/stk2/StkAppService;->mMessageHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 306
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x14

    .line 216
    const/4 v3, 0x1

    invoke-static {v3}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v3

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 218
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->mCmdsQ:Ljava/util/LinkedList;

    .line 219
    new-instance v2, Ljava/lang/Thread;

    const-string v3, "Stk App Service"

    invoke-direct {v2, v6, p0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 221
    .local v2, "serviceThread":Ljava/lang/Thread;
    const-string v3, "ro.csc.sales_code"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->salesCode:Ljava/lang/String;

    .line 223
    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 224
    invoke-virtual {p0}, Lcom/android/stk2/StkAppService;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    .line 225
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mContext:Landroid/content/Context;

    const-string v4, "notification"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 227
    sput-object p0, Lcom/android/stk2/StkAppService;->sInstance:Lcom/android/stk2/StkAppService;

    .line 229
    const-string v3, "power"

    invoke-virtual {p0, v3}, Lcom/android/stk2/StkAppService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 230
    .local v1, "pm":Landroid/os/PowerManager;
    const v3, 0x3000001a

    const-string v4, "STK"

    invoke-virtual {v1, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v3

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 234
    const-string v3, "keyguard"

    invoke-virtual {p0, v3}, Lcom/android/stk2/StkAppService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/KeyguardManager;

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 236
    new-array v3, v5, [Z

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    .line 237
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 238
    iget-object v3, p0, Lcom/android/stk2/StkAppService;->mSetEventList:[Z

    const/4 v4, 0x0

    aput-boolean v4, v3, v0

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239
    :cond_0
    new-instance v3, Lcom/android/stk2/StkAppService$MessageHandler;

    invoke-direct {v3, p0, v6}, Lcom/android/stk2/StkAppService$MessageHandler;-><init>(Lcom/android/stk2/StkAppService;Lcom/android/stk2/StkAppService$1;)V

    iput-object v3, p0, Lcom/android/stk2/StkAppService;->mMessageHandler:Landroid/os/Handler;

    .line 240
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 300
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->waitForLooper()V

    .line 301
    iget-object v0, p0, Lcom/android/stk2/StkAppService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 302
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 245
    const-string v2, "onStart)"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    invoke-direct {p0}, Lcom/android/stk2/StkAppService;->waitForLooper()V

    .line 248
    const/4 v2, 0x1

    invoke-static {v2}, Lcom/android/internal/telephony/cat/CatService;->getInstance(I)Lcom/android/internal/telephony/cat/AppInterface;

    move-result-object v2

    iput-object v2, p0, Lcom/android/stk2/StkAppService;->mStkService:Lcom/android/internal/telephony/cat/AppInterface;

    .line 252
    if-nez p1, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 258
    .local v0, "args":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 262
    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mServiceHandler:Lcom/android/stk2/StkAppService$ServiceHandler;

    invoke-virtual {v2}, Lcom/android/stk2/StkAppService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 263
    .local v1, "msg":Landroid/os/Message;
    const-string v2, "op"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 264
    iget v2, v1, Landroid/os/Message;->arg1:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 266
    :sswitch_0
    const-string v2, "cmd message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 291
    :goto_1
    :sswitch_1
    const-string v2, "Before SendMessage to ServiceHandler)"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    iget-object v2, p0, Lcom/android/stk2/StkAppService;->mServiceHandler:Lcom/android/stk2/StkAppService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/android/stk2/StkAppService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 295
    const-string v2, "After SendMessage to ServiceHandler)"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 271
    :sswitch_2
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_1

    .line 279
    :sswitch_3
    const-string v2, "event"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_1

    .line 283
    :sswitch_4
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_1

    .line 264
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x5 -> :sswitch_1
        0x7 -> :sswitch_3
        0x9 -> :sswitch_2
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x64 -> :sswitch_4
    .end sparse-switch
.end method

.method public run()V
    .locals 2

    .prologue
    .line 310
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 312
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk2/StkAppService;->mServiceLooper:Landroid/os/Looper;

    .line 313
    new-instance v0, Lcom/android/stk2/StkAppService$ServiceHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/stk2/StkAppService$ServiceHandler;-><init>(Lcom/android/stk2/StkAppService;Lcom/android/stk2/StkAppService$1;)V

    iput-object v0, p0, Lcom/android/stk2/StkAppService;->mServiceHandler:Lcom/android/stk2/StkAppService$ServiceHandler;

    .line 315
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 316
    return-void
.end method
