.class abstract Lcom/android/stk2/StkApp;
.super Landroid/app/Application;
.source "StkApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/stk2/StkApp$1;
    }
.end annotation


# direct methods
.method public static calculateDurationInMilis(Lcom/android/internal/telephony/cat/Duration;)I
    .locals 3
    .param p0, "duration"    # Lcom/android/internal/telephony/cat/Duration;

    .prologue
    .line 77
    const/4 v0, 0x0

    .line 78
    .local v0, "timeout":I
    if-eqz p0, :cond_0

    .line 79
    sget-object v1, Lcom/android/stk2/StkApp$1;->$SwitchMap$com$android$internal$telephony$cat$Duration$TimeUnit:[I

    iget-object v2, p0, Lcom/android/internal/telephony/cat/Duration;->timeUnit:Lcom/android/internal/telephony/cat/Duration$TimeUnit;

    invoke-virtual {v2}, Lcom/android/internal/telephony/cat/Duration$TimeUnit;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 88
    const/16 v0, 0x3e8

    .line 91
    :goto_0
    iget v1, p0, Lcom/android/internal/telephony/cat/Duration;->timeInterval:I

    mul-int/2addr v0, v1

    .line 93
    :cond_0
    return v0

    .line 81
    :pswitch_0
    const v0, 0xea60

    .line 82
    goto :goto_0

    .line 84
    :pswitch_1
    const/16 v0, 0x64

    .line 85
    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getUItimeOutforSTKcmds(Ljava/lang/String;)I
    .locals 6
    .param p0, "stkcmds"    # Ljava/lang/String;

    .prologue
    const/4 v3, -0x1

    .line 57
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_RIL_StkCmdTimeOut"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "checkStkCmdList":Ljava/lang/String;
    const/16 v4, 0x8

    new-array v0, v4, [Ljava/lang/String;

    .line 59
    .local v0, "checkStkCmdArray":[Ljava/lang/String;
    const-string v4, ","

    invoke-virtual {v1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 60
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_1

    .line 69
    :cond_0
    :goto_0
    return v3

    .line 64
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v4, v0

    if-ge v2, v4, :cond_0

    .line 65
    aget-object v4, v0, v2

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 66
    add-int/lit8 v3, v2, 0x1

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    mul-int/lit16 v3, v3, 0x3e8

    goto :goto_0

    .line 64
    :cond_2
    add-int/lit8 v2, v2, 0x2

    goto :goto_1
.end method
