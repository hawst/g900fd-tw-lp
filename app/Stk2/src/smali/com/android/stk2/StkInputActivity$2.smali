.class Lcom/android/stk2/StkInputActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "StkInputActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/stk2/StkInputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/stk2/StkInputActivity;


# direct methods
.method constructor <init>(Lcom/android/stk2/StkInputActivity;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v10, 0xc

    const/4 v9, -0x1

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 428
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.android.samsungtest.EVENTHANDLE_TEXTANDBUTTON"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 429
    const/4 v3, 0x0

    .line 430
    .local v3, "inputString":Ljava/lang/String;
    const/4 v1, -0x1

    .line 431
    .local v1, "button":I
    const-string v4, "SIM2"

    const-string v5, "got intent"

    invoke-static {v4, p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 432
    const-string v4, "Value"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 433
    const-string v4, "Button"

    invoke-virtual {p2, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 434
    if-eqz v3, :cond_0

    .line 435
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # getter for: Lcom/android/stk2/StkInputActivity;->mTextIn:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/android/stk2/StkInputActivity;->access$100(Lcom/android/stk2/StkInputActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 436
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # getter for: Lcom/android/stk2/StkInputActivity;->mTextIn:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/android/stk2/StkInputActivity;->access$100(Lcom/android/stk2/StkInputActivity;)Landroid/widget/EditText;

    move-result-object v4

    iget-object v5, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # getter for: Lcom/android/stk2/StkInputActivity;->mTextIn:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/android/stk2/StkInputActivity;->access$100(Lcom/android/stk2/StkInputActivity;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setSelection(I)V

    .line 438
    :cond_0
    if-nez v1, :cond_2

    .line 439
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    const/16 v5, 0x15

    # invokes: Lcom/android/stk2/StkInputActivity;->sendResponse(ILjava/lang/String;Z)V
    invoke-static {v4, v5, v8, v6}, Lcom/android/stk2/StkInputActivity;->access$200(Lcom/android/stk2/StkInputActivity;ILjava/lang/String;Z)V

    .line 440
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    invoke-virtual {v4}, Lcom/android/stk2/StkInputActivity;->finish()V

    .line 484
    .end local v1    # "button":I
    .end local v3    # "inputString":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 442
    .restart local v1    # "button":I
    .restart local v3    # "inputString":Ljava/lang/String;
    :cond_2
    if-ne v1, v7, :cond_1

    .line 443
    const/4 v2, 0x0

    .line 444
    .local v2, "input":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # invokes: Lcom/android/stk2/StkInputActivity;->verfiyTypedText()Z
    invoke-static {v4}, Lcom/android/stk2/StkInputActivity;->access$300(Lcom/android/stk2/StkInputActivity;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 447
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # getter for: Lcom/android/stk2/StkInputActivity;->mTextIn:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/android/stk2/StkInputActivity;->access$100(Lcom/android/stk2/StkInputActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 449
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # invokes: Lcom/android/stk2/StkInputActivity;->sendResponse(ILjava/lang/String;Z)V
    invoke-static {v4, v10, v2, v6}, Lcom/android/stk2/StkInputActivity;->access$200(Lcom/android/stk2/StkInputActivity;ILjava/lang/String;Z)V

    .line 450
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    invoke-virtual {v4}, Lcom/android/stk2/StkInputActivity;->finish()V

    goto :goto_0

    .line 454
    .end local v1    # "button":I
    .end local v2    # "input":Ljava/lang/String;
    .end local v3    # "inputString":Ljava/lang/String;
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v5, "com.android.samsungtest.EVENTHANDLE_BUTTON"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 455
    const-string v3, ""

    .line 456
    .restart local v3    # "inputString":Ljava/lang/String;
    const/4 v1, -0x1

    .line 457
    .restart local v1    # "button":I
    const-string v4, "SIM2"

    const-string v5, "got intent"

    invoke-static {v4, p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 458
    const-string v4, "Button"

    invoke-virtual {p2, v4, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 459
    if-nez v1, :cond_4

    .line 460
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    const/16 v5, 0x15

    # invokes: Lcom/android/stk2/StkInputActivity;->sendResponse(ILjava/lang/String;Z)V
    invoke-static {v4, v5, v8, v6}, Lcom/android/stk2/StkInputActivity;->access$200(Lcom/android/stk2/StkInputActivity;ILjava/lang/String;Z)V

    .line 461
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    invoke-virtual {v4}, Lcom/android/stk2/StkInputActivity;->finish()V

    goto :goto_0

    .line 463
    :cond_4
    if-ne v1, v7, :cond_5

    .line 464
    const/4 v2, 0x0

    .line 465
    .restart local v2    # "input":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # invokes: Lcom/android/stk2/StkInputActivity;->verfiyTypedText()Z
    invoke-static {v4}, Lcom/android/stk2/StkInputActivity;->access$300(Lcom/android/stk2/StkInputActivity;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 468
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # getter for: Lcom/android/stk2/StkInputActivity;->mTextIn:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/android/stk2/StkInputActivity;->access$100(Lcom/android/stk2/StkInputActivity;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 470
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # invokes: Lcom/android/stk2/StkInputActivity;->sendResponse(ILjava/lang/String;Z)V
    invoke-static {v4, v10, v2, v6}, Lcom/android/stk2/StkInputActivity;->access$200(Lcom/android/stk2/StkInputActivity;ILjava/lang/String;Z)V

    .line 471
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    invoke-virtual {v4}, Lcom/android/stk2/StkInputActivity;->finish()V

    goto :goto_0

    .line 473
    .end local v2    # "input":Ljava/lang/String;
    :cond_5
    const/4 v4, 0x2

    if-ne v1, v4, :cond_1

    .line 474
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    const/16 v5, 0x16

    # invokes: Lcom/android/stk2/StkInputActivity;->sendResponse(ILjava/lang/String;Z)V
    invoke-static {v4, v5, v8, v6}, Lcom/android/stk2/StkInputActivity;->access$200(Lcom/android/stk2/StkInputActivity;ILjava/lang/String;Z)V

    .line 475
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    invoke-virtual {v4}, Lcom/android/stk2/StkInputActivity;->finish()V

    goto/16 :goto_0

    .line 478
    .end local v1    # "button":I
    .end local v3    # "inputString":Ljava/lang/String;
    :cond_6
    const-string v4, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 479
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    # getter for: Lcom/android/stk2/StkInputActivity;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/stk2/StkInputActivity;->access$400(Lcom/android/stk2/StkInputActivity;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "airplane_mode_on"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 480
    .local v0, "airPlaneEnabled":I
    const-string v4, "SIM2"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Received  ACTION_AIRPLANE_MODE_CHANGED = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 481
    if-ne v0, v7, :cond_1

    .line 482
    iget-object v4, p0, Lcom/android/stk2/StkInputActivity$2;->this$0:Lcom/android/stk2/StkInputActivity;

    invoke-virtual {v4}, Lcom/android/stk2/StkInputActivity;->finish()V

    goto/16 :goto_0
.end method
