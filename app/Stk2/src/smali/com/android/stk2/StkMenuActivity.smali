.class public Lcom/android/stk2/StkMenuActivity;
.super Landroid/app/ListActivity;
.source "StkMenuActivity.java"


# static fields
.field private static salesCode:Ljava/lang/String;


# instance fields
.field appService:Lcom/android/stk2/StkAppService;

.field private mAcceptUsersInput:Z

.field private mContext:Landroid/content/Context;

.field private mEntered:Z

.field private mProgressView:Landroid/widget/ProgressBar;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mState:I

.field private mStkMenu:Lcom/android/internal/telephony/cat/Menu;

.field mTimeoutHandler:Landroid/os/Handler;

.field private mTitleIconView:Landroid/widget/ImageView;

.field private mTitleTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 63
    iput-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 65
    iput v0, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    .line 66
    iput-boolean v0, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 68
    iput-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleTextView:Landroid/widget/TextView;

    .line 69
    iput-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleIconView:Landroid/widget/ImageView;

    .line 70
    iput-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mProgressView:Landroid/widget/ProgressBar;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/stk2/StkMenuActivity;->mEntered:Z

    .line 72
    iput-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 73
    invoke-static {}, Lcom/android/stk2/StkAppService;->getInstance()Lcom/android/stk2/StkAppService;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    .line 84
    new-instance v0, Lcom/android/stk2/StkMenuActivity$1;

    invoke-direct {v0, p0}, Lcom/android/stk2/StkMenuActivity$1;-><init>(Lcom/android/stk2/StkMenuActivity;)V

    iput-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mTimeoutHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$002(Lcom/android/stk2/StkMenuActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkMenuActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 61
    iput-boolean p1, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    return p1
.end method

.method static synthetic access$100(Lcom/android/stk2/StkMenuActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/stk2/StkMenuActivity;
    .param p1, "x1"    # I

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/android/stk2/StkMenuActivity;->sendResponse(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/stk2/StkMenuActivity;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/stk2/StkMenuActivity;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private cancelTimeOut()V
    .locals 2

    .prologue
    .line 472
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mTimeoutHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 473
    return-void
.end method

.method private displayMenu()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 494
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    if-nez v1, :cond_0

    .line 495
    const-string v1, "mStkMenu == null in displayMenu()"

    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 530
    :goto_0
    return-void

    .line 500
    :cond_0
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v1, v1, Lcom/android/internal/telephony/cat/Menu;->titleIcon:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 501
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleIconView:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v2, v2, Lcom/android/internal/telephony/cat/Menu;->titleIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 502
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleIconView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 507
    :goto_1
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-boolean v1, v1, Lcom/android/internal/telephony/cat/Menu;->titleIconSelfExplanatory:Z

    if-nez v1, :cond_4

    .line 508
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 509
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/stk2/StkMenuActivity;->salesCode:Ljava/lang/String;

    .line 511
    const-string v1, "CHU"

    sget-object v2, Lcom/android/stk2/StkMenuActivity;->salesCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 512
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleTextView:Landroid/widget/TextView;

    const v2, 0x7f060001

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 525
    :goto_2
    new-instance v0, Lcom/android/stk2/StkMenuAdapter;

    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v1, v1, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    iget-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-boolean v2, v2, Lcom/android/internal/telephony/cat/Menu;->itemsIconSelfExplanatory:Z

    invoke-direct {v0, p0, v1, v2}, Lcom/android/stk2/StkMenuAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Z)V

    .line 527
    .local v0, "adapter":Lcom/android/stk2/StkMenuAdapter;
    invoke-virtual {p0, v0}, Lcom/android/stk2/StkMenuActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 529
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget v1, v1, Lcom/android/internal/telephony/cat/Menu;->defaultItem:I

    invoke-virtual {p0, v1}, Lcom/android/stk2/StkMenuActivity;->setSelection(I)V

    goto :goto_0

    .line 504
    .end local v0    # "adapter":Lcom/android/stk2/StkMenuAdapter;
    :cond_1
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleIconView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 514
    :cond_2
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v1, v1, Lcom/android/internal/telephony/cat/Menu;->title:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 515
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleTextView:Landroid/widget/TextView;

    const/high16 v2, 0x7f060000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2

    .line 517
    :cond_3
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v2, v2, Lcom/android/internal/telephony/cat/Menu;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 522
    :cond_4
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTitleTextView:Landroid/widget/TextView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method private getSelectedItem(I)Lcom/android/internal/telephony/cat/Item;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 547
    const/4 v2, 0x0

    .line 548
    .local v2, "item":Lcom/android/internal/telephony/cat/Item;
    iget-object v3, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    if-eqz v3, :cond_0

    .line 550
    :try_start_0
    iget-object v3, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-object v3, v3, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/android/internal/telephony/cat/Item;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    .line 561
    :cond_0
    :goto_0
    return-object v2

    .line 551
    :catch_0
    move-exception v1

    .line 553
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v3, "SIM2"

    const-string v4, "Invalid menu"

    invoke-static {v3, p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 555
    .end local v1    # "e":Ljava/lang/IndexOutOfBoundsException;
    :catch_1
    move-exception v1

    .line 557
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v3, "SIM2"

    const-string v4, "Invalid menu"

    invoke-static {v3, p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initFromIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 534
    if-eqz p1, :cond_1

    .line 535
    const-string v0, "STATE"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    .line 536
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->invalidateOptionsMenu()V

    .line 540
    :goto_0
    iget v0, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 541
    const-string v0, "SIM2"

    const-string v1, "get end intent"

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 542
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->finish()V

    .line 544
    :cond_0
    return-void

    .line 538
    :cond_1
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->finish()V

    goto :goto_0
.end method

.method private sendResponse(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    const/4 v0, 0x0

    .line 565
    invoke-direct {p0, p1, v0, v0}, Lcom/android/stk2/StkMenuActivity;->sendResponse(IIZ)V

    .line 566
    return-void
.end method

.method private sendResponse(IIZ)V
    .locals 5
    .param p1, "resId"    # I
    .param p2, "itemId"    # I
    .param p3, "help"    # Z

    .prologue
    .line 569
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 570
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "op"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 571
    const-string v1, "response id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 572
    const-string v1, "menu selection"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 573
    const-string v1, "help"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 574
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/android/stk2/StkMenuActivity;->mContext:Landroid/content/Context;

    const-class v4, Lcom/android/stk2/StkAppService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 576
    return-void
.end method

.method private showStkDisabledMessage()V
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    if-nez v0, :cond_0

    .line 451
    const-string v0, "appService is null for showStkDisabledMessage"

    invoke-static {p0, v0}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 469
    :cond_0
    return-void
.end method

.method private startTimeOut()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 476
    iget v1, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    if-ne v1, v4, :cond_2

    const-string v1, "KDI"

    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 478
    :cond_0
    const v0, 0x9c40

    .line 479
    .local v0, "timeOut":I
    iget-boolean v1, p0, Lcom/android/stk2/StkMenuActivity;->mEntered:Z

    if-eqz v1, :cond_1

    .line 480
    const-string v1, "Select item"

    invoke-static {v1}, Lcom/android/stk2/StkApp;->getUItimeOutforSTKcmds(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    .line 481
    const v0, 0xea60

    .line 484
    :cond_1
    const-string v1, "SIM2"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startTimeOut for => "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 487
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->cancelTimeOut()V

    .line 488
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mTimeoutHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mTimeoutHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 490
    .end local v0    # "timeOut":I
    :cond_2
    return-void
.end method


# virtual methods
.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x1

    .line 401
    iget-boolean v5, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    if-nez v5, :cond_0

    .line 429
    :goto_0
    return v4

    .line 404
    :cond_0
    iget-object v5, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-boolean v5, v5, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    if-ne v5, v4, :cond_1

    .line 405
    const-string v5, "SIM2"

    const-string v6, "menu blocked"

    invoke-static {v5, p0, v6}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 408
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 429
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onContextItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    goto :goto_0

    .line 412
    :pswitch_0
    :try_start_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v1

    check-cast v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    .local v1, "info":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->cancelTimeOut()V

    .line 418
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 419
    iget v2, v1, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    .line 420
    .local v2, "position":I
    invoke-direct {p0, v2}, Lcom/android/stk2/StkMenuActivity;->getSelectedItem(I)Lcom/android/internal/telephony/cat/Item;

    move-result-object v3

    .line 421
    .local v3, "stkItem":Lcom/android/internal/telephony/cat/Item;
    if-eqz v3, :cond_2

    .line 425
    const/16 v5, 0xb

    iget v6, v3, Lcom/android/internal/telephony/cat/Item;->id:I

    invoke-direct {p0, v5, v6, v4}, Lcom/android/stk2/StkMenuActivity;->sendResponse(IIZ)V

    goto :goto_0

    .line 413
    .end local v1    # "info":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    .end local v2    # "position":I
    .end local v3    # "stkItem":Lcom/android/internal/telephony/cat/Item;
    :catch_0
    move-exception v0

    .line 414
    .local v0, "e":Ljava/lang/ClassCastException;
    const-string v4, "SIM2"

    const-string v5, "bad menuInfo"

    invoke-static {v4, p0, v5}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "icicle"    # Landroid/os/Bundle;

    .prologue
    .line 98
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 100
    const-string v2, "SIM2"

    const-string v3, "onCreate"

    invoke-static {v2, p0, v3}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    const v2, 0x7f030003

    invoke-virtual {p0, v2}, Lcom/android/stk2/StkMenuActivity;->setContentView(I)V

    .line 106
    const v2, 0x7f080015

    invoke-virtual {p0, v2}, Lcom/android/stk2/StkMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mTitleTextView:Landroid/widget/TextView;

    .line 107
    const v2, 0x7f080014

    invoke-virtual {p0, v2}, Lcom/android/stk2/StkMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mTitleIconView:Landroid/widget/ImageView;

    .line 108
    const v2, 0x7f080017

    invoke-virtual {p0, v2}, Lcom/android/stk2/StkMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mProgressView:Landroid/widget/ProgressBar;

    .line 109
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mContext:Landroid/content/Context;

    .line 111
    const-string v2, "gsm.STK_SETUP_MENU2"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 112
    const-string v2, "gsm.STK_SETUP_MENU2"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/stk2/StkMenuActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 115
    :cond_0
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 116
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_1

    .line 117
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/android/stk2/StkMenuActivity;->salesCode:Ljava/lang/String;

    .line 118
    const-string v2, "CHU"

    sget-object v3, Lcom/android/stk2/StkMenuActivity;->salesCode:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 122
    const v2, 0x7f020003

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setLogo(I)V

    .line 123
    const v2, 0x7f060001

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 124
    const-string v2, "set cu feature for stk interface "

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    :cond_1
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 129
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/android/stk2/StkMenuActivity;->initFromIntent(Landroid/content/Intent;)V

    .line 130
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 131
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/stk2/StkMenuActivity;->mEntered:Z

    .line 134
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 135
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/android/stk2/StkMenuActivity$2;

    invoke-direct {v3, p0}, Lcom/android/stk2/StkMenuActivity$2;-><init>(Lcom/android/stk2/StkMenuActivity;)V

    iput-object v3, p0, Lcom/android/stk2/StkMenuActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 149
    iget-object v2, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    if-nez v2, :cond_2

    .line 150
    const-string v2, "appService is null in onCreate()"

    invoke-static {p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->finish()V

    .line 160
    :cond_2
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    const/4 v3, 0x0

    .line 386
    invoke-super {p0, p1, p2, p3}, Landroid/app/ListActivity;->onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V

    .line 389
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    if-eqz v0, :cond_1

    .line 390
    const-string v0, "SIM2"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateContextMenu helpAvailable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-boolean v2, v2, Lcom/android/internal/telephony/cat/Menu;->helpAvailable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 391
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-boolean v0, v0, Lcom/android/internal/telephony/cat/Menu;->helpAvailable:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 392
    const/4 v0, 0x3

    const v1, 0x7f060003

    invoke-interface {p1, v3, v0, v3, v1}, Landroid/view/ContextMenu;->add(IIII)Landroid/view/MenuItem;

    .line 397
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    const-string v0, "SIM2"

    const-string v1, "onCreateContextMenu, mStkMenu is null"

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 303
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 304
    const v0, 0x7f060002

    invoke-interface {p1, v4, v3, v3, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 305
    const/4 v0, 0x3

    const/4 v1, 0x2

    const v2, 0x7f060003

    invoke-interface {p1, v4, v0, v1, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 306
    return v3
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 287
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 288
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    if-eqz v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    invoke-virtual {v1}, Lcom/android/stk2/StkAppService;->clearmIsStartedByUser()V

    .line 291
    :cond_0
    const-string v1, "onDestroy"

    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    .line 292
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_1

    .line 294
    :try_start_0
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/stk2/StkMenuActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :cond_1
    :goto_0
    return-void

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "Failed unregisterReceiver"

    invoke-static {p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 197
    iget-boolean v1, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    if-nez v1, :cond_0

    .line 218
    :goto_0
    return v0

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-boolean v1, v1, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    if-ne v1, v0, :cond_1

    .line 201
    const-string v1, "SIM2"

    const-string v2, "menu blocked"

    invoke-static {v1, p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 205
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 218
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/app/ListActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 207
    :pswitch_0
    iget v1, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    packed-switch v1, :pswitch_data_1

    goto :goto_1

    .line 209
    :pswitch_1
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->cancelTimeOut()V

    .line 210
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 211
    const/16 v1, 0x15

    invoke-direct {p0, v1}, Lcom/android/stk2/StkMenuActivity;->sendResponse(I)V

    goto :goto_0

    .line 205
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch

    .line 207
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 173
    invoke-super/range {p0 .. p5}, Landroid/app/ListActivity;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 175
    iget-boolean v1, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    if-nez v1, :cond_1

    .line 176
    const-string v1, "SIM2"

    const-string v2, "!mAcceptUsersInput"

    invoke-static {v1, p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 193
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    invoke-direct {p0, p3}, Lcom/android/stk2/StkMenuActivity;->getSelectedItem(I)Lcom/android/internal/telephony/cat/Item;

    move-result-object v0

    .line 181
    .local v0, "item":Lcom/android/internal/telephony/cat/Item;
    if-eqz v0, :cond_0

    .line 184
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-boolean v1, v1, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    if-ne v1, v4, :cond_2

    .line 185
    const-string v1, "SIM2"

    const-string v2, "menu blocked"

    invoke-static {v1, p0, v2}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 188
    :cond_2
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->cancelTimeOut()V

    .line 189
    const/16 v1, 0xb

    iget v2, v0, Lcom/android/internal/telephony/cat/Item;->id:I

    invoke-direct {p0, v1, v2, v3}, Lcom/android/stk2/StkMenuActivity;->sendResponse(IIZ)V

    .line 190
    iput-boolean v3, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 191
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 192
    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 166
    const-string v0, "SIM2"

    const-string v1, "onNewIntent"

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    invoke-direct {p0, p1}, Lcom/android/stk2/StkMenuActivity;->initFromIntent(Landroid/content/Intent;)V

    .line 168
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 169
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 352
    iget-boolean v3, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    if-nez v3, :cond_0

    .line 380
    :goto_0
    return v2

    .line 355
    :cond_0
    iget-object v3, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-boolean v3, v3, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    if-ne v3, v2, :cond_1

    .line 356
    const-string v3, "SIM2"

    const-string v4, "menu blocked"

    invoke-static {v3, p0, v4}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_0

    .line 359
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 380
    :cond_2
    :pswitch_0
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0

    .line 361
    :pswitch_1
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->cancelTimeOut()V

    .line 362
    iput-boolean v4, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 364
    const/16 v3, 0x16

    invoke-direct {p0, v3}, Lcom/android/stk2/StkMenuActivity;->sendResponse(I)V

    goto :goto_0

    .line 367
    :pswitch_2
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->cancelTimeOut()V

    .line 368
    iput-boolean v4, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 369
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->getSelectedItemPosition()I

    move-result v0

    .line 370
    .local v0, "position":I
    invoke-direct {p0, v0}, Lcom/android/stk2/StkMenuActivity;->getSelectedItem(I)Lcom/android/internal/telephony/cat/Item;

    move-result-object v1

    .line 371
    .local v1, "stkItem":Lcom/android/internal/telephony/cat/Item;
    if-eqz v1, :cond_2

    .line 375
    const/16 v3, 0xb

    iget v4, v1, Lcom/android/internal/telephony/cat/Item;->id:I

    invoke-direct {p0, v3, v4, v2}, Lcom/android/stk2/StkMenuActivity;->sendResponse(IIZ)V

    goto :goto_0

    .line 359
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 268
    invoke-super {p0}, Landroid/app/ListActivity;->onPause()V

    .line 269
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    invoke-virtual {v0, v1}, Lcom/android/stk2/StkAppService;->indicateMenuVisibility(Z)V

    .line 270
    iput-boolean v1, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 271
    const-string v0, "ZTR"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->cancelTimeOut()V

    .line 283
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 9
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 311
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    .line 312
    const/4 v0, 0x0

    .line 313
    .local v0, "helpVisible":Z
    const/4 v4, 0x0

    .line 314
    .local v4, "mainVisible":Z
    const/4 v3, 0x0

    .line 315
    .local v3, "mTempStkMenu":Lcom/android/internal/telephony/cat/Menu;
    const/4 v2, 0x0

    .line 316
    .local v2, "mTempMainStkMenu":Lcom/android/internal/telephony/cat/Menu;
    iget-object v6, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    invoke-virtual {v6}, Lcom/android/stk2/StkAppService;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v3

    .line 317
    iget-object v6, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    invoke-virtual {v6}, Lcom/android/stk2/StkAppService;->getMainMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v2

    .line 318
    if-eqz v3, :cond_0

    if-eqz v2, :cond_0

    .line 320
    iget-object v6, v3, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    check-cast v6, Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 321
    .local v1, "items_stk_temp":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/cat/Item;>;"
    iget-object v5, v2, Lcom/android/internal/telephony/cat/Menu;->items:Ljava/util/List;

    .line 322
    .local v5, "main_items_stk_temp":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/cat/Item;>;"
    invoke-interface {v1, v5}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 323
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-nez v6, :cond_4

    .line 324
    iput v7, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    .line 330
    .end local v1    # "items_stk_temp":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/cat/Item;>;"
    .end local v5    # "main_items_stk_temp":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/cat/Item;>;"
    :cond_0
    :goto_0
    iget v6, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    if-ne v6, v8, :cond_1

    .line 331
    const/4 v4, 0x1

    .line 334
    :cond_1
    iget-object v6, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    if-eqz v6, :cond_2

    .line 335
    iget-object v6, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    iget-boolean v0, v6, Lcom/android/internal/telephony/cat/Menu;->helpAvailable:Z

    .line 338
    :cond_2
    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 339
    const/4 v6, 0x3

    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 341
    iget-boolean v6, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-boolean v6, v6, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    if-eqz v6, :cond_5

    .line 342
    :cond_3
    const-string v6, "SIM2"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onPrepareOptionsMenu mAcceptUsersInput:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "appService.mMenuItemBlock:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-boolean v8, v8, Lcom/android/stk2/StkAppService;->mMenuItemBlock:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, p0, v7}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 344
    const/4 v6, 0x0

    .line 347
    :goto_1
    return v6

    .line 326
    .restart local v1    # "items_stk_temp":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/cat/Item;>;"
    .restart local v5    # "main_items_stk_temp":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/cat/Item;>;"
    :cond_4
    iput v8, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    goto :goto_0

    .end local v1    # "items_stk_temp":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/cat/Item;>;"
    .end local v5    # "main_items_stk_temp":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/telephony/cat/Item;>;"
    :cond_5
    move v6, v7

    .line 347
    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 440
    const-string v0, "STATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    .line 441
    const-string v0, "MENU"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/internal/telephony/cat/Menu;

    iput-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 442
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 223
    invoke-super {p0}, Landroid/app/ListActivity;->onResume()V

    .line 225
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    if-nez v0, :cond_0

    .line 226
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->showStkDisabledMessage()V

    .line 227
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->finish()V

    .line 264
    :goto_0
    return-void

    .line 237
    :cond_0
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    invoke-virtual {v0, v2}, Lcom/android/stk2/StkAppService;->indicateMenuVisibility(Z)V

    .line 239
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    invoke-virtual {v0}, Lcom/android/stk2/StkAppService;->getMenu()Lcom/android/internal/telephony/cat/Menu;

    move-result-object v0

    iput-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    .line 240
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    if-nez v0, :cond_1

    .line 241
    const-string v0, "SIM2"

    const-string v1, "onDestroy, mStkMenu is null"

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->finish()V

    goto :goto_0

    .line 246
    :cond_1
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->displayMenu()V

    .line 250
    iget-boolean v0, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    iget-boolean v0, v0, Lcom/android/stk2/StkAppService;->mIsMainMenu:Z

    if-eqz v0, :cond_3

    .line 251
    :cond_2
    const-string v0, "SIM2"

    const-string v1, "onResume : It\'s STK MAIN MENU"

    invoke-static {v0, p0, v1}, Lcom/android/internal/telephony/cat/CatLog;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    iput v2, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    .line 253
    iput-boolean v2, p0, Lcom/android/stk2/StkMenuActivity;->mAcceptUsersInput:Z

    .line 254
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->appService:Lcom/android/stk2/StkAppService;

    iput-boolean v3, v0, Lcom/android/stk2/StkAppService;->mIsMainMenu:Z

    .line 257
    :cond_3
    invoke-virtual {p0}, Lcom/android/stk2/StkMenuActivity;->invalidateOptionsMenu()V

    .line 259
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 260
    iget-object v0, p0, Lcom/android/stk2/StkMenuActivity;->mProgressView:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 261
    iput-boolean v2, p0, Lcom/android/stk2/StkMenuActivity;->mEntered:Z

    .line 263
    invoke-direct {p0}, Lcom/android/stk2/StkMenuActivity;->startTimeOut()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 434
    const-string v0, "STATE"

    iget v1, p0, Lcom/android/stk2/StkMenuActivity;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 435
    const-string v0, "MENU"

    iget-object v1, p0, Lcom/android/stk2/StkMenuActivity;->mStkMenu:Lcom/android/internal/telephony/cat/Menu;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 436
    return-void
.end method
