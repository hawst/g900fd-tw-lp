.class Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;
.super Ljava/lang/Object;
.source "ShootingModeUninstaller.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ShootingModeInstaller"


# instance fields
.field private final mPackageName:Ljava/lang/String;

.field private final mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;Ljava/lang/String;)V
    .locals 0
    .param p1, "serviceContext"    # Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p2, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;->mPackageName:Ljava/lang/String;

    .line 32
    iput-object p1, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    .line 33
    return-void
.end method


# virtual methods
.method public uninstallShootingModes()Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 39
    iget-object v3, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-virtual {v3}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 40
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->URI_SHOOTING_MODES_PACKAGES:Landroid/net/Uri;

    iget-object v4, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;->mPackageName:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 42
    .local v1, "uri":Landroid/net/Uri;
    if-nez v0, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v2

    .line 45
    :cond_1
    if-eqz v1, :cond_0

    .line 48
    invoke-virtual {v0, v1, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method
