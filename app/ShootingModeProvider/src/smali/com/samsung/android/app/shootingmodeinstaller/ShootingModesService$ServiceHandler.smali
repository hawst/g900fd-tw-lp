.class final Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;
.super Landroid/os/Handler;
.source "ShootingModesService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->this$0:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    .line 106
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 107
    return-void
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 111
    monitor-enter p0

    :try_start_0
    const-string v2, "ShootingModesService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handleMessage "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 114
    .local v1, "packageName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 116
    .local v0, "modesUpdated":Z
    iget v2, p1, Landroid/os/Message;->arg1:I

    packed-switch v2, :pswitch_data_0

    .line 151
    :goto_0
    :pswitch_0
    if-eqz v0, :cond_0

    .line 152
    iget-object v2, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->this$0:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.android.action.SHOOTING_MODES_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->sendBroadcast(Landroid/content/Intent;)V

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->this$0:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-virtual {v2}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->stopInstaller()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    monitor-exit p0

    return-void

    .line 118
    :pswitch_1
    :try_start_1
    const-string v2, "com.sec.android.app.camera"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119
    new-instance v2, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;

    iget-object v3, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->this$0:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-direct {v2, v3}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;-><init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;)V

    invoke-virtual {v2}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->installShootingModes()Z

    move-result v0

    .line 120
    const-string v2, "ShootingModesService"

    const-string v3, "Install preloaded shooting modes is done"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 111
    .end local v0    # "modesUpdated":Z
    .end local v1    # "packageName":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 122
    .restart local v0    # "modesUpdated":Z
    .restart local v1    # "packageName":Ljava/lang/String;
    :cond_1
    :try_start_2
    new-instance v2, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeInstaller;

    iget-object v3, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->this$0:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-direct {v2, v3, v1}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeInstaller;-><init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeInstaller;->installAsShootingMode()Z

    move-result v0

    .line 124
    goto :goto_0

    .line 134
    :pswitch_2
    new-instance v2, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;

    iget-object v3, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->this$0:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-direct {v2, v3, v1}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;-><init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;->uninstallShootingModes()Z

    move-result v0

    .line 135
    goto :goto_0

    .line 138
    :pswitch_3
    new-instance v2, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeLocaleChanger;

    iget-object v3, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->this$0:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-direct {v2, v3}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeLocaleChanger;-><init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;)V

    invoke-virtual {v2}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeLocaleChanger;->changeShootingModesLocale()Z

    move-result v0

    .line 139
    goto :goto_0

    .line 142
    :pswitch_4
    new-instance v2, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;

    iget-object v3, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->this$0:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-direct {v2, v3}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;-><init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;)V

    invoke-virtual {v2}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->installShootingModes()Z

    move-result v0

    .line 143
    const-string v2, "ShootingModesService"

    const-string v3, "Install preloaded shooting modes is done"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
