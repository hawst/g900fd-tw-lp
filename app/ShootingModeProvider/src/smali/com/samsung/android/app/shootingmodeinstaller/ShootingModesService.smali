.class public Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;
.super Landroid/app/Service;
.source "ShootingModesService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;,
        Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$PackagesColumns;,
        Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$SMTitlesColumns;,
        Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ShootingModesColumns;
    }
.end annotation


# static fields
.field private static final ACTION_ADDED:I = 0x1

.field private static final ACTION_CHANGED:I = 0x2

.field private static final ACTION_INSTALL_PRELOAD_SHOOTING_MODES:I = 0x6

.field private static final ACTION_LOCALE_CHANGED:I = 0x5

.field private static final ACTION_REMOVED:I = 0x4

.field private static final ACTION_REPLACED:I = 0x3

.field private static final ACTION_SHOOTING_MODES_CHANGED:Ljava/lang/String; = "com.samsung.android.action.SHOOTING_MODES_CHANGED"

.field public static final CAMERA_PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.camera"

.field public static final CAMERA_SHOOTING_MODE:Ljava/lang/String; = "com.sec.android.app.camera.service.CAMERA_SHOOTING_MODE"

.field public static final INSTALL_PRELOAD_SHOOTING_MODE:Ljava/lang/String; = "com.sec.android.shootingmode.action.INSTALL_SHOOTING_MODES"

.field public static final PREFERENCES_FILE:Ljava/lang/String; = "shootingmodeinstaller_pref"

.field public static final PREF_BUILD_FINGERPRINT:Ljava/lang/String; = "build_fingerprint"

.field private static final TAG:Ljava/lang/String; = "ShootingModesService"

.field static final URI_SHOOTING_MODES:Landroid/net/Uri;

.field static final URI_SHOOTING_MODES_INCLUDE_DELETED:Landroid/net/Uri;

.field static final URI_SHOOTING_MODES_PACKAGES:Landroid/net/Uri;

.field static final URI_SHOOTING_MODES_TITLE:Landroid/net/Uri;


# instance fields
.field private mServiceHandler:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;

.field private mStartId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-string v0, "content://com.samsung.android.provider.shootingmodeprovider/packages"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->URI_SHOOTING_MODES_PACKAGES:Landroid/net/Uri;

    .line 62
    const-string v0, "content://com.samsung.android.provider.shootingmodeprovider/shooting_modes"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->URI_SHOOTING_MODES:Landroid/net/Uri;

    .line 63
    const-string v0, "content://com.samsung.android.provider.shootingmodeprovider/shooting_modes/include_deleted"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->URI_SHOOTING_MODES_INCLUDE_DELETED:Landroid/net/Uri;

    .line 64
    const-string v0, "content://com.samsung.android.provider.shootingmodeprovider/sm_titles"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->URI_SHOOTING_MODES_TITLE:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 104
    return-void
.end method

.method public static isPackageApplicableAsShootingMode(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 6
    .param p0, "packageManager"    # Landroid/content/pm/PackageManager;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 228
    const/4 v1, 0x1

    .line 230
    .local v1, "isShootingMode":Z
    if-eqz p0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 231
    :cond_0
    const/4 v4, 0x0

    .line 252
    :goto_0
    return v4

    .line 236
    :cond_1
    const/16 v4, 0x1000

    :try_start_0
    invoke-virtual {p0, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 238
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    if-eqz v4, :cond_4

    .line 239
    iget-object v4, v2, Landroid/content/pm/PackageInfo;->requestedPermissions:[Ljava/lang/String;

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 241
    .local v3, "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v4, "com.sec.android.app.camera.permission.SHOOTING_MODE"

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "android.permission.CAMERA"

    invoke-interface {v3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    const-string v4, "com.sec.android.app.camera"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-nez v4, :cond_3

    .line 243
    const/4 v1, 0x0

    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v3    # "permissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    :goto_1
    move v4, v1

    .line 252
    goto :goto_0

    .line 246
    .restart local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 248
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 249
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v4, "ShootingModesService"

    const-string v5, "Invalid package provided"

    invoke-static {v4, v5, v0}, Landroid/util/secutil/Log;->secW(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public static isPackagePresented(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 264
    const/4 v1, 0x0

    .line 267
    .local v1, "presented":Z
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_0

    .line 268
    const/4 v1, 0x1

    .line 274
    :cond_0
    :goto_0
    return v1

    .line 270
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 161
    const-string v1, "ShootingModesService"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ShootingModesServiceHandler"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 165
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 167
    new-instance v1, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;-><init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->mServiceHandler:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;

    .line 168
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 215
    const-string v0, "ShootingModesService"

    const-string v1, "ShootingModesService.onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 172
    if-nez p1, :cond_0

    .line 173
    const-string v3, "ShootingModesService"

    const-string v4, "onStartCommand(null)"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    :goto_0
    return v2

    .line 177
    :cond_0
    const-string v4, "ShootingModesService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onStartCommand("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iput p3, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->mStartId:I

    .line 181
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 182
    .local v0, "action":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->mServiceHandler:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;

    invoke-virtual {v4}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 184
    .local v1, "msg":Landroid/os/Message;
    const-string v4, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 185
    iput v3, v1, Landroid/os/Message;->arg1:I

    .line 186
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 205
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->mServiceHandler:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    move v2, v3

    .line 206
    goto :goto_0

    .line 187
    :cond_1
    const-string v4, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 188
    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 189
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_1

    .line 190
    :cond_2
    const-string v2, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 191
    const/4 v2, 0x3

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 192
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_1

    .line 193
    :cond_3
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 194
    const/4 v2, 0x4

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 195
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    goto :goto_1

    .line 196
    :cond_4
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 197
    const/4 v2, 0x5

    iput v2, v1, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 198
    :cond_5
    const-string v2, "com.sec.android.shootingmode.action.INSTALL_SHOOTING_MODES"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 199
    const/4 v2, 0x6

    iput v2, v1, Landroid/os/Message;->arg1:I

    goto :goto_1

    .line 201
    :cond_6
    iget v2, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->mStartId:I

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->stopSelfResult(I)Z

    move v2, v3

    .line 202
    goto/16 :goto_0
.end method

.method stopInstaller()V
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->mStartId:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->stopSelfResult(I)Z

    .line 211
    return-void
.end method
