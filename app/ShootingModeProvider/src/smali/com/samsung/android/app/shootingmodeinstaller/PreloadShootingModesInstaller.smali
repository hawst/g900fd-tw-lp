.class final Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;
.super Ljava/lang/Object;
.source "PreloadShootingModesInstaller.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PreloadShootingModesInstaller"


# instance fields
.field private final mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;)V
    .locals 0
    .param p1, "context"    # Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    .line 54
    return-void
.end method

.method private addPreloadedShootingMode(Ljava/util/List;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 12
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "titleName"    # Ljava/lang/String;
    .param p4, "descName"    # Ljava/lang/String;
    .param p5, "iconName"    # Ljava/lang/String;
    .param p6, "packageId"    # J
    .param p8, "activityName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentValues;",
            ">;",
            "Landroid/content/res/Resources;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "values":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    invoke-virtual {p2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget-object v6, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    .line 199
    .local v3, "locale":Ljava/lang/String;
    invoke-direct {p0, p2, p3}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->getStringId(Landroid/content/res/Resources;Ljava/lang/String;)I

    move-result v5

    .line 200
    .local v5, "titleResId":I
    move-object/from16 v0, p4

    invoke-direct {p0, p2, v0}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->getStringId(Landroid/content/res/Resources;Ljava/lang/String;)I

    move-result v1

    .line 201
    .local v1, "descResId":I
    move-object/from16 v0, p5

    invoke-direct {p0, p2, v0}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->getDrawableId(Landroid/content/res/Resources;Ljava/lang/String;)I

    move-result v2

    .line 203
    .local v2, "iconId":I
    if-eqz v5, :cond_0

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 204
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 206
    .local v4, "modeValues":Landroid/content/ContentValues;
    const-string v6, "package_id"

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 207
    const-string v6, "activity_name"

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v8, "%s.%s"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "com.sec.android.app.camera"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object p8, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const-string v6, "major_version"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 209
    const-string v6, "minor_version"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 210
    const-string v6, "icon_res_id"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 211
    const-string v6, "downloaded"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 212
    const-string v6, "title"

    invoke-virtual {p2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v6, "title_res_id"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 214
    const-string v6, "description"

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v6, "description_res_id"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 216
    const-string v6, "locale"

    invoke-virtual {v4, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    .end local v4    # "modeValues":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method private getDrawableId(Landroid/content/res/Resources;Ljava/lang/String;)I
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 230
    const-string v0, "drawable"

    const-string v1, "com.sec.android.app.camera"

    invoke-virtual {p1, p2, v0, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private getStringId(Landroid/content/res/Resources;Ljava/lang/String;)I
    .locals 2
    .param p1, "res"    # Landroid/content/res/Resources;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 242
    const-string v0, "string"

    const-string v1, "com.sec.android.app.camera"

    invoke-virtual {p1, p2, v0, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private initShootingModesValues(Ljava/util/List;Landroid/content/res/Resources;J)V
    .locals 9
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "packageId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentValues;",
            ">;",
            "Landroid/content/res/Resources;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 142
    .local p1, "values":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    const-string v3, "SM_BEAUTY"

    const-string v4, "SM_BEAUTY_description"

    const-string v5, "mode_beauty_face"

    const-string v8, "Beauty Face"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->addPreloadedShootingMode(Ljava/util/List;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 150
    const-string v3, "SM_MAGIC"

    const-string v4, "SM_MAGIC_description"

    const-string v5, "mode_magic_shot"

    const-string v8, "Shot and more"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->addPreloadedShootingMode(Ljava/util/List;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 159
    const-string v3, "SM_PANORAMA"

    const-string v4, "SM_PANORAMA_description"

    const-string v5, "mode_panorama"

    const-string v8, "Panorama"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->addPreloadedShootingMode(Ljava/util/List;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 163
    const-string v3, "SM_3DTOUR_SHORT"

    const-string v4, "SM_3DTOUR_description"

    const-string v5, "mode_3d_tour"

    const-string v8, "3D tour shot"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->addPreloadedShootingMode(Ljava/util/List;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 174
    const-string v3, "SM_DUALSHOT"

    const-string v4, "SM_DUALSHOT_description"

    const-string v5, "mode_dual"

    const-string v8, "Dual Camera"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v6, p3

    invoke-direct/range {v0 .. v8}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->addPreloadedShootingMode(Ljava/util/List;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 192
    return-void
.end method

.method private initializePreloadShootingModesDB(Landroid/content/res/Resources;)V
    .locals 8
    .param p1, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 109
    iget-object v5, p0, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-virtual {v5}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 110
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v2, "modesValuesList":Ljava/util/List;, "Ljava/util/List<Landroid/content/ContentValues;>;"
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 113
    .local v4, "packageValues":Landroid/content/ContentValues;
    const-string v5, "name"

    const-string v6, "com.sec.android.app.camera"

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    sget-object v5, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->URI_SHOOTING_MODES_PACKAGES:Landroid/net/Uri;

    invoke-virtual {v0, v5, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 115
    .local v3, "packageUri":Landroid/net/Uri;
    invoke-virtual {v4}, Landroid/content/ContentValues;->clear()V

    .line 117
    invoke-static {v3}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    invoke-direct {p0, v2, p1, v6, v7}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->initShootingModesValues(Ljava/util/List;Landroid/content/res/Resources;J)V

    .line 119
    sget-object v6, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->URI_SHOOTING_MODES:Landroid/net/Uri;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Landroid/content/ContentValues;

    invoke-interface {v2, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Landroid/content/ContentValues;

    invoke-virtual {v0, v6, v5}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v1

    .line 122
    .local v1, "insertedModes":I
    const-string v5, "PreloadShootingModesInstaller"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Inserted "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " preloaded shooting modes"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    return-void
.end method


# virtual methods
.method public installShootingModes()Z
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v2, 0x0

    .line 62
    const/4 v12, 0x0

    .line 63
    .local v12, "shootingModesCursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 64
    .local v9, "modesUpdated":Z
    const/4 v10, 0x0

    .line 66
    .local v10, "preloadModesUpdated":Z
    iget-object v0, p0, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    const-string v1, "shootingmodeinstaller_pref"

    invoke-virtual {v0, v1, v13}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "build_fingerprint"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 70
    .local v6, "buildFingerprint":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-virtual {v0}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->URI_SHOOTING_MODES_INCLUDE_DELETED:Landroid/net/Uri;

    const/4 v2, 0x0

    const-string v3, "name=\'com.sec.android.app.camera\'"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 73
    if-eqz v12, :cond_0

    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    if-eqz v6, :cond_0

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    const-string v1, "com.sec.android.app.camera"

    invoke-static {v0, v1}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->isPackagePresented(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    invoke-virtual {v0}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.android.app.camera"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v11

    .line 80
    .local v11, "res":Landroid/content/res/Resources;
    new-instance v0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;

    iget-object v1, p0, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    const-string v2, "com.sec.android.app.camera"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;-><init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeUninstaller;->uninstallShootingModes()Z

    .line 81
    invoke-direct {p0, v11}, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->initializePreloadShootingModesDB(Landroid/content/res/Resources;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    const-string v1, "shootingmodeinstaller_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "build_fingerprint"

    sget-object v2, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 84
    const/4 v10, 0x1

    .line 88
    .end local v11    # "res":Landroid/content/res/Resources;
    :cond_1
    new-instance v0, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeInstaller;

    iget-object v1, p0, Lcom/samsung/android/app/shootingmodeinstaller/PreloadShootingModesInstaller;->mServiceContext:Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeInstaller;-><init>(Lcom/samsung/android/app/shootingmodeinstaller/ShootingModesService;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/app/shootingmodeinstaller/ShootingModeInstaller;->installAsShootingMode()Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 90
    .local v7, "discoveredModes":Z
    if-nez v10, :cond_2

    if-eqz v7, :cond_5

    :cond_2
    const/4 v9, 0x1

    .line 95
    .end local v7    # "discoveredModes":Z
    :cond_3
    :goto_0
    if-eqz v12, :cond_4

    .line 96
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 100
    :cond_4
    :goto_1
    return v9

    .restart local v7    # "discoveredModes":Z
    :cond_5
    move v9, v13

    .line 90
    goto :goto_0

    .line 92
    .end local v7    # "discoveredModes":Z
    :catch_0
    move-exception v8

    .line 93
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    const-string v0, "PreloadShootingModesInstaller"

    const-string v1, "Failed to obtain camera resources"

    invoke-static {v0, v1, v8}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    if-eqz v12, :cond_4

    .line 96
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 95
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v0

    if-eqz v12, :cond_6

    .line 96
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method
