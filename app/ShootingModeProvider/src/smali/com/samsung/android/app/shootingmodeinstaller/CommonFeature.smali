.class public Lcom/samsung/android/app/shootingmodeinstaller/CommonFeature;
.super Ljava/lang/Object;
.source "CommonFeature.java"


# static fields
.field public static final BACK_CAMERA_SHOOTINGMODE_3DPANORAMA:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_3DTOUR:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_ACTION_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_ADD_ME:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_AQUA:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_AUTO:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_BEAUTY_SHOT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_BEST_FACE_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_BEST_PHOTO_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_BURST_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_CARTOON:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_CINEPIC:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_CONTINUOUS_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_DRAMA:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_DUAL:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_ERASER_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_FASTVIDEO:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_FOCUS_SELECT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_FRAME_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_GOLF:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_MAGIC:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_MOSAIC_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_NIGHT_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_NIGHT_SHOT_SCENE:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_PANORAMA_SHOT:Z = true

.field public static final BACK_CAMERA_SHOOTINGMODE_RICH_TONE:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_SLOWVIDEO:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_SMART_GIFMAKER:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_SMART_SELFSHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_SMILE_SHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_SNS:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_SOUNDSHOT:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_SPORTS_SCENE:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_STOP_MOTION:Z = false

.field public static final BACK_CAMERA_SHOOTINGMODE_VINTAGE_SHOT:Z = false

.field public static final DISABLE_SHOOTINGMODE_BEAUTY_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_3DPANORAMA:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_ACTION_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_ADD_ME:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_AUTO:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_AUTO_SELFIE:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_BEAUTY_SHOT:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_BEST_FACE_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_BEST_PHOTO_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_BURST_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_CARTOON:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_CINEPIC:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_CONTINUOUS_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_DRAMA:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_DUAL:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_ERASER_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_FRAME_SHOT:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_GOLF:Z = false

.field public static final FRONT_CAMERA_SHOOTINGMODE_MAGIC:Z = true

.field public static final FRONT_CAMERA_SHOOTINGMODE_MOSAIC_SHOT:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_NIGHT_SHOT:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_NIGHT_SHOT_SCENE:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_OUT_FOCUS:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_PANORAMA_SHOT:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_RICH_TONE:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_SMART_GIFMAKER:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_SMILE_SHOT:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_SNS:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_SOUNDSHOT:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_STOP_MOTION:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_ULTRA_WIDE_SHOT:Z

.field public static final FRONT_CAMERA_SHOOTINGMODE_VINTAGE_SHOT:Z

.field public static final NEED_TO_DISABLE_SHOOTINGMODES:Z

.field public static final SELECTIVE_FOCUS_IN_SHOOTINGMODE:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
