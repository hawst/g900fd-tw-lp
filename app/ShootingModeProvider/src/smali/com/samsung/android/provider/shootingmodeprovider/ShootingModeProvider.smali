.class public Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;
.super Landroid/content/ContentProvider;
.source "ShootingModeProvider.java"


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.provider.shootingmodeprovider"

.field private static final AUTHORITY_URI:Landroid/net/Uri;

.field private static final DBNAME:Ljava/lang/String; = "shootingmodemanager.db"

.field private static final DBVERSION:I = 0xf

.field private static final PACKAGES:I = 0x7d0

.field private static final PACKAGES_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/packages"

.field private static final PACKAGES_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/packages"

.field private static final PACKAGES_ID:I = 0x7d1

.field private static final PACKAGES_ID_SHOOTING_MODES:I = 0x7d4

.field private static final PACKAGES_NAME:I = 0x7d2

.field private static final PACKAGES_SHOOTING_MODE:I = 0x7d3

.field private static final SHOOTING_MODES:I = 0x1770

.field private static final SHOOTING_MODES_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/shooting_modes"

.field private static final SHOOTING_MODES_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/shooting_modes"

.field private static final SHOOTING_MODES_ID:I = 0x1771

.field private static final SHOOTING_MODES_INCDEL:I = 0x1774

.field private static final SHOOTING_MODES_ORDER:I = 0x1772

.field private static final SHOOTING_MODES_ORDER_BY_ID_URI:Landroid/net/Uri;

.field private static final SHOOTING_MODES_ORDER_ID:I = 0x1773

.field private static final SHOOTING_MODES_URI:Landroid/net/Uri;

.field private static final SM_TITLES:I = 0x1b58

.field private static final SM_TITLES_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/sm_titles"

.field private static final SM_TITLES_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/sm_titles"

.field private static final SM_TITLES_ID:I = 0x1b59

.field private static final STATEMENT_SHOOTING_MODES_DELETE_BY_PACKAGE_ID:Ljava/lang/String; = "DELETE FROM shooting_modes WHERE package_id = ?1"

.field private static final STATEMENT_SHOOTING_MODES_INSERT:Ljava/lang/String; = "INSERT OR REPLACE INTO shooting_modes(package_id,activity_name,title_id,major_version,minor_version,deleted,icon_res_id,library_path,camera_type,downloaded,sm_order) VALUES(?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11);"

.field private static final STATEMENT_SHOOTING_MODES_UPDATE_ALL:Ljava/lang/String; = "UPDATE shooting_modes SET title_id = ?1,major_version = ?2,minor_version = ?3,icon_res_id = ?4,library_path = ?5,camera_type = ?6,downloaded = ?7 WHERE (package_id = ?8 AND activity_name = ?9)"

.field private static final STATEMENT_SHOOTING_MODES_UPDATE_BY_ID:Ljava/lang/String; = "UPDATE shooting_modes SET deleted = ?1, sm_order = ?2  WHERE _ID = ?3;"

.field private static final STATEMENT_SHOOTING_MODES_UPDATE_BY_ORDER:Ljava/lang/String; = "UPDATE shooting_modes SET deleted = ?1, sm_order = ?2  WHERE sm_order = ?3;"

.field private static final STATEMENT_SHOOTING_MODES_UPDATE_ORDER_BY_ID:Ljava/lang/String; = "UPDATE shooting_modes SET sm_order = ?1  WHERE _ID = ?2;"

.field private static final STATEMENT_SMTITLES_INSERT:Ljava/lang/String; = "INSERT INTO shooting_modes_titles(title,title_res_id,description,description_res_id,locale) VALUES(?1,?2,?3,?4,?5);"

.field private static final STATEMENT_SM_TITLES_DELETE_BY_PACKAGE_ID:Ljava/lang/String; = "DELETE FROM shooting_modes_titles WHERE _ID IN (SELECT DISTINCT _ID FROM view_shooting_modes_titles WHERE package_id = ?1)"

.field private static final STATEMENT_SM_TITLES_UPDATE:Ljava/lang/String; = "UPDATE shooting_modes_titles SET title = ?1 , description = ?2 , locale = ?3  WHERE _ID = ?4;"

.field private static final TAG:Ljava/lang/String; = "ShootingModeProvider"

.field private static final TITLES_CONTENT_ITEM_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/titles"

.field private static final TITLES_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/titles"

.field private static final sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

.field private final mShootingModesMaxOrder:Ljava/util/concurrent/atomic/AtomicInteger;

.field private mStatementSMTitlesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

.field private mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

.field private mStatementSMTitlesUpdate:Landroid/database/sqlite/SQLiteStatement;

.field private mStatementShootingModesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

.field private mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

.field private mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

.field private mStatementShootingModesUpdateById:Landroid/database/sqlite/SQLiteStatement;

.field private mStatementShootingModesUpdateByOrder:Landroid/database/sqlite/SQLiteStatement;

.field private mStatementShootingModesUpdateOrderById:Landroid/database/sqlite/SQLiteStatement;

.field private final mStatementsLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 139
    const-string v1, "content://com.samsung.android.provider.shootingmodeprovider"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->AUTHORITY_URI:Landroid/net/Uri;

    .line 140
    sget-object v1, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->AUTHORITY_URI:Landroid/net/Uri;

    const-string v2, "shooting_modes"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->SHOOTING_MODES_URI:Landroid/net/Uri;

    .line 142
    sget-object v1, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->SHOOTING_MODES_URI:Landroid/net/Uri;

    const-string v2, "order/id"

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->SHOOTING_MODES_ORDER_BY_ID_URI:Landroid/net/Uri;

    .line 144
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v1, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 172
    sget-object v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 174
    .local v0, "matcher":Landroid/content/UriMatcher;
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "packages"

    const/16 v3, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 175
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "packages/*"

    const/16 v3, 0x7d2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 176
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "packages/*/shootingmode"

    const/16 v3, 0x7d3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 177
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "packages/#"

    const/16 v3, 0x7d1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 178
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "packages/#/shootingmode"

    const/16 v3, 0x7d4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 181
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "shooting_modes"

    const/16 v3, 0x1770

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 182
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "shooting_modes/#"

    const/16 v3, 0x1771

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 183
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "shooting_modes/order/#"

    const/16 v3, 0x1772

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 184
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "shooting_modes/order/id/#"

    const/16 v3, 0x1773

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 185
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "shooting_modes/include_deleted"

    const/16 v3, 0x1774

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 187
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "sm_titles"

    const/16 v3, 0x1b58

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 188
    const-string v1, "com.samsung.android.provider.shootingmodeprovider"

    const-string v2, "sm_titles/#"

    const/16 v3, 0x1b59

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 189
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 52
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementsLock:Ljava/lang/Object;

    .line 56
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mShootingModesMaxOrder:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 58
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

    .line 68
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesUpdate:Landroid/database/sqlite/SQLiteStatement;

    .line 76
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    .line 92
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateById:Landroid/database/sqlite/SQLiteStatement;

    .line 99
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateByOrder:Landroid/database/sqlite/SQLiteStatement;

    .line 106
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateOrderById:Landroid/database/sqlite/SQLiteStatement;

    .line 112
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    .line 125
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    .line 130
    iput-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    return-void
.end method

.method private deletePackageContent(J)I
    .locals 21
    .param p1, "packageId"    # J

    .prologue
    .line 687
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v3}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 688
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/4 v3, 0x0

    invoke-static/range {p1 .. p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v17, v3

    .line 693
    .local v17, "selectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementsLock:Ljava/lang/Object;

    monitor-enter v4

    .line 694
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    const/4 v5, 0x1

    move-wide/from16 v0, p1

    invoke-virtual {v3, v5, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 695
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    .line 696
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 697
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    const/4 v5, 0x1

    move-wide/from16 v0, p1

    invoke-virtual {v3, v5, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 698
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v14

    .line 699
    .local v14, "removedShootingModes":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 700
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 702
    if-lez v14, :cond_3

    .line 704
    const-string v3, "shooting_modes"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_ID"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "sm_order"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "sm_order ASC"

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 707
    .local v18, "shootingModesCursor":Landroid/database/Cursor;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 709
    .local v13, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz v18, :cond_2

    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 710
    const/16 v19, 0x0

    .line 712
    .local v19, "startOrder":I
    :goto_0
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 713
    sget-object v3, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->SHOOTING_MODES_ORDER_BY_ID_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "sm_order"

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 716
    add-int/lit8 v19, v19, 0x1

    goto :goto_0

    .line 700
    .end local v13    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v14    # "removedShootingModes":I
    .end local v18    # "shootingModesCursor":Landroid/database/Cursor;
    .end local v19    # "startOrder":I
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 720
    .restart local v13    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .restart local v14    # "removedShootingModes":I
    .restart local v18    # "shootingModesCursor":Landroid/database/Cursor;
    .restart local v19    # "startOrder":I
    :cond_0
    :try_start_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v16

    .line 722
    .local v16, "results":[Landroid/content/ContentProviderResult;
    const/16 v19, 0x0

    .line 725
    move-object/from16 v10, v16

    .local v10, "arr$":[Landroid/content/ContentProviderResult;
    array-length v12, v10

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_1
    if-ge v11, v12, :cond_1

    aget-object v15, v10, v11

    .line 726
    .local v15, "result":Landroid/content/ContentProviderResult;
    iget-object v3, v15, Landroid/content/ContentProviderResult;->count:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catch Landroid/content/OperationApplicationException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v3

    add-int v19, v19, v3

    .line 725
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 728
    .end local v10    # "arr$":[Landroid/content/ContentProviderResult;
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    .end local v15    # "result":Landroid/content/ContentProviderResult;
    .end local v16    # "results":[Landroid/content/ContentProviderResult;
    :catch_0
    move-exception v3

    .line 732
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mShootingModesMaxOrder:Ljava/util/concurrent/atomic/AtomicInteger;

    move/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 734
    .end local v19    # "startOrder":I
    :cond_2
    if-eqz v18, :cond_3

    .line 735
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 739
    .end local v13    # "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    .end local v18    # "shootingModesCursor":Landroid/database/Cursor;
    :cond_3
    const-string v3, "packages"

    const-string v4, "_ID = ?"

    move-object/from16 v0, v17

    invoke-virtual {v2, v3, v4, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    return v3
.end method

.method private getLatestShootingModesOrder()I
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 243
    iget-object v0, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "shooting_modes"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "(SELECT MAX(sm_order) FROM shooting_modes)"

    aput-object v4, v2, v10

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 247
    .local v8, "c":Landroid/database/Cursor;
    const/4 v9, -0x1

    .line 249
    .local v9, "order":I
    if-eqz v8, :cond_1

    .line 250
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 254
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 257
    :cond_1
    return v9
.end method

.method private initializeSQLiteStatements()V
    .locals 3

    .prologue
    .line 265
    iget-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementsLock:Ljava/lang/Object;

    monitor-enter v2

    .line 266
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 268
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "INSERT INTO shooting_modes_titles(title,title_res_id,description,description_res_id,locale) VALUES(?1,?2,?3,?4,?5);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

    .line 269
    const-string v1, "UPDATE shooting_modes_titles SET title = ?1 , description = ?2 , locale = ?3  WHERE _ID = ?4;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesUpdate:Landroid/database/sqlite/SQLiteStatement;

    .line 270
    const-string v1, "INSERT OR REPLACE INTO shooting_modes(package_id,activity_name,title_id,major_version,minor_version,deleted,icon_res_id,library_path,camera_type,downloaded,sm_order) VALUES(?1,?2,?3,?4,?5,?6,?7,?8,?9,?10,?11);"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    .line 271
    const-string v1, "UPDATE shooting_modes SET deleted = ?1, sm_order = ?2  WHERE _ID = ?3;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateById:Landroid/database/sqlite/SQLiteStatement;

    .line 272
    const-string v1, "UPDATE shooting_modes SET deleted = ?1, sm_order = ?2  WHERE sm_order = ?3;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateByOrder:Landroid/database/sqlite/SQLiteStatement;

    .line 273
    const-string v1, "DELETE FROM shooting_modes WHERE package_id = ?1"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    .line 274
    const-string v1, "DELETE FROM shooting_modes_titles WHERE _ID IN (SELECT DISTINCT _ID FROM view_shooting_modes_titles WHERE package_id = ?1)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesDeleteByPackageId:Landroid/database/sqlite/SQLiteStatement;

    .line 275
    const-string v1, "UPDATE shooting_modes SET title_id = ?1,major_version = ?2,minor_version = ?3,icon_res_id = ?4,library_path = ?5,camera_type = ?6,downloaded = ?7 WHERE (package_id = ?8 AND activity_name = ?9)"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    .line 276
    const-string v1, "UPDATE shooting_modes SET sm_order = ?1  WHERE _ID = ?2;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateOrderById:Landroid/database/sqlite/SQLiteStatement;

    .line 277
    monitor-exit v2

    .line 278
    return-void

    .line 277
    .end local v0    # "db":Landroid/database/sqlite/SQLiteDatabase;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private insertPackages(Landroid/content/ContentValues;)J
    .locals 12
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 316
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 318
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v10, -0x1

    .line 320
    .local v10, "packageId":J
    const-string v1, "name"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 321
    .local v9, "packageName":Ljava/lang/String;
    const-string v1, "packages"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_ID"

    aput-object v3, v2, v6

    const-string v3, "name = ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v9, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 326
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 327
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v10

    .line 330
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 333
    const-wide/16 v2, -0x1

    cmp-long v1, v10, v2

    if-nez v1, :cond_1

    .line 334
    const-string v1, "packages"

    invoke-virtual {v0, v1, v5, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v10

    .line 337
    :cond_1
    return-wide v10

    .line 330
    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method private insertSMTitles(Landroid/content/ContentValues;)J
    .locals 8
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 464
    iget-object v3, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementsLock:Ljava/lang/Object;

    monitor-enter v3

    .line 465
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v4, 0x1

    const-string v5, "title"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 466
    iget-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v4, 0x2

    const-string v5, "title_res_id"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 467
    iget-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v4, 0x3

    const-string v5, "description"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 468
    iget-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v4, 0x4

    const-string v5, "description_res_id"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 469
    iget-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v4, 0x5

    const-string v5, "locale"

    invoke-virtual {p1, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 471
    iget-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v0

    .line 472
    .local v0, "result":J
    iget-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesInsert:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 473
    monitor-exit v3

    .line 475
    return-wide v0

    .line 473
    .end local v0    # "result":J
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1, "selectionArgs"    # [Ljava/lang/String;
    .param p2, "arg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 342
    if-nez p1, :cond_0

    .line 343
    new-array v1, v4, [Ljava/lang/String;

    aput-object p2, v1, v3

    .line 350
    :goto_0
    return-object v1

    .line 345
    :cond_0
    array-length v2, p1

    add-int/lit8 v0, v2, 0x1

    .line 346
    .local v0, "newLength":I
    new-array v1, v0, [Ljava/lang/String;

    .line 347
    .local v1, "newSelectionArgs":[Ljava/lang/String;
    aput-object p2, v1, v3

    .line 348
    array-length v2, p1

    invoke-static {p1, v3, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method private insertShootingModes(Landroid/content/ContentValues;)J
    .locals 12
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v10, 0x0

    .line 363
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 366
    .local v0, "localValues":Landroid/content/ContentValues;
    invoke-virtual {v0, p1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 369
    const-string v1, "package_id"

    invoke-virtual {p1, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 370
    const-wide/16 v2, -0x1

    .line 450
    :goto_0
    return-wide v2

    .line 374
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->insertSMTitles(Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 376
    .local v4, "titleId":J
    iget-object v6, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementsLock:Ljava/lang/Object;

    monitor-enter v6

    .line 378
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x1

    invoke-virtual {v1, v7, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 379
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x2

    const-string v8, "major_version"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 380
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x3

    const-string v8, "minor_version"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 381
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x4

    const-string v8, "icon_res_id"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 382
    const-string v1, "library_path"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 383
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x5

    const-string v8, "library_path"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 387
    :goto_1
    const-string v1, "camera_type"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 388
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x6

    const-string v8, "camera_type"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 392
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x7

    const-string v8, "downloaded"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 393
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/16 v7, 0x8

    const-string v8, "package_id"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 394
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/16 v7, 0x9

    const-string v8, "activity_name"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 396
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v1

    int-to-long v2, v1

    .line 397
    .local v2, "result":J
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 399
    cmp-long v1, v2, v10

    if-gtz v1, :cond_2

    .line 400
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x1

    const-string v8, "package_id"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 401
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x2

    const-string v8, "activity_name"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 402
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x3

    invoke-virtual {v1, v7, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 403
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x4

    const-string v8, "major_version"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 404
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x5

    const-string v8, "minor_version"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 406
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x6

    const-wide/16 v8, 0x0

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 423
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x7

    const-string v8, "icon_res_id"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 425
    const-string v1, "library_path"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 426
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/16 v7, 0x8

    const-string v8, "library_path"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 430
    :goto_3
    const-string v1, "camera_type"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 431
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/16 v7, 0x9

    const-string v8, "camera_type"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v7, v8}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 436
    :goto_4
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/16 v7, 0xa

    const-string v8, "downloaded"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 437
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/16 v7, 0xb

    iget-object v8, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mShootingModesMaxOrder:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v8}, Ljava/util/concurrent/atomic/AtomicInteger;->intValue()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v1, v7, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 439
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    .line 441
    cmp-long v1, v2, v10

    if-lez v1, :cond_1

    .line 443
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mShootingModesMaxOrder:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 446
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 448
    :cond_2
    monitor-exit v6

    goto/16 :goto_0

    .end local v2    # "result":J
    :catchall_0
    move-exception v1

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 385
    :cond_3
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x5

    invoke-virtual {v1, v7}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_1

    .line 390
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateAll:Landroid/database/sqlite/SQLiteStatement;

    const/4 v7, 0x6

    invoke-virtual {v1, v7}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto/16 :goto_2

    .line 428
    .restart local v2    # "result":J
    :cond_5
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/16 v7, 0x8

    invoke-virtual {v1, v7}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V

    goto :goto_3

    .line 433
    :cond_6
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesInsert:Landroid/database/sqlite/SQLiteStatement;

    const/16 v7, 0x9

    invoke-virtual {v1, v7}, Landroid/database/sqlite/SQLiteStatement;->bindNull(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4
.end method

.method private queryPackageId(Ljava/lang/String;)J
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 744
    iget-object v1, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v1}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 745
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v1, "packages"

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_ID"

    aput-object v3, v2, v6

    const-string v3, "name = ?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 752
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 753
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 758
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :goto_0
    return-wide v2

    .line 755
    :cond_0
    const-wide/16 v2, -0x1

    .line 758
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v1
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "where"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 650
    sget-object v5, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 651
    .local v1, "match":I
    const/4 v0, 0x0

    .line 653
    .local v0, "count":I
    packed-switch v1, :pswitch_data_0

    .line 666
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unknown URI "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 655
    :pswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    .line 656
    .local v4, "packageName":Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->queryPackageId(Ljava/lang/String;)J

    move-result-wide v2

    .line 658
    .local v2, "packageId":J
    const-wide/16 v6, 0x0

    cmp-long v5, v2, v6

    if-lez v5, :cond_0

    .line 659
    invoke-direct {p0, v2, v3}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->deletePackageContent(J)I

    move-result v0

    .line 673
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 676
    return v0

    .line 653
    :pswitch_data_0
    .packed-switch 0x7d2
        :pswitch_0
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 193
    sget-object v1, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 194
    .local v0, "match":I
    sparse-switch v0, :sswitch_data_0

    .line 218
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 199
    :sswitch_0
    const-string v1, "vnd.android.cursor.dir/packages"

    goto :goto_0

    .line 202
    :sswitch_1
    const-string v1, "vnd.android.cursor.item/packages"

    goto :goto_0

    .line 206
    :sswitch_2
    const-string v1, "vnd.android.cursor.dir/shooting_modes"

    goto :goto_0

    .line 210
    :sswitch_3
    const-string v1, "vnd.android.cursor.item/shooting_modes"

    goto :goto_0

    .line 213
    :sswitch_4
    const-string v1, "vnd.android.cursor.dir/sm_titles"

    goto :goto_0

    .line 216
    :sswitch_5
    const-string v1, "vnd.android.cursor.item/sm_titles"

    goto :goto_0

    .line 194
    nop

    :sswitch_data_0
    .sparse-switch
        0x7d0 -> :sswitch_0
        0x7d1 -> :sswitch_1
        0x7d2 -> :sswitch_0
        0x7d3 -> :sswitch_0
        0x7d4 -> :sswitch_0
        0x1770 -> :sswitch_2
        0x1771 -> :sswitch_3
        0x1772 -> :sswitch_3
        0x1774 -> :sswitch_2
        0x1b58 -> :sswitch_4
        0x1b59 -> :sswitch_5
    .end sparse-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v4, 0x0

    .line 282
    sget-object v5, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v5, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 285
    .local v3, "match":I
    sparse-switch v3, :sswitch_data_0

    move-object v2, v4

    .line 308
    :goto_0
    return-object v2

    .line 288
    :sswitch_0
    invoke-direct {p0, p2}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->insertPackages(Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 304
    .local v0, "id":J
    :goto_1
    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-lez v5, :cond_0

    .line 305
    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 306
    .local v2, "insertedUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-virtual {v5, v2, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 292
    .end local v0    # "id":J
    .end local v2    # "insertedUri":Landroid/net/Uri;
    :sswitch_1
    invoke-direct {p0, p2}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->insertShootingModes(Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 293
    .restart local v0    # "id":J
    goto :goto_1

    .line 296
    .end local v0    # "id":J
    :sswitch_2
    invoke-direct {p0, p2}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->insertSMTitles(Landroid/content/ContentValues;)J

    move-result-wide v0

    .line 298
    .restart local v0    # "id":J
    goto :goto_1

    .line 312
    :cond_0
    new-instance v4, Landroid/database/SQLException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Failed to insert row into "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 285
    nop

    :sswitch_data_0
    .sparse-switch
        0x7d0 -> :sswitch_0
        0x1770 -> :sswitch_1
        0x1b58 -> :sswitch_2
    .end sparse-switch
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    .line 223
    new-instance v0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "shootingmodemanager.db"

    const/4 v3, 0x0

    const/16 v4, 0xf

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    iput-object v0, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    .line 229
    invoke-direct {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->initializeSQLiteStatements()V

    .line 232
    iget-object v0, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mShootingModesMaxOrder:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getLatestShootingModesOrder()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 234
    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 481
    iget-object v2, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v2}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 485
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    sget-object v2, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    .line 487
    .local v9, "match":I
    sparse-switch v9, :sswitch_data_0

    .line 525
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 489
    :sswitch_0
    const-string v1, "view_packages"

    .local v1, "tableName":Ljava/lang/String;
    :goto_0
    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    .line 528
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 532
    .local v8, "c":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-interface {v8, v2, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 533
    return-object v8

    .line 495
    .end local v1    # "tableName":Ljava/lang/String;
    .end local v8    # "c":Landroid/database/Cursor;
    :sswitch_1
    const-string v1, "view_shooting_modes"

    .line 497
    .restart local v1    # "tableName":Ljava/lang/String;
    const/16 v2, 0x1770

    if-ne v9, v2, :cond_0

    .line 498
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 499
    const-string p3, "deleted = 0"

    .line 506
    :cond_0
    :goto_1
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 507
    const-string p5, "sm_order"

    goto :goto_0

    .line 501
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "deleted = 0 AND ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_1

    .line 509
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sm_order, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 511
    goto :goto_0

    .line 515
    .end local v1    # "tableName":Ljava/lang/String;
    :sswitch_2
    const-string v1, "view_shooting_modes_titles"

    .line 517
    .restart local v1    # "tableName":Ljava/lang/String;
    invoke-static {p5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 518
    const-string p5, "package_id"

    goto :goto_0

    .line 520
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "package_id, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    .line 522
    goto :goto_0

    .line 487
    nop

    :sswitch_data_0
    .sparse-switch
        0x7d0 -> :sswitch_0
        0x1770 -> :sswitch_1
        0x1774 -> :sswitch_1
        0x1b58 -> :sswitch_2
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 539
    sget-object v4, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 540
    .local v2, "match":I
    const/4 v0, 0x0

    .line 542
    .local v0, "count":I
    sparse-switch v2, :sswitch_data_0

    .line 645
    :goto_0
    return v0

    .line 545
    :sswitch_0
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 547
    .local v1, "id":I
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 548
    iget-object v5, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementsLock:Ljava/lang/Object;

    monitor-enter v5

    .line 549
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateById:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x1

    const-string v7, "deleted"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 550
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateById:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x2

    const-string v7, "sm_order"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 551
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateById:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x3

    int-to-long v8, v1

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 552
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateById:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v0

    .line 553
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateById:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 554
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 565
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 554
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 556
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_ID = ? AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 559
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p4, v4}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 561
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v4}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "shooting_modes"

    invoke-virtual {v4, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 570
    .end local v1    # "id":I
    :sswitch_1
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 572
    .local v3, "order":I
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 573
    iget-object v5, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementsLock:Ljava/lang/Object;

    monitor-enter v5

    .line 574
    :try_start_2
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateByOrder:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x1

    const-string v7, "deleted"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 575
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateByOrder:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x2

    const-string v7, "sm_order"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 576
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateByOrder:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x3

    int-to-long v8, v3

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 577
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateByOrder:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v0

    .line 578
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateByOrder:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 579
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 590
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    .line 579
    :catchall_1
    move-exception v4

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v4

    .line 581
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sm_order = ? AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 584
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p4, v4}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 586
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v4}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "shooting_modes"

    invoke-virtual {v4, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_2

    .line 595
    .end local v3    # "order":I
    :sswitch_2
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 597
    .restart local v1    # "id":I
    iget-object v5, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementsLock:Ljava/lang/Object;

    monitor-enter v5

    .line 598
    :try_start_4
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateOrderById:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x1

    const-string v7, "sm_order"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 599
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateOrderById:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x2

    int-to-long v8, v1

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 600
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateOrderById:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v0

    .line 601
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementShootingModesUpdateOrderById:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 602
    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 604
    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    .line 602
    :catchall_2
    move-exception v4

    :try_start_5
    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v4

    .line 610
    .end local v1    # "id":I
    :sswitch_3
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v4}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "shooting_modes"

    invoke-virtual {v4, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 612
    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    .line 616
    :sswitch_4
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 618
    .restart local v1    # "id":I
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 619
    iget-object v5, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementsLock:Ljava/lang/Object;

    monitor-enter v5

    .line 620
    :try_start_6
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesUpdate:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x1

    const-string v7, "title"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 621
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesUpdate:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x2

    const-string v7, "description"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 622
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesUpdate:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x3

    const-string v7, "locale"

    invoke-virtual {p2, v7}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 623
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesUpdate:Landroid/database/sqlite/SQLiteStatement;

    const/4 v6, 0x4

    int-to-long v8, v1

    invoke-virtual {v4, v6, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 624
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesUpdate:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v0

    .line 625
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mStatementSMTitlesUpdate:Landroid/database/sqlite/SQLiteStatement;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 626
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 637
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, p1, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 638
    invoke-virtual {p0}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->SHOOTING_MODES_URI:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    .line 626
    :catchall_3
    move-exception v4

    :try_start_7
    monitor-exit v5
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v4

    .line 628
    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_ID = ? AND "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 631
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p4, v4}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->insertSelectionArg([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p4

    .line 633
    iget-object v4, p0, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeProvider;->mDbHelper:Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;

    invoke-virtual {v4}, Lcom/samsung/android/provider/shootingmodeprovider/ShootingModeDatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    const-string v5, "shooting_modes_titles"

    invoke-virtual {v4, v5, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_3

    .line 542
    nop

    :sswitch_data_0
    .sparse-switch
        0x1770 -> :sswitch_3
        0x1771 -> :sswitch_0
        0x1772 -> :sswitch_1
        0x1773 -> :sswitch_2
        0x1774 -> :sswitch_3
        0x1b59 -> :sswitch_4
    .end sparse-switch
.end method
