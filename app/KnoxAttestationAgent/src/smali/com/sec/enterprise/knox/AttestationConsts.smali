.class public Lcom/sec/enterprise/knox/AttestationConsts;
.super Ljava/lang/Object;
.source "AttestationConsts.java"


# static fields
.field public static final ACTION_KNOX_ATTESTATION_RESULT:Ljava/lang/String; = "com.sec.enterprise.knox.intent.action.KNOX_ATTESTATION_RESULT"

.field public static final EXTRA_ATTESTATION_DATA:Ljava/lang/String; = "com.sec.enterprise.knox.intent.extra.ATTESTATION_DATA"

.field public static final EXTRA_ERROR_MSG:Ljava/lang/String; = "com.sec.enterprise.knox.intent.extra.ERROR_MSG"

.field public static final EXTRA_NETWORK_ERROR:Ljava/lang/String; = "com.sec.enterprise.knox.intent.extra.NETWORK_ERROR"

.field public static final EXTRA_RESULT:Ljava/lang/String; = "com.sec.enterprise.knox.intent.extra.RESULT"

.field public static final RESULT_ATTESTATION_SUCCESSFUL:I = 0x0

.field public static final RESULT_ERROR_DEVICE_NOT_SUPPORTED:I = -0x3

.field public static final RESULT_ERROR_INVALID_NONCE:I = -0x5

.field public static final RESULT_ERROR_INVALID_URL:I = -0x8

.field public static final RESULT_ERROR_MDM_PERMISSION:I = -0x1

.field public static final RESULT_ERROR_NETWORK_FAILURE:I = -0x7

.field public static final RESULT_ERROR_NETWORK_UNAVAILABLE:I = -0x6

.field public static final RESULT_ERROR_SERVER_INTERNAL:I = -0x9

.field public static final RESULT_ERROR_SERVER_UNAVAILABLE:I = -0xa

.field public static final RESULT_ERROR_TIMA_INTERNAL:I = -0x2

.field public static final RESULT_ERROR_UNKNOWN:I = -0x4


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
