.class Lcom/sec/enterprise/knox/Attestation$1;
.super Lcom/sec/enterprise/knox/IAttestation$Stub;
.source "Attestation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/Attestation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/knox/Attestation;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/Attestation;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/sec/enterprise/knox/Attestation$1;->this$0:Lcom/sec/enterprise/knox/Attestation;

    invoke-direct {p0}, Lcom/sec/enterprise/knox/IAttestation$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public startAttestation_nonce(Ljava/lang/String;)V
    .locals 6
    .param p1, "nonce"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 152
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "Attestation.startAttestation_nonce"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 156
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation$1;->this$0:Lcom/sec/enterprise/knox/Attestation;

    # invokes: Lcom/sec/enterprise/knox/Attestation;->enforceAttestationPermission()V
    invoke-static {v2}, Lcom/sec/enterprise/knox/Attestation;->access$000(Lcom/sec/enterprise/knox/Attestation;)V

    .line 158
    if-nez p1, :cond_0

    .line 159
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation$1;->this$0:Lcom/sec/enterprise/knox/Attestation;

    const/4 v3, -0x5

    const/4 v4, 0x0

    # invokes: Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/sec/enterprise/knox/Attestation;->access$100(Lcom/sec/enterprise/knox/Attestation;ILjava/lang/String;)V

    .line 162
    :cond_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation$1;->this$0:Lcom/sec/enterprise/knox/Attestation;

    # getter for: Lcom/sec/enterprise/knox/Attestation;->mTimaService:Landroid/service/tima/ITimaService;
    invoke-static {v2}, Lcom/sec/enterprise/knox/Attestation;->access$200(Lcom/sec/enterprise/knox/Attestation;)Landroid/service/tima/ITimaService;

    move-result-object v2

    if-nez v2, :cond_1

    .line 163
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation$1;->this$0:Lcom/sec/enterprise/knox/Attestation;

    const/4 v3, -0x2

    const/4 v4, 0x0

    # invokes: Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/sec/enterprise/knox/Attestation;->access$100(Lcom/sec/enterprise/knox/Attestation;ILjava/lang/String;)V

    .line 164
    const-string v2, "KnoxAttestation"

    const-string v3, "Tima Service is not available"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :goto_0
    return-void

    .line 169
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation$1;->this$0:Lcom/sec/enterprise/knox/Attestation;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/Attestation;->getHandler()Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v2, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 170
    .local v1, "message":Landroid/os/Message;
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation$1;->this$0:Lcom/sec/enterprise/knox/Attestation;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/Attestation;->getHandler()Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 171
    .end local v1    # "message":Landroid/os/Message;
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Ljava/lang/SecurityException;
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation$1;->this$0:Lcom/sec/enterprise/knox/Attestation;

    const/4 v3, -0x1

    # invokes: Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V
    invoke-static {v2, v3, v5}, Lcom/sec/enterprise/knox/Attestation;->access$100(Lcom/sec/enterprise/knox/Attestation;ILjava/lang/String;)V

    .line 173
    const-string v2, "KnoxAttestation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startAttestation_nonce() Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
