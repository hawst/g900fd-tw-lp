.class public Lcom/sec/enterprise/knox/Attestation;
.super Landroid/app/Service;
.source "Attestation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;
    }
.end annotation


# static fields
.field private static final ATTESTATION_MEASUREMENTS_URL:Ljava/lang/String; = "/measurements"

.field private static final ATTESTATION_NONCE_URL:Ljava/lang/String; = "/nonce32?chip_id="

.field private static final ATTESTATION_PREFS:Ljava/lang/String; = "com.sec.enterprise.knox.attestationprefs"

.field private static final ATTESTATION_SERVER_URL:Ljava/lang/String; = "ATTESTATION_SERVER_URL"

.field private static final ATTESTATION_START:I = 0x2

.field public static final BIND_KNOX_ATTESTATION_SERVICE:Ljava/lang/String; = "com.sec.enterprise.knox.intent.action.BIND_KNOX_ATTESTATION_SERVICE"

.field private static final DEFAULT_ATTESTATION_URL:Ljava/lang/String; = "https://attest-api.knox-cloud.com/v1"

.field private static final DEVICE_KNOX_ID_LENGTH:I = 0x8

.field private static final HTTP_ERROR_BAD_BLOB_DATA:I = 0x193

.field private static final HTTP_ERROR_INTERNAL_SERVER:I = 0x1f4

.field private static final HTTP_ERROR_REQUEST_TIMEOUT:I = 0x198

.field private static final HTTP_ERROR_SERVER_UNAVAILABLE:I = 0x1f7

.field private static final HTTP_ERROR_URL_RESOURCE_NOT_FOUND:I = 0x194

.field private static final HTTP_POST_GET_RESPONSE_OK:I = 0xc8

.field private static final HTTP_POST_MEASUREMENTS_RESPONSE_OK:I = 0xcc

.field private static final KNOX_ATTESTATION_PERMISSION:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_ATTESTATION"

.field private static final KNOX_ATTESTATION_PERMISSION_ERROR:Ljava/lang/String; = "Need com.sec.enterprise.knox.permission.KNOX_ATTESTATION permission"

.field private static final NONCE:Ljava/lang/String; = "nonce"

.field private static final REQUEST_NONCE:I = 0x1

.field private static final SEND_BLOB:I = 0x3

.field private static final TAG:Ljava/lang/String; = "KnoxAttestation"

.field private static final TAL_KNOX_KEY_ERROR:I = 0x5a

.field private static final TIMA_ATTESTATION_SUCCESS:I = 0x0

.field private static final TIMA_ERROR_DEVICE_NOT_SUPPORTED:I = 0x4

.field private static final TIMA_INVALID_NONCE:I = 0x5b


# instance fields
.field private final TIMA_SERVICE:Ljava/lang/String;

.field private _mContext:Landroid/content/Context;

.field private final attestationServerPresent:Z

.field private final mBinder:Lcom/sec/enterprise/knox/IAttestation$Stub;

.field private mHandler:Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mTimaService:Landroid/service/tima/ITimaService;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/knox/Attestation;->attestationServerPresent:Z

    .line 88
    const-string v0, "tima"

    iput-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->TIMA_SERVICE:Ljava/lang/String;

    .line 148
    new-instance v0, Lcom/sec/enterprise/knox/Attestation$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/Attestation$1;-><init>(Lcom/sec/enterprise/knox/Attestation;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mBinder:Lcom/sec/enterprise/knox/IAttestation$Stub;

    .line 227
    return-void
.end method

.method static synthetic access$000(Lcom/sec/enterprise/knox/Attestation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/knox/Attestation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/enterprise/knox/Attestation;->enforceAttestationPermission()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/enterprise/knox/Attestation;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/knox/Attestation;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/enterprise/knox/Attestation;)Landroid/service/tima/ITimaService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/knox/Attestation;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mTimaService:Landroid/service/tima/ITimaService;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/enterprise/knox/Attestation;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/knox/Attestation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/Attestation;->attestationStartProcess(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/enterprise/knox/Attestation;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/knox/Attestation;
    .param p1, "x1"    # [B

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/Attestation;->sendBlobData([B)V

    return-void
.end method

.method private attestationStartProcess(Ljava/lang/String;)V
    .locals 13
    .param p1, "nonce"    # Ljava/lang/String;

    .prologue
    const/4 v12, -0x2

    const/4 v11, 0x0

    .line 286
    invoke-direct {p0, p1}, Lcom/sec/enterprise/knox/Attestation;->getByteArray(Ljava/lang/String;)[B

    move-result-object v7

    .line 287
    .local v7, "nonceArray":[B
    if-nez v7, :cond_0

    .line 334
    :goto_0
    return-void

    .line 290
    :cond_0
    const/4 v0, 0x0

    .line 293
    .local v0, "blob":[B
    :try_start_0
    iget-object v8, p0, Lcom/sec/enterprise/knox/Attestation;->mTimaService:Landroid/service/tima/ITimaService;

    invoke-interface {v8, v7}, Landroid/service/tima/ITimaService;->attestation([B)[B

    move-result-object v1

    .line 294
    .local v1, "blobByteArray":[B
    if-nez v1, :cond_1

    .line 295
    const/4 v8, -0x2

    const/4 v9, 0x0

    invoke-direct {p0, v8, v9}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 296
    const-string v8, "KnoxAttestation"

    const-string v9, "Blob from TIMA is invalid"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 328
    .end local v1    # "blobByteArray":[B
    :catch_0
    move-exception v3

    .line 329
    .local v3, "e":Ljava/lang/Exception;
    invoke-direct {p0, v12, v11}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 330
    const-string v8, "KnoxAttestation"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed talking with attestation policy/Invalid Nonce length"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 300
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "blobByteArray":[B
    :cond_1
    :try_start_1
    invoke-direct {p0, v1}, Lcom/sec/enterprise/knox/Attestation;->checkExitCode([B)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 301
    array-length v8, v1

    add-int/lit8 v8, v8, -0x1

    new-array v0, v8, [B

    .line 302
    const/4 v8, 0x1

    const/4 v9, 0x0

    array-length v10, v1

    add-int/lit8 v10, v10, -0x1

    invoke-static {v1, v8, v0, v9, v10}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 305
    new-instance v8, Ljava/lang/Byte;

    const/4 v9, 0x0

    aget-byte v9, v0, v9

    invoke-direct {v8, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v8}, Ljava/lang/Byte;->intValue()I

    move-result v4

    .line 306
    .local v4, "isBlobValid":I
    if-eqz v4, :cond_3

    .line 307
    const/4 v8, 0x4

    if-ne v4, v8, :cond_2

    .line 308
    const/4 v8, -0x3

    const/4 v9, 0x0

    invoke-direct {p0, v8, v9}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 313
    :goto_1
    new-instance v8, Ljava/lang/Byte;

    const/4 v9, 0x1

    aget-byte v9, v0, v9

    invoke-direct {v8, v9}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v8}, Ljava/lang/Byte;->intValue()I

    move-result v6

    .line 314
    .local v6, "messageLen":I
    new-array v2, v6, [B

    .line 315
    .local v2, "blobMessage":[B
    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-static {v0, v8, v2, v9, v6}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 316
    const-string v8, "KnoxAttestation"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Blob is not valid. ErrorCode= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " Message= "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v2}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 310
    .end local v2    # "blobMessage":[B
    .end local v6    # "messageLen":I
    :cond_2
    const/4 v8, -0x2

    const/4 v9, 0x0

    invoke-direct {p0, v8, v9}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    goto :goto_1

    .line 320
    :cond_3
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/Attestation;->getHandler()Landroid/os/Handler;

    move-result-object v8

    const/4 v9, 0x3

    invoke-virtual {v8, v9, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    .line 321
    .local v5, "message":Landroid/os/Message;
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/Attestation;->getHandler()Landroid/os/Handler;

    move-result-object v8

    invoke-virtual {v8, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 324
    .end local v4    # "isBlobValid":I
    .end local v5    # "message":Landroid/os/Message;
    :cond_4
    const-string v8, "KnoxAttestation"

    const-string v9, "Error in exit code byte array"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private checkExitCode([B)Z
    .locals 5
    .param p1, "blob"    # [B

    .prologue
    const/4 v4, 0x0

    .line 337
    new-instance v2, Ljava/lang/Byte;

    const/4 v3, 0x0

    aget-byte v3, p1, v3

    invoke-direct {v2, v3}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v2}, Ljava/lang/Byte;->intValue()I

    move-result v0

    .line 338
    .local v0, "isBlobValid":I
    const/4 v1, 0x0

    .line 340
    .local v1, "result":Z
    sparse-switch v0, :sswitch_data_0

    .line 353
    const/4 v2, -0x2

    invoke-direct {p0, v2, v4}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 354
    const/4 v1, 0x0

    .line 357
    :goto_0
    return v1

    .line 342
    :sswitch_0
    const/4 v2, -0x5

    invoke-direct {p0, v2, v4}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 343
    const/4 v1, 0x0

    .line 344
    goto :goto_0

    .line 346
    :sswitch_1
    const/4 v2, -0x3

    invoke-direct {p0, v2, v4}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 347
    const/4 v1, 0x0

    .line 348
    goto :goto_0

    .line 350
    :sswitch_2
    const/4 v1, 0x1

    .line 351
    goto :goto_0

    .line 340
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_2
        0x5a -> :sswitch_1
        0x5b -> :sswitch_0
    .end sparse-switch
.end method

.method private enforceAttestationPermission()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 521
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    .line 522
    .local v2, "uid":I
    iget-object v3, p0, Lcom/sec/enterprise/knox/Attestation;->_mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v1

    .line 523
    .local v1, "caller":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/enterprise/knox/Attestation;->_mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 524
    .local v0, "PkgName":Ljava/lang/String;
    const-string v3, "KnoxAttestation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "caller: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "PkgName: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 542
    :goto_0
    return-void

    .line 535
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/knox/Attestation;->_mContext:Landroid/content/Context;

    const-string v4, "com.sec.enterprise.knox.permission.KNOX_ATTESTATION"

    const-string v5, "Need com.sec.enterprise.knox.permission.KNOX_ATTESTATION permission"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->enforceCallingPermission(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 537
    :catchall_0
    move-exception v3

    throw v3
.end method

.method private executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 7
    .param p1, "request"    # Lorg/apache/http/client/methods/HttpUriRequest;

    .prologue
    const/16 v4, -0xa

    const/4 v3, 0x0

    .line 681
    new-instance v1, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v1}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 682
    .local v1, "httpClient":Lorg/apache/http/impl/client/DefaultHttpClient;
    const/4 v2, 0x0

    .line 684
    .local v2, "httpResponse":Lorg/apache/http/HttpResponse;
    :try_start_0
    invoke-virtual {v1, p1}, Lorg/apache/http/impl/client/DefaultHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Lorg/apache/http/conn/HttpHostConnectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    move-object v3, v2

    .line 697
    :goto_0
    return-object v3

    .line 686
    :catch_0
    move-exception v0

    .line 687
    .local v0, "ex":Lorg/apache/http/conn/HttpHostConnectException;
    invoke-direct {p0, v4, v3}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 688
    const-string v4, "KnoxAttestation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Http execute HttpHostConnectException. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 691
    .end local v0    # "ex":Lorg/apache/http/conn/HttpHostConnectException;
    :catch_1
    move-exception v0

    .line 692
    .local v0, "ex":Ljava/lang/Exception;
    invoke-direct {p0, v4, v3}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 693
    const-string v4, "KnoxAttestation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Http execute exception. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getAttestationServerNonce(I)Ljava/lang/String;
    .locals 21
    .param p1, "deviceKnoxId"    # I

    .prologue
    .line 545
    const-string v18, "KnoxAttestation"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getAttestationServerNonce() device id: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    const/4 v14, 0x0

    .line 548
    .local v14, "result":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/sec/enterprise/knox/Attestation;->isNetWorkAvailable()Z

    move-result v18

    if-nez v18, :cond_0

    .line 549
    const/16 v18, -0x6

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    move-object v15, v14

    .line 605
    .end local v14    # "result":Ljava/lang/String;
    .local v15, "result":Ljava/lang/String;
    :goto_0
    return-object v15

    .line 553
    .end local v15    # "result":Ljava/lang/String;
    .restart local v14    # "result":Ljava/lang/String;
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/sec/enterprise/knox/Attestation;->getAttestationServerUrl()Ljava/lang/String;

    move-result-object v3

    .line 554
    .local v3, "attestationServerUrl":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 556
    .local v17, "strBuilder":Ljava/lang/StringBuilder;
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    .line 560
    .local v8, "hexDeviceId":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v11

    .line 561
    .local v11, "len":I
    const/16 v18, 0x8

    move/from16 v0, v18

    if-ge v11, v0, :cond_2

    .line 562
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 563
    .local v16, "sb":Ljava/lang/StringBuilder;
    rsub-int/lit8 v12, v11, 0x8

    .line 564
    .local v12, "lengthToAdd":I
    :goto_1
    if-lez v12, :cond_1

    .line 565
    const-string v18, "0"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 566
    add-int/lit8 v12, v12, -0x1

    goto :goto_1

    .line 568
    :cond_1
    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 572
    .end local v12    # "lengthToAdd":I
    .end local v16    # "sb":Ljava/lang/StringBuilder;
    :cond_2
    const-string v18, "/nonce32?chip_id="

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 576
    const/16 v18, 0x0

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v3, v1}, Lcom/sec/enterprise/knox/Attestation;->post(Ljava/lang/String;Lorg/apache/http/entity/AbstractHttpEntity;)Lorg/apache/http/HttpResponse;

    move-result-object v9

    .line 578
    .local v9, "httpResponse":Lorg/apache/http/HttpResponse;
    if-eqz v9, :cond_4

    .line 579
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v13

    .line 580
    .local v13, "responseCode":I
    const/16 v18, 0xc8

    move/from16 v0, v18

    if-eq v13, v0, :cond_3

    .line 581
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v6

    .line 582
    .local v6, "errMessage":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v6}, Lcom/sec/enterprise/knox/Attestation;->sendHttpResponseToMDM(ILjava/lang/String;)V

    .line 583
    const-string v18, "KnoxAttestation"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Http request execution to get nonce failed. Return code: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " message= "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v15, v14

    .line 585
    .end local v14    # "result":Ljava/lang/String;
    .restart local v15    # "result":Ljava/lang/String;
    goto/16 :goto_0

    .line 589
    .end local v6    # "errMessage":Ljava/lang/String;
    .end local v15    # "result":Ljava/lang/String;
    .restart local v14    # "result":Ljava/lang/String;
    :cond_3
    invoke-interface {v9}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v4

    .line 590
    .local v4, "entityResult":Lorg/apache/http/HttpEntity;
    const-string v18, "UTF-8"

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 594
    .local v5, "entityString":Ljava/lang/String;
    :try_start_1
    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 595
    .local v10, "jsonObject":Lorg/json/JSONObject;
    const-string v18, "nonce"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v14

    .end local v4    # "entityResult":Lorg/apache/http/HttpEntity;
    .end local v5    # "entityString":Ljava/lang/String;
    .end local v9    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v10    # "jsonObject":Lorg/json/JSONObject;
    .end local v13    # "responseCode":I
    :cond_4
    :goto_2
    move-object v15, v14

    .line 605
    .end local v14    # "result":Ljava/lang/String;
    .restart local v15    # "result":Ljava/lang/String;
    goto/16 :goto_0

    .line 596
    .end local v15    # "result":Ljava/lang/String;
    .restart local v4    # "entityResult":Lorg/apache/http/HttpEntity;
    .restart local v5    # "entityString":Ljava/lang/String;
    .restart local v9    # "httpResponse":Lorg/apache/http/HttpResponse;
    .restart local v13    # "responseCode":I
    .restart local v14    # "result":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 597
    .local v7, "ex":Lorg/json/JSONException;
    :try_start_2
    const-string v18, "KnoxAttestation"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "JSONObject exception: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 601
    .end local v4    # "entityResult":Lorg/apache/http/HttpEntity;
    .end local v5    # "entityString":Ljava/lang/String;
    .end local v7    # "ex":Lorg/json/JSONException;
    .end local v9    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v13    # "responseCode":I
    :catch_1
    move-exception v7

    .line 602
    .local v7, "ex":Ljava/io/IOException;
    const-string v18, "KnoxAttestation"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "getAttestationServerNonce exception: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private getAttestationServerUrl()Ljava/lang/String;
    .locals 5

    .prologue
    .line 385
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation;->_mContext:Landroid/content/Context;

    const-string v3, "com.sec.enterprise.knox.attestationprefs"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 387
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v0, "https://attest-api.knox-cloud.com/v1"

    .line 388
    .local v0, "data":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 389
    const-string v2, "ATTESTATION_SERVER_URL"

    const-string v3, "https://attest-api.knox-cloud.com/v1"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 390
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 391
    const-string v0, "https://attest-api.knox-cloud.com/v1"

    .line 395
    :cond_0
    return-object v0
.end method

.method private getByteArray(Ljava/lang/String;)[B
    .locals 7
    .param p1, "nonce"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x10

    .line 736
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 737
    .local v2, "len":I
    and-int/lit8 v3, v2, 0x1

    if-eqz v3, :cond_1

    .line 738
    const/4 v3, -0x5

    invoke-direct {p0, v3, v0}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 746
    :cond_0
    return-object v0

    .line 741
    :cond_1
    div-int/lit8 v3, v2, 0x2

    new-array v0, v3, [B

    .line 742
    .local v0, "data":[B
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 743
    div-int/lit8 v3, v1, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v0, v3

    .line 742
    add-int/lit8 v1, v1, 0x2

    goto :goto_0
.end method

.method private getEntity([BLjava/lang/String;)Lorg/apache/http/entity/AbstractHttpEntity;
    .locals 8
    .param p1, "data"    # [B
    .param p2, "contentType"    # Ljava/lang/String;

    .prologue
    .line 701
    const/4 v2, 0x0

    .line 702
    .local v2, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    if-eqz p1, :cond_1

    .line 704
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    array-length v5, p1

    invoke-direct {v0, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 705
    .local v0, "bStream":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 706
    .local v1, "dos":Ljava/io/DataOutputStream;
    invoke-virtual {v1, p1}, Ljava/io/DataOutputStream;->write([B)V

    .line 707
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->close()V

    .line 709
    new-instance v3, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {v3, v5}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 710
    .end local v2    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    .local v3, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    if-eqz p2, :cond_0

    .line 711
    :try_start_1
    invoke-virtual {v3, p2}, Lorg/apache/http/entity/AbstractHttpEntity;->setContentType(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    move-object v2, v3

    .line 718
    .end local v0    # "bStream":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "dos":Ljava/io/DataOutputStream;
    .end local v3    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    .restart local v2    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    :cond_1
    :goto_0
    return-object v2

    .line 713
    :catch_0
    move-exception v4

    .line 714
    .local v4, "ex":Ljava/io/IOException;
    :goto_1
    const-string v5, "KnoxAttestation"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception while creating the entity "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 713
    .end local v2    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    .end local v4    # "ex":Ljava/io/IOException;
    .restart local v0    # "bStream":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "dos":Ljava/io/DataOutputStream;
    .restart local v3    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    :catch_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    .restart local v2    # "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    goto :goto_1
.end method

.method private isNetWorkAvailable()Z
    .locals 4

    .prologue
    .line 722
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation;->_mContext:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 724
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 725
    .local v1, "netInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 726
    const/4 v2, 0x1

    .line 729
    :goto_0
    return v2

    .line 728
    :cond_0
    const-string v2, "KnoxAttestation"

    const-string v3, "Network is not available"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private post(Ljava/lang/String;Lorg/apache/http/entity/AbstractHttpEntity;)Lorg/apache/http/HttpResponse;
    .locals 7
    .param p1, "attestationServerUrl"    # Ljava/lang/String;
    .param p2, "entity"    # Lorg/apache/http/entity/AbstractHttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 658
    const/4 v1, 0x0

    .line 660
    .local v1, "post":Lorg/apache/http/client/methods/HttpPost;
    :try_start_0
    new-instance v2, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v2, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 661
    .end local v1    # "post":Lorg/apache/http/client/methods/HttpPost;
    .local v2, "post":Lorg/apache/http/client/methods/HttpPost;
    if-eqz p2, :cond_0

    .line 662
    :try_start_1
    invoke-virtual {v2, p2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 677
    :cond_0
    invoke-direct {p0, v2}, Lcom/sec/enterprise/knox/Attestation;->executeHttpRequest(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    move-object v1, v2

    .end local v2    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v1    # "post":Lorg/apache/http/client/methods/HttpPost;
    :goto_0
    return-object v3

    .line 665
    :catch_0
    move-exception v0

    .line 666
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    :goto_1
    const/16 v4, -0xa

    invoke-direct {p0, v4, v3}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 667
    const-string v4, "KnoxAttestation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Http post exception. IllegalArgumentException. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 671
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 672
    .local v0, "ex":Ljava/lang/Exception;
    :goto_2
    const/4 v4, -0x4

    invoke-direct {p0, v4, v3}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 673
    const-string v4, "KnoxAttestation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Http post exception. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 671
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v2    # "post":Lorg/apache/http/client/methods/HttpPost;
    :catch_2
    move-exception v0

    move-object v1, v2

    .end local v2    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v1    # "post":Lorg/apache/http/client/methods/HttpPost;
    goto :goto_2

    .line 665
    .end local v1    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v2    # "post":Lorg/apache/http/client/methods/HttpPost;
    :catch_3
    move-exception v0

    move-object v1, v2

    .end local v2    # "post":Lorg/apache/http/client/methods/HttpPost;
    .restart local v1    # "post":Lorg/apache/http/client/methods/HttpPost;
    goto :goto_1
.end method

.method private postBlobToAttestationServer([B)Z
    .locals 13
    .param p1, "data"    # [B

    .prologue
    const/4 v9, 0x0

    .line 614
    invoke-direct {p0}, Lcom/sec/enterprise/knox/Attestation;->isNetWorkAvailable()Z

    move-result v10

    if-nez v10, :cond_1

    .line 615
    const/4 v10, -0x6

    const/4 v11, 0x0

    invoke-direct {p0, v10, v11}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 651
    :cond_0
    :goto_0
    return v9

    .line 619
    :cond_1
    if-eqz p1, :cond_0

    .line 620
    invoke-direct {p0}, Lcom/sec/enterprise/knox/Attestation;->getAttestationServerUrl()Ljava/lang/String;

    move-result-object v0

    .line 621
    .local v0, "attestationServerUrl":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 622
    .local v8, "strBuilder":Ljava/lang/StringBuilder;
    const-string v10, "/measurements"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 624
    const-string v10, "application/octet-stream"

    invoke-direct {p0, p1, v10}, Lcom/sec/enterprise/knox/Attestation;->getEntity([BLjava/lang/String;)Lorg/apache/http/entity/AbstractHttpEntity;

    move-result-object v1

    .line 626
    .local v1, "entity":Lorg/apache/http/entity/AbstractHttpEntity;
    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/sec/enterprise/knox/Attestation;->post(Ljava/lang/String;Lorg/apache/http/entity/AbstractHttpEntity;)Lorg/apache/http/HttpResponse;

    move-result-object v6

    .line 627
    .local v6, "httpResponse":Lorg/apache/http/HttpResponse;
    if-eqz v6, :cond_0

    .line 628
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v7

    .line 630
    .local v7, "responseCode":I
    const/16 v10, 0xcc

    if-eq v7, v10, :cond_2

    .line 631
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v10

    invoke-interface {v10}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v4

    .line 632
    .local v4, "errMessage":Ljava/lang/String;
    invoke-direct {p0, v7, v4}, Lcom/sec/enterprise/knox/Attestation;->sendHttpResponseToMDM(ILjava/lang/String;)V

    .line 633
    const-string v10, "KnoxAttestation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Http request execution to post device measurements failed. Return code: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " message= "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    invoke-interface {v6}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 638
    .local v2, "entityResult":Lorg/apache/http/HttpEntity;
    const-string v10, "UTF-8"

    invoke-static {v2, v10}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 639
    .local v3, "entityString":Ljava/lang/String;
    const-string v10, "KnoxAttestation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ErrorString: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 647
    .end local v2    # "entityResult":Lorg/apache/http/HttpEntity;
    .end local v3    # "entityString":Ljava/lang/String;
    .end local v4    # "errMessage":Ljava/lang/String;
    .end local v6    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v7    # "responseCode":I
    :catch_0
    move-exception v5

    .line 648
    .local v5, "ex":Ljava/io/IOException;
    const-string v10, "KnoxAttestation"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "postBlobToAttestationServer exception: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {v5}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 643
    .end local v5    # "ex":Ljava/io/IOException;
    .restart local v6    # "httpResponse":Lorg/apache/http/HttpResponse;
    .restart local v7    # "responseCode":I
    :cond_2
    const/4 v9, 0x1

    goto/16 :goto_0
.end method

.method private sendBlobData([B)V
    .locals 3
    .param p1, "blob"    # [B

    .prologue
    .line 377
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 378
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.enterprise.knox.intent.action.KNOX_ATTESTATION_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 379
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 380
    const-string v1, "com.sec.enterprise.knox.intent.extra.ATTESTATION_DATA"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 381
    iget-object v1, p0, Lcom/sec/enterprise/knox/Attestation;->_mContext:Landroid/content/Context;

    const-string v2, "com.sec.enterprise.knox.permission.KNOX_ATTESTATION"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 382
    return-void
.end method

.method private sendErrorToMDM(ILjava/lang/String;)V
    .locals 3
    .param p1, "errCode"    # I
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 403
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 404
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.sec.enterprise.knox.intent.action.KNOX_ATTESTATION_RESULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 406
    packed-switch p1, :pswitch_data_0

    .line 461
    :pswitch_0
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/4 v2, -0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 462
    if-eqz p2, :cond_0

    .line 463
    const-string v1, "com.sec.enterprise.knox.intent.extra.ERROR_MSG"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 467
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/Attestation;->_mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 468
    return-void

    .line 409
    :pswitch_1
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 414
    :pswitch_2
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/4 v2, -0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 419
    :pswitch_3
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/4 v2, -0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 425
    :pswitch_4
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/16 v2, -0xa

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 430
    :pswitch_5
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/4 v2, -0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 435
    :pswitch_6
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/4 v2, -0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 437
    if-eqz p2, :cond_0

    .line 438
    const-string v1, "com.sec.enterprise.knox.intent.extra.ERROR_MSG"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 443
    :pswitch_7
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/4 v2, -0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 448
    :pswitch_8
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/4 v2, -0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 453
    :pswitch_9
    const-string v1, "com.sec.enterprise.knox.intent.extra.RESULT"

    const/16 v2, -0x9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 455
    if-eqz p2, :cond_0

    .line 456
    const-string v1, "com.sec.enterprise.knox.intent.extra.ERROR_MSG"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 406
    nop

    :pswitch_data_0
    .packed-switch -0xa
        :pswitch_4
        :pswitch_9
        :pswitch_8
        :pswitch_3
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_1
    .end packed-switch
.end method

.method private sendHttpResponseToMDM(ILjava/lang/String;)V
    .locals 3
    .param p1, "responseCode"    # I
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 477
    sparse-switch p1, :sswitch_data_0

    .line 504
    const/16 v0, -0x9

    .line 505
    .local v0, "errCode":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 506
    .local v1, "sb":Ljava/lang/StringBuilder;
    if-eqz p2, :cond_0

    .line 507
    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 512
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/sec/enterprise/knox/Attestation;->sendErrorToMDM(ILjava/lang/String;)V

    .line 513
    return-void

    .line 479
    .end local v0    # "errCode":I
    :sswitch_0
    const/16 v0, -0xa

    .line 480
    .restart local v0    # "errCode":I
    goto :goto_0

    .line 483
    .end local v0    # "errCode":I
    :sswitch_1
    const/16 v0, -0x9

    .line 484
    .restart local v0    # "errCode":I
    goto :goto_0

    .line 487
    .end local v0    # "errCode":I
    :sswitch_2
    const/4 v0, -0x5

    .line 488
    .restart local v0    # "errCode":I
    goto :goto_0

    .line 491
    .end local v0    # "errCode":I
    :sswitch_3
    const/16 v0, -0x9

    .line 494
    .restart local v0    # "errCode":I
    const-string p2, "URL Resource Not Found"

    .line 495
    goto :goto_0

    .line 498
    .end local v0    # "errCode":I
    :sswitch_4
    const/4 v0, -0x2

    .line 501
    .restart local v0    # "errCode":I
    goto :goto_0

    .line 477
    nop

    :sswitch_data_0
    .sparse-switch
        0x193 -> :sswitch_4
        0x194 -> :sswitch_3
        0x198 -> :sswitch_2
        0x1f4 -> :sswitch_1
        0x1f7 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public getHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/enterprise/knox/Attestation;->mHandler:Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 97
    const/4 v1, 0x0

    .line 98
    .local v1, "result":Landroid/os/IBinder;
    if-nez p1, :cond_0

    .line 99
    const-string v2, "KnoxAttestation"

    const-string v3, "onBind : intent is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :goto_0
    const/4 v2, 0x0

    :goto_1
    return-object v2

    .line 101
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 102
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v2, "com.sec.enterprise.knox.intent.action.BIND_KNOX_ATTESTATION_SERVICE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/Attestation;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/Attestation;->_mContext:Landroid/content/Context;

    .line 107
    iget-object v1, p0, Lcom/sec/enterprise/knox/Attestation;->mBinder:Lcom/sec/enterprise/knox/IAttestation$Stub;

    move-object v2, v1

    .line 108
    goto :goto_1

    .line 110
    :cond_1
    const-string v2, "KnoxAttestation"

    const-string v3, "onBind : action is null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onCreate()V
    .locals 8

    .prologue
    .line 118
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 121
    :try_start_0
    const-string v4, "android.os.ServiceManager"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 122
    .local v1, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v4, "getService"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 123
    .local v3, "m":Ljava/lang/reflect/Method;
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "tima"

    aput-object v6, v4, v5

    invoke-virtual {v3, v1, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/IBinder;

    .line 124
    .local v0, "b":Landroid/os/IBinder;
    invoke-static {v0}, Landroid/service/tima/ITimaService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/tima/ITimaService;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/enterprise/knox/Attestation;->mTimaService:Landroid/service/tima/ITimaService;

    .line 125
    iget-object v4, p0, Lcom/sec/enterprise/knox/Attestation;->mTimaService:Landroid/service/tima/ITimaService;

    if-nez v4, :cond_0

    .line 126
    const-string v4, "KnoxAttestation"

    const-string v5, "TIMA service is not available!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 139
    .end local v0    # "b":Landroid/os/IBinder;
    .end local v1    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "m":Ljava/lang/reflect/Method;
    :goto_0
    return-void

    .line 129
    :catch_0
    move-exception v2

    .line 130
    .local v2, "e":Ljava/lang/ClassNotFoundException;
    const-string v4, "KnoxAttestation"

    const-string v5, "TIMA service ClassNotFoundException !!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    .end local v2    # "e":Ljava/lang/ClassNotFoundException;
    :cond_0
    :goto_1
    new-instance v4, Landroid/os/HandlerThread;

    const-string v5, "KnoxAttestation"

    invoke-direct {v4, v5}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/enterprise/knox/Attestation;->mHandlerThread:Landroid/os/HandlerThread;

    .line 136
    iget-object v4, p0, Lcom/sec/enterprise/knox/Attestation;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->start()V

    .line 137
    new-instance v4, Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;

    iget-object v5, p0, Lcom/sec/enterprise/knox/Attestation;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;-><init>(Lcom/sec/enterprise/knox/Attestation;Landroid/os/Looper;)V

    iput-object v4, p0, Lcom/sec/enterprise/knox/Attestation;->mHandler:Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;

    goto :goto_0

    .line 131
    :catch_1
    move-exception v2

    .line 132
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "KnoxAttestation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onCreate() Exception: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 144
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    .line 145
    const/4 v0, 0x2

    return v0
.end method
