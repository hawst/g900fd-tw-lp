.class final Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;
.super Landroid/os/Handler;
.source "Attestation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/Attestation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "KnoxAttestationHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/knox/Attestation;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/knox/Attestation;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 229
    iput-object p1, p0, Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;->this$0:Lcom/sec/enterprise/knox/Attestation;

    .line 230
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 231
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 234
    if-eqz p1, :cond_0

    .line 235
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 264
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 243
    :pswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    .line 244
    .local v1, "nonce":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 245
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;->this$0:Lcom/sec/enterprise/knox/Attestation;

    # invokes: Lcom/sec/enterprise/knox/Attestation;->attestationStartProcess(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/sec/enterprise/knox/Attestation;->access$300(Lcom/sec/enterprise/knox/Attestation;Ljava/lang/String;)V

    goto :goto_0

    .line 255
    .end local v1    # "nonce":Ljava/lang/String;
    :pswitch_2
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, [B

    move-object v0, v2

    check-cast v0, [B

    .line 256
    .local v0, "blob":[B
    iget-object v2, p0, Lcom/sec/enterprise/knox/Attestation$KnoxAttestationHandler;->this$0:Lcom/sec/enterprise/knox/Attestation;

    # invokes: Lcom/sec/enterprise/knox/Attestation;->sendBlobData([B)V
    invoke-static {v2, v0}, Lcom/sec/enterprise/knox/Attestation;->access$400(Lcom/sec/enterprise/knox/Attestation;[B)V

    goto :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
