.class final Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;
.super Landroid/view/InputEventReceiver;
.source "AutoDetectInputObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TouchEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;


# direct methods
.method public constructor <init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;Landroid/view/InputChannel;Landroid/os/Looper;)V
    .locals 0
    .param p2, "inputChannel"    # Landroid/view/InputChannel;
    .param p3, "looper"    # Landroid/os/Looper;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    .line 87
    invoke-direct {p0, p2, p3}, Landroid/view/InputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V

    .line 88
    return-void
.end method


# virtual methods
.method public onInputEvent(Landroid/view/InputEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/view/InputEvent;

    .prologue
    .line 92
    const-string v1, "AutoDetectInputObserver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "event : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    instance-of v1, p1, Landroid/view/MotionEvent;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 94
    check-cast v0, Landroid/view/MotionEvent;

    .line 95
    .local v0, "motionEvent":Landroid/view/MotionEvent;
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    .line 96
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    iget-boolean v1, v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->detected:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 97
    const-string v1, "AutoDetectInputObserver"

    const-string v2, "Input Event!!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    const-string v2, "INPUT"

    invoke-interface {v1, v2}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;->onDetected(Ljava/lang/String;)V

    .line 101
    .end local v0    # "motionEvent":Landroid/view/MotionEvent;
    :cond_0
    return-void
.end method
