.class public Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;
.super Lcom/sec/android/emergencymode/service/EmergencyBase;
.source "EmergencyWhiteListManager.java"


# static fields
.field public static final CHATON:Ljava/lang/String; = "CHATON"

.field public static final EM:I = 0x0

.field public static final FMM:Ljava/lang/String; = "FMM"

.field private static final TAG:Ljava/lang/String; = "EmergencyWhiteListManager"

.field public static final UPSM:I = 0x1

.field private static instance:Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;


# instance fields
.field private mActionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAlarmAllowedAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAllAllowedAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAppWidgetList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultAllowedAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mForceEnableList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mPackageGroup:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPermissionList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserAllowedAppList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyBase;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAllAllowedAppList:Ljava/util/ArrayList;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mDefaultAllowedAppList:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mUserAllowedAppList:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAlarmAllowedAppList:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAppWidgetList:Ljava/util/ArrayList;

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mActionList:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPermissionList:Ljava/util/HashMap;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mForceEnableList:Ljava/util/ArrayList;

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPackageGroup:Ljava/util/HashMap;

    .line 67
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->makePackageGroup()V

    .line 68
    return-void
.end method

.method private getExceptionalDisablePackageList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 423
    .local p1, "defaultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 424
    .local v8, "exceptList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v0, "content://com.sec.knox.provider2/EnterpriseDeviceManager"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 425
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 426
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 427
    .local v9, "result":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    .line 430
    .local v4, "selectionArgs":[Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const-string v3, "getActiveAdmins"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 431
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 432
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 434
    :cond_0
    const-string v0, "getActiveAdmins"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 435
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 436
    const-string v0, "EmergencyWhiteListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Active admin ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-static {v9}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 453
    :goto_0
    if-eqz v6, :cond_2

    .line 454
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 458
    :cond_2
    :goto_1
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getPersistentsApplication(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 459
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getNotDisablePackage()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 461
    return-object v8

    .line 442
    :cond_3
    if-nez v6, :cond_4

    .line 443
    :try_start_1
    const-string v0, "EmergencyWhiteListManager"

    const-string v2, "Active admin is null"

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 449
    :catch_0
    move-exception v7

    .line 450
    .local v7, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v0, "EmergencyWhiteListManager"

    const-string v2, "getExceptionalDisablePackageList exception!"

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 453
    if-eqz v6, :cond_2

    .line 454
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 445
    .end local v7    # "ex":Ljava/lang/Exception;
    :cond_4
    :try_start_3
    const-string v0, "EmergencyWhiteListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Active admin count is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 453
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 454
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private getListFromResource(I)Ljava/util/ArrayList;
    .locals 4
    .param p1, "allowFlag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 333
    .local v0, "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 334
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->makeDefaultAllowedAppList()Ljava/util/ArrayList;

    move-result-object v0

    .line 341
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->includeUnusualPackages()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 343
    return-object v0

    .line 335
    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 336
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f050001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .restart local v0    # "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_0

    .line 338
    :cond_1
    const-string v1, "EmergencyWhiteListManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "not Supported flag : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getPersistentsApplication(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 465
    .local p1, "defaultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 467
    .local v6, "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v8

    const/16 v9, 0x400

    invoke-interface {v8, v9}, Landroid/content/pm/IPackageManager;->getPersistentApplications(I)Ljava/util/List;

    move-result-object v4

    .line 468
    .local v4, "persistentApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    if-eqz v4, :cond_2

    .line 469
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    .line 470
    .local v0, "N":I
    const-string v8, "EmergencyWhiteListManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getPersistentsApplication : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 471
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 472
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ApplicationInfo;

    .line 473
    .local v3, "info":Landroid/content/pm/ApplicationInfo;
    if-eqz v3, :cond_0

    .line 474
    iget-object v5, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 475
    .local v5, "pkgName":Ljava/lang/String;
    iget v8, v3, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v8, v8, 0x9

    const/16 v9, 0x9

    if-ne v8, v9, :cond_1

    const/4 v7, 0x1

    .line 477
    .local v7, "systemPkg":Z
    :goto_1
    if-eqz v7, :cond_0

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 478
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 479
    const-string v8, "EmergencyWhiteListManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "add persistents Application : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 471
    .end local v5    # "pkgName":Ljava/lang/String;
    .end local v7    # "systemPkg":Z
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 475
    .restart local v5    # "pkgName":Ljava/lang/String;
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    .line 484
    .end local v0    # "N":I
    .end local v2    # "i":I
    .end local v3    # "info":Landroid/content/pm/ApplicationInfo;
    .end local v4    # "persistentApps":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v5    # "pkgName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 485
    .local v1, "e":Ljava/lang/Exception;
    const-string v8, "EmergencyWhiteListManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getPersistentsApplication e : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 488
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    return-object v6
.end method

.method private includeUnusualPackages()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    .line 347
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 349
    .local v4, "retList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mUserAllowedAppList:Ljava/util/ArrayList;

    const-string v6, "CHATON"

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getPackageGroupAsIndex(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 351
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 352
    .local v3, "pm":Landroid/content/pm/PackageManager;
    const-string v5, "CHATON"

    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getPackageGroupAsIndex(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    .line 353
    const-string v5, "EmergencyWhiteListManager"

    const-string v6, "chatOn V App is installed"

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v5, "CHATON"

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getPackageGroup(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 355
    .local v2, "pkg":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 357
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "pkg":Ljava/lang/String;
    .end local v3    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "EmergencyWhiteListManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "chatOn V App is not installed "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/16 v5, 0x440

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->checkModeType(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 363
    const-string v5, "FMM"

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getPackageGroup(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 364
    .restart local v2    # "pkg":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 365
    const-string v5, "EmergencyWhiteListManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "use FMM. add "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 371
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "pkg":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mUserAllowedAppList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 372
    .restart local v2    # "pkg":Ljava/lang/String;
    sget v5, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->PERMISSION_USE_ALARM:I

    invoke-virtual {p0, v2, v5}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->checkWhiteListPermission(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 373
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 374
    const-string v5, "EmergencyWhiteListManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " is use alarm permission."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 380
    .end local v2    # "pkg":Ljava/lang/String;
    :cond_4
    return-object v4
.end method

.method private makeAllowedApplicationList(I)V
    .locals 3
    .param p1, "flag"    # I

    .prologue
    .line 259
    const-class v1, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;

    monitor-enter v1

    .line 260
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    .line 261
    :try_start_0
    const-string v0, "EmergencyWhiteListManager"

    const-string v2, "make User Allow List"

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getWhiteListFromDb()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mUserAllowedAppList:Ljava/util/ArrayList;

    .line 263
    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    .line 264
    const-string v0, "EmergencyWhiteListManager"

    const-string v2, "make Default Allow List"

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getListFromResource(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mDefaultAllowedAppList:Ljava/util/ArrayList;

    .line 266
    :cond_1
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_2

    .line 267
    const-string v0, "EmergencyWhiteListManager"

    const-string v2, "make Alarm Allow List"

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getListFromResource(I)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAlarmAllowedAppList:Ljava/util/ArrayList;

    .line 270
    :cond_2
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAllAllowedAppList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 271
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAllAllowedAppList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mDefaultAllowedAppList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 272
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAllAllowedAppList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mUserAllowedAppList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 273
    monitor-exit v1

    .line 274
    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private makeDefaultAllowedAppList()Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 384
    new-instance v7, Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const/high16 v9, 0x7f050000

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 386
    .local v7, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    .line 387
    .local v1, "cscFeature":Lcom/sec/android/app/CscFeature;
    const-string v8, "CscFeature_Common_ConfigEmergencyModePackages"

    invoke-virtual {v1, v8}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 389
    .local v4, "operatorList":Ljava/lang/String;
    if-eqz v4, :cond_2

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 390
    const-string v8, ","

    invoke-virtual {v4, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 392
    .local v6, "pkgList":[Ljava/lang/String;
    move-object v0, v6

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v5, v0, v2

    .line 393
    .local v5, "pkg":Ljava/lang/String;
    invoke-virtual {v5, v10}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x2d

    if-ne v8, v9, :cond_0

    .line 394
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v5, v11, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 392
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 395
    :cond_0
    invoke-virtual {v5, v10}, Ljava/lang/String;->charAt(I)C

    move-result v8

    const/16 v9, 0x2b

    if-ne v8, v9, :cond_1

    .line 396
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v5, v11, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 398
    :cond_1
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 403
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "pkg":Ljava/lang/String;
    .end local v6    # "pkgList":[Ljava/lang/String;
    :cond_2
    invoke-direct {p0, v7}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getExceptionalDisablePackageList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 405
    return-object v7
.end method

.method private makePackageGroup()V
    .locals 14

    .prologue
    .line 498
    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f050002

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 500
    .local v3, "groups":[Ljava/lang/String;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v5, v4

    .end local v4    # "i$":I
    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v2, v0, v5

    .line 501
    .local v2, "group":Ljava/lang/String;
    const-string v11, ":"

    invoke-virtual {v2, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 502
    .local v1, "body":[Ljava/lang/String;
    const/4 v11, 0x0

    aget-object v10, v1, v11

    .line 503
    .local v10, "title":Ljava/lang/String;
    const/4 v11, 0x1

    aget-object v11, v1, v11

    const-string v12, ","

    invoke-virtual {v11, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 504
    .local v9, "pkgs":[Ljava/lang/String;
    new-instance v8, Ljava/util/ArrayList;

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    invoke-direct {v8, v11}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 505
    .local v8, "pkgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPackageGroup:Ljava/util/HashMap;

    invoke-virtual {v11, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .end local v5    # "i$":I
    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 507
    .local v7, "pkg":Ljava/lang/String;
    const-string v11, "EmergencyWhiteListManager"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "makePackageGroup : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " / "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 500
    .end local v7    # "pkg":Ljava/lang/String;
    :cond_0
    add-int/lit8 v4, v5, 0x1

    .local v4, "i$":I
    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto :goto_0

    .line 510
    .end local v1    # "body":[Ljava/lang/String;
    .end local v2    # "group":Ljava/lang/String;
    .end local v8    # "pkgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v9    # "pkgs":[Ljava/lang/String;
    .end local v10    # "title":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private makePermissionList()V
    .locals 12

    .prologue
    .line 217
    const/4 v6, 0x0

    .line 219
    .local v6, "cursor":Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getEnterType()I

    move-result v8

    .line 221
    .local v8, "enterMode":I
    sparse-switch v8, :sswitch_data_0

    .line 233
    const-string v0, "EmergencyWhiteListManager"

    const-string v4, "makePermissionList : This is not EM / UPSM mode. Do nothing"

    invoke-static {v0, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 224
    :sswitch_0
    const/4 v9, 0x0

    .line 238
    .local v9, "mode":I
    :goto_1
    const/4 v0, 0x2

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "package"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "permission"

    aput-object v4, v2, v0

    .line 239
    .local v2, "projection":[Ljava/lang/String;
    new-instance v3, Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "(mode=\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\')"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    .line 240
    .local v3, "selection":Ljava/lang/String;
    const-string v0, "content://com.sec.android.emergencymode/launcheradd"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 241
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 242
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 243
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 245
    :cond_1
    const-string v0, "package"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 246
    .local v11, "pkg":Ljava/lang/String;
    const-string v0, "permission"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 247
    .local v10, "permission":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPermissionList:Ljava/util/HashMap;

    invoke-virtual {v0, v11, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 253
    .end local v10    # "permission":Ljava/lang/String;
    .end local v11    # "pkg":Ljava/lang/String;
    :cond_2
    if-eqz v6, :cond_0

    .line 254
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 229
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v9    # "mode":I
    :sswitch_1
    const/4 v9, 0x1

    .line 230
    .restart local v9    # "mode":I
    goto :goto_1

    .line 250
    :catch_0
    move-exception v7

    .line 251
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "EmergencyWhiteListManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "makePermissionList e : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 253
    if-eqz v6, :cond_0

    .line 254
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 253
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 254
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 221
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x40 -> :sswitch_0
        0x200 -> :sswitch_1
        0x400 -> :sswitch_1
    .end sparse-switch
.end method

.method private printWhiteList(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 492
    .local p1, "req":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 493
    .local v1, "pkg":Ljava/lang/String;
    const-string v2, "EmergencyWhiteListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Print List : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 495
    .end local v1    # "pkg":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method protected checkInvalidBroadcast(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getBootState()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 117
    :goto_0
    return v0

    .line 116
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->checkInvalidProcess(Ljava/lang/String;)Z

    move-result v0

    .line 117
    .local v0, "isInvalid":Z
    goto :goto_0
.end method

.method protected checkInvalidProcess(Ljava/lang/String;)Z
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 101
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getBootState()Z

    move-result v3

    if-nez v3, :cond_1

    move v1, v2

    .line 111
    :cond_0
    :goto_0
    return v1

    .line 102
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->isEmergencyMode()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 103
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->isScreenOn()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-boolean v3, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->isforceBlockUserPkg:Z

    if-nez v3, :cond_2

    const/4 v0, 0x7

    .line 104
    .local v0, "allowFlag":I
    :goto_1
    const/4 v3, 0x0

    invoke-virtual {p0, p1, v3, v0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->checkValidPackage(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 105
    sget-boolean v2, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->isforceBlockUserPkg:Z

    if-eqz v2, :cond_0

    .line 106
    const-string v2, "EmergencyWhiteListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isforceBlockUserPkg 3rd party intent : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .end local v0    # "allowFlag":I
    :cond_2
    move v0, v1

    .line 103
    goto :goto_1

    :cond_3
    move v1, v2

    .line 111
    goto :goto_0
.end method

.method protected checkValidIntentAction(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "actName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 91
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getBootState()Z

    move-result v4

    if-nez v4, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v3

    .line 92
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getActionList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 93
    .local v0, "actionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 94
    .local v2, "s":Ljava/lang/String;
    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 95
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected checkValidPackage(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "actName"    # Ljava/lang/String;
    .param p3, "allowFlag"    # I

    .prologue
    const/4 v3, 0x1

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getBootState()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "com.sec.android.provider.emergencymode"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v3

    .line 73
    :cond_1
    invoke-virtual {p0, p3}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getPackageList(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 75
    .local v1, "appList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 78
    if-eqz p2, :cond_2

    const/4 v4, 0x4

    if-ne p3, v4, :cond_2

    const-string v4, "android"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 79
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->checkValidIntentAction(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    goto :goto_0

    .line 81
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82
    .local v0, "app":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_0

    .line 87
    .end local v0    # "app":Ljava/lang/String;
    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected checkWhiteListPermission(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "perm"    # I

    .prologue
    const/4 v2, 0x0

    .line 121
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPermissionList:Ljava/util/HashMap;

    if-eqz v3, :cond_2

    .line 122
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPermissionList:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 123
    .local v1, "permissionStr":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 124
    const/4 v3, 0x2

    invoke-static {v1, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    .line 125
    .local v0, "permission":I
    and-int v3, v0, p2

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 133
    .end local v0    # "permission":I
    .end local v1    # "permissionStr":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 128
    .restart local v1    # "permissionStr":Ljava/lang/String;
    :cond_1
    const-string v3, "EmergencyWhiteListManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " don\'t has permission."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 132
    .end local v1    # "permissionStr":Ljava/lang/String;
    :cond_2
    const-string v3, "EmergencyWhiteListManager"

    const-string v4, "permission list is null. return false."

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected deleteWhiteList(Ljava/lang/String;)Z
    .locals 8
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 553
    const/16 v5, 0x600

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->checkModeType(I)Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    .line 554
    .local v0, "allowFlag":I
    :goto_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pkg=\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' AND "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "allowflag"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 555
    .local v2, "selection":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_WHITELIST:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v2, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 556
    .local v1, "result":I
    if-lez v1, :cond_1

    .line 559
    :goto_1
    return v3

    .end local v0    # "allowFlag":I
    .end local v1    # "result":I
    .end local v2    # "selection":Ljava/lang/String;
    :cond_0
    move v0, v4

    .line 553
    goto :goto_0

    .restart local v0    # "allowFlag":I
    .restart local v1    # "result":I
    .restart local v2    # "selection":Ljava/lang/String;
    :cond_1
    move v3, v4

    .line 559
    goto :goto_1
.end method

.method protected getActionList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "pkg"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 316
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mActionList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 317
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAlarmAllowedAppList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 318
    .local v0, "alarms":Ljava/lang/String;
    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 319
    .local v3, "splitAlarm":[Ljava/lang/String;
    aget-object v4, v3, v6

    if-eqz v4, :cond_0

    aget-object v4, v3, v6

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 320
    array-length v4, v3

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    .line 321
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 322
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mActionList:Ljava/util/ArrayList;

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 327
    .end local v0    # "alarms":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v3    # "splitAlarm":[Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mActionList:Ljava/util/ArrayList;

    return-object v4
.end method

.method protected getForceEnableApplicationList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mForceEnableList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getNotDisablePackage()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 409
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 411
    .local v5, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPermissionList:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPermissionList:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 412
    .local v2, "keyList":[Ljava/lang/String;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/String;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v0, v1

    .line 413
    .local v4, "pkg":Ljava/lang/String;
    sget v6, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->PERMISSION_NOT_DISABLE:I

    invoke-virtual {p0, v4, v6}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->checkWhiteListPermission(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 414
    const-string v6, "EmergencyWhiteListManager"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is not disable pkg."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 412
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 419
    .end local v4    # "pkg":Ljava/lang/String;
    :cond_1
    return-object v5
.end method

.method protected getPackageGroup(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPackageGroup:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getPackageGroupAsIndex(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 517
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getPackageGroup(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 518
    .local v0, "group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 519
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 521
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected getPackageGroupAsName(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 8
    .param p1, "pkgName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 526
    iget-object v6, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPackageGroup:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPackageGroup:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->size()I

    move-result v7

    new-array v7, v7, [Ljava/lang/String;

    invoke-interface {v6, v7}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    .line 527
    .local v3, "keyList":[Ljava/lang/String;
    move-object v0, v3

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v2, v0, v1

    .line 528
    .local v2, "key":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mPackageGroup:Ljava/util/HashMap;

    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 529
    .local v5, "pkgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 533
    .end local v2    # "key":Ljava/lang/String;
    .end local v5    # "pkgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_1
    return-object v5

    .line 527
    .restart local v2    # "key":Ljava/lang/String;
    .restart local v5    # "pkgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 533
    .end local v2    # "key":Ljava/lang/String;
    .end local v5    # "pkgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public getPackageList(I)Ljava/util/ArrayList;
    .locals 3
    .param p1, "allowFlag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    const/4 v0, 0x7

    if-ne p1, v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAllAllowedAppList:Ljava/util/ArrayList;

    .line 287
    :goto_0
    return-object v0

    .line 279
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mDefaultAllowedAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 281
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 282
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mUserAllowedAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 283
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    .line 284
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAlarmAllowedAppList:Ljava/util/ArrayList;

    goto :goto_0

    .line 286
    :cond_3
    const-string v0, "EmergencyWhiteListManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not support flag, return all list."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getWhiteListFromDb()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 291
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v9, "temp":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 294
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v4, 0x1

    :try_start_0
    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "pkg"

    aput-object v5, v2, v4

    .line 295
    .local v2, "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    .line 296
    .local v3, "selection":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_WHITELIST:Landroid/net/Uri;

    .line 297
    .local v1, "uri":Landroid/net/Uri;
    const/16 v4, 0x600

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->checkModeType(I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 298
    .local v6, "allowFlag":I
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "allowflag="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 299
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 300
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 301
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 303
    :cond_0
    const-string v0, "pkg"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 309
    :cond_1
    if-eqz v7, :cond_2

    .line 310
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 312
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v6    # "allowFlag":I
    :cond_2
    :goto_1
    return-object v9

    .restart local v1    # "uri":Landroid/net/Uri;
    .restart local v2    # "projection":[Ljava/lang/String;
    .restart local v3    # "selection":Ljava/lang/String;
    :cond_3
    move v6, v0

    .line 297
    goto :goto_0

    .line 306
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    :catch_0
    move-exception v8

    .line 307
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "EmergencyWhiteListManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getListFromDb Exception "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 309
    if-eqz v7, :cond_2

    .line 310
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 309
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 310
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method protected insert(Landroid/net/Uri;Landroid/content/ContentValues;)Z
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 537
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 538
    .local v1, "resultUri":Landroid/net/Uri;
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    long-to-int v0, v2

    .line 539
    .local v0, "result":I
    if-lez v0, :cond_0

    .line 540
    const/4 v2, 0x1

    .line 542
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method protected insertWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 546
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 547
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "pkg"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 548
    const-string v2, "allowflag"

    const/16 v1, 0x600

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->checkModeType(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 549
    sget-object v1, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_WHITELIST:Landroid/net/Uri;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Z

    move-result v1

    return v1

    .line 548
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected isAppWidget(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAppWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected makeForceEnableApplicationList()V
    .locals 8

    .prologue
    .line 173
    const/4 v4, 0x0

    .line 174
    .local v4, "isUPSM":Z
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getEnterType()I

    move-result v1

    .line 176
    .local v1, "enterMode":I
    sparse-switch v1, :sswitch_data_0

    .line 188
    const-string v5, "EmergencyWhiteListManager"

    const-string v6, "makeForceEnableApplicationList : This is not EM / UPSM mode. Do nothing"

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_0
    return-void

    .line 179
    :sswitch_0
    const/4 v4, 0x0

    .line 193
    :goto_0
    if-eqz v4, :cond_2

    .line 195
    new-instance v5, Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050004

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mForceEnableList:Ljava/util/ArrayList;

    .line 202
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->getNotDisablePackage()Ljava/util/ArrayList;

    move-result-object v2

    .line 203
    .local v2, "getListFromPermission":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_1

    .line 204
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mForceEnableList:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 207
    :cond_1
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mForceEnableList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 208
    .local v0, "appName":Ljava/lang/String;
    const-string v5, "EmergencyWhiteListManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "makeForceEnableApplicationList ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 184
    .end local v0    # "appName":Ljava/lang/String;
    .end local v2    # "getListFromPermission":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :sswitch_1
    const/4 v4, 0x1

    .line 185
    goto :goto_0

    .line 198
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f050003

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mForceEnableList:Ljava/util/ArrayList;

    goto :goto_1

    .line 176
    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x40 -> :sswitch_0
        0x200 -> :sswitch_1
        0x400 -> :sswitch_1
    .end sparse-switch
.end method

.method protected resetAllowedApplicationList()V
    .locals 3

    .prologue
    .line 163
    const-class v1, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;

    monitor-enter v1

    .line 164
    :try_start_0
    const-string v0, "EmergencyWhiteListManager"

    const-string v2, "resetAllowedApplicationList"

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->makePermissionList()V

    .line 166
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->makeAllowedApplicationList(I)V

    .line 167
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->makeForceEnableApplicationList()V

    .line 168
    monitor-exit v1

    .line 169
    return-void

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected setAllWidgetPackage()V
    .locals 6

    .prologue
    .line 139
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAppWidgetList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 140
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v2

    .line 143
    .local v2, "widgets":Ljava/util/List;, "Ljava/util/List<Landroid/appwidget/AppWidgetProviderInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/appwidget/AppWidgetProviderInfo;

    .line 144
    .local v1, "widget":Landroid/appwidget/AppWidgetProviderInfo;
    iget v3, v1, Landroid/appwidget/AppWidgetProviderInfo;->widgetCategory:I

    and-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_0

    .line 145
    const-string v3, "EmergencyWhiteListManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AppWidget ignored: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 148
    :cond_0
    iget v3, v1, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    if-lez v3, :cond_1

    iget v3, v1, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    if-gtz v3, :cond_2

    .line 149
    :cond_1
    const-string v3, "EmergencyWhiteListManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AppWidget specifies an invalid size: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 153
    :cond_2
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;->mAppWidgetList:Ljava/util/ArrayList;

    iget-object v4, v1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 155
    .end local v1    # "widget":Landroid/appwidget/AppWidgetProviderInfo;
    :cond_3
    return-void
.end method
