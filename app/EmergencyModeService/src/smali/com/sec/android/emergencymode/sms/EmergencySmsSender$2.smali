.class Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;
.super Ljava/lang/Object;
.source "EmergencySmsSender.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/sms/EmergencySmsSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 144
    const-string v1, "EmergencySmsSender"

    const-string v2, "callback onServiceConnected Called !!!!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    invoke-static {p2}, Lcom/android/mms/transaction/ISnsRemoteService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/mms/transaction/ISnsRemoteService;

    move-result-object v2

    # setter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;
    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$302(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Lcom/android/mms/transaction/ISnsRemoteService;)Lcom/android/mms/transaction/ISnsRemoteService;

    .line 146
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackConnected:Z
    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$402(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Z)Z

    .line 148
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    # getter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;
    invoke-static {v1}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$300(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/ISnsRemoteService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    # getter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSnsCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;
    invoke-static {v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$500(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/mms/transaction/ISnsRemoteService;->registerCallback(Lcom/android/mms/transaction/ISnsRemoteServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 152
    :goto_0
    return-void

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 156
    const-string v1, "EmergencySmsSender"

    const-string v2, "callback onServiceDisconnected Called !!!!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    # getter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;
    invoke-static {v1}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$300(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/ISnsRemoteService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    # getter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSnsCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;
    invoke-static {v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$500(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/mms/transaction/ISnsRemoteService;->unregisterCallback(Lcom/android/mms/transaction/ISnsRemoteServiceCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;
    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$302(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Lcom/android/mms/transaction/ISnsRemoteService;)Lcom/android/mms/transaction/ISnsRemoteService;

    .line 163
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackConnected:Z
    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$402(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Z)Z

    .line 164
    return-void

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
