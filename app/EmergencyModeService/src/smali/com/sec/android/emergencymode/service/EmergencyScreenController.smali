.class public Lcom/sec/android/emergencymode/service/EmergencyScreenController;
.super Ljava/lang/Object;
.source "EmergencyScreenController.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EmergencyScreenController"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyScreenController;->mContext:Landroid/content/Context;

    .line 38
    return-void
.end method


# virtual methods
.method protected setEmergencyScreen(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyScreenController;->setGrayScreen(Z)V

    .line 70
    return-void
.end method

.method protected setGrayScreen(Z)V
    .locals 8
    .param p1, "enabled"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 46
    const-string v4, "accessibility"

    invoke-static {v4}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Landroid/view/accessibility/IAccessibilityManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/accessibility/IAccessibilityManager;

    move-result-object v1

    .line 48
    .local v1, "iAccessibilityManager":Landroid/view/accessibility/IAccessibilityManager;
    if-nez p1, :cond_0

    .line 49
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyScreenController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "powersaving_switch"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v2, :cond_0

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyScreenController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "blackgrey_powersaving_mode"

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v2, :cond_0

    .line 51
    const-string v4, "EmergencyScreenController"

    const-string v5, "Power saving mode gray on!"

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    const/4 p1, 0x1

    .line 55
    :cond_0
    sget-object v4, Lcom/sec/android/emergencymode/EmergencyConstants;->RGBCMYArray:[I

    invoke-interface {v1, p1, v4}, Landroid/view/accessibility/IAccessibilityManager;->setmDNIeColorBlind(Z[I)Z

    .line 56
    if-nez p1, :cond_1

    .line 57
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyScreenController;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "high_contrast"

    const/4 v6, 0x0

    const/4 v7, -0x2

    invoke-static {v4, v5, v6, v7}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v4

    if-ne v4, v2, :cond_2

    .line 59
    .local v2, "mNegativeColorCheck":Z
    :goto_0
    if-eqz v2, :cond_1

    .line 60
    const/4 v3, 0x1

    invoke-interface {v1, v3}, Landroid/view/accessibility/IAccessibilityManager;->setmDNIeNegative(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    .end local v2    # "mNegativeColorCheck":Z
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v2, v3

    .line 57
    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "EmergencyScreenController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t not make gray screen "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
