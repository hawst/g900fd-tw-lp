.class public Lcom/sec/android/emergencymode/util/WakeLockUtil;
.super Ljava/lang/Object;
.source "WakeLockUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "WakeLockUtil"

.field private static instance:Lcom/sec/android/emergencymode/util/WakeLockUtil;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mPM:Landroid/os/PowerManager;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mContext:Landroid/content/Context;

    .line 37
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mPM:Landroid/os/PowerManager;

    .line 38
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/util/WakeLockUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    sget-object v0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->instance:Lcom/sec/android/emergencymode/util/WakeLockUtil;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/sec/android/emergencymode/util/WakeLockUtil;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/util/WakeLockUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->instance:Lcom/sec/android/emergencymode/util/WakeLockUtil;

    .line 44
    :cond_0
    sget-object v0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->instance:Lcom/sec/android/emergencymode/util/WakeLockUtil;

    return-object v0
.end method


# virtual methods
.method public acquireWakeLock()V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mPM:Landroid/os/PowerManager;

    const v1, 0x3000000a

    const-string v2, "EmergencyMode"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 53
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 56
    const-string v0, "WakeLockUtil"

    const-string v1, "acquireWakeLock"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public releaseWakeLock()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/emergencymode/util/WakeLockUtil;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 63
    const-string v0, "WakeLockUtil"

    const-string v1, "releaseWakeLock"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_0
    return-void
.end method
