.class public Lcom/sec/android/emergencymode/service/EmergencyBase;
.super Ljava/lang/Object;
.source "EmergencyBase.java"


# static fields
.field protected static final BOOT_TIME:Ljava/lang/String; = "BootTime"

.field public static final BOOT_TIME_CLEARED:J = -0x1L

.field protected static final EMF_SHAREPREF:Ljava/lang/String; = "EM_SharedPref"

.field public static final EM_PREFERENCE:Ljava/lang/String; = "em_preference"

.field private static final ENTER_TYPE:Ljava/lang/String; = "ENTER_TYPE"

.field public static final ENTER_TYPE_MASK:I = 0xff0

.field public static final FCSTATE:Ljava/lang/String; = "fcstate"

.field public static final HUMAN_TYPE_MASK:I = 0xf

.field public static final PERMISSION_HAS_DEFAULT_LEVEL:I

.field public static final PERMISSION_NOT_DISABLE:I

.field public static final PERMISSION_USE_ALARM:I

.field public static final PERMISSION_USE_MOBILE_DATA:I

.field protected static final READY_TO_START:Ljava/lang/String; = "ReadyToStart"

.field protected static STATE_NONE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "EmergencyBase"

.field protected static enterType:I

.field protected static isBootStandBy:Z

.field protected static isForceTrigger:Z

.field protected static isModifying:Z

.field protected static isforceBlockUserPkg:Z

.field protected static mEMState:Z


# instance fields
.field protected final isDocomoFeature:Z

.field protected mContext:Landroid/content/Context;

.field protected mSP:Landroid/content/SharedPreferences;

.field protected final usePendingThread:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 48
    const-string v0, "1000"

    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->PERMISSION_NOT_DISABLE:I

    .line 49
    const-string v0, "0100"

    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->PERMISSION_USE_ALARM:I

    .line 50
    const-string v0, "0010"

    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->PERMISSION_USE_MOBILE_DATA:I

    .line 51
    const-string v0, "0001"

    invoke-static {v0, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->PERMISSION_HAS_DEFAULT_LEVEL:I

    .line 57
    sput-boolean v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->isforceBlockUserPkg:Z

    .line 58
    sput-boolean v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->isForceTrigger:Z

    .line 59
    sput-boolean v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->mEMState:Z

    .line 60
    sput-boolean v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->isModifying:Z

    .line 61
    sput-boolean v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->isBootStandBy:Z

    .line 62
    sput v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->enterType:I

    .line 69
    const/16 v0, -0x3e7

    sput v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->STATE_NONE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    .line 77
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    const-string v3, "EM_SharedPref"

    invoke-virtual {v2, v3, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    .line 78
    const-string v2, "ro.build.scafe"

    const-string v3, "unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "scafeValue":Ljava/lang/String;
    const-string v2, "EmergencyBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "scafe ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v2, "capuccino"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 81
    iput-boolean v6, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->usePendingThread:Z

    .line 86
    :goto_0
    const-string v2, "ro.csc.sales_code"

    const-string v3, "unknown"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "salesCode":Ljava/lang/String;
    const-string v2, "DCM"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 88
    iput-boolean v6, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->isDocomoFeature:Z

    .line 92
    :goto_1
    return-void

    .line 83
    .end local v0    # "salesCode":Ljava/lang/String;
    :cond_0
    iput-boolean v5, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->usePendingThread:Z

    goto :goto_0

    .line 90
    .restart local v0    # "salesCode":Ljava/lang/String;
    :cond_1
    iput-boolean v5, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->isDocomoFeature:Z

    goto :goto_1
.end method

.method private checkDeafCondtion()Z
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 210
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    .line 211
    .local v3, "resolver":Landroid/content/ContentResolver;
    const-string v7, "flash_notification"

    invoke-static {v3, v7, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_0

    move v1, v5

    .line 212
    .local v1, "flashNotification":Z
    :goto_0
    const-string v7, "all_sound_off"

    invoke-static {v3, v7, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_1

    move v4, v5

    .line 213
    .local v4, "turnOffAllSounds":Z
    :goto_1
    const-string v7, "mono_audio_db"

    invoke-static {v3, v7, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_2

    move v2, v5

    .line 214
    .local v2, "monoAudio":Z
    :goto_2
    const-string v7, "def_tactileassist_enable"

    invoke-static {v3, v7, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v7

    if-eqz v7, :cond_3

    move v0, v5

    .line 216
    .local v0, "autoHaptic":Z
    :goto_3
    return v4

    .end local v0    # "autoHaptic":Z
    .end local v1    # "flashNotification":Z
    .end local v2    # "monoAudio":Z
    .end local v4    # "turnOffAllSounds":Z
    :cond_0
    move v1, v6

    .line 211
    goto :goto_0

    .restart local v1    # "flashNotification":Z
    :cond_1
    move v4, v6

    .line 212
    goto :goto_1

    .restart local v4    # "turnOffAllSounds":Z
    :cond_2
    move v2, v6

    .line 213
    goto :goto_2

    .restart local v2    # "monoAudio":Z
    :cond_3
    move v0, v6

    .line 214
    goto :goto_3
.end method


# virtual methods
.method public checkModeType(I)Z
    .locals 5
    .param p1, "type"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 221
    and-int/lit16 v4, p1, 0xff0

    if-eqz v4, :cond_1

    move v0, v2

    .line 222
    .local v0, "regEnterType":Z
    :goto_0
    and-int/lit8 v4, p1, 0xf

    if-eqz v4, :cond_2

    move v1, v2

    .line 224
    .local v1, "regHumanType":Z
    :goto_1
    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->getModeType()I

    move-result v4

    and-int/2addr v4, p1

    if-eqz v4, :cond_3

    .line 233
    :cond_0
    :goto_2
    return v2

    .end local v0    # "regEnterType":Z
    .end local v1    # "regHumanType":Z
    :cond_1
    move v0, v3

    .line 221
    goto :goto_0

    .restart local v0    # "regEnterType":Z
    :cond_2
    move v1, v3

    .line 222
    goto :goto_1

    .restart local v1    # "regHumanType":Z
    :cond_3
    move v2, v3

    .line 225
    goto :goto_2

    .line 226
    :cond_4
    if-eqz v0, :cond_5

    .line 227
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->getEnterType()I

    move-result v4

    and-int/2addr v4, p1

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_2

    .line 228
    :cond_5
    if-eqz v1, :cond_6

    .line 229
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->getHumanType()I

    move-result v4

    and-int/2addr v4, p1

    if-nez v4, :cond_0

    move v2, v3

    goto :goto_2

    .line 232
    :cond_6
    const-string v2, "EmergencyBase"

    const-string v4, "noHasType."

    invoke-static {v2, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 233
    goto :goto_2
.end method

.method public clearBootTime()V
    .locals 6

    .prologue
    .line 252
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    .line 253
    const-class v2, Lcom/sec/android/emergencymode/service/EmergencyBase;

    monitor-enter v2

    .line 254
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 255
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v1, "BootTime"

    const-wide/16 v4, -0x1

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 256
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 257
    const-string v1, "EmergencyBase"

    const-string v3, "clearBootTime"

    invoke-static {v1, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    monitor-exit v2

    .line 262
    .end local v0    # "ed":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 258
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 260
    :cond_0
    const-string v1, "EmergencyBase"

    const-string v2, "clearBootTime mSP is null!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getBootState()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 130
    sget-boolean v2, Lcom/sec/android/emergencymode/service/EmergencyBase;->isBootStandBy:Z

    if-nez v2, :cond_0

    .line 131
    const-string v2, "sys.boot_completed"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_1

    :goto_0
    sput-boolean v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->isBootStandBy:Z

    .line 133
    :cond_0
    sget-boolean v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->isBootStandBy:Z

    return v0

    :cond_1
    move v0, v1

    .line 131
    goto :goto_0
.end method

.method public getBootTime()J
    .locals 8

    .prologue
    .line 279
    const-wide/16 v0, -0x1

    .line 280
    .local v0, "val":J
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_0

    .line 281
    const-class v3, Lcom/sec/android/emergencymode/service/EmergencyBase;

    monitor-enter v3

    .line 282
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    const-string v4, "BootTime"

    const-wide/16 v6, -0x1

    invoke-interface {v2, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 283
    monitor-exit v3

    .line 287
    :goto_0
    return-wide v0

    .line 283
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 285
    :cond_0
    const-string v2, "EmergencyBase"

    const-string v3, "getBootTime mSP is null!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getEmergencyFCState()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 123
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    const-string v3, "em_preference"

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 124
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v2, "fcstate"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 125
    .local v0, "fcstate":Z
    const-string v2, "EmergencyBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FCState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    return v0
.end method

.method public getEnterType()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 145
    sget v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->enterType:I

    if-nez v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    const-string v2, "em_preference"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 147
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v1, "ENTER_TYPE"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->enterType:I

    .line 150
    .end local v0    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    sget v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->enterType:I

    return v1
.end method

.method protected getEnterTypeString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 154
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->getEnterType()I

    move-result v0

    .line 155
    .local v0, "ent":I
    sparse-switch v0, :sswitch_data_0

    .line 171
    const-string v1, ""

    :goto_0
    return-object v1

    .line 157
    :sswitch_0
    const-string v1, "MANUAL"

    goto :goto_0

    .line 159
    :sswitch_1
    const-string v1, "SVOICE"

    goto :goto_0

    .line 161
    :sswitch_2
    const-string v1, "FMM"

    goto :goto_0

    .line 163
    :sswitch_3
    const-string v1, "DETECT"

    goto :goto_0

    .line 165
    :sswitch_4
    const-string v1, "DISATER"

    goto :goto_0

    .line 167
    :sswitch_5
    const-string v1, "UPSM"

    goto :goto_0

    .line 169
    :sswitch_6
    const-string v1, "UPSM_FMM"

    goto :goto_0

    .line 155
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_1
        0x40 -> :sswitch_2
        0x80 -> :sswitch_3
        0x100 -> :sswitch_4
        0x200 -> :sswitch_5
        0x400 -> :sswitch_6
    .end sparse-switch
.end method

.method public getHumanType()I
    .locals 2

    .prologue
    .line 197
    const/4 v0, 0x0

    .line 198
    .local v0, "humanType":I
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 199
    or-int/lit8 v0, v0, 0x2

    .line 206
    :goto_0
    return v0

    .line 200
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->checkDeafCondtion()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 201
    or-int/lit8 v0, v0, 0x4

    goto :goto_0

    .line 203
    :cond_1
    or-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getIntPreference(Ljava/lang/String;)I
    .locals 5
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 357
    sget v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->STATE_NONE:I

    .line 358
    .local v0, "val":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 359
    const-string v2, "EmergencyBase"

    const-string v3, "getIntPreference key is null!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v0

    .line 371
    .end local v0    # "val":I
    .local v1, "val":I
    :goto_0
    return v1

    .line 363
    .end local v1    # "val":I
    .restart local v0    # "val":I
    :cond_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_1

    .line 364
    const-class v3, Lcom/sec/android/emergencymode/service/EmergencyBase;

    monitor-enter v3

    .line 365
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    sget v4, Lcom/sec/android/emergencymode/service/EmergencyBase;->STATE_NONE:I

    invoke-interface {v2, p1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 366
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 367
    const-string v2, "EmergencyBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getIntPreference key["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] val["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move v1, v0

    .line 371
    .end local v0    # "val":I
    .restart local v1    # "val":I
    goto :goto_0

    .line 366
    .end local v1    # "val":I
    .restart local v0    # "val":I
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 369
    :cond_1
    const-string v2, "EmergencyBase"

    const-string v3, "getIntPreference mSP is null!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getModeType()I
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->getHumanType()I

    move-result v1

    .line 138
    .local v1, "humanType":I
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->getEnterType()I

    move-result v0

    .line 141
    .local v0, "enterType":I
    or-int v2, v0, v1

    return v2
.end method

.method public getReadyToStart()Z
    .locals 5

    .prologue
    .line 321
    const/4 v0, 0x0

    .line 323
    .local v0, "val":Z
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->usePendingThread:Z

    if-nez v1, :cond_0

    .line 324
    const/4 v0, 0x1

    .line 334
    :goto_0
    const-string v1, "EmergencyBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getReadyToStart ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    return v0

    .line 326
    :cond_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_1

    .line 327
    const-class v2, Lcom/sec/android/emergencymode/service/EmergencyBase;

    monitor-enter v2

    .line 328
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    const-string v3, "ReadyToStart"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 329
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 331
    :cond_1
    const-string v1, "EmergencyBase"

    const-string v2, "getReadyToStart mSP is null!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getUserName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 175
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    const-string v5, "user"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/UserManager;

    .line 176
    .local v3, "mUserManager":Landroid/os/UserManager;
    const-string v2, ""

    .line 178
    .local v2, "currentUserName":Ljava/lang/String;
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    .line 179
    .local v0, "currentUserId":I
    invoke-virtual {v3, v0}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v1

    .line 180
    .local v1, "currentUserInfo":Landroid/content/pm/UserInfo;
    if-eqz v1, :cond_0

    .line 181
    iget-object v2, v1, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    .line 184
    :cond_0
    return-object v2
.end method

.method public isBootFinished()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 291
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->getBootTime()J

    move-result-wide v0

    .line 292
    .local v0, "savedBootTime":J
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-nez v3, :cond_0

    .line 293
    const-string v3, "EmergencyBase"

    const-string v4, "isBootFinished : Boot was not finished!!"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    :goto_0
    return v2

    .line 297
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    cmp-long v3, v4, v0

    if-lez v3, :cond_1

    .line 298
    const-string v2, "EmergencyBase"

    const-string v3, "isBootFinished : Boot was already finished"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const/4 v2, 0x1

    goto :goto_0

    .line 303
    :cond_1
    const-string v3, "EmergencyBase"

    const-string v4, "isBootFinished : Boot time was saved but starting is more fast!!!"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isEmergencyMode()Z
    .locals 1

    .prologue
    .line 99
    sget-boolean v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mEMState:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/emergencymode/service/EmergencyBase;->isModifying:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isEmergencyMode(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "emergency_mode"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected isScreenOn()Z
    .locals 3

    .prologue
    .line 103
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 104
    .local v0, "mPM":Landroid/os/PowerManager;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->getBootState()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public notifyCurrentState(I)V
    .locals 3
    .param p1, "reason"    # I

    .prologue
    .line 237
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 238
    .local v0, "resultIntent":Landroid/content/Intent;
    const-string v1, "reason"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 239
    const-string v1, "enterType"

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->getEnterType()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 240
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 241
    return-void
.end method

.method public setBootTime()V
    .locals 6

    .prologue
    .line 265
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    .line 266
    const-class v4, Lcom/sec/android/emergencymode/service/EmergencyBase;

    monitor-enter v4

    .line 267
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 268
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 269
    .local v2, "time":J
    const-string v1, "BootTime"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 270
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 271
    const-string v1, "EmergencyBase"

    const-string v5, "setBootTime"

    invoke-static {v1, v5}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    monitor-exit v4

    .line 276
    .end local v0    # "ed":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "time":J
    :goto_0
    return-void

    .line 272
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 274
    :cond_0
    const-string v1, "EmergencyBase"

    const-string v4, "setBootTime mSP is null!"

    invoke-static {v1, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setEmergencyExternalState(Z)V
    .locals 5
    .param p1, "enabled"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 108
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "emergency_mode"

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v3, v4, v0}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 109
    const/16 v0, 0x600

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyBase;->checkModeType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "ultra_powersaving_mode"

    if-eqz p1, :cond_2

    :goto_1
    invoke-static {v0, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 112
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 108
    goto :goto_0

    :cond_2
    move v1, v2

    .line 110
    goto :goto_1
.end method

.method protected setEmergencyFCState(Z)V
    .locals 5
    .param p1, "enabled"    # Z

    .prologue
    .line 115
    sput-boolean p1, Lcom/sec/android/emergencymode/service/EmergencyBase;->isModifying:Z

    .line 116
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    const-string v3, "em_preference"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 117
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 118
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "fcstate"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 119
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 120
    return-void
.end method

.method protected setEnterType(I)V
    .locals 6
    .param p1, "flag"    # I

    .prologue
    .line 188
    and-int/lit16 v1, p1, 0xff0

    .line 189
    .local v1, "enterType":I
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mContext:Landroid/content/Context;

    const-string v4, "em_preference"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 190
    .local v2, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 191
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v3, "ENTER_TYPE"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 192
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 193
    sput v1, Lcom/sec/android/emergencymode/service/EmergencyBase;->enterType:I

    .line 194
    return-void
.end method

.method public setIntPreference(Ljava/lang/String;I)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "val"    # I

    .prologue
    .line 339
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 340
    const-string v1, "EmergencyBase"

    const-string v2, "setIntPreference key is null!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    :goto_0
    return-void

    .line 344
    :cond_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_1

    .line 345
    const-class v2, Lcom/sec/android/emergencymode/service/EmergencyBase;

    monitor-enter v2

    .line 346
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 347
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 348
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 349
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 350
    const-string v1, "EmergencyBase"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setIntPreference key["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] val["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 349
    .end local v0    # "ed":Landroid/content/SharedPreferences$Editor;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 352
    :cond_1
    const-string v1, "EmergencyBase"

    const-string v2, "setIntPreference mSP is null!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setProperties(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 245
    iget-boolean v0, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->isDocomoFeature:Z

    if-eqz v0, :cond_0

    .line 246
    const-string v0, "persist.sys.epsmodestate"

    invoke-static {v0, p1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_0
    return-void
.end method

.method public setReadyToStart(Z)V
    .locals 5
    .param p1, "isReady"    # Z

    .prologue
    .line 308
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    if-eqz v1, :cond_0

    .line 309
    const-class v2, Lcom/sec/android/emergencymode/service/EmergencyBase;

    monitor-enter v2

    .line 310
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyBase;->mSP:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 311
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v1, "ReadyToStart"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 312
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 313
    const-string v1, "EmergencyBase"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setReadyToStart ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    monitor-exit v2

    .line 318
    .end local v0    # "ed":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 314
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 316
    :cond_0
    const-string v1, "EmergencyBase"

    const-string v2, "setReadyToStart mSP is null!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
