.class public Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;
.super Ljava/lang/Object;
.source "ClockGadgetDisplayData.java"


# static fields
.field private static sInstance:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;


# instance fields
.field private currentLocale:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    invoke-direct {v0}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;-><init>()V

    sput-object v0, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->sInstance:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->currentLocale:Ljava/util/Locale;

    return-void
.end method

.method public static getInstance()Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->sInstance:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    return-object v0
.end method


# virtual methods
.method public getCurrentLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->currentLocale:Ljava/util/Locale;

    return-object v0
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 0
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->currentLocale:Ljava/util/Locale;

    .line 49
    return-void
.end method
