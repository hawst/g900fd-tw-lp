.class Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1$1;
.super Ljava/lang/Object;
.source "LocationUtility.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1$1;->this$1:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 213
    const-string v1, "LocationUtility"

    const-string v2, "start Location Info"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1$1;->this$1:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;->this$0:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    # getter for: Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->access$000(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencyFactory;

    move-result-object v0

    .line 215
    .local v0, "emFac":Lcom/sec/android/emergencymode/service/EmergencyFactory;
    const/16 v1, 0x440

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkModeType(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1$1;->this$1:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;->this$0:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    const/4 v2, -0x1

    invoke-virtual {v1, v3, v3, v2}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->setLocationState(ZZI)V

    .line 220
    :goto_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1$1;->this$1:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;->this$0:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->registerLocationListener()V

    .line 221
    return-void

    .line 218
    :cond_0
    const-string v1, "LocationUtility"

    const-string v2, "startLocationInfo : mode type is FMM. keep gps state"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
