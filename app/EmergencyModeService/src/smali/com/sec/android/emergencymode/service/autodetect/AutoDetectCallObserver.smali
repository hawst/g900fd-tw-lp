.class public Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;
.super Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;
.source "AutoDetectCallObserver.java"


# static fields
.field private static final REASON:Ljava/lang/String; = "CALL_ACCEPT"

.field private static final TAG:Ljava/lang/String; = "AutoDetectCallObserver"


# instance fields
.field private final mListener:Landroid/telephony/PhoneStateListener;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;-><init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;)V

    .line 50
    new-instance v0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver$1;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver$1;-><init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;->mListener:Landroid/telephony/PhoneStateListener;

    .line 35
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    .line 36
    return-void
.end method


# virtual methods
.method protected startObserver()V
    .locals 3

    .prologue
    .line 40
    invoke-super {p0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->startObserver()V

    .line 41
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;->mListener:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 42
    return-void
.end method

.method protected stopObserver(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->stopObserver(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;->detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CALL_ACCEPT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;->onComplete(Ljava/lang/String;)V

    .line 48
    return-void
.end method
