.class public Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;
.super Landroid/os/Handler;
.source "EmergencyDialogHandler.java"


# static fields
.field public static final ACCEPT:I = 0x3

.field private static final ACTION_STATUSBAR_EXPAND:Ljava/lang/String; = "com.android.systemui.statusbar.EXPANDED"

.field public static final CANCEL:I = 0x2

.field public static final DIALOG_ALL_OF_DIALOGS:I = -0x1

.field public static final DIALOG_CHOICE_EMERGENCY:I = 0x0

.field public static final DIALOG_CHOICE_UPSM:I = 0x1

.field public static final DIALOG_CONFIRM:I = 0x4

.field public static final DIALOG_PROGRESS:I = 0x3

.field public static final DIALOG_TIMER:I = 0x2

.field public static final FORCE_CLOSE:I = -0x1

.field private static final TAG:Ljava/lang/String; = "EmergencyDialogHandler"


# instance fields
.field private mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mEMChoiceDialog:Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

.field private mEMProgressDialog:Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;

.field private mEMTimerDialog:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mUpsmChoiceDialog:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

.field private progressing:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/EmergencyActionListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "actionListener"    # Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->progressing:Z

    .line 70
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    .line 71
    iput-object p2, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    .line 72
    new-instance v0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$1;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$1;-><init>(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 88
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mIntentFilter:Landroid/content/IntentFilter;

    .line 89
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "com.android.systemui.statusbar.EXPANDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 90
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->dismissAllDialog(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;)Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;
    .param p1, "x1"    # Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMChoiceDialog:Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;
    .param p1, "x1"    # Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mUpsmChoiceDialog:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    return-object p1
.end method

.method static synthetic access$602(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;
    .param p1, "x1"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMTimerDialog:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    return-object p1
.end method

.method private dismissAllDialog(II)V
    .locals 3
    .param p1, "msg"    # I
    .param p2, "reason"    # I

    .prologue
    .line 213
    const-string v0, "EmergencyDialogHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dismiss All Dialog "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMChoiceDialog:Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMChoiceDialog:Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

    invoke-virtual {v0, p2}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->dismiss(I)V

    .line 217
    :cond_0
    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mUpsmChoiceDialog:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    if-eqz v0, :cond_1

    .line 218
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mUpsmChoiceDialog:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    invoke-virtual {v0, p2}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->dismiss(I)V

    .line 220
    :cond_1
    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMTimerDialog:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    if-eqz v0, :cond_2

    .line 221
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMTimerDialog:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    invoke-virtual {v0, p2}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dismiss(I)V

    .line 223
    :cond_2
    return-void
.end method


# virtual methods
.method protected getProgressingState()Z
    .locals 1

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->progressing:Z

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v9, 0x1

    .line 95
    const/4 v7, -0x8

    invoke-static {v7}, Landroid/os/Process;->setThreadPriority(I)V

    .line 96
    const/4 v7, 0x0

    invoke-static {v7}, Landroid/os/Process;->setCanSelfBackground(Z)V

    .line 97
    iget v7, p1, Landroid/os/Message;->what:I

    const/4 v8, -0x1

    invoke-direct {p0, v7, v8}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->dismissAllDialog(II)V

    .line 98
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Landroid/os/Bundle;

    .line 101
    .local v2, "object":Landroid/os/Bundle;
    const-string v7, "reqType"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 102
    .local v3, "reqType":Z
    const-string v7, "show"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 103
    .local v4, "show":Z
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    move-result-object v6

    .line 104
    .local v6, "winUtil":Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;
    invoke-virtual {v6}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getDeviceTheme()I

    move-result v7

    if-ne v7, v9, :cond_1

    .line 106
    const/4 v5, 0x5

    .line 110
    .local v5, "theme":I
    :goto_0
    invoke-virtual {v6}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getDeviceType()I

    move-result v7

    if-ne v7, v9, :cond_2

    .line 111
    const/4 v0, 0x1

    .line 116
    .local v0, "isTablet":Z
    :goto_1
    iget v7, p1, Landroid/os/Message;->what:I

    packed-switch v7, :pswitch_data_0

    .line 210
    :cond_0
    :goto_2
    return-void

    .line 108
    .end local v0    # "isTablet":Z
    .end local v5    # "theme":I
    :cond_1
    const/4 v5, 0x4

    .restart local v5    # "theme":I
    goto :goto_0

    .line 113
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "isTablet":Z
    goto :goto_1

    .line 119
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMChoiceDialog:Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

    if-nez v7, :cond_0

    .line 120
    new-instance v7, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8, v3, v5, v0}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;-><init>(Landroid/content/Context;ZIZ)V

    iput-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMChoiceDialog:Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

    .line 121
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMChoiceDialog:Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

    new-instance v8, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$2;

    invoke-direct {v8, p0, v3}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$2;-><init>(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;Z)V

    invoke-virtual {v7, v8}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 134
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMChoiceDialog:Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;

    invoke-virtual {v7}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->show()Landroid/app/AlertDialog;

    .line 135
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_2

    .line 141
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mUpsmChoiceDialog:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    if-nez v7, :cond_0

    .line 142
    new-instance v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8, v3, v5, v0}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;-><init>(Landroid/content/Context;ZIZ)V

    iput-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mUpsmChoiceDialog:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    .line 143
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mUpsmChoiceDialog:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    new-instance v8, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;

    invoke-direct {v8, p0, v3}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;-><init>(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;Z)V

    invoke-virtual {v7, v8}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mUpsmChoiceDialog:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    invoke-virtual {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->show()Landroid/app/AlertDialog;

    .line 157
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_2

    .line 163
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMTimerDialog:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    if-nez v7, :cond_0

    .line 164
    new-instance v7, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8, v5, v0}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;-><init>(Landroid/content/Context;IZ)V

    iput-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMTimerDialog:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .line 165
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMTimerDialog:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    new-instance v8, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$4;

    invoke-direct {v8, p0}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$4;-><init>(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)V

    invoke-virtual {v7, v8}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 182
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMTimerDialog:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    invoke-virtual {v7}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->show()Landroid/app/AlertDialog;

    goto :goto_2

    .line 188
    :pswitch_3
    if-eqz v4, :cond_3

    .line 189
    const-string v7, "EmergencyDialogHandler"

    const-string v8, "show Progress Dialog"

    invoke-static {v7, v8}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMProgressDialog:Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;

    if-nez v7, :cond_0

    .line 191
    new-instance v7, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8, v5, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;-><init>(Landroid/content/Context;IZ)V

    iput-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMProgressDialog:Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;

    .line 192
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMProgressDialog:Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;

    invoke-virtual {v7}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->show()V

    goto/16 :goto_2

    .line 195
    :cond_3
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMProgressDialog:Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;

    if-eqz v7, :cond_0

    .line 196
    const-string v7, "EmergencyDialogHandler"

    const-string v8, "dismiss Progress Dialog"

    invoke-static {v7, v8}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMProgressDialog:Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;

    invoke-virtual {v7}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->dismiss()V

    .line 198
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMProgressDialog:Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;

    goto/16 :goto_2

    .line 205
    :pswitch_4
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    invoke-interface {v7, v3}, Lcom/sec/android/emergencymode/service/EmergencyActionListener;->onAccept(Z)V

    .line 206
    new-instance v1, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;

    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;

    invoke-direct {v1, v7, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;-><init>(Landroid/content/Context;Z)V

    .line 207
    .local v1, "mEMConfirmDialog":Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;
    invoke-virtual {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->show()Landroid/app/AlertDialog;

    goto/16 :goto_2

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected showDialog(IZZ)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "reqType"    # Z
    .param p3, "show"    # Z

    .prologue
    .line 230
    const-string v2, "EmergencyDialogHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Request show dialog "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const/4 v2, 0x3

    if-ne p1, v2, :cond_0

    .line 232
    iput-boolean p3, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->progressing:Z

    .line 234
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 235
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 236
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "reqType"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 237
    const-string v2, "show"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 238
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 239
    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->sendMessage(Landroid/os/Message;)Z

    .line 240
    return-void
.end method
