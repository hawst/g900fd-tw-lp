.class Lcom/sec/android/emergencymode/sms/EmergencySmsSender$3;
.super Lcom/android/mms/transaction/ISnsRemoteServiceCallback$Stub;
.source "EmergencySmsSender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/sms/EmergencySmsSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$3;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    invoke-direct {p0}, Lcom/android/mms/transaction/ISnsRemoteServiceCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onResponse(III)V
    .locals 3
    .param p1, "appID"    # I
    .param p2, "msgID"    # I
    .param p3, "value"    # I

    .prologue
    .line 176
    const/16 v0, 0xcad

    if-ne p1, v0, :cond_0

    .line 177
    const-string v0, "EmergencySmsSender"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResponse from PIC Message service: appid["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] msgID["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]   value["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$3;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    # invokes: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->stopMessageBindService()V
    invoke-static {v0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$600(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V

    .line 183
    :cond_0
    return-void
.end method
