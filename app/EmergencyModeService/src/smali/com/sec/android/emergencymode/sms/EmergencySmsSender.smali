.class public Lcom/sec/android/emergencymode/sms/EmergencySmsSender;
.super Ljava/lang/Object;
.source "EmergencySmsSender.java"


# static fields
.field private static final APP_ID_PICMSG:I = 0xcad

.field public static final BACK_GROUND_MSG_SENDING:Ljava/lang/String; = "com.android.mms.transaction.Send.BACKGROUND_MSG"

.field public static final EMERGENCY_MESSAGE:Ljava/lang/String; = "the_string_of_emergency_message"

.field private static final SEND_MSG0:I = 0x64

.field private static final TAG:Ljava/lang/String; = "EmergencySmsSender"


# instance fields
.field private mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

.field public mCallbackConn:Landroid/content/ServiceConnection;

.field private mCallbackConnected:Z

.field private mContext:Landroid/content/Context;

.field private mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

.field mServiceConn:Landroid/content/ServiceConnection;

.field private mSnsCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

.field private mSvcConnected:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;-><init>(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceConn:Landroid/content/ServiceConnection;

    .line 141
    new-instance v0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$2;-><init>(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackConn:Landroid/content/ServiceConnection;

    .line 167
    new-instance v0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$3;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$3;-><init>(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSnsCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    .line 57
    iput-object p1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mContext:Landroid/content/Context;

    .line 58
    invoke-direct {p0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->init()V

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Lcom/android/mms/transaction/IMessageBackgroundSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/sms/EmergencySmsSender;
    .param p1, "x1"    # Lcom/android/mms/transaction/IMessageBackgroundSender;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/sms/EmergencySmsSender;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSvcConnected:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->sendEmergencyMessage()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/ISnsRemoteService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Lcom/android/mms/transaction/ISnsRemoteService;)Lcom/android/mms/transaction/ISnsRemoteService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/sms/EmergencySmsSender;
    .param p1, "x1"    # Lcom/android/mms/transaction/ISnsRemoteService;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

    return-object p1
.end method

.method static synthetic access$402(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/sms/EmergencySmsSender;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackConnected:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/ISnsRemoteServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSnsCallback:Lcom/android/mms/transaction/ISnsRemoteServiceCallback;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->stopMessageBindService()V

    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 62
    const-string v0, "EmergencySmsSender"

    const-string v1, "init - START"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    iput-object v3, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 65
    iput-object v3, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackBinder:Lcom/android/mms/transaction/ISnsRemoteService;

    .line 66
    iput-boolean v2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSvcConnected:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackConnected:Z

    .line 69
    const-string v0, "EmergencySmsSender"

    const-string v1, "init - END"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method private makeRecipients(Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1, "recipient"    # Ljava/lang/String;

    .prologue
    .line 218
    const-string v2, ","

    invoke-virtual {p1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 220
    .local v1, "mRecipients":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 221
    const-string v2, "EmergencySmsSender"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mRecipients["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 224
    :cond_0
    return-object v1
.end method

.method private sendEmergencyMessage()V
    .locals 8

    .prologue
    .line 192
    const/4 v2, 0x1

    .line 193
    .local v2, "mRequestApp":I
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->getEmergencyContact()Ljava/lang/String;

    move-result-object v3

    .line 196
    .local v3, "receiverList":Ljava/lang/String;
    invoke-direct {p0, v3}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->makeRecipients(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 198
    .local v4, "recipients":[Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string v5, "com.android.mms.transaction.Send.BACKGROUND_MSG"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 200
    .local v0, "mIntent":Landroid/content/Intent;
    const-string v5, "recipients"

    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    const-string v5, "requestApp"

    invoke-virtual {v0, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 203
    iget-object v5, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "the_string_of_emergency_message"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 205
    .local v1, "mMessageText":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 206
    const-string v1, "help me.."

    .line 209
    :cond_0
    const-string v5, "message"

    invoke-virtual {v0, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 211
    const/16 v5, 0xcad

    const/16 v6, 0x64

    invoke-direct {p0, v0, v5, v6}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->sendIntentToMsg(Landroid/content/Intent;II)V

    .line 212
    const-string v5, "EmergencySmsSender"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Send SMS["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    return-void
.end method

.method private sendIntentToMsg(Landroid/content/Intent;II)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "nAppId"    # I
    .param p3, "nMsgType"    # I

    .prologue
    .line 229
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    if-eqz v1, :cond_0

    .line 230
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-interface {v1, p2, p3, p1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->sendMessage(IILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private stopMessageBindService()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    iget-boolean v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackConnected:Z

    if-eqz v0, :cond_0

    .line 86
    const-string v0, "EmergencySmsSender"

    const-string v1, "stopMessageCallback"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 88
    iput-boolean v2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackConnected:Z

    .line 90
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSvcConnected:Z

    if-eqz v0, :cond_1

    .line 91
    const-string v0, "EmergencySmsSender"

    const-string v1, "stopMessageBind"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 93
    iput-boolean v2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSvcConnected:Z

    .line 95
    :cond_1
    return-void
.end method


# virtual methods
.method public getEmergencyContact()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v4, 0x3

    .line 239
    const-string v0, "EmergencySmsSender"

    const-string v3, "getEmergencyContact"

    invoke-static {v0, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    const/4 v0, 0x1

    const-string v3, "display_name"

    aput-object v3, v2, v0

    const/4 v0, 0x2

    const-string v3, "photo_id"

    aput-object v3, v2, v0

    const-string v0, "data1"

    aput-object v0, v2, v4

    .line 248
    .local v2, "ICE_PROJECTION":[Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v8, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 251
    .local v8, "str":Ljava/lang/StringBuilder;
    const-string v0, "ICE"

    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 252
    .local v9, "title":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://com.android.contacts/groups/title/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/contacts"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 253
    .local v1, "iceUri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "emergency"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v3, "defaultId"

    const-string v4, "3"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 256
    const/4 v7, 0x0

    .line 258
    .local v7, "iceContactCursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 259
    if-eqz v7, :cond_2

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 260
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 262
    :cond_0
    const/4 v0, 0x3

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    invoke-interface {v7}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 264
    const-string v0, ","

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    :cond_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 271
    :cond_2
    if-eqz v7, :cond_3

    .line 272
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 275
    :cond_3
    :goto_0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 268
    :catch_0
    move-exception v6

    .line 269
    .local v6, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "EmergencySmsSender"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getEmergencyContact e : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 271
    if-eqz v7, :cond_3

    .line 272
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 271
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_4

    .line 272
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method public startMessageBindService()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 73
    const-string v0, "EmergencySmsSender"

    const-string v1, "startMessageBindService"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    invoke-direct {p0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->stopMessageBindService()V

    .line 77
    iget-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/mms/transaction/IMessageBackgroundSender;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 80
    iget-object v0, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/android/mms/transaction/ISnsRemoteService;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mCallbackConn:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 82
    return-void
.end method
