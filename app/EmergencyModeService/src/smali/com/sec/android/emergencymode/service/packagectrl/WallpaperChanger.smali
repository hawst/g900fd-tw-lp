.class public Lcom/sec/android/emergencymode/service/packagectrl/WallpaperChanger;
.super Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;
.source "WallpaperChanger.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "WallpaperChanger"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method


# virtual methods
.method public changeDefaultApp(Z)V
    .locals 10
    .param p1, "req"    # Z

    .prologue
    .line 38
    if-eqz p1, :cond_1

    .line 39
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/packagectrl/WallpaperChanger;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v6

    .line 40
    .local v6, "wm":Landroid/app/WallpaperManager;
    invoke-virtual {v6}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v5

    .line 41
    .local v5, "wi":Landroid/app/WallpaperInfo;
    if-eqz v5, :cond_0

    .line 42
    invoke-virtual {v5}, Landroid/app/WallpaperInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v1

    .line 43
    .local v1, "defaultComp":Landroid/content/ComponentName;
    const-string v7, "DEFAULT_WALLPAPER"

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/emergencymode/service/packagectrl/WallpaperChanger;->saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v7, "DEFAULT_WALLPAPER_CLASS"

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/emergencymode/service/packagectrl/WallpaperChanger;->saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v7, "WallpaperChanger"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "default live wallpaper : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    invoke-virtual {v6}, Landroid/app/WallpaperManager;->getIWallpaperManager()Landroid/app/IWallpaperManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/app/IWallpaperManager;->clearWallpaper()V

    .line 61
    .end local v1    # "defaultComp":Landroid/content/ComponentName;
    .end local v5    # "wi":Landroid/app/WallpaperInfo;
    .end local v6    # "wm":Landroid/app/WallpaperManager;
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    const-string v7, "DEFAULT_WALLPAPER"

    invoke-virtual {p0, v7}, Lcom/sec/android/emergencymode/service/packagectrl/WallpaperChanger;->loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 50
    .local v4, "pkgName":Ljava/lang/String;
    const-string v7, "DEFAULT_WALLPAPER_CLASS"

    invoke-virtual {p0, v7}, Lcom/sec/android/emergencymode/service/packagectrl/WallpaperChanger;->loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "className":Ljava/lang/String;
    const-string v7, "WallpaperChanger"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "changeDefaultWallpaper : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    if-eqz v4, :cond_0

    .line 53
    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .local v3, "oldDefaultComp":Landroid/content/ComponentName;
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/packagectrl/WallpaperChanger;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v6

    .line 55
    .restart local v6    # "wm":Landroid/app/WallpaperManager;
    invoke-virtual {v6}, Landroid/app/WallpaperManager;->getIWallpaperManager()Landroid/app/IWallpaperManager;

    move-result-object v7

    invoke-interface {v7, v3}, Landroid/app/IWallpaperManager;->setWallpaperComponent(Landroid/content/ComponentName;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 58
    .end local v0    # "className":Ljava/lang/String;
    .end local v3    # "oldDefaultComp":Landroid/content/ComponentName;
    .end local v4    # "pkgName":Ljava/lang/String;
    .end local v6    # "wm":Landroid/app/WallpaperManager;
    :catch_0
    move-exception v2

    .line 59
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "WallpaperChanger"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "changeDefaultApp e : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
