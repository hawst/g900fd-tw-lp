.class Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;
.super Landroid/os/Handler;
.source "EmergencyAutoTimerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 165
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$100(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I

    move-result v1

    if-nez v1, :cond_1

    .line 166
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$200(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 167
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dismiss(I)V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 171
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$100(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-nez v1, :cond_2

    .line 172
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mVibrator:Landroid/os/Vibrator;
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$300(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/os/Vibrator;

    move-result-object v1

    const-wide/16 v2, 0x2bc

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$100(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->reqTime:I
    invoke-static {v2}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$400(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 177
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTalkbackUtil:Lcom/sec/android/emergencymode/util/TalkBackUtil;
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$500(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Lcom/sec/android/emergencymode/util/TalkBackUtil;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 178
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTalkbackUtil:Lcom/sec/android/emergencymode/util/TalkBackUtil;
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$500(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Lcom/sec/android/emergencymode/util/TalkBackUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->warningMsg:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$600(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->requestSpeak(Ljava/lang/String;)V

    .line 180
    :cond_3
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # operator-- for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$110(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I

    .line 181
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->remainTime:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$800(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    iget-object v3, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I
    invoke-static {v3}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$100(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I

    move-result v3

    # invokes: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->getTimeString(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$700(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$100(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I

    move-result v1

    if-nez v1, :cond_4

    .line 183
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTimerHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$000(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EmergencyAutoTimerDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Vibrate Exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 185
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;->this$0:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTimerHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->access$000(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0
.end method
