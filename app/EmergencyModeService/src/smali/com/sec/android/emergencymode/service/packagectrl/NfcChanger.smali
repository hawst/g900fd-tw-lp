.class public Lcom/sec/android/emergencymode/service/packagectrl/NfcChanger;
.super Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;
.source "NfcChanger.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NfcChanger"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method


# virtual methods
.method public changeDefaultApp(Z)V
    .locals 8
    .param p1, "req"    # Z

    .prologue
    .line 37
    if-eqz p1, :cond_1

    .line 38
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/packagectrl/NfcChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "nfc_payment_default_component"

    invoke-static {v5, v6}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "componentString":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 40
    invoke-static {v1}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v2

    .line 41
    .local v2, "defaultComp":Landroid/content/ComponentName;
    if-eqz v2, :cond_0

    .line 42
    const-string v5, "DEFAULT_NFC"

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/emergencymode/service/packagectrl/NfcChanger;->saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v5, "DEFAULT_NFC_CLASS"

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/emergencymode/service/packagectrl/NfcChanger;->saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .end local v1    # "componentString":Ljava/lang/String;
    .end local v2    # "defaultComp":Landroid/content/ComponentName;
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    const-string v5, "DEFAULT_NFC"

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/packagectrl/NfcChanger;->loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 48
    .local v4, "pkgName":Ljava/lang/String;
    const-string v5, "DEFAULT_NFC_CLASS"

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/packagectrl/NfcChanger;->loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "className":Ljava/lang/String;
    const-string v5, "NfcChanger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "changeDefaultNfc : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    if-eqz v4, :cond_0

    .line 51
    new-instance v3, Landroid/content/ComponentName;

    invoke-direct {v3, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .local v3, "oldDefaultComp":Landroid/content/ComponentName;
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/packagectrl/NfcChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "nfc_payment_default_component"

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    invoke-static {v6, v7, v5}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method
