.class Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;
.super Landroid/content/BroadcastReceiver;
.source "UpsmChoiceDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)V
    .locals 0

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 285
    const-string v7, "level"

    const/4 v8, 0x0

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    # setter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$002(I)I

    .line 286
    const/4 v6, 0x0

    .line 289
    .local v6, "ultraPowerSavingTimeF":F
    :try_start_0
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->cfmsService:Landroid/os/ICustomFrequencyManager;
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$100()Landroid/os/ICustomFrequencyManager;

    move-result-object v7

    if-nez v7, :cond_0

    .line 290
    const-string v7, "CustomFrequencyManagerService"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    .line 291
    .local v0, "b":Landroid/os/IBinder;
    if-eqz v0, :cond_0

    .line 292
    invoke-static {v0}, Landroid/os/ICustomFrequencyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/ICustomFrequencyManager;

    move-result-object v7

    # setter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->cfmsService:Landroid/os/ICustomFrequencyManager;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$102(Landroid/os/ICustomFrequencyManager;)Landroid/os/ICustomFrequencyManager;

    .line 295
    .end local v0    # "b":Landroid/os/IBinder;
    :cond_0
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->cfmsService:Landroid/os/ICustomFrequencyManager;
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$100()Landroid/os/ICustomFrequencyManager;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 296
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->cfmsService:Landroid/os/ICustomFrequencyManager;
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$100()Landroid/os/ICustomFrequencyManager;

    move-result-object v7

    invoke-interface {v7}, Landroid/os/ICustomFrequencyManager;->getStandbyTimeInUltraPowerSavingMode()I

    move-result v7

    # setter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$202(I)I

    .line 297
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$200()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v7

    int-to-float v6, v7

    .line 305
    :cond_1
    :goto_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-static {v7}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v5

    .line 306
    .local v5, "nf":Ljava/text/NumberFormat;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "ar_AE"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 307
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingBattery:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$300(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    :goto_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "fi_FI"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 314
    .local v1, "bIsLocaleFinlan":Z
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "si_IN"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 316
    .local v2, "bIsLocaleSinhala":Z
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    invoke-virtual {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isSpaceRequired()Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v4, " "

    .line 318
    .local v4, "fi_space":Ljava/lang/String;
    :goto_2
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$200()I

    move-result v7

    const/16 v8, 0x5a0

    if-lt v7, v8, :cond_9

    .line 319
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$200()I

    move-result v7

    div-int/lit16 v7, v7, 0x5a0

    # setter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayTime:I
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$402(I)I

    .line 320
    const/high16 v7, 0x41200000    # 10.0f

    mul-float/2addr v7, v6

    const/high16 v8, 0x44b40000    # 1440.0f

    div-float/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    rem-int/lit8 v7, v7, 0xa

    # setter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayPointTime:I
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$502(I)I

    .line 321
    if-nez v2, :cond_7

    .line 322
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$400()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_6

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayPointTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$500()I

    move-result v7

    if-nez v7, :cond_6

    .line 323
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$600(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$400()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayPointTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$500()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090020

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    :goto_3
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 352
    const/16 v7, 0x60

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_d

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x64

    if-gt v7, v8, :cond_d

    .line 353
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_c

    const v7, 0x7f02004b

    :goto_4
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 428
    :cond_2
    :goto_5
    return-void

    .line 299
    .end local v1    # "bIsLocaleFinlan":Z
    .end local v2    # "bIsLocaleSinhala":Z
    .end local v4    # "fi_space":Ljava/lang/String;
    .end local v5    # "nf":Ljava/text/NumberFormat;
    :catch_0
    move-exception v3

    .line 300
    .local v3, "e":Landroid/os/RemoteException;
    invoke-virtual {v3}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 301
    .end local v3    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v3

    .line 302
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 308
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v5    # "nf":Ljava/text/NumberFormat;
    :cond_3
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "tr_TR"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 309
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingBattery:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$300(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 311
    :cond_4
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingBattery:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$300(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "%"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 316
    .restart local v1    # "bIsLocaleFinlan":Z
    .restart local v2    # "bIsLocaleSinhala":Z
    :cond_5
    const-string v4, ""

    goto/16 :goto_2

    .line 326
    .restart local v4    # "fi_space":Ljava/lang/String;
    :cond_6
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$600(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$400()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayPointTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$500()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090021

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 331
    :cond_7
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$400()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayPointTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$500()I

    move-result v7

    if-nez v7, :cond_8

    .line 332
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$600(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090020

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$400()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayPointTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$500()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 335
    :cond_8
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$600(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090021

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$400()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayPointTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$500()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 339
    :cond_9
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$200()I

    move-result v7

    const/16 v8, 0x3c

    if-lt v7, v8, :cond_b

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$200()I

    move-result v7

    const/16 v8, 0x5a0

    if-ge v7, v8, :cond_b

    .line 340
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$200()I

    move-result v7

    div-int/lit8 v7, v7, 0x3c

    # setter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingHourTime:I
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$702(I)I

    .line 341
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$200()I

    move-result v7

    rem-int/lit8 v7, v7, 0x3c

    # setter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingMinTime:I
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$802(I)I

    .line 342
    if-eqz v1, :cond_a

    .line 343
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$600(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingHourTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$700()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " h "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingMinTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$800()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " min"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 345
    :cond_a
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$600(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090025

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingHourTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$700()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingMinTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$800()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 347
    :cond_b
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$600(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$200()I

    move-result v9

    int-to-long v10, v9

    invoke-virtual {v5, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090023

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 353
    :cond_c
    const v7, 0x7f02004c

    goto/16 :goto_4

    .line 354
    :cond_d
    const/16 v7, 0x5e

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_f

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x60

    if-ge v7, v8, :cond_f

    .line 355
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_e

    const v7, 0x7f020049

    :goto_6
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_e
    const v7, 0x7f02004a

    goto :goto_6

    .line 356
    :cond_f
    const/16 v7, 0x5c

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_11

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x5e

    if-ge v7, v8, :cond_11

    .line 357
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_10

    const v7, 0x7f020047

    :goto_7
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_10
    const v7, 0x7f020048

    goto :goto_7

    .line 358
    :cond_11
    const/16 v7, 0x5a

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_13

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x5c

    if-ge v7, v8, :cond_13

    .line 359
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_12

    const v7, 0x7f020045

    :goto_8
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_12
    const v7, 0x7f020046

    goto :goto_8

    .line 360
    :cond_13
    const/16 v7, 0x57

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_15

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x5a

    if-ge v7, v8, :cond_15

    .line 361
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_14

    const v7, 0x7f020043

    :goto_9
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_14
    const v7, 0x7f020044

    goto :goto_9

    .line 362
    :cond_15
    const/16 v7, 0x54

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_17

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x57

    if-ge v7, v8, :cond_17

    .line 363
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_16

    const v7, 0x7f020041

    :goto_a
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_16
    const v7, 0x7f020042

    goto :goto_a

    .line 364
    :cond_17
    const/16 v7, 0x51

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_19

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x54

    if-ge v7, v8, :cond_19

    .line 365
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_18

    const v7, 0x7f02003f

    :goto_b
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_18
    const v7, 0x7f020040

    goto :goto_b

    .line 366
    :cond_19
    const/16 v7, 0x4e

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_1b

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x51

    if-ge v7, v8, :cond_1b

    .line 367
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_1a

    const v7, 0x7f02003d

    :goto_c
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_1a
    const v7, 0x7f02003e

    goto :goto_c

    .line 368
    :cond_1b
    const/16 v7, 0x4b

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_1d

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x4e

    if-ge v7, v8, :cond_1d

    .line 369
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_1c

    const v7, 0x7f02003b

    :goto_d
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_1c
    const v7, 0x7f02003c

    goto :goto_d

    .line 370
    :cond_1d
    const/16 v7, 0x48

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_1f

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x4b

    if-ge v7, v8, :cond_1f

    .line 371
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_1e

    const v7, 0x7f020039

    :goto_e
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_1e
    const v7, 0x7f02003a

    goto :goto_e

    .line 372
    :cond_1f
    const/16 v7, 0x45

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_21

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x48

    if-ge v7, v8, :cond_21

    .line 373
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_20

    const v7, 0x7f020037

    :goto_f
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_20
    const v7, 0x7f020038

    goto :goto_f

    .line 374
    :cond_21
    const/16 v7, 0x42

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_23

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x45

    if-ge v7, v8, :cond_23

    .line 375
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_22

    const v7, 0x7f020035

    :goto_10
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_22
    const v7, 0x7f020036

    goto :goto_10

    .line 376
    :cond_23
    const/16 v7, 0x3f

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_25

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x42

    if-ge v7, v8, :cond_25

    .line 377
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_24

    const v7, 0x7f020033

    :goto_11
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_24
    const v7, 0x7f020034

    goto :goto_11

    .line 378
    :cond_25
    const/16 v7, 0x3c

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_27

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x3f

    if-ge v7, v8, :cond_27

    .line 379
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_26

    const v7, 0x7f020031

    :goto_12
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_26
    const v7, 0x7f020032

    goto :goto_12

    .line 380
    :cond_27
    const/16 v7, 0x39

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_29

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x3c

    if-ge v7, v8, :cond_29

    .line 381
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_28

    const v7, 0x7f02002f

    :goto_13
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_28
    const v7, 0x7f020030

    goto :goto_13

    .line 382
    :cond_29
    const/16 v7, 0x36

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_2b

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x39

    if-ge v7, v8, :cond_2b

    .line 383
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_2a

    const v7, 0x7f02002d

    :goto_14
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_2a
    const v7, 0x7f02002e

    goto :goto_14

    .line 384
    :cond_2b
    const/16 v7, 0x35

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_2d

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x36

    if-ge v7, v8, :cond_2d

    .line 385
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_2c

    const v7, 0x7f02002b

    :goto_15
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_2c
    const v7, 0x7f02002c

    goto :goto_15

    .line 386
    :cond_2d
    const/16 v7, 0x33

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_2f

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x35

    if-ge v7, v8, :cond_2f

    .line 387
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_2e

    const v7, 0x7f020029

    :goto_16
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_2e
    const v7, 0x7f02002a

    goto :goto_16

    .line 388
    :cond_2f
    const/16 v7, 0x31

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_31

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x33

    if-ge v7, v8, :cond_31

    .line 389
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_30

    const v7, 0x7f020027

    :goto_17
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_30
    const v7, 0x7f020028

    goto :goto_17

    .line 390
    :cond_31
    const/16 v7, 0x30

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_33

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x31

    if-ge v7, v8, :cond_33

    .line 391
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_32

    const v7, 0x7f020025

    :goto_18
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_32
    const v7, 0x7f020026

    goto :goto_18

    .line 392
    :cond_33
    const/16 v7, 0x2d

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_35

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x30

    if-ge v7, v8, :cond_35

    .line 393
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_34

    const v7, 0x7f020023

    :goto_19
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_34
    const v7, 0x7f020024

    goto :goto_19

    .line 394
    :cond_35
    const/16 v7, 0x2a

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_37

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x2d

    if-ge v7, v8, :cond_37

    .line 395
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_36

    const v7, 0x7f020021

    :goto_1a
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_36
    const v7, 0x7f020022

    goto :goto_1a

    .line 396
    :cond_37
    const/16 v7, 0x27

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_39

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x2a

    if-ge v7, v8, :cond_39

    .line 397
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_38

    const v7, 0x7f02001f

    :goto_1b
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_38
    const v7, 0x7f020020

    goto :goto_1b

    .line 398
    :cond_39
    const/16 v7, 0x24

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_3b

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x27

    if-ge v7, v8, :cond_3b

    .line 399
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_3a

    const v7, 0x7f02001d

    :goto_1c
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_3a
    const v7, 0x7f02001e

    goto :goto_1c

    .line 400
    :cond_3b
    const/16 v7, 0x21

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_3d

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x24

    if-ge v7, v8, :cond_3d

    .line 401
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_3c

    const v7, 0x7f02001b

    :goto_1d
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_3c
    const v7, 0x7f02001c

    goto :goto_1d

    .line 402
    :cond_3d
    const/16 v7, 0x1e

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_3f

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x21

    if-ge v7, v8, :cond_3f

    .line 403
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_3e

    const v7, 0x7f020019

    :goto_1e
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_3e
    const v7, 0x7f02001a

    goto :goto_1e

    .line 404
    :cond_3f
    const/16 v7, 0x1d

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_41

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x1e

    if-ge v7, v8, :cond_41

    .line 405
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_40

    const v7, 0x7f020017

    :goto_1f
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_40
    const v7, 0x7f020018

    goto :goto_1f

    .line 406
    :cond_41
    const/16 v7, 0x1b

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_43

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x1d

    if-ge v7, v8, :cond_43

    .line 407
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_42

    const v7, 0x7f020015

    :goto_20
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_42
    const v7, 0x7f020016

    goto :goto_20

    .line 408
    :cond_43
    const/16 v7, 0x18

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_45

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x1b

    if-ge v7, v8, :cond_45

    .line 409
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_44

    const v7, 0x7f020013

    :goto_21
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_44
    const v7, 0x7f020014

    goto :goto_21

    .line 410
    :cond_45
    const/16 v7, 0x15

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_47

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x18

    if-ge v7, v8, :cond_47

    .line 411
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_46

    const v7, 0x7f020011

    :goto_22
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_46
    const v7, 0x7f020012

    goto :goto_22

    .line 412
    :cond_47
    const/16 v7, 0x12

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_49

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x15

    if-ge v7, v8, :cond_49

    .line 413
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_48

    const v7, 0x7f02000f

    :goto_23
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_48
    const v7, 0x7f020010

    goto :goto_23

    .line 414
    :cond_49
    const/16 v7, 0xf

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_4b

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x12

    if-ge v7, v8, :cond_4b

    .line 415
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_4a

    const v7, 0x7f02000d

    :goto_24
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_4a
    const v7, 0x7f02000e

    goto :goto_24

    .line 416
    :cond_4b
    const/16 v7, 0xc

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_4d

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0xf

    if-ge v7, v8, :cond_4d

    .line 417
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_4c

    const v7, 0x7f02000b

    :goto_25
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_4c
    const v7, 0x7f02000c

    goto :goto_25

    .line 418
    :cond_4d
    const/16 v7, 0x9

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_4f

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0xc

    if-ge v7, v8, :cond_4f

    .line 419
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_4e

    const v7, 0x7f020009

    :goto_26
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_4e
    const v7, 0x7f02000a

    goto :goto_26

    .line 420
    :cond_4f
    const/4 v7, 0x7

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_51

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/16 v8, 0x9

    if-ge v7, v8, :cond_51

    .line 421
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_50

    const v7, 0x7f020007

    :goto_27
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_50
    const v7, 0x7f020008

    goto :goto_27

    .line 422
    :cond_51
    const/4 v7, 0x6

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v8

    if-gt v7, v8, :cond_53

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/4 v8, 0x7

    if-ge v7, v8, :cond_53

    .line 423
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v8

    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    iget-boolean v7, v7, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v7, :cond_52

    const v7, 0x7f020005

    :goto_28
    invoke-virtual {v8, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    :cond_52
    const v7, 0x7f020006

    goto :goto_28

    .line 424
    :cond_53
    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I
    invoke-static {}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$000()I

    move-result v7

    const/4 v8, 0x5

    if-gt v7, v8, :cond_2

    .line 425
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 426
    iget-object v7, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;->this$0:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    # getter for: Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;
    invoke-static {v7}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->access$600(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090026

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5
.end method
