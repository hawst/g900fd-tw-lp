.class Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider$1;
.super Lcom/samsung/android/cover/CoverManager$StateListener;
.source "EmergencyModeCocktailProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider$1;->this$0:Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;

    invoke-direct {p0}, Lcom/samsung/android/cover/CoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/cover/CoverState;)V
    .locals 3
    .param p1, "state"    # Lcom/samsung/android/cover/CoverState;

    .prologue
    .line 106
    const-string v0, "EmergencyModeCocktailProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCoverStateChanged state :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/cover/CoverState;->getSwitchState()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider$1;->this$0:Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;

    # invokes: Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->setCoverState(Lcom/samsung/android/cover/CoverState;)V
    invoke-static {v0, p1}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->access$000(Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;Lcom/samsung/android/cover/CoverState;)V

    .line 108
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider$1;->this$0:Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;

    # invokes: Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->drawCocktail()V
    invoke-static {v0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->access$100(Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;)V

    .line 109
    return-void
.end method
