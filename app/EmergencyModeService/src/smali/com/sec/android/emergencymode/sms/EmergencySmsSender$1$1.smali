.class Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;
.super Ljava/lang/Object;
.source "EmergencySmsSender.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;

.field final synthetic val$service:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;Landroid/os/IBinder;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;->this$1:Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;

    iput-object p2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;->val$service:Landroid/os/IBinder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 106
    const-string v1, "EmergencySmsSender"

    const-string v2, "Send Emergency Message - START"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;->this$1:Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;

    iget-object v1, v1, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    iget-object v2, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;->val$service:Landroid/os/IBinder;

    invoke-static {v2}, Lcom/android/mms/transaction/IMessageBackgroundSender$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v2

    # setter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$002(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Lcom/android/mms/transaction/IMessageBackgroundSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 110
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;->this$1:Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;

    iget-object v1, v1, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSvcConnected:Z
    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$102(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Z)Z

    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;->this$1:Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;

    iget-object v1, v1, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    # getter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$000(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;->this$1:Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;

    iget-object v1, v1, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    # getter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$000(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->startListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;->this$1:Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;

    iget-object v1, v1, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    # invokes: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->sendEmergencyMessage()V
    invoke-static {v1}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$200(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V

    .line 120
    const-string v1, "EmergencySmsSender"

    const-string v2, "Send Emergency Message - END"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-void

    .line 116
    :catch_0
    move-exception v0

    .line 117
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
