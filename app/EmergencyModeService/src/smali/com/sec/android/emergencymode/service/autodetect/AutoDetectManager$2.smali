.class Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$2;
.super Ljava/lang/Object;
.source "AutoDetectManager.java"

# interfaces
.implements Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)V
    .locals 1

    .prologue
    .line 90
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$2;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const-string v0, "ActionDetectListener"

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$2;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onComplete(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Complete."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public onDetected(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$2;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onDetected. This is not Emergency situation."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$2;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;

    # getter for: Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->access$200(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$2;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;

    const-string v1, "DETECTED"

    # invokes: Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->stopActionDetect(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->access$000(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;Ljava/lang/String;)V

    .line 97
    return-void
.end method
