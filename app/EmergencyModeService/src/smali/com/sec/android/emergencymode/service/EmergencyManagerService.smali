.class public Lcom/sec/android/emergencymode/service/EmergencyManagerService;
.super Lcom/sec/android/emergencymode/IEmergencyManager$Stub;
.source "EmergencyManagerService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EmergencyManagerService"

.field private static instance:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

.field private prevLocState:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->instance:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/sec/android/emergencymode/IEmergencyManager$Stub;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    .line 62
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mContext:Landroid/content/Context;

    .line 63
    invoke-static {p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencyFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    .line 64
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/IEmergencyManager$Stub;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 67
    const-class v1, Lcom/sec/android/emergencymode/service/EmergencyManagerService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->instance:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    if-nez v0, :cond_0

    .line 68
    const-string v0, "EmergencyManagerService"

    const-string v2, "make instance."

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->instance:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    .line 72
    :cond_0
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->instance:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private isGuestMode()Z
    .locals 3

    .prologue
    .line 188
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getUserName()Ljava/lang/String;

    move-result-object v0

    .line 189
    .local v0, "currentUserName":Ljava/lang/String;
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    .line 190
    .local v1, "userId":I
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x64

    if-ge v1, v2, :cond_0

    .line 191
    const/4 v2, 0x1

    .line 193
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addAppToLauncher(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mContext:Landroid/content/Context;

    const-string v1, "com.sec.android.emergencymode.permission.ADD_APP_LAUNCHER"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    iget-object v0, v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->addAppToLauncher(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected checkForceRecovery()V
    .locals 3

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getEmergencyFCState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "EmergencyManagerService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Emergency Mode force stop abnormally. doRecovery Mode!("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->isEmergencyMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->forceRecovery()V

    .line 227
    :goto_0
    return-void

    .line 225
    :cond_0
    const-string v0, "EmergencyManagerService"

    const-string v1, "Emergency Mode is not FC state. Do not forceRecovery"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public checkInvalidBroadcast(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkInvalidBroadcast(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkInvalidProcess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkInvalidProcess(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkModeType(I)Z
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkModeType(I)Z

    move-result v0

    return v0
.end method

.method public checkValidIntentAction(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "actName"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkValidIntentAction(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkValidPackage(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "actName"    # Ljava/lang/String;
    .param p3, "allowFlag"    # I

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkValidPackage(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public clearBootTime()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->clearBootTime()V

    .line 150
    return-void
.end method

.method public declared-synchronized destroyInstance()V
    .locals 2

    .prologue
    .line 76
    monitor-enter p0

    :try_start_0
    const-string v0, "EmergencyManagerService"

    const-string v1, "destroyInstance"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->instance:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    monitor-exit p0

    return-void

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getEnterType()I
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getEnterType()I

    move-result v0

    return v0
.end method

.method public getHumanType()I
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getHumanType()I

    move-result v0

    return v0
.end method

.method public getModeType()I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getModeType()I

    move-result v0

    return v0
.end method

.method public getReadyToStart()Z
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getReadyToStart()Z

    move-result v0

    return v0
.end method

.method public getforceBlockUserPkg()Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getforceBlockUserPkg()Z

    move-result v0

    return v0
.end method

.method public initForEMState()V
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->initForEMState()V

    .line 166
    return-void
.end method

.method public isBootFinished()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isBootFinished()Z

    move-result v0

    return v0
.end method

.method public isEmergencyMode()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode()Z

    move-result v0

    return v0
.end method

.method public isModifying()Z
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isModifying()Z

    move-result v0

    return v0
.end method

.method public isScreenOn()Z
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isScreenOn()Z

    move-result v0

    return v0
.end method

.method public needMobileDataBlock()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->needMobileDataBlock()Z

    move-result v0

    return v0
.end method

.method public notifyCurrentState(I)V
    .locals 1
    .param p1, "reason"    # I

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->notifyCurrentState(I)V

    .line 146
    return-void
.end method

.method public setLocationProvider(Z)V
    .locals 7
    .param p1, "enable"    # Z

    .prologue
    .line 169
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mContext:Landroid/content/Context;

    const-string v3, "com.sec.android.emergencymode.permission.MODIFY_LOCATION_PROVIDER"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    new-instance v1, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;-><init>(Landroid/content/Context;)V

    .line 171
    .local v1, "locUtil":Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;
    if-eqz p1, :cond_1

    .line 172
    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->getLocationState()I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->prevLocState:I

    .line 179
    :cond_0
    :goto_0
    const/4 v2, 0x0

    iget v3, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->prevLocState:I

    invoke-virtual {v1, v2, p1, v3}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->setLocationState(ZZI)V

    .line 180
    return-void

    .line 174
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->getLastLocation()Landroid/location/Location;

    move-result-object v0

    .line 175
    .local v0, "loc":Landroid/location/Location;
    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v6

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->updateLocationInfo(DDF)V

    goto :goto_0
.end method

.method public setReadyToStart(Z)V
    .locals 1
    .param p1, "isReady"    # Z

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setReadyToStart(Z)V

    .line 162
    return-void
.end method

.method public setforceBlockUserPkg(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setforceBlockUserPkg(Z)V

    .line 118
    return-void
.end method

.method protected triggerEmergencyMode(ZIZ)I
    .locals 5
    .param p1, "state"    # Z
    .param p2, "flag"    # I
    .param p3, "skipdialog"    # Z

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->isModifying()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 198
    const-string v2, "EmergencyManagerService"

    const-string v3, "modifying! return!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const/4 v1, -0x2

    .line 216
    :goto_0
    return v1

    .line 200
    :cond_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    and-int/lit16 v2, p2, 0xff0

    if-nez v2, :cond_1

    .line 201
    const-string v2, "EmergencyManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "flag["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] is invalid type."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const/4 v1, -0x3

    goto :goto_0

    .line 203
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->isGuestMode()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 204
    const-string v2, "EmergencyManagerService"

    const-string v3, "Current user is guest."

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    const/4 v1, -0x6

    goto :goto_0

    .line 208
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->isEmergencyMode()Z

    move-result v0

    .line 209
    .local v0, "prevState":Z
    const-string v2, "EmergencyManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "diff state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    if-eq v0, p1, :cond_3

    .line 211
    const-string v2, "EmergencyManagerService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Triggering "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->mEmergencyFactory:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v2, p1, p2, p3}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->runEmergencyMode(ZIZ)I

    move-result v1

    .line 213
    .local v1, "result":I
    goto :goto_0

    .line 215
    .end local v1    # "result":I
    :cond_3
    const-string v2, "EmergencyManagerService"

    const-string v3, "state is invalid."

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const/4 v1, -0x4

    goto/16 :goto_0
.end method
