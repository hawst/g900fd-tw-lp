.class public Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;
.super Landroid/widget/ImageView;
.source "PowerSavingBatteryMeterView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;
    }
.end annotation


# static fields
.field private static final BLINKING_INTERVAL:I = 0x3e8

.field public static final EMPTY:I = 0x4

.field public static final FULL:I = 0x60

.field private static final MSG_POST_INVALIDATE:I = 0x1

.field public static final SHOW_100_PERCENT:Z = false

.field public static final SINGLE_DIGIT_PERCENT:Z = false

.field public static final SUBPIXEL:F = 0.0f

.field public static final TAG:Ljava/lang/String; = "PowerSavingBatteryMeterView"


# instance fields
.field protected isDarkTheme:Z

.field protected isTablet:Z

.field mBatteryPaint:Landroid/graphics/Paint;

.field private mBlinkingNeeded:Z

.field private final mBoltFrame:Landroid/graphics/RectF;

.field mBoltPaint:Landroid/graphics/Paint;

.field private final mBoltPath:Landroid/graphics/Path;

.field private final mBoltPoints:[F

.field private final mButtonFrame:Landroid/graphics/RectF;

.field mButtonHeight:I

.field private mButtonPadding:F

.field private final mClipFrame:Landroid/graphics/RectF;

.field mColors:[I

.field private final mFrame:Landroid/graphics/RectF;

.field mFramePaint:Landroid/graphics/Paint;

.field private mHeight:I

.field private mInvalidString:Ljava/lang/String;

.field mInvalidTextPaint:Landroid/graphics/Paint;

.field private mPostInvalidateHandler:Landroid/os/Handler;

.field private mTextHeight:F

.field mTextPaint:Landroid/graphics/Paint;

.field mTracker:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;

.field private mWarningString:Ljava/lang/String;

.field private mWarningTextHeight:F

.field mWarningTextPaint:Landroid/graphics/Paint;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 167
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 168
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 171
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 172
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 175
    invoke-direct/range {p0 .. p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->isTablet:Z

    .line 56
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->isDarkTheme:Z

    .line 70
    new-instance v11, Landroid/graphics/Path;

    invoke-direct {v11}, Landroid/graphics/Path;-><init>()V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPath:Landroid/graphics/Path;

    .line 72
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    .line 73
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonFrame:Landroid/graphics/RectF;

    .line 74
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mClipFrame:Landroid/graphics/RectF;

    .line 75
    new-instance v11, Landroid/graphics/RectF;

    invoke-direct {v11}, Landroid/graphics/RectF;-><init>()V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    .line 144
    new-instance v11, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;

    const/4 v12, 0x0

    invoke-direct {v11, p0, v12}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;-><init>(Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$1;)V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mTracker:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;

    .line 405
    new-instance v11, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$1;

    invoke-direct {v11, p0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$1;-><init>(Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;)V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mPostInvalidateHandler:Landroid/os/Handler;

    .line 176
    const-string v11, "PowerSavingBatteryMeterView"

    const-string v12, "BatteryMeterView"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 179
    .local v9, "res":Landroid/content/res/Resources;
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    move-result-object v10

    .line 180
    .local v10, "winUtil":Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;
    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getDeviceTheme()I

    move-result v11

    if-nez v11, :cond_0

    .line 181
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->isDarkTheme:Z

    .line 183
    :cond_0
    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getDeviceType()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_1

    .line 184
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->isTablet:Z

    .line 188
    :cond_1
    const/4 v11, 0x2

    new-array v6, v11, [I

    fill-array-data v6, :array_0

    .line 190
    .local v6, "levels":[I
    const/4 v11, 0x2

    new-array v1, v11, [I

    fill-array-data v1, :array_1

    .line 192
    .local v1, "colors":[I
    const/4 v11, 0x2

    new-array v2, v11, [I

    fill-array-data v2, :array_2

    .line 194
    .local v2, "colors_light":[I
    const/4 v11, 0x2

    new-array v4, v11, [I

    fill-array-data v4, :array_3

    .line 196
    .local v4, "green_colors":[I
    const/4 v11, 0x4

    new-array v8, v11, [I

    fill-array-data v8, :array_4

    .line 197
    .local v8, "red_levels":[I
    const/4 v11, 0x4

    new-array v7, v11, [I

    fill-array-data v7, :array_5

    .line 199
    .local v7, "red_colors":[I
    array-length v0, v6

    .line 200
    .local v0, "N":I
    mul-int/lit8 v11, v0, 0x2

    new-array v11, v11, [I

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mColors:[I

    .line 201
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v0, :cond_3

    .line 202
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mColors:[I

    mul-int/lit8 v12, v5, 0x2

    aget v13, v6, v5

    aput v13, v11, v12

    .line 203
    iget-boolean v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->isDarkTheme:Z

    if-eqz v11, :cond_2

    .line 204
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mColors:[I

    mul-int/lit8 v12, v5, 0x2

    add-int/lit8 v12, v12, 0x1

    aget v13, v1, v5

    aput v13, v11, v12

    .line 201
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 206
    :cond_2
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mColors:[I

    mul-int/lit8 v12, v5, 0x2

    add-int/lit8 v12, v12, 0x1

    aget v13, v2, v5

    aput v13, v11, v12

    goto :goto_1

    .line 210
    :cond_3
    const-string v11, "!"

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningString:Ljava/lang/String;

    .line 211
    const-string v11, "X"

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mInvalidString:Ljava/lang/String;

    .line 213
    new-instance v11, Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-direct {v11, v12}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFramePaint:Landroid/graphics/Paint;

    .line 214
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFramePaint:Landroid/graphics/Paint;

    iget-boolean v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->isDarkTheme:Z

    if-eqz v11, :cond_4

    const/high16 v11, 0x7f060000

    :goto_2
    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v12, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 215
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFramePaint:Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setDither(Z)V

    .line 216
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFramePaint:Landroid/graphics/Paint;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 217
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFramePaint:Landroid/graphics/Paint;

    sget-object v12, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 218
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFramePaint:Landroid/graphics/Paint;

    new-instance v12, Landroid/graphics/PorterDuffXfermode;

    sget-object v13, Landroid/graphics/PorterDuff$Mode;->DST_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v12, v13}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 220
    new-instance v11, Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-direct {v11, v12}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBatteryPaint:Landroid/graphics/Paint;

    .line 221
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBatteryPaint:Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setDither(Z)V

    .line 222
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBatteryPaint:Landroid/graphics/Paint;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 223
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBatteryPaint:Landroid/graphics/Paint;

    sget-object v12, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 225
    new-instance v11, Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-direct {v11, v12}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mTextPaint:Landroid/graphics/Paint;

    .line 226
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mTextPaint:Landroid/graphics/Paint;

    const/4 v12, -0x1

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 227
    const-string v11, "sans-serif-condensed"

    const/4 v12, 0x0

    invoke-static {v11, v12}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 228
    .local v3, "font":Landroid/graphics/Typeface;
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v11, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 229
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mTextPaint:Landroid/graphics/Paint;

    sget-object v12, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 231
    new-instance v11, Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-direct {v11, v12}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextPaint:Landroid/graphics/Paint;

    .line 232
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextPaint:Landroid/graphics/Paint;

    const/4 v12, -0x1

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 233
    const-string v11, "sans-serif"

    const/4 v12, 0x1

    invoke-static {v11, v12}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 234
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v11, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 235
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextPaint:Landroid/graphics/Paint;

    sget-object v12, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 237
    new-instance v11, Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-direct {v11, v12}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mInvalidTextPaint:Landroid/graphics/Paint;

    .line 238
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mInvalidTextPaint:Landroid/graphics/Paint;

    const v12, -0x17cbf7

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setColor(I)V

    .line 239
    const-string v11, "sans-serif"

    const/4 v12, 0x1

    invoke-static {v11, v12}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v3

    .line 240
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mInvalidTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v11, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 241
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mInvalidTextPaint:Landroid/graphics/Paint;

    sget-object v12, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 243
    new-instance v11, Landroid/graphics/Paint;

    invoke-direct {v11}, Landroid/graphics/Paint;-><init>()V

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPaint:Landroid/graphics/Paint;

    .line 244
    iget-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPaint:Landroid/graphics/Paint;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 246
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPaint:Landroid/graphics/Paint;

    iget-boolean v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->isDarkTheme:Z

    if-eqz v11, :cond_5

    const v11, 0x7f060002

    :goto_3
    invoke-virtual {v9, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v12, v11}, Landroid/graphics/Paint;->setColor(I)V

    .line 247
    invoke-static {v9}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->loadBoltPoints(Landroid/content/res/Resources;)[F

    move-result-object v11

    iput-object v11, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPoints:[F

    .line 248
    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 249
    return-void

    .line 214
    .end local v3    # "font":Landroid/graphics/Typeface;
    :cond_4
    const v11, 0x7f060001

    goto/16 :goto_2

    .line 246
    .restart local v3    # "font":Landroid/graphics/Typeface;
    :cond_5
    const v11, 0x7f060003

    goto :goto_3

    .line 188
    nop

    :array_0
    .array-data 4
        0x4
        0x64
    .end array-data

    .line 190
    :array_1
    .array-data 4
        -0x1
        -0x1
    .end array-data

    .line 192
    :array_2
    .array-data 4
        -0xcc8f6f
        -0xcc8f6f
    .end array-data

    .line 194
    :array_3
    .array-data 4
        -0x1
        -0x7433eb
    .end array-data

    .line 196
    :array_4
    .array-data 4
        0x4
        0xa
        0x14
        0x64
    .end array-data

    .line 197
    :array_5
    .array-data 4
        -0xcf00
        -0xcf00
        -0x2900
        -0x1
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mPostInvalidateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private getColorForLevel(I)I
    .locals 6
    .param p1, "percent"    # I

    .prologue
    .line 276
    const/4 v0, 0x0

    .line 277
    .local v0, "color":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mColors:[I

    array-length v4, v4

    if-ge v2, v4, :cond_1

    .line 278
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mColors:[I

    aget v3, v4, v2

    .line 279
    .local v3, "thresh":I
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mColors:[I

    add-int/lit8 v5, v2, 0x1

    aget v0, v4, v5

    .line 280
    if-gt p1, v3, :cond_0

    move v1, v0

    .line 283
    .end local v0    # "color":I
    .end local v3    # "thresh":I
    .local v1, "color":I
    :goto_1
    return v1

    .line 277
    .end local v1    # "color":I
    .restart local v0    # "color":I
    .restart local v3    # "thresh":I
    :cond_0
    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    .end local v3    # "thresh":I
    :cond_1
    move v1, v0

    .line 283
    .end local v0    # "color":I
    .restart local v1    # "color":I
    goto :goto_1
.end method

.method private static loadBoltPoints(Landroid/content/res/Resources;)[F
    .locals 8
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 252
    const v5, 0x7f050005

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v3

    .line 253
    .local v3, "pts":[I
    const/4 v1, 0x0

    .local v1, "maxX":I
    const/4 v2, 0x0

    .line 254
    .local v2, "maxY":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, v3

    if-ge v0, v5, :cond_0

    .line 255
    aget v5, v3, v0

    invoke-static {v1, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 256
    add-int/lit8 v5, v0, 0x1

    aget v5, v3, v5

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 254
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 258
    :cond_0
    array-length v5, v3

    new-array v4, v5, [F

    .line 259
    .local v4, "ptsF":[F
    const/4 v0, 0x0

    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_1

    .line 260
    aget v5, v3, v0

    int-to-float v5, v5

    int-to-float v6, v1

    div-float/2addr v5, v6

    aput v5, v4, v0

    .line 261
    add-int/lit8 v5, v0, 0x1

    add-int/lit8 v6, v0, 0x1

    aget v6, v3, v6

    int-to-float v6, v6

    int-to-float v7, v2

    div-float/2addr v6, v7

    aput v6, v4, v5

    .line 259
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 263
    :cond_1
    return-object v4
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 29
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    .line 288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mTracker:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;

    move-object/from16 v19, v0

    .line 290
    .local v19, "tracker":Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;
    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->status:I

    move/from16 v23, v0

    const/16 v24, 0x4

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->health:I

    move/from16 v23, v0

    const/16 v24, 0x3

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->health:I

    move/from16 v23, v0

    const/16 v24, 0x7

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->health:I

    move/from16 v23, v0

    const/16 v24, 0x6

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_3

    .line 295
    :cond_0
    const/16 v23, 0x1

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBlinkingNeeded:Z

    .line 297
    const-string v23, "PowerSavingBatteryMeterView"

    const-string v24, "battery icon blink..."

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mPostInvalidateHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v23

    if-nez v23, :cond_1

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mPostInvalidateHandler:Landroid/os/Handler;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    const-wide/16 v26, 0x3e8

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 305
    :cond_1
    :goto_0
    move-object/from16 v0, v19

    iget v14, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->level:I

    .line 306
    .local v14, "level":I
    const/16 v23, -0x1

    move/from16 v0, v23

    if-ne v14, v0, :cond_4

    .line 403
    :cond_2
    :goto_1
    return-void

    .line 302
    .end local v14    # "level":I
    :cond_3
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBlinkingNeeded:Z

    goto :goto_0

    .line 308
    .restart local v14    # "level":I
    :cond_4
    int-to-float v0, v14

    move/from16 v23, v0

    const/high16 v24, 0x42c80000    # 100.0f

    div-float v11, v23, v24

    .line 309
    .local v11, "drawFrac":F
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->getPaddingTop()I

    move-result v18

    .line 310
    .local v18, "pt":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->getPaddingStart()I

    move-result v16

    .line 311
    .local v16, "pl":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->getPaddingEnd()I

    move-result v17

    .line 312
    .local v17, "pr":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->getPaddingBottom()I

    move-result v15

    .line 313
    .local v15, "pb":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mHeight:I

    move/from16 v23, v0

    sub-int v23, v23, v18

    sub-int v12, v23, v15

    .line 314
    .local v12, "height":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWidth:I

    move/from16 v23, v0

    sub-int v23, v23, v16

    sub-int v20, v23, v17

    .line 316
    .local v20, "width":I
    int-to-float v0, v12

    move/from16 v23, v0

    const v24, 0x3df5c28f    # 0.12f

    mul-float v23, v23, v24

    move/from16 v0, v23

    float-to-int v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonHeight:I

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v26, v0

    int-to-float v0, v12

    move/from16 v27, v0

    invoke-virtual/range {v23 .. v27}, Landroid/graphics/RectF;->set(FFFF)V

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v24, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v25, v0

    invoke-virtual/range {v23 .. v25}, Landroid/graphics/RectF;->offset(FF)V

    .line 321
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->getContext()Landroid/content/Context;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    const/high16 v24, 0x7f070000

    invoke-virtual/range {v23 .. v24}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonPadding:F

    .line 323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v25, v0

    const/high16 v26, 0x3e800000    # 0.25f

    mul-float v25, v25, v26

    add-float v24, v24, v25

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonPadding:F

    move/from16 v25, v0

    add-float v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v27, v0

    const/high16 v28, 0x3e800000    # 0.25f

    mul-float v27, v27, v28

    sub-float v26, v26, v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonPadding:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonHeight:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    add-float v27, v27, v28

    const/high16 v28, 0x40a00000    # 5.0f

    add-float v27, v27, v28

    invoke-virtual/range {v23 .. v27}, Landroid/graphics/RectF;->set(FFFF)V

    .line 329
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    const/16 v25, 0x0

    add-float v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    const/16 v25, 0x0

    add-float v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    const/16 v25, 0x0

    sub-float v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonHeight:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    add-float v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 334
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    const/16 v25, 0x0

    add-float v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/RectF;->left:F

    .line 335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    const/16 v25, 0x0

    add-float v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 336
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v24, v0

    const/16 v25, 0x0

    sub-float v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/RectF;->right:F

    .line 337
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v24, v0

    const/16 v25, 0x0

    sub-float v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFramePaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 344
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->getColorForLevel(I)I

    move-result v10

    .line 345
    .local v10, "color":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBatteryPaint:Landroid/graphics/Paint;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 346
    const-string v23, "PowerSavingBatteryMeterView"

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "onDraw batteryColor : "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    const/16 v23, 0x60

    move/from16 v0, v23

    if-lt v14, v0, :cond_7

    .line 349
    const/high16 v11, 0x3f800000    # 1.0f

    .line 354
    :cond_5
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mButtonFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    const/high16 v23, 0x3f800000    # 1.0f

    cmpl-float v23, v11, v23

    if-nez v23, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBatteryPaint:Landroid/graphics/Paint;

    move-object/from16 v23, v0

    :goto_3
    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v23

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mClipFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 357
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mClipFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Landroid/graphics/RectF;->height()F

    move-result v25

    const/high16 v26, 0x3f800000    # 1.0f

    sub-float v26, v26, v11

    mul-float v25, v25, v26

    add-float v24, v24, v25

    move/from16 v0, v24

    move-object/from16 v1, v23

    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 359
    const/16 v23, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->save(I)I

    .line 360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mClipFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBatteryPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 362
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 364
    move-object/from16 v0, v19

    iget-boolean v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->plugged:Z

    move/from16 v23, v0

    if-eqz v23, :cond_c

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->status:I

    move/from16 v23, v0

    const/16 v24, 0x5

    move/from16 v0, v23

    move/from16 v1, v24

    if-eq v0, v1, :cond_c

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->width()F

    move-result v24

    const/high16 v25, 0x40900000    # 4.5f

    div-float v24, v24, v25

    add-float v7, v23, v24

    .line 367
    .local v7, "bl":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    const/high16 v25, 0x40c00000    # 6.0f

    div-float v24, v24, v25

    add-float v9, v23, v24

    .line 368
    .local v9, "bt":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->width()F

    move-result v24

    const/high16 v25, 0x40e00000    # 7.0f

    div-float v24, v24, v25

    sub-float v8, v23, v24

    .line 369
    .local v8, "br":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/graphics/RectF;->height()F

    move-result v24

    const/high16 v25, 0x41200000    # 10.0f

    div-float v24, v24, v25

    sub-float v6, v23, v24

    .line 370
    .local v6, "bb":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v23, v0

    cmpl-float v23, v23, v7

    if-nez v23, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    cmpl-float v23, v23, v9

    if-nez v23, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v23, v0

    cmpl-float v23, v23, v8

    if-nez v23, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v23, v0

    cmpl-float v23, v23, v6

    if-eqz v23, :cond_a

    .line 372
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v7, v9, v8, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPath:Landroid/graphics/Path;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/Path;->reset()V

    .line 374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPath:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPoints:[F

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aget v25, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/RectF;->width()F

    move-result v26

    mul-float v25, v25, v26

    add-float v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPoints:[F

    move-object/from16 v26, v0

    const/16 v27, 0x1

    aget v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/RectF;->height()F

    move-result v27

    mul-float v26, v26, v27

    add-float v25, v25, v26

    invoke-virtual/range {v23 .. v25}, Landroid/graphics/Path;->moveTo(FF)V

    .line 377
    const/4 v13, 0x2

    .local v13, "i":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPoints:[F

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v23, v0

    move/from16 v0, v23

    if-ge v13, v0, :cond_9

    .line 378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPath:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPoints:[F

    move-object/from16 v25, v0

    aget v25, v25, v13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/RectF;->width()F

    move-result v26

    mul-float v25, v25, v26

    add-float v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPoints:[F

    move-object/from16 v26, v0

    add-int/lit8 v27, v13, 0x1

    aget v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/RectF;->height()F

    move-result v27

    mul-float v26, v26, v27

    add-float v25, v25, v26

    invoke-virtual/range {v23 .. v25}, Landroid/graphics/Path;->lineTo(FF)V

    .line 377
    add-int/lit8 v13, v13, 0x2

    goto :goto_4

    .line 350
    .end local v6    # "bb":F
    .end local v7    # "bl":F
    .end local v8    # "br":F
    .end local v9    # "bt":F
    .end local v13    # "i":I
    :cond_7
    const/16 v23, 0x4

    move/from16 v0, v23

    if-gt v14, v0, :cond_5

    .line 351
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 354
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mFramePaint:Landroid/graphics/Paint;

    move-object/from16 v23, v0

    goto/16 :goto_3

    .line 382
    .restart local v6    # "bb":F
    .restart local v7    # "bl":F
    .restart local v8    # "br":F
    .restart local v9    # "bt":F
    .restart local v13    # "i":I
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPath:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPoints:[F

    move-object/from16 v25, v0

    const/16 v26, 0x0

    aget v25, v25, v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/graphics/RectF;->width()F

    move-result v26

    mul-float v25, v25, v26

    add-float v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPoints:[F

    move-object/from16 v26, v0

    const/16 v27, 0x1

    aget v26, v26, v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltFrame:Landroid/graphics/RectF;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/RectF;->height()F

    move-result v27

    mul-float v26, v26, v27

    add-float v25, v25, v26

    invoke-virtual/range {v23 .. v25}, Landroid/graphics/Path;->lineTo(FF)V

    .line 387
    .end local v13    # "i":I
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBlinkingNeeded:Z

    move/from16 v23, v0

    if-nez v23, :cond_b

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPath:Landroid/graphics/Path;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mBoltPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 391
    :cond_b
    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->status:I

    move/from16 v23, v0

    const/16 v24, 0x3

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->plugType:I

    move/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_2

    .line 393
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWidth:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    const/high16 v24, 0x3f000000    # 0.5f

    mul-float v21, v23, v24

    .line 394
    .local v21, "x":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextHeight:F

    move/from16 v24, v0

    add-float v23, v23, v24

    const v24, 0x3ef5c28f    # 0.48f

    mul-float v22, v23, v24

    .line 395
    .local v22, "y":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mInvalidString:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mInvalidTextPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v21

    move/from16 v3, v22

    move-object/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 398
    .end local v6    # "bb":F
    .end local v7    # "bl":F
    .end local v8    # "br":F
    .end local v9    # "bt":F
    .end local v21    # "x":F
    .end local v22    # "y":F
    :cond_c
    const/16 v23, 0x4

    move/from16 v0, v23

    if-gt v14, v0, :cond_2

    .line 399
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWidth:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    const/high16 v24, 0x3f000000    # 0.5f

    mul-float v21, v23, v24

    .line 400
    .restart local v21    # "x":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mHeight:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextHeight:F

    move/from16 v24, v0

    add-float v23, v23, v24

    const v24, 0x3ef5c28f    # 0.48f

    mul-float v22, v23, v24

    .line 401
    .restart local v22    # "y":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningString:Ljava/lang/String;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextPaint:Landroid/graphics/Paint;

    move-object/from16 v24, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    move/from16 v2, v21

    move/from16 v3, v22

    move-object/from16 v4, v24

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_1
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    .line 148
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 149
    const-string v2, "PowerSavingBatteryMeterView"

    const-string v3, "onAttachedToWindow2"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 151
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mTracker:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    .line 153
    .local v1, "sticky":Landroid/content/Intent;
    if-eqz v1, :cond_0

    .line 155
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mTracker:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 157
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 161
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 162
    const-string v0, "PowerSavingBatteryMeterView"

    const-string v1, "onDetachedFromWindow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mTracker:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 164
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/high16 v2, 0x3f400000    # 0.75f

    .line 268
    iput p2, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mHeight:I

    .line 269
    iput p1, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWidth:I

    .line 270
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextPaint:Landroid/graphics/Paint;

    int-to-float v1, p2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 271
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mInvalidTextPaint:Landroid/graphics/Paint;

    int-to-float v1, p2

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 272
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mWarningTextHeight:F

    .line 273
    return-void
.end method
