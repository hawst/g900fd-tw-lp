.class Lcom/sec/android/emergencymode/util/TalkBackUtil$1;
.super Landroid/speech/tts/UtteranceProgressListener;
.source "TalkBackUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/util/TalkBackUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/util/TalkBackUtil;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/util/TalkBackUtil;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil$1;->this$0:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    invoke-direct {p0}, Landroid/speech/tts/UtteranceProgressListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDone(Ljava/lang/String;)V
    .locals 2
    .param p1, "utterId"    # Ljava/lang/String;

    .prologue
    .line 37
    const-string v0, "TTS_END"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    const-string v0, "TalkBackUtils"

    const-string v1, "TTS speaking is done!!!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil$1;->this$0:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->onDestroy()V

    .line 41
    :cond_0
    return-void
.end method

.method public onError(Ljava/lang/String;)V
    .locals 2
    .param p1, "utterId"    # Ljava/lang/String;

    .prologue
    .line 45
    const-string v0, "TTS_END"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const-string v0, "TalkBackUtils"

    const-string v1, "Error occured during TTS speaks!!!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil$1;->this$0:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->onDestroy()V

    .line 49
    :cond_0
    return-void
.end method

.method public onStart(Ljava/lang/String;)V
    .locals 2
    .param p1, "utterId"    # Ljava/lang/String;

    .prologue
    .line 53
    const-string v0, "TalkBackUtils"

    const-string v1, "TTS speaks now!!!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    return-void
.end method
