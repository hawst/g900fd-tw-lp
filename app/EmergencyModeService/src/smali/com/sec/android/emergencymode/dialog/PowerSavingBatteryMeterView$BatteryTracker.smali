.class Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;
.super Landroid/content/BroadcastReceiver;
.source "PowerSavingBatteryMeterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BatteryTracker"
.end annotation


# static fields
.field public static final UNKNOWN_LEVEL:I = -0x1


# instance fields
.field health:I

.field level:I

.field plugType:I

.field plugged:Z

.field status:I

.field technology:Ljava/lang/String;

.field temperature:I

.field final synthetic this$0:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;

.field voltage:I


# direct methods
.method private constructor <init>(Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;)V
    .locals 1

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->this$0:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->level:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;
    .param p2, "x1"    # Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$1;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;-><init>(Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 98
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, "action":Ljava/lang/String;
    const-string v6, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 101
    const/high16 v6, 0x42c80000    # 100.0f

    const-string v9, "level"

    invoke-virtual {p2, v9, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v6, v9

    const-string v9, "scale"

    const/16 v10, 0x64

    invoke-virtual {p2, v9, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v6, v9

    float-to-int v1, v6

    .line 105
    .local v1, "battLevel":I
    iget v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->level:I

    if-eq v6, v1, :cond_6

    move v3, v7

    .line 106
    .local v3, "levelChange":Z
    :goto_0
    if-eqz v3, :cond_0

    .line 107
    iput v1, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->level:I

    .line 109
    :cond_0
    iget v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->plugType:I

    const-string v9, "plugged"

    invoke-virtual {p2, v9, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    if-eq v6, v9, :cond_7

    move v4, v7

    .line 110
    .local v4, "plugTypeChange":Z
    :goto_1
    if-eqz v4, :cond_1

    .line 111
    const-string v6, "plugged"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->plugType:I

    .line 112
    :cond_1
    iget v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->plugType:I

    if-eqz v6, :cond_8

    move v6, v7

    :goto_2
    iput-boolean v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->plugged:Z

    .line 114
    iget v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->health:I

    const-string v9, "health"

    invoke-virtual {p2, v9, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    if-eq v6, v9, :cond_9

    move v2, v7

    .line 116
    .local v2, "healthChange":Z
    :goto_3
    if-eqz v2, :cond_2

    .line 117
    const-string v6, "health"

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->health:I

    .line 120
    :cond_2
    iget v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->status:I

    const-string v9, "status"

    invoke-virtual {p2, v9, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v9

    if-eq v6, v9, :cond_a

    move v5, v7

    .line 122
    .local v5, "statusChange":Z
    :goto_4
    if-eqz v5, :cond_3

    .line 123
    const-string v6, "status"

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->status:I

    .line 126
    :cond_3
    const-string v6, "technology"

    invoke-virtual {p2, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->technology:Ljava/lang/String;

    .line 127
    const-string v6, "voltage"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->voltage:I

    .line 128
    const-string v6, "temperature"

    invoke-virtual {p2, v6, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->temperature:I

    .line 130
    const-string v6, "PowerSavingBatteryMeterView"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACTION_BATTERY_CHANGED : level:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->level:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " status:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->status:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " health:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->health:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    if-nez v3, :cond_4

    if-nez v4, :cond_4

    if-nez v2, :cond_4

    if-eqz v5, :cond_5

    .line 132
    :cond_4
    iget-object v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->this$0:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;

    const v8, 0x7f09001e

    invoke-virtual {p1, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 133
    const-string v6, "PowerSavingBatteryMeterView"

    const-string v8, "setContentDescription"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->this$0:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;

    # getter for: Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mPostInvalidateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->access$000(Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-nez v6, :cond_5

    .line 137
    iget-object v6, p0, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView$BatteryTracker;->this$0:Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;

    # getter for: Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->mPostInvalidateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;->access$000(Lcom/sec/android/emergencymode/dialog/PowerSavingBatteryMeterView;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 141
    .end local v1    # "battLevel":I
    .end local v2    # "healthChange":Z
    .end local v3    # "levelChange":Z
    .end local v4    # "plugTypeChange":Z
    .end local v5    # "statusChange":Z
    :cond_5
    return-void

    .restart local v1    # "battLevel":I
    :cond_6
    move v3, v8

    .line 105
    goto/16 :goto_0

    .restart local v3    # "levelChange":Z
    :cond_7
    move v4, v8

    .line 109
    goto/16 :goto_1

    .restart local v4    # "plugTypeChange":Z
    :cond_8
    move v6, v8

    .line 112
    goto/16 :goto_2

    :cond_9
    move v2, v8

    .line 114
    goto/16 :goto_3

    .restart local v2    # "healthChange":Z
    :cond_a
    move v5, v8

    .line 120
    goto/16 :goto_4
.end method
