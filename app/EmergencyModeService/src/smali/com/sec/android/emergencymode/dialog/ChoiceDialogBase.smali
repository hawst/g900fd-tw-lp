.class public Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;
.super Landroid/app/AlertDialog$Builder;
.source "ChoiceDialogBase.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "ChoiceDialogBase"


# instance fields
.field protected final isDocomoFeature:Z

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 35
    iput-object p1, p0, Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;->mContext:Landroid/content/Context;

    .line 36
    const-string v1, "ro.csc.sales_code"

    const-string v2, "unknown"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "salesCode":Ljava/lang/String;
    const-string v1, "DCM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;->isDocomoFeature:Z

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;->isDocomoFeature:Z

    goto :goto_0
.end method


# virtual methods
.method public create()Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "EmergencyChoiceDialog.create(): use show() instead.."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public show()Landroid/app/AlertDialog;
    .locals 4

    .prologue
    .line 51
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 52
    .local v0, "dialog":Landroid/app/AlertDialog;
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;->mContext:Landroid/content/Context;

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    .line 54
    .local v1, "kgm":Landroid/app/KeyguardManager;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 55
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d9

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    .line 58
    :goto_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1120010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 59
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 62
    :cond_0
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 64
    return-object v0

    .line 57
    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d8

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    goto :goto_0
.end method
