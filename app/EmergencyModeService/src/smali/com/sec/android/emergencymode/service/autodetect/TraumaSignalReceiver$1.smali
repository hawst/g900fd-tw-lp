.class Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver$1;
.super Ljava/lang/Object;
.source "TraumaSignalReceiver.java"

# interfaces
.implements Lcom/samsung/android/contextaware/manager/ContextAwareListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver$1;->this$0:Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContextChanged(ILandroid/os/Bundle;)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "context"    # Landroid/os/Bundle;

    .prologue
    .line 63
    const-string v1, "TraumaSignalReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "### onContextChanged() - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    sget v1, Lcom/samsung/android/contextaware/ContextAwareManager;->CARE_GIVER_SERVICE:I

    if-ne p1, v1, :cond_0

    .line 66
    const-string v1, "UserStatus"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 67
    .local v0, "userStatus":I
    const-string v1, "TraumaSignalReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "user status:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    packed-switch v0, :pswitch_data_0

    .line 80
    .end local v0    # "userStatus":I
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 72
    .restart local v0    # "userStatus":I
    :pswitch_1
    const-string v1, "TraumaSignalReceiver"

    const-string v2, "Trauma Signal Received."

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver$1;->this$0:Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;

    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->unReadyReceiver()V

    .line 74
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver$1;->this$0:Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;

    # getter for: Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->access$000(Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;)Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    move-result-object v1

    const-string v2, "TRAUMA"

    invoke-interface {v1, v2}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;->onDetected(Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
