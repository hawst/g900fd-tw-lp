.class public Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;
.super Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;
.source "LauncherChanger.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LauncherChanger"


# instance fields
.field private mPM:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;-><init>(Landroid/content/Context;)V

    .line 44
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mPM:Landroid/content/pm/PackageManager;

    .line 45
    return-void
.end method

.method public static getAllLauncherList(Landroid/content/Context;)Ljava/util/List;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 142
    .local v1, "pm":Landroid/content/pm/PackageManager;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v0, "homeActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getHomeActivities(Ljava/util/List;)Landroid/content/ComponentName;

    .line 144
    const-string v2, "LauncherChanger"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAllLauncherList size : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    return-object v0
.end method

.method private getDefaultLauncher()Landroid/content/ComponentName;
    .locals 5

    .prologue
    .line 128
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v1, "homeActivities":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/pm/ResolveInfo;>;"
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mPM:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getHomeActivities(Ljava/util/List;)Landroid/content/ComponentName;

    move-result-object v0

    .line 131
    .local v0, "currentDefaultHome":Landroid/content/ComponentName;
    if-eqz v0, :cond_0

    .line 132
    const-string v2, "LauncherChanger"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Current default home is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :goto_0
    return-object v0

    .line 134
    :cond_0
    const-string v2, "LauncherChanger"

    const-string v3, "Current default home is: null"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setApplicationEnabled(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    .line 195
    if-eqz p2, :cond_0

    const/4 v1, 0x1

    .line 197
    .local v1, "flag":I
    :goto_0
    if-eqz p1, :cond_1

    .line 198
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mPM:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v1, v3}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 205
    :goto_1
    return-void

    .line 195
    .end local v1    # "flag":I
    :cond_0
    const/4 v1, 0x2

    goto :goto_0

    .line 200
    .restart local v1    # "flag":I
    :cond_1
    const-string v2, "LauncherChanger"

    const-string v3, "launcher pkgName is null"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "LauncherChanger"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setApplicationEnabled e : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private setDefaultLauncher(Landroid/content/ComponentName;Ljava/util/ArrayList;)V
    .locals 13
    .param p1, "component"    # Landroid/content/ComponentName;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ComponentName;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    .local p2, "disablePkgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 96
    const-string v10, "LauncherChanger"

    const-string v11, "setDefaultLauncher pkgName: null"

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    :goto_0
    return-void

    .line 101
    :cond_0
    new-instance v9, Landroid/content/IntentFilter;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v9, v10}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 102
    .local v9, "mHomeFilter":Landroid/content/IntentFilter;
    const-string v10, "android.intent.category.HOME"

    invoke-virtual {v9, v10}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 103
    const-string v10, "android.intent.category.DEFAULT"

    invoke-virtual {v9, v10}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 105
    iget-object v10, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->getAllLauncherList(Landroid/content/Context;)Ljava/util/List;

    move-result-object v7

    .line 106
    .local v7, "lcList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    new-array v8, v10, [Landroid/content/ComponentName;

    .line 108
    .local v8, "mHomeComponentSet":[Landroid/content/ComponentName;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v10

    if-ge v4, v10, :cond_1

    .line 109
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 110
    .local v1, "candidate":Landroid/content/pm/ResolveInfo;
    iget-object v6, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 111
    .local v6, "info":Landroid/content/pm/ActivityInfo;
    new-instance v0, Landroid/content/ComponentName;

    iget-object v10, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v11, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v10, v11}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .local v0, "activityName":Landroid/content/ComponentName;
    aput-object v0, v8, v4

    .line 108
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 116
    .end local v0    # "activityName":Landroid/content/ComponentName;
    .end local v1    # "candidate":Landroid/content/pm/ResolveInfo;
    .end local v6    # "info":Landroid/content/pm/ActivityInfo;
    :cond_1
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mPM:Landroid/content/pm/PackageManager;

    const/high16 v11, 0x100000

    invoke-virtual {v10, v9, v11, v8, p1}, Landroid/content/pm/PackageManager;->replacePreferredActivity(Landroid/content/IntentFilter;I[Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    .line 118
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 119
    .local v2, "disablePkg":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mPM:Landroid/content/pm/PackageManager;

    invoke-virtual {v10, v2}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 121
    .end local v2    # "disablePkg":Ljava/lang/String;
    .end local v5    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v3

    .line 122
    .local v3, "e":Ljava/lang/Exception;
    const-string v10, "LauncherChanger"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "setDefaultLauncher e : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    const-string v10, "LauncherChanger"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "New default home is: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public changeDefaultApp(Z)V
    .locals 13
    .param p1, "req"    # Z

    .prologue
    .line 49
    const/4 v8, 0x0

    .line 50
    .local v8, "startClassName":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .local v3, "enablePkgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 53
    .local v1, "disablePkgs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    .line 54
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->getDefaultLauncher()Landroid/content/ComponentName;

    move-result-object v6

    .line 55
    .local v6, "oldDefaultLauncher":Landroid/content/ComponentName;
    if-eqz v6, :cond_0

    .line 56
    const-string v10, "DEFAULT_LAUNCHER"

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const-string v10, "DEFAULT_LAUNCHER_CLASS"

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    :cond_0
    const-string v9, "com.sec.android.emergencylauncher"

    .line 60
    .local v9, "startPkgName":Ljava/lang/String;
    const-string v8, "com.sec.android.emergencylauncher.LauncherActivity"

    .line 66
    .end local v6    # "oldDefaultLauncher":Landroid/content/ComponentName;
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->getEnableLauncherList(Z)Ljava/util/ArrayList;

    move-result-object v3

    .line 67
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 68
    .local v2, "enablePkg":Ljava/lang/String;
    const-string v10, "LauncherChanger"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "enable necessary launcher : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const/4 v10, 0x1

    invoke-direct {p0, v2, v10}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->setApplicationEnabled(Ljava/lang/String;Z)V

    goto :goto_1

    .line 62
    .end local v2    # "enablePkg":Ljava/lang/String;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v9    # "startPkgName":Ljava/lang/String;
    :cond_1
    const-string v10, "DEFAULT_LAUNCHER"

    invoke-virtual {p0, v10}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 63
    .restart local v9    # "startPkgName":Ljava/lang/String;
    const-string v10, "DEFAULT_LAUNCHER_CLASS"

    invoke-virtual {p0, v10}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 72
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_2
    const/4 v7, 0x0

    .line 73
    .local v7, "reqComp":Landroid/content/ComponentName;
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 74
    .local v5, "launcherIntent":Landroid/content/Intent;
    const-string v10, "android.intent.action.MAIN"

    invoke-virtual {v5, v10}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const/high16 v10, 0x10000000

    invoke-virtual {v5, v10}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 76
    const-string v10, "android.intent.category.HOME"

    invoke-virtual {v5, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    if-eqz v9, :cond_3

    if-eqz v8, :cond_3

    .line 78
    invoke-virtual {v5, v9, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 79
    new-instance v7, Landroid/content/ComponentName;

    .end local v7    # "reqComp":Landroid/content/ComponentName;
    invoke-direct {v7, v9, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .restart local v7    # "reqComp":Landroid/content/ComponentName;
    :goto_2
    iget-object v10, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mContext:Landroid/content/Context;

    new-instance v11, Landroid/os/UserHandle;

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v12

    invoke-direct {v11, v12}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v10, v5, v11}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 85
    if-nez p1, :cond_4

    const/4 v10, 0x1

    :goto_3
    invoke-virtual {p0, v10}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->getEnableLauncherList(Z)Ljava/util/ArrayList;

    move-result-object v1

    .line 86
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    .local v0, "disablePkg":Ljava/lang/String;
    const-string v10, "LauncherChanger"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "disable unnecessary launcher : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const/4 v10, 0x0

    invoke-direct {p0, v0, v10}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->setApplicationEnabled(Ljava/lang/String;Z)V

    goto :goto_4

    .line 81
    .end local v0    # "disablePkg":Ljava/lang/String;
    :cond_3
    const-string v10, "LauncherChanger"

    const-string v11, "Saved default home package was null!"

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 85
    :cond_4
    const/4 v10, 0x0

    goto :goto_3

    .line 91
    :cond_5
    invoke-direct {p0, v7, v1}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->setDefaultLauncher(Landroid/content/ComponentName;Ljava/util/ArrayList;)V

    .line 92
    return-void
.end method

.method protected getAllLauncherListWithDisabledPackage()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.intent.action.MAIN"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 150
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "android.intent.category.HOME"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 151
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mPM:Landroid/content/pm/PackageManager;

    const/16 v6, 0x280

    invoke-virtual {v5, v1, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v4

    .line 155
    .local v4, "rList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v2, "launchers":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 157
    .local v3, "r":Landroid/content/pm/ResolveInfo;
    iget v5, v3, Landroid/content/pm/ResolveInfo;->priority:I

    if-gtz v5, :cond_0

    .line 159
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 162
    .end local v3    # "r":Landroid/content/pm/ResolveInfo;
    :cond_1
    const-string v5, "LauncherChanger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getAllLauncherListWithDisabledPackage size : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    return-object v2
.end method

.method public getEnableLauncherList(Z)Ljava/util/ArrayList;
    .locals 10
    .param p1, "req"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 169
    .local v6, "ret":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_1

    .line 170
    const-string v7, "com.sec.android.emergencylauncher"

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    :cond_0
    return-object v6

    .line 172
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->getAllLauncherListWithDisabledPackage()Ljava/util/List;

    move-result-object v4

    .line 173
    .local v4, "lcList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 174
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 175
    .local v0, "candidate":Landroid/content/pm/ResolveInfo;
    iget-object v3, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 176
    .local v3, "info":Landroid/content/pm/ActivityInfo;
    iget-object v5, v3, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 178
    .local v5, "pkgName":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencyFactory;

    move-result-object v1

    .line 179
    .local v1, "emFac":Lcom/sec/android/emergencymode/service/EmergencyFactory;
    const-string v7, "com.sec.android.emergencylauncher"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    const/4 v7, 0x0

    const/4 v8, 0x1

    invoke-virtual {v1, v5, v7, v8}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkValidPackage(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v7

    if-nez v7, :cond_3

    .line 181
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 182
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 185
    :cond_3
    const-string v7, "LauncherChanger"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getEnableLauncherList excepted : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
