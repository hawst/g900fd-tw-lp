.class Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;
.super Ljava/lang/Object;
.source "EmergencyDialogHandler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

.field final synthetic val$reqType:Z


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;Z)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    iput-boolean p2, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;->val$reqType:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 145
    sget v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->DISMISS_CAUSE:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 146
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    invoke-static {v0}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$200(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;->val$reqType:Z

    invoke-interface {v0, v1}, Lcom/sec/android/emergencymode/service/EmergencyActionListener;->onAccept(Z)V

    .line 151
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mUpsmChoiceDialog:Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;
    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$502(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    .line 152
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$000(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$400(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 153
    const-string v0, "EmergencyDialogHandler"

    const-string v1, "onDismiss called."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    return-void

    .line 147
    :cond_1
    sget v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->DISMISS_CAUSE:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 148
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$3;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    invoke-static {v0}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$200(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/emergencymode/service/EmergencyActionListener;->onCancel()V

    .line 149
    const-string v0, "EmergencyDialogHandler"

    const-string v1, "Choice Dialog is canceled by user."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
