.class public Lcom/sec/android/emergencymode/service/EmergencySettingManager;
.super Lcom/sec/android/emergencymode/service/EmergencyBase;
.source "EmergencySettingManager.java"


# static fields
.field public static final ANDROID_BEAM_STATE:Ljava/lang/String; = "ANDROID_BEAM_STATE"

.field public static final ANIMATION_DURATION:Ljava/lang/String; = "ANIMATION_DURATION"

.field public static final ANIMATION_TRANSITION:Ljava/lang/String; = "ANIMATION_TRANSITION"

.field public static final ANIMATION_WINDOW:Ljava/lang/String; = "ANIMATION_WINDOW"

.field public static final AUTOSYNC_STATE:Ljava/lang/String; = "AUTOSYNC_STATE"

.field public static final AUTO_ROTATION_STATE:Ljava/lang/String; = "AUTO_ROTATION_STATE"

.field public static final AUTO_ROTATION_STATE_FIRST:Ljava/lang/String; = "AUTO_ROTATION_STATE_FIRST"

.field public static final AUTO_ROTATION_STATE_SECOND:Ljava/lang/String; = "AUTO_ROTATION_STATE_SECOND"

.field public static final BLUTOOTH_STATE:Ljava/lang/String; = "BLUTOOTH_STATE"

.field public static final LOCATION_STATE:Ljava/lang/String; = "LOCATION_STATE"

.field public static final MOBILE_DATA:Ljava/lang/String; = "MOBILE_DATA"

.field public static final NFC_STATE:Ljava/lang/String; = "NFC_STATE"

.field public static final ROTATION_STATE:Ljava/lang/String; = "ROTATION_STATE"

.field public static final STREAM_RING:Ljava/lang/String; = "STREAM_RING"

.field private static final TAG:Ljava/lang/String; = "EmergencySettingManager"

.field public static final WIFI_STATE:Ljava/lang/String; = "WIFI_STATE"

.field private static instance:Lcom/sec/android/emergencymode/service/EmergencySettingManager;


# instance fields
.field protected mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

.field protected mLocationUtility:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

.field private mPrefSettingsList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mProcessUtility:Lcom/sec/android/emergencymode/service/settingutils/ProcessUtility;

.field protected mScreenCtrl:Lcom/sec/android/emergencymode/service/EmergencyScreenController;

.field protected mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyBase;-><init>(Landroid/content/Context;)V

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    .line 83
    new-instance v0, Lcom/sec/android/emergencymode/service/EmergencyScreenController;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/emergencymode/service/EmergencyScreenController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mScreenCtrl:Lcom/sec/android/emergencymode/service/EmergencyScreenController;

    .line 84
    new-instance v0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mLocationUtility:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    .line 85
    new-instance v0, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    .line 86
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    .line 87
    new-instance v0, Lcom/sec/android/emergencymode/service/settingutils/ProcessUtility;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/emergencymode/service/settingutils/ProcessUtility;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mProcessUtility:Lcom/sec/android/emergencymode/service/settingutils/ProcessUtility;

    .line 88
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencySettingManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    const-class v1, Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    monitor-enter v1

    .line 73
    :try_start_0
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->instance:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    if-nez v0, :cond_0

    .line 74
    const-string v0, "EmergencySettingManager"

    const-string v2, "make instance"

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    new-instance v0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->instance:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    .line 77
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->instance:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    return-object v0

    .line 77
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected adjustLocationInfoAndConnectivity()V
    .locals 2

    .prologue
    .line 332
    const-string v0, "EmergencySettingManager"

    const-string v1, "adjustLocationInfoAndConnectivity"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const/16 v0, 0x600

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->checkModeType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 334
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mLocationUtility:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->stopLocationInfo()V

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->needDataTrigger(Z)V

    .line 337
    const/16 v0, 0x440

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->checkModeType(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->isWifiConnected()Z

    move-result v0

    if-nez v0, :cond_4

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1120044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1120045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 339
    :cond_2
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setWifiState(Z)V

    .line 343
    :cond_3
    :goto_0
    return-void

    .line 341
    :cond_4
    const-string v0, "EmergencySettingManager"

    const-string v1, "keep wifi connection due to FMM using wifi module"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected getPrefBoolean(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pref"    # Ljava/lang/String;

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected getPrefDouble(Ljava/lang/String;)D
    .locals 2
    .param p1, "pref"    # Ljava/lang/String;

    .prologue
    .line 131
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    return-wide v0
.end method

.method protected getPrefFloat(Ljava/lang/String;)F
    .locals 1
    .param p1, "pref"    # Ljava/lang/String;

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method protected getPrefInt(Ljava/lang/String;)I
    .locals 1
    .param p1, "pref"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method protected getPrefLong(Ljava/lang/String;)J
    .locals 2
    .param p1, "pref"    # Ljava/lang/String;

    .prologue
    .line 127
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected getPrefValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "pref"    # Ljava/lang/String;

    .prologue
    .line 96
    const/4 v7, 0x0

    .line 97
    .local v7, "ret":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "ret":Ljava/lang/String;
    check-cast v7, Ljava/lang/String;

    .line 115
    .restart local v7    # "ret":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v7

    .line 100
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "pref=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 101
    .local v3, "selection":Ljava/lang/String;
    const/4 v6, 0x0

    .line 103
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_PREFSETTINGS:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 104
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    .line 105
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 106
    const-string v0, "value"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 107
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_2
    if-eqz v6, :cond_0

    .line 112
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 111
    if-eqz v6, :cond_0

    .line 112
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 111
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 112
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected isPrefContains(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pref"    # Ljava/lang/String;

    .prologue
    .line 91
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public sendSettingBroadcast(Z)V
    .locals 10
    .param p1, "restore"    # Z

    .prologue
    const/4 v9, -0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 347
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.settings.DORMANTMODE_SWITCH_CHANGED"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v8, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v4, v7, v8}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 348
    if-nez p1, :cond_0

    .line 350
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.settings.POWERSAVINGMODE_CHANGED"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v8, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v4, v7, v8}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 353
    :cond_0
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "smart_pause"

    invoke-static {v4, v7, v6, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    .line 354
    .local v3, "smart_pause_mode":I
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.SMART_PAUSE_CHANGED"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 355
    .local v2, "smart_pause_changed":Landroid/content/Intent;
    const-string v7, "isEnable"

    if-ne v3, v5, :cond_1

    move v4, v5

    :goto_0
    invoke-virtual {v2, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 356
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    sget-object v7, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v4, v2, v7}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 358
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v7, "air_motion_wake_up"

    invoke-static {v4, v7, v6, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 359
    .local v1, "air_motion_mode":I
    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.sec.gesture.AIR_WAKE_UP_SETTINGS_CHANGED"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 360
    .local v0, "air_motion_changed":Landroid/content/Intent;
    const-string v4, "isEnable"

    if-ne v1, v5, :cond_2

    :goto_1
    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 361
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    sget-object v5, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 362
    return-void

    .end local v0    # "air_motion_changed":Landroid/content/Intent;
    .end local v1    # "air_motion_mode":I
    :cond_1
    move v4, v6

    .line 355
    goto :goto_0

    .restart local v0    # "air_motion_changed":Landroid/content/Intent;
    .restart local v1    # "air_motion_mode":I
    :cond_2
    move v5, v6

    .line 360
    goto :goto_1
.end method

.method protected setEmergencySettings(Z)V
    .locals 14
    .param p1, "enabled"    # Z

    .prologue
    .line 139
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 140
    .local v6, "resolver":Landroid/content/ContentResolver;
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    const-string v9, "audio"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 141
    .local v0, "audioMgr":Landroid/media/AudioManager;
    new-instance v7, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;-><init>(Landroid/content/Context;)V

    .line 143
    .local v7, "settingDepot":Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;
    if-eqz p1, :cond_2

    .line 145
    invoke-virtual {v7}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;->getOriginalDatabases()Ljava/util/HashMap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    .line 147
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "WIFI_STATE"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->getWifiState()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 148
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "BLUTOOTH_STATE"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->getBluetoothState()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "ANDROID_BEAM_STATE"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->getAndroidBeamState()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "NFC_STATE"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->getNfcState()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "AUTOSYNC_STATE"

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "AUTO_ROTATION_STATE"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getAutoRotation()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v8}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->isSupportFolderType()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 155
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "AUTO_ROTATION_STATE_FIRST"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getAutoRotationFirst()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "AUTO_ROTATION_STATE_SECOND"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getAutoRotationSecond()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    :cond_0
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "ROTATION_STATE"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getCurrentRotation()I

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "ANIMATION_WINDOW"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getAnimationScale(I)F

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "ANIMATION_TRANSITION"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getAnimationScale(I)F

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mLocationUtility:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    invoke-virtual {v8}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->getLastLocation()Landroid/location/Location;

    move-result-object v5

    .line 163
    .local v5, "loc":Landroid/location/Location;
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v10, "GPS_LATITUDE"

    if-eqz v5, :cond_b

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v8

    :goto_0
    invoke-virtual {v9, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v10, "GPS_LONGITUDE"

    if-eqz v5, :cond_c

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v8

    :goto_1
    invoke-virtual {v9, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v10, "GPS_ACCURACY"

    if-eqz v5, :cond_d

    invoke-virtual {v5}, Landroid/location/Location;->getAccuracy()F

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v8

    :goto_2
    invoke-virtual {v9, v10, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    const/16 v8, 0x600

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->checkModeType(I)Z

    move-result v8

    if-nez v8, :cond_e

    .line 173
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "zen_mode"

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 174
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "MOBILE_DATA"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->getMobileDataState()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const/4 v8, 0x2

    invoke-virtual {v0, v8}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 183
    :cond_1
    :goto_3
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;->requestBackUpDatabase(Ljava/util/HashMap;Z)I

    .line 187
    .end local v5    # "loc":Landroid/location/Location;
    :cond_2
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    invoke-virtual {v8}, Ljava/util/HashMap;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ge v8, v9, :cond_3

    .line 188
    invoke-virtual {v7}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;->getBackUpDatabase()Ljava/util/HashMap;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    .line 192
    :cond_3
    if-eqz p1, :cond_11

    .line 194
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mScreenCtrl:Lcom/sec/android/emergencymode/service/EmergencyScreenController;

    invoke-virtual {v8, p1}, Lcom/sec/android/emergencymode/service/EmergencyScreenController;->setEmergencyScreen(Z)V

    .line 196
    const/16 v8, 0x600

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->checkModeType(I)Z

    move-result v8

    if-eqz v8, :cond_f

    .line 198
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mLocationUtility:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->setLocationState(I)V

    .line 199
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x1120044

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x1120045

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 200
    :cond_4
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setWifiState(Z)V

    .line 215
    :cond_5
    :goto_4
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setWifiApState(Z)V

    .line 216
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setWifiIBSSEnabled(Z)V

    .line 218
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setAndroidBeamState(Z)V

    .line 220
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setNfcState(Z)V

    .line 257
    :cond_6
    :goto_5
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setWifiDisplayState(Z)V

    .line 260
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setUSBTether(Z)V

    .line 263
    const-string v8, "AUTO_ROTATION_STATE"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 264
    if-eqz p1, :cond_17

    const/4 v1, 0x0

    .line 265
    .local v1, "autoRotation":Z
    :goto_6
    const/4 v4, 0x0

    .line 266
    .local v4, "freeze":I
    if-nez p1, :cond_7

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v8}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->shouldRestoreRotation()Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_7

    .line 267
    const-string v8, "ROTATION_STATE"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefInt(Ljava/lang/String;)I

    move-result v4

    .line 269
    :cond_7
    const-string v8, "EmergencySettingManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setAutoRotation : autoRotation = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", freeze = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v8, v1, v4}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->setAutoRotation(ZI)V

    .line 271
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v8}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->isSupportFolderType()Z

    move-result v8

    if-eqz v8, :cond_8

    .line 272
    if-eqz p1, :cond_18

    const/4 v2, 0x0

    .line 273
    .local v2, "autoRotationFirst":Z
    :goto_7
    if-eqz p1, :cond_19

    const/4 v3, 0x0

    .line 274
    .local v3, "autoRotationSecond":Z
    :goto_8
    const-string v8, "EmergencySettingManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setAutoRotationFirst : autoRotationFirst = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    const-string v8, "EmergencySettingManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setAutoRotationSecond : autoRotationSecond = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v8, v2}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->setAutoRotationFirst(Z)V

    .line 277
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v8, v3}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->setAutoRotationSecond(Z)V

    .line 280
    .end local v1    # "autoRotation":Z
    .end local v2    # "autoRotationFirst":Z
    .end local v3    # "autoRotationSecond":Z
    .end local v4    # "freeze":I
    :cond_8
    const-string v8, "ANIMATION_WINDOW"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_9

    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    const/4 v10, 0x0

    if-eqz p1, :cond_1a

    const/4 v8, 0x0

    :goto_9
    invoke-virtual {v9, v10, v8}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->setAnimationScale(IF)V

    .line 281
    :cond_9
    const-string v8, "ANIMATION_TRANSITION"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_a

    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    const/4 v10, 0x1

    if-eqz p1, :cond_1b

    const/4 v8, 0x0

    :goto_a
    invoke-virtual {v9, v10, v8}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->setAnimationScale(IF)V

    .line 284
    :cond_a
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mWindowUtility:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-virtual {v8, p1}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->adjustFrameRate(Z)V

    .line 286
    if-eqz p1, :cond_1c

    .line 287
    invoke-virtual {v7}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;->setEmergencyDatabase()V

    .line 291
    :goto_b
    return-void

    .line 163
    .restart local v5    # "loc":Landroid/location/Location;
    :cond_b
    const-string v8, ""

    goto/16 :goto_0

    .line 164
    :cond_c
    const-string v8, ""

    goto/16 :goto_1

    .line 165
    :cond_d
    const-string v8, ""

    goto/16 :goto_2

    .line 179
    :cond_e
    const-string v8, "JPN"

    const-string v9, "OPEN"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 180
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    const-string v9, "MOBILE_DATA"

    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->getMobileDataState()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 205
    .end local v5    # "loc":Landroid/location/Location;
    :cond_f
    const-string v8, "JPN"

    const-string v9, "OPEN"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 206
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "toddler_mode_switch"

    const-string v10, "0"

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 209
    :cond_10
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setMobileDataState(Z)V

    .line 210
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setWifiState(Z)V

    .line 211
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mLocationUtility:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    invoke-virtual {v8}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->startLocationInfo()V

    goto/16 :goto_4

    .line 225
    :cond_11
    const/16 v8, 0x600

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->checkModeType(I)Z

    move-result v8

    if-nez v8, :cond_12

    .line 226
    const-string v8, "JPN"

    const-string v9, "OPEN"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 227
    const-string v8, "toddler_mode_switch"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 228
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "toddler_mode_switch"

    const-string v10, "toddler_mode_switch"

    invoke-virtual {p0, v10}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 234
    :cond_12
    const-string v8, "WIFI_STATE"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_13

    .line 235
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const-string v9, "WIFI_STATE"

    invoke-virtual {p0, v9}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setWifiState(Z)V

    .line 239
    :cond_13
    const-string v8, "MOBILE_DATA"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_14

    .line 240
    const-string v8, "EmergencySettingManager"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Pref Data : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "MOBILE_DATA"

    invoke-virtual {p0, v10}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const-string v9, "MOBILE_DATA"

    invoke-virtual {p0, v9}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setMobileDataState(Z)V

    .line 248
    :cond_14
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mLocationUtility:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    invoke-virtual {v8}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->unregisterLocationListener()V

    .line 250
    const-string v8, "AUTOSYNC_STATE"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_15

    const-string v8, "AUTOSYNC_STATE"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-static {v8}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    .line 252
    :cond_15
    const-string v8, "NFC_STATE"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_16

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const-string v9, "NFC_STATE"

    invoke-virtual {p0, v9}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setNfcState(Z)V

    .line 254
    :cond_16
    const-string v8, "ANDROID_BEAM_STATE"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const-string v9, "ANDROID_BEAM_STATE"

    invoke-virtual {p0, v9}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setAndroidBeamState(Z)V

    goto/16 :goto_5

    .line 264
    :cond_17
    const-string v8, "AUTO_ROTATION_STATE"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v1

    goto/16 :goto_6

    .line 272
    .restart local v1    # "autoRotation":Z
    .restart local v4    # "freeze":I
    :cond_18
    const-string v8, "AUTO_ROTATION_STATE_FIRST"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v2

    goto/16 :goto_7

    .line 273
    .restart local v2    # "autoRotationFirst":Z
    :cond_19
    const-string v8, "AUTO_ROTATION_STATE_SECOND"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v3

    goto/16 :goto_8

    .line 280
    .end local v1    # "autoRotation":Z
    .end local v2    # "autoRotationFirst":Z
    .end local v4    # "freeze":I
    :cond_1a
    const-string v8, "ANIMATION_WINDOW"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefFloat(Ljava/lang/String;)F

    move-result v8

    goto/16 :goto_9

    .line 281
    :cond_1b
    const-string v8, "ANIMATION_TRANSITION"

    invoke-virtual {p0, v8}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefFloat(Ljava/lang/String;)F

    move-result v8

    goto/16 :goto_a

    .line 289
    :cond_1c
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    invoke-virtual {v7, v8}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;->setRestoreDatabase(Ljava/util/HashMap;)V

    goto/16 :goto_b
.end method

.method protected setLastDisableSetting()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 295
    invoke-static {v8}, Landroid/content/ContentResolver;->setMasterSyncAutomatically(Z)V

    .line 297
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    invoke-virtual {v5, v8}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setBluetoothState(Z)V

    .line 300
    const/16 v5, 0x600

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->checkModeType(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 301
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    const-string v6, "audio"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 302
    .local v0, "audioMgr":Landroid/media/AudioManager;
    invoke-virtual {v0, v9}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v2

    .line 303
    .local v2, "maxVolume":I
    invoke-virtual {v0, v9}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v1

    .line 304
    .local v1, "curVol":I
    const-string v5, "EmergencySettingManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "curVol : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    invoke-virtual {v0, v9, v2, v8}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 308
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 309
    .local v4, "volPrefSet":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v5, "STREAM_RING"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    new-instance v3, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;

    iget-object v5, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;-><init>(Landroid/content/Context;)V

    .line 311
    .local v3, "settingDepot":Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;
    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;->requestBackUpDatabase(Ljava/util/HashMap;Z)I

    .line 314
    .end local v0    # "audioMgr":Landroid/media/AudioManager;
    .end local v1    # "curVol":I
    .end local v2    # "maxVolume":I
    .end local v3    # "settingDepot":Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;
    .end local v4    # "volPrefSet":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_0
    return-void
.end method

.method protected setLastEnableSetting()V
    .locals 5

    .prologue
    .line 318
    const-string v2, "STREAM_RING"

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 319
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 320
    .local v0, "audioMgr":Landroid/media/AudioManager;
    const/4 v2, 0x2

    const-string v3, "STREAM_RING"

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefInt(Ljava/lang/String;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 325
    .end local v0    # "audioMgr":Landroid/media/AudioManager;
    :cond_0
    const-string v2, "BLUTOOTH_STATE"

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->isPrefContains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mConnectivityUtility:Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    const-string v3, "BLUTOOTH_STATE"

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getPrefBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->setBluetoothState(Z)V

    .line 327
    :cond_1
    new-instance v1, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;-><init>(Landroid/content/Context;)V

    .line 328
    .local v1, "settingDepot":Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mPrefSettingsList:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/service/settingutils/depot/SettingDatabaseDepot;->setLastEnableSetting(Ljava/util/HashMap;)V

    .line 329
    return-void
.end method
