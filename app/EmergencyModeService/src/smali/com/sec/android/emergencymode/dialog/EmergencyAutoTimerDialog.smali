.class public Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;
.super Landroid/app/AlertDialog$Builder;
.source "EmergencyAutoTimerDialog.java"


# static fields
.field public static DISMISS_CAUSE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "EmergencyAutoTimerDialog"


# instance fields
.field private dialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private mIsDarkTheme:Z

.field private mIsTablet:Z

.field private mSandIcon:Landroid/widget/ImageView;

.field private mTalkbackUtil:Lcom/sec/android/emergencymode/util/TalkBackUtil;

.field private mTextColor:I

.field private mTimerHandler:Landroid/os/Handler;

.field private mVibrator:Landroid/os/Vibrator;

.field private remainTime:Landroid/widget/TextView;

.field private reqTime:I

.field private time:I

.field private warningMsg:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->DISMISS_CAUSE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I
    .param p3, "isTablet"    # Z

    .prologue
    .line 62
    const v0, 0x7f09000f

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x3c

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;-><init>(Landroid/content/Context;Ljava/lang/String;IIZ)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIZ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "warningText"    # Ljava/lang/String;
    .param p3, "expiredTime"    # I
    .param p4, "theme"    # I
    .param p5, "isTablet"    # Z

    .prologue
    const/4 v3, 0x0

    .line 72
    invoke-direct {p0, p1, p4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 162
    new-instance v2, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;

    invoke-direct {v2, p0}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$2;-><init>(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)V

    iput-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTimerHandler:Landroid/os/Handler;

    .line 73
    const/4 v2, 0x4

    if-eq p4, v2, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mIsDarkTheme:Z

    .line 74
    iput-object p1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mContext:Landroid/content/Context;

    .line 75
    iput-boolean p5, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mIsTablet:Z

    .line 76
    iput p3, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I

    .line 77
    iput p3, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->reqTime:I

    .line 78
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/util/TalkBackUtil;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTalkbackUtil:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    .line 79
    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 80
    sput v3, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->DISMISS_CAUSE:I

    .line 82
    invoke-direct {p0}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->setTextColor()V

    .line 83
    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 85
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030005

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 86
    .local v0, "content":Landroid/view/View;
    const v2, 0x7f0b0015

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->remainTime:Landroid/widget/TextView;

    .line 87
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->remainTime:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTextColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 88
    const v2, 0x7f0b0013

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->warningMsg:Landroid/widget/TextView;

    .line 89
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->warningMsg:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTextColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 90
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->remainTime:Landroid/widget/TextView;

    iget v3, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I

    invoke-direct {p0, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->getTimeString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->warningMsg:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mIsDarkTheme:Z

    if-nez v2, :cond_0

    .line 93
    const v2, 0x7f0b0014

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mSandIcon:Landroid/widget/ImageView;

    .line 94
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mSandIcon:Landroid/widget/ImageView;

    const v3, 0x7f02004e

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 97
    :cond_0
    const v2, 0x7f09003a

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 98
    const v2, 0x1040009

    new-instance v3, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$1;

    invoke-direct {v3, p0}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog$1;-><init>(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)V

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 106
    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 107
    return-void

    .line 73
    .end local v0    # "content":Landroid/view/View;
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_1
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IZ)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "warningText"    # Ljava/lang/String;
    .param p3, "theme"    # I
    .param p4, "isTablet"    # Z

    .prologue
    .line 67
    const/16 v3, 0x3c

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;-><init>(Landroid/content/Context;Ljava/lang/String;IIZ)V

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTimerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I

    return v0
.end method

.method static synthetic access$110(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->time:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/os/Vibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mVibrator:Landroid/os/Vibrator;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->reqTime:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Lcom/sec/android/emergencymode/util/TalkBackUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTalkbackUtil:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->warningMsg:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->getTimeString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->remainTime:Landroid/widget/TextView;

    return-object v0
.end method

.method private getTimeString(I)Ljava/lang/String;
    .locals 7
    .param p1, "time"    # I

    .prologue
    const/16 v5, 0xa

    .line 198
    div-int/lit8 v1, p1, 0x3c

    .line 199
    .local v1, "min":I
    rem-int/lit8 v3, p1, 0x3c

    .line 200
    .local v3, "sec":I
    const-string v0, ":"

    .line 201
    .local v0, "delimiter":Ljava/lang/String;
    const-string v2, ""

    .line 202
    .local v2, "minStr":Ljava/lang/String;
    const-string v4, ""

    .line 204
    .local v4, "secStr":Ljava/lang/String;
    if-ge v1, v5, :cond_0

    .line 205
    const-string v2, "0"

    .line 207
    :cond_0
    if-ge v3, v5, :cond_1

    .line 208
    const-string v4, "0"

    .line 211
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 212
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 214
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method private setTextColor()V
    .locals 2

    .prologue
    .line 111
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 112
    .local v0, "res":Landroid/content/res/Resources;
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mIsTablet:Z

    if-eqz v1, :cond_1

    .line 114
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mIsDarkTheme:Z

    if-eqz v1, :cond_0

    .line 116
    const v1, 0x7f06000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTextColor:I

    .line 131
    :goto_0
    return-void

    .line 119
    :cond_0
    const v1, 0x7f060012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTextColor:I

    goto :goto_0

    .line 123
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mIsDarkTheme:Z

    if-eqz v1, :cond_2

    .line 125
    const v1, 0x7f060004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTextColor:I

    goto :goto_0

    .line 128
    :cond_2
    const v1, 0x7f060009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTextColor:I

    goto :goto_0
.end method


# virtual methods
.method public create()Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "EmergencyAutoTimerDialog.create(): use show() instead.."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dismiss(I)V
    .locals 1
    .param p1, "reason"    # I

    .prologue
    .line 190
    sput p1, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->DISMISS_CAUSE:I

    .line 191
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTalkbackUtil:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTalkbackUtil:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->onDestroy()V

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 195
    :cond_1
    return-void
.end method

.method public show()Landroid/app/AlertDialog;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 141
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    .line 142
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mContext:Landroid/content/Context;

    const-string v3, "keyguard"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 143
    .local v0, "kgm":Landroid/app/KeyguardManager;
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "vibrator"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Vibrator;

    iput-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mVibrator:Landroid/os/Vibrator;

    .line 144
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/emergencymode/util/WakeLockUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/util/WakeLockUtil;

    move-result-object v1

    .line 145
    .local v1, "mWakelockUtil":Lcom/sec/android/emergencymode/util/WakeLockUtil;
    invoke-virtual {v1}, Lcom/sec/android/emergencymode/util/WakeLockUtil;->acquireWakeLock()V

    .line 147
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 148
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d9

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    .line 151
    :goto_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1120010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 152
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 154
    :cond_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v2, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 155
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 157
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 159
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    return-object v2

    .line 150
    :cond_1
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x7d8

    invoke-virtual {v2, v3}, Landroid/view/Window;->setType(I)V

    goto :goto_0
.end method
