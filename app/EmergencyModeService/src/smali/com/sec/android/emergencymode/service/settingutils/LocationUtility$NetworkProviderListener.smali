.class Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;
.super Ljava/lang/Object;
.source "LocationUtility.java"

# interfaces
.implements Landroid/location/LocationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkProviderListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;


# direct methods
.method private constructor <init>(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;->this$0:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;
    .param p2, "x1"    # Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;

    .prologue
    .line 389
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;-><init>(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;)V

    return-void
.end method


# virtual methods
.method public onLocationChanged(Landroid/location/Location;)V
    .locals 7
    .param p1, "loc"    # Landroid/location/Location;

    .prologue
    .line 392
    if-eqz p1, :cond_0

    .line 393
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;->this$0:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v6

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->updateLocationInfo(DDF)V

    .line 394
    const-string v0, "LocationUtility"

    const-string v1, "NetworkProvider get Gps Information Success!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_0
    return-void
.end method

.method public onProviderDisabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 400
    return-void
.end method

.method public onProviderEnabled(Ljava/lang/String;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 404
    return-void
.end method

.method public onStatusChanged(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "status"    # I
    .param p3, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 408
    return-void
.end method
