.class public Lcom/sec/android/emergencymode/service/packagectrl/SmsChanger;
.super Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;
.source "SmsChanger.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SmsChanger"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method


# virtual methods
.method public changeDefaultApp(Z)V
    .locals 7
    .param p1, "req"    # Z

    .prologue
    .line 38
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/packagectrl/SmsChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 39
    .local v2, "r":Landroid/content/res/Resources;
    const v4, 0x1040029

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 40
    .local v3, "secSms":Ljava/lang/String;
    if-eqz p1, :cond_3

    .line 41
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/packagectrl/SmsChanger;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/android/internal/telephony/SmsApplication;->getDefaultSmsApplication(Landroid/content/Context;Z)Landroid/content/ComponentName;

    move-result-object v0

    .line 42
    .local v0, "oldApp":Landroid/content/ComponentName;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "oldDefaultSms":Ljava/lang/String;
    :goto_0
    if-eqz v1, :cond_0

    .line 44
    const-string v4, "DEFAULT_SMS"

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/emergencymode/service/packagectrl/SmsChanger;->saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    :cond_0
    const-string v4, "SmsChanger"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changeDefaultSms old pkg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/packagectrl/SmsChanger;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/android/internal/telephony/SmsApplication;->setDefaultApplication(Ljava/lang/String;Landroid/content/Context;)V

    .line 55
    .end local v0    # "oldApp":Landroid/content/ComponentName;
    :cond_1
    :goto_1
    return-void

    .line 42
    .end local v1    # "oldDefaultSms":Ljava/lang/String;
    .restart local v0    # "oldApp":Landroid/content/ComponentName;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 49
    .end local v0    # "oldApp":Landroid/content/ComponentName;
    :cond_3
    const-string v4, "DEFAULT_SMS"

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencymode/service/packagectrl/SmsChanger;->loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 50
    .restart local v1    # "oldDefaultSms":Ljava/lang/String;
    const-string v4, "SmsChanger"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "changeDefaultSms : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    if-eqz v1, :cond_1

    .line 52
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/packagectrl/SmsChanger;->mContext:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/android/internal/telephony/SmsApplication;->setDefaultApplication(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_1
.end method
