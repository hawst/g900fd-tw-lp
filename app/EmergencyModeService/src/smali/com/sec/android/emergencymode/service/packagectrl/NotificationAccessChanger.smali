.class public Lcom/sec/android/emergencymode/service/packagectrl/NotificationAccessChanger;
.super Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;
.source "NotificationAccessChanger.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "NotificationAccessChanger"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;-><init>(Landroid/content/Context;)V

    .line 31
    return-void
.end method


# virtual methods
.method public changeDefaultApp(Z)V
    .locals 4
    .param p1, "req"    # Z

    .prologue
    .line 35
    if-eqz p1, :cond_2

    .line 36
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/packagectrl/NotificationAccessChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "enabled_notification_listeners"

    invoke-static {v1, v2}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "oldDefaultList":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 38
    const-string v1, "DEFAULT_NOTI_ACCESS"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/emergencymode/service/packagectrl/NotificationAccessChanger;->saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_0
    const-string v1, "NotificationAccessChanger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "changeDefaultApp old list : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    :cond_1
    :goto_0
    return-void

    .line 42
    .end local v0    # "oldDefaultList":Ljava/lang/String;
    :cond_2
    const-string v1, "DEFAULT_NOTI_ACCESS"

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/packagectrl/NotificationAccessChanger;->loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 43
    .restart local v0    # "oldDefaultList":Ljava/lang/String;
    const-string v1, "NotificationAccessChanger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "changeDefaultApp : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    if-eqz v0, :cond_1

    .line 45
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/packagectrl/NotificationAccessChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "enabled_notification_listeners"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method
