.class public Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;
.super Landroid/app/Activity;
.source "UpsmDisclaimerActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UpsmDisclaimerActivity"


# instance fields
.field protected isDarkTheme:Z

.field protected isPhone:Z

.field private mAllAgreeCheck:Landroid/widget/CheckBox;

.field private mAllAgreeCheck2:Landroid/widget/CheckBox;

.field private mAllAgreeCheck3:Landroid/widget/CheckBox;

.field private mAllCheckListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mAllCheckListener2:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mAllCheckListener3:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mCancelButtonListener:Landroid/view/View$OnClickListener;

.field private mOkButtonListener:Landroid/view/View$OnClickListener;

.field private okButton:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 179
    new-instance v0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$1;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$1;-><init>(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mCancelButtonListener:Landroid/view/View$OnClickListener;

    .line 187
    new-instance v0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$2;-><init>(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mOkButtonListener:Landroid/view/View$OnClickListener;

    .line 196
    new-instance v0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$3;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$3;-><init>(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllCheckListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 213
    new-instance v0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;-><init>(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllCheckListener2:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 230
    new-instance v0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$5;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$5;-><init>(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllCheckListener3:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;Landroid/widget/Button;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;
    .param p1, "x1"    # Landroid/widget/Button;
    .param p2, "x2"    # Z

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setEnabledButton(Landroid/widget/Button;Z)V

    return-void
.end method

.method private decideGoOrNot()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 274
    const-string v2, "UpsmDisclaimerActivity"

    const-string v3, "decideGoOrNot"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    invoke-static {p0, v4}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->getDisclaimerUserAgreeState(Landroid/content/Context;Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 276
    const-string v2, "UpsmDisclaimerActivity"

    const-string v3, "User Disagreed!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    sget-object v2, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    if-eqz v2, :cond_0

    .line 278
    sget-object v2, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    invoke-virtual {v2}, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->stopSelf()V

    .line 279
    invoke-static {p0}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    move-result-object v1

    check-cast v1, Lcom/sec/android/emergencymode/service/EmergencyManagerService;

    .line 280
    .local v1, "mgrService":Lcom/sec/android/emergencymode/service/EmergencyManagerService;
    const/4 v2, -0x8

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->notifyCurrentState(I)V

    .line 291
    .end local v1    # "mgrService":Lcom/sec/android/emergencymode/service/EmergencyManagerService;
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    const-string v2, "UpsmDisclaimerActivity"

    const-string v3, "User Agreed!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.EMERGENCY_START_SERVICE_BY_ORDER"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 285
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "enabled"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 286
    const-string v2, "flag"

    const/16 v3, 0x200

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 287
    const-string v2, "skipdialog"

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "skipdialog"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 289
    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0
.end method

.method private setButtonView()V
    .locals 8

    .prologue
    const v7, 0x7f06000c

    const v6, 0x7f060007

    const/high16 v5, 0x7f020000

    .line 80
    const v3, 0x7f0b001b

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 81
    .local v0, "cancelButton":Landroid/widget/Button;
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mCancelButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v3, 0x7f0b001c

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;

    .line 84
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mOkButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 87
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f0b001a

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 88
    .local v1, "linearLayoutlButtons":Landroid/widget/LinearLayout;
    iget-boolean v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isPhone:Z

    if-eqz v3, :cond_0

    .line 89
    iget-boolean v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isDarkTheme:Z

    if-eqz v3, :cond_1

    .line 91
    const v3, 0x7f020052

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 92
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setTextColor(I)V

    .line 93
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 94
    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 95
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 105
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setEnabledButton(Landroid/widget/Button;Z)V

    .line 106
    return-void

    .line 98
    :cond_1
    const v3, 0x7f020054

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    .line 99
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setTextColor(I)V

    .line 101
    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0
.end method

.method private setEnabledButton(Landroid/widget/Button;Z)V
    .locals 2
    .param p1, "bt"    # Landroid/widget/Button;
    .param p2, "isEnable"    # Z

    .prologue
    .line 109
    if-nez p1, :cond_0

    .line 130
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isPhone:Z

    if-eqz v1, :cond_1

    .line 114
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 115
    .local v0, "res":Landroid/content/res/Resources;
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isDarkTheme:Z

    if-eqz v1, :cond_3

    .line 116
    if-eqz p2, :cond_2

    .line 117
    const v1, 0x7f060007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 129
    .end local v0    # "res":Landroid/content/res/Resources;
    :cond_1
    :goto_1
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 119
    .restart local v0    # "res":Landroid/content/res/Resources;
    :cond_2
    const v1, 0x7f060008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_1

    .line 122
    :cond_3
    if-eqz p2, :cond_4

    .line 123
    const v1, 0x7f06000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_1

    .line 125
    :cond_4
    const v1, 0x7f06000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_1
.end method

.method private setScrollView()V
    .locals 3

    .prologue
    const v2, 0x7f0b000a

    .line 133
    invoke-virtual {p0, v2}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOverScrollMode(I)V

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 135
    iget-boolean v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isDarkTheme:Z

    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {p0, v2}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f02005b

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    invoke-virtual {p0, v2}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f02005c

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public finish()V
    .locals 0

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->decideGoOrNot()V

    .line 270
    invoke-super {p0}, Landroid/app/Activity;->finish()V

    .line 271
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-static {p0}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    move-result-object v0

    .line 43
    .local v0, "winUtil":Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;
    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getDeviceType()I

    move-result v1

    if-nez v1, :cond_2

    .line 45
    iput-boolean v2, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isPhone:Z

    .line 46
    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getDeviceTheme()I

    move-result v1

    if-nez v1, :cond_1

    .line 47
    iput-boolean v2, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isDarkTheme:Z

    .line 48
    const v1, 0x7f0a0002

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setTheme(I)V

    .line 65
    :goto_0
    const v1, 0x7f09002a

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setTitle(I)V

    .line 66
    const v1, 0x7f030007

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setContentView(I)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 70
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 73
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setButtonView()V

    .line 74
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setScrollView()V

    .line 75
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setDisclaimerBodyView()V

    .line 76
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setCheckBoxView()V

    .line 77
    return-void

    .line 50
    :cond_1
    iput-boolean v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isDarkTheme:Z

    .line 51
    const v1, 0x7f0a0003

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setTheme(I)V

    goto :goto_0

    .line 55
    :cond_2
    iput-boolean v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isPhone:Z

    .line 56
    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getDeviceTheme()I

    move-result v1

    if-nez v1, :cond_3

    .line 57
    iput-boolean v2, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isDarkTheme:Z

    .line 58
    const v1, 0x7f0a0004

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setTheme(I)V

    goto :goto_0

    .line 60
    :cond_3
    iput-boolean v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->isDarkTheme:Z

    .line 61
    const v1, 0x7f0a0005

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setTheme(I)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 258
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 260
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->finish()V

    .line 262
    :cond_1
    const-string v0, "UpsmDisclaimerActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onKeyDown : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 295
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 299
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 297
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->finish()V

    goto :goto_0

    .line 295
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 306
    const-string v0, "UpsmDisclaimerActivity"

    const-string v1, "onStop"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->decideGoOrNot()V

    .line 308
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 309
    return-void
.end method

.method public setCheckBoxView()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 162
    const v0, 0x7f0b0017

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck:Landroid/widget/CheckBox;

    .line 163
    const v0, 0x7f0b0018

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck2:Landroid/widget/CheckBox;

    .line 164
    const v0, 0x7f0b0019

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck3:Landroid/widget/CheckBox;

    .line 166
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllCheckListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 167
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck2:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllCheckListener2:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 168
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck3:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllCheckListener3:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 170
    const-string v0, "CHN"

    const-string v1, "OPEN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck2:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck3:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck2:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method public setDisclaimerBodyView()V
    .locals 7

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 145
    .local v3, "res":Landroid/content/res/Resources;
    const v5, 0x7f0b0016

    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 146
    .local v2, "mTextV":Landroid/widget/TextView;
    const-string v4, ""

    .line 147
    .local v4, "sTxt":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    const v6, 0x7f090034

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v5

    .line 149
    .local v0, "disclaimerBody":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_2

    .line 150
    aget-object v5, v0, v1

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 151
    if-eqz v1, :cond_0

    .line 152
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 154
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 149
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 158
    :cond_2
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    return-void
.end method

.method public setOKButton()V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck3:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setEnabledButton(Landroid/widget/Button;Z)V

    .line 255
    :goto_0
    return-void

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setEnabledButton(Landroid/widget/Button;Z)V

    goto :goto_0
.end method
