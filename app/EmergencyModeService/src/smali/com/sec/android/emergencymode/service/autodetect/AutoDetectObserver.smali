.class public Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;
.super Ljava/lang/Object;
.source "AutoDetectObserver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AutoDetectObserver"


# instance fields
.field protected detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

.field protected detected:Z

.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "detectListener"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->detected:Z

    .line 34
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->mContext:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    .line 36
    return-void
.end method


# virtual methods
.method protected startObserver()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "AutoDetectObserver"

    const-string v1, "super.startObserver()"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->detected:Z

    .line 41
    return-void
.end method

.method protected stopObserver(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 44
    const-string v0, "AutoDetectObserver"

    const-string v1, "super.stopObserver()"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->detected:Z

    .line 46
    return-void
.end method
