.class public Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;
.super Ljava/lang/Object;
.source "TraumaSignalReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TraumaSignalReceiver"


# instance fields
.field private detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

.field private mCAListener:Lcom/samsung/android/contextaware/manager/ContextAwareListener;

.field private mContext:Landroid/content/Context;

.field private mContextAwareManager:Lcom/samsung/android/contextaware/ContextAwareManager;

.field private noActionTime:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->noActionTime:I

    .line 59
    new-instance v0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver$1;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver$1;-><init>(Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mCAListener:Lcom/samsung/android/contextaware/manager/ContextAwareListener;

    .line 37
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mContext:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    .line 39
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mContext:Landroid/content/Context;

    const-string v1, "context_aware"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/contextaware/ContextAwareManager;

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mContextAwareManager:Lcom/samsung/android/contextaware/ContextAwareManager;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;)Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    return-object v0
.end method


# virtual methods
.method protected readyReceiver()V
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mContextAwareManager:Lcom/samsung/android/contextaware/ContextAwareManager;

    sget v1, Lcom/samsung/android/contextaware/ContextAwareManager;->CARE_GIVER_SERVICE:I

    const/16 v2, 0x29

    iget v3, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->noActionTime:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/contextaware/ContextAwareManager;->setCAProperty(III)Z

    .line 47
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mContextAwareManager:Lcom/samsung/android/contextaware/ContextAwareManager;

    sget v1, Lcom/samsung/android/contextaware/ContextAwareManager;->CARE_GIVER_SERVICE:I

    const/16 v2, 0x28

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/contextaware/ContextAwareManager;->setCAProperty(III)Z

    .line 51
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mContextAwareManager:Lcom/samsung/android/contextaware/ContextAwareManager;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mCAListener:Lcom/samsung/android/contextaware/manager/ContextAwareListener;

    sget v2, Lcom/samsung/android/contextaware/ContextAwareManager;->CARE_GIVER_SERVICE:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/contextaware/ContextAwareManager;->registerListener(Lcom/samsung/android/contextaware/manager/ContextAwareListener;I)V

    .line 53
    return-void
.end method

.method protected unReadyReceiver()V
    .locals 3

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mContextAwareManager:Lcom/samsung/android/contextaware/ContextAwareManager;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->mCAListener:Lcom/samsung/android/contextaware/manager/ContextAwareListener;

    sget v2, Lcom/samsung/android/contextaware/ContextAwareManager;->CARE_GIVER_SERVICE:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/contextaware/ContextAwareManager;->unregisterListener(Lcom/samsung/android/contextaware/manager/ContextAwareListener;I)V

    .line 57
    return-void
.end method
