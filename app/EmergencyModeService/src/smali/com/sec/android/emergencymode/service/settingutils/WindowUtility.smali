.class public Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;
.super Ljava/lang/Object;
.source "WindowUtility.java"


# static fields
.field public static final DEVICE_PHONE:I = 0x0

.field public static final DEVICE_TABLET:I = 0x1

.field public static final DEVICE_UNCERTAIN:I = -0x1

.field private static final TAG:Ljava/lang/String; = "WindowUtility"

.field public static final THEME_DARK:I = 0x0

.field public static final THEME_LIGHT:I = 0x1

.field public static final THEME_UNCERTAIN:I = -0x1

.field private static mInstance:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDeviceTheme:I

.field private mDeviceType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mInstance:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, -0x1

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput v0, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceType:I

    .line 56
    iput v0, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceTheme:I

    .line 69
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    .line 70
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 59
    const-class v1, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    monitor-enter v1

    .line 60
    :try_start_0
    sget-object v0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mInstance:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    if-nez v0, :cond_0

    .line 61
    const-string v0, "WindowUtility"

    const-string v2, "make instance"

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    new-instance v0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mInstance:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    .line 64
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    sget-object v0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mInstance:Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;

    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public adjustFrameRate(Z)V
    .locals 6
    .param p1, "enabled"    # Z

    .prologue
    .line 73
    const/4 v1, 0x0

    .line 75
    .local v1, "out":Ljava/io/BufferedWriter;
    :try_start_0
    new-instance v2, Ljava/io/BufferedWriter;

    new-instance v3, Ljava/io/FileWriter;

    const-string v4, "/sys/class/lcd/panel/dynamic_fps_use_te"

    invoke-direct {v3, v4}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    .end local v1    # "out":Ljava/io/BufferedWriter;
    .local v2, "out":Ljava/io/BufferedWriter;
    if-eqz p1, :cond_2

    const/16 v3, 0x31

    :goto_0
    :try_start_1
    invoke-virtual {v2, v3}, Ljava/io/BufferedWriter;->write(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 81
    if-eqz v2, :cond_0

    .line 82
    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 86
    .end local v2    # "out":Ljava/io/BufferedWriter;
    .restart local v1    # "out":Ljava/io/BufferedWriter;
    :cond_1
    :goto_1
    return-void

    .line 76
    .end local v1    # "out":Ljava/io/BufferedWriter;
    .restart local v2    # "out":Ljava/io/BufferedWriter;
    :cond_2
    const/16 v3, 0x30

    goto :goto_0

    .line 83
    :catch_0
    move-exception v3

    move-object v1, v2

    .line 85
    .end local v2    # "out":Ljava/io/BufferedWriter;
    .restart local v1    # "out":Ljava/io/BufferedWriter;
    goto :goto_1

    .line 77
    :catch_1
    move-exception v0

    .line 78
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_3
    const-string v3, "WindowUtility"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Framerate Exp : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 81
    if-eqz v1, :cond_1

    .line 82
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 83
    :catch_2
    move-exception v3

    goto :goto_1

    .line 80
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    .line 81
    :goto_3
    if-eqz v1, :cond_3

    .line 82
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 84
    :cond_3
    :goto_4
    throw v3

    .line 83
    :catch_3
    move-exception v4

    goto :goto_4

    .line 80
    .end local v1    # "out":Ljava/io/BufferedWriter;
    .restart local v2    # "out":Ljava/io/BufferedWriter;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "out":Ljava/io/BufferedWriter;
    .restart local v1    # "out":Ljava/io/BufferedWriter;
    goto :goto_3

    .line 77
    .end local v1    # "out":Ljava/io/BufferedWriter;
    .restart local v2    # "out":Ljava/io/BufferedWriter;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "out":Ljava/io/BufferedWriter;
    .restart local v1    # "out":Ljava/io/BufferedWriter;
    goto :goto_2
.end method

.method public getAnimationScale(I)F
    .locals 4
    .param p1, "which"    # I

    .prologue
    .line 131
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 132
    .local v1, "wm":Landroid/view/IWindowManager;
    invoke-interface {v1, p1}, Landroid/view/IWindowManager;->getAnimationScale(I)F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 136
    .end local v1    # "wm":Landroid/view/IWindowManager;
    :goto_0
    return v2

    .line 133
    :catch_0
    move-exception v0

    .line 134
    .local v0, "exc":Landroid/os/RemoteException;
    const-string v2, "WindowUtility"

    const-string v3, "Unable to save auto-rotate setting"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const/high16 v2, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public getAutoRotation()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 90
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 91
    .local v1, "wm":Landroid/view/IWindowManager;
    invoke-interface {v1}, Landroid/view/IWindowManager;->isRotationFrozen()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x1

    .line 95
    .end local v1    # "wm":Landroid/view/IWindowManager;
    :cond_0
    :goto_0
    return v2

    .line 92
    :catch_0
    move-exception v0

    .line 93
    .local v0, "exc":Landroid/os/RemoteException;
    const-string v3, "WindowUtility"

    const-string v4, "Unable to save auto-rotate setting"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAutoRotationFirst()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 184
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 185
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v2, "accelerometer_rotation"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public getAutoRotationSecond()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 189
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 190
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v2, "accelerometer_rotation_second"

    const/4 v3, -0x2

    invoke-static {v0, v2, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public getCurrentRotation()I
    .locals 4

    .prologue
    .line 100
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 101
    .local v1, "wm":Landroid/view/IWindowManager;
    invoke-interface {v1}, Landroid/view/IWindowManager;->getRotation()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 106
    .end local v1    # "wm":Landroid/view/IWindowManager;
    :goto_0
    return v2

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "exc":Landroid/os/RemoteException;
    const-string v2, "WindowUtility"

    const-string v3, "Unable to save auto-rotate setting"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDeviceTheme()I
    .locals 3

    .prologue
    .line 169
    iget v0, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceTheme:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 170
    iget v0, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceTheme:I

    .line 180
    :goto_0
    return v0

    .line 173
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceTheme:I

    .line 178
    const-string v0, "WindowUtility"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDeviceTheme["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceTheme:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget v0, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceTheme:I

    goto :goto_0
.end method

.method public getDeviceType()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 154
    iget v1, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceType:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 155
    iget v1, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceType:I

    .line 165
    :goto_0
    return v1

    .line 157
    :cond_0
    const-string v1, "ro.build.characteristics"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v3, :cond_1

    .line 159
    iput v3, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceType:I

    .line 164
    :goto_1
    const-string v1, "WindowUtility"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDeviceType["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceType:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget v1, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceType:I

    goto :goto_0

    .line 161
    :cond_1
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mDeviceType:I

    goto :goto_1
.end method

.method public isSupportFolderType()Z
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.folder_type"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setAnimationScale(IF)V
    .locals 5
    .param p1, "which"    # I
    .param p2, "scale"    # F

    .prologue
    .line 141
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 142
    .local v1, "wm":Landroid/view/IWindowManager;
    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v2, p2, v2

    if-lez v2, :cond_0

    .line 143
    const-string v2, "WindowUtility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAnimationScale scale("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    invoke-interface {v1, p1, p2}, Landroid/view/IWindowManager;->setAnimationScale(IF)V

    .line 151
    .end local v1    # "wm":Landroid/view/IWindowManager;
    :goto_0
    return-void

    .line 146
    .restart local v1    # "wm":Landroid/view/IWindowManager;
    :cond_0
    const-string v2, "WindowUtility"

    const-string v3, "setAnimationScale value is invalid!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    .end local v1    # "wm":Landroid/view/IWindowManager;
    :catch_0
    move-exception v0

    .line 149
    .local v0, "exc":Landroid/os/RemoteException;
    const-string v2, "WindowUtility"

    const-string v3, "Unable to save setAnimationScale setting"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAutoRotation(ZI)V
    .locals 4
    .param p1, "autorotate"    # Z
    .param p2, "freeze"    # I

    .prologue
    .line 118
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    .line 119
    .local v1, "wm":Landroid/view/IWindowManager;
    if-eqz p1, :cond_0

    .line 120
    invoke-interface {v1}, Landroid/view/IWindowManager;->thawRotation()V

    .line 127
    .end local v1    # "wm":Landroid/view/IWindowManager;
    :goto_0
    return-void

    .line 122
    .restart local v1    # "wm":Landroid/view/IWindowManager;
    :cond_0
    invoke-interface {v1, p2}, Landroid/view/IWindowManager;->freezeRotation(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 124
    .end local v1    # "wm":Landroid/view/IWindowManager;
    :catch_0
    move-exception v0

    .line 125
    .local v0, "exc":Landroid/os/RemoteException;
    const-string v2, "WindowUtility"

    const-string v3, "Unable to save auto-rotate setting"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAutoRotationFirst(Z)V
    .locals 4
    .param p1, "autorotate"    # Z

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 196
    return-void

    .line 194
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAutoRotationSecond(Z)V
    .locals 4
    .param p1, "autorotate"    # Z

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "accelerometer_rotation_second"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 201
    return-void

    .line 199
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public shouldRestoreRotation()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/settingutils/WindowUtility;->getDeviceType()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 113
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
