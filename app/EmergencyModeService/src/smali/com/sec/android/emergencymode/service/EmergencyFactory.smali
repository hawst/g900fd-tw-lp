.class public Lcom/sec/android/emergencymode/service/EmergencyFactory;
.super Lcom/sec/android/emergencymode/service/EmergencyBase;
.source "EmergencyFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;
    }
.end annotation


# static fields
.field private static final ANDROID_INTERNAL_PKGNAME:Ljava/lang/String; = "com.android.internal.app"

.field private static final EMERGENCY_KNOX_FORCE_CLOSED:Ljava/lang/String; = "android.intent.action.EMERGENCY_KNOX_FORCE_CLOSED"

.field private static final EMERGENCY_LAUNCHER:Ljava/lang/String; = "com.sec.android.emergencylauncher"

.field private static final TAG:Ljava/lang/String; = "EmergencyFactory"

.field private static final TOUCHWIZ_LAUNCHER:Ljava/lang/String; = "com.sec.android.app.launcher"

.field private static instance:Lcom/sec/android/emergencymode/service/EmergencyFactory;


# instance fields
.field private final mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

.field protected mDialogHandler:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

.field private mEmergencyStateReceiver:Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;

.field protected mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

.field protected mSettingMgr:Lcom/sec/android/emergencymode/service/EmergencySettingManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyBase;-><init>(Landroid/content/Context;)V

    .line 336
    new-instance v0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;-><init>(Lcom/sec/android/emergencymode/service/EmergencyFactory;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    .line 75
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->initFactory()V

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/service/EmergencyFactory;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyFactory;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->destroyFactory()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/emergencymode/service/EmergencyFactory;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyFactory;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->requestUILock(ZZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/emergencymode/service/EmergencyFactory;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyFactory;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    return-object v0
.end method

.method private destroyFactory()V
    .locals 1

    .prologue
    .line 396
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->unregisterEmergencyReceiver()V

    .line 397
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->instance:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    .line 398
    return-void
.end method

.method public static getInstance()Lcom/sec/android/emergencymode/service/EmergencyFactory;
    .locals 2

    .prologue
    .line 89
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->instance:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    if-nez v0, :cond_0

    .line 90
    const-string v0, "EmergencyFactory"

    const-string v1, "instance is null!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    :cond_0
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->instance:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencyFactory;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const-class v1, Lcom/sec/android/emergencymode/service/EmergencyFactory;

    monitor-enter v1

    .line 80
    :try_start_0
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->instance:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    if-nez v0, :cond_0

    .line 81
    const-string v0, "EmergencyFactory"

    const-string v2, "make instance."

    invoke-static {v0, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    new-instance v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->instance:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    .line 84
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->instance:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    return-object v0

    .line 84
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private guidanceTTSMessage()V
    .locals 4

    .prologue
    .line 282
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/util/TalkBackUtil;

    move-result-object v1

    .line 283
    .local v1, "mTalkbackUtil":Lcom/sec/android/emergencymode/util/TalkBackUtil;
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    const v3, 0x7f090027

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "guidance":Ljava/lang/String;
    invoke-virtual {v1, v0}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->requestSpeak(Ljava/lang/String;)V

    .line 285
    return-void
.end method

.method private initFactory()V
    .locals 3

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode(Landroid/content/Context;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mEMState:Z

    .line 381
    new-instance v0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;-><init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/EmergencyActionListener;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mDialogHandler:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    .line 382
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mSettingMgr:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    .line 383
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    .line 385
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->registerEmergencyReceiver()V

    .line 387
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->resetAllowedApplicationList()V

    .line 388
    const-string v0, "EmergencyFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "init Factory : emState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mEMState:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fcState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getEmergencyFCState()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getEmergencyFCState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    const-string v0, "EmergencyFactory"

    const-string v1, "This is abnormal state. turn off state."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mEMState:Z

    .line 393
    :cond_0
    return-void
.end method

.method private isKnoxmode()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 311
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 312
    .local v1, "intentToResolve":Landroid/content/Intent;
    const-string v4, "android.intent.category.HOME"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 313
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 315
    .local v2, "ris":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 316
    .local v0, "HomeReceiver":Landroid/content/pm/ResolveInfo;
    if-eqz v0, :cond_0

    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const-string v5, "com.android.internal.app"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 317
    const-string v3, "EmergencyFactory"

    const-string v4, "isKnoxmode(), Knox mode. Return true"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    const/4 v3, 0x1

    .line 320
    :cond_0
    return v3
.end method

.method private lockNotifications(Z)V
    .locals 4
    .param p1, "lock"    # Z

    .prologue
    .line 253
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    const-string v3, "statusbar"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/StatusBarManager;

    .line 255
    .local v0, "mStatusBarManager":Landroid/app/StatusBarManager;
    if-eqz v0, :cond_0

    .line 256
    if-eqz p1, :cond_1

    .line 257
    const-string v2, "EmergencyFactory"

    const-string v3, "disableNotifications"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const/high16 v1, 0x1a70000

    .line 264
    .local v1, "statusBarFlag":I
    invoke-virtual {v0, v1}, Landroid/app/StatusBarManager;->disable(I)V

    .line 270
    .end local v1    # "statusBarFlag":I
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    const-string v2, "EmergencyFactory"

    const-string v3, "enableNotifications"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/StatusBarManager;->disable(I)V

    goto :goto_0
.end method

.method private rebootDevice()V
    .locals 5

    .prologue
    .line 324
    const-wide/16 v2, 0x1388

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 326
    .local v1, "startIntent":Landroid/content/Intent;
    const-string v2, "android.intent.action.REBOOT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 327
    const-string v2, "android.intent.extra.KEY_CONFIRM"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 328
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 329
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 330
    return-void

    .line 324
    .end local v1    # "startIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EmergencyFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sleep Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private registerEmergencyReceiver()V
    .locals 5

    .prologue
    .line 402
    :try_start_0
    const-string v2, "EmergencyFactory"

    const-string v3, "registerEmergencyReceiver"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mEmergencyStateReceiver:Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;

    if-nez v2, :cond_0

    .line 404
    new-instance v2, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;-><init>(Lcom/sec/android/emergencymode/service/EmergencyFactory;Lcom/sec/android/emergencymode/service/EmergencyFactory$1;)V

    iput-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mEmergencyStateReceiver:Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;

    .line 406
    :cond_0
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 407
    .local v1, "emFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 408
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 409
    const-string v2, "android.intent.action.EMERGENCY_KNOX_FORCE_CLOSED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 411
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mEmergencyStateReceiver:Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 415
    .end local v1    # "emFilter":Landroid/content/IntentFilter;
    :goto_0
    return-void

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "EmergencyFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "registerEmergencyReceiver e : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private reqKnoxSwitch()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 288
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/os/PersonaManager;->getKnoxContainerVersion(Landroid/content/Context;)Landroid/os/PersonaManager$KnoxContainerVersion;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/PersonaManager$KnoxContainerVersion;->toString()Ljava/lang/String;

    move-result-object v2

    .line 289
    .local v2, "version":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.EMERGENCY_KNOX_FORCE_CLOSED"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 291
    .local v0, "intent":Landroid/content/Intent;
    const-string v4, "EmergencyFactory"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "knox version "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v4, "2.3.0"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 293
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    const-string v5, "persona"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PersonaManager;

    .line 294
    .local v1, "mPersona":Landroid/os/PersonaManager;
    if-eqz v1, :cond_2

    .line 295
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isKnoxmode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 296
    invoke-virtual {v1, v3}, Landroid/os/PersonaManager;->launchPersonaHome(I)Z

    .line 297
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    new-instance v5, Landroid/os/UserHandle;

    invoke-direct {v5, v3}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 298
    const-string v3, "EmergencyFactory"

    const-string v4, "knox is enabled. request change owner."

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const/4 v3, 0x1

    .line 307
    .end local v1    # "mPersona":Landroid/os/PersonaManager;
    :cond_0
    :goto_0
    return v3

    .line 301
    .restart local v1    # "mPersona":Landroid/os/PersonaManager;
    :cond_1
    const-string v4, "EmergencyFactory"

    const-string v5, "Not knox user."

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 304
    :cond_2
    const-string v4, "EmergencyFactory"

    const-string v5, "Cannot get the PersonaManager"

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private requestUILock(ZZ)V
    .locals 3
    .param p1, "req"    # Z
    .param p2, "emState"    # Z

    .prologue
    .line 218
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mDialogHandler:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2, p2, p1}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->showDialog(IZZ)V

    .line 219
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->lockNotifications(Z)V

    .line 220
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/emergencymode/util/WakeLockUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/util/WakeLockUtil;

    move-result-object v0

    .line 221
    .local v0, "mWakelockUtil":Lcom/sec/android/emergencymode/util/WakeLockUtil;
    if-eqz p1, :cond_1

    .line 222
    invoke-virtual {v0}, Lcom/sec/android/emergencymode/util/WakeLockUtil;->acquireWakeLock()V

    .line 223
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->usePendingThread:Z

    if-eqz v1, :cond_0

    .line 225
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setReadyToStart(Z)V

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->usePendingThread:Z

    if-nez v1, :cond_0

    .line 230
    invoke-virtual {v0}, Lcom/sec/android/emergencymode/util/WakeLockUtil;->releaseWakeLock()V

    goto :goto_0
.end method

.method private sendEmergencyMessage()V
    .locals 2

    .prologue
    .line 277
    new-instance v0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;-><init>(Landroid/content/Context;)V

    .line 278
    .local v0, "smsSender":Lcom/sec/android/emergencymode/sms/EmergencySmsSender;
    invoke-virtual {v0}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->startMessageBindService()V

    .line 279
    return-void
.end method

.method private setEmergencyState(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    const/4 v0, 0x1

    .line 181
    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setEmergencyFCState(Z)V

    .line 182
    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setEmergencyExternalState(Z)V

    .line 183
    sput-boolean p1, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mEMState:Z

    .line 184
    return-void
.end method

.method private unregisterEmergencyReceiver()V
    .locals 4

    .prologue
    .line 419
    :try_start_0
    const-string v1, "EmergencyFactory"

    const-string v2, "unregisterEmergencyReceiver"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mEmergencyStateReceiver:Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 424
    :goto_0
    return-void

    .line 421
    :catch_0
    move-exception v0

    .line 422
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EmergencyFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unregisterEmergencyReceiver e : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public checkAfterBootCompleted()V
    .locals 3

    .prologue
    .line 463
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getEmergencyFCState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 464
    const-string v0, "EmergencyFactory"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Emergency Mode force stop abnormally. doRecovery Mode!("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->forceRecovery()V

    .line 467
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setBootTime()V

    .line 485
    :cond_0
    :goto_0
    return-void

    .line 469
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 470
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mSettingMgr:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    iget-object v0, v0, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mScreenCtrl:Lcom/sec/android/emergencymode/service/EmergencyScreenController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencymode/service/EmergencyScreenController;->setGrayScreen(Z)V

    .line 471
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setBootTime()V

    goto :goto_0

    .line 473
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkEmergenyLauncerState()V

    .line 474
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setBootTime()V

    .line 475
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    if-eqz v0, :cond_0

    .line 476
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->lastAction:Ljava/lang/String;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->lastAction:Ljava/lang/String;

    const-string v1, "android.intent.action.EMERGENCY_START_SERVICE_BY_ORDER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 478
    :cond_3
    const-string v0, "off"

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setProperties(Ljava/lang/String;)V

    .line 479
    const-string v0, "EmergencyFactory"

    const-string v1, "No Recovery State, kill my self."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->stopSelf()V

    goto :goto_0
.end method

.method protected checkEmergenyLauncerState()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 488
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 489
    .local v3, "packageManager":Landroid/content/pm/PackageManager;
    const-string v4, "com.sec.android.emergencylauncher"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    .line 491
    .local v0, "emEnabledState":I
    if-ne v0, v5, :cond_0

    .line 494
    :try_start_0
    const-string v4, "EmergencyFactory"

    const-string v5, "Abnormal state. Change EL to default (disable) state!"

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 495
    const-string v4, "com.sec.android.emergencylauncher"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->clearPackagePreferredActivities(Ljava/lang/String;)V

    .line 496
    const-string v4, "com.sec.android.emergencylauncher"

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 498
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 499
    .local v2, "homeList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getHomeActivities(Ljava/util/List;)Landroid/content/ComponentName;

    .line 500
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-gtz v4, :cond_0

    .line 502
    const-string v4, "EmergencyFactory"

    const-string v5, "There is no home Launcher which is enabled. Abnormal state. Enable touchwiz home launcher!"

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 503
    const-string v4, "com.sec.android.app.launcher"

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 510
    .end local v2    # "homeList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    :cond_0
    :goto_0
    return-void

    .line 505
    :catch_0
    move-exception v1

    .line 506
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const-string v4, "EmergencyFactory"

    const-string v5, "IllegalArgumentException!"

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public checkInvalidBroadcast(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkInvalidBroadcast(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkInvalidProcess(Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 169
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    invoke-virtual {v0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkInvalidProcess(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkValidIntentAction(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "actName"    # Ljava/lang/String;

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkValidIntentAction(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public checkValidPackage(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 1
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "actName"    # Ljava/lang/String;
    .param p3, "allowFlag"    # I

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkValidPackage(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method protected forceRecovery()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isForceTrigger:Z

    .line 100
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->runEmergencyMode(Z)V

    .line 101
    return-void
.end method

.method public getforceBlockUserPkg()Z
    .locals 1

    .prologue
    .line 206
    sget-boolean v0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isforceBlockUserPkg:Z

    return v0
.end method

.method public initForEMState()V
    .locals 0

    .prologue
    .line 515
    return-void
.end method

.method public isModifying()Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mDialogHandler:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->getProgressingState()Z

    move-result v0

    return v0
.end method

.method public makeReadyToStart()V
    .locals 6

    .prologue
    .line 236
    iget-boolean v3, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->usePendingThread:Z

    if-eqz v3, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode()Z

    move-result v1

    .line 238
    .local v1, "emergencyState":Z
    const-string v3, "EmergencyFactory"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "makeReadyToStart EmergencyState["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setReadyToStart(Z)V

    .line 240
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/emergencymode/util/WakeLockUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/util/WakeLockUtil;

    move-result-object v2

    .line 241
    .local v2, "mWakelockUtil":Lcom/sec/android/emergencymode/util/WakeLockUtil;
    invoke-virtual {v2}, Lcom/sec/android/emergencymode/util/WakeLockUtil;->releaseWakeLock()V

    .line 242
    if-nez v1, :cond_0

    .line 243
    sget-object v3, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    if-eqz v3, :cond_0

    .line 244
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_0
    const-string v3, "EmergencyFactory"

    const-string v4, "killed by myself"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    sget-object v3, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    invoke-virtual {v3}, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->stopSelf()V

    .line 250
    .end local v1    # "emergencyState":Z
    .end local v2    # "mWakelockUtil":Lcom/sec/android/emergencymode/util/WakeLockUtil;
    :cond_0
    return-void

    .line 244
    .restart local v1    # "emergencyState":Z
    .restart local v2    # "mWakelockUtil":Lcom/sec/android/emergencymode/util/WakeLockUtil;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "EmergencyFactory"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Sleep Exception : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public needMobileDataBlock()Z
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x1

    .line 187
    const/16 v7, 0x440

    invoke-virtual {p0, v7}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkModeType(I)Z

    move-result v0

    .line 188
    .local v0, "fmmState":Z
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const-string v9, "CHATON"

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getPackageGroupAsIndex(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9, v6}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkValidPackage(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    .line 189
    .local v5, "vChatOnUsed":Z
    const/4 v1, 0x0

    .local v1, "hasUseMobileData":Z
    const/4 v4, 0x0

    .line 190
    .local v4, "retVal":Z
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    invoke-virtual {v7, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getPackageList(I)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 191
    .local v3, "pkg":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    sget v8, Lcom/sec/android/emergencymode/service/EmergencyFactory;->PERMISSION_USE_MOBILE_DATA:I

    invoke-virtual {v7, v3, v8}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkWhiteListPermission(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 192
    const/4 v1, 0x1

    goto :goto_0

    .line 195
    .end local v3    # "pkg":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isScreenOn()Z

    move-result v7

    if-nez v7, :cond_2

    if-nez v0, :cond_2

    if-nez v5, :cond_2

    if-nez v1, :cond_2

    move v4, v6

    .line 196
    :goto_1
    const-string v6, "EmergencyFactory"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "needMobileDataBlock ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    return v4

    .line 195
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected declared-synchronized runEmergencyMode(ZIZ)I
    .locals 4
    .param p1, "enabled"    # Z
    .param p2, "flag"    # I
    .param p3, "skipdialog"    # Z

    .prologue
    const/4 v0, 0x1

    .line 104
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->registerEmergencyReceiver()V

    .line 105
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mDialogHandler:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->getProgressingState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 106
    const-string v0, "EmergencyFactory"

    const-string v1, "is Progressing..."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    const/4 v0, -0x2

    .line 136
    :goto_0
    monitor-exit p0

    return v0

    .line 109
    :cond_0
    :try_start_1
    invoke-virtual {p0, p2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setEnterType(I)V

    .line 110
    const-string v1, "EmergencyFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Triggered by "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getEnterTypeString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", skipdialog is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getEnterType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 114
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mDialogHandler:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p1, v3}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->showDialog(IZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 117
    :sswitch_1
    if-nez p3, :cond_1

    .line 118
    :try_start_2
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mDialogHandler:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    const/4 v2, 0x1

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p1, v3}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->showDialog(IZZ)V

    goto :goto_0

    .line 120
    :cond_1
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->runEmergencyMode(Z)V

    goto :goto_0

    .line 124
    :sswitch_2
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->runEmergencyMode(Z)V

    goto :goto_0

    .line 128
    :sswitch_3
    if-eqz p1, :cond_2

    .line 129
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mDialogHandler:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p1, v3}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->showDialog(IZZ)V

    goto :goto_0

    .line 131
    :cond_2
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mDialogHandler:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    const/4 v2, 0x4

    const/4 v3, 0x1

    invoke-virtual {v1, v2, p1, v3}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->showDialog(IZZ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 111
    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x20 -> :sswitch_2
        0x40 -> :sswitch_3
        0x100 -> :sswitch_0
        0x200 -> :sswitch_1
        0x400 -> :sswitch_3
    .end sparse-switch
.end method

.method protected runEmergencyMode(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    const/4 v0, 0x1

    .line 141
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mSettingMgr:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mProcessUtility:Lcom/sec/android/emergencymode/service/settingutils/ProcessUtility;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/service/settingutils/ProcessUtility;->setProcessLimitOption(I)V

    .line 142
    invoke-direct {p0, v0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->requestUILock(ZZ)V

    .line 143
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->reqKnoxSwitch()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 146
    :cond_1
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mSettingMgr:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    invoke-virtual {v1, p1}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->setEmergencySettings(Z)V

    .line 147
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setEmergencyState(Z)V

    .line 148
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mSettingMgr:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    if-nez p1, :cond_2

    :goto_1
    invoke-virtual {v1, v0}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->sendSettingBroadcast(Z)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    invoke-virtual {v0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->runApplicationOperation(Z)V

    .line 150
    if-eqz p1, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 148
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setforceBlockUserPkg(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 201
    if-eqz p1, :cond_0

    const-string v0, "EmergencyFactory"

    const-string v1, "ETWS msg received."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :cond_0
    sput-boolean p1, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isforceBlockUserPkg:Z

    .line 203
    return-void
.end method
