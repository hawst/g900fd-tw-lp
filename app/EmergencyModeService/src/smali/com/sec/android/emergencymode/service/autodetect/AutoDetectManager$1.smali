.class Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$1;
.super Landroid/os/Handler;
.source "AutoDetectManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$1;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 83
    const-string v0, "AutoDetectManager"

    const-string v1, "Detection is expired. This is Emergency situation."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$1;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;

    const-string v1, "EXPIRED"

    # invokes: Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->stopActionDetect(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->access$000(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;Ljava/lang/String;)V

    .line 85
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$1;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;

    # getter for: Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->actionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    invoke-static {v0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->access$100(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0x80

    invoke-interface {v0, v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyActionListener;->onAutoAccept(ZI)V

    .line 86
    return-void
.end method
