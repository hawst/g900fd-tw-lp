.class Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;
.super Ljava/lang/Thread;
.source "EmergencyPackageController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/emergencymode/service/EmergencyPackageController;->runApplicationOperation(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

.field final synthetic val$enabled:Z


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Z)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iput-boolean p2, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->val$enabled:Z

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 218
    const/16 v9, -0x13

    invoke-static {v9}, Landroid/os/Process;->setThreadPriority(I)V

    .line 219
    const/4 v9, 0x0

    invoke-static {v9}, Landroid/os/Process;->setCanSelfBackground(Z)V

    .line 220
    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->val$enabled:Z

    if-eqz v9, :cond_2

    const/4 v9, 0x2

    :goto_0
    invoke-virtual {v10, v9}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->notifyCurrentState(I)V

    .line 221
    iget-boolean v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->val$enabled:Z

    if-eqz v9, :cond_3

    .line 222
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->clearAppListDB()V
    invoke-static {v9}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$000(Lcom/sec/android/emergencymode/service/EmergencyPackageController;)V

    .line 225
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->makeAppListToEnable()Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$100(Lcom/sec/android/emergencymode/service/EmergencyPackageController;)Ljava/util/ArrayList;

    move-result-object v4

    .line 226
    .local v4, "appListToEnable":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v4, :cond_0

    .line 227
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v11, v11, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->usePendingThread:Z

    const/4 v12, 0x0

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    invoke-static {v9, v4, v10, v11, v12}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V

    .line 228
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->sendBootComplete(Ljava/util/ArrayList;)V
    invoke-static {v9, v4}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$300(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;)V

    .line 233
    :cond_0
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x1

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->changeDefaultApplication(Z)V
    invoke-static {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$400(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Z)V

    .line 234
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->makeAppListToDisable()Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$500(Lcom/sec/android/emergencymode/service/EmergencyPackageController;)Ljava/util/ArrayList;

    move-result-object v1

    .line 235
    .local v1, "appListToDisable":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_1

    .line 237
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const-string v10, "com.sec.android.app.launcher"

    invoke-virtual {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->killThisPackage(Ljava/lang/String;)V

    .line 239
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->killRunningProcesses(Ljava/util/HashSet;)V

    .line 240
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x3

    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v11, v11, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->usePendingThread:Z

    const/4 v12, 0x1

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    invoke-static {v9, v1, v10, v11, v12}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V

    .line 246
    :cond_1
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-object v9, v9, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    move-result-object v5

    .line 247
    .local v5, "eSettingManager":Lcom/sec/android/emergencymode/service/EmergencySettingManager;
    invoke-virtual {v5}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->adjustLocationInfoAndConnectivity()V

    .line 292
    .end local v5    # "eSettingManager":Lcom/sec/android/emergencymode/service/EmergencySettingManager;
    :goto_1
    iget-object v10, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->val$enabled:Z

    if-eqz v9, :cond_4

    const/4 v9, 0x3

    :goto_2
    invoke-virtual {v10, v9}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->notifyCurrentState(I)V

    .line 293
    return-void

    .line 220
    .end local v1    # "appListToDisable":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "appListToEnable":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    const/4 v9, 0x4

    goto :goto_0

    .line 250
    :cond_3
    const/4 v9, 0x0

    sput-boolean v9, Lcom/sec/android/emergencymode/service/EmergencyBase;->isModifying:Z

    .line 251
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setEmergencyExternalState(Z)V

    .line 254
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->loadEmergencyDisabledAppList(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 255
    .local v0, "appListToDefault":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->loadEmergencyDisabledAppList(I)Ljava/util/ArrayList;

    move-result-object v4

    .line 258
    .restart local v4    # "appListToEnable":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->makeWidgetAllList()Ljava/util/HashSet;
    invoke-static {v9}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$600(Lcom/sec/android/emergencymode/service/EmergencyPackageController;)Ljava/util/HashSet;

    move-result-object v6

    .line 259
    .local v6, "widgetListAll":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->divideWidgetAndApp(Ljava/util/ArrayList;Ljava/util/HashSet;)Ljava/util/ArrayList;
    invoke-static {v9, v0, v6}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$700(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v7

    .line 260
    .local v7, "widgetListToDefault":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->divideWidgetAndApp(Ljava/util/ArrayList;Ljava/util/HashSet;)Ljava/util/ArrayList;
    invoke-static {v9, v4, v6}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$700(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v8

    .line 263
    .local v8, "widgetListToEnable":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v11, v11, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->usePendingThread:Z

    const/4 v12, 0x0

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    invoke-static {v9, v7, v10, v11, v12}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V

    .line 264
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v11, v11, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->usePendingThread:Z

    const/4 v12, 0x0

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    invoke-static {v9, v8, v10, v11, v12}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V

    .line 267
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v11, v11, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->usePendingThread:Z

    const/4 v12, 0x0

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    invoke-static {v9, v0, v10, v11, v12}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V

    .line 268
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v11, v11, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->usePendingThread:Z

    const/4 v12, 0x0

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    invoke-static {v9, v4, v10, v11, v12}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V

    .line 271
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->sendBootComplete(Ljava/util/ArrayList;)V
    invoke-static {v9, v7}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$300(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;)V

    .line 272
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->sendBootComplete(Ljava/util/ArrayList;)V
    invoke-static {v9, v8}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$300(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;)V

    .line 273
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->sendBootComplete(Ljava/util/ArrayList;)V
    invoke-static {v9, v0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$300(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;)V

    .line 274
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->sendBootComplete(Ljava/util/ArrayList;)V
    invoke-static {v9, v4}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$300(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;)V

    .line 276
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x0

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->changeDefaultApplication(Z)V
    invoke-static {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$400(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Z)V

    .line 280
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->loadEmergencyDisabledAppList(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 281
    .local v3, "appListToDisableUntilUsed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->loadEmergencyDisabledAppList(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 282
    .local v2, "appListToDisableByUser":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->loadEmergencyDisabledAppList(I)Ljava/util/ArrayList;

    move-result-object v1

    .line 283
    .restart local v1    # "appListToDisable":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v9, "com.sec.android.emergencylauncher"

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x4

    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v11, v11, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->usePendingThread:Z

    const/4 v12, 0x0

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    invoke-static {v9, v3, v10, v11, v12}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V

    .line 286
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x3

    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v11, v11, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->usePendingThread:Z

    const/4 v12, 0x0

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    invoke-static {v9, v2, v10, v11, v12}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V

    .line 287
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const/4 v10, 0x2

    iget-object v11, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    iget-boolean v11, v11, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->usePendingThread:Z

    const/4 v12, 0x1

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    invoke-static {v9, v1, v10, v11, v12}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V

    .line 290
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyPackageController;->clearAppListDB()V
    invoke-static {v9}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->access$000(Lcom/sec/android/emergencymode/service/EmergencyPackageController;)V

    goto/16 :goto_1

    .line 292
    .end local v0    # "appListToDefault":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "appListToDisableByUser":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "appListToDisableUntilUsed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "widgetListAll":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v7    # "widgetListToDefault":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "widgetListToEnable":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    const/4 v9, 0x5

    goto/16 :goto_2
.end method
