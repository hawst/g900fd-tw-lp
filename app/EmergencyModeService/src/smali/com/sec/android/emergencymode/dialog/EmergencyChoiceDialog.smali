.class public Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;
.super Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;
.source "EmergencyChoiceDialog.java"


# static fields
.field public static DISMISS_CAUSE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "EmergencyChoiceDialog"


# instance fields
.field protected colorTextMain:I

.field private dialog:Landroid/app/AlertDialog;

.field protected isDarkTheme:Z

.field protected isTablet:Z

.field private mContext:Landroid/content/Context;

.field private reqState:Z

.field protected theme:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->DISMISS_CAUSE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZIZ)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reqState"    # Z
    .param p3, "theme"    # I
    .param p4, "isTablet"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    invoke-direct {p0, p1, p3}, Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;-><init>(Landroid/content/Context;I)V

    .line 46
    iput-boolean v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->isDarkTheme:Z

    .line 53
    iput-object p1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    .line 54
    iput p3, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->theme:I

    .line 55
    iput-boolean p4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->isTablet:Z

    .line 56
    iput-boolean p2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->reqState:Z

    .line 57
    sput v3, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->DISMISS_CAUSE:I

    .line 59
    const/4 v2, 0x4

    if-eq p3, v2, :cond_0

    .line 60
    iput-boolean v3, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->isDarkTheme:Z

    .line 63
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->reqState:Z

    if-ne v2, v4, :cond_1

    .line 64
    const v0, 0x7f090037

    .line 69
    .local v0, "ok_str":I
    :goto_0
    new-instance v2, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog$1;-><init>(Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;)V

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 76
    const v2, 0x1040009

    new-instance v3, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog$2;

    invoke-direct {v3, p0}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog$2;-><init>(Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;)V

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 85
    .local v1, "res":Landroid/content/res/Resources;
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->isTablet:Z

    if-eqz v2, :cond_3

    .line 87
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->isDarkTheme:Z

    if-eqz v2, :cond_2

    .line 89
    const v2, 0x7f06000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->colorTextMain:I

    .line 105
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->updateView()V

    .line 106
    return-void

    .line 66
    .end local v0    # "ok_str":I
    .end local v1    # "res":Landroid/content/res/Resources;
    :cond_1
    const v0, 0x7f090038

    .restart local v0    # "ok_str":I
    goto :goto_0

    .line 92
    .restart local v1    # "res":Landroid/content/res/Resources;
    :cond_2
    const v2, 0x7f060012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->colorTextMain:I

    goto :goto_1

    .line 96
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->isDarkTheme:Z

    if-eqz v2, :cond_4

    .line 98
    const v2, 0x7f060004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->colorTextMain:I

    goto :goto_1

    .line 101
    :cond_4
    const v2, 0x7f060009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->colorTextMain:I

    goto :goto_1
.end method

.method private getChoiceDialogView()Landroid/view/View;
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 151
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v9, "warningString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    const v13, 0x7f090005

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    const v13, 0x7f090006

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    const v13, 0x7f090007

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    const v13, 0x7f090008

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    iget-boolean v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->isDocomoFeature:Z

    if-eqz v12, :cond_0

    .line 161
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    const v13, 0x7f090009

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    :cond_0
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    const v13, 0x7f09000a

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    const-string v13, "layout_inflater"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 166
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v12, 0x7f030001

    invoke-virtual {v5, v12, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 168
    .local v0, "content":Landroid/view/View;
    const v12, 0x7f0b0003

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 170
    .local v2, "headerText":Landroid/widget/TextView;
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    const v13, 0x7f090004

    invoke-virtual {v12, v13}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "header":Ljava/lang/String;
    iget v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->colorTextMain:I

    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 172
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    const v12, 0x7f0b0004

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 176
    .local v11, "warningView":Landroid/widget/LinearLayout;
    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 177
    .local v6, "str":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 178
    const v12, 0x7f030002

    invoke-virtual {v5, v12, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 179
    .local v7, "summaryView":Landroid/view/View;
    const v12, 0x7f0b0006

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 180
    .local v4, "imgView":Landroid/widget/ImageView;
    iget-object v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    iget-boolean v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->isDarkTheme:Z

    if-eqz v12, :cond_2

    const v12, 0x7f02004f

    :goto_1
    invoke-virtual {v13, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    invoke-virtual {v4, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 181
    const v12, 0x7f0b0007

    invoke-virtual {v7, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 182
    .local v10, "warningText":Landroid/widget/TextView;
    iget v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->colorTextMain:I

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 183
    invoke-virtual {v10, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    invoke-virtual {v11, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 180
    .end local v10    # "warningText":Landroid/widget/TextView;
    :cond_2
    const v12, 0x7f020050

    goto :goto_1

    .line 189
    .end local v4    # "imgView":Landroid/widget/ImageView;
    .end local v6    # "str":Ljava/lang/String;
    .end local v7    # "summaryView":Landroid/view/View;
    :cond_3
    const v12, 0x7f0b0005

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 190
    .local v8, "warningEnd":Landroid/widget/TextView;
    iget v12, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->colorTextMain:I

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setTextColor(I)V

    .line 192
    return-object v0
.end method

.method private getDisableDialogView()Landroid/view/View;
    .locals 6

    .prologue
    .line 139
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 140
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030003

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 142
    .local v0, "content":Landroid/view/View;
    const v4, 0x7f0b000b

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 143
    .local v3, "messageText":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090003

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 144
    .local v2, "message":Ljava/lang/String;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    iget v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->colorTextMain:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 146
    return-object v0
.end method


# virtual methods
.method public create()Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "EmergencyChoiceDialog.create(): use show() instead.."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dismiss(I)V
    .locals 1
    .param p1, "reason"    # I

    .prologue
    .line 120
    sput p1, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->DISMISS_CAUSE:I

    .line 121
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 123
    :cond_0
    return-void
.end method

.method public show()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 115
    invoke-super {p0}, Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->dialog:Landroid/app/AlertDialog;

    .line 116
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->dialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public updateView()V
    .locals 3

    .prologue
    .line 128
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "title":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 131
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->reqState:Z

    if-eqz v1, :cond_0

    .line 132
    invoke-direct {p0}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->getChoiceDialogView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 136
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->getDisableDialogView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/dialog/EmergencyChoiceDialog;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method
