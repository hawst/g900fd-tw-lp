.class Lcom/sec/android/emergencymode/service/EmergencyFactory$1;
.super Ljava/lang/Object;
.source "EmergencyFactory.java"

# interfaces
.implements Lcom/sec/android/emergencymode/service/EmergencyActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/EmergencyFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/EmergencyFactory;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccept(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->runEmergencyMode(Z)V

    .line 346
    return-void
.end method

.method public onAutoAccept(ZI)V
    .locals 4
    .param p1, "enabled"    # Z
    .param p2, "reason"    # I

    .prologue
    .line 340
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->runEmergencyMode(ZIZ)I

    move-result v0

    .line 341
    .local v0, "result":I
    const-string v1, "EmergencyFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "request Auto Trigger "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    return-void
.end method

.method public onCancel()V
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    const/4 v1, -0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->notifyCurrentState(I)V

    .line 350
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 351
    const-string v0, "EmergencyFactory"

    const-string v1, "User canceled. Service Stop"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    if-eqz v0, :cond_0

    .line 353
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->stopSelf()V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyFactory;->destroyFactory()V
    invoke-static {v0}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->access$000(Lcom/sec/android/emergencymode/service/EmergencyFactory;)V

    .line 357
    :cond_1
    return-void
.end method

.method public onComplete(Z)V
    .locals 6
    .param p1, "enabled"    # Z

    .prologue
    const/4 v5, 0x0

    .line 360
    const-string v1, "EmergencyFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mode trigger complete req state : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    iget-object v4, v4, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode(Landroid/content/Context;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    if-eqz p1, :cond_1

    .line 362
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mSettingMgr:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->setLastDisableSetting()V

    .line 367
    :goto_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyFactory;->requestUILock(ZZ)V
    invoke-static {v1, v5, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->access$100(Lcom/sec/android/emergencymode/service/EmergencyFactory;ZZ)V

    .line 368
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v1, v5}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setEmergencyFCState(Z)V

    .line 369
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    iget-boolean v1, v1, Lcom/sec/android/emergencymode/service/EmergencyFactory;->usePendingThread:Z

    if-nez v1, :cond_0

    .line 370
    if-nez p1, :cond_0

    sget-object v1, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    if-eqz v1, :cond_0

    .line 371
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 372
    :goto_1
    const-string v1, "EmergencyFactory"

    const-string v2, "killed by myself"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    sget-object v1, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->stopSelf()V

    .line 376
    :cond_0
    return-void

    .line 364
    :cond_1
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mSettingMgr:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->mScreenCtrl:Lcom/sec/android/emergencymode/service/EmergencyScreenController;

    invoke-virtual {v1, v5}, Lcom/sec/android/emergencymode/service/EmergencyScreenController;->setGrayScreen(Z)V

    .line 365
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mSettingMgr:Lcom/sec/android/emergencymode/service/EmergencySettingManager;

    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/EmergencySettingManager;->setLastEnableSetting()V

    goto :goto_0

    .line 371
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EmergencyFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Sleep Exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
