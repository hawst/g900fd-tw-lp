.class public Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;
.super Ljava/lang/Object;
.source "EmergencyApiTestUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EmergencyApiTestUtil"


# instance fields
.field private TEST_SUPPORT:Z

.field private mContext:Landroid/content/Context;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->TEST_SUPPORT:Z

    .line 43
    iput-object p1, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mContext:Landroid/content/Context;

    .line 44
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mResolver:Landroid/content/ContentResolver;

    .line 45
    return-void
.end method


# virtual methods
.method public runApiTest()V
    .locals 3

    .prologue
    .line 48
    iget-boolean v0, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->TEST_SUPPORT:Z

    if-nez v0, :cond_0

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    const-string v0, "EmergencyApiTestUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "testEmergencyNumber result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->testEmergencyNumber()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    const-string v0, "EmergencyApiTestUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "testEmergencySettings result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->testEmergencySettings()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public testEmergencyNumber()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 55
    iget-boolean v3, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->TEST_SUPPORT:Z

    if-nez v3, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v2

    .line 57
    :cond_1
    iget-object v3, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "emcall"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/EmergencySettings;->getEmergencyNumber(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .local v0, "emCall":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mResolver:Landroid/content/ContentResolver;

    const-string v4, "heatsensing"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/EmergencySettings;->getEmergencyNumber(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "heatSensing":Ljava/lang/String;
    const-string v3, "EmergencyApiTestUtil"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "testEmergencyNumber : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 61
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public testEmergencySettings()Z
    .locals 12

    .prologue
    const-wide v10, 0x3ff199999999999aL    # 1.1

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 67
    iget-boolean v6, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->TEST_SUPPORT:Z

    if-nez v6, :cond_1

    .line 86
    :cond_0
    :goto_0
    return v4

    .line 69
    :cond_1
    iget-object v6, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "TEST_PREF1"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    .line 70
    iget-object v6, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "TEST_PREF2"

    const-string v8, "a"

    invoke-static {v6, v7, v8}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    iget-object v6, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "TEST_PREF3"

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    .line 73
    iget-object v6, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "TEST_PREF1"

    invoke-static {v6, v7, v4}, Lcom/sec/android/emergencymode/EmergencySettings;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 74
    .local v0, "test1":I
    iget-object v6, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "TEST_PREF2"

    const-string v8, "b"

    invoke-static {v6, v7, v8}, Lcom/sec/android/emergencymode/EmergencySettings;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    .local v1, "test2":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/android/emergencymode/util/EmergencyApiTestUtil;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "TEST_PREF3"

    const-wide v8, 0x400199999999999aL    # 2.2

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/emergencymode/EmergencySettings;->getDouble(Landroid/content/ContentResolver;Ljava/lang/String;D)D

    move-result-wide v2

    .line 77
    .local v2, "test3":D
    const-string v6, "EmergencyApiTestUtil"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "testEmergencySettings : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    if-ne v0, v5, :cond_0

    .line 81
    const-string v6, "a"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 83
    cmpl-double v6, v2, v10

    if-gtz v6, :cond_0

    move v4, v5

    .line 86
    goto :goto_0
.end method
