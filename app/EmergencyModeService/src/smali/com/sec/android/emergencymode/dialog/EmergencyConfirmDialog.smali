.class public Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;
.super Landroid/app/AlertDialog$Builder;
.source "EmergencyConfirmDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EmergencyConfirmDialog"


# instance fields
.field private dialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reqState"    # Z

    .prologue
    .line 35
    const v0, 0x7f090010

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;-><init>(Landroid/content/Context;ZLjava/lang/String;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLjava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reqState"    # Z
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object p1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->mContext:Landroid/content/Context;

    .line 41
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 42
    if-eqz p2, :cond_0

    const v0, 0x7f09003a

    :goto_0
    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 43
    const v0, 0x104000a

    new-instance v1, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog$1;

    invoke-direct {v1, p0}, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog$1;-><init>(Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;)V

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 48
    invoke-virtual {p0, p3}, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 49
    return-void

    .line 42
    :cond_0
    const v0, 0x7f09003b

    goto :goto_0
.end method


# virtual methods
.method public create()Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "EmergencyChoiceDialog.create(): use show() instead.."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 77
    :cond_0
    return-void
.end method

.method public show()Landroid/app/AlertDialog;
    .locals 3

    .prologue
    .line 58
    invoke-super {p0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    .line 59
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->mContext:Landroid/content/Context;

    const-string v2, "keyguard"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 61
    .local v0, "kgm":Landroid/app/KeyguardManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 62
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d9

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 65
    :goto_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x1120010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->addFlags(I)V

    .line 68
    :cond_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 69
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 71
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    return-object v1

    .line 64
    :cond_1
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyConfirmDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d8

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    goto :goto_0
.end method
