.class public Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;
.super Ljava/lang/Object;
.source "AutoDetectManager.java"


# static fields
.field private static final DETECTED:Ljava/lang/String; = "DETECTED"

.field private static final EXPIRED:Ljava/lang/String; = "EXPIRED"

.field private static final TAG:Ljava/lang/String; = "AutoDetectManager"


# instance fields
.field private actionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

.field private expiredSec:I

.field private final mActionDetectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

.field private mContext:Landroid/content/Context;

.field private mDetectObservers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private final mTraumaDetectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

.field private mTraumaReceiver:Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/EmergencyActionListener;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "actionListener"    # Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->expiredSec:I

    .line 80
    new-instance v0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$1;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$1;-><init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mHandler:Landroid/os/Handler;

    .line 89
    new-instance v0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$2;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$2;-><init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mActionDetectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    .line 104
    new-instance v0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$3;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager$3;-><init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mTraumaDetectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    .line 45
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mContext:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->actionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mDetectObservers:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mTraumaDetectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;-><init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mTraumaReceiver:Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;

    .line 50
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->makeObserverList()V

    .line 51
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->stopActionDetect(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->actionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->startActionDetect()V

    return-void
.end method

.method private makeObserverList()V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mDetectObservers:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mActionDetectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;-><init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mDetectObservers:Ljava/util/ArrayList;

    new-instance v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mActionDetectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;-><init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method private startActionDetect()V
    .locals 6

    .prologue
    .line 67
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mDetectObservers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;

    .line 68
    .local v1, "observer":Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;
    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->startObserver()V

    goto :goto_0

    .line 70
    .end local v1    # "observer":Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->expiredSec:I

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 71
    return-void
.end method

.method private stopActionDetect(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 74
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mDetectObservers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;

    .line 75
    .local v1, "observer":Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;
    invoke-virtual {v1, p1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->stopObserver(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    .end local v1    # "observer":Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mTraumaReceiver:Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;

    invoke-virtual {v2}, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->readyReceiver()V

    .line 78
    return-void
.end method


# virtual methods
.method public turnOffDetect()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mTraumaReceiver:Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->unReadyReceiver()V

    .line 59
    return-void
.end method

.method public turnOnDetect()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectManager;->mTraumaReceiver:Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/autodetect/TraumaSignalReceiver;->readyReceiver()V

    .line 55
    return-void
.end method
