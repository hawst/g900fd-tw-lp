.class Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "EmergencyFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/EmergencyFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EmergencyStateReceiver"
.end annotation


# static fields
.field private static final BR_TAG:Ljava/lang/String; = "EmergencyStateReceiver"


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;


# direct methods
.method private constructor <init>(Lcom/sec/android/emergencymode/service/EmergencyFactory;)V
    .locals 0

    .prologue
    .line 426
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/emergencymode/service/EmergencyFactory;Lcom/sec/android/emergencymode/service/EmergencyFactory$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyFactory;
    .param p2, "x1"    # Lcom/sec/android/emergencymode/service/EmergencyFactory$1;

    .prologue
    .line 426
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;-><init>(Lcom/sec/android/emergencymode/service/EmergencyFactory;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 432
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    const-string v1, "EmergencyStateReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 437
    const-string v1, "reason"

    invoke-virtual {p2, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 438
    .local v0, "reason":I
    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 439
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyFactory;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->access$300(Lcom/sec/android/emergencymode/service/EmergencyFactory;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    move-result-object v1

    invoke-interface {v1, v5}, Lcom/sec/android/emergencymode/service/EmergencyActionListener;->onComplete(Z)V

    .line 440
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    const-string v2, "on"

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setProperties(Ljava/lang/String;)V

    goto :goto_0

    .line 441
    :cond_2
    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    .line 442
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyFactory;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->access$300(Lcom/sec/android/emergencymode/service/EmergencyFactory;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    move-result-object v1

    invoke-interface {v1, v4}, Lcom/sec/android/emergencymode/service/EmergencyActionListener;->onComplete(Z)V

    .line 443
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    const-string v2, "off"

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setProperties(Ljava/lang/String;)V

    goto :goto_0

    .line 444
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    .line 445
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    const-string v2, "off2on"

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setProperties(Ljava/lang/String;)V

    goto :goto_0

    .line 446
    :cond_4
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 447
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    const-string v2, "on2off"

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setProperties(Ljava/lang/String;)V

    goto :goto_0

    .line 449
    .end local v0    # "reason":I
    :cond_5
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 450
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->isEmergencyMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 451
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/EmergencyFactory;->mPackageCtrl:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    const-string v2, "SCREEN_OFF"

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->killUserBackgroundProcesses(Ljava/lang/String;)V

    .line 452
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v1, v4}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->setforceBlockUserPkg(Z)V

    goto/16 :goto_0

    .line 454
    :cond_6
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "android.intent.action.EMERGENCY_KNOX_FORCE_CLOSED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    const-string v1, "EmergencyStateReceiver"

    const-string v2, "KNOX Close complete. start emergency mode."

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 456
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyFactory$EmergencyStateReceiver;->this$0:Lcom/sec/android/emergencymode/service/EmergencyFactory;

    invoke-virtual {v1, v5}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->runEmergencyMode(Z)V

    goto/16 :goto_0
.end method
