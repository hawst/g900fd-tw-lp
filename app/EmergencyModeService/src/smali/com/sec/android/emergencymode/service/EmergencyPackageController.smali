.class public Lcom/sec/android/emergencymode/service/EmergencyPackageController;
.super Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;
.source "EmergencyPackageController.java"


# static fields
.field private static final KNOX_LAUNCHER:Ljava/lang/String; = "com.sec.android.app.launcher"

.field public static final PACKAGE_STATE_BACKUP:Ljava/lang/String; = "PackageStateBackup"

.field private static final TAG:Ljava/lang/String; = "EmergencyPackageController"

.field private static instance:Lcom/sec/android/emergencymode/service/EmergencyPackageController;


# instance fields
.field private mPM:Landroid/content/pm/PackageManager;

.field private mPackageStateBackup:Landroid/content/SharedPreferences;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyWhiteListManager;-><init>(Landroid/content/Context;)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPM:Landroid/content/pm/PackageManager;

    .line 66
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    const-string v1, "PackageStateBackup"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/service/EmergencyPackageController;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->clearAppListDB()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/emergencymode/service/EmergencyPackageController;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->makeAppListToEnable()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyPackageController;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # I
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyPackageController;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->sendBootComplete(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyPackageController;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->changeDefaultApplication(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/emergencymode/service/EmergencyPackageController;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->makeAppListToDisable()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/emergencymode/service/EmergencyPackageController;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->makeWidgetAllList()Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Ljava/util/ArrayList;Ljava/util/HashSet;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/EmergencyPackageController;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # Ljava/util/HashSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->divideWidgetAndApp(Ljava/util/ArrayList;Ljava/util/HashSet;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private changeDefaultApplication(Z)V
    .locals 5
    .param p1, "req"    # Z

    .prologue
    .line 383
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 384
    .local v1, "defaultAppChanger":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;>;"
    new-instance v3, Lcom/sec/android/emergencymode/service/packagectrl/WallpaperChanger;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/emergencymode/service/packagectrl/WallpaperChanger;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    new-instance v3, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/emergencymode/service/packagectrl/LauncherChanger;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    new-instance v3, Lcom/sec/android/emergencymode/service/packagectrl/SmsChanger;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/emergencymode/service/packagectrl/SmsChanger;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    new-instance v3, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    new-instance v3, Lcom/sec/android/emergencymode/service/packagectrl/NfcChanger;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/emergencymode/service/packagectrl/NfcChanger;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    new-instance v3, Lcom/sec/android/emergencymode/service/packagectrl/NotificationAccessChanger;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/emergencymode/service/packagectrl/NotificationAccessChanger;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;

    .line 392
    .local v0, "changer":Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;
    invoke-virtual {v0, p1}, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;->changeDefaultApp(Z)V

    goto :goto_0

    .line 394
    .end local v0    # "changer":Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;
    :cond_0
    return-void
.end method

.method private clearAppListDB()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 141
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 142
    .local v0, "resolver":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 143
    sget-object v1, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_DISABLEDPKG:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 145
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->clearAppListDBBackup()V

    .line 146
    return-void
.end method

.method private deleteDisabledList(Ljava/lang/String;)Z
    .locals 5
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 349
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->deleteDisabledListBackup(Ljava/lang/String;)V

    .line 350
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "pkg=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 351
    .local v1, "selection":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_DISABLEDPKG:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 352
    .local v0, "result":I
    if-lez v0, :cond_0

    .line 353
    const/4 v2, 0x1

    .line 355
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private divideWidgetAndApp(Ljava/util/ArrayList;Ljava/util/HashSet;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 536
    .local p1, "appAllList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p2, "widgetAllList":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 538
    .local v2, "widgetAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    .line 539
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 540
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 541
    .local v1, "thisApp":Ljava/lang/String;
    invoke-virtual {p2, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 543
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 544
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 545
    add-int/lit8 v0, v0, -0x1

    .line 539
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 550
    .end local v0    # "i":I
    .end local v1    # "thisApp":Ljava/lang/String;
    :cond_1
    return-object v2
.end method

.method private getEnabledSetting(Ljava/lang/String;)I
    .locals 9
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 359
    const/4 v8, 0x0

    .line 360
    .local v8, "enabledSetting":I
    const/4 v6, 0x0

    .line 362
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v0, 0x2

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "pkg"

    aput-object v4, v2, v0

    const/4 v0, 0x1

    const-string v4, "value"

    aput-object v4, v2, v0

    .line 363
    .local v2, "projection":[Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pkg=\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "\'"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 364
    .local v3, "selection":Ljava/lang/String;
    sget-object v1, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_DISABLEDPKG:Landroid/net/Uri;

    .line 365
    .local v1, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 366
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 367
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 369
    :cond_0
    const-string v0, "value"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 370
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 376
    :cond_1
    if-eqz v6, :cond_2

    .line 377
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 379
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    .end local v3    # "selection":Ljava/lang/String;
    :cond_2
    :goto_0
    return v8

    .line 372
    :catch_0
    move-exception v7

    .line 373
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 374
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getEnabledSettingBackup(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v8

    .line 376
    if-eqz v6, :cond_2

    .line 377
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 376
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 377
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencyPackageController;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->instance:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    if-nez v0, :cond_0

    .line 71
    const-string v0, "EmergencyPackageController"

    const-string v1, "instance is null!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->instance:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    .line 74
    :cond_0
    sget-object v0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->instance:Lcom/sec/android/emergencymode/service/EmergencyPackageController;

    return-object v0
.end method

.method private makeAppListToDisable()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 183
    iget-object v6, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPM:Landroid/content/pm/PackageManager;

    const/16 v7, 0x2200

    invoke-virtual {v6, v7}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v4

    .line 185
    .local v4, "temp":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 186
    .local v0, "appList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 188
    .local v2, "orderResult":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v4, :cond_1

    .line 189
    const-string v6, "EmergencyPackageController"

    const-string v7, "Application list is null "

    invoke-static {v6, v7}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_0
    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->makeContentValues(Ljava/util/HashMap;)[Landroid/content/ContentValues;

    move-result-object v5

    .line 208
    .local v5, "values":[Landroid/content/ContentValues;
    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->bulkInsertList([Landroid/content/ContentValues;)I

    .line 210
    return-object v2

    .line 191
    .end local v5    # "values":[Landroid/content/ContentValues;
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v1, v6, :cond_0

    .line 192
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    iget-object v3, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 194
    .local v3, "pkgName":Ljava/lang/String;
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    const/4 v7, 0x3

    if-eq v6, v7, :cond_2

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_3

    .line 191
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    :cond_3
    const/4 v6, 0x0

    const/4 v7, 0x7

    invoke-virtual {p0, v3, v6, v7}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkValidPackage(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 199
    const-string v6, "EmergencyPackageController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AllowedPackage Name : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 201
    :cond_4
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/ApplicationInfo;

    iget v6, v6, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private makeAppListToEnable()Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 150
    .local v5, "resultMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v4, "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 152
    .local v2, "forceEnableList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 154
    .local v0, "appState":I
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getForceEnableApplicationList()Ljava/util/ArrayList;

    move-result-object v2

    .line 155
    if-eqz v2, :cond_0

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-gtz v8, :cond_1

    .line 156
    :cond_0
    const-string v8, "EmergencyPackageController"

    const-string v9, "makeAppListToEnable : forceEnableList is empty. Do nothing."

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const/4 v4, 0x0

    .line 179
    .end local v4    # "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-object v4

    .line 160
    .restart local v4    # "resultList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 162
    .local v6, "toEnableApp":Ljava/lang/String;
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPM:Landroid/content/pm/PackageManager;

    invoke-virtual {v8, v6}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 169
    if-eqz v0, :cond_2

    const/4 v8, 0x1

    if-eq v0, v8, :cond_2

    .line 171
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v6, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 163
    :catch_0
    move-exception v1

    .line 164
    .local v1, "ex":Ljava/lang/IllegalArgumentException;
    const-string v8, "EmergencyPackageController"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] is abnormal package!"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 176
    .end local v1    # "ex":Ljava/lang/IllegalArgumentException;
    .end local v6    # "toEnableApp":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0, v5}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->makeContentValues(Ljava/util/HashMap;)[Landroid/content/ContentValues;

    move-result-object v7

    .line 177
    .local v7, "values":[Landroid/content/ContentValues;
    invoke-virtual {p0, v7}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->bulkInsertList([Landroid/content/ContentValues;)I

    goto :goto_0
.end method

.method private makeWidgetAllList()Ljava/util/HashSet;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 518
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 519
    .local v3, "result":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 520
    .local v2, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPM:Landroid/content/pm/PackageManager;

    const/16 v7, 0x200

    invoke-virtual {v6, v2, v7}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v5

    .line 522
    .local v5, "widgetBroadcastReceiver":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    if-nez v5, :cond_0

    const/4 v0, 0x0

    .line 526
    .local v0, "broadcastReceiverSize":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 527
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 528
    .local v4, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 526
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 522
    .end local v0    # "broadcastReceiverSize":I
    .end local v1    # "i":I
    .end local v4    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 531
    .restart local v0    # "broadcastReceiverSize":I
    .restart local v1    # "i":I
    :cond_1
    return-object v3
.end method

.method private sendBootComplete(Ljava/lang/String;)V
    .locals 3
    .param p1, "targetPkg"    # Ljava/lang/String;

    .prologue
    .line 512
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 513
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 514
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 515
    return-void
.end method

.method private sendBootComplete(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 505
    .local p1, "targetList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "EmergencyPackageController"

    const-string v3, "All package enabled. send BootComplete."

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 507
    .local v1, "pkg":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->sendBootComplete(Ljava/lang/String;)V

    goto :goto_0

    .line 509
    .end local v1    # "pkg":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setApplicationEnabledWithList(Ljava/util/ArrayList;IZZ)V
    .locals 6
    .param p2, "newState"    # I
    .param p3, "usePending"    # Z
    .param p4, "startNow"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;IZZ)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p1, "listPackage":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPM:Landroid/content/pm/PackageManager;

    const/4 v3, 0x0

    move-object v1, p1

    move v2, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/pm/PackageManager;->setApplicationEnabledSettingWithList(Ljava/util/List;IIZZ)V

    .line 138
    :cond_0
    return-void
.end method


# virtual methods
.method protected addAppToLauncher(Ljava/lang/String;Z)Z
    .locals 8
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    const/4 v7, 0x2

    .line 78
    const-string v4, "EmergencyPackageController"

    const-string v5, "User Package Changed"

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    const-string v4, "EmergencyPackageController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pkg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " enabled="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->isEmergencyMode()Z

    move-result v4

    if-eqz v4, :cond_2

    if-eqz p1, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 81
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getPackageGroupAsName(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 82
    .local v2, "group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    .line 83
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 84
    .local v1, "gr":Ljava/lang/String;
    invoke-virtual {p0, v1, p2, v7}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabled(Ljava/lang/String;ZI)V

    goto :goto_0

    .line 86
    .end local v1    # "gr":Ljava/lang/String;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    invoke-virtual {p0, p1, p2, v7}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->setApplicationEnabled(Ljava/lang/String;ZI)V

    .line 88
    :cond_1
    new-instance v0, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;-><init>(Landroid/content/Context;)V

    .line 89
    .local v0, "conUtil":Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;->needDataTrigger(Z)V

    .line 92
    .end local v0    # "conUtil":Lcom/sec/android/emergencymode/service/settingutils/ConnectivityUtility;
    .end local v2    # "group":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    const/4 v4, 0x1

    return v4
.end method

.method protected bulkInsertList([Landroid/content/ContentValues;)I
    .locals 2
    .param p1, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 335
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->bulkInsertListBackup([Landroid/content/ContentValues;)V

    .line 336
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_DISABLEDPKG:Landroid/net/Uri;

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method protected bulkInsertListBackup([Landroid/content/ContentValues;)V
    .locals 11
    .param p1, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 555
    const-string v8, "EmergencyPackageController"

    const-string v9, "bulkInsertListBackup!"

    invoke-static {v8, v9}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    const/4 v1, 0x0

    .line 558
    .local v1, "ed":Landroid/content/SharedPreferences$Editor;
    iget-object v9, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    monitor-enter v9

    .line 560
    :try_start_0
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 561
    move-object v0, p1

    .local v0, "arr$":[Landroid/content/ContentValues;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v7, v0, v3

    .line 562
    .local v7, "thisContent":Landroid/content/ContentValues;
    const-string v8, "pkg"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 563
    .local v5, "packageName":Ljava/lang/String;
    const-string v8, "value"

    invoke-virtual {v7, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 564
    .local v6, "packageState":Ljava/lang/String;
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 565
    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 561
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 572
    .end local v5    # "packageName":Ljava/lang/String;
    .end local v6    # "packageState":Ljava/lang/String;
    .end local v7    # "thisContent":Landroid/content/ContentValues;
    :cond_1
    if-eqz v1, :cond_2

    .line 573
    :try_start_1
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 576
    .end local v0    # "arr$":[Landroid/content/ContentValues;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_2
    :goto_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 577
    return-void

    .line 568
    :catch_0
    move-exception v2

    .line 569
    .local v2, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v8, "EmergencyPackageController"

    const-string v10, "bulkInsertListBackup error!"

    invoke-static {v8, v10}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 572
    if-eqz v1, :cond_2

    .line 573
    :try_start_3
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1

    .line 576
    .end local v2    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v8

    .line 572
    :catchall_1
    move-exception v8

    if-eqz v1, :cond_3

    .line 573
    :try_start_4
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_3
    throw v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method protected clearAppListDBBackup()V
    .locals 5

    .prologue
    .line 620
    const-string v2, "EmergencyPackageController"

    const-string v3, "clearAppListDBBackup!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    const/4 v0, 0x0

    .line 622
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    monitor-enter v3

    .line 624
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 625
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 630
    if-eqz v0, :cond_0

    .line 631
    :try_start_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 634
    :cond_0
    :goto_0
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635
    return-void

    .line 626
    :catch_0
    move-exception v1

    .line 627
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "EmergencyPackageController"

    const-string v4, "clearAppListDBBackup error!"

    invoke-static {v2, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 630
    if-eqz v0, :cond_0

    .line 631
    :try_start_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 634
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 630
    :catchall_1
    move-exception v2

    if-eqz v0, :cond_1

    .line 631
    :try_start_4
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method protected deleteDisabledListBackup(Ljava/lang/String;)V
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 602
    const-string v2, "EmergencyPackageController"

    const-string v3, "deleteDisabledListBackup!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    const/4 v0, 0x0

    .line 604
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    monitor-enter v3

    .line 606
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 607
    invoke-interface {v0, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 612
    if-eqz v0, :cond_0

    .line 613
    :try_start_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 616
    :cond_0
    :goto_0
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 617
    return-void

    .line 608
    :catch_0
    move-exception v1

    .line 609
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "EmergencyPackageController"

    const-string v4, "deleteDisabledListBackup error!"

    invoke-static {v2, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 610
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 612
    if-eqz v0, :cond_0

    .line 613
    :try_start_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 616
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 612
    :catchall_1
    move-exception v2

    if-eqz v0, :cond_1

    .line 613
    :try_start_4
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method protected getEnabledSettingBackup(Ljava/lang/String;)I
    .locals 5
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 638
    const-string v2, "EmergencyPackageController"

    const-string v3, "getEnabledSettingBackup!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    monitor-enter v3

    .line 642
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    const-string v4, ""

    invoke-interface {v2, p1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 648
    .local v1, "packageState":I
    :goto_0
    :try_start_1
    monitor-exit v3

    .line 649
    return v1

    .line 643
    .end local v1    # "packageState":I
    :catch_0
    move-exception v0

    .line 644
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "EmergencyPackageController"

    const-string v4, "getEnabledSettingBackup error!"

    invoke-static {v2, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 646
    const/4 v1, 0x0

    .restart local v1    # "packageState":I
    goto :goto_0

    .line 648
    .end local v0    # "ex":Ljava/lang/Exception;
    .end local v1    # "packageState":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method protected insertDisabledList(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "enableSetting"    # I

    .prologue
    .line 340
    const-string v1, "EmergencyPackageController"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "insertDisabledList : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 342
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "pkg"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const-string v1, "value"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->insertDisabledListBackup(Ljava/lang/String;I)V

    .line 345
    sget-object v1, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_DISABLEDPKG:Landroid/net/Uri;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Z

    move-result v1

    return v1
.end method

.method protected insertDisabledListBackup(Ljava/lang/String;I)V
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "enableSetting"    # I

    .prologue
    .line 580
    const-string v3, "EmergencyPackageController"

    const-string v4, "insertDisabledListBackup!"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    const/4 v0, 0x0

    .line 583
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    monitor-enter v4

    .line 585
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 586
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    .line 587
    .local v2, "packageState":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 588
    invoke-interface {v0, p1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 594
    :cond_0
    if-eqz v0, :cond_1

    .line 595
    :try_start_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 598
    .end local v2    # "packageState":Ljava/lang/String;
    :cond_1
    :goto_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 599
    return-void

    .line 590
    :catch_0
    move-exception v1

    .line 591
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_2
    const-string v3, "EmergencyPackageController"

    const-string v5, "insertDisabledListBackup error!"

    invoke-static {v3, v5}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 594
    if-eqz v0, :cond_1

    .line 595
    :try_start_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 598
    .end local v1    # "ex":Ljava/lang/Exception;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v3

    .line 594
    :catchall_1
    move-exception v3

    if-eqz v0, :cond_2

    .line 595
    :try_start_4
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_2
    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method protected killRecentTaskList()V
    .locals 10

    .prologue
    const/16 v9, 0x64

    .line 477
    iget-object v6, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    const-string v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 478
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v6, 0x6

    invoke-virtual {v0, v9, v6}, Landroid/app/ActivityManager;->getRecentTasks(II)Ljava/util/List;

    move-result-object v5

    .line 482
    .local v5, "recentTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    if-eqz v5, :cond_2

    .line 483
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RecentTaskInfo;

    .line 485
    .local v3, "info":Landroid/app/ActivityManager$RecentTaskInfo;
    :try_start_0
    iget-object v6, v3, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-virtual {v6}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 486
    .local v4, "pkgName":Ljava/lang/String;
    const-string v6, "EmergencyPackageController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "task list ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]  userId ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Landroid/app/ActivityManager$RecentTaskInfo;->userId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    const-string v6, "com.android.settings"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "com.sec.android.app.camera"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "com.samsung.android.app.cocktailbarservice"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "com.samsung.contacts"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "com.android.contacts"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    iget v6, v3, Landroid/app/ActivityManager$RecentTaskInfo;->userId:I

    if-lt v6, v9, :cond_0

    .line 493
    :cond_1
    iget v6, v3, Landroid/app/ActivityManager$RecentTaskInfo;->persistentId:I

    const/4 v7, 0x1

    invoke-virtual {v0, v6, v7}, Landroid/app/ActivityManager;->removeTask(II)Z

    .line 494
    const-string v6, "EmergencyPackageController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "kill Setting Task. "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Landroid/app/ActivityManager$RecentTaskInfo;->persistentId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 496
    .end local v4    # "pkgName":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 497
    .local v1, "e":Ljava/lang/Exception;
    const-string v6, "EmergencyPackageController"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception on killing Recent Task(id) : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Landroid/app/ActivityManager$RecentTaskInfo;->persistentId:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 498
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 502
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "info":Landroid/app/ActivityManager$RecentTaskInfo;
    :cond_2
    return-void
.end method

.method protected killRunningProcesses(Ljava/util/HashSet;)V
    .locals 13
    .param p1, "allPkgList"    # Ljava/util/HashSet;

    .prologue
    .line 425
    :try_start_0
    const-string v10, "EmergencyPackageController"

    const-string v11, "killRunningProcesses"

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    if-nez p1, :cond_1

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 429
    .local v0, "am":Landroid/app/IActivityManager;
    if-eqz v0, :cond_0

    .line 430
    invoke-interface {v0}, Landroid/app/IActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v9

    .line 431
    .local v9, "processList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-eqz v9, :cond_0

    .line 432
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 433
    .local v5, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v1, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_2

    aget-object v8, v1, v4

    .line 434
    .local v8, "pkg":Ljava/lang/String;
    invoke-virtual {p1, v8}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 435
    const-string v10, "EmergencyPackageController"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "killRunningProcesses : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " / pid ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] uid ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const/4 v10, 0x1

    new-array v7, v10, [I

    .line 437
    .local v7, "pids":[I
    const/4 v10, 0x0

    iget v11, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    aput v11, v7, v10

    .line 438
    const-string v10, "EmergencyMode"

    const/4 v11, 0x1

    invoke-interface {v0, v7, v10, v11}, Landroid/app/IActivityManager;->killPids([ILjava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 433
    .end local v7    # "pids":[I
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 444
    .end local v0    # "am":Landroid/app/IActivityManager;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    .end local v6    # "len$":I
    .end local v8    # "pkg":Ljava/lang/String;
    .end local v9    # "processList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :catch_0
    move-exception v2

    .line 445
    .local v2, "e":Ljava/lang/Exception;
    const-string v10, "EmergencyPackageController"

    const-string v11, "killRunningProcesses Exception!"

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected killThisPackage(Ljava/lang/String;)V
    .locals 13
    .param p1, "thisPackage"    # Ljava/lang/String;

    .prologue
    .line 452
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 474
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v0

    .line 455
    .local v0, "am":Landroid/app/IActivityManager;
    if-eqz v0, :cond_0

    .line 456
    invoke-interface {v0}, Landroid/app/IActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v9

    .line 457
    .local v9, "processList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-eqz v9, :cond_0

    .line 458
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 459
    .local v5, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v1, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v6, v1

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_2

    aget-object v8, v1, v4

    .line 460
    .local v8, "pkg":Ljava/lang/String;
    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 461
    const-string v10, "EmergencyPackageController"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "killThisPackage : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " / pid ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] uid ["

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->uid:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    const/4 v10, 0x1

    new-array v7, v10, [I

    .line 463
    .local v7, "pids":[I
    const/4 v10, 0x0

    iget v11, v5, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    aput v11, v7, v10

    .line 464
    const-string v10, "EmergencyMode"

    const/4 v11, 0x1

    invoke-interface {v0, v7, v10, v11}, Landroid/app/IActivityManager;->killPids([ILjava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    .end local v7    # "pids":[I
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 470
    .end local v0    # "am":Landroid/app/IActivityManager;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v4    # "i$":I
    .end local v5    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    .end local v6    # "len$":I
    .end local v8    # "pkg":Ljava/lang/String;
    .end local v9    # "processList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    :catch_0
    move-exception v2

    .line 471
    .local v2, "e":Ljava/lang/Exception;
    const-string v10, "EmergencyPackageController"

    const-string v11, "killThisPackage Exception!"

    invoke-static {v10, v11}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected killUserBackgroundProcesses(Ljava/lang/String;)V
    .locals 17
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 398
    :try_start_0
    const-string v14, "EmergencyPackageController"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Kill background process : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    .line 400
    .local v1, "am":Landroid/app/IActivityManager;
    const/4 v14, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getPackageList(I)Ljava/util/ArrayList;

    move-result-object v13

    .line 401
    .local v13, "userAllowed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getPackageList(I)Ljava/util/ArrayList;

    move-result-object v3

    .line 402
    .local v3, "defaultAllowed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_2

    .line 403
    invoke-interface {v1}, Landroid/app/IActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v12

    .line 404
    .local v12, "processList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    if-eqz v12, :cond_2

    .line 405
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 406
    .local v7, "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget-object v11, v7, Landroid/app/ActivityManager$RunningAppProcessInfo;->pkgList:[Ljava/lang/String;

    .line 407
    .local v11, "pkgList":[Ljava/lang/String;
    move-object v2, v11

    .local v2, "arr$":[Ljava/lang/String;
    array-length v8, v2

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_0
    if-ge v6, v8, :cond_0

    aget-object v10, v2, v6

    .line 408
    .local v10, "pkg":Ljava/lang/String;
    invoke-virtual {v13, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1

    sget v14, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->PERMISSION_HAS_DEFAULT_LEVEL:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v10, v14}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkWhiteListPermission(Ljava/lang/String;I)Z

    move-result v14

    if-nez v14, :cond_1

    .line 409
    const-string v14, "EmergencyPackageController"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Kill user_allowed process : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " / "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget v0, v7, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 410
    const/4 v14, 0x1

    new-array v9, v14, [I

    .line 411
    .local v9, "pids":[I
    const/4 v14, 0x0

    iget v15, v7, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    aput v15, v9, v14

    .line 412
    const-string v14, "EmergencyMode"

    const/4 v15, 0x1

    invoke-interface {v1, v9, v14, v15}, Landroid/app/IActivityManager;->killPids([ILjava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 407
    .end local v9    # "pids":[I
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 418
    .end local v1    # "am":Landroid/app/IActivityManager;
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "defaultAllowed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "i$":I
    .end local v7    # "info":Landroid/app/ActivityManager$RunningAppProcessInfo;
    .end local v8    # "len$":I
    .end local v10    # "pkg":Ljava/lang/String;
    .end local v11    # "pkgList":[Ljava/lang/String;
    .end local v12    # "processList":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    .end local v13    # "userAllowed":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v4

    .line 419
    .local v4, "e":Ljava/lang/Exception;
    const-string v14, "EmergencyPackageController"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Kill Process Exception : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_2
    return-void
.end method

.method protected loadEmergencyDisabledAppList(I)Ljava/util/ArrayList;
    .locals 9
    .param p1, "flag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 301
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 302
    .local v7, "disabledList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    .line 304
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "value=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 305
    .local v3, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/emergencymode/EmergencyConstants;->URI_DISABLEDPKG:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 306
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 307
    invoke-interface {v6}, Landroid/database/Cursor;->moveToLast()Z

    .line 309
    :cond_0
    const-string v0, "pkg"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 310
    invoke-interface {v6}, Landroid/database/Cursor;->moveToPrevious()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 316
    :cond_1
    if-eqz v6, :cond_2

    .line 317
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 319
    .end local v3    # "selection":Ljava/lang/String;
    :cond_2
    :goto_0
    return-object v7

    .line 312
    :catch_0
    move-exception v8

    .line 313
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V

    .line 314
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->loadEmergencyDisabledAppListBackup(I)Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v7

    .line 316
    if-eqz v6, :cond_2

    .line 317
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 316
    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 317
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method protected loadEmergencyDisabledAppListBackup(I)Ljava/util/ArrayList;
    .locals 10
    .param p1, "flag"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 653
    const-string v7, "EmergencyPackageController"

    const-string v8, "loadEmergencyDisabledAppListBackup!"

    invoke-static {v7, v8}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 654
    const/4 v0, 0x0

    .line 655
    .local v0, "allPkgMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;*>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 656
    .local v4, "pkgList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v8, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    monitor-enter v8

    .line 658
    :try_start_0
    iget-object v7, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPackageStateBackup:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 659
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 660
    .local v3, "keySets":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 664
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 665
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 667
    .local v6, "thisPkg":Ljava/lang/String;
    :try_start_1
    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v5

    .line 672
    .local v5, "pkgState":I
    :goto_1
    if-ne p1, v5, :cond_0

    .line 673
    :try_start_2
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 677
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v3    # "keySets":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "pkgState":I
    .end local v6    # "thisPkg":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 678
    .local v1, "ex":Ljava/lang/Exception;
    :try_start_3
    const-string v7, "EmergencyPackageController"

    const-string v9, "loadEmergencyDisabledAppListBackup error!"

    invoke-static {v7, v9}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 681
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_1
    monitor-exit v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 682
    return-object v4

    .line 668
    .restart local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v3    # "keySets":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v6    # "thisPkg":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 669
    .restart local v1    # "ex":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 670
    const/4 v5, -0x1

    .restart local v5    # "pkgState":I
    goto :goto_1

    .line 681
    .end local v1    # "ex":Ljava/lang/Exception;
    .end local v2    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v3    # "keySets":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v5    # "pkgState":I
    .end local v6    # "thisPkg":Ljava/lang/String;
    :catchall_0
    move-exception v7

    :try_start_5
    monitor-exit v8
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v7
.end method

.method protected makeContentValues(Ljava/util/HashMap;)[Landroid/content/ContentValues;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)[",
            "Landroid/content/ContentValues;"
        }
    .end annotation

    .prologue
    .line 323
    .local p1, "orderList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v4

    new-array v3, v4, [Landroid/content/ContentValues;

    .line 324
    .local v3, "values":[Landroid/content/ContentValues;
    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 325
    .local v1, "pkgs":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v4, v1

    if-ge v0, v4, :cond_0

    .line 326
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 327
    .local v2, "value":Landroid/content/ContentValues;
    const-string v4, "pkg"

    aget-object v5, v1, v0

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    const-string v5, "value"

    aget-object v4, v1, v0

    invoke-virtual {p1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    aput-object v2, v3, v0

    .line 325
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 331
    .end local v2    # "value":Landroid/content/ContentValues;
    :cond_0
    return-object v3
.end method

.method protected runApplicationOperation(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 214
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->resetAllowedApplicationList()V

    .line 215
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->killRecentTaskList()V

    .line 216
    new-instance v0, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController$1;-><init>(Lcom/sec/android/emergencymode/service/EmergencyPackageController;Z)V

    .line 296
    .local v0, "runEmergencyModeThread":Ljava/lang/Thread;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 297
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 298
    return-void
.end method

.method protected setApplicationEnabled(Ljava/lang/String;ZI)V
    .locals 7
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "allowFlag"    # I

    .prologue
    .line 98
    if-nez p1, :cond_0

    .line 99
    :try_start_0
    const-string v4, "EmergencyPackageController"

    const-string v5, "setApplicationEnabled pkgName is null"

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :goto_0
    return-void

    .line 102
    :cond_0
    const/4 v4, 0x2

    if-ne p3, v4, :cond_3

    .line 103
    if-eqz p2, :cond_2

    .line 104
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->insertWhiteList(Ljava/lang/String;)Z

    .line 108
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->resetAllowedApplicationList()V

    .line 109
    sget v4, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->PERMISSION_NOT_DISABLE:I

    invoke-virtual {p0, p1, v4}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkWhiteListPermission(Ljava/lang/String;I)Z

    move-result v4

    if-nez v4, :cond_1

    const/16 v4, 0x440

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->checkModeType(I)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "FMM"

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getPackageGroup(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 111
    :cond_1
    const-string v4, "EmergencyPackageController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pkg="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is default allowed package."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "EmergencyPackageController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Can\'t control pkg bcz "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->deleteWhiteList(Ljava/lang/String;)Z

    goto :goto_1

    .line 115
    :cond_3
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPM:Landroid/content/pm/PackageManager;

    const/16 v5, 0x2200

    invoke-virtual {v4, p1, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 116
    .local v3, "info":Landroid/content/pm/ApplicationInfo;
    iget v1, v3, Landroid/content/pm/ApplicationInfo;->enabledSetting:I

    .line 117
    .local v1, "enableSetting":I
    if-eqz p2, :cond_5

    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->getEnabledSetting(Ljava/lang/String;)I

    move-result v2

    .line 118
    .local v2, "flag":I
    :goto_2
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->mPM:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    invoke-virtual {v4, p1, v2, v5}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 119
    if-eqz p2, :cond_4

    .line 120
    const-string v4, "EmergencyPackageController"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " package enabled. send BootComplete."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->sendBootComplete(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 127
    :cond_4
    if-eqz p2, :cond_6

    .line 128
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->deleteDisabledList(Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 117
    .end local v2    # "flag":I
    :cond_5
    const/4 v2, 0x3

    goto :goto_2

    .line 130
    .restart local v2    # "flag":I
    :cond_6
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/emergencymode/service/EmergencyPackageController;->insertDisabledList(Ljava/lang/String;I)Z

    goto/16 :goto_0
.end method
