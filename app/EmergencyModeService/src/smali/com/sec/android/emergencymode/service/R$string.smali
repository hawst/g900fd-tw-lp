.class public final Lcom/sec/android/emergencymode/service/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final EmptyString:I = 0x7f090028

.field public static final TurnOffBluetooth:I = 0x7f090029

.field public static final app_name:I = 0x7f090002

.field public static final calendar_date_format:I = 0x7f090000

.field public static final choice_dialog_emergency_mode_disable_msg:I = 0x7f090003

.field public static final choice_dialog_emergency_mode_info:I = 0x7f09000d

.field public static final choice_dialog_emergency_mode_info_1:I = 0x7f090005

.field public static final choice_dialog_emergency_mode_info_2:I = 0x7f090006

.field public static final choice_dialog_emergency_mode_info_3:I = 0x7f090007

.field public static final choice_dialog_emergency_mode_info_4:I = 0x7f090008

.field public static final choice_dialog_emergency_mode_info_5:I = 0x7f090009

.field public static final choice_dialog_emergency_mode_info_6:I = 0x7f09000a

.field public static final choice_dialog_emergency_mode_info_end:I = 0x7f09000b

.field public static final choice_dialog_emergency_mode_info_end_2:I = 0x7f09000c

.field public static final choice_dialog_emergency_mode_info_header:I = 0x7f090004

.field public static final choice_dialog_upsm_disable_msg:I = 0x7f090014

.field public static final choice_dialog_upsm_info:I = 0x7f09000e

.field public static final choice_dialog_upsm_info_end:I = 0x7f09001b

.field public static final choice_dialog_upsm_info_header:I = 0x7f090015

.field public static final choice_dialog_upsm_mode_info_1:I = 0x7f090016

.field public static final choice_dialog_upsm_mode_info_2:I = 0x7f090017

.field public static final choice_dialog_upsm_mode_info_3:I = 0x7f090018

.field public static final choice_dialog_upsm_mode_info_4:I = 0x7f090019

.field public static final choice_dialog_upsm_mode_info_5:I = 0x7f09001a

.field public static final configuring_device_please_wait:I = 0x7f09003c

.field public static final confirm_dialog_emergency_mode_info_fmm:I = 0x7f090010

.field public static final disable:I = 0x7f090038

.field public static final disable_emergency_mode:I = 0x7f09003b

.field public static final emergency_mode:I = 0x7f090039

.field public static final emergency_start_progressing:I = 0x7f090011

.field public static final emergency_stop_progressing:I = 0x7f090012

.field public static final emergency_tts_guidance_msg:I = 0x7f090027

.field public static final enable:I = 0x7f090037

.field public static final enable_emergency_mode:I = 0x7f09003a

.field public static final gadget_UPSM_text:I = 0x7f090036

.field public static final gadget_emergencymode_text:I = 0x7f090035

.field public static final hello:I = 0x7f090001

.field public static final hours:I = 0x7f090022

.field public static final min:I = 0x7f090023

.field public static final percentage:I = 0x7f090024

.field public static final power_usage_summary_title:I = 0x7f09001e

.field public static final powersaving_day:I = 0x7f090020

.field public static final powersaving_days:I = 0x7f090021

.field public static final powersaving_low_battery:I = 0x7f090026

.field public static final safetycare_disclaimer_emergency_desc1:I = 0x7f09002c

.field public static final safetycare_disclaimer_emergency_desc2:I = 0x7f09002d

.field public static final safetycare_disclaimer_emergency_desc3:I = 0x7f09002e

.field public static final safetycare_disclaimer_emergency_desc4:I = 0x7f09002f

.field public static final safetycare_disclaimer_emergency_desc5:I = 0x7f090030

.field public static final safetycare_disclaimer_emergency_desc6:I = 0x7f090031

.field public static final safetycare_disclaimer_emergency_desc7:I = 0x7f090032

.field public static final safetycare_disclaimer_emergency_desc8:I = 0x7f090033

.field public static final safetycare_disclaimer_title:I = 0x7f09002a

.field public static final safetycare_disclaimer_upsm_desc1:I = 0x7f090034

.field public static final safetycare_terms_and_conditions_checkbox_text:I = 0x7f09002b

.field public static final stms_appgroup:I = 0x7f09003d

.field public static final timer_dialog_emergency_mode_info_fmm:I = 0x7f09000f

.field public static final ultra_power_saving_mode:I = 0x7f090013

.field public static final upsm_start_progressing:I = 0x7f09001c

.field public static final upsm_stop_progressing:I = 0x7f09001d

.field public static final usage_time:I = 0x7f090025

.field public static final usage_time_left:I = 0x7f09001f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
