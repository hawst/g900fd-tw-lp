.class public Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;
.super Landroid/app/Service;
.source "EmergencyServiceStarter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EmergencyServiceStarter"

.field public static lastAction:Ljava/lang/String;

.field public static mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;


# instance fields
.field private serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private setProcessForeground(Landroid/os/IBinder;Z)V
    .locals 5
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "start"    # Z

    .prologue
    .line 34
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    .line 36
    .local v1, "mAm":Landroid/app/IActivityManager;
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 37
    :try_start_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-interface {v1, p1, v2, p2}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V

    .line 38
    const-string v2, "EmergencyServiceStarter"

    const-string v3, "setProcessForeground"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    :cond_0
    :goto_0
    return-void

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 42
    const-string v2, "EmergencyServiceStarter"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setProcessForeground fail e : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 167
    const-string v0, "EmergencyServiceStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBind: intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    const-string v0, "EmergencyServiceStarter"

    const-string v1, "Block try to bind other module."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 48
    const-string v0, "EmergencyServiceStarter"

    const-string v1, "onCreate: called"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v0, "emergency_service"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_1

    .line 50
    const-string v0, "EmergencyServiceStarter"

    const-string v1, "Creating EmergencyManagerService!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-static {p0}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    .line 53
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    if-eqz v0, :cond_0

    .line 54
    const-string v0, "emergency_service"

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    invoke-static {v0, v1}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 55
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/IEmergencyManager$Stub;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->setProcessForeground(Landroid/os/IBinder;Z)V

    .line 56
    const-string v0, "EmergencyServiceStarter"

    const-string v1, "Service Reg successfully"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :goto_0
    return-void

    .line 58
    :cond_0
    const-string v0, "EmergencyServiceStarter"

    const-string v1, "Service create failed"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 61
    :cond_1
    const-string v0, "EmergencyServiceStarter"

    const-string v1, "Service already exists"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 67
    const-string v1, "EmergencyServiceStarter"

    const-string v2, "onDestroy"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 69
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    if-eqz v1, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    check-cast v0, Lcom/sec/android/emergencymode/service/EmergencyManagerService;

    .line 71
    .local v0, "mgrService":Lcom/sec/android/emergencymode/service/EmergencyManagerService;
    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->destroyInstance()V

    .line 73
    .end local v0    # "mgrService":Lcom/sec/android/emergencymode/service/EmergencyManagerService;
    :cond_0
    iput-object v3, p0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    .line 74
    sput-object v3, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    .line 75
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    .line 76
    return-void
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 180
    const-string v0, "EmergencyServiceStarter"

    const-string v1, "onLowMemory"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 185
    const-string v0, "EmergencyServiceStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRebind"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 17
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 80
    const-string v14, "EmergencyServiceStarter"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "onStartCommand() "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-static/range {p0 .. p0}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    .line 82
    sput-object p0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->mService:Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;

    .line 83
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->serviceIfc:Lcom/sec/android/emergencymode/IEmergencyManager$Stub;

    check-cast v10, Lcom/sec/android/emergencymode/service/EmergencyManagerService;

    .line 84
    .local v10, "mgrService":Lcom/sec/android/emergencymode/service/EmergencyManagerService;
    if-eqz p1, :cond_a

    .line 86
    const-string v14, "clearBootTime"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 88
    :try_start_0
    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->clearBootTime()V

    .line 89
    const/4 v14, 0x1

    invoke-virtual {v10, v14}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->setReadyToStart(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :cond_0
    :goto_0
    const-string v14, "initForEMState"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 97
    :try_start_1
    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->initForEMState()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 103
    :cond_1
    :goto_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 104
    .local v1, "action":Ljava/lang/String;
    sput-object v1, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->lastAction:Ljava/lang/String;

    .line 105
    if-eqz v1, :cond_8

    const-string v14, "android.intent.action.EMERGENCY_START_SERVICE_BY_ORDER"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 106
    const-string v14, "enabled"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 107
    .local v4, "enabled":Z
    const-string v14, "flag"

    const/4 v15, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 108
    .local v7, "flag":I
    const-string v14, "skipdialog"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    .line 109
    .local v13, "skipdialog":Z
    const/4 v12, 0x0

    .line 110
    .local v12, "result":I
    const/4 v8, 0x0

    .line 111
    .local v8, "isBootFinished":Z
    const/4 v9, 0x0

    .line 114
    .local v9, "isReadyToStart":Z
    :try_start_2
    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->isBootFinished()Z

    move-result v8

    .line 115
    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->getReadyToStart()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result v9

    .line 120
    :goto_2
    if-eqz v8, :cond_7

    if-eqz v9, :cond_7

    .line 123
    :try_start_3
    new-instance v2, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;-><init>(Landroid/content/Context;)V

    .line 124
    .local v2, "disclaimerCaller":Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;
    invoke-virtual {v2, v4, v7, v13}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->callDisclaimer(ZIZ)Z

    move-result v14

    if-nez v14, :cond_2

    .line 125
    invoke-virtual {v10, v4, v7, v13}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->triggerEmergencyMode(ZIZ)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v12

    .line 131
    :cond_2
    if-gez v12, :cond_4

    .line 132
    invoke-virtual {v10, v12}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->notifyCurrentState(I)V

    .line 133
    const-string v14, "EmergencyServiceStarter"

    const-string v15, "trigger failed."

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    .end local v1    # "action":Ljava/lang/String;
    .end local v2    # "disclaimerCaller":Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;
    .end local v4    # "enabled":Z
    .end local v7    # "flag":I
    .end local v8    # "isBootFinished":Z
    .end local v9    # "isReadyToStart":Z
    .end local v12    # "result":I
    .end local v13    # "skipdialog":Z
    :cond_3
    :goto_3
    const/4 v14, 0x1

    return v14

    .line 90
    :catch_0
    move-exception v5

    .line 91
    .local v5, "ex":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 98
    .end local v5    # "ex":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    .line 99
    .restart local v5    # "ex":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 116
    .end local v5    # "ex":Ljava/lang/Exception;
    .restart local v1    # "action":Ljava/lang/String;
    .restart local v4    # "enabled":Z
    .restart local v7    # "flag":I
    .restart local v8    # "isBootFinished":Z
    .restart local v9    # "isReadyToStart":Z
    .restart local v12    # "result":I
    .restart local v13    # "skipdialog":Z
    :catch_2
    move-exception v5

    .line 117
    .restart local v5    # "ex":Ljava/lang/Exception;
    const-string v14, "EmergencyServiceStarter"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Checking isBootFinished was failed : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 135
    .end local v5    # "ex":Ljava/lang/Exception;
    .restart local v2    # "disclaimerCaller":Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;
    :cond_4
    const-string v14, "EmergencyServiceStarter"

    const-string v15, "trigger success."

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 127
    .end local v2    # "disclaimerCaller":Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;
    :catch_3
    move-exception v3

    .line 128
    .local v3, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v14, "EmergencyServiceStarter"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "trigger failed. e : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 129
    const/16 v12, -0x9

    .line 131
    if-gez v12, :cond_5

    .line 132
    invoke-virtual {v10, v12}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->notifyCurrentState(I)V

    .line 133
    const-string v14, "EmergencyServiceStarter"

    const-string v15, "trigger failed."

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 135
    :cond_5
    const-string v14, "EmergencyServiceStarter"

    const-string v15, "trigger success."

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 131
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v14

    if-gez v12, :cond_6

    .line 132
    invoke-virtual {v10, v12}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->notifyCurrentState(I)V

    .line 133
    const-string v15, "EmergencyServiceStarter"

    const-string v16, "trigger failed."

    invoke-static/range {v15 .. v16}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :goto_4
    throw v14

    :cond_6
    const-string v15, "EmergencyServiceStarter"

    const-string v16, "trigger success."

    invoke-static/range {v15 .. v16}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 140
    :cond_7
    const v14, 0x7f09003c

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    .line 141
    const/16 v14, -0xa

    invoke-virtual {v10, v14}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->notifyCurrentState(I)V

    goto/16 :goto_3

    .line 143
    .end local v4    # "enabled":Z
    .end local v7    # "flag":I
    .end local v8    # "isBootFinished":Z
    .end local v9    # "isReadyToStart":Z
    .end local v12    # "result":I
    .end local v13    # "skipdialog":Z
    :cond_8
    if-eqz v1, :cond_3

    const-string v14, "android.intent.action.EMERGENCY_CHECK_ABNORMAL_STATE"

    invoke-virtual {v1, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 144
    const-string v14, "em_preference"

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/emergencymode/service/EmergencyServiceStarter;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 145
    .local v11, "pref":Landroid/content/SharedPreferences;
    const-string v14, "fcstate"

    const/4 v15, 0x0

    invoke-interface {v11, v14, v15}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 146
    .local v6, "fcstate":Z
    const-string v14, "EmergencyServiceStarter"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "FCState : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    if-nez v6, :cond_9

    .line 148
    const-string v14, "EmergencyServiceStarter"

    const-string v15, "Not FC State stop service."

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 150
    :cond_9
    const-string v14, "EmergencyServiceStarter"

    const-string v15, "abnormal state. keep service."

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 155
    .end local v1    # "action":Ljava/lang/String;
    .end local v6    # "fcstate":Z
    .end local v11    # "pref":Landroid/content/SharedPreferences;
    :cond_a
    const-string v14, "EmergencyServiceStarter"

    const-string v15, "Intent is NULL!!! : Check that mode changing was ongoing"

    invoke-static {v14, v15}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :try_start_5
    invoke-virtual {v10}, Lcom/sec/android/emergencymode/service/EmergencyManagerService;->checkForceRecovery()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_3

    .line 158
    :catch_4
    move-exception v3

    .line 159
    .restart local v3    # "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 3
    .param p1, "rootIntent"    # Landroid/content/Intent;

    .prologue
    .line 190
    const-string v0, "EmergencyServiceStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onTaskRemoved"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 174
    const-string v0, "EmergencyServiceStarter"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUnbind: intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
