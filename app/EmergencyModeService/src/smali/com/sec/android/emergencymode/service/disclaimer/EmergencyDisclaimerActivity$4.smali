.class Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity$4;
.super Ljava/lang/Object;
.source "EmergencyDisclaimerActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    const/4 v2, 0x0

    .line 220
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;

    # getter for: Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->mAllAgreeCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->access$000(Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 221
    const-string v0, "CHN"

    const-string v1, "OPEN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->setOKButton()V

    .line 230
    :goto_0
    return-void

    .line 224
    :cond_0
    if-eqz p2, :cond_1

    .line 225
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;

    # getter for: Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->okButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->access$100(Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->setEnabledButton(Landroid/widget/Button;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->access$200(Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;Landroid/widget/Button;Z)V

    goto :goto_0

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;

    # getter for: Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->okButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->access$100(Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;)Landroid/widget/Button;

    move-result-object v1

    # invokes: Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->setEnabledButton(Landroid/widget/Button;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;->access$200(Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;Landroid/widget/Button;Z)V

    goto :goto_0
.end method
