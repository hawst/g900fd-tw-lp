.class public Lcom/sec/android/emergencymode/util/TalkBackUtil;
.super Ljava/lang/Object;
.source "TalkBackUtil.java"

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# static fields
.field private static final END_OF_SPEECH:Ljava/lang/String; = "TTS_END"

.field private static final TAG:Ljava/lang/String; = "TalkBackUtils"

.field private static mInstance:Lcom/sec/android/emergencymode/util/TalkBackUtil;


# instance fields
.field private mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private final mProgressListener:Landroid/speech/tts/UtteranceProgressListener;

.field private mTTS:Landroid/speech/tts/TextToSpeech;

.field private oldVolumeLevel:I

.field private text:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Lcom/sec/android/emergencymode/util/TalkBackUtil$1;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/util/TalkBackUtil$1;-><init>(Lcom/sec/android/emergencymode/util/TalkBackUtil;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    .line 58
    const-string v0, "TalkBackUtils"

    const-string v1, "TalkBackUtil instance create!!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iput-object p1, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mContext:Landroid/content/Context;

    .line 60
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mAudioManager:Landroid/media/AudioManager;

    .line 61
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/util/TalkBackUtil;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    sget-object v0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mInstance:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/sec/android/emergencymode/util/TalkBackUtil;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/util/TalkBackUtil;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mInstance:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    .line 67
    :cond_0
    sget-object v0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mInstance:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    return-object v0
.end method

.method private isAccessibilityEnabled()Z
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    .line 120
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTalkBackEnabled(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x0

    .line 157
    const/16 v1, 0x3a

    .line 158
    .local v1, "ENABLED_SERVICES_SEPARATOR":C
    const-string v0, "com.google.android.marvin.talkback"

    .line 159
    .local v0, "DEFAULT_SCREENREADER_NAME":Ljava/lang/String;
    new-instance v6, Landroid/text/TextUtils$SimpleStringSplitter;

    const/16 v8, 0x3a

    invoke-direct {v6, v8}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    .line 161
    .local v6, "sStringColonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    if-nez p0, :cond_1

    .line 186
    :cond_0
    :goto_0
    return v7

    .line 164
    :cond_1
    const/4 v5, 0x0

    .line 165
    .local v5, "enabledServicesSetting":Ljava/lang/String;
    if-eqz p0, :cond_2

    .line 166
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const-string v9, "enabled_accessibility_services"

    invoke-static {v8, v9}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 168
    :cond_2
    if-nez v5, :cond_3

    .line 169
    const-string v5, ""

    .line 172
    :cond_3
    move-object v2, v6

    .line 174
    .local v2, "colonSplitter":Landroid/text/TextUtils$SimpleStringSplitter;
    invoke-virtual {v2, v5}, Landroid/text/TextUtils$SimpleStringSplitter;->setString(Ljava/lang/String;)V

    .line 176
    :cond_4
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 177
    invoke-virtual {v2}, Landroid/text/TextUtils$SimpleStringSplitter;->next()Ljava/lang/String;

    move-result-object v3

    .line 178
    .local v3, "componentNameString":Ljava/lang/String;
    invoke-static {v3}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v4

    .line 180
    .local v4, "enabledService":Landroid/content/ComponentName;
    if-eqz v4, :cond_4

    .line 181
    const-string v8, "com.google.android.marvin.talkback"

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const-string v8, "com.google.android.marvin.talkback/com.google.android.marvin.talkback.TalkBackService"

    invoke-virtual {v5, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 183
    const/4 v7, 0x1

    goto :goto_0
.end method


# virtual methods
.method public init()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/speech/tts/TextToSpeech;-><init>(Landroid/content/Context;Landroid/speech/tts/TextToSpeech$OnInitListener;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    .line 80
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mContext:Landroid/content/Context;

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 83
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    iget-object v1, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mProgressListener:Landroid/speech/tts/UtteranceProgressListener;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 85
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 143
    const-string v0, "TalkBackUtils"

    const-string v1, "TTS Destroy."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mAudioManager:Landroid/media/AudioManager;

    const/4 v1, 0x3

    iget v2, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->oldVolumeLevel:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 146
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0, v4}, Landroid/speech/tts/TextToSpeech;->setOnUtteranceProgressListener(Landroid/speech/tts/UtteranceProgressListener;)I

    .line 148
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->stop()I

    .line 149
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->shutdown()V

    .line 150
    iput-object v4, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    .line 152
    :cond_0
    sput-object v4, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mInstance:Lcom/sec/android/emergencymode/util/TalkBackUtil;

    .line 153
    iput-object v4, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 154
    return-void
.end method

.method public onInit(I)V
    .locals 2
    .param p1, "initTTS"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    if-eqz v0, :cond_4

    if-nez p1, :cond_4

    .line 127
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v0

    if-nez v0, :cond_3

    .line 130
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 133
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v0}, Landroid/speech/tts/TextToSpeech;->getLanguage()Ljava/util/Locale;

    move-result-object v0

    if-nez v0, :cond_1

    .line 134
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    .line 135
    :cond_1
    const-string v0, "TalkBackUtils"

    const-string v1, "init Success!!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->text:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->speak(Ljava/lang/String;)V

    .line 140
    :cond_2
    :goto_1
    return-void

    .line 131
    :cond_3
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->isLanguageAvailable(Ljava/util/Locale;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Landroid/speech/tts/TextToSpeech;->setLanguage(Ljava/util/Locale;)I

    goto :goto_0

    .line 137
    :cond_4
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    .line 138
    const-string v0, "TalkBackUtils"

    const-string v1, "Do not init TTS!!"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public requestSpeak(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 71
    const-string v0, "TalkBackUtils"

    const-string v1, "requestSpeak."

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/util/TalkBackUtil;->init()V

    .line 74
    iput-object p1, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->text:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public speak(Ljava/lang/String;)V
    .locals 7
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 91
    iget-object v2, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    if-nez v2, :cond_1

    .line 93
    const-string v2, "TalkBackUtils"

    const-string v3, "mTTS is null, do not speak!!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v2, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2}, Landroid/speech/tts/TextToSpeech;->isSpeaking()Z

    move-result v2

    if-nez v2, :cond_0

    .line 98
    iget-object v2, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->oldVolumeLevel:I

    .line 99
    iget-object v2, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v5}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 100
    .local v0, "maxVolume":I
    iget-object v2, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v2, v5, v0, v6}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 102
    const-string v2, "TalkBackUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "d_engine "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v4}, Landroid/speech/tts/TextToSpeech;->getDefaultEngine()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 105
    .local v1, "ttsHash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "utteranceId"

    const-string v3, "TTS_END"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v2, "streamType"

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 110
    iget-object v2, p0, Lcom/sec/android/emergencymode/util/TalkBackUtil;->mTTS:Landroid/speech/tts/TextToSpeech;

    invoke-virtual {v2, p1, v6, v1}, Landroid/speech/tts/TextToSpeech;->speak(Ljava/lang/String;ILjava/util/HashMap;)I

    goto :goto_0
.end method
