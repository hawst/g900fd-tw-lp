.class Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$1;
.super Landroid/content/BroadcastReceiver;
.source "EmergencyDialogHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;-><init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/EmergencyActionListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x2

    const/4 v7, -0x1

    .line 74
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$000(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Landroid/content/Context;

    move-result-object v4

    const-string v5, "keyguard"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    .line 75
    .local v2, "keyguardManager":Landroid/app/KeyguardManager;
    invoke-virtual {v2}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    .line 76
    .local v1, "isKeyguardLocked":Z
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "action":Ljava/lang/String;
    const-string v4, "EmergencyDialogHandler"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onReceive action ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "] isKeyguardLocked ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v4, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "com.android.systemui.statusbar.EXPANDED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-nez v1, :cond_2

    .line 79
    :cond_0
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->dismissAllDialog(II)V
    invoke-static {v4, v7, v8}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$100(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;II)V

    .line 86
    :cond_1
    :goto_0
    return-void

    .line 80
    :cond_2
    const-string v4, "android.intent.action.PHONE_STATE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 81
    const-string v4, "state"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 82
    .local v3, "phoneStateExtra":Ljava/lang/String;
    sget-object v4, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 83
    :cond_3
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$1;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # invokes: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->dismissAllDialog(II)V
    invoke-static {v4, v7, v8}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$100(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;II)V

    goto :goto_0
.end method
