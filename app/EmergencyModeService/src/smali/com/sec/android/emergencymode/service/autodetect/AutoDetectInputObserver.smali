.class public Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;
.super Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;
.source "AutoDetectInputObserver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$1;,
        Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;,
        Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;,
        Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$InputObserverHandler;
    }
.end annotation


# static fields
.field private static final HARDKEY_INTENT:Ljava/lang/String; = "android.intent.action.EMERGENCY_HARDKEY_DETECTED"

.field private static final REASON:Ljava/lang/String; = "INPUT"

.field private static final TAG:Ljava/lang/String; = "AutoDetectInputObserver"


# instance fields
.field private mHardKeyReceiver:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;

.field private mInputChannel:Landroid/view/InputChannel;

.field private mInputEventReceiver:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;

.field private mInputManager:Landroid/hardware/input/InputManager;

.field private mObserverHandler:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$InputObserverHandler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;-><init>(Landroid/content/Context;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;)V

    .line 50
    new-instance v0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$InputObserverHandler;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$InputObserverHandler;-><init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mObserverHandler:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$InputObserverHandler;

    .line 51
    new-instance v0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;-><init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$1;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mHardKeyReceiver:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;

    .line 52
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mContext:Landroid/content/Context;

    const-string v1, "input"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mInputManager:Landroid/hardware/input/InputManager;

    .line 54
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mObserverHandler:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$InputObserverHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$InputObserverHandler;->sendEmptyMessage(I)Z

    .line 55
    return-void
.end method

.method static synthetic access$102(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;)Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;
    .param p1, "x1"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mInputEventReceiver:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;)Landroid/view/InputChannel;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mInputChannel:Landroid/view/InputChannel;

    return-object v0
.end method


# virtual methods
.method protected startObserver()V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->startObserver()V

    .line 60
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mHardKeyReceiver:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;->register()V

    .line 61
    return-void
.end method

.method protected stopObserver(Ljava/lang/String;)V
    .locals 3
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectObserver;->stopObserver(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mInputEventReceiver:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mInputEventReceiver:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;->dispose()V

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mInputEventReceiver:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$TouchEventReceiver;

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mHardKeyReceiver:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;->unregister()V

    .line 71
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "INPUT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;->onComplete(Ljava/lang/String;)V

    .line 72
    return-void
.end method
