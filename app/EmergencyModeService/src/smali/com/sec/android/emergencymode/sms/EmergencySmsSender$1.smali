.class Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;
.super Ljava/lang/Object;
.source "EmergencySmsSender.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/sms/EmergencySmsSender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 101
    const-string v0, "EmergencySmsSender"

    const-string v1, "MSG service onServiceConnected Called !!!! ==="

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1$1;-><init>(Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;Landroid/os/IBinder;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 123
    const-string v0, "EmergencySmsSender"

    const-string v1, "MSG service onServiceConnected Called END ==="

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;

    .prologue
    .line 128
    const-string v1, "EmergencySmsSender"

    const-string v2, "MSG service onServiceDisconnected Called !!!!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    # getter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$000(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/mms/transaction/IMessageBackgroundSender;->stopListening()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :goto_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mServiceBinder:Lcom/android/mms/transaction/IMessageBackgroundSender;
    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$002(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Lcom/android/mms/transaction/IMessageBackgroundSender;)Lcom/android/mms/transaction/IMessageBackgroundSender;

    .line 137
    iget-object v1, p0, Lcom/sec/android/emergencymode/sms/EmergencySmsSender$1;->this$0:Lcom/sec/android/emergencymode/sms/EmergencySmsSender;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->mSvcConnected:Z
    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/sms/EmergencySmsSender;->access$102(Lcom/sec/android/emergencymode/sms/EmergencySmsSender;Z)Z

    .line 138
    return-void

    .line 132
    :catch_0
    move-exception v0

    .line 133
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
