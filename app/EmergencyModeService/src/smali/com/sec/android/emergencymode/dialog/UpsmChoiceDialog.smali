.class public Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;
.super Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;
.source "UpsmChoiceDialog.java"


# static fields
.field public static DISMISS_CAUSE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "UpsmChoiceDialog"

.field private static cfmsService:Landroid/os/ICustomFrequencyManager;

.field private static mBatteryLevel:I

.field private static mUltraPowerSavingDayPointTime:I

.field private static mUltraPowerSavingDayTime:I

.field private static mUltraPowerSavingHourTime:I

.field private static mUltraPowerSavingMinTime:I

.field private static mUltraPowerSavingTime:I


# instance fields
.field protected colorTextMain:I

.field protected colorTextSub:I

.field protected colorVerticalBar:I

.field private dialog:Landroid/app/AlertDialog;

.field protected isDarkTheme:Z

.field protected isTablet:Z

.field mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mUltraPowerSavingBattery:Landroid/widget/TextView;

.field private mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;

.field private mUltraPowerSavingUsageTime:Landroid/widget/TextView;

.field private reqState:Z

.field protected theme:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    sput v1, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I

    .line 72
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->cfmsService:Landroid/os/ICustomFrequencyManager;

    .line 81
    sput v1, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->DISMISS_CAUSE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZIZ)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "reqState"    # Z
    .param p3, "theme"    # I
    .param p4, "isTablet"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 84
    invoke-direct {p0, p1, p3}, Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;-><init>(Landroid/content/Context;I)V

    .line 75
    iput-boolean v4, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    .line 282
    new-instance v2, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;

    invoke-direct {v2, p0}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$3;-><init>(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)V

    iput-object v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 85
    iput-object p1, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    .line 86
    iput p3, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->theme:I

    .line 87
    const/4 v2, 0x4

    if-eq p3, v2, :cond_0

    .line 88
    iput-boolean v3, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    .line 90
    :cond_0
    iput-boolean p4, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isTablet:Z

    .line 91
    iput-boolean p2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->reqState:Z

    .line 92
    sput v3, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->DISMISS_CAUSE:I

    .line 95
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->reqState:Z

    if-ne v2, v4, :cond_1

    .line 96
    const v0, 0x7f090037

    .line 101
    .local v0, "ok_str":I
    :goto_0
    new-instance v2, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$1;-><init>(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)V

    invoke-virtual {p0, v0, v2}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 108
    const v2, 0x1040009

    new-instance v3, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$2;

    invoke-direct {v3, p0}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog$2;-><init>(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)V

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 116
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 117
    .local v1, "res":Landroid/content/res/Resources;
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isTablet:Z

    if-eqz v2, :cond_3

    .line 119
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v2, :cond_2

    .line 121
    const v2, 0x7f06000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextMain:I

    .line 122
    const v2, 0x7f060010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextSub:I

    .line 123
    const v2, 0x7f060011

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorVerticalBar:I

    .line 145
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->updateView()V

    .line 146
    invoke-direct {p0, v4}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->registerReceiver(Z)V

    .line 147
    return-void

    .line 98
    .end local v0    # "ok_str":I
    .end local v1    # "res":Landroid/content/res/Resources;
    :cond_1
    const v0, 0x7f090038

    .restart local v0    # "ok_str":I
    goto :goto_0

    .line 126
    .restart local v1    # "res":Landroid/content/res/Resources;
    :cond_2
    const v2, 0x7f060012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextMain:I

    .line 127
    const v2, 0x7f060013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextSub:I

    .line 128
    const v2, 0x7f060014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorVerticalBar:I

    goto :goto_1

    .line 132
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v2, :cond_4

    .line 134
    const v2, 0x7f060004

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextMain:I

    .line 135
    const v2, 0x7f060005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextSub:I

    .line 136
    const v2, 0x7f060006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorVerticalBar:I

    goto :goto_1

    .line 139
    :cond_4
    const v2, 0x7f060009

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextMain:I

    .line 140
    const v2, 0x7f06000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextSub:I

    .line 141
    const v2, 0x7f06000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorVerticalBar:I

    goto :goto_1
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I

    return v0
.end method

.method static synthetic access$002(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 54
    sput p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I

    return p0
.end method

.method static synthetic access$100()Landroid/os/ICustomFrequencyManager;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->cfmsService:Landroid/os/ICustomFrequencyManager;

    return-object v0
.end method

.method static synthetic access$102(Landroid/os/ICustomFrequencyManager;)Landroid/os/ICustomFrequencyManager;
    .locals 0
    .param p0, "x0"    # Landroid/os/ICustomFrequencyManager;

    .prologue
    .line 54
    sput-object p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->cfmsService:Landroid/os/ICustomFrequencyManager;

    return-object p0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I

    return v0
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 54
    sput p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingTime:I

    return p0
.end method

.method static synthetic access$300(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingBattery:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayTime:I

    return v0
.end method

.method static synthetic access$402(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 54
    sput p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayTime:I

    return p0
.end method

.method static synthetic access$500()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayPointTime:I

    return v0
.end method

.method static synthetic access$502(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 54
    sput p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingDayPointTime:I

    return p0
.end method

.method static synthetic access$600(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingHourTime:I

    return v0
.end method

.method static synthetic access$702(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 54
    sput p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingHourTime:I

    return p0
.end method

.method static synthetic access$800()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingMinTime:I

    return v0
.end method

.method static synthetic access$802(I)I
    .locals 0
    .param p0, "x0"    # I

    .prologue
    .line 54
    sput p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingMinTime:I

    return p0
.end method

.method static synthetic access$900(Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method private getDisableDialogView()Landroid/view/View;
    .locals 6

    .prologue
    .line 202
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 203
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030003

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 205
    .local v0, "content":Landroid/view/View;
    const v4, 0x7f0b000b

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 206
    .local v3, "messageText":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090014

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 207
    .local v2, "message":Ljava/lang/String;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 208
    iget v4, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextMain:I

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 209
    return-object v0
.end method

.method private registerReceiver(Z)V
    .locals 5
    .param p1, "reg"    # Z

    .prologue
    .line 170
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->reqState:Z

    if-eqz v1, :cond_0

    const-string v1, "JPN"

    const-string v2, "OPEN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    if-eqz p1, :cond_1

    .line 173
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 180
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 181
    :catch_1
    move-exception v0

    .line 182
    .restart local v0    # "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public create()Landroid/app/AlertDialog;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "UpsmChoiceDialog.create(): use show() instead.."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public dismiss(I)V
    .locals 1
    .param p1, "reason"    # I

    .prologue
    .line 161
    sput p1, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->DISMISS_CAUSE:I

    .line 162
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->dialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    invoke-virtual {p0, p1}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->sendLoggingBatteryLevel(I)V

    .line 164
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->dialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 166
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->registerReceiver(Z)V

    .line 167
    return-void
.end method

.method public getChoiceDialogView()Landroid/view/View;
    .locals 17

    .prologue
    .line 213
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    const-string v16, "layout_inflater"

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 214
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v15, 0x7f030008

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 216
    .local v1, "content":Landroid/view/View;
    const-string v15, "JPN"

    const-string v16, "OPEN"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 217
    const v15, 0x7f0b001d

    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 218
    .local v7, "timeView":Landroid/widget/LinearLayout;
    const/16 v15, 0x8

    invoke-virtual {v7, v15}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 221
    .end local v7    # "timeView":Landroid/widget/LinearLayout;
    :cond_0
    const v15, 0x7f0b001f

    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingBattery:Landroid/widget/TextView;

    .line 222
    const v15, 0x7f0b0023

    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;

    .line 223
    const v15, 0x7f0b0022

    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageIcon:Landroid/widget/ImageView;

    .line 226
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingBattery:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextMain:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setTextColor(I)V

    .line 227
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mUltraPowerSavingUsageTime:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextMain:I

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/TextView;->setTextColor(I)V

    .line 228
    const v15, 0x7f0b001e

    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 229
    .local v8, "ultraPowerSavingBatterySub":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextSub:I

    invoke-virtual {v8, v15}, Landroid/widget/TextView;->setTextColor(I)V

    .line 230
    const v15, 0x7f0b0021

    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 231
    .local v9, "ultraPowerSavingUsageTimeSub":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextSub:I

    invoke-virtual {v9, v15}, Landroid/widget/TextView;->setTextColor(I)V

    .line 232
    const v15, 0x7f0b0005

    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 233
    .local v10, "ultraPowerSavingUsageWarningEnd":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextMain:I

    invoke-virtual {v10, v15}, Landroid/widget/TextView;->setTextColor(I)V

    .line 234
    const v15, 0x7f0b0020

    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    .line 235
    .local v11, "verticalBar":Landroid/view/View;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorVerticalBar:I

    invoke-virtual {v11, v15}, Landroid/view/View;->setBackgroundColor(I)V

    .line 239
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 242
    .local v12, "warningString":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    const v16, 0x7f090016

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 244
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    const v16, 0x7f090017

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 246
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    const v16, 0x7f090018

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x1120044

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v15

    if-nez v15, :cond_1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x1120045

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 250
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    const v16, 0x7f090019

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDocomoFeature:Z

    if-eqz v15, :cond_2

    .line 256
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    const v16, 0x7f090009

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    :cond_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    const v16, 0x7f09001a

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    const v15, 0x7f0b0004

    invoke-virtual {v1, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/LinearLayout;

    .line 263
    .local v14, "warningView":Landroid/widget/LinearLayout;
    const-string v15, "JPN"

    const-string v16, "OPEN"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 264
    const/16 v15, 0x8

    invoke-virtual {v10, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 267
    :cond_3
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 268
    .local v5, "str":Ljava/lang/String;
    if-eqz v5, :cond_4

    .line 269
    const v15, 0x7f030002

    const/16 v16, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v4, v15, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 270
    .local v6, "summaryView":Landroid/view/View;
    const v15, 0x7f0b0006

    invoke-virtual {v6, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 271
    .local v3, "imgView":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    move-object/from16 v0, p0

    iget-boolean v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->isDarkTheme:Z

    if-eqz v15, :cond_6

    const v15, 0x7f02004f

    :goto_2
    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v15

    invoke-virtual {v3, v15}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 272
    const v15, 0x7f0b0007

    invoke-virtual {v6, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 273
    .local v13, "warningText":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->colorTextMain:I

    invoke-virtual {v13, v15}, Landroid/widget/TextView;->setTextColor(I)V

    .line 274
    invoke-virtual {v13, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    invoke-virtual {v14, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 252
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "imgView":Landroid/widget/ImageView;
    .end local v5    # "str":Ljava/lang/String;
    .end local v6    # "summaryView":Landroid/view/View;
    .end local v13    # "warningText":Landroid/widget/TextView;
    .end local v14    # "warningView":Landroid/widget/LinearLayout;
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    const v16, 0x7f090029

    invoke-virtual/range {v15 .. v16}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 271
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "imgView":Landroid/widget/ImageView;
    .restart local v5    # "str":Ljava/lang/String;
    .restart local v6    # "summaryView":Landroid/view/View;
    .restart local v14    # "warningView":Landroid/widget/LinearLayout;
    :cond_6
    const v15, 0x7f020050

    goto :goto_2

    .line 279
    .end local v3    # "imgView":Landroid/widget/ImageView;
    .end local v5    # "str":Ljava/lang/String;
    .end local v6    # "summaryView":Landroid/view/View;
    :cond_7
    return-object v1
.end method

.method public isSpaceRequired()Z
    .locals 2

    .prologue
    .line 432
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fi_FI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "iw_IL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_AU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_GB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_CA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_NZ"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_SG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_IE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "en_ZA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "nb_NO"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "it_IT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public sendLoggingBatteryLevel(I)V
    .locals 5
    .param p1, "reason"    # I

    .prologue
    .line 447
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->reqState:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v2, 0x3

    if-ne p1, v2, :cond_0

    .line 448
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v2

    const-string v3, "SEC_FLOATING_FEATURE_CONTEXTSERVICE_ENABLE_SURVEY_MODE"

    invoke-virtual {v2, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 449
    const-string v2, "UpsmChoiceDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendLoggingBatteryLevel = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 452
    .local v1, "cv":Landroid/content/ContentValues;
    const-string v2, "app_id"

    const-string v3, "com.sec.android.emergencymode.service"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const-string v2, "feature"

    const-string v3, "UPSM"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    const-string v2, "value"

    sget v3, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mBatteryLevel:I

    mul-int/lit16 v3, v3, 0x3e8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 456
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 458
    .local v0, "broadcastIntent":Landroid/content/Intent;
    const-string v2, "com.samsung.android.providers.context.log.action.USE_APP_FEATURE_SURVEY"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 459
    const-string v2, "data"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 460
    const-string v2, "com.samsung.android.providers.context"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 462
    iget-object v2, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 465
    .end local v0    # "broadcastIntent":Landroid/content/Intent;
    .end local v1    # "cv":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method

.method public show()Landroid/app/AlertDialog;
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Lcom/sec/android/emergencymode/dialog/ChoiceDialogBase;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->dialog:Landroid/app/AlertDialog;

    .line 157
    iget-object v0, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->dialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public updateView()V
    .locals 3

    .prologue
    .line 191
    iget-object v1, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090013

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "title":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 194
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->reqState:Z

    if-eqz v1, :cond_0

    .line 195
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->getChoiceDialogView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 199
    :goto_0
    return-void

    .line 197
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->getDisableDialogView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/dialog/UpsmChoiceDialog;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method
