.class public Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;
.super Ljava/lang/Object;
.source "EmergencyDisclaimerCaller.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EmergencyDisclaimerCaller"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->mContext:Landroid/content/Context;

    .line 39
    return-void
.end method

.method protected static getDisclaimerUserAgreeState(Landroid/content/Context;Z)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "upsm"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 103
    if-eqz p1, :cond_0

    .line 104
    const-string v3, "em_preference"

    invoke-virtual {p0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 105
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v3, "disclaimer_upsm"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 109
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    .local v0, "agree":Z
    :goto_0
    return v0

    .line 107
    .end local v0    # "agree":Z
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "safety_care_user_agree"

    const/4 v5, -0x1

    invoke-static {v3, v4, v5, v2}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-ne v3, v0, :cond_1

    .restart local v0    # "agree":Z
    :goto_1
    goto :goto_0

    .end local v0    # "agree":Z
    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method protected static setDisclaimerUserAgreeState(Landroid/content/Context;ZZ)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enabled"    # Z
    .param p2, "upsm"    # Z

    .prologue
    const/4 v3, 0x0

    .line 91
    if-eqz p2, :cond_0

    .line 92
    const-string v2, "em_preference"

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 93
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 94
    .local v0, "ed":Landroid/content/SharedPreferences$Editor;
    const-string v2, "disclaimer_upsm"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 95
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 99
    .end local v0    # "ed":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "safety_care_user_agree"

    if-eqz p1, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v4, v5, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method private startDisclaimerActivity(IZ)V
    .locals 5
    .param p1, "flag"    # I
    .param p2, "skipdialog"    # Z

    .prologue
    .line 77
    and-int/lit16 v2, p1, 0x600

    if-eqz v2, :cond_0

    .line 78
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    .local v0, "disclaimerIntent":Landroid/content/Intent;
    :goto_0
    const-string v2, "skipdialog"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 83
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 84
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->mContext:Landroid/content/Context;

    sget-object v3, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 88
    .end local v0    # "disclaimerIntent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 80
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v0    # "disclaimerIntent":Landroid/content/Intent;
    goto :goto_0

    .line 85
    .end local v0    # "disclaimerIntent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 86
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "EmergencyDisclaimerCaller"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startDisclaimerActivity e : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private switchUser()V
    .locals 3

    .prologue
    .line 67
    const-string v0, "EmergencyDisclaimerCaller"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "switchUser : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->mContext:Landroid/content/Context;

    const-string v1, "persona"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 72
    :cond_0
    return-void
.end method


# virtual methods
.method public callDisclaimer(ZIZ)Z
    .locals 6
    .param p1, "enabled"    # Z
    .param p2, "flag"    # I
    .param p3, "skipdialog"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 44
    if-nez p1, :cond_1

    .line 46
    const-string v2, "EmergencyDisclaimerCaller"

    const-string v3, "Skip checking disclaimer"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    :goto_0
    return v1

    .line 50
    :cond_1
    and-int/lit16 v3, p2, 0x600

    if-eqz v3, :cond_2

    .line 51
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->mContext:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->getDisclaimerUserAgreeState(Landroid/content/Context;Z)Z

    move-result v0

    .line 55
    .local v0, "disclaimerState":Z
    :goto_1
    const-string v3, "EmergencyDisclaimerCaller"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "disclaimer : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    if-nez v0, :cond_0

    and-int/lit16 v3, p2, 0x440

    if-nez v3, :cond_0

    .line 57
    const-string v1, "EmergencyDisclaimerCaller"

    const-string v3, "Need user agree. call disclaimer."

    invoke-static {v1, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->switchUser()V

    .line 59
    invoke-direct {p0, p2, p3}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->startDisclaimerActivity(IZ)V

    move v1, v2

    .line 60
    goto :goto_0

    .line 53
    .end local v0    # "disclaimerState":Z
    :cond_2
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/sec/android/emergencymode/service/disclaimer/EmergencyDisclaimerCaller;->getDisclaimerUserAgreeState(Landroid/content/Context;Z)Z

    move-result v0

    .restart local v0    # "disclaimerState":Z
    goto :goto_1
.end method
