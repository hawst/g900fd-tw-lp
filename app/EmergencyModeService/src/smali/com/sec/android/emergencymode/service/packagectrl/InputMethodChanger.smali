.class public Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;
.super Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;
.source "InputMethodChanger.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "InputMethodChanger"


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mImm:Landroid/view/inputmethod/InputMethodManager;

.field private final mMethodList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mMethodMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/view/inputmethod/InputMethodInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final mSettings:Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;-><init>(Landroid/content/Context;)V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodList:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodMap:Ljava/util/HashMap;

    .line 51
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mContext:Landroid/content/Context;

    .line 52
    new-instance v0, Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodMap:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodList:Ljava/util/ArrayList;

    invoke-static {}, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->getDefaultCurrentUserId()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;-><init>(Landroid/content/res/Resources;Landroid/content/ContentResolver;Ljava/util/HashMap;Ljava/util/ArrayList;I)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mSettings:Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;

    .line 54
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mContext:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 55
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->refreshAllInputMethodAndSubtypes()V

    .line 56
    return-void
.end method

.method private getCurrentInputMethod()Ljava/lang/String;
    .locals 6

    .prologue
    .line 97
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodMap:Ljava/util/HashMap;

    monitor-enter v3

    .line 98
    const/4 v1, 0x0

    .line 99
    .local v1, "methodName":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodMap:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mSettings:Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v4}, Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodInfo;

    .line 100
    .local v0, "imi":Landroid/view/inputmethod/InputMethodInfo;
    if-nez v0, :cond_0

    .line 101
    const-string v2, "InputMethodChanger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid selected imi: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mSettings:Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;

    invoke-virtual {v5}, Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;->getSelectedInputMethod()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const/4 v2, 0x0

    monitor-exit v3

    .line 106
    :goto_0
    return-object v2

    .line 104
    :cond_0
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 105
    const-string v2, "InputMethodChanger"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentInputMethod : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    monitor-exit v3

    move-object v2, v1

    goto :goto_0

    .line 107
    .end local v0    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static getDefaultCurrentUserId()I
    .locals 4

    .prologue
    .line 76
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/app/IActivityManager;->getCurrentUser()Landroid/content/pm/UserInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/UserInfo;->id:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    .local v0, "e":Landroid/os/RemoteException;
    :goto_0
    return v1

    .line 77
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_0
    move-exception v0

    .line 78
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "InputMethodChanger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Couldn\'t get current user ID; guessing it\'s 0 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private refreshAllInputMethodAndSubtypes()V
    .locals 6

    .prologue
    .line 84
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodMap:Ljava/util/HashMap;

    monitor-enter v4

    .line 85
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 86
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 87
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodManager;->getInputMethodList()Ljava/util/List;

    move-result-object v2

    .line 89
    .local v2, "imms":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 90
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodInfo;

    .line 91
    .local v1, "imi":Landroid/view/inputmethod/InputMethodInfo;
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodMap:Ljava/util/HashMap;

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 93
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    .end local v2    # "imms":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "imms":Ljava/util/List;, "Ljava/util/List<Landroid/view/inputmethod/InputMethodInfo;>;"
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    return-void
.end method

.method private setDefaultInputMethod(Ljava/lang/String;)V
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 111
    iget-object v5, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodMap:Ljava/util/HashMap;

    monitor-enter v5

    .line 112
    :try_start_0
    const-string v4, "InputMethodChanger"

    const-string v6, "setDefaultInputMethod."

    invoke-static {v4, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    const/4 v2, 0x0

    .line 114
    .local v2, "imi":Landroid/view/inputmethod/InputMethodInfo;
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mMethodList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodInfo;

    .line 115
    .local v3, "mInfo":Landroid/view/inputmethod/InputMethodInfo;
    invoke-virtual {v3}, Landroid/view/inputmethod/InputMethodInfo;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, "imPkg":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 117
    const-string v4, "InputMethodChanger"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setDefaultInputMethod : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    move-object v2, v3

    .line 122
    .end local v1    # "imPkg":Ljava/lang/String;
    .end local v3    # "mInfo":Landroid/view/inputmethod/InputMethodInfo;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->mSettings:Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Landroid/view/inputmethod/InputMethodInfo;->getId()Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-virtual {v6, v4}, Lcom/android/internal/inputmethod/InputMethodUtils$InputMethodSettings;->putSelectedInputMethod(Ljava/lang/String;)V

    .line 123
    monitor-exit v5

    .line 124
    return-void

    .line 122
    :cond_2
    const-string v4, ""

    goto :goto_0

    .line 123
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "imi":Landroid/view/inputmethod/InputMethodInfo;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method


# virtual methods
.method public changeDefaultApp(Z)V
    .locals 4
    .param p1, "req"    # Z

    .prologue
    .line 60
    if-eqz p1, :cond_1

    .line 61
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->getCurrentInputMethod()Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "oldInputMethod":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 63
    const-string v1, "DEFAULT_INPUTMETHOD"

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 66
    .end local v0    # "oldInputMethod":Ljava/lang/String;
    :cond_1
    const-string v1, "DEFAULT_INPUTMETHOD"

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    .restart local v0    # "oldInputMethod":Ljava/lang/String;
    const-string v1, "InputMethodChanger"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "changeDefaultApp : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    if-eqz v0, :cond_0

    .line 69
    invoke-direct {p0, v0}, Lcom/sec/android/emergencymode/service/packagectrl/InputMethodChanger;->setDefaultInputMethod(Ljava/lang/String;)V

    goto :goto_0
.end method
