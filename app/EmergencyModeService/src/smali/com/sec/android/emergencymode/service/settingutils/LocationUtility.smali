.class public Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;
.super Ljava/lang/Object;
.source "LocationUtility.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;,
        Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;,
        Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;,
        Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$OneTouchReportLocationSettingsUtility;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LocationUtility"


# instance fields
.field private final PACKAGE_NAME_GMS:Ljava/lang/String;

.field private final PACKAGE_NAME_GSF:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mGpsProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;

.field private mNetworkProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;

.field private mPassiveProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const-string v0, "com.google.android.gsf"

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->PACKAGE_NAME_GSF:Ljava/lang/String;

    .line 50
    const-string v0, "com.google.android.gms"

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->PACKAGE_NAME_GMS:Ljava/lang/String;

    .line 59
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private setGoogleNetworkProviderStatus(Z)Z
    .locals 14
    .param p1, "value"    # Z

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x1

    .line 239
    const/4 v5, 0x0

    .line 240
    .local v5, "gsfEnabledState":I
    const/4 v3, 0x0

    .line 241
    .local v3, "gmsEnabledState":I
    const/4 v1, 0x0

    .line 242
    .local v1, "cansetnetwork_location_opt_in":Z
    const/4 v6, 0x0

    .line 243
    .local v6, "gsfIsInstalled":Z
    const/4 v4, 0x0

    .line 248
    .local v4, "gmsIsInstalled":Z
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 249
    .local v8, "pm":Landroid/content/pm/PackageManager;
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 251
    .local v0, "applicationList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/pm/ApplicationInfo;

    .line 252
    .local v9, "thisApp":Landroid/content/pm/ApplicationInfo;
    iget-object v10, v9, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    if-eqz v10, :cond_0

    .line 253
    if-nez v6, :cond_1

    iget-object v10, v9, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v11, "com.google.android.gsf"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 254
    const/4 v6, 0x1

    .line 256
    :cond_1
    if-nez v4, :cond_2

    iget-object v10, v9, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v11, "com.google.android.gms"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 257
    const/4 v4, 0x1

    .line 259
    :cond_2
    if-eqz v6, :cond_0

    if-eqz v4, :cond_0

    .line 265
    .end local v9    # "thisApp":Landroid/content/pm/ApplicationInfo;
    :cond_3
    if-eqz v6, :cond_8

    if-eqz v4, :cond_8

    .line 266
    const-string v10, "com.google.android.gsf"

    invoke-virtual {v8, v10}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v5

    .line 267
    const-string v10, "com.google.android.gms"

    invoke-virtual {v8, v10}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 269
    if-eqz v5, :cond_4

    if-ne v5, v12, :cond_6

    :cond_4
    if-eqz v3, :cond_5

    if-ne v3, v12, :cond_6

    .line 272
    :cond_5
    const/4 v1, 0x1

    .line 282
    .end local v0    # "applicationList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "pm":Landroid/content/pm/PackageManager;
    :cond_6
    :goto_0
    if-eqz v1, :cond_7

    .line 283
    if-eqz p1, :cond_9

    .line 284
    iget-object v10, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "network_location_opt_in"

    invoke-static {v10, v11, v12}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$OneTouchReportLocationSettingsUtility$Partner;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 291
    :cond_7
    :goto_1
    return v1

    .line 276
    .restart local v0    # "applicationList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .restart local v7    # "i$":Ljava/util/Iterator;
    .restart local v8    # "pm":Landroid/content/pm/PackageManager;
    :cond_8
    const/4 v1, 0x1

    goto :goto_0

    .line 278
    .end local v0    # "applicationList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ApplicationInfo;>;"
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v8    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v2

    .line 279
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 287
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_9
    iget-object v10, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v11, "network_location_opt_in"

    invoke-static {v10, v11, v13}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$OneTouchReportLocationSettingsUtility$Partner;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_1
.end method


# virtual methods
.method public getLastLocation()Landroid/location/Location;
    .locals 4

    .prologue
    .line 63
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    const-string v3, "location"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 64
    .local v0, "lm":Landroid/location/LocationManager;
    const-string v2, "network"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 65
    .local v1, "loc":Landroid/location/Location;
    if-nez v1, :cond_0

    .line 66
    const-string v2, "passive"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 67
    :cond_0
    if-nez v1, :cond_1

    .line 68
    const-string v2, "gps"

    invoke-virtual {v0, v2}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v1

    .line 70
    :cond_1
    return-object v1
.end method

.method public getLocationState()I
    .locals 5

    .prologue
    .line 74
    const/4 v1, 0x0

    .line 77
    .local v1, "result":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "location_mode"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 83
    :goto_0
    return v1

    .line 78
    :catch_0
    move-exception v0

    .line 79
    .local v0, "ex":Landroid/provider/Settings$SettingNotFoundException;
    const-string v2, "LocationUtility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getLocationState e : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerLocationListener()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 332
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 333
    .local v0, "mLocMgr":Landroid/location/LocationManager;
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mNetworkProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;

    if-nez v1, :cond_0

    new-instance v1, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;-><init>(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;)V

    iput-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mNetworkProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;

    .line 334
    :cond_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mPassiveProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;

    if-nez v1, :cond_1

    new-instance v1, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;-><init>(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;)V

    iput-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mPassiveProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;

    .line 335
    :cond_1
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mGpsProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;

    if-nez v1, :cond_2

    new-instance v1, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;

    invoke-direct {v1, p0, v3}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;-><init>(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;)V

    iput-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mGpsProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;

    .line 338
    :cond_2
    :try_start_0
    const-string v1, "LocationUtility"

    const-string v2, "requestLocationUpdates by locationProvider"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    const-string v1, "network"

    const-wide/16 v2, 0x3e8

    const/high16 v4, 0x3f000000    # 0.5f

    iget-object v5, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mNetworkProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 346
    :goto_0
    :try_start_1
    const-string v1, "LocationUtility"

    const-string v2, "requestLocationUpdates by passiveProvider"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    const-string v1, "passive"

    const-wide/16 v2, 0x3e8

    const/high16 v4, 0x3f000000    # 0.5f

    iget-object v5, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mPassiveProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 354
    :goto_1
    :try_start_2
    const-string v1, "LocationUtility"

    const-string v2, "requestLocationUpdates by gpsProvider"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v1, "gps"

    const-wide/16 v2, 0x3e8

    const/high16 v4, 0x3f000000    # 0.5f

    iget-object v5, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mGpsProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;

    invoke-virtual/range {v0 .. v5}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 360
    :goto_2
    return-void

    .line 341
    :catch_0
    move-exception v6

    .line 342
    .local v6, "e":Ljava/lang/Exception;
    const-string v1, "LocationUtility"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "locationProvider request e : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 349
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 350
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v1, "LocationUtility"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "passiveProvider request e : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 357
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v6

    .line 358
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v1, "LocationUtility"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "gpsProvider request e : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public setLocationState(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 91
    const-string v0, "LocationUtility"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLocationState ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "location_mode"

    invoke-static {v0, v1, p1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 93
    return-void
.end method

.method public setLocationState(ZZI)V
    .locals 6
    .param p1, "isEntering"    # Z
    .param p2, "turnOn"    # Z
    .param p3, "previousMode"    # I

    .prologue
    const/4 v5, 0x3

    .line 127
    const-string v2, "LocationUtility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLocationState isEntering["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] turnOn["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] previousMode["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const/4 v0, 0x0

    .local v0, "currentLocationMode":I
    const/4 v1, 0x0

    .line 129
    .local v1, "newLocationMode":I
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->getLocationState()I

    move-result v0

    .line 131
    if-ltz v0, :cond_0

    if-ge v5, v0, :cond_2

    .line 132
    :cond_0
    const-string v2, "LocationUtility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLocationState currentLocationMode is Invaid value!!! ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_1
    :goto_0
    return-void

    .line 136
    :cond_2
    if-eqz p1, :cond_4

    .line 139
    if-eqz p2, :cond_3

    .line 140
    packed-switch v0, :pswitch_data_0

    .line 200
    :goto_1
    if-eq v0, v1, :cond_1

    .line 201
    const-string v2, "LocationUtility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLocationState newLocationMode["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "location_mode"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 142
    :pswitch_0
    const/4 v1, 0x1

    .line 143
    goto :goto_1

    .line 145
    :pswitch_1
    const/4 v1, 0x1

    .line 146
    goto :goto_1

    .line 148
    :pswitch_2
    const/4 v1, 0x2

    .line 149
    goto :goto_1

    .line 151
    :pswitch_3
    const/4 v1, 0x2

    goto :goto_1

    .line 155
    :cond_3
    packed-switch v0, :pswitch_data_1

    goto :goto_1

    .line 157
    :pswitch_4
    const/4 v1, 0x0

    .line 158
    goto :goto_1

    .line 160
    :pswitch_5
    const/4 v1, 0x0

    .line 161
    goto :goto_1

    .line 163
    :pswitch_6
    const/4 v1, 0x2

    .line 164
    goto :goto_1

    .line 166
    :pswitch_7
    const/4 v1, 0x2

    goto :goto_1

    .line 174
    :cond_4
    if-eqz p2, :cond_5

    .line 175
    packed-switch v0, :pswitch_data_2

    goto :goto_1

    .line 177
    :pswitch_8
    const/4 v1, 0x1

    .line 178
    goto :goto_1

    .line 180
    :pswitch_9
    const/4 v1, 0x1

    .line 181
    goto :goto_1

    .line 183
    :pswitch_a
    const/4 v1, 0x3

    .line 184
    goto :goto_1

    .line 186
    :pswitch_b
    const/4 v1, 0x3

    goto :goto_1

    .line 190
    :cond_5
    if-ltz p3, :cond_6

    if-ge v5, p3, :cond_7

    .line 191
    :cond_6
    const-string v2, "LocationUtility"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLocationState previousMode is Invaid value!!! ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 194
    :cond_7
    move v1, p3

    goto :goto_1

    .line 140
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 155
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 175
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public startLocationInfo()V
    .locals 2

    .prologue
    .line 208
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;

    invoke-direct {v1, p0}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$1;-><init>(Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 226
    return-void
.end method

.method public stopLocationInfo()V
    .locals 4

    .prologue
    .line 229
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencyFactory;

    move-result-object v0

    .line 230
    .local v0, "emFac":Lcom/sec/android/emergencymode/service/EmergencyFactory;
    const/16 v1, 0x440

    invoke-virtual {v0, v1}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkModeType(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->unregisterLocationListener()V

    .line 232
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->setLocationState(ZZI)V

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_0
    const-string v1, "LocationUtility"

    const-string v2, "stopLocationInfo : mode type is FMM. keep gps state"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public unregisterLocationListener()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 363
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 364
    .local v0, "mLocMgr":Landroid/location/LocationManager;
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mNetworkProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;

    if-eqz v1, :cond_0

    .line 365
    const-string v1, "LocationUtility"

    const-string v2, "unregisterLocationListener networkProvider."

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mNetworkProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 367
    iput-object v3, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mNetworkProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$NetworkProviderListener;

    .line 369
    :cond_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mPassiveProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;

    if-eqz v1, :cond_1

    .line 370
    const-string v1, "LocationUtility"

    const-string v2, "unregisterLocationListener passiveProvider."

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mPassiveProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 372
    iput-object v3, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mPassiveProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$PassiveProviderListener;

    .line 374
    :cond_1
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mGpsProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;

    if-eqz v1, :cond_2

    .line 375
    const-string v1, "LocationUtility"

    const-string v2, "unregisterLocationListener gpsProvider."

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mGpsProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 377
    iput-object v3, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mGpsProviderListener:Lcom/sec/android/emergencymode/service/settingutils/LocationUtility$GpsProviderListener;

    .line 379
    :cond_2
    return-void
.end method

.method public updateLocationInfo(DDF)V
    .locals 3
    .param p1, "lastLat"    # D
    .param p3, "lastLong"    # D
    .param p5, "lastAcc"    # F

    .prologue
    .line 382
    const-string v1, "LocationUtility"

    const-string v2, "updateLocationInfo"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/settingutils/LocationUtility;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 384
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "GPS_LATITUDE"

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    .line 385
    const-string v1, "GPS_LONGITUDE"

    invoke-static {p3, p4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    .line 386
    const-string v1, "GPS_ACCURACY"

    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    .line 387
    return-void
.end method
