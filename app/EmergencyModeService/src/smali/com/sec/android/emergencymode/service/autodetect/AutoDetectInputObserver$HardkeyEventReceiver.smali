.class final Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AutoDetectInputObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HardkeyEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;


# direct methods
.method private constructor <init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;
    .param p2, "x1"    # Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$1;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;-><init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 121
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "action":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "android.intent.action.EMERGENCY_HARDKEY_DETECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 123
    const-string v1, "AutoDetectInputObserver"

    const-string v2, "HardKey Event!!"

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    iget-boolean v1, v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->detected:Z

    if-nez v1, :cond_0

    .line 125
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    const-string v2, "INPUT"

    invoke-interface {v1, v2}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;->onDetected(Ljava/lang/String;)V

    .line 128
    :cond_0
    return-void
.end method

.method public register()V
    .locals 2

    .prologue
    .line 106
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 107
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.EMERGENCY_HARDKEY_DETECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 108
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 109
    return-void
.end method

.method public unregister()V
    .locals 2

    .prologue
    .line 113
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver$HardkeyEventReceiver;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;

    iget-object v1, v1, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectInputObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
