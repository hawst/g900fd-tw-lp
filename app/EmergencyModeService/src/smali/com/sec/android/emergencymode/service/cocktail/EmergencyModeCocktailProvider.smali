.class public Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;
.super Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailProvider;
.source "EmergencyModeCocktailProvider.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EmergencyModeCocktailProvider"


# instance fields
.field mCocktailIds:[I

.field mCocktailManager:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;

.field private mContext:Landroid/content/Context;

.field private mCoverManager:Lcom/samsung/android/cover/CoverManager;

.field private mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

.field mFlipCoverClosed:Z

.field mPm:Landroid/os/PowerManager;

.field mRemoteViews:Landroid/widget/RemoteViews;

.field mStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailProvider;-><init>()V

    .line 35
    iput-object v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    .line 103
    new-instance v0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider$1;

    invoke-direct {v0, p0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider$1;-><init>(Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

    .line 127
    iput-object v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;Lcom/samsung/android/cover/CoverState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;
    .param p1, "x1"    # Lcom/samsung/android/cover/CoverState;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->setCoverState(Lcom/samsung/android/cover/CoverState;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->drawCocktail()V

    return-void
.end method

.method private drawCocktail()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    const v4, 0x7f0b000c

    .line 233
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mPm:Landroid/os/PowerManager;

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 235
    const-string v2, "EmergencyModeCocktailProvider"

    const-string v3, "drawCocktail : screen is ON"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    iget-boolean v2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mFlipCoverClosed:Z

    if-eqz v2, :cond_0

    .line 237
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->setClockGadgetView()V

    .line 238
    const-string v2, "EmergencyModeCocktailProvider"

    const-string v3, "drawCocktail : VISIBLE"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 249
    :goto_0
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCocktailIds:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCocktailIds:[I

    array-length v2, v2

    if-lez v2, :cond_2

    .line 250
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCocktailIds:[I

    aget v0, v2, v5

    .line 251
    .local v0, "cocktailId":I
    const/4 v1, 0x0

    .line 252
    .local v1, "info":Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo;
    new-instance v2, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo$Builder;

    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo$Builder;-><init>(Landroid/widget/RemoteViews;)V

    invoke-virtual {v2}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo$Builder;->build()Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo;

    move-result-object v1

    .line 253
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCocktailManager:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;->updateCocktail(ILcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo;)V

    .line 257
    .end local v0    # "cocktailId":I
    .end local v1    # "info":Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager$CocktailInfo;
    :goto_1
    return-void

    .line 241
    :cond_0
    const-string v2, "EmergencyModeCocktailProvider"

    const-string v3, "drawyCocktail : GONE"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v4, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 245
    :cond_1
    const-string v2, "EmergencyModeCocktailProvider"

    const-string v3, "drawCocktail : screen is OFF, displayCocktail : GONE"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget-object v2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v2, v4, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 255
    :cond_2
    const-string v2, "EmergencyModeCocktailProvider"

    const-string v3, "Incorrect cocktail IDs returned!"

    invoke-static {v2, v3}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private getDateFormatBoldDayOfMonth(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p1, "dateFormat"    # Ljava/lang/String;

    .prologue
    const/16 v9, 0x64

    const/4 v6, 0x0

    .line 206
    const-string v7, "EmergencyModeCocktailProvider"

    const-string v8, "getDateFormatBoldDayOfMonth"

    invoke-static {v7, v8}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    .line 208
    .local v0, "chars":[C
    const/4 v4, 0x0

    .line 209
    .local v4, "isEscaped":Z
    const/4 v2, -0x1

    .line 210
    .local v2, "dayOfMonthStartPosition":I
    const/4 v1, -0x1

    .line 211
    .local v1, "dayOfMonthEndPosition":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v7, v0

    if-ge v3, v7, :cond_4

    .line 212
    aget-char v7, v0, v3

    const/16 v8, 0x27

    if-ne v7, v8, :cond_2

    .line 213
    if-nez v4, :cond_1

    const/4 v4, 0x1

    .line 211
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v4, v6

    .line 213
    goto :goto_1

    .line 214
    :cond_2
    if-nez v4, :cond_0

    aget-char v7, v0, v3

    if-ne v7, v9, :cond_0

    .line 215
    move v2, v3

    .line 217
    :cond_3
    add-int/lit8 v3, v3, 0x1

    .line 218
    move v1, v3

    .line 219
    array-length v7, v0

    if-ge v3, v7, :cond_0

    aget-char v7, v0, v3

    if-eq v7, v9, :cond_3

    goto :goto_1

    .line 222
    :cond_4
    const/4 v7, -0x1

    if-le v2, v7, :cond_5

    .line 223
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 224
    .local v5, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'<b>\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'</b>\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 229
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .end local p1    # "dateFormat":Ljava/lang/String;
    :cond_5
    return-object p1
.end method

.method private registerCoverManagerListerner()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    if-nez v0, :cond_0

    .line 114
    new-instance v0, Lcom/samsung/android/cover/CoverManager;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/cover/CoverManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    .line 115
    const-string v0, "EmergencyModeCocktailProvider"

    const-string v1, "CoverManager create"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/cover/CoverManager;->registerListener(Lcom/samsung/android/cover/CoverManager$StateListener;)V

    .line 119
    const-string v0, "EmergencyModeCocktailProvider"

    const-string v1, "mCoverManager.registerListener"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    :cond_1
    return-void
.end method

.method private registerEmergencyCacktailReceiver()V
    .locals 2

    .prologue
    .line 131
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 132
    .local v0, "emFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 133
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 134
    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 139
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 140
    return-void
.end method

.method private setClockGadgetView()V
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    if-nez v0, :cond_0

    .line 144
    invoke-static {}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->getInstance()Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    .line 146
    :cond_0
    const-string v0, "EmergencyModeCocktailProvider"

    const-string v1, "setClockGadgetView"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->updateGadgetTime()V

    .line 149
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->updateGadgetDate()V

    .line 150
    return-void
.end method

.method private setCoverState(Lcom/samsung/android/cover/CoverState;)V
    .locals 4
    .param p1, "state"    # Lcom/samsung/android/cover/CoverState;

    .prologue
    const/4 v3, 0x1

    .line 90
    const-string v0, "EmergencyModeCocktailProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " setCoverState : state.getType() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/cover/CoverState;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    invoke-virtual {p1}, Lcom/samsung/android/cover/CoverState;->getType()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 92
    invoke-virtual {p1}, Lcom/samsung/android/cover/CoverState;->getSwitchState()Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 93
    const-string v0, "EmergencyModeCocktailProvider"

    const-string v1, " setCoverState : SWITCH_STATE_COVER_OPEN"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mFlipCoverClosed:Z

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    const-string v0, "EmergencyModeCocktailProvider"

    const-string v1, " setCoverState : SWITCH_STATE_COVER_CLOSE"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    iput-boolean v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mFlipCoverClosed:Z

    goto :goto_0
.end method

.method private updateGadgetDate()V
    .locals 7

    .prologue
    .line 191
    const-string v4, "EmergencyModeCocktailProvider"

    const-string v5, "updateGadgetDate"

    invoke-static {v4, v5}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f090000

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 196
    .local v3, "dateFormat":Ljava/lang/String;
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {p0, v3}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->getDateFormatBoldDayOfMonth(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    invoke-virtual {v5}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 199
    .local v0, "boldedDateFormat":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    .line 200
    .local v2, "date":Ljava/util/Date;
    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "calendarText":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    const v5, 0x7f0b0012

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 203
    return-void
.end method

.method private updateGadgetTime()V
    .locals 10

    .prologue
    const v9, 0x7f0b000e

    const/4 v8, 0x0

    const v7, 0x7f0b000d

    const/16 v5, 0x8

    const v6, 0x7f0b0010

    .line 153
    const-string v3, "EmergencyModeCocktailProvider"

    const-string v4, "updateGadgetTime"

    invoke-static {v3, v4}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    if-eqz v3, :cond_0

    .line 155
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 156
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "HH:"

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    invoke-virtual {v4}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 157
    .local v0, "hourFormat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "mm"

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    invoke-virtual {v4}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 159
    .local v1, "minuteFormat":Ljava/text/SimpleDateFormat;
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v6, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 161
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 163
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    const v4, 0x7f0b000f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 188
    .end local v0    # "hourFormat":Ljava/text/SimpleDateFormat;
    .end local v1    # "minuteFormat":Ljava/text/SimpleDateFormat;
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v3, "hh:"

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    invoke-virtual {v4}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 167
    .restart local v0    # "hourFormat":Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, "mm"

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    invoke-virtual {v4}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 168
    .restart local v1    # "minuteFormat":Ljava/text/SimpleDateFormat;
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "a"

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    invoke-virtual {v4}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 170
    .local v2, "unitFormat":Ljava/text/SimpleDateFormat;
    sget-object v3, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    iget-object v4, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    invoke-virtual {v4}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 171
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v6, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 172
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v7, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 174
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v7, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 182
    :goto_1
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 184
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    const v4, 0x7f0b000f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 177
    :cond_2
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v6, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 178
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v7, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 179
    iget-object v3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 62
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "action":Ljava/lang/String;
    const-string v1, "EmergencyModeCocktailProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " onReceive : intent.getAction() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    if-nez v1, :cond_0

    .line 68
    invoke-static {}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->getInstance()Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mData:Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Lcom/sec/android/emergencymode/service/cocktail/ClockGadgetDisplayData;->setLocale(Ljava/util/Locale;)V

    .line 71
    const-string v1, "EmergencyModeCocktailProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " onReceive : ACTION_LOCALE_CHANGED locale : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mFlipCoverClosed:Z

    if-eqz v1, :cond_3

    .line 76
    const-string v1, "android.intent.action.TIME_TICK"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 82
    :cond_2
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    if-eqz v1, :cond_3

    .line 83
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->drawCocktail()V

    .line 86
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 87
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;[I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cocktailManager"    # Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;
    .param p3, "cocktailIds"    # [I

    .prologue
    .line 42
    const-string v0, "EmergencyModeCocktailProvider"

    const-string v1, "onUpdate"

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mContext:Landroid/content/Context;

    .line 45
    iput-object p3, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCocktailIds:[I

    .line 46
    iput-object p2, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCocktailManager:Lcom/samsung/android/sdk/look/cocktailbar/SlookCocktailManager;

    .line 47
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mPm:Landroid/os/PowerManager;

    .line 50
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->registerCoverManagerListerner()V

    .line 51
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->registerEmergencyCacktailReceiver()V

    .line 52
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/cover/CoverManager;->getCoverState()Lcom/samsung/android/cover/CoverState;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->setCoverState(Lcom/samsung/android/cover/CoverState;)V

    .line 53
    new-instance v0, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030004

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    .line 54
    invoke-direct {p0}, Lcom/sec/android/emergencymode/service/cocktail/EmergencyModeCocktailProvider;->drawCocktail()V

    .line 56
    return-void
.end method
