.class public Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;
.super Landroid/app/ProgressDialog;
.source "EmergencyProgressDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog$1;,
        Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog$MyOnKeyListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "EmergencyProgressDialog"


# instance fields
.field mContext:Landroid/content/Context;

.field reqState:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I
    .param p3, "reqState"    # Z

    .prologue
    const/4 v3, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    .line 45
    iput-object p1, p0, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->mContext:Landroid/content/Context;

    .line 46
    iput-boolean p3, p0, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->reqState:Z

    .line 48
    const-string v1, "keyguard"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 49
    .local v0, "kgm":Landroid/app/KeyguardManager;
    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->setCancelable(Z)V

    .line 50
    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 51
    new-instance v1, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog$MyOnKeyListener;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog$MyOnKeyListener;-><init>(Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog$1;)V

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 52
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->setIndeterminate(Z)V

    .line 53
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d9

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    .line 58
    :goto_0
    invoke-virtual {p0, v3}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->setProgressStyle(I)V

    .line 59
    return-void

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d8

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/app/ProgressDialog;->onCreate(Landroid/os/Bundle;)V

    .line 64
    const v4, 0x102000d

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    .line 65
    .local v1, "mProgress":Landroid/widget/ProgressBar;
    const v4, 0x102000b

    invoke-virtual {p0, v4}, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 66
    .local v3, "view":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/service/EmergencyFactory;

    move-result-object v0

    .line 68
    .local v0, "factory":Lcom/sec/android/emergencymode/service/EmergencyFactory;
    const/16 v4, 0x600

    invoke-virtual {v0, v4}, Lcom/sec/android/emergencymode/service/EmergencyFactory;->checkModeType(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 69
    iget-boolean v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->reqState:Z

    if-eqz v4, :cond_0

    .line 70
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09001c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "title":Ljava/lang/String;
    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    const-string v4, "..."

    const-string v5, ""

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 83
    const-string v4, "\u00a0"

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 84
    return-void

    .line 72
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09001d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "title":Ljava/lang/String;
    goto :goto_0

    .line 75
    .end local v2    # "title":Ljava/lang/String;
    :cond_1
    iget-boolean v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->reqState:Z

    if-eqz v4, :cond_2

    .line 76
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090011

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "title":Ljava/lang/String;
    goto :goto_0

    .line 78
    .end local v2    # "title":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/sec/android/emergencymode/dialog/EmergencyProgressDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090012

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "title":Ljava/lang/String;
    goto :goto_0
.end method
