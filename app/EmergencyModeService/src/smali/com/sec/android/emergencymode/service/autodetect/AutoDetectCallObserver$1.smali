.class Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver$1;
.super Landroid/telephony/PhoneStateListener;
.source "AutoDetectCallObserver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver$1;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "incomingNumber"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver$1;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;

    iget-boolean v0, v0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;->detected:Z

    if-nez v0, :cond_0

    .line 54
    const-string v0, "AutoDetectCallObserver"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Call State Change : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver$1;->this$0:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;

    iget-object v0, v0, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectCallObserver;->detectListener:Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;

    const-string v1, "CALL_ACCEPT"

    invoke-interface {v0, v1}, Lcom/sec/android/emergencymode/service/autodetect/AutoDetectListener;->onDetected(Ljava/lang/String;)V

    .line 59
    :cond_0
    return-void
.end method
