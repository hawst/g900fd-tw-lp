.class public final Lcom/sec/android/emergencymode/service/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final actionbar_itemtext_disable_dark:I = 0x7f060008

.field public static final actionbar_itemtext_disable_light:I = 0x7f06000d

.field public static final actionbar_itemtext_normal_dark:I = 0x7f060007

.field public static final actionbar_itemtext_normal_light:I = 0x7f06000c

.field public static final actionbar_itemtext_shadow_light:I = 0x7f06000e

.field public static final batterymeter_bolt_color:I = 0x7f060002

.field public static final batterymeter_bolt_color_light:I = 0x7f060003

.field public static final batterymeter_frame_color:I = 0x7f060000

.field public static final batterymeter_frame_color_light:I = 0x7f060001

.field public static final choicedialog_maintext_dark:I = 0x7f060004

.field public static final choicedialog_maintext_dark_tablet:I = 0x7f06000f

.field public static final choicedialog_maintext_light:I = 0x7f060009

.field public static final choicedialog_maintext_light_tablet:I = 0x7f060012

.field public static final choicedialog_subtext_dark:I = 0x7f060005

.field public static final choicedialog_subtext_dark_tablet:I = 0x7f060010

.field public static final choicedialog_subtext_light:I = 0x7f06000a

.field public static final choicedialog_subtext_light_tablet:I = 0x7f060013

.field public static final choicedialog_verticalbar_dark:I = 0x7f060006

.field public static final choicedialog_verticalbar_dark_tablet:I = 0x7f060011

.field public static final choicedialog_verticalbar_light:I = 0x7f06000b

.field public static final choicedialog_verticalbar_light_tablet:I = 0x7f060014


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
