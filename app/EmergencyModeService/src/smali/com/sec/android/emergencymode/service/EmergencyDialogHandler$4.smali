.class Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$4;
.super Ljava/lang/Object;
.source "EmergencyDialogHandler.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$4;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 167
    const-string v1, "EmergencyDialogHandler"

    const-string v2, "onDismiss called."

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    sget v1, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->DISMISS_CAUSE:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 169
    const-string v1, "EmergencyDialogHandler"

    const-string v2, "Auto Timer Dialog is expired. Enter Emergency Mode."

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$4;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$200(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyActionListener;->onAccept(Z)V

    .line 177
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$4;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$000(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/emergencymode/util/WakeLockUtil;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/util/WakeLockUtil;

    move-result-object v0

    .line 178
    .local v0, "mWakelockUtil":Lcom/sec/android/emergencymode/util/WakeLockUtil;
    invoke-virtual {v0}, Lcom/sec/android/emergencymode/util/WakeLockUtil;->releaseWakeLock()V

    .line 179
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$4;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mEMTimerDialog:Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;
    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$602(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;)Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;

    .line 180
    return-void

    .line 172
    .end local v0    # "mWakelockUtil":Lcom/sec/android/emergencymode/util/WakeLockUtil;
    :cond_1
    sget v1, Lcom/sec/android/emergencymode/dialog/EmergencyAutoTimerDialog;->DISMISS_CAUSE:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler$4;->this$0:Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;

    # getter for: Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->mActionListener:Lcom/sec/android/emergencymode/service/EmergencyActionListener;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;->access$200(Lcom/sec/android/emergencymode/service/EmergencyDialogHandler;)Lcom/sec/android/emergencymode/service/EmergencyActionListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/emergencymode/service/EmergencyActionListener;->onCancel()V

    .line 174
    const-string v1, "EmergencyDialogHandler"

    const-string v2, "Auto Timer Dialog is canceled by user."

    invoke-static {v1, v2}, Lcom/sec/android/emergencymode/Elog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
