.class Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;
.super Ljava/lang/Object;
.source "UpsmDisclaimerActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;


# direct methods
.method constructor <init>(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "arg0"    # Landroid/widget/CompoundButton;
    .param p2, "arg1"    # Z

    .prologue
    const/4 v2, 0x0

    .line 217
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    # getter for: Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->mAllAgreeCheck:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->access$000(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->playSoundEffect(I)V

    .line 218
    const-string v0, "CHN"

    const-string v1, "OPEN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    invoke-virtual {v0}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setOKButton()V

    .line 227
    :goto_0
    return-void

    .line 221
    :cond_0
    if-eqz p2, :cond_1

    .line 222
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    # getter for: Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->access$100(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setEnabledButton(Landroid/widget/Button;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->access$200(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;Landroid/widget/Button;Z)V

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    iget-object v1, p0, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity$4;->this$0:Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;

    # getter for: Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->okButton:Landroid/widget/Button;
    invoke-static {v1}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->access$100(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;)Landroid/widget/Button;

    move-result-object v1

    # invokes: Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->setEnabledButton(Landroid/widget/Button;Z)V
    invoke-static {v0, v1, v2}, Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;->access$200(Lcom/sec/android/emergencymode/service/disclaimer/UpsmDisclaimerActivity;Landroid/widget/Button;Z)V

    goto :goto_0
.end method
