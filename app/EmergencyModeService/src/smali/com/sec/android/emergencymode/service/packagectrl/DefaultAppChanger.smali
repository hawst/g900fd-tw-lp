.class public abstract Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;
.super Ljava/lang/Object;
.source "DefaultAppChanger.java"


# static fields
.field public static final INPUTMETHOD:Ljava/lang/String; = "DEFAULT_INPUTMETHOD"

.field public static final LAUNCHER:Ljava/lang/String; = "DEFAULT_LAUNCHER"

.field public static final LAUNCHER_CLASS:Ljava/lang/String; = "DEFAULT_LAUNCHER_CLASS"

.field public static final NFC:Ljava/lang/String; = "DEFAULT_NFC"

.field public static final NFC_CLASS:Ljava/lang/String; = "DEFAULT_NFC_CLASS"

.field public static final NOTIFICATION_ACCESS:Ljava/lang/String; = "DEFAULT_NOTI_ACCESS"

.field public static final SMS:Ljava/lang/String; = "DEFAULT_SMS"

.field private static final TAG:Ljava/lang/String; = "EmergencyPackageController"

.field public static final WALLPAPER:Ljava/lang/String; = "DEFAULT_WALLPAPER"

.field public static final WALLPAPER_CLASS:Ljava/lang/String; = "DEFAULT_WALLPAPER_CLASS"


# instance fields
.field protected mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;->mContext:Landroid/content/Context;

    .line 43
    return-void
.end method


# virtual methods
.method public abstract changeDefaultApp(Z)V
.end method

.method protected loadDefaultApp(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "pref"    # Ljava/lang/String;

    .prologue
    .line 53
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 54
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/sec/android/emergencymode/EmergencySettings;->getString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected saveDefaultApp(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "pref"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v1, p0, Lcom/sec/android/emergencymode/service/packagectrl/DefaultAppChanger;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 49
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-static {v0, p1, p2}, Lcom/sec/android/emergencymode/EmergencySettings;->put(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/Object;)V

    .line 50
    return-void
.end method
