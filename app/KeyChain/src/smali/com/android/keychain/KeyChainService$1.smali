.class Lcom/android/keychain/KeyChainService$1;
.super Landroid/security/IKeyChainService$Stub;
.source "KeyChainService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keychain/KeyChainService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final mKeyStore:Landroid/security/KeyStore;

.field private final mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

.field final synthetic this$0:Lcom/android/keychain/KeyChainService;


# direct methods
.method constructor <init>(Lcom/android/keychain/KeyChainService;)V
    .locals 1

    .prologue
    .line 110
    iput-object p1, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    invoke-direct {p0}, Landroid/security/IKeyChainService$Stub;-><init>()V

    .line 111
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keychain/KeyChainService$1;->mKeyStore:Landroid/security/KeyStore;

    .line 112
    new-instance v0, Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-direct {v0}, Lcom/android/org/conscrypt/TrustedCertificateStore;-><init>()V

    iput-object v0, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    return-void
.end method

.method private checkArgs(Ljava/lang/String;)V
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 141
    if-nez p1, :cond_0

    .line 142
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "alias == null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 144
    :cond_0
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->mKeyStore:Landroid/security/KeyStore;

    invoke-virtual {v1}, Landroid/security/KeyStore;->isUnlocked()Z

    move-result v1

    if-nez v1, :cond_1

    .line 145
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "keystore is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/keychain/KeyChainService$1;->mKeyStore:Landroid/security/KeyStore;

    invoke-virtual {v3}, Landroid/security/KeyStore;->state()Landroid/security/KeyStore$State;

    move-result-object v3

    invoke-virtual {v3}, Landroid/security/KeyStore$State;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 149
    :cond_1
    invoke-static {}, Lcom/android/keychain/KeyChainService$1;->getCallingUid()I

    move-result v0

    .line 150
    .local v0, "callingUid":I
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    iget-object v2, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    iget-object v2, v2, Lcom/android/keychain/KeyChainService;->mDatabaseHelper:Lcom/android/keychain/KeyChainService$DatabaseHelper;

    invoke-virtual {v2}, Lcom/android/keychain/KeyChainService$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    # invokes: Lcom/android/keychain/KeyChainService;->hasGrantInternal(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)Z
    invoke-static {v1, v2, v0, p1}, Lcom/android/keychain/KeyChainService;->access$000(Lcom/android/keychain/KeyChainService;Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 151
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uid "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t have permission to access the requested alias"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 154
    :cond_2
    return-void
.end method

.method private checkCEPCaller()Z
    .locals 3

    .prologue
    .line 340
    iget-object v2, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    invoke-virtual {v2}, Lcom/android/keychain/KeyChainService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 341
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 342
    .local v0, "actualForSCEP":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v2, "android"

    invoke-virtual {v1, v2, v0}, Landroid/content/pm/PackageManager;->checkSignatures(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    .line 344
    const/4 v2, 0x1

    .line 346
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private checkCaller(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "expectedPackage"    # Ljava/lang/String;

    .prologue
    .line 335
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    invoke-virtual {v1}, Lcom/android/keychain/KeyChainService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {}, Lcom/android/keychain/KeyChainService$1;->getCallingUid()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getNameForUid(I)Ljava/lang/String;

    move-result-object v0

    .line 336
    .local v0, "actualPackage":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .end local v0    # "actualPackage":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "actualPackage":Ljava/lang/String;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkCertInstallerOrSystemCaller()V
    .locals 2

    .prologue
    .line 307
    const-string v1, "com.android.certinstaller"

    invoke-direct {p0, v1}, Lcom/android/keychain/KeyChainService$1;->checkCaller(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 308
    .local v0, "actual":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkCEPCaller()Z

    move-result v1

    if-nez v1, :cond_0

    .line 317
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    goto :goto_0
.end method

.method private checkSystemCaller()V
    .locals 2

    .prologue
    .line 320
    const-string v1, "android.uid.system:1000"

    invoke-direct {p0, v1}, Lcom/android/keychain/KeyChainService$1;->checkCaller(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 321
    .local v0, "actual":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 322
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 324
    :cond_0
    return-void
.end method

.method private checkUserRestriction()V
    .locals 3

    .prologue
    .line 326
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    const-string v2, "user"

    invoke-virtual {v1, v2}, Lcom/android/keychain/KeyChainService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 327
    .local v0, "um":Landroid/os/UserManager;
    const-string v1, "no_config_credentials"

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->hasUserRestriction(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 328
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "User cannot modify credentials"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 330
    :cond_0
    return-void
.end method

.method private deleteCertificateEntry(Ljava/lang/String;)Z
    .locals 5
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 295
    :try_start_0
    iget-object v2, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v2, p1}, Lcom/android/org/conscrypt/TrustedCertificateStore;->deleteCertificateEntry(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 296
    const/4 v1, 0x1

    .line 302
    :goto_0
    return v1

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "KeyChain"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Problem removing CA certificate "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 300
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 301
    .local v0, "e":Ljava/security/cert/CertificateException;
    const-string v2, "KeyChain"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Problem removing CA certificate "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private makeAliasesParcelableSynchronised(Ljava/util/Set;)Landroid/content/pm/ParceledListSlice;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/content/pm/ParceledListSlice",
            "<",
            "Lcom/android/internal/util/ParcelableString;",
            ">;"
        }
    .end annotation

    .prologue
    .line 420
    .local p1, "aliasSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 421
    .local v1, "aliases":Ljava/util/List;, "Ljava/util/List<Lcom/android/internal/util/ParcelableString;>;"
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 422
    .local v0, "alias":Ljava/lang/String;
    new-instance v3, Lcom/android/internal/util/ParcelableString;

    invoke-direct {v3}, Lcom/android/internal/util/ParcelableString;-><init>()V

    .line 423
    .local v3, "parcelableString":Lcom/android/internal/util/ParcelableString;
    iput-object v0, v3, Lcom/android/internal/util/ParcelableString;->string:Ljava/lang/String;

    .line 424
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 426
    .end local v0    # "alias":Ljava/lang/String;
    .end local v3    # "parcelableString":Lcom/android/internal/util/ParcelableString;
    :cond_0
    new-instance v4, Landroid/content/pm/ParceledListSlice;

    invoke-direct {v4, v1}, Landroid/content/pm/ParceledListSlice;-><init>(Ljava/util/List;)V

    return-object v4
.end method

.method private parseCertificate([B)Ljava/security/cert/X509Certificate;
    .locals 2
    .param p1, "bytes"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 228
    const-string v1, "X.509"

    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    .line 229
    .local v0, "cf":Ljava/security/cert/CertificateFactory;
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v1

    check-cast v1, Ljava/security/cert/X509Certificate;

    return-object v1
.end method


# virtual methods
.method public allSystemAliases()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 404
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    .line 405
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v1

    .line 407
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v2}, Lcom/android/org/conscrypt/TrustedCertificateStore;->allSystemAliases()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1

    return-object v0

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public containsAlias(Ljava/lang/String;)Z
    .locals 2
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 412
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    .line 413
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v1

    .line 414
    :try_start_0
    iget-object v0, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v0, p1}, Lcom/android/org/conscrypt/TrustedCertificateStore;->containsAlias(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 415
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public containsCaAlias(Ljava/lang/String;)Z
    .locals 1
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 447
    iget-object v0, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v0, p1}, Lcom/android/org/conscrypt/TrustedCertificateStore;->containsAlias(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public deleteCaCertificate(Ljava/lang/String;)Z
    .locals 9
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x5

    const/4 v1, 0x1

    .line 254
    invoke-static {p1}, Lcom/android/org/conscrypt/TrustedCertificateStore;->isUser(Ljava/lang/String;)Z

    move-result v6

    .line 258
    .local v6, "isUser":Z
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    .line 259
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkUserRestriction()V

    .line 260
    const/4 v7, 0x1

    .line 261
    .local v7, "ok":Z
    iget-object v3, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v3

    .line 262
    :try_start_0
    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainService$1;->deleteCertificateEntry(Ljava/lang/String;)Z

    move-result v7

    .line 263
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    if-eqz v7, :cond_1

    .line 267
    if-eqz v6, :cond_0

    .line 268
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "KeyChainService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Deleting certificate "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " succeeded"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v2, v1

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 289
    :goto_0
    iget-object v0, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    # invokes: Lcom/android/keychain/KeyChainService;->broadcastStorageChange()V
    invoke-static {v0}, Lcom/android/keychain/KeyChainService;->access$100(Lcom/android/keychain/KeyChainService;)V

    .line 290
    return v7

    .line 263
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 272
    :cond_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "KeyChainService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Disabling certificate "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " succeeded"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move v2, v1

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 277
    :cond_1
    if-eqz v6, :cond_2

    .line 278
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "KeyChainService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Deleting certificate "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " failed"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 282
    :cond_2
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "KeyChainService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Disabling certificate "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " failed"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getCaCertificateChainAliases(Ljava/lang/String;Z)Ljava/util/List;
    .locals 11
    .param p1, "rootAlias"    # Ljava/lang/String;
    .param p2, "includeDeletedSystem"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 471
    iget-object v8, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v8

    .line 472
    :try_start_0
    iget-object v7, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v7, p1, p2}, Lcom/android/org/conscrypt/TrustedCertificateStore;->getCertificate(Ljava/lang/String;Z)Ljava/security/cert/Certificate;

    move-result-object v6

    check-cast v6, Ljava/security/cert/X509Certificate;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 475
    .local v6, "root":Ljava/security/cert/X509Certificate;
    :try_start_1
    iget-object v7, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v7, v6}, Lcom/android/org/conscrypt/TrustedCertificateStore;->getCertificateChain(Ljava/security/cert/X509Certificate;)Ljava/util/List;

    move-result-object v2

    .line 477
    .local v2, "chain":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v7

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 478
    .local v1, "aliases":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    .line 479
    .local v5, "n":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v5, :cond_1

    .line 480
    iget-object v9, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/security/cert/Certificate;

    const/4 v10, 0x1

    invoke-virtual {v9, v7, v10}, Lcom/android/org/conscrypt/TrustedCertificateStore;->getCertificateAlias(Ljava/security/cert/Certificate;Z)Ljava/lang/String;

    move-result-object v0

    .line 482
    .local v0, "alias":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 483
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 479
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 486
    .end local v0    # "alias":Ljava/lang/String;
    :cond_1
    :try_start_2
    monitor-exit v8

    .line 489
    .end local v1    # "aliases":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "chain":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/X509Certificate;>;"
    .end local v4    # "i":I
    .end local v5    # "n":I
    :goto_1
    return-object v1

    .line 487
    :catch_0
    move-exception v3

    .line 488
    .local v3, "e":Ljava/security/cert/CertificateException;
    const-string v7, "KeyChain"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Error retrieving cert chain for root "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    monitor-exit v8

    goto :goto_1

    .line 491
    .end local v3    # "e":Ljava/security/cert/CertificateException;
    .end local v6    # "root":Ljava/security/cert/X509Certificate;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v7
.end method

.method public getCertificate(Ljava/lang/String;)[B
    .locals 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainService$1;->checkArgs(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/android/keychain/KeyChainService$1;->mKeyStore:Landroid/security/KeyStore;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "USRCERT_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/security/KeyStore;->get(Ljava/lang/String;)[B

    move-result-object v0

    return-object v0
.end method

.method public getCertificateAlias([B)Ljava/lang/String;
    .locals 8
    .param p1, "cert"    # [B

    .prologue
    .line 389
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    .line 390
    iget-object v5, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v5

    .line 391
    const/4 v0, 0x0

    .line 393
    .local v0, "alias":Ljava/lang/String;
    :try_start_0
    const-string v4, "X.509"

    invoke-static {v4}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v2

    .line 394
    .local v2, "cf":Ljava/security/cert/CertificateFactory;
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v2, v4}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v1

    .line 395
    .local v1, "c":Ljava/security/cert/Certificate;
    iget-object v4, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v4, v1}, Lcom/android/org/conscrypt/TrustedCertificateStore;->getCertificateAlias(Ljava/security/cert/Certificate;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 399
    .end local v1    # "c":Ljava/security/cert/Certificate;
    .end local v2    # "cf":Ljava/security/cert/CertificateFactory;
    :goto_0
    :try_start_1
    monitor-exit v5

    return-object v0

    .line 396
    :catch_0
    move-exception v3

    .line 397
    .local v3, "e":Ljava/security/cert/CertificateException;
    const-string v4, "KeyChain"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Problem converting CA certificate "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 400
    .end local v3    # "e":Ljava/security/cert/CertificateException;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public getCertificateFromTrustCredential(Ljava/lang/String;Z)[B
    .locals 8
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "includeDeletedSystem"    # Z

    .prologue
    .line 362
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    .line 363
    iget-object v5, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v5

    .line 364
    const/4 v2, 0x0

    .line 365
    .local v2, "certData":[B
    :try_start_0
    iget-object v4, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v4, p1, p2}, Lcom/android/org/conscrypt/TrustedCertificateStore;->getCertificate(Ljava/lang/String;Z)Ljava/security/cert/Certificate;

    move-result-object v1

    .line 366
    .local v1, "cert":Ljava/security/cert/Certificate;
    if-eqz v1, :cond_0

    .line 367
    const/4 v4, 0x1

    new-array v0, v4, [Ljava/security/cert/Certificate;

    const/4 v4, 0x0

    aput-object v1, v0, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    .local v0, "ca":[Ljava/security/cert/Certificate;
    :try_start_1
    invoke-static {v0}, Landroid/security/Credentials;->convertToPem([Ljava/security/cert/Certificate;)[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 376
    .end local v0    # "ca":[Ljava/security/cert/Certificate;
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v5

    return-object v2

    .line 370
    .restart local v0    # "ca":[Ljava/security/cert/Certificate;
    :catch_0
    move-exception v3

    .line 371
    .local v3, "e":Ljava/io/IOException;
    const-string v4, "KeyChain"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Problem retrieving CA certificate "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 377
    .end local v0    # "ca":[Ljava/security/cert/Certificate;
    .end local v1    # "cert":Ljava/security/cert/Certificate;
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 372
    .restart local v0    # "ca":[Ljava/security/cert/Certificate;
    .restart local v1    # "cert":Ljava/security/cert/Certificate;
    :catch_1
    move-exception v3

    .line 373
    .local v3, "e":Ljava/security/cert/CertificateEncodingException;
    :try_start_3
    const-string v4, "KeyChain"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Problem retrieving CA certificate "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public getEncodedCaCertificate(Ljava/lang/String;Z)[B
    .locals 7
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "includeDeletedSystem"    # Z

    .prologue
    const/4 v2, 0x0

    .line 452
    iget-object v3, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v3

    .line 453
    :try_start_0
    iget-object v4, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v4, p1, p2}, Lcom/android/org/conscrypt/TrustedCertificateStore;->getCertificate(Ljava/lang/String;Z)Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 455
    .local v0, "certificate":Ljava/security/cert/X509Certificate;
    if-nez v0, :cond_0

    .line 456
    const-string v4, "KeyChain"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Could not find CA certificate "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    :goto_0
    return-object v2

    .line 460
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getEncoded()[B
    :try_end_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    :try_start_2
    monitor-exit v3

    goto :goto_0

    .line 465
    .end local v0    # "certificate":Ljava/security/cert/X509Certificate;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    .line 461
    .restart local v0    # "certificate":Ljava/security/cert/X509Certificate;
    :catch_0
    move-exception v1

    .line 462
    .local v1, "e":Ljava/security/cert/CertificateEncodingException;
    :try_start_3
    const-string v4, "KeyChain"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error while encoding CA certificate "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public getSystemCaAliases()Landroid/content/pm/ParceledListSlice;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/content/pm/ParceledListSlice",
            "<",
            "Lcom/android/internal/util/ParcelableString;",
            ">;"
        }
    .end annotation

    .prologue
    .line 439
    iget-object v2, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v2

    .line 440
    :try_start_0
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v1}, Lcom/android/org/conscrypt/TrustedCertificateStore;->allSystemAliases()Ljava/util/Set;

    move-result-object v0

    .line 441
    .local v0, "aliasSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0, v0}, Lcom/android/keychain/KeyChainService$1;->makeAliasesParcelableSynchronised(Ljava/util/Set;)Landroid/content/pm/ParceledListSlice;

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 442
    .end local v0    # "aliasSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public getUserCaAliases()Landroid/content/pm/ParceledListSlice;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/content/pm/ParceledListSlice",
            "<",
            "Lcom/android/internal/util/ParcelableString;",
            ">;"
        }
    .end annotation

    .prologue
    .line 431
    iget-object v2, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v2

    .line 432
    :try_start_0
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v1}, Lcom/android/org/conscrypt/TrustedCertificateStore;->userAliases()Ljava/util/Set;

    move-result-object v0

    .line 433
    .local v0, "aliasSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-direct {p0, v0}, Lcom/android/keychain/KeyChainService$1;->makeAliasesParcelableSynchronised(Ljava/util/Set;)Landroid/content/pm/ParceledListSlice;

    move-result-object v1

    monitor-exit v2

    return-object v1

    .line 434
    .end local v0    # "aliasSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public hasGrant(ILjava/lang/String;)Z
    .locals 2
    .param p1, "uid"    # I
    .param p2, "alias"    # Ljava/lang/String;

    .prologue
    .line 351
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    .line 352
    iget-object v0, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    iget-object v1, v1, Lcom/android/keychain/KeyChainService;->mDatabaseHelper:Lcom/android/keychain/KeyChainService$DatabaseHelper;

    invoke-virtual {v1}, Lcom/android/keychain/KeyChainService$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    # invokes: Lcom/android/keychain/KeyChainService;->hasGrantInternal(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)Z
    invoke-static {v0, v1, p1, p2}, Lcom/android/keychain/KeyChainService;->access$000(Lcom/android/keychain/KeyChainService;Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public installCaCertificate([B)V
    .locals 14
    .param p1, "caCertificate"    # [B

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x5

    const/4 v1, 0x1

    .line 157
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkCertInstallerOrSystemCaller()V

    .line 158
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkUserRestriction()V

    .line 161
    iget-object v3, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    iget-object v3, v3, Lcom/android/keychain/KeyChainService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v10

    .line 162
    .local v10, "cp":Lcom/sec/enterprise/knox/certificate/CertificatePolicy;
    if-eqz v10, :cond_0

    invoke-virtual {v10, p1, v1}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->isCaCertificateTrusted([BZ)Z

    move-result v3

    if-nez v3, :cond_0

    .line 163
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "KeyChainService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Installing certificate "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " failed"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 205
    :goto_0
    return-void

    .line 171
    :cond_0
    :try_start_0
    iget-object v12, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v12
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 172
    :try_start_1
    iget-object v3, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainService$1;->parseCertificate([B)Ljava/security/cert/X509Certificate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/org/conscrypt/TrustedCertificateStore;->installCertificate(Ljava/security/cert/X509Certificate;)V

    .line 176
    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainService$1;->parseCertificate([B)Ljava/security/cert/X509Certificate;

    move-result-object v9

    .line 177
    .local v9, "c":Ljava/security/cert/X509Certificate;
    iget-object v3, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v3, v9}, Lcom/android/org/conscrypt/TrustedCertificateStore;->getCertificateAlias(Ljava/security/cert/Certificate;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 178
    const/4 v3, 0x5

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v6

    const-string v7, "KeyChainService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Installing certificate "

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v13, " succeeded"

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v3 .. v8}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 184
    :cond_1
    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    iget-object v0, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    # invokes: Lcom/android/keychain/KeyChainService;->broadcastStorageChange()V
    invoke-static {v0}, Lcom/android/keychain/KeyChainService;->access$100(Lcom/android/keychain/KeyChainService;)V

    goto :goto_0

    .line 184
    .end local v9    # "c":Ljava/security/cert/X509Certificate;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v12
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_1

    .line 185
    :catch_0
    move-exception v11

    .line 188
    .local v11, "e":Ljava/io/IOException;
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "KeyChainService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Installing certificate "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " failed"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 193
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 194
    .end local v11    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v11

    .line 197
    .local v11, "e":Ljava/security/cert/CertificateException;
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const-string v4, "KeyChainService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Installing certificate "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " failed"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 202
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public installKeyPair([B[BLjava/lang/String;)Z
    .locals 6
    .param p1, "privateKey"    # [B
    .param p2, "userCertificate"    # [B
    .param p3, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v5, -0x1

    const/4 v1, 0x1

    .line 209
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkCertInstallerOrSystemCaller()V

    .line 210
    iget-object v2, p0, Lcom/android/keychain/KeyChainService$1;->mKeyStore:Landroid/security/KeyStore;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "USRPKEY_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p1, v5, v1}, Landroid/security/KeyStore;->importKey(Ljava/lang/String;[BII)Z

    move-result v2

    if-nez v2, :cond_1

    .line 212
    const-string v1, "KeyChain"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to import private key "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :cond_0
    :goto_0
    return v0

    .line 215
    :cond_1
    iget-object v2, p0, Lcom/android/keychain/KeyChainService$1;->mKeyStore:Landroid/security/KeyStore;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "USRCERT_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2, v5, v1}, Landroid/security/KeyStore;->put(Ljava/lang/String;[BII)Z

    move-result v2

    if-nez v2, :cond_2

    .line 217
    const-string v1, "KeyChain"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to import user certificate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->mKeyStore:Landroid/security/KeyStore;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "USRPKEY_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/security/KeyStore;->delKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 219
    const-string v1, "KeyChain"

    const-string v2, "Failed to delete private key after certificate importing failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 223
    :cond_2
    iget-object v0, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    # invokes: Lcom/android/keychain/KeyChainService;->broadcastStorageChange()V
    invoke-static {v0}, Lcom/android/keychain/KeyChainService;->access$100(Lcom/android/keychain/KeyChainService;)V

    move v0, v1

    .line 224
    goto :goto_0
.end method

.method public requestPrivateKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainService$1;->checkArgs(Ljava/lang/String;)V

    .line 119
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "USRPKEY_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "keystoreAlias":Ljava/lang/String;
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    .line 121
    .local v3, "uid":I
    iget-object v5, p0, Lcom/android/keychain/KeyChainService$1;->mKeyStore:Landroid/security/KeyStore;

    invoke-virtual {v5, v0, v3}, Landroid/security/KeyStore;->grant(Ljava/lang/String;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 122
    const/4 v5, 0x0

    .line 132
    :goto_0
    return-object v5

    .line 124
    :cond_0
    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v4

    .line 125
    .local v4, "userHandle":I
    const/16 v5, 0x3e8

    invoke-static {v4, v5}, Landroid/os/UserHandle;->getUid(II)I

    move-result v2

    .line 127
    .local v2, "systemUidForUser":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 128
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 129
    const/16 v5, 0x5f

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 130
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public reset()Z
    .locals 5

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    .line 235
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkUserRestriction()V

    .line 236
    iget-object v3, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    iget-object v4, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    iget-object v4, v4, Lcom/android/keychain/KeyChainService;->mDatabaseHelper:Lcom/android/keychain/KeyChainService$DatabaseHelper;

    invoke-virtual {v4}, Lcom/android/keychain/KeyChainService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    # invokes: Lcom/android/keychain/KeyChainService;->removeAllGrants(Landroid/database/sqlite/SQLiteDatabase;)V
    invoke-static {v3, v4}, Lcom/android/keychain/KeyChainService;->access$200(Lcom/android/keychain/KeyChainService;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 237
    const/4 v2, 0x1

    .line 238
    .local v2, "ok":Z
    iget-object v4, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v4

    .line 240
    :try_start_0
    iget-object v3, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v3}, Lcom/android/org/conscrypt/TrustedCertificateStore;->aliases()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 241
    .local v0, "alias":Ljava/lang/String;
    invoke-static {v0}, Lcom/android/org/conscrypt/TrustedCertificateStore;->isUser(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 242
    invoke-direct {p0, v0}, Lcom/android/keychain/KeyChainService$1;->deleteCertificateEntry(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 243
    const/4 v2, 0x0

    goto :goto_0

    .line 247
    .end local v0    # "alias":Ljava/lang/String;
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    iget-object v3, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    # invokes: Lcom/android/keychain/KeyChainService;->broadcastStorageChange()V
    invoke-static {v3}, Lcom/android/keychain/KeyChainService;->access$100(Lcom/android/keychain/KeyChainService;)V

    .line 249
    return v2

    .line 247
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method public setGrant(ILjava/lang/String;Z)V
    .locals 2
    .param p1, "uid"    # I
    .param p2, "alias"    # Ljava/lang/String;
    .param p3, "value"    # Z

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    .line 357
    iget-object v0, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    iget-object v1, v1, Lcom/android/keychain/KeyChainService;->mDatabaseHelper:Lcom/android/keychain/KeyChainService$DatabaseHelper;

    invoke-virtual {v1}, Lcom/android/keychain/KeyChainService$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    # invokes: Lcom/android/keychain/KeyChainService;->setGrantInternal(Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;Z)V
    invoke-static {v0, v1, p1, p2, p3}, Lcom/android/keychain/KeyChainService;->access$300(Lcom/android/keychain/KeyChainService;Landroid/database/sqlite/SQLiteDatabase;ILjava/lang/String;Z)V

    .line 358
    iget-object v0, p0, Lcom/android/keychain/KeyChainService$1;->this$0:Lcom/android/keychain/KeyChainService;

    # invokes: Lcom/android/keychain/KeyChainService;->broadcastStorageChange()V
    invoke-static {v0}, Lcom/android/keychain/KeyChainService;->access$100(Lcom/android/keychain/KeyChainService;)V

    .line 359
    return-void
.end method

.method public userAliases()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 381
    invoke-direct {p0}, Lcom/android/keychain/KeyChainService$1;->checkSystemCaller()V

    .line 382
    iget-object v1, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    monitor-enter v1

    .line 384
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/android/keychain/KeyChainService$1;->mTrustedCertificateStore:Lcom/android/org/conscrypt/TrustedCertificateStore;

    invoke-virtual {v2}, Lcom/android/org/conscrypt/TrustedCertificateStore;->userAliases()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1

    return-object v0

    .line 385
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
