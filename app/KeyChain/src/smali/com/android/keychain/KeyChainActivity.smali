.class public Lcom/android/keychain/KeyChainActivity;
.super Landroid/app/Activity;
.source "KeyChainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keychain/KeyChainActivity$6;,
        Lcom/android/keychain/KeyChainActivity$ResponseSender;,
        Lcom/android/keychain/KeyChainActivity$ViewHolder;,
        Lcom/android/keychain/KeyChainActivity$CertificateAdapter;,
        Lcom/android/keychain/KeyChainActivity$AliasLoader;,
        Lcom/android/keychain/KeyChainActivity$State;
    }
.end annotation


# static fields
.field private static KEY_STATE:Ljava/lang/String;


# instance fields
.field private mKeyStore:Landroid/security/KeyStore;

.field private mSender:Landroid/app/PendingIntent;

.field private mSenderUid:I

.field private mState:Lcom/android/keychain/KeyChainActivity$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-string v0, "state"

    sput-object v0, Lcom/android/keychain/KeyChainActivity;->KEY_STATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 88
    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mKeyStore:Landroid/security/KeyStore;

    .line 460
    return-void
.end method

.method static synthetic access$100(Lcom/android/keychain/KeyChainActivity;)Landroid/app/PendingIntent;
    .locals 1
    .param p0, "x0"    # Lcom/android/keychain/KeyChainActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/keychain/KeyChainActivity;)Landroid/security/KeyStore;
    .locals 1
    .param p0, "x0"    # Lcom/android/keychain/KeyChainActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mKeyStore:Landroid/security/KeyStore;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/keychain/KeyChainActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/keychain/KeyChainActivity;

    .prologue
    .line 67
    iget v0, p0, Lcom/android/keychain/KeyChainActivity;->mSenderUid:I

    return v0
.end method

.method static synthetic access$500(Lcom/android/keychain/KeyChainActivity;Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/keychain/KeyChainActivity;
    .param p1, "x1"    # Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainActivity;->displayCertChooserDialog(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V

    return-void
.end method

.method static synthetic access$700(Lcom/android/keychain/KeyChainActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/keychain/KeyChainActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    return-void
.end method

.method private displayCertChooserDialog(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V
    .locals 33
    .param p1, "adapter"    # Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    .prologue
    .line 216
    new-instance v8, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 218
    .local v8, "builder":Landroid/app/AlertDialog$Builder;
    const v29, 0x7f020002

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 219
    .local v10, "contextView":Landroid/widget/TextView;
    const v29, 0x7f020001

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    .line 221
    .local v14, "footer":Landroid/view/View;
    const/high16 v29, 0x7f020000

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ListView;

    .line 222
    .local v22, "lv":Landroid/widget/ListView;
    const/16 v29, 0x0

    const/16 v30, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v10, v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 223
    const/16 v29, 0x0

    const/16 v30, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v14, v1, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 224
    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 225
    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 227
    new-instance v29, Lcom/android/keychain/KeyChainActivity$1;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/android/keychain/KeyChainActivity$1;-><init>(Lcom/android/keychain/KeyChainActivity;Landroid/widget/ListView;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 233
    # getter for: Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;
    invoke-static/range {p1 .. p1}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->access$600(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)Ljava/util/List;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->isEmpty()Z

    move-result v13

    .line 234
    .local v13, "empty":Z
    if-eqz v13, :cond_2

    const/high16 v23, 0x1040000

    .line 235
    .local v23, "negativeLabel":I
    :goto_0
    new-instance v29, Lcom/android/keychain/KeyChainActivity$2;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/keychain/KeyChainActivity$2;-><init>(Lcom/android/keychain/KeyChainActivity;)V

    move/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v8, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 242
    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    .line 243
    .local v27, "res":Landroid/content/res/Resources;
    if-eqz v13, :cond_3

    .line 244
    const v29, 0x7f030001

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 278
    .local v28, "title":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 279
    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    .line 285
    .local v11, "dialog":Landroid/app/Dialog;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/content/IntentSender;->getTargetPackage()Ljava/lang/String;

    move-result-object v24

    .line 286
    .local v24, "pkg":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v25

    .line 289
    .local v25, "pm":Landroid/content/pm/PackageManager;
    const/16 v29, 0x0

    :try_start_0
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v29

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 293
    .local v7, "applicationLabel":Ljava/lang/CharSequence;
    :goto_2
    const v29, 0x7f030003

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    aput-object v7, v30, v31

    invoke-static/range {v29 .. v30}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 296
    .local v6, "appMessage":Ljava/lang/String;
    move-object v9, v6

    .line 297
    .local v9, "contextMessage":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v29

    const-string v30, "host"

    invoke-virtual/range {v29 .. v30}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 298
    .local v15, "host":Ljava/lang/String;
    if-eqz v15, :cond_1

    .line 299
    move-object/from16 v17, v15

    .line 300
    .local v17, "hostString":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v29

    const-string v30, "port"

    const/16 v31, -0x1

    invoke-virtual/range {v29 .. v31}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v26

    .line 301
    .local v26, "port":I
    const/16 v29, -0x1

    move/from16 v0, v26

    move/from16 v1, v29

    if-eq v0, v1, :cond_0

    .line 302
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ":"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 304
    :cond_0
    const v29, 0x7f030004

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    aput-object v17, v30, v31

    invoke-static/range {v29 .. v30}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 306
    .local v16, "hostMessage":Ljava/lang/String;
    if-nez v9, :cond_6

    .line 307
    move-object/from16 v9, v16

    .line 312
    .end local v16    # "hostMessage":Ljava/lang/String;
    .end local v17    # "hostString":Ljava/lang/String;
    .end local v26    # "port":I
    :cond_1
    :goto_3
    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 314
    const v29, 0x7f030005

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x2

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    const-string v32, ".pfx"

    aput-object v32, v30, v31

    const/16 v31, 0x1

    const-string v32, ".p12"

    aput-object v32, v30, v31

    invoke-static/range {v29 .. v30}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 316
    .local v19, "installMessage":Ljava/lang/String;
    const/high16 v29, 0x7f050000

    move/from16 v0, v29

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 317
    .local v20, "installText":Landroid/widget/TextView;
    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    const v29, 0x7f050001

    move/from16 v0, v29

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/Button;

    .line 320
    .local v18, "installButton":Landroid/widget/Button;
    new-instance v29, Lcom/android/keychain/KeyChainActivity$4;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11}, Lcom/android/keychain/KeyChainActivity$4;-><init>(Lcom/android/keychain/KeyChainActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 330
    new-instance v29, Lcom/android/keychain/KeyChainActivity$5;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/keychain/KeyChainActivity$5;-><init>(Lcom/android/keychain/KeyChainActivity;)V

    move-object/from16 v0, v29

    invoke-virtual {v11, v0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 335
    invoke-virtual {v11}, Landroid/app/Dialog;->show()V

    .line 336
    return-void

    .line 234
    .end local v6    # "appMessage":Ljava/lang/String;
    .end local v7    # "applicationLabel":Ljava/lang/CharSequence;
    .end local v9    # "contextMessage":Ljava/lang/String;
    .end local v11    # "dialog":Landroid/app/Dialog;
    .end local v15    # "host":Ljava/lang/String;
    .end local v18    # "installButton":Landroid/widget/Button;
    .end local v19    # "installMessage":Ljava/lang/String;
    .end local v20    # "installText":Landroid/widget/TextView;
    .end local v23    # "negativeLabel":I
    .end local v24    # "pkg":Ljava/lang/String;
    .end local v25    # "pm":Landroid/content/pm/PackageManager;
    .end local v27    # "res":Landroid/content/res/Resources;
    .end local v28    # "title":Ljava/lang/String;
    :cond_2
    const v23, 0x7f030008

    goto/16 :goto_0

    .line 246
    .restart local v23    # "negativeLabel":I
    .restart local v27    # "res":Landroid/content/res/Resources;
    :cond_3
    const v29, 0x7f030002

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 247
    .restart local v28    # "title":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v29

    const-string v30, "alias"

    invoke-virtual/range {v29 .. v30}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 248
    .local v5, "alias":Ljava/lang/String;
    if-eqz v5, :cond_5

    .line 250
    # getter for: Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;
    invoke-static/range {p1 .. p1}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->access$600(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)Ljava/util/List;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 251
    .local v4, "adapterPosition":I
    const/16 v29, -0x1

    move/from16 v0, v29

    if-eq v4, v0, :cond_4

    .line 252
    add-int/lit8 v21, v4, 0x1

    .line 253
    .local v21, "listViewPosition":I
    const/16 v29, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v21

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 262
    .end local v4    # "adapterPosition":I
    .end local v21    # "listViewPosition":I
    :cond_4
    :goto_4
    const v29, 0x7f030007

    new-instance v30, Lcom/android/keychain/KeyChainActivity$3;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/android/keychain/KeyChainActivity$3;-><init>(Lcom/android/keychain/KeyChainActivity;Landroid/widget/ListView;Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V

    move/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v8, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    .line 255
    :cond_5
    # getter for: Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;
    invoke-static/range {p1 .. p1}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->access$600(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)Ljava/util/List;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v29

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_4

    .line 257
    const/4 v4, 0x0

    .line 258
    .restart local v4    # "adapterPosition":I
    add-int/lit8 v21, v4, 0x1

    .line 259
    .restart local v21    # "listViewPosition":I
    const/16 v29, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v21

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_4

    .line 290
    .end local v4    # "adapterPosition":I
    .end local v5    # "alias":Ljava/lang/String;
    .end local v21    # "listViewPosition":I
    .restart local v11    # "dialog":Landroid/app/Dialog;
    .restart local v24    # "pkg":Ljava/lang/String;
    .restart local v25    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v12

    .line 291
    .local v12, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    move-object/from16 v7, v24

    .restart local v7    # "applicationLabel":Ljava/lang/CharSequence;
    goto/16 :goto_2

    .line 309
    .end local v12    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v6    # "appMessage":Ljava/lang/String;
    .restart local v9    # "contextMessage":Ljava/lang/String;
    .restart local v15    # "host":Ljava/lang/String;
    .restart local v16    # "hostMessage":Ljava/lang/String;
    .restart local v17    # "hostString":Ljava/lang/String;
    .restart local v26    # "port":I
    :cond_6
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_3
.end method

.method private finish(Ljava/lang/String;)V
    .locals 5
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 443
    if-nez p1, :cond_0

    .line 444
    invoke-virtual {p0, v4}, Lcom/android/keychain/KeyChainActivity;->setResult(I)V

    .line 450
    :goto_0
    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "response"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getIBinderExtra(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/security/IKeyChainAliasCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/security/IKeyChainAliasCallback;

    move-result-object v0

    .line 453
    .local v0, "keyChainAliasResponse":Landroid/security/IKeyChainAliasCallback;
    if-eqz v0, :cond_1

    .line 454
    new-instance v2, Lcom/android/keychain/KeyChainActivity$ResponseSender;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v0, p1, v3}, Lcom/android/keychain/KeyChainActivity$ResponseSender;-><init>(Lcom/android/keychain/KeyChainActivity;Landroid/security/IKeyChainAliasCallback;Ljava/lang/String;Lcom/android/keychain/KeyChainActivity$1;)V

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/android/keychain/KeyChainActivity$ResponseSender;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 458
    :goto_1
    return-void

    .line 446
    .end local v0    # "keyChainAliasResponse":Landroid/security/IKeyChainAliasCallback;
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 447
    .local v1, "result":Landroid/content/Intent;
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 448
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/android/keychain/KeyChainActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    .line 457
    .end local v1    # "result":Landroid/content/Intent;
    .restart local v0    # "keyChainAliasResponse":Landroid/security/IKeyChainAliasCallback;
    :cond_1
    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->finish()V

    goto :goto_1
.end method

.method private showCertChooserDialog()V
    .locals 2

    .prologue
    .line 170
    new-instance v0, Lcom/android/keychain/KeyChainActivity$AliasLoader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/keychain/KeyChainActivity$AliasLoader;-><init>(Lcom/android/keychain/KeyChainActivity;Lcom/android/keychain/KeyChainActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/keychain/KeyChainActivity$AliasLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 171
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 427
    packed-switch p1, :pswitch_data_0

    .line 438
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 429
    :pswitch_0
    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mKeyStore:Landroid/security/KeyStore;

    invoke-virtual {v0}, Landroid/security/KeyStore;->isUnlocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    sget-object v0, Lcom/android/keychain/KeyChainActivity$State;->INITIAL:Lcom/android/keychain/KeyChainActivity$State;

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    .line 431
    invoke-direct {p0}, Lcom/android/keychain/KeyChainActivity;->showCertChooserDialog()V

    .line 436
    :goto_0
    return-void

    .line 434
    :cond_0
    sget-object v0, Lcom/android/keychain/KeyChainActivity$State;->UNLOCK_CANCELED:Lcom/android/keychain/KeyChainActivity$State;

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    goto :goto_0

    .line 427
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 495
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    .line 496
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 91
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 92
    if-nez p1, :cond_1

    .line 93
    sget-object v0, Lcom/android/keychain/KeyChainActivity$State;->INITIAL:Lcom/android/keychain/KeyChainActivity$State;

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    sget-object v0, Lcom/android/keychain/KeyChainActivity;->KEY_STATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/android/keychain/KeyChainActivity$State;

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    .line 96
    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    if-nez v0, :cond_0

    .line 97
    sget-object v0, Lcom/android/keychain/KeyChainActivity$State;->INITIAL:Lcom/android/keychain/KeyChainActivity$State;

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    goto :goto_0
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 103
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 105
    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "sender"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Landroid/app/PendingIntent;

    iput-object v5, p0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    .line 106
    iget-object v5, p0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    if-nez v5, :cond_0

    .line 108
    invoke-direct {p0, v8}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    .line 163
    :goto_0
    :pswitch_0
    return-void

    .line 112
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v6, p0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    invoke-virtual {v6}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/IntentSender;->getTargetPackage()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v5, p0, Lcom/android/keychain/KeyChainActivity;->mSenderUid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    sget-object v5, Lcom/android/keychain/KeyChainActivity$6;->$SwitchMap$com$android$keychain$KeyChainActivity$State:[I

    iget-object v6, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    invoke-virtual {v6}, Lcom/android/keychain/KeyChainActivity$State;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 165
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    .line 114
    :catch_0
    move-exception v1

    .line 117
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-direct {p0, v8}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    goto :goto_0

    .line 124
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :pswitch_1
    iget-object v5, p0, Lcom/android/keychain/KeyChainActivity;->mKeyStore:Landroid/security/KeyStore;

    invoke-virtual {v5}, Landroid/security/KeyStore;->isUnlocked()Z

    move-result v5

    if-nez v5, :cond_1

    .line 125
    sget-object v5, Lcom/android/keychain/KeyChainActivity$State;->UNLOCK_REQUESTED:Lcom/android/keychain/KeyChainActivity$State;

    iput-object v5, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    .line 126
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.credentials.UNLOCK"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Lcom/android/keychain/KeyChainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 136
    :cond_1
    iget-object v5, p0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    invoke-virtual {v5}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/IntentSender;->getCreatorPackage()Ljava/lang/String;

    move-result-object v3

    .line 137
    .local v3, "pkgName":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "host"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 138
    .local v2, "host":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "port"

    const/4 v7, -0x1

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 139
    .local v4, "port":I
    invoke-static {p0}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/certificate/CertificatePolicy;

    move-result-object v5

    iget v6, p0, Lcom/android/keychain/KeyChainActivity;->mSenderUid:I

    invoke-static {v6}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v6

    invoke-virtual {v5, v3, v2, v4, v6}, Lcom/sec/enterprise/knox/certificate/CertificatePolicy;->isPrivateKeyApplicationPermittedAsUser(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    .line 143
    .local v0, "alias":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 145
    const-string v5, " [KNOX]"

    invoke-virtual {v0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 146
    const-string v5, " [KNOX]"

    const-string v6, ""

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 148
    :cond_2
    invoke-direct {p0, v0}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 153
    :cond_3
    invoke-direct {p0}, Lcom/android/keychain/KeyChainActivity;->showCertChooserDialog()V

    goto/16 :goto_0

    .line 161
    .end local v0    # "alias":Ljava/lang/String;
    .end local v2    # "host":Ljava/lang/String;
    .end local v3    # "pkgName":Ljava/lang/String;
    .end local v4    # "port":I
    :pswitch_2
    sget-object v5, Lcom/android/keychain/KeyChainActivity$State;->INITIAL:Lcom/android/keychain/KeyChainActivity$State;

    iput-object v5, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    .line 162
    invoke-direct {p0, v8}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 122
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 499
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 500
    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    sget-object v1, Lcom/android/keychain/KeyChainActivity$State;->INITIAL:Lcom/android/keychain/KeyChainActivity$State;

    if-eq v0, v1, :cond_0

    .line 501
    sget-object v0, Lcom/android/keychain/KeyChainActivity;->KEY_STATE:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 503
    :cond_0
    return-void
.end method
