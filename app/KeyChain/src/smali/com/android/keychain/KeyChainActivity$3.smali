.class Lcom/android/keychain/KeyChainActivity$3;
.super Ljava/lang/Object;
.source "KeyChainActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/keychain/KeyChainActivity;->displayCertChooserDialog(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keychain/KeyChainActivity;

.field final synthetic val$adapter:Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

.field final synthetic val$lv:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/android/keychain/KeyChainActivity;Landroid/widget/ListView;Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/android/keychain/KeyChainActivity$3;->this$0:Lcom/android/keychain/KeyChainActivity;

    iput-object p2, p0, Lcom/android/keychain/KeyChainActivity$3;->val$lv:Landroid/widget/ListView;

    iput-object p3, p0, Lcom/android/keychain/KeyChainActivity$3;->val$adapter:Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 264
    iget-object v3, p0, Lcom/android/keychain/KeyChainActivity$3;->val$lv:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v2

    .line 265
    .local v2, "listViewPosition":I
    add-int/lit8 v0, v2, -0x1

    .line 266
    .local v0, "adapterPosition":I
    if-ltz v0, :cond_1

    iget-object v3, p0, Lcom/android/keychain/KeyChainActivity$3;->val$adapter:Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    invoke-virtual {v3, v0}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v1

    .line 269
    .local v1, "alias":Ljava/lang/String;
    :goto_0
    if-eqz v1, :cond_0

    .line 270
    const-string v3, " [KNOX]"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 271
    const-string v3, " [KNOX]"

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 274
    :cond_0
    iget-object v3, p0, Lcom/android/keychain/KeyChainActivity$3;->this$0:Lcom/android/keychain/KeyChainActivity;

    # invokes: Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V
    invoke-static {v3, v1}, Lcom/android/keychain/KeyChainActivity;->access$700(Lcom/android/keychain/KeyChainActivity;Ljava/lang/String;)V

    .line 275
    return-void

    .line 266
    .end local v1    # "alias":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
