.class Lcom/android/keychain/KeyChainActivity$AliasLoader;
.super Landroid/os/AsyncTask;
.source "KeyChainActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/keychain/KeyChainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AliasLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/android/keychain/KeyChainActivity$CertificateAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/keychain/KeyChainActivity;


# direct methods
.method private constructor <init>(Lcom/android/keychain/KeyChainActivity;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/android/keychain/KeyChainActivity$AliasLoader;->this$0:Lcom/android/keychain/KeyChainActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/keychain/KeyChainActivity;Lcom/android/keychain/KeyChainActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/keychain/KeyChainActivity;
    .param p2, "x1"    # Lcom/android/keychain/KeyChainActivity$1;

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainActivity$AliasLoader;-><init>(Lcom/android/keychain/KeyChainActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/android/keychain/KeyChainActivity$CertificateAdapter;
    .locals 10
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 176
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v2, "aliasListFinal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 179
    .local v6, "timaAliasList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/android/keychain/KeyChainActivity$AliasLoader;->this$0:Lcom/android/keychain/KeyChainActivity;

    # getter for: Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;
    invoke-static {v7}, Lcom/android/keychain/KeyChainActivity;->access$100(Lcom/android/keychain/KeyChainActivity;)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/IntentSender;->getCreatorPackage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/tima_keychain/TimaKeychain;->isTimaKeystoreAndCCMEnabledForPackage(Ljava/lang/String;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 180
    iget-object v7, p0, Lcom/android/keychain/KeyChainActivity$AliasLoader;->this$0:Lcom/android/keychain/KeyChainActivity;

    # getter for: Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;
    invoke-static {v7}, Lcom/android/keychain/KeyChainActivity;->access$100(Lcom/android/keychain/KeyChainActivity;)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/IntentSender;->getCreatorPackage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/tima_keychain/TimaKeychain;->getAliasListFromTimaKeystore(Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 181
    if-nez v6, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 182
    :cond_0
    invoke-static {v6}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 183
    invoke-interface {v2, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 187
    :cond_1
    iget-object v7, p0, Lcom/android/keychain/KeyChainActivity$AliasLoader;->this$0:Lcom/android/keychain/KeyChainActivity;

    # getter for: Lcom/android/keychain/KeyChainActivity;->mKeyStore:Landroid/security/KeyStore;
    invoke-static {v7}, Lcom/android/keychain/KeyChainActivity;->access$200(Lcom/android/keychain/KeyChainActivity;)Landroid/security/KeyStore;

    move-result-object v7

    const-string v8, "USRPKEY_"

    invoke-virtual {v7, v8}, Landroid/security/KeyStore;->saw(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 188
    .local v1, "aliasArray":[Ljava/lang/String;
    if-nez v1, :cond_3

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 191
    .local v0, "aksAliasList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 192
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 195
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 196
    .local v4, "hashSetWithoutDuplicates":Ljava/util/HashSet;
    invoke-virtual {v4, v2}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 197
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 198
    invoke-interface {v2, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 200
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 201
    .local v3, "ccmAlias":Ljava/lang/String;
    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 202
    invoke-interface {v2, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 203
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/android/keychain/KeyChainActivity$AliasLoader;->this$0:Lcom/android/keychain/KeyChainActivity;

    # getter for: Lcom/android/keychain/KeyChainActivity;->mSenderUid:I
    invoke-static {v8}, Lcom/android/keychain/KeyChainActivity;->access$300(Lcom/android/keychain/KeyChainActivity;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 204
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " [KNOX]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 188
    .end local v0    # "aksAliasList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "ccmAlias":Ljava/lang/String;
    .end local v4    # "hashSetWithoutDuplicates":Ljava/util/HashSet;
    .end local v5    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 208
    .restart local v0    # "aksAliasList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v4    # "hashSetWithoutDuplicates":Ljava/util/HashSet;
    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_4
    new-instance v7, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    iget-object v8, p0, Lcom/android/keychain/KeyChainActivity$AliasLoader;->this$0:Lcom/android/keychain/KeyChainActivity;

    const/4 v9, 0x0

    invoke-direct {v7, v8, v2, v9}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;-><init>(Lcom/android/keychain/KeyChainActivity;Ljava/util/List;Lcom/android/keychain/KeyChainActivity$1;)V

    return-object v7
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 173
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/keychain/KeyChainActivity$AliasLoader;->doInBackground([Ljava/lang/Void;)Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity$AliasLoader;->this$0:Lcom/android/keychain/KeyChainActivity;

    # invokes: Lcom/android/keychain/KeyChainActivity;->displayCertChooserDialog(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V
    invoke-static {v0, p1}, Lcom/android/keychain/KeyChainActivity;->access$500(Lcom/android/keychain/KeyChainActivity;Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V

    .line 212
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 173
    check-cast p1, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/android/keychain/KeyChainActivity$AliasLoader;->onPostExecute(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V

    return-void
.end method
