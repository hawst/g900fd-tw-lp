.class Lcom/blurb/checkout/MainShadowActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "MainShadowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/MainShadowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/MainShadowActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$1;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 178
    const-string v4, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 179
    const-string v4, "result_code"

    const/16 v5, -0x3e7

    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 184
    .local v3, "resultCode":I
    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 185
    const-string v4, "access_token"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    .local v0, "accessToken":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 193
    .local v2, "locale":Ljava/lang/String;
    new-instance v4, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;

    iget-object v5, p0, Lcom/blurb/checkout/MainShadowActivity$1;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    iget-object v6, p0, Lcom/blurb/checkout/MainShadowActivity$1;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-direct {v4, v5, v6, v0, v2}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;-><init>(Lcom/blurb/checkout/MainShadowActivity;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 202
    .end local v0    # "accessToken":Ljava/lang/String;
    .end local v2    # "locale":Ljava/lang/String;
    .end local v3    # "resultCode":I
    :goto_0
    return-void

    .line 195
    .restart local v3    # "resultCode":I
    :cond_0
    const-string v4, "error_message"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "errorMessage":Ljava/lang/String;
    iget-object v4, p0, Lcom/blurb/checkout/MainShadowActivity$1;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const/4 v5, 0x1

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->showError(Ljava/lang/String;Z)V
    invoke-static {v4, v1, v5}, Lcom/blurb/checkout/MainShadowActivity;->access$000(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;Z)V

    goto :goto_0

    .line 200
    .end local v1    # "errorMessage":Ljava/lang/String;
    .end local v3    # "resultCode":I
    :cond_1
    const-string v4, "BlurbShadowApp"

    const-string v5, "Unknown broadcast"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
