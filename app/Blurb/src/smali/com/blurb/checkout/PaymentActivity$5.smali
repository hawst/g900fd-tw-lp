.class Lcom/blurb/checkout/PaymentActivity$5;
.super Ljava/lang/Object;
.source "PaymentActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/PaymentActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/PaymentActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/PaymentActivity;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/blurb/checkout/PaymentActivity$5;->this$0:Lcom/blurb/checkout/PaymentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 310
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 311
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity$5;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/blurb/checkout/PaymentActivity;->access$300(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 313
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 306
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 302
    return-void
.end method
