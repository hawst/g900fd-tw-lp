.class public Lcom/blurb/checkout/BlurbAPI;
.super Ljava/lang/Object;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/BlurbAPI$1;,
        Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;,
        Lcom/blurb/checkout/BlurbAPI$ValidateAddressResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;,
        Lcom/blurb/checkout/BlurbAPI$CreateProjectResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;,
        Lcom/blurb/checkout/BlurbAPI$PlaceOrderResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;,
        Lcom/blurb/checkout/BlurbAPI$CreateBookResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$CreateBookResult;,
        Lcom/blurb/checkout/BlurbAPI$ImageUploadResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;,
        Lcom/blurb/checkout/BlurbAPI$BookPricingResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$BookPricingResult;,
        Lcom/blurb/checkout/BlurbAPI$CreateUserResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$CreateUserResult;,
        Lcom/blurb/checkout/BlurbAPI$Address;,
        Lcom/blurb/checkout/BlurbAPI$GenerateBBFResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;,
        Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResponseHandler;,
        Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;,
        Lcom/blurb/checkout/BlurbAPI$Checkout;,
        Lcom/blurb/checkout/BlurbAPI$BlurbResult;,
        Lcom/blurb/checkout/BlurbAPI$Price;
    }
.end annotation


# static fields
.field public static final API_KEY:Ljava/lang/String; = "3770a58f2974fa2757171d46c41763"

.field public static final BLURB_HOST:Ljava/lang/String; = "android-api.blurb.com"

.field public static final BLURB_HOST_URL:Ljava/lang/String; = "https://android-api.blurb.com"

.field private static final BLURB_PHOTOS_URL:Ljava/lang/String; = "http://android-images.blurb.com/api/v2/photos/"

.field public static final BLURB_SCHEME:Ljava/lang/String; = "https"

.field public static final BOOK_ID_URL:Ljava/lang/String; = "/api/v2/books/"

.field public static final CHECKOUT_API_URL:Ljava/lang/String; = "/api/v2/checkouts.xml"

.field public static final CREATE_BOOK_API_URL:Ljava/lang/String; = "/api/v2/books.xml"

.field public static final CREATE_PROJECT_URL:Ljava/lang/String; = "/api/v2/projects.xml"

.field public static final CREATE_USER_API_URL:Ljava/lang/String; = "/api/v2/users.xml"

.field private static final DEBUG:Z = false

.field public static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"

.field static final PARAM_CITY:Ljava/lang/String; = "city"

.field static final PARAM_COUNTRY:Ljava/lang/String; = "country_id"

.field static final PARAM_FIRST_NAME:Ljava/lang/String; = "first_name"

.field static final PARAM_LAST_NAME:Ljava/lang/String; = "last_name"

.field static final PARAM_PHONE:Ljava/lang/String; = "phone"

.field static final PARAM_PO_BOX_OR_APO:Ljava/lang/String; = "po_box_or_apo"

.field static final PARAM_STATE_ID:Ljava/lang/String; = "state_id"

.field static final PARAM_STATE_NAME:Ljava/lang/String; = "state_name"

.field static final PARAM_STREET1:Ljava/lang/String; = "street1"

.field static final PARAM_STREET2:Ljava/lang/String; = "street2"

.field static final PARAM_ZIP:Ljava/lang/String; = "zip"

.field public static final PLACE_ORDER_API_URL:Ljava/lang/String; = "/api/v2/orders.xml"

.field private static final SOURCE_NAME:Ljava/lang/String; = "episodes"

.field private static final SOURCE_VERSION:Ljava/lang/String; = "1.0"

.field public static final UPLOAD_IMG_API_URL:Ljava/lang/String; = "/api/v2/photos/"

.field public static final VALIDATE_ADDRESS_API_URL:Ljava/lang/String; = "/api/v2/addresses/validate.xml"

.field private static country3To2Map:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static errorMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static final ns:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    const/4 v0, 0x0

    sput-object v0, Lcom/blurb/checkout/BlurbAPI;->ns:Ljava/lang/String;

    .line 2074
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    .line 2077
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "and"

    const-string v2, "ad"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2078
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "are"

    const-string v2, "ae"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2079
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "afg"

    const-string v2, "af"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2080
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "atg"

    const-string v2, "ag"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2081
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "aia"

    const-string v2, "ai"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2082
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "alb"

    const-string v2, "al"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2083
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "arm"

    const-string v2, "am"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2084
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ago"

    const-string v2, "ao"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2085
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ata"

    const-string v2, "aq"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2086
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "arg"

    const-string v2, "ar"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2087
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "asm"

    const-string v2, "as"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2088
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "aut"

    const-string v2, "at"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2089
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "aus"

    const-string v2, "au"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2090
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "abw"

    const-string v2, "aw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2091
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "aze"

    const-string v2, "az"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2092
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bih"

    const-string v2, "ba"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2093
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "brb"

    const-string v2, "bb"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2094
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bgd"

    const-string v2, "bd"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2095
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bel"

    const-string v2, "be"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2096
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bfa"

    const-string v2, "bf"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2097
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bgr"

    const-string v2, "bg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2098
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bhr"

    const-string v2, "bh"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2099
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bdi"

    const-string v2, "bi"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2100
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ben"

    const-string v2, "bj"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2101
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bmu"

    const-string v2, "bm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "brn"

    const-string v2, "bn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2103
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bol"

    const-string v2, "bo"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2104
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bra"

    const-string v2, "br"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2105
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bhs"

    const-string v2, "bs"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2106
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "btn"

    const-string v2, "bt"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2107
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bvt"

    const-string v2, "bv"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2108
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "bwa"

    const-string v2, "bw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2109
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "blr"

    const-string v2, "by"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2110
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "blz"

    const-string v2, "bz"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2111
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "can"

    const-string v2, "ca"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2112
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cck"

    const-string v2, "cc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2113
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cod"

    const-string v2, "cd"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2114
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "caf"

    const-string v2, "cf"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2115
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cog"

    const-string v2, "cg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2116
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "che"

    const-string v2, "ch"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2117
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "civ"

    const-string v2, "ci"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2118
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cok"

    const-string v2, "ck"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2119
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "chl"

    const-string v2, "cl"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2120
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cmr"

    const-string v2, "cm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2121
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "chn"

    const-string v2, "cn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2122
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "col"

    const-string v2, "co"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2123
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cri"

    const-string v2, "cr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2124
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cub"

    const-string v2, "cu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2125
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cpv"

    const-string v2, "cv"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2126
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cxr"

    const-string v2, "cx"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2127
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cyp"

    const-string v2, "cy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2128
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cze"

    const-string v2, "cz"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2129
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "deu"

    const-string v2, "de"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2130
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "dji"

    const-string v2, "dj"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2131
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "dnk"

    const-string v2, "dk"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2132
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "dma"

    const-string v2, "dm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2133
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "dom"

    const-string v2, "do"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2134
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "dza"

    const-string v2, "dz"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2135
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ecu"

    const-string v2, "ec"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2136
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "est"

    const-string v2, "ee"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2137
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "egy"

    const-string v2, "eg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2138
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "esh"

    const-string v2, "eh"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2139
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "eri"

    const-string v2, "er"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2140
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "esp"

    const-string v2, "es"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2141
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "eth"

    const-string v2, "et"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2142
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "fin"

    const-string v2, "fi"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2143
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "fji"

    const-string v2, "fj"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2144
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "flk"

    const-string v2, "fk"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2145
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "fsm"

    const-string v2, "fm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2146
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "fro"

    const-string v2, "fo"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2147
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "fra"

    const-string v2, "fr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2148
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gab"

    const-string v2, "ga"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2149
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gbr"

    const-string v2, "gb"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2150
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "grd"

    const-string v2, "gd"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2151
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "geo"

    const-string v2, "ge"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2152
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "guf"

    const-string v2, "gf"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2153
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gha"

    const-string v2, "gh"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2154
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gib"

    const-string v2, "gi"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2155
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "grl"

    const-string v2, "gl"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2156
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gmb"

    const-string v2, "gm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2157
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gin"

    const-string v2, "gn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2158
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "glp"

    const-string v2, "gp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2159
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gnq"

    const-string v2, "gq"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2160
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "grc"

    const-string v2, "gr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2161
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "sgs"

    const-string v2, "gs"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2162
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gtm"

    const-string v2, "gt"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2163
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gum"

    const-string v2, "gu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2164
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "gnb"

    const-string v2, "gw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2165
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "guy"

    const-string v2, "gy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2166
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "hkg"

    const-string v2, "hk"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2167
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "hmd"

    const-string v2, "hm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2168
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "hnd"

    const-string v2, "hn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2169
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "hrv"

    const-string v2, "hr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2170
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "hti"

    const-string v2, "ht"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2171
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "hun"

    const-string v2, "hu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2172
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "idn"

    const-string v2, "id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2173
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "irl"

    const-string v2, "ie"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2174
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "isr"

    const-string v2, "il"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2175
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ind"

    const-string v2, "in"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2176
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "iot"

    const-string v2, "io"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2177
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "irq"

    const-string v2, "iq"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2178
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "irn"

    const-string v2, "ir"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2179
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "isl"

    const-string v2, "is"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2180
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ita"

    const-string v2, "it"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2181
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "jam"

    const-string v2, "jm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2182
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "jor"

    const-string v2, "jo"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2183
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "jpn"

    const-string v2, "jp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2184
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ken"

    const-string v2, "ke"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2185
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "kgz"

    const-string v2, "kg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2186
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "khm"

    const-string v2, "kh"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2187
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "kir"

    const-string v2, "ki"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2188
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "com"

    const-string v2, "km"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2189
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "kna"

    const-string v2, "kn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2190
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "prk"

    const-string v2, "kp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2191
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "kor"

    const-string v2, "kr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2192
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "kwt"

    const-string v2, "kw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2193
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "cym"

    const-string v2, "ky"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2194
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "kaz"

    const-string v2, "kz"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2195
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lao"

    const-string v2, "la"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2196
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lbn"

    const-string v2, "lb"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2197
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lca"

    const-string v2, "lc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2198
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lie"

    const-string v2, "li"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2199
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lka"

    const-string v2, "lk"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2200
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lbr"

    const-string v2, "lr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2201
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lso"

    const-string v2, "ls"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2202
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ltu"

    const-string v2, "lt"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2203
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lux"

    const-string v2, "lu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2204
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lva"

    const-string v2, "lv"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2205
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "lby"

    const-string v2, "ly"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2206
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mar"

    const-string v2, "ma"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mco"

    const-string v2, "mc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2208
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mda"

    const-string v2, "md"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2209
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mdg"

    const-string v2, "mg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2210
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mhl"

    const-string v2, "mh"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2211
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mkd"

    const-string v2, "mk"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2212
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mli"

    const-string v2, "ml"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2213
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mmr"

    const-string v2, "mm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2214
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mng"

    const-string v2, "mn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2215
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mac"

    const-string v2, "mo"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2216
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mnp"

    const-string v2, "mp"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2217
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mtq"

    const-string v2, "mq"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2218
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mrt"

    const-string v2, "mr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "msr"

    const-string v2, "ms"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2220
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mlt"

    const-string v2, "mt"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mus"

    const-string v2, "mu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2222
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mdv"

    const-string v2, "mv"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2223
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mwi"

    const-string v2, "mw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2224
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mex"

    const-string v2, "mx"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2225
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "mys"

    const-string v2, "my"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2226
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "moz"

    const-string v2, "mz"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2227
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "nam"

    const-string v2, "na"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2228
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ncl"

    const-string v2, "nc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2229
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ner"

    const-string v2, "ne"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2230
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "nfk"

    const-string v2, "nf"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2231
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "nga"

    const-string v2, "ng"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2232
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "nic"

    const-string v2, "ni"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2233
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "nld"

    const-string v2, "nl"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2234
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "nor"

    const-string v2, "no"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2235
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "npl"

    const-string v2, "np"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2236
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "nru"

    const-string v2, "nr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "niu"

    const-string v2, "nu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2238
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "nzl"

    const-string v2, "nz"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2239
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "omn"

    const-string v2, "om"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2240
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "pan"

    const-string v2, "pa"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2241
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "per"

    const-string v2, "pe"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2242
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "pyf"

    const-string v2, "pf"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2243
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "png"

    const-string v2, "pg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2244
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "phl"

    const-string v2, "ph"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2245
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "pak"

    const-string v2, "pk"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2246
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "pol"

    const-string v2, "pl"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2247
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "spm"

    const-string v2, "pm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2248
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "pcn"

    const-string v2, "pn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2249
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "pri"

    const-string v2, "pr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "prt"

    const-string v2, "pt"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2251
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "plw"

    const-string v2, "pw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2252
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "pry"

    const-string v2, "py"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2253
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "qat"

    const-string v2, "qa"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2254
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "reu"

    const-string v2, "re"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2255
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "rou"

    const-string v2, "ro"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "srb"

    const-string v2, "rs"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2257
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "rus"

    const-string v2, "ru"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2258
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "rwa"

    const-string v2, "rw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "sau"

    const-string v2, "sa"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2260
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "slb"

    const-string v2, "sb"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2261
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "syc"

    const-string v2, "sc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2262
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "sdn"

    const-string v2, "sd"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2263
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "swe"

    const-string v2, "se"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2264
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "sgp"

    const-string v2, "sg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2265
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "shn"

    const-string v2, "sh"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2266
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "svn"

    const-string v2, "si"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "sjm"

    const-string v2, "sj"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2268
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "svk"

    const-string v2, "sk"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2269
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "sle"

    const-string v2, "sl"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2270
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "smr"

    const-string v2, "sm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2271
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "sen"

    const-string v2, "sn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2272
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "som"

    const-string v2, "so"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2273
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "sur"

    const-string v2, "sr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2274
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "stp"

    const-string v2, "st"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2275
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "slv"

    const-string v2, "sv"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2276
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "syr"

    const-string v2, "sy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2277
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "swz"

    const-string v2, "sz"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2278
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tca"

    const-string v2, "tc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2279
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tcd"

    const-string v2, "td"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2280
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "atf"

    const-string v2, "tf"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2281
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tgo"

    const-string v2, "tg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2282
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tha"

    const-string v2, "th"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2283
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tjk"

    const-string v2, "tj"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2284
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tkl"

    const-string v2, "tk"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2285
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tkm"

    const-string v2, "tm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2286
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tun"

    const-string v2, "tn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2287
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ton"

    const-string v2, "to"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2288
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tur"

    const-string v2, "tr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2289
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tto"

    const-string v2, "tt"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2290
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tuv"

    const-string v2, "tv"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2291
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "twn"

    const-string v2, "tw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2292
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "tza"

    const-string v2, "tz"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2293
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ukr"

    const-string v2, "ua"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2294
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "uga"

    const-string v2, "ug"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2295
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "umi"

    const-string v2, "um"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2296
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "usa"

    const-string v2, "us"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2297
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ury"

    const-string v2, "uy"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2298
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "uzb"

    const-string v2, "uz"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2299
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "vat"

    const-string v2, "va"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2300
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "vct"

    const-string v2, "vc"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2301
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "ven"

    const-string v2, "ve"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2302
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "vgb"

    const-string v2, "vg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2303
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "vir"

    const-string v2, "vi"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2304
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "vnm"

    const-string v2, "vn"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2305
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "vut"

    const-string v2, "vu"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2306
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "wlf"

    const-string v2, "wf"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2307
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "wsm"

    const-string v2, "ws"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2308
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "yem"

    const-string v2, "ye"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2309
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "myt"

    const-string v2, "yt"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2310
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "zaf"

    const-string v2, "za"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2311
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "zmb"

    const-string v2, "zm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2312
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    const-string v1, "zwe"

    const-string v2, "zw"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2313
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    return-void
.end method

.method static synthetic access$1500(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;
    .locals 1
    .param p0, "x0"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-static {p0, p1}, Lcom/blurb/checkout/BlurbAPI;->readOrderPrice(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lorg/xmlpull/v1/XmlPullParser;)Lcom/blurb/checkout/BlurbAPI$Price;
    .locals 1
    .param p0, "x0"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 53
    invoke-static {p0}, Lcom/blurb/checkout/BlurbAPI;->readPrice(Lorg/xmlpull/v1/XmlPullParser;)Lcom/blurb/checkout/BlurbAPI$Price;

    move-result-object v0

    return-object v0
.end method

.method public static codeToMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "code"    # Ljava/lang/String;
    .param p2, "countryCode"    # Ljava/lang/String;

    .prologue
    .line 167
    invoke-static {}, Lcom/blurb/checkout/BlurbAPI;->initErrorMap()V

    .line 169
    const/4 v0, 0x0

    .line 171
    .local v0, "message":Ljava/lang/String;
    const-string v2, "models."

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 172
    const-string v2, "models."

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 174
    :cond_0
    const-string v2, "us"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "address.zip_code_is_not_valid"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 176
    const v1, 0x7f040063

    .line 177
    .local v1, "messageId":I
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 200
    .end local v1    # "messageId":I
    :cond_1
    :goto_0
    if-nez v0, :cond_2

    .line 201
    move-object v0, p1

    .line 203
    :cond_2
    const-string v2, "BlurbShadowApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 206
    return-object v0

    .line 178
    :cond_3
    const-string v2, "address.state_cannot_be_blank"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 181
    const-string v2, "au"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 182
    const v1, 0x7f04005f

    .line 193
    .restart local v1    # "messageId":I
    :goto_1
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 194
    goto :goto_0

    .line 183
    .end local v1    # "messageId":I
    :cond_4
    const-string v2, "ca"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 184
    const v1, 0x7f040060

    .restart local v1    # "messageId":I
    goto :goto_1

    .line 185
    .end local v1    # "messageId":I
    :cond_5
    const-string v2, "jp"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 186
    const v1, 0x7f040061

    .restart local v1    # "messageId":I
    goto :goto_1

    .line 187
    .end local v1    # "messageId":I
    :cond_6
    const-string v2, "kr"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 188
    const v1, 0x7f040062

    .restart local v1    # "messageId":I
    goto :goto_1

    .line 190
    .end local v1    # "messageId":I
    :cond_7
    const v1, 0x7f04005e

    .restart local v1    # "messageId":I
    goto :goto_1

    .line 195
    .end local v1    # "messageId":I
    :cond_8
    sget-object v2, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 196
    .local v1, "messageId":Ljava/lang/Integer;
    if-eqz v1, :cond_1

    .line 197
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static getBlurbVersions()Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;
    .locals 10

    .prologue
    .line 2389
    new-instance v6, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;

    invoke-direct {v6}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;-><init>()V

    .line 2390
    .local v6, "result":Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v2

    .line 2391
    .local v2, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v3, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v3}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 2393
    .local v3, "context":Lorg/apache/http/protocol/BasicHttpContext;
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 2397
    .local v1, "builder":Landroid/net/Uri$Builder;
    const-string v8, "https"

    invoke-virtual {v1, v8}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "android-api.blurb.com"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "api"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "v2"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "app_versions.json"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2409
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2410
    .local v7, "url":Ljava/lang/String;
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v7}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 2416
    .local v5, "request":Lorg/apache/http/client/methods/HttpGet;
    :try_start_0
    new-instance v8, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResponseHandler;

    invoke-direct {v8}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResponseHandler;-><init>()V

    invoke-virtual {v2, v5, v8, v3}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;

    move-object v6, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2434
    :goto_0
    return-object v6

    .line 2417
    :catch_0
    move-exception v4

    .line 2418
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->setHttpStatus(I)V

    .line 2419
    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 2420
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v4

    .line 2421
    .local v4, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v8, -0x1

    invoke-virtual {v6, v8}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->setHttpStatus(I)V

    .line 2422
    invoke-virtual {v4}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 2423
    .end local v4    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v4

    .line 2424
    .local v4, "e":Ljava/io/InterruptedIOException;
    const/4 v8, -0x3

    invoke-virtual {v6, v8}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->setHttpStatus(I)V

    .line 2425
    invoke-virtual {v4}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_0

    .line 2426
    .end local v4    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v4

    .line 2427
    .local v4, "e":Ljava/io/IOException;
    const/4 v8, -0x2

    invoke-virtual {v6, v8}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->setHttpStatus(I)V

    .line 2428
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static getBookPricing(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/blurb/checkout/BlurbAPI$BookPricingResult;
    .locals 11
    .param p0, "coverType"    # Ljava/lang/String;
    .param p1, "bookType"    # Ljava/lang/String;
    .param p2, "paperType"    # Ljava/lang/String;
    .param p3, "pages"    # I
    .param p4, "customLogoUpgrade"    # Z

    .prologue
    .line 1171
    new-instance v6, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    invoke-direct {v6}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;-><init>()V

    .line 1172
    .local v6, "result":Lcom/blurb/checkout/BlurbAPI$BookPricingResult;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v2

    .line 1173
    .local v2, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v3, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v3}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 1175
    .local v3, "context":Lorg/apache/http/protocol/BasicHttpContext;
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    .line 1176
    .local v1, "builder":Landroid/net/Uri$Builder;
    const-string v8, "https"

    invoke-virtual {v1, v8}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "android-api.blurb.com"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "api"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "v1_1"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "book_prices"

    invoke-virtual {v8, v9}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "api_key"

    const-string v10, "3770a58f2974fa2757171d46c41763"

    invoke-virtual {v8, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "cover_type"

    invoke-virtual {v8, v9, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "book_type"

    invoke-virtual {v8, v9, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "paper_type"

    invoke-virtual {v8, v9, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "pages"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const-string v9, "clu"

    invoke-static {p4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1188
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1189
    .local v7, "url":Ljava/lang/String;
    new-instance v5, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v5, v7}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 1195
    .local v5, "request":Lorg/apache/http/client/methods/HttpGet;
    :try_start_0
    new-instance v8, Lcom/blurb/checkout/BlurbAPI$BookPricingResponseHandler;

    invoke-direct {v8}, Lcom/blurb/checkout/BlurbAPI$BookPricingResponseHandler;-><init>()V

    invoke-virtual {v2, v5, v8, v3}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    move-object v6, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1229
    :goto_0
    return-object v6

    .line 1196
    :catch_0
    move-exception v4

    .line 1197
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpStatus(I)V

    .line 1198
    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 1199
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v4

    .line 1200
    .local v4, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v8, -0x1

    invoke-virtual {v6, v8}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpStatus(I)V

    .line 1201
    invoke-virtual {v4}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 1202
    .end local v4    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v4

    .line 1203
    .local v4, "e":Ljava/io/InterruptedIOException;
    const/4 v8, -0x3

    invoke-virtual {v6, v8}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpStatus(I)V

    .line 1204
    invoke-virtual {v4}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_0

    .line 1205
    .end local v4    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v4

    .line 1206
    .local v4, "e":Ljava/io/IOException;
    const/4 v8, -0x2

    invoke-virtual {v6, v8}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpStatus(I)V

    .line 1207
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private static initErrorMap()V
    .locals 7

    .prologue
    const v6, 0x7f04006c

    const v5, 0x7f040068

    const v4, 0x7f040074

    const v3, 0x7f04009e

    .line 78
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 161
    :goto_0
    return-void

    .line 80
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    .line 81
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "not_authenticated"

    const v2, 0x7f04007c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "invalid_sso_provider"

    const v2, 0x7f04007d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "error.arguments"

    const v2, 0x7f04007e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway_timeout"

    const v2, 0x7f04007f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "invalid_upstream_response"

    const v2, 0x7f040080

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "internal_server_error"

    const v2, 0x7f040081

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "service_unavailable"

    const v2, 0x7f040082

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.firstname_can_not_be_blank"

    const v2, 0x7f04004e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.lastname_can_not_be_blank"

    const v2, 0x7f04004f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.street1_can_not_be_blank"

    const v2, 0x7f040050

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.city_can_not_be_blank"

    const v2, 0x7f040051

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.first_name_too_long"

    const v2, 0x7f040052

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.last_name_too_long"

    const v2, 0x7f040053

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.street1_too_long"

    const v2, 0x7f040054

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.street2_too_long"

    const v2, 0x7f040055

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.city_too_long"

    const v2, 0x7f040056

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.street1_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.street2_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.city_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.zip_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.country_id_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.state_id_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.phone_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.po_box_or_apo_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.first_name_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.last_name_invalid_characters"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.phone_number_must_include_area_code"

    const v2, 0x7f040057

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.phone_cant_be_blank"

    const v2, 0x7f040058

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.phone_must_be_less"

    const v2, 0x7f040059

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.phone_format_invalid"

    const v2, 0x7f04005a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.zip_code_does_not_exist"

    const v2, 0x7f04005b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 113
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.zip_code_does_not_match_state"

    const v2, 0x7f04005c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.state_cannot_be_blank"

    const v2, 0x7f04005e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.invalid_postal_code"

    const v2, 0x7f040063

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.zip_code_does_not_match_province"

    const v2, 0x7f040064

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.province_cannot_be_blank"

    const v2, 0x7f040065

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.zip_code_is_not_valid"

    const v2, 0x7f04005d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "user.user_does_not_exist"

    const v2, 0x7f040066

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "book.book_does_not_exist"

    const v2, 0x7f040067

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "checkout.shipping_address_missing"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "checkout.shipping_address_can_not_be_blank"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "checkout.shipping_address_invalid"

    const v2, 0x7f040069

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "checkout.shipping_method_missing"

    const v2, 0x7f04006a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "checkout.product_missing"

    const v2, 0x7f04006b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "checkout.billing_address_missing"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "checkout.billing_address_can_not_be_blank"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_first_name_missing"

    const v2, 0x7f04006d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_first_name_format"

    const v2, 0x7f04006e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_last_name_missing"

    const v2, 0x7f04006f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_last_name_format"

    const v2, 0x7f040070

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_cv_number_missing"

    const v2, 0x7f040071

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_cv_number_format"

    const v2, 0x7f040072

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_number_missing"

    const v2, 0x7f040073

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_number_format"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_type_not_accepted"

    const v2, 0x7f040075

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_month_missing"

    const v2, 0x7f040076

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_month_format"

    const v2, 0x7f040077

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_year_missing"

    const v2, 0x7f040078

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_year_format"

    const v2, 0x7f040079

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_expired"

    const v2, 0x7f04007a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "address.country_can_not_be_blank"

    const v2, 0x7f04007b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "processor.invalid_credit_card_number"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "processor.not_authorized_to_use_credit_card"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "paymentech.unable_to_process"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "checkout.error_retrieve_checkout"

    const v2, 0x7f0400de

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway.unable_to_process"

    const v2, 0x7f0400df

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway.card_expired"

    const v2, 0x7f0400e0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway.security_code_declined_by_bank"

    const v2, 0x7f0400e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway.security_code_declined_by_amex"

    const v2, 0x7f0400e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway.insufficient_funds"

    const v2, 0x7f0400e3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway.address_doesnt_match_billing_address"

    const v2, 0x7f0400e4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway.zipcode_doesnt_match_billing_address"

    const v2, 0x7f0400e5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway.postal_code_doesnt_match_billing_address"

    const v2, 0x7f0400e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    sget-object v0, Lcom/blurb/checkout/BlurbAPI;->errorMap:Ljava/util/HashMap;

    const-string v1, "gateway.technical_issues"

    const v2, 0x7f0400e7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0
.end method

.method static postCreateBook(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$CreateBookResult;
    .locals 12
    .param p0, "episodeId"    # Ljava/lang/String;
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "bookType"    # Ljava/lang/String;
    .param p3, "coverType"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "numPages"    # Ljava/lang/String;

    .prologue
    .line 1467
    if-nez p5, :cond_0

    .line 1468
    const-string v9, "BlurbShadowApp"

    const-string v10, "numPages is null"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1471
    :cond_0
    new-instance v7, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;

    invoke-direct {v7}, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;-><init>()V

    .line 1472
    .local v7, "result":Lcom/blurb/checkout/BlurbAPI$CreateBookResult;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 1473
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v2, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 1474
    .local v2, "context":Lorg/apache/http/protocol/BasicHttpContext;
    const-string v8, "https://android-api.blurb.com/api/v2/books.xml"

    .line 1475
    .local v8, "url":Ljava/lang/String;
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    const-string v9, "https://android-api.blurb.com/api/v2/books.xml"

    invoke-direct {v6, v9}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1477
    .local v6, "request":Lorg/apache/http/client/methods/HttpPost;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1478
    .local v5, "param":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "api_key"

    const-string v11, "3770a58f2974fa2757171d46c41763"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1479
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "episode_id"

    invoke-direct {v9, v10, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1480
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "session_id"

    invoke-direct {v9, v10, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1481
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "book_type"

    invoke-direct {v9, v10, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1482
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "cover_type"

    invoke-direct {v9, v10, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1483
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "title"

    move-object/from16 v0, p4

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1484
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "pages"

    move-object/from16 v0, p5

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1486
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "device_id"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getDeviceId()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1487
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "device_model"

    sget-object v11, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1488
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "network_type"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getNetworkOperator()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1491
    :try_start_0
    new-instance v4, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v9, "UTF-8"

    invoke-direct {v4, v5, v9}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 1493
    .local v4, "entity":Lorg/apache/http/HttpEntity;
    invoke-virtual {v6, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1498
    new-instance v9, Lcom/blurb/checkout/BlurbAPI$CreateBookResponseHandler;

    invoke-direct {v9}, Lcom/blurb/checkout/BlurbAPI$CreateBookResponseHandler;-><init>()V

    invoke-virtual {v1, v6, v9, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;

    move-object v7, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1513
    .end local v4    # "entity":Lorg/apache/http/HttpEntity;
    :goto_0
    const-string v9, "BlurbShadowApp"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "books "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;->getHttpStatus()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1515
    return-object v7

    .line 1499
    :catch_0
    move-exception v3

    .line 1500
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;->setHttpStatus(I)V

    .line 1501
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 1502
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    .line 1503
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v9, -0x1

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;->setHttpStatus(I)V

    .line 1504
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 1505
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 1506
    .local v3, "e":Ljava/io/InterruptedIOException;
    const/4 v9, -0x3

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;->setHttpStatus(I)V

    .line 1507
    invoke-virtual {v3}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_0

    .line 1508
    .end local v3    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v3

    .line 1509
    .local v3, "e":Ljava/io/IOException;
    const/4 v9, -0x2

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;->setHttpStatus(I)V

    .line 1510
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static postCreateCheckout(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;
    .locals 12
    .param p0, "sessionId"    # Ljava/lang/String;
    .param p1, "addressId"    # Ljava/lang/String;
    .param p2, "firstName"    # Ljava/lang/String;
    .param p3, "lastName"    # Ljava/lang/String;
    .param p4, "street1"    # Ljava/lang/String;
    .param p5, "street2"    # Ljava/lang/String;
    .param p6, "poBoxOrApo"    # Ljava/lang/String;
    .param p7, "city"    # Ljava/lang/String;
    .param p8, "stateId"    # Ljava/lang/String;
    .param p9, "zip"    # Ljava/lang/String;
    .param p10, "countryCode"    # Ljava/lang/String;
    .param p11, "phone"    # Ljava/lang/String;
    .param p12, "bookId"    # Ljava/lang/String;
    .param p13, "quantity"    # I
    .param p14, "currencyId"    # Ljava/lang/String;
    .param p15, "couponCode"    # Ljava/lang/String;
    .param p16, "coverType"    # Ljava/lang/String;

    .prologue
    .line 476
    new-instance v7, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    invoke-direct {v7}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;-><init>()V

    .line 477
    .local v7, "result":Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;
    const-string v8, "https://android-api.blurb.com/api/v2/checkouts.xml"

    .line 478
    .local v8, "url":Ljava/lang/String;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 479
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    const-string v9, "https://android-api.blurb.com/api/v2/checkouts.xml"

    invoke-direct {v6, v9}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 480
    .local v6, "request":Lorg/apache/http/client/methods/HttpPost;
    new-instance v2, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 482
    .local v2, "context":Lorg/apache/http/protocol/BasicHttpContext;
    const-string p6, "false"

    .line 484
    const-string v9, "us"

    move-object/from16 v0, p10

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 485
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v9

    move-object/from16 v0, p8

    invoke-virtual {v0, v9}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p8

    .line 488
    :cond_0
    sget-object v9, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    move-object/from16 v0, p10

    invoke-interface {v9, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 489
    sget-object v9, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    move-object/from16 v0, p10

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p10

    .end local p10    # "countryCode":Ljava/lang/String;
    check-cast p10, Ljava/lang/String;

    .line 491
    .restart local p10    # "countryCode":Ljava/lang/String;
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 493
    .local v5, "param":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "api_key"

    const-string v11, "3770a58f2974fa2757171d46c41763"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 494
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_2

    .line 495
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[id]"

    invoke-direct {v9, v10, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 496
    :cond_2
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[first_name]"

    invoke-direct {v9, v10, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[last_name]"

    invoke-direct {v9, v10, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 498
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[street1]"

    move-object/from16 v0, p4

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 499
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[street2]"

    move-object/from16 v0, p5

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 500
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[city]"

    move-object/from16 v0, p7

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 501
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[zip]"

    move-object/from16 v0, p9

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 502
    const-string v9, "us"

    move-object/from16 v0, p10

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "ca"

    move-object/from16 v0, p10

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    const-string v9, "au"

    move-object/from16 v0, p10

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 506
    :cond_3
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[state_id]"

    move-object/from16 v0, p8

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 510
    :goto_0
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[country_id]"

    move-object/from16 v0, p10

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 511
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[phone]"

    move-object/from16 v0, p11

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 512
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[po_box_or_apo]"

    move-object/from16 v0, p6

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 513
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "book_id"

    move-object/from16 v0, p12

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 514
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "quantity"

    invoke-static/range {p13 .. p13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 515
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "currency_id"

    move-object/from16 v0, p14

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 516
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "session_id"

    invoke-direct {v9, v10, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 517
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "coupon_code"

    move-object/from16 v0, p15

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 518
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "cover_type"

    move-object/from16 v0, p16

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 519
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "device_id"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getDeviceId()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 520
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "device_model"

    sget-object v11, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 521
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "network_type"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getNetworkOperator()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 524
    :try_start_0
    new-instance v4, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v9, "UTF-8"

    invoke-direct {v4, v5, v9}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 526
    .local v4, "entity":Lorg/apache/http/HttpEntity;
    invoke-virtual {v6, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 533
    const-string v9, "Content-Type"

    const-string v10, "application/x-www-form-urlencoded"

    invoke-virtual {v6, v9, v10}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    new-instance v9, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResponseHandler;

    invoke-direct {v9}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResponseHandler;-><init>()V

    invoke-virtual {v1, v6, v9, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    move-object v7, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 550
    .end local v4    # "entity":Lorg/apache/http/HttpEntity;
    :goto_1
    const-string v9, "BlurbShadowApp"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "checkout "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p12

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->getHttpStatus()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    return-object v7

    .line 508
    :cond_4
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "shipping_address[state_name]"

    move-object/from16 v0, p8

    invoke-direct {v9, v10, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 536
    :catch_0
    move-exception v3

    .line 537
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpStatus(I)V

    .line 538
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 539
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    .line 540
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v9, -0x1

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpStatus(I)V

    .line 541
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_1

    .line 542
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 543
    .local v3, "e":Ljava/io/InterruptedIOException;
    const/4 v9, -0x3

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpStatus(I)V

    .line 544
    invoke-virtual {v3}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_1

    .line 545
    .end local v3    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v3

    .line 546
    .local v3, "e":Ljava/io/IOException;
    const/4 v9, -0x2

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpStatus(I)V

    .line 547
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method static postCreateProject(Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;
    .locals 12
    .param p0, "sessionId"    # Ljava/lang/String;
    .param p1, "bookId"    # Ljava/lang/String;

    .prologue
    .line 1836
    new-instance v7, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    invoke-direct {v7}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;-><init>()V

    .line 1837
    .local v7, "result":Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 1838
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v2, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 1839
    .local v2, "context":Lorg/apache/http/protocol/BasicHttpContext;
    const-string v8, "https://android-api.blurb.com/api/v2/projects.xml"

    .line 1840
    .local v8, "url":Ljava/lang/String;
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    const-string v9, "https://android-api.blurb.com/api/v2/projects.xml"

    invoke-direct {v6, v9}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1842
    .local v6, "request":Lorg/apache/http/client/methods/HttpPost;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1843
    .local v5, "param":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "api_key"

    const-string v11, "3770a58f2974fa2757171d46c41763"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1844
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "session_id"

    invoke-direct {v9, v10, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1845
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "source_name"

    const-string v11, "episodes"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1846
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "source_version"

    const-string v11, "1.0"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1847
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "book_id"

    invoke-direct {v9, v10, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1849
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "device_id"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getDeviceId()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1850
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "device_model"

    sget-object v11, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1851
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "network_type"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getNetworkOperator()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1854
    :try_start_0
    new-instance v4, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v9, "UTF-8"

    invoke-direct {v4, v5, v9}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 1856
    .local v4, "entity":Lorg/apache/http/HttpEntity;
    invoke-virtual {v6, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1861
    new-instance v9, Lcom/blurb/checkout/BlurbAPI$CreateProjectResponseHandler;

    invoke-direct {v9}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResponseHandler;-><init>()V

    invoke-virtual {v1, v6, v9, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    move-object v7, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1876
    .end local v4    # "entity":Lorg/apache/http/HttpEntity;
    :goto_0
    const-string v9, "BlurbShadowApp"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "projects "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->getHttpStatus()I

    move-result v11

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1878
    return-object v7

    .line 1862
    :catch_0
    move-exception v3

    .line 1863
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->setHttpStatus(I)V

    .line 1864
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 1865
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    .line 1866
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v9, -0x1

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->setHttpStatus(I)V

    .line 1867
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 1868
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 1869
    .local v3, "e":Ljava/io/InterruptedIOException;
    const/4 v9, -0x3

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->setHttpStatus(I)V

    .line 1870
    invoke-virtual {v3}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_0

    .line 1871
    .end local v3    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v3

    .line 1872
    .local v3, "e":Ljava/io/IOException;
    const/4 v9, -0x2

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->setHttpStatus(I)V

    .line 1873
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static postCreateUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$CreateUserResult;
    .locals 12
    .param p0, "userId"    # Ljava/lang/String;
    .param p1, "accessToken"    # Ljava/lang/String;
    .param p2, "locale"    # Ljava/lang/String;

    .prologue
    .line 982
    new-instance v7, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-direct {v7}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;-><init>()V

    .line 983
    .local v7, "result":Lcom/blurb/checkout/BlurbAPI$CreateUserResult;
    const-string v8, "https://android-api.blurb.com/api/v2/users.xml"

    .line 984
    .local v8, "url":Ljava/lang/String;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 985
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    const-string v9, "https://android-api.blurb.com/api/v2/users.xml"

    invoke-direct {v6, v9}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 986
    .local v6, "request":Lorg/apache/http/client/methods/HttpPost;
    new-instance v2, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 988
    .local v2, "context":Lorg/apache/http/protocol/BasicHttpContext;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 989
    .local v5, "param":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "api_key"

    const-string v11, "3770a58f2974fa2757171d46c41763"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 991
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "sso[provider]"

    const-string v11, "samsung"

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 992
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "sso[credentials][user_id]"

    invoke-direct {v9, v10, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 993
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "sso[credentials][access_token]"

    invoke-direct {v9, v10, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 994
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "locale"

    invoke-direct {v9, v10, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 996
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "device_id"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getDeviceId()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 997
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "device_model"

    sget-object v11, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 998
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "network_type"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getNetworkOperator()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1004
    :try_start_0
    new-instance v4, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v9, "UTF-8"

    invoke-direct {v4, v5, v9}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 1011
    .local v4, "entity":Lorg/apache/http/HttpEntity;
    invoke-virtual {v6, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1013
    new-instance v9, Lcom/blurb/checkout/BlurbAPI$CreateUserResponseHandler;

    invoke-direct {v9}, Lcom/blurb/checkout/BlurbAPI$CreateUserResponseHandler;-><init>()V

    invoke-virtual {v1, v6, v9, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    move-object v7, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1031
    .end local v4    # "entity":Lorg/apache/http/HttpEntity;
    :goto_0
    return-object v7

    .line 1014
    :catch_0
    move-exception v3

    .line 1015
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->setHttpStatus(I)V

    .line 1016
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 1017
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    .line 1018
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v9, -0x1

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->setHttpStatus(I)V

    .line 1019
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 1020
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 1021
    .local v3, "e":Ljava/io/InterruptedIOException;
    const/4 v9, -0x3

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->setHttpStatus(I)V

    .line 1022
    invoke-virtual {v3}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_0

    .line 1023
    .end local v3    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v3

    .line 1024
    .local v3, "e":Ljava/io/IOException;
    const/4 v9, -0x2

    invoke-virtual {v7, v9}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->setHttpStatus(I)V

    .line 1025
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public static postImageUpload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;
    .locals 11
    .param p0, "sessionId"    # Ljava/lang/String;
    .param p1, "projectId"    # Ljava/lang/String;
    .param p2, "imageUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/InterruptedIOException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v10, -0x64

    .line 1331
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1333
    .local v3, "image":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1334
    const-string v7, "BlurbShadowApp"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "postImageUpload image: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " does not exist"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1335
    new-instance v5, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;

    invoke-direct {v5, v10}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;-><init>(I)V

    .line 1366
    :goto_0
    return-object v5

    .line 1337
    :cond_0
    invoke-virtual {v3}, Ljava/io/File;->canRead()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1338
    const-string v7, "BlurbShadowApp"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "postImageUpload image: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " cannot be read"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1339
    new-instance v5, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;

    invoke-direct {v5, v10}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;-><init>(I)V

    goto :goto_0

    .line 1342
    :cond_1
    new-instance v5, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;

    invoke-direct {v5}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;-><init>()V

    .line 1343
    .local v5, "result":Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getUploadHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    .line 1344
    .local v0, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v1, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v1}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 1345
    .local v1, "context":Lorg/apache/http/protocol/BasicHttpContext;
    const-string v6, "http://android-images.blurb.com/api/v2/photos/"

    .line 1346
    .local v6, "url":Ljava/lang/String;
    new-instance v4, Lorg/apache/http/client/methods/HttpPost;

    const-string v7, "http://android-images.blurb.com/api/v2/photos/"

    invoke-direct {v4, v7}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1347
    .local v4, "request":Lorg/apache/http/client/methods/HttpPost;
    new-instance v2, Lorg/apache/http/entity/mime/MultipartEntity;

    sget-object v7, Lorg/apache/http/entity/mime/HttpMultipartMode;->BROWSER_COMPATIBLE:Lorg/apache/http/entity/mime/HttpMultipartMode;

    invoke-direct {v2, v7}, Lorg/apache/http/entity/mime/MultipartEntity;-><init>(Lorg/apache/http/entity/mime/HttpMultipartMode;)V

    .line 1349
    .local v2, "entity":Lorg/apache/http/entity/mime/MultipartEntity;
    const-string v7, "Blurb-API-Session-ID"

    invoke-virtual {v4, v7, p0}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1354
    const-string v7, "session_id"

    new-instance v8, Lorg/apache/http/entity/mime/content/StringBody;

    invoke-direct {v8, p0}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 1355
    const-string v7, "project_id"

    new-instance v8, Lorg/apache/http/entity/mime/content/StringBody;

    invoke-direct {v8, p1}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 1356
    const-string v7, "media-content-length"

    new-instance v8, Lorg/apache/http/entity/mime/content/StringBody;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/apache/http/entity/mime/content/StringBody;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 1357
    const-string v7, "file"

    new-instance v8, Lorg/apache/http/entity/mime/content/FileBody;

    const-string v9, "image/jpeg"

    invoke-direct {v8, v3, v9}, Lorg/apache/http/entity/mime/content/FileBody;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/entity/mime/MultipartEntity;->addPart(Ljava/lang/String;Lorg/apache/http/entity/mime/content/ContentBody;)V

    .line 1359
    invoke-virtual {v4, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1361
    new-instance v7, Lcom/blurb/checkout/BlurbAPI$ImageUploadResponseHandler;

    invoke-direct {v7}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResponseHandler;-><init>()V

    invoke-virtual {v0, v4, v7, v1}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "result":Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;
    check-cast v5, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;

    .line 1362
    .restart local v5    # "result":Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;
    iput-object p2, v5, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->imageUrl:Ljava/lang/String;

    .line 1364
    const-string v7, "BlurbShadowApp"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "photos "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v5}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method static postPlaceOrder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
    .locals 13
    .param p0, "sessionId"    # Ljava/lang/String;
    .param p1, "checkoutId"    # Ljava/lang/String;
    .param p2, "couponCode"    # Ljava/lang/String;
    .param p3, "firstName"    # Ljava/lang/String;
    .param p4, "lastName"    # Ljava/lang/String;
    .param p5, "street1"    # Ljava/lang/String;
    .param p6, "street2"    # Ljava/lang/String;
    .param p7, "city"    # Ljava/lang/String;
    .param p8, "zip"    # Ljava/lang/String;
    .param p9, "inStateId"    # Ljava/lang/String;
    .param p10, "countryCode"    # Ljava/lang/String;
    .param p11, "ccFirstName"    # Ljava/lang/String;
    .param p12, "ccLastName"    # Ljava/lang/String;
    .param p13, "ccNumber"    # Ljava/lang/String;
    .param p14, "cvv"    # Ljava/lang/String;
    .param p15, "ccExpirationMonth"    # Ljava/lang/String;
    .param p16, "ccExpirationYear"    # Ljava/lang/String;
    .param p17, "wantNewsletter"    # Z

    .prologue
    .line 1656
    new-instance v7, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    invoke-direct {v7}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;-><init>()V

    .line 1657
    .local v7, "result":Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 1658
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v2, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 1659
    .local v2, "context":Lorg/apache/http/protocol/BasicHttpContext;
    const-string v9, "https://android-api.blurb.com/api/v2/orders.xml"

    .line 1660
    .local v9, "url":Ljava/lang/String;
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    const-string v10, "https://android-api.blurb.com/api/v2/orders.xml"

    invoke-direct {v6, v10}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1661
    .local v6, "request":Lorg/apache/http/client/methods/HttpPost;
    move-object/from16 v8, p9

    .line 1663
    .local v8, "stateId":Ljava/lang/String;
    const-string v10, "us"

    move-object/from16 v0, p10

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1664
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    move-object/from16 v0, p9

    invoke-virtual {v0, v10}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 1671
    :cond_0
    sget-object v10, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    move-object/from16 v0, p10

    invoke-interface {v10, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1672
    sget-object v10, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    move-object/from16 v0, p10

    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p10

    .end local p10    # "countryCode":Ljava/lang/String;
    check-cast p10, Ljava/lang/String;

    .line 1674
    .restart local p10    # "countryCode":Ljava/lang/String;
    :cond_1
    if-nez p6, :cond_2

    .line 1675
    const-string p6, ""

    .line 1677
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1678
    .local v5, "param":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "api_key"

    const-string v12, "3770a58f2974fa2757171d46c41763"

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1679
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "credit_card[exp_month]"

    move-object/from16 v0, p15

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1680
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "credit_card[exp_year]"

    move-object/from16 v0, p16

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1681
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "credit_card[first_name]"

    move-object/from16 v0, p11

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1682
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "credit_card[last_name]"

    move-object/from16 v0, p12

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1683
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "credit_card[cv_number]"

    move-object/from16 v0, p14

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1684
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "credit_card[number]"

    move-object/from16 v0, p13

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1685
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "checkout_id"

    invoke-direct {v10, v11, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1686
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "billing_address[first_name]"

    move-object/from16 v0, p3

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "billing_address[last_name]"

    move-object/from16 v0, p4

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1688
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "billing_address[street1]"

    move-object/from16 v0, p5

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1689
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "billing_address[street2]"

    move-object/from16 v0, p6

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1690
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "billing_address[city]"

    move-object/from16 v0, p7

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1691
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "billing_address[zip]"

    move-object/from16 v0, p8

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1692
    const-string v10, "us"

    move-object/from16 v0, p10

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "ca"

    move-object/from16 v0, p10

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_3

    const-string v10, "au"

    move-object/from16 v0, p10

    invoke-virtual {v10, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1696
    :cond_3
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "billing_address[state_id]"

    invoke-direct {v10, v11, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1700
    :goto_0
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "billing_address[country_id]"

    move-object/from16 v0, p10

    invoke-direct {v10, v11, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1701
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "session_id"

    invoke-direct {v10, v11, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1702
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "coupon_code"

    invoke-direct {v10, v11, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1703
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "want_newsletter"

    invoke-static/range {p17 .. p17}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1705
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "device_id"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getDeviceId()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1706
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "device_model"

    sget-object v12, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1707
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "network_type"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getNetworkOperator()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1710
    :try_start_0
    new-instance v4, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v10, "UTF-8"

    invoke-direct {v4, v5, v10}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 1712
    .local v4, "entity":Lorg/apache/http/HttpEntity;
    invoke-virtual {v6, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1719
    new-instance v10, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResponseHandler;

    invoke-direct {v10}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResponseHandler;-><init>()V

    invoke-virtual {v1, v6, v10, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v10

    move-object v0, v10

    check-cast v0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    move-object v7, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1734
    .end local v4    # "entity":Lorg/apache/http/HttpEntity;
    :goto_1
    const-string v10, "BlurbShadowApp"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "order "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v7}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->getHttpStatus()I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1736
    return-object v7

    .line 1698
    :cond_4
    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string v11, "billing_address[state_name]"

    invoke-direct {v10, v11, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1720
    :catch_0
    move-exception v3

    .line 1721
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpStatus(I)V

    .line 1722
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 1723
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    .line 1724
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v10, -0x1

    invoke-virtual {v7, v10}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpStatus(I)V

    .line 1725
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_1

    .line 1726
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 1727
    .local v3, "e":Ljava/io/InterruptedIOException;
    const/4 v10, -0x3

    invoke-virtual {v7, v10}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpStatus(I)V

    .line 1728
    invoke-virtual {v3}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_1

    .line 1729
    .end local v3    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v3

    .line 1730
    .local v3, "e":Ljava/io/IOException;
    const/4 v10, -0x2

    invoke-virtual {v7, v10}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpStatus(I)V

    .line 1731
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method static postValidateAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;
    .locals 15
    .param p0, "firstName"    # Ljava/lang/String;
    .param p1, "lastName"    # Ljava/lang/String;
    .param p2, "street1"    # Ljava/lang/String;
    .param p3, "street2"    # Ljava/lang/String;
    .param p4, "city"    # Ljava/lang/String;
    .param p5, "zip"    # Ljava/lang/String;
    .param p6, "inStateId"    # Ljava/lang/String;
    .param p7, "countryCode"    # Ljava/lang/String;
    .param p8, "phone"    # Ljava/lang/String;
    .param p9, "poBoxOrApo"    # Ljava/lang/String;

    .prologue
    .line 1991
    new-instance v9, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;

    invoke-direct {v9}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;-><init>()V

    .line 1992
    .local v9, "result":Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 1993
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v2, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 1994
    .local v2, "context":Lorg/apache/http/protocol/BasicHttpContext;
    const-string v11, "https://android-api.blurb.com/api/v2/addresses/validate.xml"

    .line 1995
    .local v11, "url":Ljava/lang/String;
    new-instance v8, Lorg/apache/http/client/methods/HttpPost;

    const-string v12, "https://android-api.blurb.com/api/v2/addresses/validate.xml"

    invoke-direct {v8, v12}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1996
    .local v8, "request":Lorg/apache/http/client/methods/HttpPost;
    move-object/from16 v10, p6

    .line 1998
    .local v10, "stateId":Ljava/lang/String;
    const-string v12, "us"

    move-object/from16 v0, p7

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1999
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v12

    move-object/from16 v0, p6

    invoke-virtual {v0, v12}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    .line 2006
    :cond_0
    sget-object v12, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    move-object/from16 v0, p7

    invoke-interface {v12, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 2007
    sget-object v12, Lcom/blurb/checkout/BlurbAPI;->country3To2Map:Ljava/util/Map;

    move-object/from16 v0, p7

    invoke-interface {v12, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p7

    .end local p7    # "countryCode":Ljava/lang/String;
    check-cast p7, Ljava/lang/String;

    .line 2009
    .restart local p7    # "countryCode":Ljava/lang/String;
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2011
    .local v7, "param":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "api_key"

    const-string v14, "3770a58f2974fa2757171d46c41763"

    invoke-direct {v12, v13, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2012
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "first_name"

    invoke-direct {v12, v13, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2013
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "last_name"

    move-object/from16 v0, p1

    invoke-direct {v12, v13, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2014
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "street1"

    move-object/from16 v0, p2

    invoke-direct {v12, v13, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2015
    if-eqz p3, :cond_2

    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_2

    .line 2016
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "street2"

    move-object/from16 v0, p3

    invoke-direct {v12, v13, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2017
    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_3

    .line 2018
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "city"

    move-object/from16 v0, p4

    invoke-direct {v12, v13, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2019
    :cond_3
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_4

    .line 2020
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "zip"

    move-object/from16 v0, p5

    invoke-direct {v12, v13, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2021
    :cond_4
    const-string v12, "us"

    move-object/from16 v0, p7

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_5

    const-string v12, "ca"

    move-object/from16 v0, p7

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_5

    const-string v12, "au"

    move-object/from16 v0, p7

    invoke-virtual {v12, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 2025
    :cond_5
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "state_id"

    invoke-direct {v12, v13, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2028
    :goto_0
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "country_id"

    move-object/from16 v0, p7

    invoke-direct {v12, v13, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2029
    if-eqz p8, :cond_6

    invoke-virtual/range {p8 .. p8}, Ljava/lang/String;->length()I

    move-result v12

    if-lez v12, :cond_6

    .line 2030
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "phone"

    move-object/from16 v0, p8

    invoke-direct {v12, v13, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2031
    :cond_6
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "po_box_or_apo"

    move-object/from16 v0, p9

    invoke-direct {v12, v13, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2033
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "device_id"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getDeviceId()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v13, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2034
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "device_model"

    sget-object v14, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v12, v13, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2035
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "network_type"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getNetworkOperator()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v13, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2040
    :try_start_0
    const-string v12, "UTF-8"

    invoke-static {v7, v12}, Lorg/apache/http/client/utils/URLEncodedUtils;->format(Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2041
    .local v6, "entityValue":Ljava/lang/String;
    const-string v12, "\\+"

    const-string v13, "%20"

    invoke-virtual {v6, v12, v13}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2042
    new-instance v5, Lorg/apache/http/entity/StringEntity;

    const-string v12, "UTF-8"

    invoke-direct {v5, v6, v12}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2043
    .local v5, "entity":Lorg/apache/http/entity/StringEntity;
    const-string v12, "application/x-www-form-urlencoded"

    invoke-virtual {v5, v12}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V

    .line 2045
    invoke-static {v5}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v3

    .line 2049
    .local v3, "debugStr":Ljava/lang/String;
    invoke-virtual {v8, v5}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 2054
    new-instance v12, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResponseHandler;

    invoke-direct {v12}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResponseHandler;-><init>()V

    invoke-virtual {v1, v8, v12, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v12

    move-object v0, v12

    check-cast v0, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;

    move-object v9, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 2069
    .end local v3    # "debugStr":Ljava/lang/String;
    .end local v5    # "entity":Lorg/apache/http/entity/StringEntity;
    .end local v6    # "entityValue":Ljava/lang/String;
    :goto_1
    const-string v12, "BlurbShadowApp"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "validate "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v9}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->getHttpStatus()I

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2071
    return-object v9

    .line 2027
    :cond_7
    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string v13, "state_name"

    invoke-direct {v12, v13, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2055
    :catch_0
    move-exception v4

    .line 2056
    .local v4, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->setHttpStatus(I)V

    .line 2057
    invoke-virtual {v4}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 2058
    .end local v4    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v4

    .line 2059
    .local v4, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v12, -0x1

    invoke-virtual {v9, v12}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->setHttpStatus(I)V

    .line 2060
    invoke-virtual {v4}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_1

    .line 2061
    .end local v4    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v4

    .line 2062
    .local v4, "e":Ljava/io/InterruptedIOException;
    const/4 v12, -0x3

    invoke-virtual {v9, v12}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->setHttpStatus(I)V

    .line 2063
    invoke-virtual {v4}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_1

    .line 2064
    .end local v4    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v4

    .line 2065
    .local v4, "e":Ljava/io/IOException;
    const/4 v12, -0x2

    invoke-virtual {v9, v12}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->setHttpStatus(I)V

    .line 2066
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static putGenerateBBF(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;
    .locals 16
    .param p0, "bookId"    # Ljava/lang/String;
    .param p1, "sessionId"    # Ljava/lang/String;
    .param p2, "projectId"    # Ljava/lang/String;
    .param p3, "frontCoverId"    # Ljava/lang/String;
    .param p4, "backCoverId"    # Ljava/lang/String;
    .param p5, "spineId"    # Ljava/lang/String;
    .param p7, "coverType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;"
        }
    .end annotation

    .prologue
    .line 665
    .local p6, "photoIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v11, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;

    invoke-direct {v11}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;-><init>()V

    .line 666
    .local v11, "result":Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "https://android-api.blurb.com/api/v2/books/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".xml"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 667
    .local v12, "url":Ljava/lang/String;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getUploadHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 668
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v10, Lorg/apache/http/client/methods/HttpPut;

    invoke-direct {v10, v12}, Lorg/apache/http/client/methods/HttpPut;-><init>(Ljava/lang/String;)V

    .line 669
    .local v10, "request":Lorg/apache/http/client/methods/HttpPut;
    new-instance v2, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 671
    .local v2, "context":Lorg/apache/http/protocol/BasicHttpContext;
    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    .line 672
    .local v7, "pages":Ljava/lang/StringBuffer;
    const/4 v5, 0x1

    .line 673
    .local v5, "first":Z
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    .line 674
    .local v9, "photoId":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 675
    const-string v13, ","

    invoke-virtual {v7, v13}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 676
    :cond_0
    const/4 v5, 0x0

    .line 677
    invoke-virtual {v7, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 680
    .end local v9    # "photoId":Ljava/lang/String;
    :cond_1
    if-nez p5, :cond_2

    .line 681
    const-string p5, ""

    .line 686
    :cond_2
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 688
    .local v8, "param":Ljava/util/List;, "Ljava/util/List<Lorg/apache/http/NameValuePair;>;"
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "api_key"

    const-string v15, "3770a58f2974fa2757171d46c41763"

    invoke-direct {v13, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 689
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "session_id"

    move-object/from16 v0, p1

    invoke-direct {v13, v14, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 690
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "project_id"

    move-object/from16 v0, p2

    invoke-direct {v13, v14, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 691
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "front_cover"

    move-object/from16 v0, p3

    invoke-direct {v13, v14, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 692
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "back_cover"

    move-object/from16 v0, p4

    invoke-direct {v13, v14, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 693
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "spine"

    move-object/from16 v0, p5

    invoke-direct {v13, v14, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 694
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "pages"

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v13, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 697
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "cover_type"

    move-object/from16 v0, p7

    invoke-direct {v13, v14, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 699
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "device_id"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getDeviceId()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v13, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 700
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "device_model"

    sget-object v15, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-direct {v13, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 701
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    const-string v14, "network_type"

    invoke-static {}, Lcom/blurb/checkout/WebService;->getNetworkOperator()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v13, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 704
    :try_start_0
    new-instance v4, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    const-string v13, "UTF-8"

    invoke-direct {v4, v8, v13}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 708
    .local v4, "entity":Lorg/apache/http/HttpEntity;
    invoke-virtual {v10, v4}, Lorg/apache/http/client/methods/HttpPut;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 713
    new-instance v13, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResponseHandler;

    invoke-direct {v13}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResponseHandler;-><init>()V

    invoke-virtual {v1, v10, v13, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v13

    move-object v0, v13

    check-cast v0, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;

    move-object v11, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 728
    .end local v4    # "entity":Lorg/apache/http/HttpEntity;
    :goto_1
    const-string v13, "BlurbShadowApp"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "books "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v11}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->getHttpStatus()I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 730
    return-object v11

    .line 714
    :catch_0
    move-exception v3

    .line 715
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v13, 0x0

    invoke-virtual {v11, v13}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->setHttpStatus(I)V

    .line 716
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1

    .line 717
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    .line 718
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v13, -0x1

    invoke-virtual {v11, v13}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->setHttpStatus(I)V

    .line 719
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_1

    .line 720
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 721
    .local v3, "e":Ljava/io/InterruptedIOException;
    const/4 v13, -0x3

    invoke-virtual {v11, v13}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->setHttpStatus(I)V

    .line 722
    invoke-virtual {v3}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_1

    .line 723
    .end local v3    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v3

    .line 724
    .local v3, "e":Ljava/io/IOException;
    const/4 v13, -0x2

    invoke-virtual {v11, v13}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->setHttpStatus(I)V

    .line 725
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private static readOrderPrice(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;
    .locals 3
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1047
    const/4 v1, 0x2

    sget-object v2, Lcom/blurb/checkout/BlurbAPI;->ns:Ljava/lang/String;

    invoke-interface {p0, v1, v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 1049
    new-instance v0, Lcom/blurb/checkout/BlurbAPI$Price;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbAPI$Price;-><init>()V

    .line 1051
    .local v0, "price":Lcom/blurb/checkout/BlurbAPI$Price;
    const/4 v1, 0x0

    const-string v2, "currency_id"

    invoke-interface {p0, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Price;->currency:Ljava/lang/String;

    .line 1052
    invoke-static {p0}, Lcom/blurb/checkout/WebService;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Price;->value:Ljava/lang/String;

    .line 1054
    const/4 v1, 0x3

    sget-object v2, Lcom/blurb/checkout/BlurbAPI;->ns:Ljava/lang/String;

    invoke-interface {p0, v1, v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 1056
    return-object v0
.end method

.method private static readPrice(Lorg/xmlpull/v1/XmlPullParser;)Lcom/blurb/checkout/BlurbAPI$Price;
    .locals 5
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1035
    const/4 v1, 0x2

    sget-object v2, Lcom/blurb/checkout/BlurbAPI;->ns:Ljava/lang/String;

    const-string v3, "price"

    invoke-interface {p0, v1, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 1037
    new-instance v0, Lcom/blurb/checkout/BlurbAPI$Price;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbAPI$Price;-><init>()V

    .line 1039
    .local v0, "price":Lcom/blurb/checkout/BlurbAPI$Price;
    const-string v1, "currency"

    invoke-interface {p0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Price;->currency:Ljava/lang/String;

    .line 1040
    const-string v1, "symbol"

    invoke-interface {p0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Price;->symbol:Ljava/lang/String;

    .line 1041
    const-string v1, "value"

    invoke-interface {p0, v4, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Price;->value:Ljava/lang/String;

    .line 1043
    return-object v0
.end method
