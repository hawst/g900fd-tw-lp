.class Lcom/blurb/checkout/BlurbAPI$PlaceOrderResponseHandler;
.super Ljava/lang/Object;
.source "BlurbAPI.java"

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlaceOrderResponseHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1625
    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
    .locals 1
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 1628
    new-instance v0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;-><init>()V

    .line 1630
    .local v0, "result":Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
    invoke-virtual {v0, p1}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpResponse(Lorg/apache/http/HttpResponse;)V

    .line 1632
    return-object v0
.end method

.method public bridge synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1622
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    move-result-object v0

    return-object v0
.end method
