.class public Lcom/blurb/checkout/SamsungAPI;
.super Ljava/lang/Object;
.source "SamsungAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/SamsungAPI$1;,
        Lcom/blurb/checkout/SamsungAPI$GetUserInfoResponseHandler;,
        Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;,
        Lcom/blurb/checkout/SamsungAPI$ValidateTokenResponseHandler;,
        Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    return-void
.end method

.method static getUserInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;
    .locals 10
    .param p0, "accessToken"    # Ljava/lang/String;
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 287
    new-instance v5, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-direct {v5}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;-><init>()V

    .line 288
    .local v5, "result":Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://api.samsungosp.com/v2/profile/user/user/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 289
    .local v6, "url":Ljava/lang/String;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 290
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4, v6}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 291
    .local v4, "request":Lorg/apache/http/client/methods/HttpGet;
    new-instance v2, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 293
    .local v2, "context":Lorg/apache/http/protocol/BasicHttpContext;
    const-string v7, "authorization"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Bearer "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    const-string v7, "x-osp-appId"

    const-string v8, "2m3m68snwb"

    invoke-virtual {v4, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const-string v7, "x-osp-userId"

    invoke-virtual {v4, v7, p1}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    :try_start_0
    new-instance v7, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResponseHandler;

    invoke-direct {v7}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResponseHandler;-><init>()V

    invoke-virtual {v1, v4, v7, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    move-object v5, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 319
    :goto_0
    return-object v5

    .line 302
    :catch_0
    move-exception v3

    .line 303
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V

    .line 304
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 305
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    .line 306
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v7, -0x1

    invoke-virtual {v5, v7}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V

    .line 307
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 308
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 309
    .local v3, "e":Ljava/io/InterruptedIOException;
    const/4 v7, -0x3

    invoke-virtual {v5, v7}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V

    .line 310
    invoke-virtual {v3}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_0

    .line 311
    .end local v3    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v3

    .line 312
    .local v3, "e":Ljava/io/IOException;
    const/4 v7, -0x2

    invoke-virtual {v5, v7}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V

    .line 313
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method static getValidateToken(Ljava/lang/String;)Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;
    .locals 9
    .param p0, "accessToken"    # Ljava/lang/String;

    .prologue
    .line 141
    new-instance v5, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    invoke-direct {v5}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;-><init>()V

    .line 142
    .local v5, "result":Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "https://api.samsungosp.com/v2/license/security/authorizeToken?authToken="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 143
    .local v6, "url":Ljava/lang/String;
    invoke-static {}, Lcom/blurb/checkout/WebService;->getHttpClient()Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    .line 144
    .local v1, "client":Landroid/net/http/AndroidHttpClient;
    new-instance v4, Lorg/apache/http/client/methods/HttpGet;

    invoke-direct {v4, v6}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 145
    .local v4, "request":Lorg/apache/http/client/methods/HttpGet;
    new-instance v2, Lorg/apache/http/protocol/BasicHttpContext;

    invoke-direct {v2}, Lorg/apache/http/protocol/BasicHttpContext;-><init>()V

    .line 147
    .local v2, "context":Lorg/apache/http/protocol/BasicHttpContext;
    const-string v7, "authorization"

    const-string v8, "Basic Mm0zbTY4c253Yjo1MDA2Nzc0RERFQkFBNjM0QTI0NDI0Mjg0QjYxQTExNA=="

    invoke-virtual {v4, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v7, "x-osp-appId"

    const-string v8, "2m3m68snwb"

    invoke-virtual {v4, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :try_start_0
    new-instance v7, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResponseHandler;

    invoke-direct {v7}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResponseHandler;-><init>()V

    invoke-virtual {v1, v4, v7, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/client/ResponseHandler;Lorg/apache/http/protocol/HttpContext;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    move-object v5, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 172
    :goto_0
    return-object v5

    .line 155
    :catch_0
    move-exception v3

    .line 156
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V

    .line 157
    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    .line 158
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v3

    .line 159
    .local v3, "e":Lorg/apache/http/client/ClientProtocolException;
    const/4 v7, -0x1

    invoke-virtual {v5, v7}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V

    .line 160
    invoke-virtual {v3}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    goto :goto_0

    .line 161
    .end local v3    # "e":Lorg/apache/http/client/ClientProtocolException;
    :catch_2
    move-exception v3

    .line 162
    .local v3, "e":Ljava/io/InterruptedIOException;
    const/4 v7, -0x3

    invoke-virtual {v5, v7}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V

    .line 163
    invoke-virtual {v3}, Ljava/io/InterruptedIOException;->printStackTrace()V

    goto :goto_0

    .line 164
    .end local v3    # "e":Ljava/io/InterruptedIOException;
    :catch_3
    move-exception v3

    .line 165
    .local v3, "e":Ljava/io/IOException;
    const/4 v7, -0x2

    invoke-virtual {v5, v7}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V

    .line 166
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
