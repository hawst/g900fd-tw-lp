.class public Lcom/blurb/checkout/PaymentActivity;
.super Landroid/app/Activity;
.source "PaymentActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;,
        Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;
    }
.end annotation


# static fields
.field private static final CURRENCY_JPY:Ljava/lang/String; = "JPY"

.field private static final CURRENCY_KRW:Ljava/lang/String; = "KRW"

.field private static final DEBUG:Z = false

.field private static final DIALOG_IMAGES_RECEIVED:I = 0x3

.field private static final DIALOG_NO_IMAGES:I = 0x1

.field private static final DIALOG_RENDER_FAILED:I = 0x2

.field public static final EXTRA_CREDIT_CARD_TYPE:Ljava/lang/String; = "creditCardType"

.field public static final EXTRA_LAST_FOUR_CC:Ljava/lang/String; = "last_four_cc"

.field public static final EXTRA_SUB_TOTAL:Ljava/lang/String; = "subTotal"

.field private static final FIELD_CARD:I = 0x8

.field private static final FIELD_CITY:I = 0x4

.field private static final FIELD_COUNTRY:I = 0x7

.field private static final FIELD_CVV:I = 0x9

.field private static final FIELD_FIRST_NAME:I = 0x1

.field private static final FIELD_LAST_NAME:I = 0x2

.field private static final FIELD_MONTH:I = 0xa

.field private static final FIELD_STATE_ID:I = 0x6

.field private static final FIELD_STREET1:I = 0x3

.field private static final FIELD_YEAR:I = 0xb

.field private static final FIELD_ZIP:I = 0x5

.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"

.field private static final PLACE_ORDER_RETRY_MS:I = 0x1d4c0

.field private static final expirationPattern:Ljava/util/regex/Pattern;


# instance fields
.field private billingCountryCode:Ljava/lang/String;

.field private billing_city:Ljava/lang/String;

.field private billing_first_name:Ljava/lang/String;

.field private billing_last_name:Ljava/lang/String;

.field private billing_state_id:Ljava/lang/String;

.field private billing_street_address:Ljava/lang/String;

.field private billing_street_address2:Ljava/lang/String;

.field private billing_zip:Ljava/lang/String;

.field private bookId:Ljava/lang/String;

.field private bookType:Ljava/lang/String;

.field private cardCvvEditText:Landroid/widget/EditText;

.field private cardExpirationMonthEditText:Landroid/widget/EditText;

.field private cardExpirationYearEditText:Landroid/widget/EditText;

.field private cardNumEditText:Landroid/widget/EditText;

.field private checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

.field private cityEditText:Landroid/widget/EditText;

.field private countriesListView:Landroid/widget/ListView;

.field private countryDialog:Landroid/app/Dialog;

.field private countryEditText:Landroid/widget/TextView;

.field private coupon_code:Ljava/lang/String;

.field private coverData:[B

.field private coverType:Ljava/lang/String;

.field private dialogShowing:I

.field private discountLine:Landroid/widget/RelativeLayout;

.field private displayDiscount:Landroid/widget/TextView;

.field private displayShipping:Landroid/widget/TextView;

.field private displaySubTotal:Landroid/widget/TextView;

.field private displayTax:Landroid/widget/TextView;

.field private displayTitle:Landroid/widget/TextView;

.field private displayTotal:Landroid/widget/TextView;

.field private email:Ljava/lang/String;

.field private episodeId:Ljava/lang/String;

.field private errorToFieldMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private familyNameEditText:Landroid/widget/EditText;

.field private firstNameEditText:Landroid/widget/EditText;

.field private last_four_cc:Ljava/lang/String;

.field private myImagesCompleteReceiver:Landroid/content/BroadcastReceiver;

.field private nextScreenButton:Landroid/view/View;

.field private num_Pages:I

.field private parameterToFieldMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field placeOrderResult:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

.field private quantity:Ljava/lang/String;

.field private sameAddressSwitch:Landroid/widget/Switch;

.field private sessionId:Ljava/lang/String;

.field private shippingCity:Ljava/lang/String;

.field private shippingCountryCode:Ljava/lang/String;

.field private shippingFirstName:Ljava/lang/String;

.field private shippingLastName:Ljava/lang/String;

.field private shippingPhone:Ljava/lang/String;

.field private shippingState:Ljava/lang/String;

.field private shippingStreetAddress:Ljava/lang/String;

.field private shippingStreetAddress2:Ljava/lang/String;

.field private shippingZip:Ljava/lang/String;

.field private stateEditText:Landroid/widget/EditText;

.field private streetAddressEditText:Landroid/widget/EditText;

.field private title:Ljava/lang/String;

.field private triedToOrder:Z

.field private zipCodeEditText:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 83
    const-string v0, "\\d{2}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/blurb/checkout/PaymentActivity;->expirationPattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/16 v6, 0x9

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0x8

    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 132
    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_first_name:Ljava/lang/String;

    .line 133
    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_last_name:Ljava/lang/String;

    .line 134
    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_street_address:Ljava/lang/String;

    .line 135
    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_street_address2:Ljava/lang/String;

    .line 136
    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_city:Ljava/lang/String;

    .line 137
    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_state_id:Ljava/lang/String;

    .line 138
    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_zip:Ljava/lang/String;

    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/blurb/checkout/PaymentActivity;->triedToOrder:Z

    .line 152
    const/4 v0, 0x0

    iput v0, p0, Lcom/blurb/checkout/PaymentActivity;->dialogShowing:I

    .line 154
    new-instance v0, Lcom/blurb/checkout/PaymentActivity$1;

    invoke-direct {v0, p0}, Lcom/blurb/checkout/PaymentActivity$1;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->myImagesCompleteReceiver:Landroid/content/BroadcastReceiver;

    .line 1073
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    .line 1076
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "country_id"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1077
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "state_id"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1078
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "state_name"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1079
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "zip"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1080
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "city"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1081
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "street1"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1082
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "first_name"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1083
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "last_name"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1084
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "first_name_format"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1085
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "first_name"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1086
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "last_name_format"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1087
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    const-string v1, "last_name"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1090
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    .line 1093
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "gateway.card_expired"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1094
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "gateway.security_code_declined_by_bank"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1095
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "gateway.security_code_declined_by_amex"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1096
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "gateway.insufficient_funds"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1097
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "gateway.address_doesnt_match_billing_address"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1098
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "gateway.zipcode_doesnt_match_billing_address"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1099
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "gateway.postal_code_doesnt_match_billing_address"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1100
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_first_name_missing"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1101
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_first_name_format"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1102
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_last_name_missing"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1103
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_last_name_format"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1104
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_cv_number_missing"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1105
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_cv_number_format"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1106
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_number_missing"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1107
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_number_format"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_type_not_accepted"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1109
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_month_missing"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1110
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_month_format"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1111
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_year_missing"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1112
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_year_format"

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1113
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    const-string v1, "credit_card.card_expired"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1121
    return-void
.end method

.method static synthetic access$000(Lcom/blurb/checkout/PaymentActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/blurb/checkout/PaymentActivity;->triedToOrder:Z

    return v0
.end method

.method static synthetic access$100(Lcom/blurb/checkout/PaymentActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget v0, p0, Lcom/blurb/checkout/PaymentActivity;->dialogShowing:I

    return v0
.end method

.method static synthetic access$1000(Lcom/blurb/checkout/PaymentActivity;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->countryDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/blurb/checkout/PaymentActivity;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/blurb/checkout/PaymentActivity;->countryDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$102(Lcom/blurb/checkout/PaymentActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;
    .param p1, "x1"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/blurb/checkout/PaymentActivity;->dialogShowing:I

    return p1
.end method

.method static synthetic access$1200(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->sessionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->coupon_code:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_first_name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_last_name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_street_address:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/blurb/checkout/PaymentActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/blurb/checkout/PaymentActivity;->updateBillingAddressFields(Z)V

    return-void
.end method

.method static synthetic access$2000(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_street_address2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_city:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_zip:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billing_state_id:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->bookId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/blurb/checkout/PaymentActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/blurb/checkout/PaymentActivity;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/blurb/checkout/PaymentActivity;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/blurb/checkout/PaymentActivity;->handleValidationError(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->episodeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->shippingFirstName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->shippingLastName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress2:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->shippingState:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->shippingZip:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->shippingPhone:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->email:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->last_four_cc:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/blurb/checkout/PaymentActivity;)[B
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->coverData:[B

    return-object v0
.end method

.method static synthetic access$4200(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->bookType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->coverType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/blurb/checkout/PaymentActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget v0, p0, Lcom/blurb/checkout/PaymentActivity;->num_Pages:I

    return v0
.end method

.method static synthetic access$4500(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->quantity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->countriesListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->countryEditText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/blurb/checkout/PaymentActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/blurb/checkout/PaymentActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/PaymentActivity;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/blurb/checkout/PaymentActivity;->updateCountryFields()V

    return-void
.end method

.method private static checkCreditCardWithLuhn(Ljava/lang/String;)Z
    .locals 7
    .param p0, "number"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 997
    const/4 v3, 0x0

    .line 998
    .local v3, "sum":I
    const/4 v0, 0x0

    .line 999
    .local v0, "alternate":Z
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 1000
    add-int/lit8 v6, v1, 0x1

    invoke-virtual {p0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 1001
    .local v2, "n":I
    if-eqz v0, :cond_0

    .line 1002
    mul-int/lit8 v2, v2, 0x2

    .line 1003
    const/16 v6, 0x9

    if-le v2, v6, :cond_0

    .line 1004
    rem-int/lit8 v6, v2, 0xa

    add-int/lit8 v2, v6, 0x1

    .line 1007
    :cond_0
    add-int/2addr v3, v2

    .line 1008
    if-nez v0, :cond_1

    move v0, v4

    .line 999
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    move v0, v5

    .line 1008
    goto :goto_1

    .line 1010
    .end local v2    # "n":I
    :cond_2
    rem-int/lit8 v6, v3, 0xa

    if-nez v6, :cond_3

    :goto_2
    return v4

    :cond_3
    move v4, v5

    goto :goto_2
.end method

.method private clearAddressValidationErrors()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 738
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 739
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 740
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->streetAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 741
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 742
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 743
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 744
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->countryEditText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 745
    return-void
.end method

.method private clearValidationFields()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 731
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 732
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 733
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 734
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 735
    return-void
.end method

.method public static getLocalizedDiscountPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "unitCents"    # Ljava/lang/String;
    .param p1, "count"    # I
    .param p2, "currency"    # Ljava/lang/String;

    .prologue
    .line 469
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    mul-int v2, v3, p1

    .line 470
    .local v2, "priceCents":I
    neg-int v3, v2

    int-to-float v3, v3

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    float-to-double v0, v3

    .line 472
    .local v0, "price":D
    invoke-static {v0, v1, p2}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getLocalizedPrice(DLjava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "price"    # D
    .param p2, "currencyId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x2

    .line 476
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    .line 477
    .local v2, "locale":Ljava/util/Locale;
    invoke-static {v2}, Ljava/text/NumberFormat;->getCurrencyInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    .line 479
    .local v3, "numberFormat":Ljava/text/NumberFormat;
    invoke-static {p2}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    .line 480
    .local v0, "currency":Ljava/util/Currency;
    invoke-virtual {v3, v0}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    .line 482
    const-string v4, "JPY"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "KRW"

    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 484
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 485
    invoke-virtual {v3, v5}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 491
    :goto_0
    invoke-virtual {v3, p0, p1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    .line 493
    .local v1, "formatted":Ljava/lang/String;
    return-object v1

    .line 487
    .end local v1    # "formatted":Ljava/lang/String;
    :cond_1
    invoke-virtual {v3, v5}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 488
    invoke-virtual {v3, v5}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    goto :goto_0
.end method

.method public static getLocalizedPrice(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "value"    # Ljava/lang/String;
    .param p1, "currency"    # Ljava/lang/String;

    .prologue
    .line 450
    if-nez p0, :cond_0

    .line 451
    const-string p0, "0.00"

    .line 452
    :cond_0
    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 454
    .local v0, "price":D
    invoke-static {v0, v1, p1}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static getLocalizedPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "unitCents"    # Ljava/lang/String;
    .param p1, "count"    # I
    .param p2, "currency"    # Ljava/lang/String;

    .prologue
    .line 460
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    mul-int v2, v3, p1

    .line 461
    .local v2, "priceCents":I
    int-to-float v3, v2

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    float-to-double v0, v3

    .line 463
    .local v0, "price":D
    invoke-static {v0, v1, p2}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private handleValidationError(ILjava/lang/String;)V
    .locals 5
    .param p1, "field"    # I
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    .line 748
    iget-object v2, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    invoke-static {p0, p2, v2}, Lcom/blurb/checkout/BlurbAPI;->codeToMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 749
    .local v1, "message":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 750
    move-object v1, p2

    .line 754
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 789
    const/4 v0, 0x0

    .line 793
    .local v0, "editText":Landroid/widget/EditText;
    :goto_0
    if-eqz v0, :cond_1

    .line 794
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 795
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 797
    new-instance v2, Lcom/blurb/checkout/PaymentActivity$11;

    invoke-direct {v2, p0, v0}, Lcom/blurb/checkout/PaymentActivity$11;-><init>(Lcom/blurb/checkout/PaymentActivity;Landroid/widget/EditText;)V

    const-wide/16 v3, 0xc8

    invoke-virtual {v0, v2, v3, v4}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 805
    :cond_1
    return-void

    .line 756
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_0
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    .line 757
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 759
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_1
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->familyNameEditText:Landroid/widget/EditText;

    .line 760
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 762
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_2
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->streetAddressEditText:Landroid/widget/EditText;

    .line 763
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 765
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_3
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cityEditText:Landroid/widget/EditText;

    .line 766
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 768
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_4
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->stateEditText:Landroid/widget/EditText;

    .line 769
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 771
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_5
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->zipCodeEditText:Landroid/widget/EditText;

    .line 772
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 774
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_6
    const/4 v0, 0x0

    .line 775
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 777
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_7
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    .line 778
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 780
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_8
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    .line 781
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 783
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_9
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    .line 784
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 786
    .end local v0    # "editText":Landroid/widget/EditText;
    :pswitch_a
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    .line 787
    .restart local v0    # "editText":Landroid/widget/EditText;
    goto :goto_0

    .line 754
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private loadFromIntent(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x0

    .line 625
    const-string v4, "firstName"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingFirstName:Ljava/lang/String;

    .line 626
    const-string v4, "lastName"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingLastName:Ljava/lang/String;

    .line 627
    const-string v4, "streetAddress1"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress:Ljava/lang/String;

    .line 628
    const-string v4, "streetAddress2"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress2:Ljava/lang/String;

    .line 629
    const-string v4, "cityEditText"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCity:Ljava/lang/String;

    .line 630
    const-string v4, "stateEditText"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingState:Ljava/lang/String;

    .line 631
    const-string v4, "postalCode"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingZip:Ljava/lang/String;

    .line 632
    const-string v4, "country"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCountryCode:Ljava/lang/String;

    .line 633
    const-string v4, "phone"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingPhone:Ljava/lang/String;

    .line 635
    const-string v4, "billingAddress"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/blurb/checkout/BlurbAPI$Address;

    .line 637
    .local v1, "billingAddress":Lcom/blurb/checkout/BlurbAPI$Address;
    const/4 v3, 0x1

    .line 639
    .local v3, "sameAddress":Z
    if-eqz v1, :cond_d

    .line 640
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 641
    const/4 v3, 0x0

    .line 642
    :cond_0
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress2:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 643
    const/4 v3, 0x0

    .line 644
    :cond_1
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCity:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 645
    const/4 v3, 0x0

    .line 646
    :cond_2
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    if-eqz v4, :cond_3

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingState:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 647
    const/4 v3, 0x0

    .line 648
    :cond_3
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    if-eqz v4, :cond_4

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingZip:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 649
    const/4 v3, 0x0

    .line 650
    :cond_4
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    if-eqz v4, :cond_5

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_5

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCountryCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 651
    const/4 v3, 0x0

    .line 653
    :cond_5
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    if-eqz v4, :cond_c

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_c

    .line 654
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    .line 662
    :goto_0
    invoke-direct {p0}, Lcom/blurb/checkout/PaymentActivity;->updateCountryFields()V

    .line 664
    if-nez v3, :cond_f

    .line 665
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingFirstName:Ljava/lang/String;

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->billing_first_name:Ljava/lang/String;

    .line 666
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingLastName:Ljava/lang/String;

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->billing_last_name:Ljava/lang/String;

    .line 668
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->sameAddressSwitch:Landroid/widget/Switch;

    invoke-virtual {v4, v7}, Landroid/widget/Switch;->setChecked(Z)V

    .line 670
    const v4, 0x7f060021

    invoke-virtual {p0, v4}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 671
    .local v0, "address":Landroid/view/View;
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 673
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingFirstName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 674
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->familyNameEditText:Landroid/widget/EditText;

    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingLastName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 676
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    if-eqz v4, :cond_6

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_6

    .line 677
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->streetAddressEditText:Landroid/widget/EditText;

    iget-object v5, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 678
    :cond_6
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    if-eqz v4, :cond_7

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_7

    .line 679
    const v4, 0x7f060008

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 680
    .local v2, "et":Landroid/widget/EditText;
    if-eqz v2, :cond_7

    .line 681
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 683
    .end local v2    # "et":Landroid/widget/EditText;
    :cond_7
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    if-eqz v4, :cond_8

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_8

    .line 684
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->cityEditText:Landroid/widget/EditText;

    iget-object v5, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 685
    :cond_8
    const-string v4, "us"

    iget-object v5, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, "ca"

    iget-object v5, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    const-string v4, "au"

    iget-object v5, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 688
    :cond_9
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    if-eqz v4, :cond_a

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_a

    .line 689
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->stateEditText:Landroid/widget/EditText;

    iget-object v5, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 695
    :cond_a
    :goto_1
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    if-eqz v4, :cond_b

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_b

    .line 696
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->zipCodeEditText:Landroid/widget/EditText;

    iget-object v5, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 701
    .end local v0    # "address":Landroid/view/View;
    :cond_b
    :goto_2
    const-string v4, "sessionId"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->sessionId:Ljava/lang/String;

    .line 702
    const-string v4, "orderCheckout"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Lcom/blurb/checkout/BlurbAPI$Checkout;

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    .line 703
    const-string v4, "email"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->email:Ljava/lang/String;

    .line 705
    const-string v4, "episode_id"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->episodeId:Ljava/lang/String;

    .line 706
    const-string v4, "book_type"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->bookType:Ljava/lang/String;

    .line 707
    const-string v4, "cover_type"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->coverType:Ljava/lang/String;

    .line 708
    const-string v4, "title"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->title:Ljava/lang/String;

    .line 709
    const-string v4, "cover_data"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->coverData:[B

    .line 710
    const-string v4, "num_pages"

    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/blurb/checkout/PaymentActivity;->num_Pages:I

    .line 711
    const-string v4, "couponCode"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->coupon_code:Ljava/lang/String;

    .line 712
    const-string v4, "quantity"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->quantity:Ljava/lang/String;

    .line 714
    const-string v4, "bookId"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->bookId:Ljava/lang/String;

    .line 715
    return-void

    .line 656
    :cond_c
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCountryCode:Ljava/lang/String;

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 659
    :cond_d
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCountryCode:Ljava/lang/String;

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    goto/16 :goto_0

    .line 691
    .restart local v0    # "address":Landroid/view/View;
    :cond_e
    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateName:Ljava/lang/String;

    if-eqz v4, :cond_a

    iget-object v4, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_a

    .line 692
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->stateEditText:Landroid/widget/EditText;

    iget-object v5, v1, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 698
    .end local v0    # "address":Landroid/view/View;
    :cond_f
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCountryCode:Ljava/lang/String;

    iput-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    goto/16 :goto_2
.end method

.method private popupCountryView()V
    .locals 15

    .prologue
    const v14, 0x3f4ccccd    # 0.8f

    .line 808
    invoke-virtual {p0}, Lcom/blurb/checkout/PaymentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    .line 809
    .local v4, "fv":Landroid/view/View;
    if-eqz v4, :cond_0

    .line 810
    const-string v11, "input_method"

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v12

    const/4 v13, 0x2

    invoke-virtual {v11, v12, v13}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 814
    :cond_0
    new-instance v8, Landroid/util/DisplayMetrics;

    invoke-direct {v8}, Landroid/util/DisplayMetrics;-><init>()V

    .line 815
    .local v8, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/blurb/checkout/PaymentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v11

    invoke-interface {v11}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 817
    iget v11, v8, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v11, v11

    mul-float/2addr v11, v14

    float-to-int v10, v11

    .line 818
    .local v10, "width":I
    iget v11, v8, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v11, v11

    mul-float/2addr v11, v14

    float-to-int v5, v11

    .line 820
    .local v5, "height":I
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 821
    .local v6, "inflater":Landroid/view/LayoutInflater;
    const v11, 0x7f030009

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 822
    .local v3, "countriesView":Landroid/view/View;
    invoke-static {p0}, Lcom/blurb/checkout/MainShadowActivity;->getSortedCountries(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    .line 824
    .local v2, "countries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/blurb/checkout/MainShadowActivity$Country;>;"
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v11, 0x1090003

    invoke-direct {v0, p0, v11, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 827
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/blurb/checkout/MainShadowActivity$Country;>;"
    const v11, 0x7f060018

    invoke-virtual {v3, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ListView;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->countriesListView:Landroid/widget/ListView;

    .line 829
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->countriesListView:Landroid/widget/ListView;

    invoke-virtual {v11, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 831
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 832
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v11, 0x7f040001

    invoke-virtual {v1, v11}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v11

    invoke-virtual {v11, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 835
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->countryDialog:Landroid/app/Dialog;

    .line 837
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->countriesListView:Landroid/widget/ListView;

    new-instance v12, Lcom/blurb/checkout/PaymentActivity$12;

    invoke-direct {v12, p0}, Lcom/blurb/checkout/PaymentActivity$12;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v11, v12}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 853
    move v9, v10

    .line 855
    .local v9, "size":I
    if-le v10, v5, :cond_1

    .line 856
    move v9, v5

    .line 858
    :cond_1
    new-instance v7, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v7}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 859
    .local v7, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->countryDialog:Landroid/app/Dialog;

    invoke-virtual {v11}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v11

    invoke-virtual {v7, v11}, Landroid/view/WindowManager$LayoutParams;->copyFrom(Landroid/view/WindowManager$LayoutParams;)I

    .line 860
    iput v9, v7, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 861
    const/4 v11, -0x1

    iput v11, v7, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 863
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->countryDialog:Landroid/app/Dialog;

    invoke-virtual {v11}, Landroid/app/Dialog;->show()V

    .line 865
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->countryDialog:Landroid/app/Dialog;

    invoke-virtual {v11}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11, v9, v9}, Landroid/view/Window;->setLayout(II)V

    .line 867
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->countryDialog:Landroid/app/Dialog;

    invoke-virtual {v11, p0}, Landroid/app/Dialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 868
    return-void
.end method

.method private showHideDiscount()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 174
    const v3, 0x7f06002b

    invoke-virtual {p0, v3}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 176
    .local v1, "sep":Landroid/view/View;
    const/4 v0, 0x0

    .line 178
    .local v0, "hasDiscount":Z
    iget-object v3, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v3, v3, Lcom/blurb/checkout/BlurbAPI$Checkout;->discountCents:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 180
    :try_start_0
    iget-object v3, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v3, v3, Lcom/blurb/checkout/BlurbAPI$Checkout;->discountCents:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-lez v3, :cond_1

    const/4 v0, 0x1

    .line 185
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 186
    iget-object v3, p0, Lcom/blurb/checkout/PaymentActivity;->discountLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 187
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 192
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 180
    goto :goto_0

    .line 189
    :cond_2
    iget-object v2, p0, Lcom/blurb/checkout/PaymentActivity;->discountLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 190
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 181
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private showHideTax()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x0

    .line 195
    const v4, 0x7f060028

    invoke-virtual {p0, v4}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 196
    .local v2, "taxView":Landroid/view/View;
    const v4, 0x7f060027

    invoke-virtual {p0, v4}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 198
    .local v1, "sep":Landroid/view/View;
    const/4 v0, 0x0

    .line 200
    .local v0, "hasTax":Z
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v4, v4, Lcom/blurb/checkout/BlurbAPI$Checkout;->taxCents:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 202
    :try_start_0
    iget-object v4, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v4, v4, Lcom/blurb/checkout/BlurbAPI$Checkout;->taxCents:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-lez v4, :cond_1

    const/4 v0, 0x1

    .line 207
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 208
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 209
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 214
    :goto_1
    return-void

    :cond_1
    move v0, v3

    .line 202
    goto :goto_0

    .line 211
    :cond_2
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 212
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 203
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method private updateBillingAddressFields(Z)V
    .locals 4
    .param p1, "hideAddress"    # Z

    .prologue
    const v3, 0x7f06000a

    const v2, 0x7f060009

    .line 515
    if-eqz p1, :cond_0

    .line 516
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    const v1, 0x7f060035

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    .line 533
    :goto_0
    return-void

    .line 518
    :cond_0
    const-string v0, "us"

    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ca"

    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "au"

    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    goto :goto_0

    .line 523
    :cond_2
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    const-string v1, "jp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 524
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    goto :goto_0

    .line 525
    :cond_3
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    const-string v1, "kr"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 526
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    goto :goto_0

    .line 527
    :cond_4
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassHk:Ljava/util/Set;

    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 528
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    const v1, 0x7f060005

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    goto :goto_0

    .line 530
    :cond_5
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    goto :goto_0
.end method

.method private updateCountryFields()V
    .locals 21

    .prologue
    .line 536
    const/4 v11, 0x0

    .local v11, "first":Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "last":Ljava/lang/String;
    const/16 v16, 0x0

    .local v16, "street1":Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "city":Ljava/lang/String;
    const/4 v15, 0x0

    .line 538
    .local v15, "state":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 539
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    .line 540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->familyNameEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 541
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->streetAddressEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v16

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->cityEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 543
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->stateEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    .line 546
    :cond_0
    const v17, 0x7f060021

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    .line 547
    .local v10, "countryView":Landroid/view/ViewGroup;
    invoke-virtual {v10}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 551
    const-string v17, "us"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_1

    const-string v17, "ca"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_1

    const-string v17, "au"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 555
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/PaymentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v17

    const v18, 0x7f030003

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 564
    .local v5, "address":Landroid/view/View;
    :goto_0
    const v17, 0x7f060001

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 565
    .local v14, "shipping":Landroid/view/View;
    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/view/View;->setVisibility(I)V

    .line 566
    const v17, 0x7f060002

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 567
    .local v6, "billing":Landroid/view/View;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 569
    const v17, 0x7f060003

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    .line 570
    const v17, 0x7f060004

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/blurb/checkout/PaymentActivity;->familyNameEditText:Landroid/widget/EditText;

    .line 571
    const v17, 0x7f060007

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/blurb/checkout/PaymentActivity;->streetAddressEditText:Landroid/widget/EditText;

    .line 572
    const v17, 0x7f060005

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/blurb/checkout/PaymentActivity;->cityEditText:Landroid/widget/EditText;

    .line 573
    const v17, 0x7f060006

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/blurb/checkout/PaymentActivity;->stateEditText:Landroid/widget/EditText;

    .line 574
    const v17, 0x7f060009

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/blurb/checkout/PaymentActivity;->zipCodeEditText:Landroid/widget/EditText;

    .line 576
    const v17, 0x7f06000c

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/blurb/checkout/PaymentActivity;->countryEditText:Landroid/widget/TextView;

    .line 577
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->countryEditText:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 579
    const-string v17, "kr"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 580
    const v17, 0x7f06000d

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    .line 581
    .local v13, "phoneView":Landroid/view/View;
    const/16 v17, 0x8

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/view/View;->setVisibility(I)V

    .line 584
    .end local v13    # "phoneView":Landroid/view/View;
    :cond_2
    const v17, 0x7f06000a

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageButton;

    .line 585
    .local v9, "countryChooser":Landroid/widget/ImageButton;
    move-object/from16 v0, p0

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-static {v0, v1}, Lcom/blurb/checkout/MainShadowActivity;->getCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 588
    .local v8, "country":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->countryEditText:Landroid/widget/TextView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 590
    if-eqz v11, :cond_5

    .line 591
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 592
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->familyNameEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 593
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->streetAddressEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 594
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->cityEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 596
    const-string v17, "us"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_3

    const-string v17, "ca"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_3

    const-string v17, "au"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 600
    :cond_3
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v17

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-le v0, v1, :cond_4

    .line 601
    const-string v15, ""

    .line 603
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->stateEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 606
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v17, v0

    const v18, 0x7f060005

    const v19, 0x7f060006

    const v20, 0x7f060009

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move/from16 v2, v18

    move/from16 v3, v19

    move/from16 v4, v20

    invoke-static {v0, v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity;->updateCountryFields(Landroid/app/Activity;Ljava/lang/String;III)V

    .line 608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->zipCodeEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const v18, 0x7f060037

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setNextFocusRightId(I)V

    .line 610
    const-string v17, "us"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_6

    const-string v17, "ca"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_6

    const-string v17, "au"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_a

    .line 614
    :cond_6
    const v17, 0x7f06004e

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 622
    :goto_1
    return-void

    .line 556
    .end local v5    # "address":Landroid/view/View;
    .end local v6    # "billing":Landroid/view/View;
    .end local v8    # "country":Ljava/lang/String;
    .end local v9    # "countryChooser":Landroid/widget/ImageButton;
    .end local v14    # "shipping":Landroid/view/View;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "jp"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 557
    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/PaymentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v17

    const/high16 v18, 0x7f030000

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .restart local v5    # "address":Landroid/view/View;
    goto/16 :goto_0

    .line 558
    .end local v5    # "address":Landroid/view/View;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "kr"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 559
    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/PaymentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v17

    const v18, 0x7f030001

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .restart local v5    # "address":Landroid/view/View;
    goto/16 :goto_0

    .line 561
    .end local v5    # "address":Landroid/view/View;
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/PaymentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v17

    const v18, 0x7f030002

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .restart local v5    # "address":Landroid/view/View;
    goto/16 :goto_0

    .line 615
    .restart local v6    # "billing":Landroid/view/View;
    .restart local v8    # "country":Ljava/lang/String;
    .restart local v9    # "countryChooser":Landroid/widget/ImageButton;
    .restart local v14    # "shipping":Landroid/view/View;
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "jp"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_b

    .line 616
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const v18, 0x7f06004e

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    goto :goto_1

    .line 617
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "kr"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_c

    .line 618
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    move-object/from16 v17, v0

    const v18, 0x7f06004e

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    goto/16 :goto_1

    .line 620
    :cond_c
    const v17, 0x7f06004e

    move/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    goto/16 :goto_1
.end method

.method private validateFields()Z
    .locals 8

    .prologue
    const v7, 0x7f040074

    const/4 v5, 0x0

    .line 1014
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1015
    .local v0, "cardNum":Ljava/lang/String;
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1016
    .local v2, "cvv":Ljava/lang/String;
    invoke-static {v0, v2}, Lcom/blurb/checkout/CreditCard;->checkCardType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 1018
    .local v1, "cardResult":I
    packed-switch v1, :pswitch_data_0

    .line 1040
    invoke-static {v0}, Lcom/blurb/checkout/PaymentActivity;->checkCreditCardWithLuhn(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1041
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {p0, v7}, Lcom/blurb/checkout/PaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1042
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    .line 1070
    :goto_0
    return v5

    .line 1020
    :pswitch_0
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    const v7, 0x7f040071

    invoke-virtual {p0, v7}, Lcom/blurb/checkout/PaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1021
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1025
    :pswitch_1
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    const v7, 0x7f040072

    invoke-virtual {p0, v7}, Lcom/blurb/checkout/PaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1026
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1029
    :pswitch_2
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {p0, v7}, Lcom/blurb/checkout/PaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1030
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1046
    :cond_0
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1047
    .local v3, "month":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_1

    .line 1048
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    const v7, 0x7f040076

    invoke-virtual {p0, v7}, Lcom/blurb/checkout/PaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1049
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1052
    :cond_1
    sget-object v6, Lcom/blurb/checkout/PaymentActivity;->expirationPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-nez v6, :cond_2

    .line 1053
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    const v7, 0x7f040077

    invoke-virtual {p0, v7}, Lcom/blurb/checkout/PaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1054
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1058
    :cond_2
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1059
    .local v4, "year":Ljava/lang/String;
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_3

    .line 1060
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    const v7, 0x7f040078

    invoke-virtual {p0, v7}, Lcom/blurb/checkout/PaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1061
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    .line 1064
    :cond_3
    sget-object v6, Lcom/blurb/checkout/PaymentActivity;->expirationPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v6, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v6

    if-nez v6, :cond_4

    .line 1065
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    const v7, 0x7f040079

    invoke-virtual {p0, v7}, Lcom/blurb/checkout/PaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1066
    iget-object v6, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    invoke-virtual {v6}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    .line 1070
    :cond_4
    const/4 v5, 0x1

    goto/16 :goto_0

    .line 1018
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    .line 927
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f06000a

    if-eq v5, v6, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f06000c

    if-ne v5, v6, :cond_2

    .line 928
    :cond_0
    invoke-direct {p0}, Lcom/blurb/checkout/PaymentActivity;->popupCountryView()V

    .line 980
    :cond_1
    :goto_0
    return-void

    .line 929
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v5

    const v6, 0x7f060044

    if-ne v5, v6, :cond_1

    .line 930
    invoke-direct {p0}, Lcom/blurb/checkout/PaymentActivity;->clearValidationFields()V

    .line 931
    invoke-direct {p0}, Lcom/blurb/checkout/PaymentActivity;->clearAddressValidationErrors()V

    .line 933
    invoke-direct {p0}, Lcom/blurb/checkout/PaymentActivity;->validateFields()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 936
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->episodeId:Ljava/lang/String;

    invoke-static {v5}, Lcom/blurb/checkout/ImagesReceiver;->getRenderedImages(Ljava/lang/String;)Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    move-result-object v1

    .line 938
    .local v1, "imagesForEpisode":Lcom/blurb/checkout/ImagesReceiver$RenderedImages;
    if-nez v1, :cond_3

    .line 939
    iput-boolean v7, p0, Lcom/blurb/checkout/PaymentActivity;->triedToOrder:Z

    .line 942
    invoke-virtual {p0, v7}, Lcom/blurb/checkout/PaymentActivity;->showDialog(I)V

    goto :goto_0

    .line 944
    :cond_3
    iget-boolean v5, v1, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->renderFailed:Z

    if-eqz v5, :cond_4

    .line 946
    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/PaymentActivity;->showDialog(I)V

    goto :goto_0

    .line 950
    :cond_4
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v2

    .line 951
    .local v2, "length":I
    add-int/lit8 v3, v2, -0x4

    .line 952
    .local v3, "start":I
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->last_four_cc:Ljava/lang/String;

    .line 955
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->sameAddressSwitch:Landroid/widget/Switch;

    invoke-virtual {v5}, Landroid/widget/Switch;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 957
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingFirstName:Ljava/lang/String;

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_first_name:Ljava/lang/String;

    .line 958
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingLastName:Ljava/lang/String;

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_last_name:Ljava/lang/String;

    .line 959
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress:Ljava/lang/String;

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_street_address:Ljava/lang/String;

    .line 960
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress2:Ljava/lang/String;

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_street_address2:Ljava/lang/String;

    .line 961
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCity:Ljava/lang/String;

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_city:Ljava/lang/String;

    .line 962
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingState:Ljava/lang/String;

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_state_id:Ljava/lang/String;

    .line 963
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingZip:Ljava/lang/String;

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_zip:Ljava/lang/String;

    .line 964
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->shippingCountryCode:Ljava/lang/String;

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    .line 977
    :goto_1
    new-instance v4, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;

    const/4 v5, 0x0

    invoke-direct {v4, p0, p0, v5}, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;-><init>(Lcom/blurb/checkout/PaymentActivity;Landroid/app/Activity;Lcom/blurb/checkout/PaymentActivity$1;)V

    .line 978
    .local v4, "task":Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;
    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 966
    .end local v4    # "task":Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;
    :cond_5
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_first_name:Ljava/lang/String;

    .line 967
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_last_name:Ljava/lang/String;

    .line 968
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->streetAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_street_address:Ljava/lang/String;

    .line 969
    const v5, 0x7f060008

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 970
    .local v0, "et":Landroid/widget/EditText;
    if-eqz v0, :cond_6

    .line 971
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_street_address2:Ljava/lang/String;

    .line 972
    :cond_6
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_city:Ljava/lang/String;

    .line 973
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_state_id:Ljava/lang/String;

    .line 974
    iget-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/PaymentActivity;->billing_zip:Ljava/lang/String;

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 218
    const/4 v11, 0x1

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->requestWindowFeature(I)Z

    .line 220
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 222
    const v11, 0x7f03000c

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->setContentView(I)V

    .line 224
    const v11, 0x7f06000f

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 225
    .local v9, "titleView":Landroid/widget/TextView;
    const v11, 0x7f04009f

    invoke-virtual {v9, v11}, Landroid/widget/TextView;->setText(I)V

    .line 227
    const v11, 0x7f06002c

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->discountLine:Landroid/widget/RelativeLayout;

    .line 229
    const v11, 0x7f060035

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/Switch;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->sameAddressSwitch:Landroid/widget/Switch;

    .line 230
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->sameAddressSwitch:Landroid/widget/Switch;

    const/4 v12, -0x1

    invoke-virtual {v11, v12}, Landroid/widget/Switch;->setTextColor(I)V

    .line 231
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->sameAddressSwitch:Landroid/widget/Switch;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/widget/Switch;->setChecked(Z)V

    .line 233
    const v11, 0x7f060044

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->nextScreenButton:Landroid/view/View;

    .line 234
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->nextScreenButton:Landroid/view/View;

    invoke-virtual {v11, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    const v11, 0x7f060037

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    .line 237
    const v11, 0x7f060038

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    .line 238
    const v11, 0x7f060039

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    .line 239
    iget-object v10, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    check-cast v10, Lcom/blurb/checkout/YearEditText;

    .line 240
    .local v10, "year":Lcom/blurb/checkout/YearEditText;
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    invoke-virtual {v10, v11}, Lcom/blurb/checkout/YearEditText;->setMonthEditText(Landroid/widget/EditText;)V

    .line 241
    const v11, 0x7f06003b

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    .line 243
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->sameAddressSwitch:Landroid/widget/Switch;

    new-instance v12, Lcom/blurb/checkout/PaymentActivity$2;

    invoke-direct {v12, p0}, Lcom/blurb/checkout/PaymentActivity$2;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v11, v12}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 258
    const v11, 0x7f060032

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 259
    .local v6, "paymentid":Landroid/view/View;
    new-instance v11, Lcom/blurb/checkout/PaymentActivity$3;

    invoke-direct {v11, p0}, Lcom/blurb/checkout/PaymentActivity$3;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v6, v11}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 286
    const v11, 0x7f060033

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ScrollView;

    .line 287
    .local v7, "paymentscrollview":Landroid/widget/ScrollView;
    new-instance v11, Lcom/blurb/checkout/PaymentActivity$4;

    invoke-direct {v11, p0}, Lcom/blurb/checkout/PaymentActivity$4;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v7, v11}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 299
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;

    new-instance v12, Lcom/blurb/checkout/PaymentActivity$5;

    invoke-direct {v12, p0}, Lcom/blurb/checkout/PaymentActivity$5;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 316
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;

    new-instance v12, Lcom/blurb/checkout/PaymentActivity$6;

    invoke-direct {v12, p0}, Lcom/blurb/checkout/PaymentActivity$6;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 333
    const v11, 0x7f06003b

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/EditText;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    .line 334
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;

    new-instance v12, Lcom/blurb/checkout/PaymentActivity$7;

    invoke-direct {v12, p0}, Lcom/blurb/checkout/PaymentActivity$7;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v11, v12}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 352
    const v11, 0x7f06003d

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayTitle:Landroid/widget/TextView;

    .line 354
    const v11, 0x7f06003e

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displaySubTotal:Landroid/widget/TextView;

    .line 355
    const v11, 0x7f06003f

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayTax:Landroid/widget/TextView;

    .line 356
    const v11, 0x7f060040

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayShipping:Landroid/widget/TextView;

    .line 357
    const v11, 0x7f060042

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayTotal:Landroid/widget/TextView;

    .line 358
    const v11, 0x7f060041

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayDiscount:Landroid/widget/TextView;

    .line 360
    invoke-virtual {p0}, Lcom/blurb/checkout/PaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 362
    .local v4, "intent":Landroid/content/Intent;
    invoke-direct {p0, v4}, Lcom/blurb/checkout/PaymentActivity;->loadFromIntent(Landroid/content/Intent;)V

    .line 364
    invoke-direct {p0}, Lcom/blurb/checkout/PaymentActivity;->showHideDiscount()V

    .line 365
    invoke-direct {p0}, Lcom/blurb/checkout/PaymentActivity;->showHideTax()V

    .line 368
    const v11, 0x7f060043

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 369
    .local v2, "confirmationEmail":Landroid/widget/TextView;
    const v11, 0x7f0400db

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/blurb/checkout/PaymentActivity;->email:Ljava/lang/String;

    aput-object v14, v12, v13

    invoke-virtual {p0, v11, v12}, Lcom/blurb/checkout/PaymentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 371
    .local v1, "confirmation":Ljava/lang/String;
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->email:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 372
    .local v3, "emailStart":I
    new-instance v8, Landroid/text/SpannableString;

    invoke-direct {v8, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 374
    .local v8, "span":Landroid/text/Spannable;
    new-instance v11, Landroid/text/style/BackgroundColorSpan;

    const/16 v12, -0x100

    invoke-direct {v11, v12}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    iget-object v12, p0, Lcom/blurb/checkout/PaymentActivity;->email:Ljava/lang/String;

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/2addr v12, v3

    const/16 v13, 0x21

    invoke-interface {v8, v11, v3, v12, v13}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 375
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayTitle:Landroid/widget/TextView;

    iget-object v12, p0, Lcom/blurb/checkout/PaymentActivity;->title:Ljava/lang/String;

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 381
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v11, v11, Lcom/blurb/checkout/BlurbAPI$Checkout;->productCents:Ljava/lang/String;

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v13, v13, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    invoke-static {v11, v12, v13}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 382
    .local v5, "localizedPrice":Ljava/lang/String;
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displaySubTotal:Landroid/widget/TextView;

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 383
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v11, v11, Lcom/blurb/checkout/BlurbAPI$Checkout;->taxCents:Ljava/lang/String;

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v13, v13, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    invoke-static {v11, v12, v13}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 384
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayTax:Landroid/widget/TextView;

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 386
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v11, v11, Lcom/blurb/checkout/BlurbAPI$Checkout;->shippingCents:Ljava/lang/String;

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v13, v13, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    invoke-static {v11, v12, v13}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 387
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayShipping:Landroid/widget/TextView;

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 388
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v11, v11, Lcom/blurb/checkout/BlurbAPI$Checkout;->orderCents:Ljava/lang/String;

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v13, v13, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    invoke-static {v11, v12, v13}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 389
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayTotal:Landroid/widget/TextView;

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 390
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v11, v11, Lcom/blurb/checkout/BlurbAPI$Checkout;->discountCents:Ljava/lang/String;

    const/4 v12, 0x1

    iget-object v13, p0, Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    iget-object v13, v13, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    invoke-static {v11, v12, v13}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedDiscountPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 391
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->displayDiscount:Landroid/widget/TextView;

    invoke-virtual {v11, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 399
    const/4 v11, 0x0

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->setResult(I)V

    .line 401
    const v11, 0x7f06003c

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 402
    .local v0, "cb":Landroid/widget/CheckBox;
    const-string v11, "us"

    iget-object v12, p0, Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    invoke-virtual {v0, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 404
    const v11, 0x7f060033

    invoke-virtual {p0, v11}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/blurb/checkout/ui/EnhancedScrollView;

    new-instance v12, Lcom/blurb/checkout/PaymentActivity$8;

    invoke-direct {v12, p0}, Lcom/blurb/checkout/PaymentActivity$8;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v11, v12}, Lcom/blurb/checkout/ui/EnhancedScrollView;->setOnScrollListener(Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;)V

    .line 425
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    invoke-virtual {v11}, Landroid/widget/EditText;->requestFocus()Z

    .line 426
    iget-object v11, p0, Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;

    new-instance v12, Lcom/blurb/checkout/PaymentActivity$9;

    invoke-direct {v12, p0}, Lcom/blurb/checkout/PaymentActivity$9;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    const-wide/16 v13, 0xc8

    invoke-virtual {v11, v12, v13, v14}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 433
    return-void
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "id"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f04004a

    .line 875
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 876
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v1, 0x0

    .line 878
    .local v1, "dialog":Landroid/app/Dialog;
    packed-switch p1, :pswitch_data_0

    .line 917
    :goto_0
    if-eqz v1, :cond_0

    .line 918
    iput p1, p0, Lcom/blurb/checkout/PaymentActivity;->dialogShowing:I

    .line 922
    .end local v1    # "dialog":Landroid/app/Dialog;
    :goto_1
    return-object v1

    .line 880
    .restart local v1    # "dialog":Landroid/app/Dialog;
    :pswitch_0
    const v2, 0x7f0400c0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/blurb/checkout/PaymentActivity$13;

    invoke-direct {v3, p0}, Lcom/blurb/checkout/PaymentActivity$13;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 888
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 889
    goto :goto_0

    .line 891
    :pswitch_1
    const v2, 0x7f04008b

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/blurb/checkout/PaymentActivity$14;

    invoke-direct {v3, p0}, Lcom/blurb/checkout/PaymentActivity$14;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 899
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 900
    goto :goto_0

    .line 902
    :pswitch_2
    const v2, 0x7f04008a

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f040089

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/blurb/checkout/PaymentActivity$15;

    invoke-direct {v3, p0}, Lcom/blurb/checkout/PaymentActivity$15;-><init>(Lcom/blurb/checkout/PaymentActivity;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 913
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 922
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    goto :goto_1

    .line 878
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->myImagesCompleteReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/blurb/checkout/PaymentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 439
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 440
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 444
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity;->myImagesCompleteReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.samsung.android.app.episodes.action.IMAGES_READY"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/blurb/checkout/PaymentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 446
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 447
    return-void
.end method

.method public showError(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "error_msg"    # Ljava/lang/String;
    .param p2, "fatal"    # Z

    .prologue
    .line 497
    if-eqz p1, :cond_1

    .line 498
    new-instance v0, Lcom/blurb/checkout/PaymentActivity$10;

    invoke-direct {v0, p0, p0, p1, p2}, Lcom/blurb/checkout/PaymentActivity$10;-><init>(Lcom/blurb/checkout/PaymentActivity;Landroid/content/Context;Ljava/lang/String;Z)V

    .line 508
    .local v0, "errorDialog":Lcom/blurb/checkout/ErrorDialog;
    if-nez p2, :cond_0

    .line 509
    const v1, 0x7f04004a

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/ErrorDialog;->setButtonText(I)V

    .line 510
    :cond_0
    invoke-virtual {v0}, Lcom/blurb/checkout/ErrorDialog;->show()V

    .line 512
    .end local v0    # "errorDialog":Lcom/blurb/checkout/ErrorDialog;
    :cond_1
    return-void
.end method
