.class Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;
.super Lcom/blurb/checkout/BlurbAPI$BlurbResult;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CreateCheckoutResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;
    }
.end annotation


# instance fields
.field public checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 388
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 299
    new-instance v0, Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbAPI$Checkout;-><init>()V

    iput-object v0, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    .line 389
    return-void
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "httpStatus"    # I

    .prologue
    .line 391
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 299
    new-instance v0, Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbAPI$Checkout;-><init>()V

    iput-object v0, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    .line 392
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpStatus(I)V

    .line 393
    return-void
.end method


# virtual methods
.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 7
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 396
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpStatus(I)V

    .line 401
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 402
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 403
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpStatus(I)V

    .line 441
    :goto_0
    return-void

    .line 407
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_1

    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0x190

    if-ne v5, v6, :cond_2

    .line 409
    :cond_1
    :try_start_0
    const-string v5, "UTF-8"

    invoke-static {v1, v5}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 410
    .local v4, "responseString":Ljava/lang/String;
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 413
    .local v3, "reader":Ljava/io/StringReader;
    new-instance v2, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;

    const/4 v5, 0x0

    invoke-direct {v2, p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;Lcom/blurb/checkout/BlurbAPI$1;)V

    .line 415
    .local v2, "parser":Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;
    # invokes: Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->parseXml(Ljava/io/Reader;)V
    invoke-static {v2, v3}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->access$200(Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;Ljava/io/Reader;)V

    .line 416
    invoke-virtual {v2}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->getErrors()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->errors:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    .line 437
    .end local v2    # "parser":Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;
    .end local v3    # "reader":Ljava/io/StringReader;
    .end local v4    # "responseString":Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 438
    :catch_0
    move-exception v0

    .line 439
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 417
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 418
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v5, -0x2

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpStatus(I)V

    .line 419
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 420
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 421
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 422
    const/4 v5, -0x4

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpStatus(I)V

    goto :goto_1
.end method
