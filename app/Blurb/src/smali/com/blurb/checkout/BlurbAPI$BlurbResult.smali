.class Lcom/blurb/checkout/BlurbAPI$BlurbResult;
.super Lcom/blurb/checkout/WebService$HttpResult;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BlurbResult"
.end annotation


# instance fields
.field protected errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/blurb/checkout/BlurbResponseParser$BlurbError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/blurb/checkout/WebService$HttpResult;-><init>()V

    return-void
.end method


# virtual methods
.method public getErrors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/blurb/checkout/BlurbResponseParser$BlurbError;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/blurb/checkout/BlurbAPI$BlurbResult;->errors:Ljava/util/List;

    return-object v0
.end method
