.class Lcom/blurb/checkout/PaymentActivity$7;
.super Ljava/lang/Object;
.source "PaymentActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/PaymentActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/PaymentActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/PaymentActivity;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/blurb/checkout/PaymentActivity$7;->this$0:Lcom/blurb/checkout/PaymentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 345
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 346
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$7;->this$0:Lcom/blurb/checkout/PaymentActivity;

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Lcom/blurb/checkout/PaymentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 347
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$7;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$400(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromInputMethod(Landroid/os/IBinder;I)V

    .line 349
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 341
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 337
    return-void
.end method
