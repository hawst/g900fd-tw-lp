.class Lcom/blurb/checkout/MainShadowActivity$4;
.super Ljava/lang/Object;
.source "MainShadowActivity.java"

# interfaces
.implements Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/MainShadowActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field focusedTextView:Landroid/widget/EditText;

.field final synthetic this$0:Lcom/blurb/checkout/MainShadowActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0

    .prologue
    .line 337
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$4;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEndScroll()V
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$4;->focusedTextView:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$4;->focusedTextView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$4;->focusedTextView:Landroid/widget/EditText;

    .line 356
    :cond_0
    return-void
.end method

.method public onStartScroll()V
    .locals 3

    .prologue
    .line 342
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$4;->focusedTextView:Landroid/widget/EditText;

    .line 343
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$4;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-virtual {v1}, Lcom/blurb/checkout/MainShadowActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 344
    .local v0, "focusedView":Landroid/view/View;
    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 345
    check-cast v0, Landroid/widget/EditText;

    .end local v0    # "focusedView":Landroid/view/View;
    iput-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$4;->focusedTextView:Landroid/widget/EditText;

    .line 346
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$4;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const v2, 0x7f060047

    invoke-virtual {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 348
    :cond_0
    return-void
.end method
