.class Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "BlurbManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/service/BlurbManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DatabaseHelper"
.end annotation


# static fields
.field private static final BOOK_COLUMN_BOOK_ID:Ljava/lang/String; = "bookId"

.field private static final BOOK_COLUMN_COVER_TYPE:Ljava/lang/String; = "coverType"

.field private static final BOOK_COLUMN_EPISODE_ID:Ljava/lang/String; = "episodeId"

.field private static final BOOK_COLUMN_ID:Ljava/lang/String; = "id"

.field private static final BOOK_COLUMN_PROJECT_ID:Ljava/lang/String; = "projectId"

.field private static final BOOK_COLUMN_SESSION_ID:Ljava/lang/String; = "sessionId"

.field private static final BOOK_COLUMN_START_DATE:Ljava/lang/String; = "startDate"

.field private static final BOOK_COLUMN_TITLE:Ljava/lang/String; = "title"

.field private static final BOOK_TABLE:Ljava/lang/String; = "bookTable"

.field private static final NAME:Ljava/lang/String; = "Blurb"

.field private static final ORDER_COLUMN_BOOK_ID:Ljava/lang/String; = "bookId"

.field private static final ORDER_COLUMN_BOOK_TYPE:Ljava/lang/String; = "bookType"

.field private static final ORDER_COLUMN_CITY:Ljava/lang/String; = "city"

.field private static final ORDER_COLUMN_COUNTRY_CODE:Ljava/lang/String; = "countryCode"

.field private static final ORDER_COLUMN_COUPON_CODE:Ljava/lang/String; = "couponCode"

.field private static final ORDER_COLUMN_COVER:Ljava/lang/String; = "cover"

.field private static final ORDER_COLUMN_COVER_TYPE:Ljava/lang/String; = "coverType"

.field private static final ORDER_COLUMN_CREDIT_CARD_TYPE:Ljava/lang/String; = "creditCardType"

.field private static final ORDER_COLUMN_DISCOUNT_CENTS:Ljava/lang/String; = "discountCents"

.field private static final ORDER_COLUMN_DISCOUNT_TOTAL:Ljava/lang/String; = "discountTotal"

.field private static final ORDER_COLUMN_EMAIL:Ljava/lang/String; = "email"

.field private static final ORDER_COLUMN_EPISODE_ID:Ljava/lang/String; = "episodeId"

.field private static final ORDER_COLUMN_FIRST_NAME:Ljava/lang/String; = "firstName"

.field private static final ORDER_COLUMN_ID:Ljava/lang/String; = "id"

.field private static final ORDER_COLUMN_LAST_FOUR_CC:Ljava/lang/String; = "lastFourCC"

.field private static final ORDER_COLUMN_LAST_NAME:Ljava/lang/String; = "lastName"

.field private static final ORDER_COLUMN_NUM_PAGES:Ljava/lang/String; = "numPages"

.field private static final ORDER_COLUMN_ORDER_ID:Ljava/lang/String; = "orderId"

.field private static final ORDER_COLUMN_ORDER_TOTAL:Ljava/lang/String; = "orderTotal"

.field private static final ORDER_COLUMN_PHONE:Ljava/lang/String; = "phone"

.field private static final ORDER_COLUMN_POSTAL_CODE:Ljava/lang/String; = "postalCode"

.field private static final ORDER_COLUMN_QUANTITY:Ljava/lang/String; = "quantity"

.field private static final ORDER_COLUMN_SHIPPING_TOTAL:Ljava/lang/String; = "shippingTotal"

.field private static final ORDER_COLUMN_STATE:Ljava/lang/String; = "state"

.field private static final ORDER_COLUMN_STREET_ADDRESS1:Ljava/lang/String; = "address1"

.field private static final ORDER_COLUMN_STREET_ADDRESS2:Ljava/lang/String; = "address2"

.field private static final ORDER_COLUMN_SUB_TOTAL:Ljava/lang/String; = "subTotal"

.field private static final ORDER_COLUMN_TAX_CENTS:Ljava/lang/String; = "taxCents"

.field private static final ORDER_COLUMN_TAX_TOTAL:Ljava/lang/String; = "taxTotal"

.field private static final ORDER_COLUMN_TITLE:Ljava/lang/String; = "title"

.field private static final ORDER_TABLE:Ljava/lang/String; = "orderTable"

.field private static final PHOTO_COLUMN_BLURB_ID:Ljava/lang/String; = "blurbId"

.field private static final PHOTO_COLUMN_BOOK:Ljava/lang/String; = "bookPK"

.field private static final PHOTO_COLUMN_ID:Ljava/lang/String; = "id"

.field private static final PHOTO_COLUMN_INDEX:Ljava/lang/String; = "photoIndex"

.field private static final PHOTO_COLUMN_URL:Ljava/lang/String; = "url"

.field private static final PHOTO_TABLE:Ljava/lang/String; = "photoTable"

.field private static final VERSION:I = 0x3


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 267
    const-string v0, "Blurb"

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 268
    return-void
.end method

.method private createOrderTableSql()Ljava/lang/String;
    .locals 1

    .prologue
    .line 324
    const-string v0, "create table orderTable (id integer primary key autoincrement,episodeId varchar not null,bookId varchar not null,orderId varchar not null,firstName varchar,lastName varchar,address1 varchar,address2 varchar,city varchar,state varchar,postalCode varchar,countryCode varchar not null,phone varchar,email varchar,lastFourCC varchar not null,title varchar,cover blob,bookType varchar not null,coverType varchar not null,numPages varchar not null,quantity varchar not null,orderTotal varchar not null,shippingTotal varchar not null,discountTotal varchar not null,discountCents varchar not null,taxTotal varchar not null,taxCents varchar not null,subTotal varchar not null,creditCardType integer,couponCode varchar);"

    .line 357
    .local v0, "create":Ljava/lang/String;
    return-object v0
.end method


# virtual methods
.method deleteBook(Lcom/blurb/checkout/service/BlurbManager$Book;)Z
    .locals 7
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$Book;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 680
    invoke-virtual {p0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 681
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 684
    .local v1, "result":Z
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 686
    const-string v3, "bookPK=?"

    .line 687
    .local v3, "whereClause":Ljava/lang/String;
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->bookId:Ljava/lang/String;

    aput-object v5, v2, v4

    .line 688
    .local v2, "whereArgs":[Ljava/lang/String;
    const-string v4, "photoTable"

    invoke-virtual {v0, v4, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 690
    const-string v3, "id=?"

    .line 691
    const/4 v4, 0x1

    new-array v2, v4, [Ljava/lang/String;

    .end local v2    # "whereArgs":[Ljava/lang/String;
    const/4 v4, 0x0

    iget-wide v5, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v4

    .line 692
    .restart local v2    # "whereArgs":[Ljava/lang/String;
    const-string v4, "bookTable"

    invoke-virtual {v0, v4, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 694
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 695
    const-wide/16 v4, -0x1

    iput-wide v4, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 697
    const/4 v1, 0x1

    .line 699
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 702
    return v1

    .line 699
    .end local v2    # "whereArgs":[Ljava/lang/String;
    .end local v3    # "whereClause":Ljava/lang/String;
    :catchall_0
    move-exception v4

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method deleteBookByBookId(Ljava/lang/String;)Z
    .locals 7
    .param p1, "bookId"    # Ljava/lang/String;

    .prologue
    .line 706
    invoke-virtual {p0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 707
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 710
    .local v2, "result":Z
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 712
    const-string v4, "bookPK=?"

    .line 713
    .local v4, "whereClause":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v3, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v3, v5

    .line 714
    .local v3, "whereArgs":[Ljava/lang/String;
    const-string v5, "photoTable"

    invoke-virtual {v0, v5, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 716
    const-string v4, "bookId=?"

    .line 717
    const/4 v5, 0x1

    new-array v3, v5, [Ljava/lang/String;

    .end local v3    # "whereArgs":[Ljava/lang/String;
    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    .line 718
    .restart local v3    # "whereArgs":[Ljava/lang/String;
    const-string v5, "bookTable"

    invoke-virtual {v0, v5, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 720
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 722
    const/4 v2, 0x1

    .line 726
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 729
    .end local v3    # "whereArgs":[Ljava/lang/String;
    .end local v4    # "whereClause":Ljava/lang/String;
    :goto_0
    return v2

    .line 723
    :catch_0
    move-exception v1

    .line 724
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 726
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5
.end method

.method deleteOrderInfo(Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)Z
    .locals 8
    .param p1, "info"    # Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .prologue
    .line 592
    invoke-virtual {p0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 593
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v2, 0x0

    .line 596
    .local v2, "result":Z
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 598
    const-string v4, "id=?"

    .line 599
    .local v4, "whereClause":Ljava/lang/String;
    const/4 v5, 0x1

    new-array v3, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p1, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->pk:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    .line 600
    .local v3, "whereArgs":[Ljava/lang/String;
    const-string v5, "orderTable"

    invoke-virtual {v0, v5, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 602
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 603
    const-wide/16 v5, -0x1

    iput-wide v5, p1, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->pk:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 605
    const/4 v2, 0x1

    .line 609
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 612
    .end local v3    # "whereArgs":[Ljava/lang/String;
    .end local v4    # "whereClause":Ljava/lang/String;
    :goto_0
    return v2

    .line 606
    :catch_0
    move-exception v1

    .line 607
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 609
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v5
.end method

.method getBookPhotos(Lcom/blurb/checkout/service/BlurbManager$Book;)Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    .locals 8
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$Book;

    .prologue
    .line 641
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getPhotosUnsorted(Lcom/blurb/checkout/service/BlurbManager$Book;)Ljava/util/ArrayList;

    move-result-object v5

    .line 642
    .local v5, "unsorted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/blurb/checkout/service/BlurbManager$Photo;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v4, v6, -0x2

    .line 643
    .local v4, "size":I
    if-gez v4, :cond_0

    .line 644
    const/4 v4, 0x0

    .line 646
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 647
    .local v3, "photos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/blurb/checkout/service/BlurbManager$Photo;>;"
    new-instance v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;

    invoke-direct {v0}, Lcom/blurb/checkout/service/BlurbManager$BookComplete;-><init>()V

    .line 649
    .local v0, "complete":Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    iget-wide v6, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    iput-wide v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->pk:J

    .line 650
    iget-object v6, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->bookId:Ljava/lang/String;

    iput-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    .line 651
    iget-object v6, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->sessionId:Ljava/lang/String;

    iput-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->sessionId:Ljava/lang/String;

    .line 652
    iget-object v6, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->projectId:Ljava/lang/String;

    iput-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->projectId:Ljava/lang/String;

    .line 653
    iget-object v6, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->episodeId:Ljava/lang/String;

    iput-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->episodeId:Ljava/lang/String;

    .line 654
    iget-object v6, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->coverType:Ljava/lang/String;

    iput-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->coverType:Ljava/lang/String;

    .line 655
    iget-object v6, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->startDate:Ljava/util/Date;

    iput-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->startDate:Ljava/util/Date;

    .line 656
    iget-object v6, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->title:Ljava/lang/String;

    iput-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->title:Ljava/lang/String;

    .line 657
    iput-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->photos:Ljava/util/ArrayList;

    .line 659
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/blurb/checkout/service/BlurbManager$Photo;

    .line 660
    .local v2, "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    iget v6, v2, Lcom/blurb/checkout/service/BlurbManager$Photo;->index:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_1

    .line 661
    iput-object v2, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->frontCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    goto :goto_0

    .line 662
    :cond_1
    iget v6, v2, Lcom/blurb/checkout/service/BlurbManager$Photo;->index:I

    const/4 v7, -0x2

    if-ne v6, v7, :cond_2

    .line 663
    iput-object v2, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->backCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    goto :goto_0

    .line 664
    :cond_2
    iget v6, v2, Lcom/blurb/checkout/service/BlurbManager$Photo;->index:I

    const/4 v7, -0x3

    if-ne v6, v7, :cond_3

    .line 665
    iput-object v2, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->spine:Lcom/blurb/checkout/service/BlurbManager$Photo;

    goto :goto_0

    .line 667
    :cond_3
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 670
    .end local v2    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    :cond_4
    new-instance v6, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper$1;

    invoke-direct {v6, p0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper$1;-><init>(Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;)V

    invoke-static {v3, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 676
    return-object v0
.end method

.method getOldestBook(I)Lcom/blurb/checkout/service/BlurbManager$Book;
    .locals 13
    .param p1, "counter"    # I

    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 433
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v12, 0x0

    .line 435
    .local v12, "result":Lcom/blurb/checkout/service/BlurbManager$Book;
    const/16 v1, 0x8

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "id"

    aput-object v3, v2, v1

    const/4 v1, 0x1

    const-string v3, "bookId"

    aput-object v3, v2, v1

    const/4 v1, 0x2

    const-string v3, "sessionId"

    aput-object v3, v2, v1

    const/4 v1, 0x3

    const-string v3, "projectId"

    aput-object v3, v2, v1

    const/4 v1, 0x4

    const-string v3, "episodeId"

    aput-object v3, v2, v1

    const/4 v1, 0x5

    const-string v3, "coverType"

    aput-object v3, v2, v1

    const/4 v1, 0x6

    const-string v3, "startDate"

    aput-object v3, v2, v1

    const/4 v1, 0x7

    const-string v3, "title"

    aput-object v3, v2, v1

    .line 437
    .local v2, "columns":[Ljava/lang/String;
    const-string v1, "bookTable"

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "startDate"

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 439
    .local v9, "cursor":Landroid/database/Cursor;
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 440
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 441
    if-lez p1, :cond_0

    .line 442
    invoke-interface {v9, p1}, Landroid/database/Cursor;->move(I)Z

    .line 446
    :cond_0
    new-instance v8, Lcom/blurb/checkout/service/BlurbManager$Book;

    invoke-direct {v8}, Lcom/blurb/checkout/service/BlurbManager$Book;-><init>()V

    .line 448
    .local v8, "book":Lcom/blurb/checkout/service/BlurbManager$Book;
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, v8, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    .line 449
    const/4 v1, 0x1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/blurb/checkout/service/BlurbManager$Book;->bookId:Ljava/lang/String;

    .line 450
    const/4 v1, 0x2

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/blurb/checkout/service/BlurbManager$Book;->sessionId:Ljava/lang/String;

    .line 451
    const/4 v1, 0x3

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/blurb/checkout/service/BlurbManager$Book;->projectId:Ljava/lang/String;

    .line 452
    const/4 v1, 0x4

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/blurb/checkout/service/BlurbManager$Book;->episodeId:Ljava/lang/String;

    .line 453
    const/4 v1, 0x5

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/blurb/checkout/service/BlurbManager$Book;->coverType:Ljava/lang/String;

    .line 455
    const/4 v1, 0x6

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 456
    .local v10, "date":J
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v10, v11}, Ljava/util/Date;-><init>(J)V

    iput-object v1, v8, Lcom/blurb/checkout/service/BlurbManager$Book;->startDate:Ljava/util/Date;

    .line 457
    const/4 v1, 0x7

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v8, Lcom/blurb/checkout/service/BlurbManager$Book;->title:Ljava/lang/String;

    .line 459
    move-object v12, v8

    .line 463
    .end local v8    # "book":Lcom/blurb/checkout/service/BlurbManager$Book;
    .end local v10    # "date":J
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 465
    return-object v12
.end method

.method getOrderInfo(Lcom/blurb/checkout/service/BlurbManager$Book;)Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    .locals 13
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$Book;

    .prologue
    const/4 v12, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 469
    const/4 v11, 0x0

    .line 470
    .local v11, "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    invoke-virtual {p0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 472
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const/16 v1, 0x1e

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "id"

    aput-object v1, v2, v7

    const-string v1, "episodeId"

    aput-object v1, v2, v12

    const/4 v1, 0x2

    const-string v6, "bookId"

    aput-object v6, v2, v1

    const/4 v1, 0x3

    const-string v6, "orderId"

    aput-object v6, v2, v1

    const/4 v1, 0x4

    const-string v6, "firstName"

    aput-object v6, v2, v1

    const/4 v1, 0x5

    const-string v6, "lastName"

    aput-object v6, v2, v1

    const/4 v1, 0x6

    const-string v6, "address1"

    aput-object v6, v2, v1

    const/4 v1, 0x7

    const-string v6, "address2"

    aput-object v6, v2, v1

    const/16 v1, 0x8

    const-string v6, "city"

    aput-object v6, v2, v1

    const/16 v1, 0x9

    const-string v6, "state"

    aput-object v6, v2, v1

    const/16 v1, 0xa

    const-string v6, "postalCode"

    aput-object v6, v2, v1

    const/16 v1, 0xb

    const-string v6, "countryCode"

    aput-object v6, v2, v1

    const/16 v1, 0xc

    const-string v6, "phone"

    aput-object v6, v2, v1

    const/16 v1, 0xd

    const-string v6, "email"

    aput-object v6, v2, v1

    const/16 v1, 0xe

    const-string v6, "lastFourCC"

    aput-object v6, v2, v1

    const/16 v1, 0xf

    const-string v6, "title"

    aput-object v6, v2, v1

    const/16 v1, 0x10

    const-string v6, "cover"

    aput-object v6, v2, v1

    const/16 v1, 0x11

    const-string v6, "bookType"

    aput-object v6, v2, v1

    const/16 v1, 0x12

    const-string v6, "coverType"

    aput-object v6, v2, v1

    const/16 v1, 0x13

    const-string v6, "numPages"

    aput-object v6, v2, v1

    const/16 v1, 0x14

    const-string v6, "quantity"

    aput-object v6, v2, v1

    const/16 v1, 0x15

    const-string v6, "orderTotal"

    aput-object v6, v2, v1

    const/16 v1, 0x16

    const-string v6, "shippingTotal"

    aput-object v6, v2, v1

    const/16 v1, 0x17

    const-string v6, "discountTotal"

    aput-object v6, v2, v1

    const/16 v1, 0x18

    const-string v6, "discountCents"

    aput-object v6, v2, v1

    const/16 v1, 0x19

    const-string v6, "taxTotal"

    aput-object v6, v2, v1

    const/16 v1, 0x1a

    const-string v6, "taxCents"

    aput-object v6, v2, v1

    const/16 v1, 0x1b

    const-string v6, "subTotal"

    aput-object v6, v2, v1

    const/16 v1, 0x1c

    const-string v6, "creditCardType"

    aput-object v6, v2, v1

    const/16 v1, 0x1d

    const-string v6, "couponCode"

    aput-object v6, v2, v1

    .line 505
    .local v2, "columns":[Ljava/lang/String;
    const-string v3, "bookId=?"

    .line 506
    .local v3, "whereClause":Ljava/lang/String;
    new-array v4, v12, [Ljava/lang/String;

    iget-object v1, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->bookId:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    .line 508
    .local v4, "whereArgs":[Ljava/lang/String;
    const-string v1, "orderTable"

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 510
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 512
    new-instance v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .end local v11    # "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    invoke-direct {v11}, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;-><init>()V

    .line 513
    .restart local v11    # "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    const/4 v9, 0x0

    .line 515
    .local v9, "field":I
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .local v10, "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iput-wide v5, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->pk:J

    .line 516
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->episodeId:Ljava/lang/String;

    .line 517
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    .line 518
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderId:Ljava/lang/String;

    .line 519
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->firstName:Ljava/lang/String;

    .line 520
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->lastName:Ljava/lang/String;

    .line 521
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->address1:Ljava/lang/String;

    .line 522
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->address2:Ljava/lang/String;

    .line 523
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->city:Ljava/lang/String;

    .line 524
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->state:Ljava/lang/String;

    .line 525
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->postalCode:Ljava/lang/String;

    .line 526
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->countryCode:Ljava/lang/String;

    .line 527
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->phone:Ljava/lang/String;

    .line 528
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->email:Ljava/lang/String;

    .line 529
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->lastFourCC:Ljava/lang/String;

    .line 530
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->title:Ljava/lang/String;

    .line 531
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->cover:[B

    .line 532
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookType:Ljava/lang/String;

    .line 533
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->coverType:Ljava/lang/String;

    .line 534
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->numPages:I

    .line 535
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->quantity:Ljava/lang/String;

    .line 536
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderTotal:Ljava/lang/String;

    .line 537
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->shippingTotal:Ljava/lang/String;

    .line 538
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->discountTotal:Ljava/lang/String;

    .line 539
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->discountCents:Ljava/lang/String;

    .line 540
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->taxTotal:Ljava/lang/String;

    .line 541
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->taxCents:Ljava/lang/String;

    .line 542
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->subTotal:Ljava/lang/String;

    .line 543
    add-int/lit8 v10, v9, 0x1

    .end local v9    # "field":I
    .restart local v10    # "field":I
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->creditCardType:I

    .line 544
    add-int/lit8 v9, v10, 0x1

    .end local v10    # "field":I
    .restart local v9    # "field":I
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->couponCode:Ljava/lang/String;

    .line 548
    .end local v9    # "field":I
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 550
    return-object v11
.end method

.method getPhotosUnsorted(Lcom/blurb/checkout/service/BlurbManager$Book;)Ljava/util/ArrayList;
    .locals 11
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$Book;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/blurb/checkout/service/BlurbManager$Book;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/blurb/checkout/service/BlurbManager$Photo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 378
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 379
    .local v10, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/blurb/checkout/service/BlurbManager$Photo;>;"
    invoke-virtual {p0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 381
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    const-string v3, "bookPK=?"

    .line 382
    .local v3, "whereClause":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-wide v5, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 384
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v1, 0x5

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "id"

    aput-object v5, v2, v1

    const/4 v1, 0x1

    const-string v5, "bookPK"

    aput-object v5, v2, v1

    const/4 v1, 0x2

    const-string v5, "photoIndex"

    aput-object v5, v2, v1

    const/4 v1, 0x3

    const-string v5, "url"

    aput-object v5, v2, v1

    const/4 v1, 0x4

    const-string v5, "blurbId"

    aput-object v5, v2, v1

    .line 386
    .local v2, "columns":[Ljava/lang/String;
    const-string v1, "photoTable"

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 388
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 390
    :cond_0
    new-instance v9, Lcom/blurb/checkout/service/BlurbManager$Photo;

    invoke-direct {v9}, Lcom/blurb/checkout/service/BlurbManager$Photo;-><init>()V

    .line 392
    .local v9, "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iput-wide v5, v9, Lcom/blurb/checkout/service/BlurbManager$Photo;->pk:J

    .line 393
    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iput-wide v5, v9, Lcom/blurb/checkout/service/BlurbManager$Photo;->bookPK:J

    .line 394
    const/4 v1, 0x2

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v9, Lcom/blurb/checkout/service/BlurbManager$Photo;->index:I

    .line 395
    const/4 v1, 0x3

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    .line 396
    const/4 v1, 0x4

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    .line 398
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 402
    .end local v9    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 404
    return-object v10
.end method

.method insertBook(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Book;)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "book"    # Lcom/blurb/checkout/service/BlurbManager$Book;

    .prologue
    .line 733
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 735
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "bookId"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->bookId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    const-string v1, "sessionId"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->sessionId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    const-string v1, "projectId"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->projectId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 738
    const-string v1, "episodeId"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->episodeId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    const-string v1, "coverType"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->coverType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    const-string v1, "startDate"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->startDate:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 741
    const-string v1, "title"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    const-string v1, "bookTable"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    iput-wide v1, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    .line 745
    iget-wide v1, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method insertOrderInfo(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "info"    # Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .prologue
    .line 554
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 556
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "episodeId"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->episodeId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    const-string v1, "bookId"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    const-string v1, "orderId"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    const-string v1, "firstName"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->firstName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    const-string v1, "lastName"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->lastName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 561
    const-string v1, "address1"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->address1:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const-string v1, "address2"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->address2:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 563
    const-string v1, "city"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->city:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    const-string v1, "state"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->state:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const-string v1, "postalCode"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->postalCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    const-string v1, "countryCode"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->countryCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 567
    const-string v1, "phone"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->phone:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    const-string v1, "email"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->email:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    const-string v1, "lastFourCC"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->lastFourCC:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const-string v1, "title"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    const-string v1, "cover"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->cover:[B

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 572
    const-string v1, "bookType"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 573
    const-string v1, "coverType"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->coverType:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 574
    const-string v1, "numPages"

    iget v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->numPages:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 575
    const-string v1, "quantity"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    const-string v1, "orderTotal"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderTotal:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    const-string v1, "shippingTotal"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->shippingTotal:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    const-string v1, "discountTotal"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->discountTotal:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    const-string v1, "discountCents"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->discountCents:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    const-string v1, "taxTotal"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->taxTotal:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    const-string v1, "taxCents"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->taxCents:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    const-string v1, "subTotal"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->subTotal:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    const-string v1, "creditCardType"

    iget v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->creditCardType:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 584
    const-string v1, "couponCode"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->couponCode:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    const-string v1, "orderTable"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    iput-wide v1, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->pk:J

    .line 588
    iget-wide v1, p2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->pk:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method insertPhoto(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Photo;)Z
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "photo"    # Lcom/blurb/checkout/service/BlurbManager$Photo;

    .prologue
    .line 408
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 410
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "bookPK"

    iget-wide v2, p2, Lcom/blurb/checkout/service/BlurbManager$Photo;->bookPK:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 411
    const-string v1, "photoIndex"

    iget v2, p2, Lcom/blurb/checkout/service/BlurbManager$Photo;->index:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 412
    const-string v1, "url"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    const-string v1, "blurbId"

    iget-object v2, p2, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    const-string v1, "photoTable"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    iput-wide v1, p2, Lcom/blurb/checkout/service/BlurbManager$Photo;->pk:J

    .line 417
    iget-wide v1, p2, Lcom/blurb/checkout/service/BlurbManager$Photo;->pk:J

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 279
    :try_start_0
    const-string v2, "PRAGMA foreign_keys = ON;"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 281
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 285
    const-string v0, "create table bookTable (id integer primary key autoincrement,bookId varchar not null unique,sessionId varchar not null,projectId varchar not null,episodeId varchar not null,coverType varchar not null,startDate integer,title varchar not null);"

    .line 296
    .local v0, "create":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 298
    const-string v0, "create table photoTable (id integer primary key autoincrement,photoIndex integer,url varchar not null,blurbId varchar unique,bookPK integer,FOREIGN KEY(bookPK) REFERENCES bookTable(id));"

    .line 307
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 309
    invoke-direct {p0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->createOrderTableSql()Ljava/lang/String;

    move-result-object v0

    .line 311
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 313
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 315
    const-string v2, "BlurbManager"

    const-string v3, "Blurb db created"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 321
    .end local v0    # "create":Ljava/lang/String;
    :goto_0
    return-void

    .line 316
    :catch_0
    move-exception v1

    .line 317
    .local v1, "e":Landroid/database/SQLException;
    :try_start_1
    invoke-virtual {v1}, Landroid/database/SQLException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 319
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v1    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v2

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 5
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 365
    const/4 v2, 0x2

    if-ge p2, v2, :cond_0

    .line 366
    invoke-direct {p0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->createOrderTableSql()Ljava/lang/String;

    move-result-object v0

    .line 368
    .local v0, "create":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 375
    .end local v0    # "create":Ljava/lang/String;
    :goto_0
    return-void

    .line 369
    :cond_0
    const/4 v2, 0x3

    if-ge p2, v2, :cond_1

    .line 370
    const-string v1, "ALTER TABLE orderTable ADD COLUMN taxCents"

    .line 371
    .local v1, "sql":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    goto :goto_0

    .line 373
    .end local v1    # "sql":Ljava/lang/String;
    :cond_1
    const-string v2, "BlurbManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown version upgrade "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method updateBookDate(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Book;)Z
    .locals 8
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "book"    # Lcom/blurb/checkout/service/BlurbManager$Book;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 749
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 751
    .local v0, "values":Landroid/content/ContentValues;
    const-string v5, "startDate"

    iget-object v6, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->startDate:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 753
    const-string v2, "id=?"

    .line 754
    .local v2, "whereClause":Ljava/lang/String;
    new-array v1, v3, [Ljava/lang/String;

    iget-wide v5, p2, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 756
    .local v1, "whereArgs":[Ljava/lang/String;
    const-string v5, "bookTable"

    invoke-virtual {p1, v5, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-ne v5, v3, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method

.method updatePhotoBlurbId(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Photo;)Z
    .locals 7
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "photo"    # Lcom/blurb/checkout/service/BlurbManager$Photo;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 421
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 423
    .local v0, "values":Landroid/content/ContentValues;
    const-string v5, "blurbId"

    iget-object v6, p2, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    const-string v2, "id=?"

    .line 426
    .local v2, "whereClause":Ljava/lang/String;
    new-array v1, v3, [Ljava/lang/String;

    iget-wide v5, p2, Lcom/blurb/checkout/service/BlurbManager$Photo;->pk:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 428
    .local v1, "whereArgs":[Ljava/lang/String;
    const-string v5, "photoTable"

    invoke-virtual {p1, v5, v0, v2, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    if-ne v5, v3, :cond_0

    :goto_0
    return v3

    :cond_0
    move v3, v4

    goto :goto_0
.end method
