.class public Lcom/blurb/checkout/service/UploadService;
.super Landroid/app/IntentService;
.source "UploadService.java"


# static fields
.field public static final ACTION_UPLOAD_BOOKS:Ljava/lang/String; = "com.blurb.checkout.service.UPLOAD_BOOKS"

.field private static final DEBUG:Z = false

.field public static final EXTRA_BOOK_ID:Ljava/lang/String; = "bookId"

.field public static final EXTRA_CURRENT_STEP:Ljava/lang/String; = "currentStep"

.field public static final EXTRA_PROGRESS:Ljava/lang/String; = "progress"

.field public static final EXTRA_TOTAL_STEPS:Ljava/lang/String; = "totalSteps"

.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"

.field public static final NUM_EXTRA_PHOTOS:I = 0x3

.field private static final RETRY_LIMIT_SECONDS:I = 0x200

.field public static final UPLOAD_EXPIRED:I = -0x1

.field private static final UPLOAD_EXPIRE_MS:J = 0xa4cb800L

.field private static final UPLOAD_EXPIRE_SECONDS:J = 0x2a300L

.field public static final UPLOAD_FAILED:I = -0x2


# instance fields
.field private currentRetry:I

.field private mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 65
    const-string v0, "BlurbUploadService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 60
    iput v1, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    .line 69
    invoke-virtual {p0, v1}, Lcom/blurb/checkout/service/UploadService;->setIntentRedelivery(Z)V

    .line 70
    return-void
.end method

.method private getFailureNotifyIntent(Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;Z)Landroid/app/PendingIntent;
    .locals 5
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    .param p2, "info"    # Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    .param p3, "completed"    # Z

    .prologue
    .line 310
    if-nez p2, :cond_0

    .line 311
    const-string v3, "BlurbShadowApp"

    const-string v4, "getFailureNotifyIntent: No order info!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    const/4 v2, 0x0

    .line 332
    :goto_0
    return-object v2

    .line 315
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/blurb/checkout/UnrecoverableActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 318
    .local v1, "notifyIntent":Landroid/content/Intent;
    const v3, 0x10008000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 319
    const-string v3, "orderInfo"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 321
    iget-object v3, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 324
    .local v0, "bookId":I
    const/high16 v3, 0x8000000

    invoke-static {p0, v0, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 332
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    goto :goto_0
.end method

.method private getNotifyIntent(Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;Z)Landroid/app/PendingIntent;
    .locals 5
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    .param p2, "info"    # Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    .param p3, "completed"    # Z

    .prologue
    .line 282
    if-nez p2, :cond_0

    .line 283
    const-string v3, "BlurbShadowApp"

    const-string v4, "getNotifyIntent: No order info!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    const/4 v2, 0x0

    .line 306
    :goto_0
    return-object v2

    .line 288
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/blurb/checkout/OrderPhotoBookActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 291
    .local v1, "notifyIntent":Landroid/content/Intent;
    const v3, 0x10008000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 292
    const-string v3, "orderInfo"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 293
    const-string v3, "uploadCompleted"

    invoke-virtual {v1, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 295
    iget-object v3, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 298
    .local v0, "bookId":I
    const/high16 v3, 0x8000000

    invoke-static {p0, v0, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 306
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    goto :goto_0
.end method

.method private getPhotoIds(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)Ljava/util/ArrayList;
    .locals 4
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/blurb/checkout/service/BlurbManager$BookComplete;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 481
    .local v2, "photoIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/blurb/checkout/service/BlurbManager$Photo;

    .line 482
    .local v1, "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    iget-object v3, v1, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 485
    .end local v1    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    :cond_0
    return-object v2
.end method

.method private getPhotoUrls(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)Ljava/util/ArrayList;
    .locals 4
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/blurb/checkout/service/BlurbManager$BookComplete;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 489
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 490
    .local v2, "photoUrls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/blurb/checkout/service/BlurbManager$Photo;

    .line 491
    .local v1, "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    iget-object v3, v1, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 494
    .end local v1    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    :cond_0
    return-object v2
.end method

.method private static isNetworkAvailable(Landroid/content/Context;)Z
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x1

    .line 510
    const/4 v10, 0x0

    .line 511
    .local v10, "result":Z
    const/4 v6, 0x0

    .line 512
    .local v6, "hasWifi":Z
    const/4 v5, 0x0

    .line 514
    .local v5, "hasMobile":Z
    const-string v11, "connectivity"

    invoke-virtual {p0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/ConnectivityManager;

    .line 515
    .local v3, "connectivity":Landroid/net/ConnectivityManager;
    if-eqz v3, :cond_2

    .line 516
    invoke-virtual {v3}, Landroid/net/ConnectivityManager;->getAllNetworkInfo()[Landroid/net/NetworkInfo;

    move-result-object v8

    .line 517
    .local v8, "info":[Landroid/net/NetworkInfo;
    if-eqz v8, :cond_2

    .line 518
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    array-length v11, v8

    if-ge v7, v11, :cond_2

    .line 519
    aget-object v11, v8, v7

    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v11

    sget-object v12, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v11, v12, :cond_0

    .line 520
    aget-object v11, v8, v7

    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getType()I

    move-result v11

    if-ne v13, v11, :cond_1

    .line 521
    const/4 v6, 0x1

    .line 518
    :cond_0
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 522
    :cond_1
    aget-object v11, v8, v7

    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getType()I

    move-result v11

    if-nez v11, :cond_0

    .line 523
    const/4 v5, 0x1

    goto :goto_1

    .line 529
    .end local v7    # "i":I
    .end local v8    # "info":[Landroid/net/NetworkInfo;
    :cond_2
    if-nez v5, :cond_3

    if-eqz v6, :cond_4

    .line 533
    :cond_3
    :try_start_0
    const-string v11, "blurb.com"

    invoke-static {v11}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    .line 535
    .local v1, "address":Ljava/net/InetAddress;
    if-nez v1, :cond_5

    .line 536
    const-string v11, "BlurbShadowApp"

    const-string v12, "DNS lookup of blurb.com failed"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 558
    .end local v1    # "address":Ljava/net/InetAddress;
    :cond_4
    :goto_2
    return v10

    .line 537
    .restart local v1    # "address":Ljava/net/InetAddress;
    :cond_5
    if-eqz v6, :cond_6

    .line 541
    const/4 v10, 0x1

    goto :goto_2

    .line 542
    :cond_6
    if-eqz v5, :cond_4

    .line 543
    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    .line 544
    .local v0, "addrBytes":[B
    const/4 v11, 0x3

    aget-byte v11, v0, v11

    shl-int/lit8 v11, v11, 0x18

    const/4 v12, 0x2

    aget-byte v12, v0, v12

    shl-int/lit8 v12, v12, 0x10

    or-int/2addr v11, v12

    const/4 v12, 0x1

    aget-byte v12, v0, v12

    shl-int/lit8 v12, v12, 0x8

    or-int/2addr v11, v12

    const/4 v12, 0x0

    aget-byte v12, v0, v12

    or-int v9, v11, v12

    .line 548
    .local v9, "ipAddress":I
    const-string v11, "connectivity"

    invoke-virtual {p0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 549
    .local v2, "cm":Landroid/net/ConnectivityManager;
    const/4 v11, 0x0

    invoke-virtual {v2, v11, v9}, Landroid/net/ConnectivityManager;->requestRouteToHost(II)Z
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    goto :goto_2

    .line 553
    .end local v0    # "addrBytes":[B
    .end local v1    # "address":Ljava/net/InetAddress;
    .end local v2    # "cm":Landroid/net/ConnectivityManager;
    .end local v9    # "ipAddress":I
    :catch_0
    move-exception v4

    .line 554
    .local v4, "e":Ljava/net/UnknownHostException;
    const-string v11, "BlurbShadowApp"

    const-string v12, "Blurb.com is unknown host"

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private notifyFailure(Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V
    .locals 7
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    .param p2, "info"    # Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .prologue
    const/4 v6, 0x0

    .line 246
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f020002

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f0400d7

    invoke-virtual {p0, v3}, Lcom/blurb/checkout/service/UploadService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f0400d8

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->title:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/blurb/checkout/service/UploadService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 252
    .local v0, "failBuilder":Landroid/app/Notification$Builder;
    const/4 v1, 0x0

    .line 253
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    if-eqz p2, :cond_0

    .line 254
    invoke-direct {p0, p1, p2, v6}, Lcom/blurb/checkout/service/UploadService;->getFailureNotifyIntent(Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;Z)Landroid/app/PendingIntent;

    move-result-object v1

    .line 256
    :cond_0
    if-eqz v1, :cond_1

    .line 257
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 259
    :cond_1
    iget-object v2, p0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v3, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v4

    invoke-virtual {v2, v3, v6, v4}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 263
    if-eqz p2, :cond_2

    .line 264
    iget-object v2, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    const/4 v3, -0x2

    invoke-direct {p0, v2, v3, v6}, Lcom/blurb/checkout/service/UploadService;->sendUploadProgressBroadcast(Ljava/lang/String;II)V

    .line 267
    :goto_0
    return-void

    .line 266
    :cond_2
    iget-object v2, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-direct {p0, v2, v3, v6}, Lcom/blurb/checkout/service/UploadService;->sendUploadProgressBroadcast(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method private notifyPause(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)V
    .locals 6
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;

    .prologue
    const/4 v5, 0x0

    .line 270
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f020002

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f0400d5

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/service/UploadService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    const v2, 0x7f0400dd

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->title:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/blurb/checkout/service/UploadService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 276
    .local v0, "pauseBuilder":Landroid/app/Notification$Builder;
    iget-object v1, p0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v2, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v1, v2, v5, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 279
    return-void
.end method

.method private sendUploadProgressBroadcast(Ljava/lang/String;II)V
    .locals 4
    .param p1, "bookId"    # Ljava/lang/String;
    .param p2, "currentStep"    # I
    .param p3, "totalSteps"    # I

    .prologue
    .line 498
    if-ltz p2, :cond_0

    int-to-float v2, p2

    int-to-float v3, p3

    div-float v1, v2, v3

    .line 499
    .local v1, "progress":F
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/blurb/checkout/UploadProgressReceiver;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 501
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "bookId"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 502
    const-string v2, "progress"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 503
    const-string v2, "currentStep"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 504
    const-string v2, "totalSteps"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 506
    invoke-virtual {p0, v0}, Lcom/blurb/checkout/service/UploadService;->sendBroadcast(Landroid/content/Intent;)V

    .line 507
    return-void

    .line 498
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "progress":F
    :cond_0
    int-to-float v1, p2

    goto :goto_0
.end method

.method private snooze()V
    .locals 2

    .prologue
    .line 237
    :try_start_0
    iget v0, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_0
    iget v0, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    const/16 v1, 0x200

    if-ge v0, v1, :cond_0

    .line 242
    iget v0, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    .line 243
    :cond_0
    return-void

    .line 238
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private tryUpload(Lcom/blurb/checkout/service/BlurbManager;)V
    .locals 13
    .param p1, "mgr"    # Lcom/blurb/checkout/service/BlurbManager;

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 98
    .local v0, "book":Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    const/4 v2, 0x0

    .line 101
    .local v2, "counter":I
    :cond_0
    :goto_0
    const/4 v3, 0x0

    .line 104
    .local v3, "doSleep":Z
    :try_start_0
    invoke-virtual {p1, v2}, Lcom/blurb/checkout/service/BlurbManager;->getOldestBook(I)Lcom/blurb/checkout/service/BlurbManager$BookComplete;

    move-result-object v0

    .line 106
    if-nez v0, :cond_2

    .line 107
    if-nez v2, :cond_1

    .line 109
    return-void

    .line 112
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 116
    :cond_2
    :goto_1
    invoke-static {p0}, Lcom/blurb/checkout/service/UploadService;->isNetworkAvailable(Landroid/content/Context;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 117
    invoke-direct {p0, v0}, Lcom/blurb/checkout/service/UploadService;->notifyPause(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)V

    .line 118
    invoke-direct {p0}, Lcom/blurb/checkout/service/UploadService;->snooze()V
    :try_end_0
    .catch Lorg/apache/http/NoHttpResponseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/HttpRetryException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/InterruptedIOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_6

    goto :goto_1

    .line 200
    :catch_0
    move-exception v4

    .line 201
    .local v4, "e":Lorg/apache/http/NoHttpResponseException;
    const-string v8, "BlurbShadowApp"

    const-string v9, "UploadService.onHandleIntent: NoHTTPResponseException, retrying"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    const/4 v3, 0x1

    .line 224
    .end local v4    # "e":Lorg/apache/http/NoHttpResponseException;
    :cond_3
    :goto_2
    if-eqz v3, :cond_0

    .line 225
    if-eqz v0, :cond_4

    .line 226
    invoke-direct {p0, v0}, Lcom/blurb/checkout/service/UploadService;->notifyPause(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)V

    .line 227
    :cond_4
    invoke-direct {p0}, Lcom/blurb/checkout/service/UploadService;->snooze()V

    goto :goto_0

    .line 121
    :cond_5
    :try_start_1
    invoke-direct {p0, v0}, Lcom/blurb/checkout/service/UploadService;->uploadExpired(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 122
    const-string v8, "BlurbShadowApp"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "UploadService.onHandleIntent book expired, deleting "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    invoke-virtual {p1, v0}, Lcom/blurb/checkout/service/BlurbManager;->deleteBook(Lcom/blurb/checkout/service/BlurbManager$Book;)V

    .line 125
    const/4 v8, 0x0

    invoke-direct {p0, v0, v8}, Lcom/blurb/checkout/service/UploadService;->notifyFailure(Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V

    .line 127
    const/4 v0, 0x0

    goto :goto_2

    .line 129
    :cond_6
    invoke-virtual {p1, v0}, Lcom/blurb/checkout/service/BlurbManager;->getOrderInfo(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    move-result-object v5

    .line 130
    .local v5, "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    if-nez v5, :cond_7

    .line 132
    const-string v8, "BlurbShadowApp"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Order info can\'t be found for book id "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    invoke-direct {p0, v0, v5}, Lcom/blurb/checkout/service/UploadService;->notifyFailure(Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V

    .line 135
    invoke-virtual {p1, v0}, Lcom/blurb/checkout/service/BlurbManager;->deleteBook(Lcom/blurb/checkout/service/BlurbManager$Book;)V

    .line 137
    const/4 v0, 0x0

    goto :goto_2

    .line 139
    :cond_7
    const/4 v8, 0x0

    invoke-direct {p0, v0, v5, v8}, Lcom/blurb/checkout/service/UploadService;->getNotifyIntent(Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;Z)Landroid/app/PendingIntent;

    move-result-object v6

    .line 141
    .local v6, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v8, Landroid/app/Notification$Builder;

    invoke-direct {v8, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v9, 0x7f020002

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    const v9, 0x7f0400d5

    invoke-virtual {p0, v9}, Lcom/blurb/checkout/service/UploadService;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    const v9, 0x7f0400d6

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->title:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/blurb/checkout/service/UploadService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    iget-object v9, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->photos:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, 0x3

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    move-result-object v1

    .line 148
    .local v1, "builder":Landroid/app/Notification$Builder;
    if-eqz v6, :cond_8

    .line 149
    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 151
    :cond_8
    iget-object v8, p0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v9, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 155
    invoke-direct {p0, p1, v0, v1}, Lcom/blurb/checkout/service/UploadService;->uploadBook(Lcom/blurb/checkout/service/BlurbManager;Lcom/blurb/checkout/service/BlurbManager$BookComplete;Landroid/app/Notification$Builder;)I

    move-result v7

    .line 157
    .local v7, "status":I
    const/16 v8, 0xc8

    if-ne v7, v8, :cond_9

    .line 158
    const/4 v8, 0x1

    iput v8, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    .line 160
    const v8, 0x7f0400d3

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/service/UploadService;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    const v9, 0x7f0400d4

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->title:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/blurb/checkout/service/UploadService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 164
    const/4 v8, 0x1

    invoke-direct {p0, v0, v5, v8}, Lcom/blurb/checkout/service/UploadService;->getNotifyIntent(Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;Z)Landroid/app/PendingIntent;

    move-result-object v6

    .line 165
    invoke-virtual {v1, v6}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 168
    iget-object v8, p0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    iget-object v9, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v11

    invoke-virtual {v8, v9, v10, v11}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 172
    invoke-virtual {p1, v5}, Lcom/blurb/checkout/service/BlurbManager;->deleteOrderInfo(Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V

    .line 175
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 176
    :cond_9
    const/16 v8, -0x64

    if-ne v7, v8, :cond_a

    .line 178
    invoke-direct {p0, v0, v5}, Lcom/blurb/checkout/service/UploadService;->notifyFailure(Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V

    .line 179
    invoke-virtual {p1, v0}, Lcom/blurb/checkout/service/BlurbManager;->deleteBook(Lcom/blurb/checkout/service/BlurbManager$Book;)V

    .line 180
    invoke-virtual {p1, v5}, Lcom/blurb/checkout/service/BlurbManager;->deleteOrderInfo(Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V

    .line 181
    iget-object v8, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    invoke-static {v8}, Lcom/blurb/checkout/UploadProgressReceiver;->forgetProgress(Ljava/lang/String;)V

    .line 183
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 185
    :cond_a
    invoke-direct {p0, v0}, Lcom/blurb/checkout/service/UploadService;->notifyPause(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)V
    :try_end_1
    .catch Lorg/apache/http/NoHttpResponseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/HttpRetryException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/InterruptedIOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6

    .line 191
    :try_start_2
    iget v8, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    mul-int/lit16 v8, v8, 0x3e8

    int-to-long v8, v8

    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V

    .line 193
    iget v8, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    const/16 v9, 0x200

    if-ge v8, v9, :cond_3

    .line 194
    iget v8, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    mul-int/lit8 v8, v8, 0x2

    iput v8, p0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lorg/apache/http/NoHttpResponseException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/HttpRetryException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    goto/16 :goto_2

    .line 195
    :catch_1
    move-exception v8

    goto/16 :goto_2

    .line 203
    .end local v1    # "builder":Landroid/app/Notification$Builder;
    .end local v5    # "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    .end local v6    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v7    # "status":I
    :catch_2
    move-exception v4

    .line 204
    .local v4, "e":Ljava/net/HttpRetryException;
    const-string v8, "BlurbShadowApp"

    const-string v9, "UploadService.onHandleIntent: HttpRetryException, retrying"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    const/4 v3, 0x1

    .line 222
    goto/16 :goto_2

    .line 206
    .end local v4    # "e":Ljava/net/HttpRetryException;
    :catch_3
    move-exception v4

    .line 207
    .local v4, "e":Ljava/net/SocketException;
    const-string v8, "BlurbShadowApp"

    const-string v9, "UploadService.onHandleIntent: SocketException, retrying"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    const/4 v3, 0x1

    .line 222
    goto/16 :goto_2

    .line 209
    .end local v4    # "e":Ljava/net/SocketException;
    :catch_4
    move-exception v4

    .line 210
    .local v4, "e":Ljava/io/InterruptedIOException;
    const-string v8, "BlurbShadowApp"

    const-string v9, "UploadService.onHandleIntent: InterruptedIOException, retrying"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v3, 0x1

    .line 222
    goto/16 :goto_2

    .line 212
    .end local v4    # "e":Ljava/io/InterruptedIOException;
    :catch_5
    move-exception v4

    .line 213
    .local v4, "e":Ljava/net/UnknownHostException;
    const-string v8, "BlurbShadowApp"

    const-string v9, "UploadService.onHandleIntent: UnknownHostException, retrying"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/4 v3, 0x1

    .line 222
    goto/16 :goto_2

    .line 215
    .end local v4    # "e":Ljava/net/UnknownHostException;
    :catch_6
    move-exception v4

    .line 217
    .local v4, "e":Ljava/lang/Exception;
    add-int/lit8 v2, v2, 0x1

    .line 219
    const-string v8, "BlurbShadowApp"

    const-string v9, "UploadService.onHandleIntent: Unhandled exception!"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 221
    const/4 v3, 0x1

    goto/16 :goto_2
.end method

.method private uploadBook(Lcom/blurb/checkout/service/BlurbManager;Lcom/blurb/checkout/service/BlurbManager$BookComplete;Landroid/app/Notification$Builder;)I
    .locals 28
    .param p1, "mgr"    # Lcom/blurb/checkout/service/BlurbManager;
    .param p2, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    .param p3, "builder"    # Landroid/app/Notification$Builder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 366
    const-string v3, "BlurbShadowApp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UploadService.uploadBook starting book "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v27, v3, 0x3

    .line 369
    .local v27, "totalSteps":I
    const/16 v23, 0x0

    .line 370
    .local v23, "currentStep":I
    const/16 v25, 0x0

    .line 372
    .local v25, "result":Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;
    const v3, 0x7f0400ec

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Lcom/blurb/checkout/service/UploadService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v26

    .line 374
    .local v26, "steps":Ljava/lang/String;
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->frontCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    if-eqz v3, :cond_1

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->frontCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    if-nez v3, :cond_1

    .line 377
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->sessionId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->projectId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->frontCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/blurb/checkout/service/UploadService;->uploadPhoto(Lcom/blurb/checkout/service/BlurbManager;Ljava/lang/String;Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$Photo;)Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;

    move-result-object v25

    .line 379
    invoke-virtual/range {v25 .. v25}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_0

    .line 380
    invoke-virtual/range {v25 .. v25}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v3

    .line 476
    :goto_0
    return v3

    .line 381
    :cond_0
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    .line 384
    :cond_1
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v27

    invoke-direct {v0, v3, v1, v2}, Lcom/blurb/checkout/service/UploadService;->sendUploadProgressBroadcast(Ljava/lang/String;II)V

    .line 385
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0400d5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/blurb/checkout/service/UploadService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0400ec

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/blurb/checkout/service/UploadService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 386
    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    const/4 v4, 0x0

    move/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v3, v0, v1, v4}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 388
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {p3 .. p3}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 392
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->backCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    if-eqz v3, :cond_3

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->backCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 395
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->sessionId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->projectId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->backCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/blurb/checkout/service/UploadService;->uploadPhoto(Lcom/blurb/checkout/service/BlurbManager;Ljava/lang/String;Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$Photo;)Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;

    move-result-object v25

    .line 397
    invoke-virtual/range {v25 .. v25}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_2

    .line 398
    invoke-virtual/range {v25 .. v25}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v3

    goto/16 :goto_0

    .line 399
    :cond_2
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    .line 402
    :cond_3
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v27

    invoke-direct {v0, v3, v1, v2}, Lcom/blurb/checkout/service/UploadService;->sendUploadProgressBroadcast(Ljava/lang/String;II)V

    .line 403
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0400d5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/blurb/checkout/service/UploadService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0400ec

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/blurb/checkout/service/UploadService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 404
    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    const/4 v4, 0x0

    move/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v3, v0, v1, v4}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 406
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {p3 .. p3}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 410
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->spine:Lcom/blurb/checkout/service/BlurbManager$Photo;

    if-eqz v3, :cond_5

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->spine:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 413
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->sessionId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->projectId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v8, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->spine:Lcom/blurb/checkout/service/BlurbManager$Photo;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/blurb/checkout/service/UploadService;->uploadPhoto(Lcom/blurb/checkout/service/BlurbManager;Ljava/lang/String;Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$Photo;)Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;

    move-result-object v25

    .line 415
    invoke-virtual/range {v25 .. v25}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_4

    .line 416
    invoke-virtual/range {v25 .. v25}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v3

    goto/16 :goto_0

    .line 417
    :cond_4
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    .line 420
    :cond_5
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v27

    invoke-direct {v0, v3, v1, v2}, Lcom/blurb/checkout/service/UploadService;->sendUploadProgressBroadcast(Ljava/lang/String;II)V

    .line 421
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0400d5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/blurb/checkout/service/UploadService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0400ec

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/blurb/checkout/service/UploadService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 422
    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    const/4 v4, 0x0

    move/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v3, v0, v1, v4}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 424
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {p3 .. p3}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 428
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v24

    .local v24, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/blurb/checkout/service/BlurbManager$Photo;

    .line 432
    .local v8, "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    iget-object v3, v8, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 433
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->sessionId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->projectId:Ljava/lang/String;

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object/from16 v7, p2

    invoke-direct/range {v3 .. v8}, Lcom/blurb/checkout/service/UploadService;->uploadPhoto(Lcom/blurb/checkout/service/BlurbManager;Ljava/lang/String;Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$Photo;)Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;

    move-result-object v25

    .line 435
    invoke-virtual/range {v25 .. v25}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v3

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_6

    .line 436
    invoke-virtual/range {v25 .. v25}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v3

    goto/16 :goto_0

    .line 437
    :cond_6
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput v3, v0, Lcom/blurb/checkout/service/UploadService;->currentRetry:I

    .line 440
    :cond_7
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    add-int/lit8 v23, v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    move/from16 v2, v27

    invoke-direct {v0, v3, v1, v2}, Lcom/blurb/checkout/service/UploadService;->sendUploadProgressBroadcast(Ljava/lang/String;II)V

    .line 441
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0400d5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/blurb/checkout/service/UploadService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const v4, 0x7f0400ec

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Lcom/blurb/checkout/service/UploadService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    .line 442
    move-object/from16 v0, p3

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    const/4 v4, 0x0

    move/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v3, v0, v1, v4}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    .line 444
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {p3 .. p3}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_1

    .line 449
    .end local v8    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    :cond_8
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/blurb/checkout/service/UploadService;->getPhotoIds(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)Ljava/util/ArrayList;

    move-result-object v15

    .line 454
    .local v15, "photoIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v14, ""

    .line 455
    .local v14, "spineId":Ljava/lang/String;
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->spine:Lcom/blurb/checkout/service/BlurbManager$Photo;

    if-eqz v3, :cond_9

    .line 456
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->spine:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v14, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    .line 458
    :cond_9
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v10, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->sessionId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->projectId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->frontCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v12, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->backCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v13, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->coverType:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v9 .. v16}, Lcom/blurb/checkout/BlurbAPI;->putGenerateBBF(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;

    move-result-object v22

    .line 460
    .local v22, "bbfResult":Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;
    invoke-virtual/range {v22 .. v22}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->getHttpStatus()I

    move-result v3

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_a

    .line 461
    invoke-virtual/range {p1 .. p2}, Lcom/blurb/checkout/service/BlurbManager;->deleteBook(Lcom/blurb/checkout/service/BlurbManager$Book;)V

    .line 469
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/blurb/checkout/service/UploadService;->getPhotoUrls(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)Ljava/util/ArrayList;

    move-result-object v21

    .line 470
    .local v21, "photoUrls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p2

    iget-object v0, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->episodeId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->frontCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    if-eqz v3, :cond_b

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->frontCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v0, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    move-object/from16 v18, v0

    :goto_2
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->backCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    if-eqz v3, :cond_c

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->backCover:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v0, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    move-object/from16 v19, v0

    :goto_3
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->spine:Lcom/blurb/checkout/service/BlurbManager$Photo;

    if-eqz v3, :cond_d

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->spine:Lcom/blurb/checkout/service/BlurbManager$Photo;

    iget-object v0, v3, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    move-object/from16 v20, v0

    :goto_4
    move-object/from16 v16, p0

    invoke-static/range {v16 .. v21}, Lcom/blurb/checkout/EpisodeAPI;->sendImagesCompleteBroadcast(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 476
    const/16 v3, 0xc8

    goto/16 :goto_0

    .line 463
    .end local v21    # "photoUrls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_a
    invoke-virtual/range {v22 .. v22}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->getHttpStatus()I

    move-result v3

    goto/16 :goto_0

    .line 470
    .restart local v21    # "photoUrls":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_b
    const/16 v18, 0x0

    goto :goto_2

    :cond_c
    const/16 v19, 0x0

    goto :goto_3

    :cond_d
    const/16 v20, 0x0

    goto :goto_4
.end method

.method private uploadExpired(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)Z
    .locals 7
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;

    .prologue
    .line 336
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 337
    .local v2, "now":Ljava/util/Date;
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    iget-object v5, p1, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->startDate:Ljava/util/Date;

    invoke-virtual {v5}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    sub-long v0, v3, v5

    .line 338
    .local v0, "delta":J
    const-wide/32 v3, 0xa4cb800

    cmp-long v3, v0, v3

    if-lez v3, :cond_0

    .line 339
    const/4 v3, 0x1

    .line 341
    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private uploadPhoto(Lcom/blurb/checkout/service/BlurbManager;Ljava/lang/String;Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$BookComplete;Lcom/blurb/checkout/service/BlurbManager$Photo;)Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;
    .locals 4
    .param p1, "mgr"    # Lcom/blurb/checkout/service/BlurbManager;
    .param p2, "sessionId"    # Ljava/lang/String;
    .param p3, "projectId"    # Ljava/lang/String;
    .param p4, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    .param p5, "photo"    # Lcom/blurb/checkout/service/BlurbManager$Photo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/InterruptedIOException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v2, 0xc8

    .line 347
    iget-object v1, p5, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    invoke-static {p2, p3, v1}, Lcom/blurb/checkout/BlurbAPI;->postImageUpload(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;

    move-result-object v0

    .line 349
    .local v0, "imageResult":Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;
    invoke-virtual {v0}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v1

    if-ne v1, v2, :cond_1

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->photoId:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 351
    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->photoId:Ljava/lang/String;

    invoke-virtual {p1, p4, p5, v1}, Lcom/blurb/checkout/service/BlurbManager;->setPhotoUploaded(Lcom/blurb/checkout/service/BlurbManager$Book;Lcom/blurb/checkout/service/BlurbManager$Photo;Ljava/lang/String;)V

    .line 362
    :cond_0
    :goto_0
    return-object v0

    .line 358
    :cond_1
    invoke-virtual {v0}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v1

    if-eq v1, v2, :cond_0

    .line 359
    const-string v1, "BlurbShadowApp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "uploadPhoto failed for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p4, Lcom/blurb/checkout/service/BlurbManager$BookComplete;->bookId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p5, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " with http status "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "action":Ljava/lang/String;
    iget-object v2, p0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    .line 80
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/service/UploadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iput-object v2, p0, Lcom/blurb/checkout/service/UploadService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 82
    :cond_0
    const-string v2, "com.blurb.checkout.service.UPLOAD_BOOKS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    new-instance v1, Lcom/blurb/checkout/service/BlurbManager;

    invoke-direct {v1, p0}, Lcom/blurb/checkout/service/BlurbManager;-><init>(Landroid/content/Context;)V

    .line 86
    .local v1, "mgr":Lcom/blurb/checkout/service/BlurbManager;
    :try_start_0
    invoke-direct {p0, v1}, Lcom/blurb/checkout/service/UploadService;->tryUpload(Lcom/blurb/checkout/service/BlurbManager;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    invoke-virtual {v1}, Lcom/blurb/checkout/service/BlurbManager;->close()V

    .line 94
    .end local v1    # "mgr":Lcom/blurb/checkout/service/BlurbManager;
    :cond_1
    return-void

    .line 88
    .restart local v1    # "mgr":Lcom/blurb/checkout/service/BlurbManager;
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/blurb/checkout/service/BlurbManager;->close()V

    throw v2
.end method
