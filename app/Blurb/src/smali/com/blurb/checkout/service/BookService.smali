.class public Lcom/blurb/checkout/service/BookService;
.super Landroid/app/IntentService;
.source "BookService.java"


# static fields
.field public static final ACTION_PUT_BOOK:Ljava/lang/String; = "com.blurb.checkout.service.PUT_BOOK"

.field public static final ACTION_TEST:Ljava/lang/String; = "com.blurb.checkout.service.TEST"

.field private static final DEBUG:Z = false

.field public static final EXTRA_BACK_COVER:Ljava/lang/String; = "backCover"

.field public static final EXTRA_BOOK_ID:Ljava/lang/String; = "bookId"

.field public static final EXTRA_COVER_TYPE:Ljava/lang/String; = "coverType"

.field public static final EXTRA_EPISODE_ID:Ljava/lang/String; = "episodeId"

.field public static final EXTRA_FRONT_COVER:Ljava/lang/String; = "frontCover"

.field public static final EXTRA_ORDER_INFO:Ljava/lang/String; = "orderInfo"

.field public static final EXTRA_PHOTOS:Ljava/lang/String; = "photos"

.field public static final EXTRA_PROJECT_ID:Ljava/lang/String; = "projectId"

.field public static final EXTRA_SESSION_ID:Ljava/lang/String; = "sessionId"

.field public static final EXTRA_SPINE:Ljava/lang/String; = "spine"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "title"

.field static final INCREMENT_TEST_ID:I = 0x64

.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"

.field static testNum:I


# instance fields
.field private mNotificationManager:Landroid/app/NotificationManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput v0, Lcom/blurb/checkout/service/BookService;->testNum:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    const-string v0, "BlurbBookService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method private getFailureNotifyIntent(Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;Z)Landroid/app/PendingIntent;
    .locals 5
    .param p1, "bookIdString"    # Ljava/lang/String;
    .param p2, "info"    # Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    .param p3, "completed"    # Z

    .prologue
    .line 142
    if-nez p2, :cond_0

    .line 143
    const-string v3, "BlurbShadowApp"

    const-string v4, "getFailureNotifyIntent: No order info!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    const/4 v2, 0x0

    .line 165
    :goto_0
    return-object v2

    .line 148
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/blurb/checkout/UnrecoverableActivity;

    invoke-direct {v1, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 151
    .local v1, "notifyIntent":Landroid/content/Intent;
    const v3, 0x10008000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 152
    const-string v3, "orderInfo"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 154
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 157
    .local v0, "bookId":I
    const/high16 v3, 0x8000000

    invoke-static {p0, v0, v1, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 165
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    goto :goto_0
.end method

.method private notifyFailure(Ljava/lang/String;Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V
    .locals 6
    .param p1, "bookIdString"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "info"    # Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .prologue
    const/4 v5, 0x0

    .line 169
    iget-object v2, p0, Lcom/blurb/checkout/service/BookService;->mNotificationManager:Landroid/app/NotificationManager;

    if-nez v2, :cond_0

    .line 170
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/service/BookService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    iput-object v2, p0, Lcom/blurb/checkout/service/BookService;->mNotificationManager:Landroid/app/NotificationManager;

    .line 172
    :cond_0
    new-instance v2, Landroid/app/Notification$Builder;

    invoke-direct {v2, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f020002

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f0400d7

    invoke-virtual {p0, v3}, Lcom/blurb/checkout/service/BookService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    const v3, 0x7f0400d8

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/blurb/checkout/service/BookService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 178
    .local v0, "failBuilder":Landroid/app/Notification$Builder;
    const/4 v1, 0x0

    .line 179
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    if-eqz p3, :cond_1

    .line 180
    invoke-direct {p0, p1, p3, v5}, Lcom/blurb/checkout/service/BookService;->getFailureNotifyIntent(Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;Z)Landroid/app/PendingIntent;

    move-result-object v1

    .line 182
    :cond_1
    if-eqz v1, :cond_2

    .line 183
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 185
    :cond_2
    iget-object v2, p0, Lcom/blurb/checkout/service/BookService;->mNotificationManager:Landroid/app/NotificationManager;

    invoke-virtual {v0}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v2, p1, v5, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 189
    if-eqz p3, :cond_3

    .line 190
    const/high16 v2, -0x40000000    # -2.0f

    invoke-direct {p0, p1, v2}, Lcom/blurb/checkout/service/BookService;->sendUploadProgressBroadcast(Ljava/lang/String;F)V

    .line 193
    :goto_0
    return-void

    .line 192
    :cond_3
    const/high16 v2, -0x40800000    # -1.0f

    invoke-direct {p0, p1, v2}, Lcom/blurb/checkout/service/BookService;->sendUploadProgressBroadcast(Ljava/lang/String;F)V

    goto :goto_0
.end method

.method private sendUploadProgressBroadcast(Ljava/lang/String;F)V
    .locals 2
    .param p1, "bookId"    # Ljava/lang/String;
    .param p2, "progress"    # F

    .prologue
    .line 196
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/blurb/checkout/UploadProgressReceiver;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 198
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "bookId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const-string v1, "progress"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    .line 201
    invoke-virtual {p0, v0}, Lcom/blurb/checkout/service/BookService;->sendBroadcast(Landroid/content/Intent;)V

    .line 202
    return-void
.end method

.method private startUpload()V
    .locals 2

    .prologue
    .line 206
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/blurb/checkout/service/UploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 208
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.blurb.checkout.service.UPLOAD_BOOKS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 210
    invoke-virtual {p0, v0}, Lcom/blurb/checkout/service/BookService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 211
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 15
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 50
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v11

    .line 52
    .local v11, "action":Ljava/lang/String;
    const-string v14, "com.blurb.checkout.service.PUT_BOOK"

    invoke-virtual {v14, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 53
    new-instance v0, Lcom/blurb/checkout/service/BlurbManager;

    invoke-direct {v0, p0}, Lcom/blurb/checkout/service/BlurbManager;-><init>(Landroid/content/Context;)V

    .line 55
    .local v0, "mgr":Lcom/blurb/checkout/service/BlurbManager;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v12

    .line 57
    .local v12, "bundle":Landroid/os/Bundle;
    const-string v14, "bookId"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    .local v1, "bookId":Ljava/lang/String;
    const-string v14, "sessionId"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "sessionId":Ljava/lang/String;
    const-string v14, "projectId"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 60
    .local v3, "projectId":Ljava/lang/String;
    const-string v14, "episodeId"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 61
    .local v4, "episodeId":Ljava/lang/String;
    const-string v14, "coverType"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 62
    .local v6, "coverType":Ljava/lang/String;
    const-string v14, "frontCover"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 63
    .local v7, "frontCover":Ljava/lang/String;
    const-string v14, "backCover"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 64
    .local v8, "backCover":Ljava/lang/String;
    const-string v14, "spine"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 65
    .local v10, "spine":Ljava/lang/String;
    const-string v14, "title"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 66
    .local v5, "title":Ljava/lang/String;
    const-string v14, "photos"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 67
    .local v9, "photos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v14, "orderInfo"

    invoke-virtual {v12, v14}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v13

    check-cast v13, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .line 69
    .local v13, "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    invoke-virtual/range {v0 .. v10}, Lcom/blurb/checkout/service/BlurbManager;->writeBook(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_2

    .line 70
    invoke-direct {p0, v1, v5, v13}, Lcom/blurb/checkout/service/BookService;->notifyFailure(Ljava/lang/String;Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V

    .line 75
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/blurb/checkout/service/BlurbManager;->close()V

    .line 80
    invoke-direct {p0}, Lcom/blurb/checkout/service/BookService;->startUpload()V

    .line 109
    .end local v0    # "mgr":Lcom/blurb/checkout/service/BlurbManager;
    .end local v1    # "bookId":Ljava/lang/String;
    .end local v2    # "sessionId":Ljava/lang/String;
    .end local v3    # "projectId":Ljava/lang/String;
    .end local v4    # "episodeId":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "coverType":Ljava/lang/String;
    .end local v7    # "frontCover":Ljava/lang/String;
    .end local v8    # "backCover":Ljava/lang/String;
    .end local v9    # "photos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v10    # "spine":Ljava/lang/String;
    .end local v12    # "bundle":Landroid/os/Bundle;
    .end local v13    # "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    :cond_1
    return-void

    .line 71
    .restart local v0    # "mgr":Lcom/blurb/checkout/service/BlurbManager;
    .restart local v1    # "bookId":Ljava/lang/String;
    .restart local v2    # "sessionId":Ljava/lang/String;
    .restart local v3    # "projectId":Ljava/lang/String;
    .restart local v4    # "episodeId":Ljava/lang/String;
    .restart local v5    # "title":Ljava/lang/String;
    .restart local v6    # "coverType":Ljava/lang/String;
    .restart local v7    # "frontCover":Ljava/lang/String;
    .restart local v8    # "backCover":Ljava/lang/String;
    .restart local v9    # "photos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v10    # "spine":Ljava/lang/String;
    .restart local v12    # "bundle":Landroid/os/Bundle;
    .restart local v13    # "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    :cond_2
    invoke-virtual {v0, v13}, Lcom/blurb/checkout/service/BlurbManager;->writeOrderInfo(Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 72
    invoke-direct {p0, v1, v5, v13}, Lcom/blurb/checkout/service/BookService;->notifyFailure(Ljava/lang/String;Ljava/lang/String;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V

    goto :goto_0
.end method
