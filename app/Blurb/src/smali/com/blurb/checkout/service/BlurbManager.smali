.class public Lcom/blurb/checkout/service/BlurbManager;
.super Ljava/lang/Object;
.source "BlurbManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/service/BlurbManager$1;,
        Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;,
        Lcom/blurb/checkout/service/BlurbManager$OrderInfo;,
        Lcom/blurb/checkout/service/BlurbManager$BookComplete;,
        Lcom/blurb/checkout/service/BlurbManager$Book;,
        Lcom/blurb/checkout/service/BlurbManager$Photo;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final INDEX_BACK_COVER:I = -0x2

.field private static final INDEX_FRONT_COVER:I = -0x1

.field private static final INDEX_SPINE:I = -0x3

.field private static final PK_INVALID:I = -0x1

.field private static final TAG:Ljava/lang/String; = "BlurbManager"


# instance fields
.field private mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-direct {v0, p1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    .line 37
    return-void
.end method


# virtual methods
.method close()V
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->close()V

    .line 942
    return-void
.end method

.method deleteBook(Lcom/blurb/checkout/service/BlurbManager$Book;)V
    .locals 1
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$Book;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 947
    iget-object v0, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v0, p1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->deleteBook(Lcom/blurb/checkout/service/BlurbManager$Book;)Z

    .line 948
    return-void
.end method

.method deleteOrderInfo(Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .prologue
    .line 951
    iget-object v0, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v0, p1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->deleteOrderInfo(Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)Z

    .line 952
    return-void
.end method

.method getOldestBook(I)Lcom/blurb/checkout/service/BlurbManager$BookComplete;
    .locals 3
    .param p1, "counter"    # I

    .prologue
    .line 919
    iget-object v2, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v2, p1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getOldestBook(I)Lcom/blurb/checkout/service/BlurbManager$Book;

    move-result-object v0

    .line 920
    .local v0, "book":Lcom/blurb/checkout/service/BlurbManager$Book;
    if-eqz v0, :cond_0

    .line 921
    iget-object v2, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v2, v0}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getBookPhotos(Lcom/blurb/checkout/service/BlurbManager$Book;)Lcom/blurb/checkout/service/BlurbManager$BookComplete;

    move-result-object v1

    .line 925
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getOrderInfo(Lcom/blurb/checkout/service/BlurbManager$BookComplete;)Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    .locals 2
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$BookComplete;

    .prologue
    .line 913
    iget-object v1, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v1, p1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getOrderInfo(Lcom/blurb/checkout/service/BlurbManager$Book;)Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    move-result-object v0

    .line 915
    .local v0, "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    return-object v0
.end method

.method setPhotoUploaded(Lcom/blurb/checkout/service/BlurbManager$Book;Lcom/blurb/checkout/service/BlurbManager$Photo;Ljava/lang/String;)V
    .locals 2
    .param p1, "book"    # Lcom/blurb/checkout/service/BlurbManager$Book;
    .param p2, "photo"    # Lcom/blurb/checkout/service/BlurbManager$Photo;
    .param p3, "photoId"    # Ljava/lang/String;

    .prologue
    .line 929
    iget-object v1, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 931
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    iput-object p3, p2, Lcom/blurb/checkout/service/BlurbManager$Photo;->blurbId:Ljava/lang/String;

    .line 932
    iget-object v1, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v1, v0, p2}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->updatePhotoBlurbId(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Photo;)Z

    .line 934
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p1, Lcom/blurb/checkout/service/BlurbManager$Book;->startDate:Ljava/util/Date;

    .line 935
    iget-object v1, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v1, v0, p1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->updateBookDate(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Book;)Z

    .line 937
    invoke-virtual {p0}, Lcom/blurb/checkout/service/BlurbManager;->close()V

    .line 938
    return-void
.end method

.method writeBook(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)Z
    .locals 10
    .param p1, "bookId"    # Ljava/lang/String;
    .param p2, "sessionId"    # Ljava/lang/String;
    .param p3, "projectId"    # Ljava/lang/String;
    .param p4, "episodeId"    # Ljava/lang/String;
    .param p5, "title"    # Ljava/lang/String;
    .param p6, "coverType"    # Ljava/lang/String;
    .param p7, "frontCover"    # Ljava/lang/String;
    .param p8, "backCover"    # Ljava/lang/String;
    .param p10, "spine"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 789
    .local p9, "photos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    if-eqz p5, :cond_0

    if-eqz p6, :cond_0

    if-eqz p7, :cond_0

    if-eqz p8, :cond_0

    if-nez p9, :cond_a

    .line 792
    :cond_0
    if-nez p1, :cond_1

    .line 793
    const-string v7, "BlurbManager"

    const-string v8, "BookService.writeBook received bookId == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    :cond_1
    if-nez p2, :cond_2

    .line 795
    const-string v7, "BlurbManager"

    const-string v8, "BookService.writeBook received sessionId == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    :cond_2
    if-nez p3, :cond_3

    .line 797
    const-string v7, "BlurbManager"

    const-string v8, "BookService.writeBook received projectId == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 798
    :cond_3
    if-nez p4, :cond_4

    .line 799
    const-string v7, "BlurbManager"

    const-string v8, "BookService.writeBook received episodeId == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 800
    :cond_4
    if-nez p5, :cond_5

    .line 801
    const-string v7, "BlurbManager"

    const-string v8, "BookService.writeBook received title == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    :cond_5
    if-nez p6, :cond_6

    .line 803
    const-string v7, "BlurbManager"

    const-string v8, "BookService.writeBook received coverType == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 804
    :cond_6
    if-nez p7, :cond_7

    .line 805
    const-string v7, "BlurbManager"

    const-string v8, "BookService.writeBook received frontCover == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 806
    :cond_7
    if-nez p8, :cond_8

    .line 807
    const-string v7, "BlurbManager"

    const-string v8, "BookService.writeBook received backCover == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    :cond_8
    if-nez p9, :cond_9

    .line 809
    const-string v7, "BlurbManager"

    const-string v8, "BookService.writeBook received photos == null"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    :cond_9
    const/4 v7, 0x0

    .line 909
    :goto_0
    return v7

    .line 814
    :cond_a
    iget-object v7, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v7}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 817
    .local v2, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 819
    new-instance v1, Lcom/blurb/checkout/service/BlurbManager$Book;

    invoke-direct {v1}, Lcom/blurb/checkout/service/BlurbManager$Book;-><init>()V

    .line 820
    .local v1, "book":Lcom/blurb/checkout/service/BlurbManager$Book;
    iput-object p1, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->bookId:Ljava/lang/String;

    .line 821
    iput-object p2, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->sessionId:Ljava/lang/String;

    .line 822
    iput-object p3, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->projectId:Ljava/lang/String;

    .line 823
    iput-object p4, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->episodeId:Ljava/lang/String;

    .line 824
    move-object/from16 v0, p6

    iput-object v0, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->coverType:Ljava/lang/String;

    .line 825
    iput-object p5, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->title:Ljava/lang/String;

    .line 826
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    iput-object v7, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->startDate:Ljava/util/Date;

    .line 833
    iget-object v7, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v7, p1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->deleteBookByBookId(Ljava/lang/String;)Z

    .line 835
    iget-object v7, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v7, v2, v1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->insertBook(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Book;)Z

    move-result v5

    .line 837
    .local v5, "insertSuccess":Z
    if-nez v5, :cond_b

    .line 838
    const-string v7, "BlurbManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Book could not be added to database!!!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 839
    const/4 v7, 0x0

    .line 906
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .line 844
    :cond_b
    if-eqz p7, :cond_c

    :try_start_1
    invoke-virtual/range {p7 .. p7}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_c

    .line 845
    new-instance v6, Lcom/blurb/checkout/service/BlurbManager$Photo;

    invoke-direct {v6}, Lcom/blurb/checkout/service/BlurbManager$Photo;-><init>()V

    .line 846
    .local v6, "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    iget-wide v7, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    iput-wide v7, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->bookPK:J

    .line 847
    const/4 v7, -0x1

    iput v7, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->index:I

    .line 848
    move-object/from16 v0, p7

    iput-object v0, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    .line 850
    iget-object v7, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v7, v2, v6}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->insertPhoto(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Photo;)Z

    move-result v5

    .line 852
    if-nez v5, :cond_c

    .line 853
    const-string v7, "BlurbManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Photo could not be added to database!!!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 854
    const/4 v7, 0x0

    .line 906
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 858
    .end local v6    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    :cond_c
    if-eqz p8, :cond_d

    :try_start_2
    invoke-virtual/range {p8 .. p8}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_d

    .line 859
    new-instance v6, Lcom/blurb/checkout/service/BlurbManager$Photo;

    invoke-direct {v6}, Lcom/blurb/checkout/service/BlurbManager$Photo;-><init>()V

    .line 860
    .restart local v6    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    iget-wide v7, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    iput-wide v7, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->bookPK:J

    .line 861
    const/4 v7, -0x2

    iput v7, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->index:I

    .line 862
    move-object/from16 v0, p8

    iput-object v0, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    .line 864
    iget-object v7, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v7, v2, v6}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->insertPhoto(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Photo;)Z

    move-result v5

    .line 866
    if-nez v5, :cond_d

    .line 867
    const-string v7, "BlurbManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Photo could not be added to database!!!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 868
    const/4 v7, 0x0

    .line 906
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 872
    .end local v6    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    :cond_d
    if-eqz p10, :cond_e

    :try_start_3
    invoke-virtual/range {p10 .. p10}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_e

    .line 873
    new-instance v6, Lcom/blurb/checkout/service/BlurbManager$Photo;

    invoke-direct {v6}, Lcom/blurb/checkout/service/BlurbManager$Photo;-><init>()V

    .line 874
    .restart local v6    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    iget-wide v7, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    iput-wide v7, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->bookPK:J

    .line 875
    const/4 v7, -0x3

    iput v7, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->index:I

    .line 876
    move-object/from16 v0, p10

    iput-object v0, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    .line 878
    iget-object v7, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v7, v2, v6}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->insertPhoto(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Photo;)Z

    move-result v5

    .line 880
    if-nez v5, :cond_e

    .line 881
    const-string v7, "BlurbManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Photo could not be added to database!!!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 882
    const/4 v7, 0x0

    .line 906
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 886
    .end local v6    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    :cond_e
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    :try_start_4
    invoke-virtual/range {p9 .. p9}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v4, v7, :cond_10

    .line 887
    new-instance v6, Lcom/blurb/checkout/service/BlurbManager$Photo;

    invoke-direct {v6}, Lcom/blurb/checkout/service/BlurbManager$Photo;-><init>()V

    .line 888
    .restart local v6    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    iget-wide v7, v1, Lcom/blurb/checkout/service/BlurbManager$Book;->pk:J

    iput-wide v7, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->bookPK:J

    .line 889
    iput v4, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->index:I

    .line 890
    move-object/from16 v0, p9

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    iput-object v7, v6, Lcom/blurb/checkout/service/BlurbManager$Photo;->url:Ljava/lang/String;

    .line 892
    iget-object v7, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v7, v2, v6}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->insertPhoto(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$Photo;)Z

    move-result v5

    .line 894
    if-nez v5, :cond_f

    .line 895
    const-string v7, "BlurbManager"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Photo could not be added to database!!!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 896
    const/4 v7, 0x0

    .line 906
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 886
    :cond_f
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 900
    .end local v6    # "photo":Lcom/blurb/checkout/service/BlurbManager$Photo;
    :cond_10
    :try_start_5
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 902
    const/4 v7, 0x1

    .line 906
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 903
    .end local v1    # "book":Lcom/blurb/checkout/service/BlurbManager$Book;
    .end local v4    # "i":I
    .end local v5    # "insertSuccess":Z
    :catch_0
    move-exception v3

    .line 904
    .local v3, "e":Ljava/lang/Exception;
    :try_start_6
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 906
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 909
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 906
    .end local v3    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v7
.end method

.method writeOrderInfo(Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)Z
    .locals 6
    .param p1, "info"    # Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .prologue
    const/4 v2, 0x0

    .line 761
    if-nez p1, :cond_0

    .line 783
    :goto_0
    return v2

    .line 764
    :cond_0
    iget-object v3, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v3}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 767
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 769
    iget-object v3, p0, Lcom/blurb/checkout/service/BlurbManager;->mDatabaseHelper:Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;

    invoke-virtual {v3, v0, p1}, Lcom/blurb/checkout/service/BlurbManager$DatabaseHelper;->insertOrderInfo(Landroid/database/sqlite/SQLiteDatabase;Lcom/blurb/checkout/service/BlurbManager$OrderInfo;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 770
    const-string v3, "BlurbManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Order info could not be added to database!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p1, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 780
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .line 774
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 776
    const/4 v2, 0x1

    .line 780
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .line 777
    :catch_0
    move-exception v1

    .line 778
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 780
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method
