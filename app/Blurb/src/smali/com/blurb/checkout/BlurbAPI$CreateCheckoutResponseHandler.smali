.class Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResponseHandler;
.super Ljava/lang/Object;
.source "BlurbAPI.java"

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CreateCheckoutResponseHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 446
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 447
    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;
    .locals 1
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 450
    new-instance v0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;-><init>()V

    .line 452
    .local v0, "result":Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;
    invoke-virtual {v0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->setHttpResponse(Lorg/apache/http/HttpResponse;)V

    .line 454
    return-object v0
.end method

.method public bridge synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 444
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    move-result-object v0

    return-object v0
.end method
