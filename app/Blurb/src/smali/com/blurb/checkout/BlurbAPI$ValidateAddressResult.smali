.class Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;
.super Lcom/blurb/checkout/BlurbAPI$BlurbResult;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ValidateAddressResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1910
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 1911
    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 1913
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 1914
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->setHttpStatus(I)V

    .line 1915
    return-void
.end method


# virtual methods
.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 7
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 1918
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->setHttpStatus(I)V

    .line 1923
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 1924
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 1925
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->setHttpStatus(I)V

    .line 1951
    :goto_0
    return-void

    .line 1929
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_1

    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0x190

    if-ne v5, v6, :cond_2

    .line 1931
    :cond_1
    :try_start_0
    const-string v5, "UTF-8"

    invoke-static {v1, v5}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1932
    .local v4, "responseString":Ljava/lang/String;
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1933
    .local v3, "reader":Ljava/io/StringReader;
    new-instance v2, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;

    const/4 v5, 0x0

    invoke-direct {v2, v5}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$1;)V

    .line 1935
    .local v2, "parser":Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;
    # invokes: Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;->parseXml(Ljava/io/Reader;)V
    invoke-static {v2, v3}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;->access$2100(Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;Ljava/io/Reader;)V

    .line 1936
    invoke-virtual {v2}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;->getErrors()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->errors:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1947
    .end local v2    # "parser":Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;
    .end local v3    # "reader":Ljava/io/StringReader;
    .end local v4    # "responseString":Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1948
    :catch_0
    move-exception v0

    .line 1949
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1937
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1938
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v5, -0x2

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->setHttpStatus(I)V

    .line 1939
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1940
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 1941
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 1942
    const/4 v5, -0x4

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->setHttpStatus(I)V

    goto :goto_1
.end method
