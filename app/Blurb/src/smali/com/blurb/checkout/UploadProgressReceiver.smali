.class public Lcom/blurb/checkout/UploadProgressReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UploadProgressReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/UploadProgressReceiver$Progress;,
        Lcom/blurb/checkout/UploadProgressReceiver$IProgress;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"

.field static progressMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/blurb/checkout/UploadProgressReceiver$Progress;",
            ">;"
        }
    .end annotation
.end field

.field static wantsProgress:Lcom/blurb/checkout/UploadProgressReceiver$IProgress;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/blurb/checkout/UploadProgressReceiver;->progressMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 22
    return-void
.end method

.method public static forgetProgress(Ljava/lang/String;)V
    .locals 1
    .param p0, "bookId"    # Ljava/lang/String;

    .prologue
    .line 51
    sget-object v0, Lcom/blurb/checkout/UploadProgressReceiver;->progressMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method

.method static getProgress(Ljava/lang/String;)Lcom/blurb/checkout/UploadProgressReceiver$Progress;
    .locals 1
    .param p0, "bookId"    # Ljava/lang/String;

    .prologue
    .line 47
    sget-object v0, Lcom/blurb/checkout/UploadProgressReceiver;->progressMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/blurb/checkout/UploadProgressReceiver$Progress;

    return-object v0
.end method

.method static registerForProgress(Lcom/blurb/checkout/UploadProgressReceiver$IProgress;)V
    .locals 0
    .param p0, "p"    # Lcom/blurb/checkout/UploadProgressReceiver$IProgress;

    .prologue
    .line 55
    sput-object p0, Lcom/blurb/checkout/UploadProgressReceiver;->wantsProgress:Lcom/blurb/checkout/UploadProgressReceiver$IProgress;

    .line 56
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 31
    const-string v2, "bookId"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "progressBookId":Ljava/lang/String;
    new-instance v0, Lcom/blurb/checkout/UploadProgressReceiver$Progress;

    invoke-direct {v0}, Lcom/blurb/checkout/UploadProgressReceiver$Progress;-><init>()V

    .line 33
    .local v0, "progress":Lcom/blurb/checkout/UploadProgressReceiver$Progress;
    const-string v2, "progress"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v2

    iput v2, v0, Lcom/blurb/checkout/UploadProgressReceiver$Progress;->progress:F

    .line 34
    const-string v2, "currentStep"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Lcom/blurb/checkout/UploadProgressReceiver$Progress;->currentStep:I

    .line 35
    const-string v2, "totalSteps"

    invoke-virtual {p2, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, v0, Lcom/blurb/checkout/UploadProgressReceiver$Progress;->totalSteps:I

    .line 40
    sget-object v2, Lcom/blurb/checkout/UploadProgressReceiver;->wantsProgress:Lcom/blurb/checkout/UploadProgressReceiver$IProgress;

    if-eqz v2, :cond_0

    .line 41
    sget-object v2, Lcom/blurb/checkout/UploadProgressReceiver;->wantsProgress:Lcom/blurb/checkout/UploadProgressReceiver$IProgress;

    invoke-interface {v2, v1, v0}, Lcom/blurb/checkout/UploadProgressReceiver$IProgress;->onUploadProgress(Ljava/lang/String;Lcom/blurb/checkout/UploadProgressReceiver$Progress;)V

    .line 43
    :cond_0
    sget-object v2, Lcom/blurb/checkout/UploadProgressReceiver;->progressMap:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-void
.end method
