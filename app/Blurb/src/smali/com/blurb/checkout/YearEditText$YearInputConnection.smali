.class Lcom/blurb/checkout/YearEditText$YearInputConnection;
.super Landroid/view/inputmethod/InputConnectionWrapper;
.source "YearEditText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/YearEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "YearInputConnection"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/YearEditText;


# direct methods
.method public constructor <init>(Lcom/blurb/checkout/YearEditText;Landroid/view/inputmethod/InputConnection;Z)V
    .locals 0
    .param p2, "target"    # Landroid/view/inputmethod/InputConnection;
    .param p3, "mutable"    # Z

    .prologue
    .line 38
    iput-object p1, p0, Lcom/blurb/checkout/YearEditText$YearInputConnection;->this$0:Lcom/blurb/checkout/YearEditText;

    .line 39
    invoke-direct {p0, p2, p3}, Landroid/view/inputmethod/InputConnectionWrapper;-><init>(Landroid/view/inputmethod/InputConnection;Z)V

    .line 40
    return-void
.end method


# virtual methods
.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 44
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x43

    if-ne v0, v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/blurb/checkout/YearEditText$YearInputConnection;->this$0:Lcom/blurb/checkout/YearEditText;

    invoke-virtual {v0}, Lcom/blurb/checkout/YearEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/blurb/checkout/YearEditText$YearInputConnection;->this$0:Lcom/blurb/checkout/YearEditText;

    # getter for: Lcom/blurb/checkout/YearEditText;->monthEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/blurb/checkout/YearEditText;->access$000(Lcom/blurb/checkout/YearEditText;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/blurb/checkout/YearEditText$YearInputConnection;->this$0:Lcom/blurb/checkout/YearEditText;

    # getter for: Lcom/blurb/checkout/YearEditText;->monthEditText:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/blurb/checkout/YearEditText;->access$000(Lcom/blurb/checkout/YearEditText;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 48
    :cond_0
    invoke-super {p0, p1}, Landroid/view/inputmethod/InputConnectionWrapper;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
