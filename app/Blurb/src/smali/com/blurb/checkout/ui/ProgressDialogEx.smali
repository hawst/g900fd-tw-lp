.class public Lcom/blurb/checkout/ui/ProgressDialogEx;
.super Landroid/app/ProgressDialog;
.source "ProgressDialogEx.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 12
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 16
    invoke-super {p0, p1}, Landroid/app/ProgressDialog;->onCreate(Landroid/os/Bundle;)V

    .line 18
    const v1, 0x102000b

    invoke-virtual {p0, v1}, Lcom/blurb/checkout/ui/ProgressDialogEx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 19
    .local v0, "view":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 20
    const/4 v1, 0x2

    const/high16 v2, 0x41980000    # 19.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 22
    :cond_0
    return-void
.end method
