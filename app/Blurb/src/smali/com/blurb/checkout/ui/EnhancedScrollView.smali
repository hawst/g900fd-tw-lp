.class public Lcom/blurb/checkout/ui/EnhancedScrollView;
.super Landroid/widget/ScrollView;
.source "EnhancedScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;
    }
.end annotation


# instance fields
.field private mOnScrollListener:Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

.field private mScrolling:Z

.field private mTouching:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/blurb/checkout/ui/EnhancedScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/blurb/checkout/ui/EnhancedScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    iput-boolean v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mTouching:Z

    .line 16
    iput-boolean v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mScrolling:Z

    .line 29
    return-void
.end method


# virtual methods
.method public getOnScrollListener()Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mOnScrollListener:Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

    return-object v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 55
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 63
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 57
    :pswitch_0
    iget-object v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mOnScrollListener:Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mOnScrollListener:Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

    invoke-interface {v0}, Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;->onStartScroll()V

    .line 60
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mTouching:Z

    goto :goto_0

    .line 55
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onScrollChanged(IIII)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "oldX"    # I
    .param p4, "oldY"    # I

    .prologue
    .line 34
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 35
    sub-int v0, p2, p4

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/blurb/checkout/ui/EnhancedScrollView;->getMeasuredHeight()I

    move-result v0

    if-ge p2, v0, :cond_0

    if-nez p2, :cond_2

    .line 36
    :cond_0
    iget-boolean v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mScrolling:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mTouching:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mOnScrollListener:Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mOnScrollListener:Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

    invoke-interface {v0}, Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;->onEndScroll()V

    .line 39
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mScrolling:Z

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mScrolling:Z

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 78
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 71
    :pswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mTouching:Z

    .line 72
    iget-boolean v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mScrolling:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mOnScrollListener:Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mOnScrollListener:Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

    invoke-interface {v0}, Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;->onEndScroll()V

    goto :goto_0

    .line 68
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnScrollListener(Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;)V
    .locals 0
    .param p1, "mOnEndScrollListener"    # Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/blurb/checkout/ui/EnhancedScrollView;->mOnScrollListener:Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;

    .line 51
    return-void
.end method
