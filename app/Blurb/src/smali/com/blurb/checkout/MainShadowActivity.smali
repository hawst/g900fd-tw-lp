.class public Lcom/blurb/checkout/MainShadowActivity;
.super Landroid/app/Activity;
.source "MainShadowActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ParserError"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;,
        Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;,
        Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;,
        Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;,
        Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;,
        Lcom/blurb/checkout/MainShadowActivity$Country;
    }
.end annotation


# static fields
.field private static final CLIENT_ID:Ljava/lang/String; = "2m3m68snwb"

.field private static final CLIENT_SECRET:Ljava/lang/String; = "5006774DDEBAA634A24424284B61A114"

.field static final COUNTRY_CODE_AU:Ljava/lang/String; = "au"

.field static final COUNTRY_CODE_CA:Ljava/lang/String; = "ca"

.field static final COUNTRY_CODE_JP:Ljava/lang/String; = "jp"

.field static final COUNTRY_CODE_KR:Ljava/lang/String; = "kr"

.field static final COUNTRY_CODE_US2:Ljava/lang/String; = "us"

.field static final COUNTRY_CODE_US3:Ljava/lang/String; = "usa"

.field private static final DIALOG_CHOOSE_COUNTRY:I = 0x4

.field private static final DIALOG_SERVICE_UNAVAILABLE:I = 0x3

.field private static final DIALOG_SSO_CANCELED:I = 0x1

.field private static final DIALOG_SSO_FAILED:I = 0x2

.field private static final ENABLE_LOG:Z = false

.field public static final EXTRA_BILLING_ADDRESS:Ljava/lang/String; = "billingAddress"

.field public static final EXTRA_BOOK_ID:Ljava/lang/String; = "bookId"

.field public static final EXTRA_CITY:Ljava/lang/String; = "cityEditText"

.field public static final EXTRA_COUNTRY_CODE:Ljava/lang/String; = "country"

.field public static final EXTRA_COVER_BITMAP:Ljava/lang/String; = "cover_bitmap"

.field public static final EXTRA_COVER_DATA:Ljava/lang/String; = "cover_data"

.field public static final EXTRA_EMAIL:Ljava/lang/String; = "email"

.field public static final EXTRA_FIRST_NAME:Ljava/lang/String; = "firstName"

.field public static final EXTRA_LAST_NAME:Ljava/lang/String; = "lastName"

.field public static final EXTRA_ORDER_CHECKOUT:Ljava/lang/String; = "orderCheckout"

.field public static final EXTRA_ORDER_ID:Ljava/lang/String; = "orderId"

.field public static final EXTRA_ORDER_INFO:Ljava/lang/String; = "orderInfo"

.field public static final EXTRA_PHONE:Ljava/lang/String; = "phone"

.field public static final EXTRA_POSTAL_CODE:Ljava/lang/String; = "postalCode"

.field public static final EXTRA_QUANTITY:Ljava/lang/String; = "quantity"

.field public static final EXTRA_SESSION_ID:Ljava/lang/String; = "sessionId"

.field public static final EXTRA_STATE:Ljava/lang/String; = "stateEditText"

.field public static final EXTRA_STREET_ADDRESS:Ljava/lang/String; = "streetAddress1"

.field public static final EXTRA_STREET_ADDRESS2:Ljava/lang/String; = "streetAddress2"

.field public static final EXTRA_TOTAL_PRICE:Ljava/lang/String; = "total_price"

.field public static final EXTRA_UPLOAD_COMPLETED:Ljava/lang/String; = "uploadCompleted"

.field private static final FIELD_CITY:I = 0x3

.field private static final FIELD_COUNTRY:I = 0x5

.field private static final FIELD_FIRST_NAME:I = 0x9

.field private static final FIELD_LAST_NAME:I = 0xa

.field private static final FIELD_PHONE:I = 0x7

.field private static final FIELD_PO_BOX_OR_APO:I = 0x8

.field private static final FIELD_STATE_ID:I = 0x6

.field private static final FIELD_STREET1:I = 0x1

.field private static final FIELD_STREET2:I = 0x2

.field private static final FIELD_ZIP:I = 0x4

.field public static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"

.field private static final REQUEST_GET_ACCESS_TOKEN:I = 0xc

.field private static final REQUEST_SHIPPING:I = 0xb

.field private static final REQUEST_SIGNIN:I = 0xa

.field private static final SSO_RESPONSE:Ljava/lang/String; = "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

.field public static countryClassAt:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static countryClassAu:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static countryClassCa:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static countryClassDe:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static countryClassGb:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static countryClassHk:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static countryClassIe:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static countryCodeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/blurb/checkout/MainShadowActivity$Country;",
            ">;"
        }
    .end annotation
.end field

.field private static jpZipCodePattern:Ljava/util/regex/Pattern;

.field private static krZipCodePattern:Ljava/util/regex/Pattern;


# instance fields
.field billingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

.field private bookType:Ljava/lang/String;

.field private cityEditText:Landroid/widget/EditText;

.field private countriesListView:Landroid/widget/ListView;

.field private countryChooser:Landroid/widget/ImageButton;

.field private countryCode:Ljava/lang/String;

.field private countryDialog:Landroid/app/Dialog;

.field private countryEditText:Landroid/widget/TextView;

.field private couponCode:Ljava/lang/String;

.field private coverData:[B

.field private coverType:Ljava/lang/String;

.field private currencyId:Ljava/lang/String;

.field private discountText:Landroid/widget/TextView;

.field private displayBookType:Landroid/widget/TextView;

.field private displayCoverUri:Landroid/widget/ImageView;

.field private displayNumPages:Landroid/widget/TextView;

.field private displayTitle:Landroid/widget/TextView;

.field private email:Ljava/lang/String;

.field private episodeId:Ljava/lang/String;

.field private familyNameEditText:Landroid/widget/EditText;

.field private firstNameEditText:Landroid/widget/EditText;

.field private mySSOTokenReceiver:Landroid/content/BroadcastReceiver;

.field private numPages:Ljava/lang/String;

.field private orderCountEditText:Landroid/widget/EditText;

.field private pageCount:I

.field private paperType:Ljava/lang/String;

.field private priceSubtotalView:Landroid/widget/TextView;

.field private quantity:Ljava/lang/String;

.field private sessionId:Ljava/lang/String;

.field private singlePrice:Ljava/lang/String;

.field private stateEditText:Landroid/widget/EditText;

.field private streetAddressEditText:Landroid/widget/EditText;

.field private title:Ljava/lang/String;

.field private zipCodeEditText:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 576
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    .line 578
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040002

    const-string v3, "ae"

    const-string v4, "are"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040003

    const-string v3, "ar"

    const-string v4, "arg"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040004

    const-string v3, "at"

    const-string v4, "aut"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 581
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040005

    const-string v3, "au"

    const-string v4, "aus"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 582
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040006

    const-string v3, "be"

    const-string v4, "bel"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040007

    const-string v3, "bg"

    const-string v4, "bgr"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 584
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040008

    const-string v3, "bm"

    const-string v4, "bmu"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 585
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040009

    const-string v3, "br"

    const-string v4, "bra"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 586
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04000a

    const-string v3, "ca"

    const-string v4, "can"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 587
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04000b

    const-string v3, "ch"

    const-string v4, "che"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04000c

    const-string v3, "cl"

    const-string v4, "chl"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 589
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04000d

    const-string v3, "co"

    const-string v4, "col"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 590
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04000e

    const-string v3, "cy"

    const-string v4, "cyp"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04000f

    const-string v3, "cz"

    const-string v4, "cze"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040010

    const-string v3, "de"

    const-string v4, "deu"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 593
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040011

    const-string v3, "dk"

    const-string v4, "dnk"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040012

    const-string v3, "ee"

    const-string v4, "est"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040013

    const-string v3, "es"

    const-string v4, "esp"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040014

    const-string v3, "fi"

    const-string v4, "fin"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040015

    const-string v3, "fm"

    const-string v4, "fsm"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040016

    const-string v3, "fr"

    const-string v4, "fra"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 599
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040017

    const-string v3, "gb"

    const-string v4, "gbr"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040018

    const-string v3, "gr"

    const-string v4, "grc"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040019

    const-string v3, "hk"

    const-string v4, "hkg"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 602
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04001a

    const-string v3, "hr"

    const-string v4, "hrv"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 603
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04001b

    const-string v3, "hu"

    const-string v4, "hun"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 604
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04001c

    const-string v3, "id"

    const-string v4, "idn"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 605
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04001d

    const-string v3, "ie"

    const-string v4, "irl"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04001e

    const-string v3, "il"

    const-string v4, "isr"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 607
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04001f

    const-string v3, "in"

    const-string v4, "ind"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 608
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040020

    const-string v3, "is"

    const-string v4, "isl"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 609
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040021

    const-string v3, "it"

    const-string v4, "ita"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 610
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040022

    const-string v3, "jm"

    const-string v4, "jam"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040023

    const-string v3, "jp"

    const-string v4, "jpn"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 612
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040024

    const-string v3, "kr"

    const-string v4, "kor"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 613
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040025

    const-string v3, "ky"

    const-string v4, "cym"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 614
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040026

    const-string v3, "lt"

    const-string v4, "ltu"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 615
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040027

    const-string v3, "lu"

    const-string v4, "lux"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 616
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040028

    const-string v3, "lv"

    const-string v4, "lva"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 617
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040029

    const-string v3, "mo"

    const-string v4, "mac"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 618
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04002a

    const-string v3, "mt"

    const-string v4, "mlt"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 619
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04002b

    const-string v3, "mx"

    const-string v4, "mex"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 620
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04002c

    const-string v3, "my"

    const-string v4, "mys"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04002d

    const-string v3, "nl"

    const-string v4, "nld"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 622
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04002e

    const-string v3, "no"

    const-string v4, "nor"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 623
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04002f

    const-string v3, "nz"

    const-string v4, "nzl"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 624
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040030

    const-string v3, "pe"

    const-string v4, "per"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 625
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040031

    const-string v3, "ph"

    const-string v4, "phl"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 626
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040032

    const-string v3, "pl"

    const-string v4, "pol"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 627
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040033

    const-string v3, "pt"

    const-string v4, "prt"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 628
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040034

    const-string v3, "py"

    const-string v4, "pry"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 629
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040035

    const-string v3, "ro"

    const-string v4, "rou"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 630
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040036

    const-string v3, "rs"

    const-string v4, "srb"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040037

    const-string v3, "ru"

    const-string v4, "rus"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040038

    const-string v3, "sa"

    const-string v4, "sau"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 633
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040039

    const-string v3, "se"

    const-string v4, "swe"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 634
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04003a

    const-string v3, "sg"

    const-string v4, "sgp"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04003b

    const-string v3, "si"

    const-string v4, "svn"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 636
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04003c

    const-string v3, "sk"

    const-string v4, "svk"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04003d

    const-string v3, "th"

    const-string v4, "tha"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 638
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04003e

    const-string v3, "tr"

    const-string v4, "tur"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 639
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f04003f

    const-string v3, "tw"

    const-string v4, "twn"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 640
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040040

    const-string v3, "ua"

    const-string v4, "ukr"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040041

    const-string v3, "us"

    const-string v4, "usa"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 642
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040042

    const-string v3, "uy"

    const-string v4, "ury"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 643
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040043

    const-string v3, "vg"

    const-string v4, "vgb"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 644
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040044

    const-string v3, "vn"

    const-string v4, "vnm"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 645
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$Country;

    const v2, 0x7f040045

    const-string v3, "za"

    const-string v4, "zaf"

    invoke-direct {v1, v2, v3, v4}, Lcom/blurb/checkout/MainShadowActivity$Country;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 686
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassAt:Ljava/util/Set;

    .line 687
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassAu:Ljava/util/Set;

    .line 688
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassIe:Ljava/util/Set;

    .line 689
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassCa:Ljava/util/Set;

    .line 690
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassGb:Ljava/util/Set;

    .line 691
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassHk:Ljava/util/Set;

    .line 692
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    .line 696
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassAt:Ljava/util/Set;

    const-string v1, "be"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 697
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassAt:Ljava/util/Set;

    const-string v1, "at"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 700
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassAu:Ljava/util/Set;

    const-string v1, "au"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 703
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassIe:Ljava/util/Set;

    const-string v1, "ie"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 704
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassIe:Ljava/util/Set;

    const-string v1, "lt"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 709
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassCa:Ljava/util/Set;

    const-string v1, "ca"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 712
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassGb:Ljava/util/Set;

    const-string v1, "gb"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 715
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassHk:Ljava/util/Set;

    const-string v1, "ae"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 716
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassHk:Ljava/util/Set;

    const-string v1, "co"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 717
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassHk:Ljava/util/Set;

    const-string v1, "jm"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 718
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassHk:Ljava/util/Set;

    const-string v1, "hk"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 719
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassHk:Ljava/util/Set;

    const-string v1, "mo"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 720
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassHk:Ljava/util/Set;

    const-string v1, "pe"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 725
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "deu"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 727
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "cze"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 728
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "dnk"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 729
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "esp"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 730
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "fin"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 731
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "fra"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 732
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "grc"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 733
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "hrv"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 734
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "hun"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 735
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "isl"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 736
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "ita"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 737
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "lux"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 738
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "nld"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 739
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "nor"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 740
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "pol"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 741
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "prt"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 742
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "rou"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 743
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "srb"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 744
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "swe"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 746
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "arg"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 747
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "bgr"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 748
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "bmu"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 749
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "bra"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 750
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "chl"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 751
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "cym"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 752
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "cyp"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 753
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "est"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 754
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "fsm"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 755
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "idn"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 756
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "ind"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 757
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "isr"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 758
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "lva"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 759
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "mex"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 760
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "mlt"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 761
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "mys"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 762
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "nzl"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 763
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "phl"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 764
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "pry"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 765
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "rus"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 766
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "sau"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 767
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "sgp"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 768
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "svk"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 769
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "svn"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 770
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "tha"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 771
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "tur"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 772
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "twn"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 773
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "ukr"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 774
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "ury"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 775
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "vgb"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 776
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "vnm"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 777
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    const-string v1, "zaf"

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 963
    const-string v0, "\\d{0,3}(?:-\\d{0,3})?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->krZipCodePattern:Ljava/util/regex/Pattern;

    .line 1029
    const-string v0, "\\d{0,3}(?:-\\d{0,4})?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/blurb/checkout/MainShadowActivity;->jpZipCodePattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 162
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->currencyId:Ljava/lang/String;

    .line 171
    const-string v0, "0.00"

    iput-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->singlePrice:Ljava/lang/String;

    .line 173
    const-string v0, "standard_paper"

    iput-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->paperType:Ljava/lang/String;

    .line 175
    new-instance v0, Lcom/blurb/checkout/MainShadowActivity$1;

    invoke-direct {v0, p0}, Lcom/blurb/checkout/MainShadowActivity$1;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    iput-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->mySSOTokenReceiver:Landroid/content/BroadcastReceiver;

    .line 2337
    return-void
.end method

.method static synthetic access$000(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/blurb/checkout/MainShadowActivity;->showError(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/blurb/checkout/MainShadowActivity;)D
    .locals 2
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->getOrderTotal()D

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->updateCountryFields()V

    return-void
.end method

.method static synthetic access$1200(Lcom/blurb/checkout/MainShadowActivity;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->countryDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/blurb/checkout/MainShadowActivity;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity;->countryDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/blurb/checkout/MainShadowActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget v0, p0, Lcom/blurb/checkout/MainShadowActivity;->pageCount:I

    return v0
.end method

.method static synthetic access$1402(Lcom/blurb/checkout/MainShadowActivity;I)I
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;
    .param p1, "x1"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/blurb/checkout/MainShadowActivity;->pageCount:I

    return p1
.end method

.method static synthetic access$1500(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->numPages:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->coverType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->bookType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->paperType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Ljava/lang/String;
    .param p5, "x5"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct/range {p0 .. p5}, Lcom/blurb/checkout/MainShadowActivity;->setUserInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->currencyId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity;->singlePrice:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->sessionId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity;->sessionId:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->setFirstFocus()V

    return-void
.end method

.method static synthetic access$2800(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->episodeId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->priceSubtotalView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/blurb/checkout/MainShadowActivity;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lcom/blurb/checkout/MainShadowActivity;->handleValidationError(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$3100(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->email:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/blurb/checkout/MainShadowActivity;)[B
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->coverData:[B

    return-object v0
.end method

.method static synthetic access$3500(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->quantity:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->couponCode:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->handleNewerVersionAvailable()V

    return-void
.end method

.method static synthetic access$3800(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->startSSOSignin()V

    return-void
.end method

.method static synthetic access$400(Lcom/blurb/checkout/MainShadowActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->getOrderCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->krZipCodePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$700()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/blurb/checkout/MainShadowActivity;->jpZipCodePattern:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic access$800(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->countriesListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/MainShadowActivity;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->countryEditText:Landroid/widget/TextView;

    return-object v0
.end method

.method public static calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 7
    .param p0, "options"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 486
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 487
    .local v0, "height":I
    iget v3, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 488
    .local v3, "width":I
    const/4 v2, 0x1

    .line 490
    .local v2, "inSampleSize":I
    if-gt v0, p2, :cond_0

    if-le v3, p1, :cond_1

    .line 494
    :cond_0
    int-to-float v5, v0

    int-to-float v6, p2

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 495
    .local v1, "heightRatio":I
    int-to-float v5, v3

    int-to-float v6, p1

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 500
    .local v4, "widthRatio":I
    if-ge v1, v4, :cond_2

    move v2, v1

    .line 503
    .end local v1    # "heightRatio":I
    .end local v4    # "widthRatio":I
    :cond_1
    :goto_0
    return v2

    .restart local v1    # "heightRatio":I
    .restart local v4    # "widthRatio":I
    :cond_2
    move v2, v4

    .line 500
    goto :goto_0
.end method

.method private clearValidationErrors()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1826
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1827
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1828
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1829
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->countryEditText:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 1830
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1831
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1832
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1834
    const-string v1, "kr"

    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1835
    const v1, 0x7f06000d

    invoke-virtual {p0, v1}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1836
    .local v0, "phoneEditText":Landroid/widget/EditText;
    if-eqz v0, :cond_0

    .line 1837
    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1841
    .end local v0    # "phoneEditText":Landroid/widget/EditText;
    :cond_0
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1842
    return-void
.end method

.method private static countryCode3to2(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "code3"    # Ljava/lang/String;

    .prologue
    .line 666
    sget-object v2, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/blurb/checkout/MainShadowActivity$Country;

    .line 667
    .local v0, "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$Country;->threeLetterCode:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 668
    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$Country;->twoLetterCode:Ljava/lang/String;

    .line 673
    .end local v0    # "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    :goto_0
    return-object v2

    .line 671
    :cond_1
    const-string v2, "BlurbShadowApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "countryCode3to2: unknown country code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    const-string v2, "us"

    goto :goto_0
.end method

.method public static decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 2
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 472
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 473
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 474
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 477
    invoke-static {v0, p1, p2}, Lcom/blurb/checkout/MainShadowActivity;->calculateInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v1

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 480
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 481
    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method public static getBookType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "coverType"    # Ljava/lang/String;
    .param p2, "bookType"    # Ljava/lang/String;

    .prologue
    .line 813
    const-string v1, "softcover"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "small_square"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 814
    const v0, 0x7f040099

    .line 826
    .local v0, "bid":I
    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 815
    .end local v0    # "bid":I
    :cond_0
    const-string v1, "softcover"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "square"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 816
    const v0, 0x7f04009a

    .restart local v0    # "bid":I
    goto :goto_0

    .line 817
    .end local v0    # "bid":I
    :cond_1
    const-string v1, "imagewrap"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "square"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 818
    const v0, 0x7f04009b

    .restart local v0    # "bid":I
    goto :goto_0

    .line 819
    .end local v0    # "bid":I
    :cond_2
    const-string v1, "softcover"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "magazine"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 820
    const v0, 0x7f0400b0

    .restart local v0    # "bid":I
    goto :goto_0

    .line 822
    .end local v0    # "bid":I
    :cond_3
    const-string v1, "BlurbShadowApp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBookType: unknown cover type="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bookType="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 823
    const v0, 0x7f040099

    .restart local v0    # "bid":I
    goto :goto_0
.end method

.method public static getCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "code2"    # Ljava/lang/String;

    .prologue
    .line 798
    sget-object v3, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/blurb/checkout/MainShadowActivity$Country;

    .line 799
    .local v0, "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    iget-object v3, v0, Lcom/blurb/checkout/MainShadowActivity$Country;->twoLetterCode:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 800
    iget v3, v0, Lcom/blurb/checkout/MainShadowActivity$Country;->countryId:I

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 807
    .end local v0    # "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    :goto_0
    return-object v2

    .line 805
    :cond_1
    const-string v3, "BlurbShadowApp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCountry: unknown country code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    const v3, 0x7f040041

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static getCountryCode(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "countryName"    # Ljava/lang/String;

    .prologue
    .line 786
    sget-object v2, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/blurb/checkout/MainShadowActivity$Country;

    .line 787
    .local v0, "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$Country;->myName:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 788
    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$Country;->twoLetterCode:Ljava/lang/String;

    .line 794
    .end local v0    # "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    :goto_0
    return-object v2

    .line 792
    :cond_1
    const-string v2, "BlurbShadowApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCountryCode: unknown country "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 794
    const-string v2, "us"

    goto :goto_0
.end method

.method private getOrderCount()I
    .locals 3

    .prologue
    .line 2099
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2100
    .local v1, "countText":Ljava/lang/String;
    const/4 v0, 0x0

    .line 2102
    .local v0, "count":I
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2106
    :goto_0
    return v0

    .line 2103
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private getOrderTotal()D
    .locals 6

    .prologue
    .line 2110
    const-wide/16 v0, 0x0

    .line 2112
    .local v0, "priceTotal":D
    :try_start_0
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->getOrderCount()I

    move-result v2

    int-to-double v2, v2

    iget-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->singlePrice:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    mul-double v0, v2, v4

    .line 2116
    :goto_0
    return-wide v0

    .line 2113
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private getSamsungAccountVersion()I
    .locals 6

    .prologue
    .line 1665
    :try_start_0
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1666
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const-string v4, "com.osp.app.signin"

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1667
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1674
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v3

    .line 1672
    :catch_0
    move-exception v0

    .line 1674
    .local v0, "e":Ljava/lang/Exception;
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getSoftwareVersion()Ljava/lang/String;
    .locals 5

    .prologue
    .line 210
    :try_start_0
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 212
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-object v2

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v2, "BlurbShadowApp"

    const-string v3, "Package name not found"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 217
    const-string v2, ""

    goto :goto_0
.end method

.method static getSortedCountries(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/blurb/checkout/MainShadowActivity$Country;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1494
    new-instance v2, Ljava/util/ArrayList;

    sget-object v6, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1495
    .local v2, "countriesSorted":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/blurb/checkout/MainShadowActivity$Country;>;"
    const/4 v5, 0x0

    .line 1497
    .local v5, "usa":Lcom/blurb/checkout/MainShadowActivity$Country;
    sget-object v6, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/blurb/checkout/MainShadowActivity$Country;

    .line 1498
    .local v3, "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    const-string v6, "us"

    iget-object v7, v3, Lcom/blurb/checkout/MainShadowActivity$Country;->twoLetterCode:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1499
    move-object v5, v3

    goto :goto_0

    .line 1501
    :cond_0
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1505
    .end local v3    # "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-static {v6}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    .line 1506
    .local v0, "col":Ljava/text/Collator;
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Ljava/text/Collator;->setStrength(I)V

    .line 1508
    new-instance v6, Lcom/blurb/checkout/MainShadowActivity$17;

    invoke-direct {v6, v0}, Lcom/blurb/checkout/MainShadowActivity$17;-><init>(Ljava/text/Collator;)V

    invoke-static {v2, v6}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1516
    new-instance v1, Ljava/util/ArrayList;

    sget-object v6, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 1517
    .local v1, "countries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/blurb/checkout/MainShadowActivity$Country;>;"
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1518
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1520
    return-object v1
.end method

.method private handleIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/16 v5, 0x100

    .line 423
    if-nez p1, :cond_1

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    const-string v4, "episode_id"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->episodeId:Ljava/lang/String;

    .line 427
    iget-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->episodeId:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 428
    const-string v4, "currency_id"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->currencyId:Ljava/lang/String;

    .line 431
    const-string v4, "book_type"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->bookType:Ljava/lang/String;

    .line 432
    const-string v4, "cover_type"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->coverType:Ljava/lang/String;

    .line 433
    const-string v4, "title"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->title:Ljava/lang/String;

    .line 434
    const-string v4, "cover_uri"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 435
    .local v2, "coverUri":Ljava/lang/String;
    const-string v4, "num_pages"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->numPages:Ljava/lang/String;

    .line 436
    const-string v4, "couponCode"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->couponCode:Ljava/lang/String;

    .line 437
    const-string v4, "paper_type"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->paperType:Ljava/lang/String;

    .line 439
    iget-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->paperType:Ljava/lang/String;

    if-nez v4, :cond_2

    .line 440
    const-string v4, "premium_lustre_paper"

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->paperType:Ljava/lang/String;

    .line 442
    :cond_2
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->showHideDiscount()V

    .line 444
    iget-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->numPages:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/blurb/checkout/MainShadowActivity;->pageCount:I

    .line 446
    if-eqz v2, :cond_0

    .line 447
    invoke-static {v2, v5, v5}, Lcom/blurb/checkout/MainShadowActivity;->decodeSampledBitmapFromFile(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 449
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-nez v0, :cond_3

    .line 450
    const-string v4, "BlurbShadowApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Error: could not decode cover="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/blurb/checkout/MainShadowActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 456
    :cond_3
    :try_start_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 457
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x28

    invoke-virtual {v0, v4, v5, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 458
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    iput-object v4, p0, Lcom/blurb/checkout/MainShadowActivity;->coverData:[B

    .line 459
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 464
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0

    .line 460
    :catch_0
    move-exception v3

    .line 461
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private handleNewerVersionAvailable()V
    .locals 5

    .prologue
    .line 2405
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 2407
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0400f1

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x7f040000

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f04004a

    new-instance v4, Lcom/blurb/checkout/MainShadowActivity$24;

    invoke-direct {v4, p0}, Lcom/blurb/checkout/MainShadowActivity$24;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f04004b

    new-instance v4, Lcom/blurb/checkout/MainShadowActivity$23;

    invoke-direct {v4, p0}, Lcom/blurb/checkout/MainShadowActivity$23;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 2434
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 2435
    .local v1, "dialog":Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 2436
    return-void
.end method

.method private handleValidationError(ILjava/lang/String;)V
    .locals 3
    .param p1, "field"    # I
    .param p2, "code"    # Ljava/lang/String;

    .prologue
    .line 1845
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-static {p0, p2, v2}, Lcom/blurb/checkout/BlurbAPI;->codeToMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1846
    .local v0, "message":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1847
    move-object v0, p2

    .line 1849
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1888
    :cond_1
    :goto_0
    :pswitch_0
    return-void

    .line 1851
    :pswitch_1
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1852
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1855
    :pswitch_2
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1856
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1859
    :pswitch_3
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1860
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1863
    :pswitch_4
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->countryEditText:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1866
    :pswitch_5
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1867
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1870
    :pswitch_6
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1871
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1874
    :pswitch_7
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1875
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1878
    :pswitch_8
    const v2, 0x7f06000d

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 1879
    .local v1, "phoneEditText":Landroid/widget/EditText;
    if-eqz v1, :cond_1

    .line 1880
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1881
    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    .line 1849
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private loadCountryNames()V
    .locals 3

    .prologue
    .line 649
    sget-object v2, Lcom/blurb/checkout/MainShadowActivity;->countryCodeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/blurb/checkout/MainShadowActivity$Country;

    .line 650
    .local v0, "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    iget v2, v0, Lcom/blurb/checkout/MainShadowActivity$Country;->countryId:I

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/MainShadowActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$Country;->myName:Ljava/lang/String;

    goto :goto_0

    .line 652
    .end local v0    # "country":Lcom/blurb/checkout/MainShadowActivity$Country;
    :cond_0
    return-void
.end method

.method private popupCountryView()V
    .locals 14

    .prologue
    const v13, 0x3f4ccccd    # 0.8f

    .line 1524
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    .line 1525
    .local v4, "fv":Landroid/view/View;
    if-eqz v4, :cond_0

    .line 1526
    const-string v10, "input_method"

    invoke-virtual {p0, v10}, Lcom/blurb/checkout/MainShadowActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v11

    const/4 v12, 0x2

    invoke-virtual {v10, v11, v12}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1530
    :cond_0
    new-instance v7, Landroid/util/DisplayMetrics;

    invoke-direct {v7}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1531
    .local v7, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v10

    invoke-interface {v10}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v10

    invoke-virtual {v10, v7}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1533
    iget v10, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v10, v10

    mul-float/2addr v10, v13

    float-to-int v9, v10

    .line 1534
    .local v9, "width":I
    iget v10, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v10, v10

    mul-float/2addr v10, v13

    float-to-int v5, v10

    .line 1536
    .local v5, "height":I
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 1537
    .local v6, "inflater":Landroid/view/LayoutInflater;
    const v10, 0x7f030009

    const/4 v11, 0x0

    invoke-virtual {v6, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 1538
    .local v3, "countriesView":Landroid/view/View;
    invoke-static {p0}, Lcom/blurb/checkout/MainShadowActivity;->getSortedCountries(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1540
    .local v2, "countries":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/blurb/checkout/MainShadowActivity$Country;>;"
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v10, 0x1090003

    invoke-direct {v0, p0, v10, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1543
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Lcom/blurb/checkout/MainShadowActivity$Country;>;"
    const v10, 0x7f060018

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ListView;

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countriesListView:Landroid/widget/ListView;

    .line 1545
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countriesListView:Landroid/widget/ListView;

    invoke-virtual {v10, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1547
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1548
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v10, 0x7f040001

    invoke-virtual {v1, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    invoke-virtual {v10, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 1551
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v10

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryDialog:Landroid/app/Dialog;

    .line 1553
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countriesListView:Landroid/widget/ListView;

    new-instance v11, Lcom/blurb/checkout/MainShadowActivity$18;

    invoke-direct {v11, p0}, Lcom/blurb/checkout/MainShadowActivity$18;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v10, v11}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1571
    move v8, v9

    .line 1573
    .local v8, "size":I
    if-le v9, v5, :cond_1

    .line 1574
    move v8, v5

    .line 1576
    :cond_1
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryDialog:Landroid/app/Dialog;

    invoke-virtual {v10}, Landroid/app/Dialog;->show()V

    .line 1578
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryDialog:Landroid/app/Dialog;

    invoke-virtual {v10}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v10

    invoke-virtual {v10, v8, v8}, Landroid/view/Window;->setLayout(II)V

    .line 1580
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryDialog:Landroid/app/Dialog;

    invoke-virtual {v10, p0}, Landroid/app/Dialog;->setOwnerActivity(Landroid/app/Activity;)V

    .line 1581
    return-void
.end method

.method public static setEditTextForAuPostalCode(Landroid/widget/EditText;)V
    .locals 4
    .param p0, "view"    # Landroid/widget/EditText;

    .prologue
    .line 955
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 957
    const/4 v1, 0x1

    new-array v0, v1, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x4

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    .line 960
    .local v0, "filters":[Landroid/text/InputFilter;
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 961
    return-void
.end method

.method public static setEditTextForAuState(Landroid/widget/EditText;)V
    .locals 4
    .param p0, "view"    # Landroid/widget/EditText;

    .prologue
    .line 855
    const/16 v1, 0x90

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 857
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/blurb/checkout/MainShadowActivity$9;

    invoke-direct {v2}, Lcom/blurb/checkout/MainShadowActivity$9;-><init>()V

    aput-object v2, v0, v1

    .line 876
    .local v0, "usStateFilters":[Landroid/text/InputFilter;
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 877
    return-void
.end method

.method public static setEditTextForCAPostalCode(Landroid/widget/EditText;)V
    .locals 4
    .param p0, "view"    # Landroid/widget/EditText;

    .prologue
    .line 905
    const/16 v1, 0x90

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 907
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x7

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/blurb/checkout/MainShadowActivity$11;

    invoke-direct {v2}, Lcom/blurb/checkout/MainShadowActivity$11;-><init>()V

    aput-object v2, v0, v1

    .line 926
    .local v0, "filters":[Landroid/text/InputFilter;
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 927
    return-void
.end method

.method public static setEditTextForInternationalAddress(Landroid/widget/EditText;)V
    .locals 1
    .param p0, "view"    # Landroid/widget/EditText;

    .prologue
    .line 1096
    if-eqz p0, :cond_0

    .line 1097
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/text/InputFilter;

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1098
    const/16 v0, 0x2090

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 1100
    :cond_0
    return-void
.end method

.method static setEditTextForJpZipCode(Landroid/widget/EditText;)V
    .locals 4
    .param p0, "view"    # Landroid/widget/EditText;

    .prologue
    .line 1032
    const/16 v1, 0x90

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 1034
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/blurb/checkout/MainShadowActivity$15;

    invoke-direct {v2}, Lcom/blurb/checkout/MainShadowActivity$15;-><init>()V

    aput-object v2, v0, v1

    .line 1065
    .local v0, "jpZipFilters":[Landroid/text/InputFilter;
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1067
    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$16;

    invoke-direct {v1}, Lcom/blurb/checkout/MainShadowActivity$16;-><init>()V

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1093
    return-void
.end method

.method static setEditTextForKrZipCode(Landroid/widget/EditText;)V
    .locals 4
    .param p0, "view"    # Landroid/widget/EditText;

    .prologue
    .line 966
    const/16 v1, 0x90

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 968
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x7

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/blurb/checkout/MainShadowActivity$13;

    invoke-direct {v2}, Lcom/blurb/checkout/MainShadowActivity$13;-><init>()V

    aput-object v2, v0, v1

    .line 999
    .local v0, "krZipFilters":[Landroid/text/InputFilter;
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 1001
    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$14;

    invoke-direct {v1}, Lcom/blurb/checkout/MainShadowActivity$14;-><init>()V

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1027
    return-void
.end method

.method public static setEditTextForUKPostalCode(Landroid/widget/EditText;)V
    .locals 4
    .param p0, "view"    # Landroid/widget/EditText;

    .prologue
    .line 930
    const/16 v1, 0x90

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 932
    const/4 v1, 0x2

    new-array v0, v1, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/16 v3, 0x8

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/blurb/checkout/MainShadowActivity$12;

    invoke-direct {v2}, Lcom/blurb/checkout/MainShadowActivity$12;-><init>()V

    aput-object v2, v0, v1

    .line 951
    .local v0, "filters":[Landroid/text/InputFilter;
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 952
    return-void
.end method

.method public static setEditTextForUSState(Landroid/widget/EditText;)V
    .locals 4
    .param p0, "view"    # Landroid/widget/EditText;

    .prologue
    const/4 v3, 0x2

    .line 830
    const/16 v1, 0x90

    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 832
    new-array v0, v3, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/blurb/checkout/MainShadowActivity$8;

    invoke-direct {v2}, Lcom/blurb/checkout/MainShadowActivity$8;-><init>()V

    aput-object v2, v0, v1

    .line 851
    .local v0, "usStateFilters":[Landroid/text/InputFilter;
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 852
    return-void
.end method

.method public static setEditTextForUSZipCode(Landroid/widget/EditText;)V
    .locals 4
    .param p0, "view"    # Landroid/widget/EditText;

    .prologue
    const/4 v1, 0x2

    .line 880
    invoke-virtual {p0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 882
    new-array v0, v1, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/blurb/checkout/MainShadowActivity$10;

    invoke-direct {v2}, Lcom/blurb/checkout/MainShadowActivity$10;-><init>()V

    aput-object v2, v0, v1

    .line 901
    .local v0, "usZipFilters":[Landroid/text/InputFilter;
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 902
    return-void
.end method

.method private setFirstFocus()V
    .locals 6

    .prologue
    .line 373
    const/4 v1, 0x0

    .line 374
    .local v1, "focusView":Landroid/view/View;
    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_0

    .line 375
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    .line 376
    :cond_0
    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    .line 377
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    .line 378
    :cond_1
    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    .line 379
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    .line 380
    :cond_2
    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getVisibility()I

    move-result v3

    if-nez v3, :cond_3

    .line 381
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    .line 382
    :cond_3
    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getVisibility()I

    move-result v3

    if-nez v3, :cond_4

    .line 383
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    .line 384
    :cond_4
    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getVisibility()I

    move-result v3

    if-nez v3, :cond_5

    .line 385
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    .line 389
    :cond_5
    const v3, 0x7f060008

    invoke-virtual {p0, v3}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 390
    .local v0, "et":Landroid/widget/EditText;
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v3

    if-nez v3, :cond_6

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_6

    .line 391
    move-object v1, v0

    .line 392
    :cond_6
    const v3, 0x7f06000d

    invoke-virtual {p0, v3}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "et":Landroid/widget/EditText;
    check-cast v0, Landroid/widget/EditText;

    .line 393
    .restart local v0    # "et":Landroid/widget/EditText;
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_7

    .line 394
    move-object v1, v0

    .line 396
    :cond_7
    if-eqz v1, :cond_8

    .line 397
    move-object v2, v1

    .line 399
    .local v2, "showKeyboardView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 401
    new-instance v3, Lcom/blurb/checkout/MainShadowActivity$5;

    invoke-direct {v3, p0, v2}, Lcom/blurb/checkout/MainShadowActivity$5;-><init>(Lcom/blurb/checkout/MainShadowActivity;Landroid/view/View;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v3, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 412
    .end local v2    # "showKeyboardView":Landroid/view/View;
    :goto_0
    return-void

    .line 410
    :cond_8
    const v3, 0x7f060047

    invoke-virtual {p0, v3}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method

.method private setListenersForOrderCount()V
    .locals 4

    .prologue
    .line 526
    const v2, 0x7f060048

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 527
    .local v0, "minus_button":Landroid/widget/ImageButton;
    const v2, 0x7f06004e

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 528
    .local v1, "plus_button":Landroid/widget/ImageButton;
    const v2, 0x7f06004d

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    .line 530
    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 531
    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 532
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    new-instance v3, Lcom/blurb/checkout/MainShadowActivity$6;

    invoke-direct {v3, p0}, Lcom/blurb/checkout/MainShadowActivity$6;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 548
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    new-instance v3, Lcom/blurb/checkout/MainShadowActivity$7;

    invoke-direct {v3, p0}, Lcom/blurb/checkout/MainShadowActivity$7;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 560
    return-void
.end method

.method private setUserInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "firstName"    # Ljava/lang/String;
    .param p2, "lastName"    # Ljava/lang/String;
    .param p3, "postalCode"    # Ljava/lang/String;
    .param p4, "code3"    # Ljava/lang/String;
    .param p5, "loginId"    # Ljava/lang/String;

    .prologue
    .line 2069
    if-nez p4, :cond_0

    .line 2070
    const-string p4, "usa"

    .line 2071
    :cond_0
    if-nez p5, :cond_1

    .line 2072
    const-string p5, ""

    .line 2074
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {p4, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->countryCode3to2(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    .line 2076
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->updateCountryFields()V

    .line 2078
    if-eqz p1, :cond_2

    .line 2079
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2080
    :cond_2
    if-eqz p2, :cond_3

    .line 2081
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v1, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2082
    :cond_3
    if-eqz p3, :cond_4

    .line 2083
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v1, p3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2085
    :cond_4
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/blurb/checkout/MainShadowActivity;->getCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2087
    .local v0, "countryName":Ljava/lang/String;
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->countryEditText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2088
    iput-object p5, p0, Lcom/blurb/checkout/MainShadowActivity;->email:Ljava/lang/String;

    .line 2090
    return-void
.end method

.method private showError(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "error_msg"    # Ljava/lang/String;
    .param p2, "fatal"    # Z

    .prologue
    .line 1584
    if-eqz p1, :cond_1

    .line 1585
    new-instance v0, Lcom/blurb/checkout/MainShadowActivity$19;

    invoke-direct {v0, p0, p0, p1, p2}, Lcom/blurb/checkout/MainShadowActivity$19;-><init>(Lcom/blurb/checkout/MainShadowActivity;Landroid/content/Context;Ljava/lang/String;Z)V

    .line 1597
    .local v0, "errorDialog":Lcom/blurb/checkout/ErrorDialog;
    if-nez p2, :cond_0

    .line 1598
    const v1, 0x7f04004a

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/ErrorDialog;->setButtonText(I)V

    .line 1599
    :cond_0
    invoke-virtual {v0}, Lcom/blurb/checkout/ErrorDialog;->show()V

    .line 1601
    .end local v0    # "errorDialog":Lcom/blurb/checkout/ErrorDialog;
    :cond_1
    return-void
.end method

.method private showHideDiscount()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 508
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->couponCode:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->couponCode:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    .line 510
    .local v0, "hasDiscount":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 511
    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity;->discountText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 515
    :goto_1
    return-void

    .end local v0    # "hasDiscount":Z
    :cond_0
    move v0, v1

    .line 508
    goto :goto_0

    .line 513
    .restart local v0    # "hasDiscount":Z
    :cond_1
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity;->discountText:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private startCheckNewerVersion()V
    .locals 2

    .prologue
    .line 2401
    new-instance v0, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;

    invoke-direct {v0, p0, p0}, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;-><init>(Lcom/blurb/checkout/MainShadowActivity;Landroid/app/Activity;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2402
    return-void
.end method

.method private startGetAccessTokenActivity()V
    .locals 6

    .prologue
    .line 1682
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 1683
    .local v3, "manager":Landroid/accounts/AccountManager;
    const-string v4, "com.osp.app.signin"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 1684
    .local v0, "accounts":[Landroid/accounts/Account;
    array-length v4, v0

    if-lez v4, :cond_0

    .line 1685
    const/4 v4, 0x4

    new-array v1, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "login_id"

    aput-object v5, v1, v4

    const/4 v4, 0x1

    const-string v5, "login_id_type"

    aput-object v5, v1, v4

    const/4 v4, 0x2

    const-string v5, "api_server_url"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string v5, "auth_server_url"

    aput-object v5, v1, v4

    .line 1687
    .local v1, "additional":[Ljava/lang/String;
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.msc.action.samsungaccount.REQUEST_ACCESSTOKEN"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1688
    .local v2, "bundle":Landroid/content/Intent;
    const-string v4, "client_id"

    const-string v5, "2m3m68snwb"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1689
    const-string v4, "client_secret"

    const-string v5, "5006774DDEBAA634A24424284B61A114"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1690
    const-string v4, "additional"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1691
    const-string v4, "progress_theme"

    const-string v5, "light"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1692
    const/16 v4, 0xc

    invoke-virtual {p0, v2, v4}, Lcom/blurb/checkout/MainShadowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1696
    .end local v1    # "additional":[Ljava/lang/String;
    .end local v2    # "bundle":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private startGetAccessTokenBroadcast(Ljava/lang/String;)V
    .locals 3
    .param p1, "expiredAccessToken"    # Ljava/lang/String;

    .prologue
    .line 1703
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.ACCESSTOKEN_V02_REQUEST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1706
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "client_id"

    const-string v2, "2m3m68snwb"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1707
    const-string v1, "client_secret"

    const-string v2, "5006774DDEBAA634A24424284B61A114"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1708
    const-string v1, "mypackage"

    const-string v2, "com.blurb.checkout"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1709
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1710
    const-string v1, "MODE"

    const-string v2, "BACKGROUND"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1712
    if-eqz p1, :cond_0

    .line 1713
    const-string v1, "expired_access_token"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1717
    :cond_0
    invoke-virtual {p0, v0}, Lcom/blurb/checkout/MainShadowActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 1718
    return-void
.end method

.method private startSSOSignin()V
    .locals 3

    .prologue
    .line 1724
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1725
    .local v0, "localIntent":Landroid/content/Intent;
    const-string v1, "com.osp.app.signin"

    const-string v2, "com.osp.app.signin.AccountView"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1726
    const-string v1, "client_id"

    const-string v2, "2m3m68snwb"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1727
    const-string v1, "client_ secret"

    const-string v2, "5006774DDEBAA634A24424284B61A114"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1728
    const-string v1, "account_mode"

    const-string v2, "AGREE_TO_DISCLAIMER"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1729
    const-string v1, "OSP_VER"

    const-string v2, "OSP_02"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1730
    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/blurb/checkout/MainShadowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1731
    return-void
.end method

.method private startUpload()V
    .locals 2

    .prologue
    .line 415
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/blurb/checkout/service/UploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 417
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.blurb.checkout.service.UPLOAD_BOOKS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 419
    invoke-virtual {p0, v0}, Lcom/blurb/checkout/MainShadowActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 420
    return-void
.end method

.method private updateCountryFields()V
    .locals 14

    .prologue
    .line 1103
    const/4 v4, 0x0

    .local v4, "first":Ljava/lang/String;
    const/4 v5, 0x0

    .local v5, "last":Ljava/lang/String;
    const/4 v9, 0x0

    .local v9, "street1":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "city":Ljava/lang/String;
    const/4 v8, 0x0

    .line 1105
    .local v8, "state":Ljava/lang/String;
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    if-eqz v10, :cond_0

    .line 1106
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1107
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1108
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1109
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1110
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v10}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1113
    :cond_0
    const v10, 0x7f060021

    invoke-virtual {p0, v10}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 1114
    .local v3, "countryView":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1118
    const-string v10, "us"

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "ca"

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    const-string v10, "au"

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1122
    :cond_1
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v10

    const v11, 0x7f030003

    invoke-virtual {v10, v11, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1131
    .local v0, "address":Landroid/view/View;
    :goto_0
    const v10, 0x7f060003

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    .line 1132
    const v10, 0x7f060004

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    .line 1133
    const v10, 0x7f060007

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    .line 1134
    const v10, 0x7f060005

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    .line 1135
    const v10, 0x7f060006

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    .line 1136
    const v10, 0x7f060009

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    .line 1138
    const v10, 0x7f06000c

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryEditText:Landroid/widget/TextView;

    .line 1139
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryEditText:Landroid/widget/TextView;

    invoke-virtual {v10, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1141
    const v10, 0x7f06000a

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageButton;

    iput-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryChooser:Landroid/widget/ImageButton;

    .line 1142
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryChooser:Landroid/widget/ImageButton;

    invoke-virtual {v10, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1144
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-static {p0, v10}, Lcom/blurb/checkout/MainShadowActivity;->getCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1145
    .local v2, "country":Ljava/lang/String;
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryEditText:Landroid/widget/TextView;

    invoke-virtual {v10, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1147
    if-eqz v4, :cond_4

    .line 1148
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v10, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1149
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v10, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1150
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v10, v9}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1151
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v10, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1153
    const-string v10, "us"

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "ca"

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "au"

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1157
    :cond_2
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    const/4 v11, 0x2

    if-le v10, v11, :cond_3

    .line 1158
    const-string v8, ""

    .line 1161
    :cond_3
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v10, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1164
    :cond_4
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    const v11, 0x7f060005

    const v12, 0x7f060006

    const v13, 0x7f060009

    invoke-static {p0, v10, v11, v12, v13}, Lcom/blurb/checkout/MainShadowActivity;->updateCountryFields(Landroid/app/Activity;Ljava/lang/String;III)V

    .line 1166
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    const-string v11, "kr"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1167
    const v10, 0x7f06000d

    invoke-virtual {v0, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/EditText;

    .line 1168
    .local v6, "phoneEditText":Landroid/widget/EditText;
    const/4 v10, 0x6

    invoke-virtual {v6, v10}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 1173
    .end local v6    # "phoneEditText":Landroid/widget/EditText;
    :goto_1
    const v10, 0x7f06004e

    invoke-virtual {p0, v10}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    .line 1175
    .local v7, "plusButton":Landroid/widget/ImageButton;
    const-string v10, "us"

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    const-string v10, "ca"

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    const-string v10, "au"

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1179
    :cond_5
    const v10, 0x7f06000a

    invoke-virtual {v7, v10}, Landroid/widget/ImageButton;->setNextFocusRightId(I)V

    .line 1180
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryChooser:Landroid/widget/ImageButton;

    const v11, 0x7f06004e

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    .line 1191
    :goto_2
    return-void

    .line 1123
    .end local v0    # "address":Landroid/view/View;
    .end local v2    # "country":Ljava/lang/String;
    .end local v7    # "plusButton":Landroid/widget/ImageButton;
    :cond_6
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    const-string v11, "jp"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1124
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v10

    const/high16 v11, 0x7f030000

    invoke-virtual {v10, v11, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "address":Landroid/view/View;
    goto/16 :goto_0

    .line 1125
    .end local v0    # "address":Landroid/view/View;
    :cond_7
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    const-string v11, "kr"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1126
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v10

    const v11, 0x7f030001

    invoke-virtual {v10, v11, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "address":Landroid/view/View;
    goto/16 :goto_0

    .line 1128
    .end local v0    # "address":Landroid/view/View;
    :cond_8
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v10

    const v11, 0x7f030002

    invoke-virtual {v10, v11, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .restart local v0    # "address":Landroid/view/View;
    goto/16 :goto_0

    .line 1170
    .restart local v2    # "country":Ljava/lang/String;
    :cond_9
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    const/4 v11, 0x6

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setImeOptions(I)V

    goto :goto_1

    .line 1181
    .restart local v7    # "plusButton":Landroid/widget/ImageButton;
    :cond_a
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    const-string v11, "jp"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 1182
    const v10, 0x7f060003

    invoke-virtual {v7, v10}, Landroid/widget/ImageButton;->setNextFocusRightId(I)V

    .line 1183
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    const v11, 0x7f06004e

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    goto :goto_2

    .line 1184
    :cond_b
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    const-string v11, "kr"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_c

    .line 1185
    const v10, 0x7f060003

    invoke-virtual {v7, v10}, Landroid/widget/ImageButton;->setNextFocusRightId(I)V

    .line 1186
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    const v11, 0x7f06004e

    invoke-virtual {v10, v11}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    goto :goto_2

    .line 1188
    :cond_c
    const v10, 0x7f06000a

    invoke-virtual {v7, v10}, Landroid/widget/ImageButton;->setNextFocusRightId(I)V

    .line 1189
    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->countryChooser:Landroid/widget/ImageButton;

    const v11, 0x7f06004e

    invoke-virtual {v10, v11}, Landroid/widget/ImageButton;->setNextFocusLeftId(I)V

    goto/16 :goto_2
.end method

.method static updateCountryFields(Landroid/app/Activity;Ljava/lang/String;III)V
    .locals 10
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "countryCode"    # Ljava/lang/String;
    .param p2, "cityId"    # I
    .param p3, "stateId"    # I
    .param p4, "zipId"    # I

    .prologue
    const v9, 0x7f0400a1

    const v8, 0x7f040094

    const v7, 0x7f060005

    const v6, 0x7f060009

    const/4 v5, 0x0

    .line 1194
    invoke-virtual {p0, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 1195
    .local v0, "cityEditText":Landroid/widget/EditText;
    invoke-virtual {p0, p3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 1196
    .local v2, "stateEditText":Landroid/widget/EditText;
    invoke-virtual {p0, p4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 1197
    .local v3, "zipCodeEditText":Landroid/widget/EditText;
    const v4, 0x7f060044

    invoke-virtual {p0, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1199
    .local v1, "nextScreenButton":Landroid/view/View;
    const-string v4, "us"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1201
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1202
    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1203
    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1205
    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setHint(I)V

    .line 1206
    const v4, 0x7f04008d

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1207
    const v4, 0x7f04008e

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1212
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForUSState(Landroid/widget/EditText;)V

    .line 1213
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForUSZipCode(Landroid/widget/EditText;)V

    .line 1214
    const/4 v4, 0x6

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 1216
    invoke-virtual {v1, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 1370
    :goto_0
    return-void

    .line 1218
    :cond_0
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForInternationalAddress(Landroid/widget/EditText;)V

    .line 1220
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForInternationalAddress(Landroid/widget/EditText;)V

    .line 1222
    const-string v4, "chn"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1229
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1230
    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1231
    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1233
    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setHint(I)V

    .line 1234
    const v4, 0x7f0400a7

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1235
    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_0

    .line 1236
    :cond_1
    const-string v4, "kr"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1238
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForKrZipCode(Landroid/widget/EditText;)V

    .line 1240
    const v4, 0x7f06000d

    invoke-virtual {v1, v4}, Landroid/view/View;->setNextFocusUpId(I)V

    goto :goto_0

    .line 1241
    :cond_2
    const-string v4, "jp"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1243
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForJpZipCode(Landroid/widget/EditText;)V

    .line 1245
    const v4, 0x7f06000a

    invoke-virtual {v1, v4}, Landroid/view/View;->setNextFocusUpId(I)V

    goto :goto_0

    .line 1246
    :cond_3
    sget-object v4, Lcom/blurb/checkout/MainShadowActivity;->countryClassAt:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1249
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1250
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1251
    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1253
    const v4, 0x7f0400a3

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1254
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1255
    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setHint(I)V

    .line 1257
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1259
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 1260
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setNextFocusRightId(I)V

    .line 1261
    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    .line 1262
    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    .line 1264
    invoke-virtual {v1, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    goto :goto_0

    .line 1265
    :cond_4
    sget-object v4, Lcom/blurb/checkout/MainShadowActivity;->countryClassAu:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1268
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForAuState(Landroid/widget/EditText;)V

    .line 1269
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForAuPostalCode(Landroid/widget/EditText;)V

    .line 1271
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1272
    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1273
    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1275
    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setHint(I)V

    .line 1276
    const v4, 0x7f0400a4

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1277
    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setHint(I)V

    .line 1279
    invoke-virtual {v1, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    goto/16 :goto_0

    .line 1280
    :cond_5
    sget-object v4, Lcom/blurb/checkout/MainShadowActivity;->countryClassIe:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1283
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1284
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1285
    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1287
    const v4, 0x7f0400a5

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1288
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1289
    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setHint(I)V

    .line 1291
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1293
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 1294
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setNextFocusRightId(I)V

    .line 1295
    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    .line 1296
    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    .line 1298
    invoke-virtual {v1, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    goto/16 :goto_0

    .line 1299
    :cond_6
    sget-object v4, Lcom/blurb/checkout/MainShadowActivity;->countryClassCa:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1302
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForUSState(Landroid/widget/EditText;)V

    .line 1303
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForCAPostalCode(Landroid/widget/EditText;)V

    .line 1305
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1306
    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1307
    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1309
    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setHint(I)V

    .line 1310
    const v4, 0x7f0400a7

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1311
    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setHint(I)V

    .line 1313
    invoke-virtual {v1, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    goto/16 :goto_0

    .line 1314
    :cond_7
    sget-object v4, Lcom/blurb/checkout/MainShadowActivity;->countryClassGb:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1317
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->setEditTextForUKPostalCode(Landroid/widget/EditText;)V

    .line 1319
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1320
    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1321
    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1323
    const v4, 0x7f0400a6

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1324
    const v4, 0x7f0400aa

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1325
    const v4, 0x7f0400a2

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(I)V

    .line 1327
    invoke-virtual {v1, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 1328
    invoke-virtual {v1, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    goto/16 :goto_0

    .line 1329
    :cond_8
    sget-object v4, Lcom/blurb/checkout/MainShadowActivity;->countryClassHk:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1332
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1333
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1334
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1336
    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setHint(I)V

    .line 1337
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1338
    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1340
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1341
    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1343
    const v4, 0x7f060044

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 1344
    const v4, 0x7f060044

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setNextFocusRightId(I)V

    .line 1346
    invoke-virtual {v1, v7}, Landroid/view/View;->setNextFocusUpId(I)V

    goto/16 :goto_0

    .line 1347
    :cond_9
    sget-object v4, Lcom/blurb/checkout/MainShadowActivity;->countryClassDe:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1350
    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1351
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1352
    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setVisibility(I)V

    .line 1354
    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setHint(I)V

    .line 1355
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1356
    invoke-virtual {v3, v9}, Landroid/widget/EditText;->setHint(I)V

    .line 1358
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1360
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    .line 1361
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setNextFocusRightId(I)V

    .line 1362
    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    .line 1363
    invoke-virtual {v3, v7}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    .line 1365
    invoke-virtual {v1, v6}, Landroid/view/View;->setNextFocusUpId(I)V

    goto/16 :goto_0

    .line 1367
    :cond_a
    const-string v4, "BlurbShadowApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateCountryFields: Unhandled country code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x2

    const/4 v4, -0x1

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 1617
    packed-switch p1, :pswitch_data_0

    .line 1661
    :cond_0
    :goto_0
    return-void

    .line 1619
    :pswitch_0
    if-ne p2, v4, :cond_1

    .line 1620
    if-eqz p3, :cond_0

    const-string v4, "is_agree_to_disclaimer"

    invoke-virtual {p3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1621
    invoke-virtual {p0, v3}, Lcom/blurb/checkout/MainShadowActivity;->sendSsoBroadcast(Ljava/lang/String;)V

    goto :goto_0

    .line 1623
    :cond_1
    if-nez p2, :cond_2

    .line 1624
    invoke-virtual {p0, v5, v3}, Lcom/blurb/checkout/MainShadowActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    .line 1625
    :cond_2
    if-ne p2, v5, :cond_3

    .line 1626
    invoke-virtual {p0, v7, v3}, Lcom/blurb/checkout/MainShadowActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    .line 1628
    :cond_3
    const-string v4, "BlurbShadowApp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onActivityResult: Unknown result code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1629
    invoke-virtual {p0, v7, v3}, Lcom/blurb/checkout/MainShadowActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    .line 1633
    :pswitch_1
    if-ne p2, v4, :cond_4

    .line 1635
    invoke-virtual {p0, v4}, Lcom/blurb/checkout/MainShadowActivity;->setResult(I)V

    .line 1636
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->finish()V

    goto :goto_0

    .line 1637
    :cond_4
    if-ne p2, v5, :cond_0

    .line 1638
    invoke-virtual {p0, v5}, Lcom/blurb/checkout/MainShadowActivity;->setResult(I)V

    .line 1639
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->finish()V

    goto :goto_0

    .line 1644
    :pswitch_2
    if-ne p2, v4, :cond_5

    if-eqz p3, :cond_5

    .line 1645
    const-string v3, "access_token"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1647
    .local v0, "accessToken":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1649
    .local v2, "locale":Ljava/lang/String;
    new-instance v3, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;

    invoke-direct {v3, p0, p0, v0, v2}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;-><init>(Lcom/blurb/checkout/MainShadowActivity;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1652
    .end local v0    # "accessToken":Ljava/lang/String;
    .end local v2    # "locale":Ljava/lang/String;
    :cond_5
    if-nez p3, :cond_6

    move-object v1, v3

    .line 1654
    .local v1, "errorMessage":Ljava/lang/String;
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1655
    invoke-virtual {p0, v7, v3}, Lcom/blurb/checkout/MainShadowActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    .line 1652
    .end local v1    # "errorMessage":Ljava/lang/String;
    :cond_6
    const-string v4, "error_message"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1657
    .restart local v1    # "errorMessage":Ljava/lang/String;
    :cond_7
    invoke-direct {p0, v1, v5}, Lcom/blurb/checkout/MainShadowActivity;->showError(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1617
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 18
    .param p1, "v"    # Landroid/view/View;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ParserError"
        }
    .end annotation

    .prologue
    .line 1375
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f060044

    if-ne v1, v2, :cond_a

    .line 1376
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->quantity:Ljava/lang/String;

    .line 1378
    const/4 v14, 0x0

    .line 1381
    .local v14, "count":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->quantity:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v14

    .line 1385
    :goto_0
    if-gtz v14, :cond_1

    .line 1386
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    const v2, 0x7f0400f0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/blurb/checkout/MainShadowActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1387
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 1472
    .end local v14    # "count":I
    :cond_0
    :goto_1
    return-void

    .line 1391
    .restart local v14    # "count":I
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/blurb/checkout/MainShadowActivity;->clearValidationErrors()V

    .line 1393
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    if-nez v1, :cond_5

    const-string v10, ""

    .line 1394
    .local v10, "zip":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    if-nez v1, :cond_7

    const-string v9, ""

    .line 1396
    .local v9, "state":Ljava/lang/String;
    :goto_3
    const-string v1, "us"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1397
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    .line 1399
    :cond_2
    const-string v7, ""

    .line 1401
    .local v7, "streetAddress2":Ljava/lang/String;
    const-string v1, "kr"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "jp"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1402
    :cond_3
    const v1, 0x7f060008

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/EditText;

    .line 1403
    .local v16, "et":Landroid/widget/EditText;
    invoke-virtual/range {v16 .. v16}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1406
    .end local v16    # "et":Landroid/widget/EditText;
    :cond_4
    const-string v12, ""

    .line 1407
    .local v12, "phone":Ljava/lang/String;
    const-string v13, ""

    .line 1409
    .local v13, "poBoxOrApo":Ljava/lang/String;
    const-string v1, "kr"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1410
    const v1, 0x7f06000d

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/EditText;

    .line 1411
    .local v17, "phoneEditText":Landroid/widget/EditText;
    if-eqz v17, :cond_9

    .line 1412
    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    .line 1414
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_9

    .line 1415
    const v1, 0x7f040058

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1416
    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_1

    .line 1393
    .end local v7    # "streetAddress2":Ljava/lang/String;
    .end local v9    # "state":Ljava/lang/String;
    .end local v10    # "zip":Ljava/lang/String;
    .end local v12    # "phone":Ljava/lang/String;
    .end local v13    # "poBoxOrApo":Ljava/lang/String;
    .end local v17    # "phoneEditText":Landroid/widget/EditText;
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_2

    :cond_6
    const-string v10, ""

    goto/16 :goto_2

    .line 1394
    .restart local v10    # "zip":Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_3

    :cond_8
    const-string v9, ""

    goto/16 :goto_3

    .line 1422
    .restart local v7    # "streetAddress2":Ljava/lang/String;
    .restart local v9    # "state":Ljava/lang/String;
    .restart local v12    # "phone":Ljava/lang/String;
    .restart local v13    # "poBoxOrApo":Ljava/lang/String;
    :cond_9
    new-instance v1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    invoke-direct/range {v1 .. v13}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;-><init>(Lcom/blurb/checkout/MainShadowActivity;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_1

    .line 1434
    .end local v7    # "streetAddress2":Ljava/lang/String;
    .end local v9    # "state":Ljava/lang/String;
    .end local v10    # "zip":Ljava/lang/String;
    .end local v12    # "phone":Ljava/lang/String;
    .end local v13    # "poBoxOrApo":Ljava/lang/String;
    .end local v14    # "count":I
    :cond_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f06000a

    if-eq v1, v2, :cond_b

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f06000c

    if-ne v1, v2, :cond_c

    .line 1435
    :cond_b
    invoke-direct/range {p0 .. p0}, Lcom/blurb/checkout/MainShadowActivity;->popupCountryView()V

    goto/16 :goto_1

    .line 1436
    :cond_c
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f060048

    if-ne v1, v2, :cond_e

    .line 1437
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1438
    .local v15, "countEditText":Ljava/lang/String;
    const/4 v14, 0x1

    .line 1441
    .restart local v14    # "count":I
    :try_start_1
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v14

    .line 1445
    :goto_4
    const/4 v1, 0x1

    if-le v14, v1, :cond_d

    .line 1446
    add-int/lit8 v14, v14, -0x1

    .line 1447
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1449
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->priceSubtotalView:Landroid/widget/TextView;

    invoke-direct/range {p0 .. p0}, Lcom/blurb/checkout/MainShadowActivity;->getOrderTotal()D

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/blurb/checkout/MainShadowActivity;->currencyId:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1451
    :cond_d
    const v1, 0x7f0400f0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 1453
    .end local v14    # "count":I
    .end local v15    # "countEditText":Ljava/lang/String;
    :cond_e
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f06004e

    if-ne v1, v2, :cond_0

    .line 1454
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v15

    .line 1455
    .restart local v15    # "countEditText":Ljava/lang/String;
    const/4 v14, 0x1

    .line 1458
    .restart local v14    # "count":I
    :try_start_2
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    move-result v14

    .line 1462
    :goto_5
    const/16 v1, 0x63

    if-ge v14, v1, :cond_f

    .line 1463
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 1464
    add-int/lit8 v14, v14, 0x1

    .line 1465
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1467
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity;->priceSubtotalView:Landroid/widget/TextView;

    invoke-direct/range {p0 .. p0}, Lcom/blurb/checkout/MainShadowActivity;->getOrderTotal()D

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/blurb/checkout/MainShadowActivity;->currencyId:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1469
    :cond_f
    const v1, 0x7f0400b9

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 1459
    :catch_0
    move-exception v1

    goto :goto_5

    .line 1442
    :catch_1
    move-exception v1

    goto/16 :goto_4

    .line 1382
    .end local v15    # "countEditText":Ljava/lang/String;
    :catch_2
    move-exception v1

    goto/16 :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 2331
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2335
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x0

    .line 223
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 230
    const-string v8, "phone"

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    .line 232
    .local v7, "tmgr":Landroid/telephony/TelephonyManager;
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->getSoftwareVersion()Ljava/lang/String;

    move-result-object v8

    sget-object v9, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    sget-object v10, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v12

    invoke-static {v8, v9, v10, v11, v12}, Lcom/blurb/checkout/WebService;->setHttpUserAgent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    const v8, 0x7f03000d

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->setContentView(I)V

    .line 236
    const v8, 0x7f06000f

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 237
    .local v6, "titleView":Landroid/widget/TextView;
    const v8, 0x7f0400a0

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(I)V

    .line 239
    const-string v8, "us"

    iput-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;

    .line 241
    const v8, 0x7f060044

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 242
    .local v2, "nextScreenButton":Landroid/view/View;
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    const v8, 0x7f060050

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->discountText:Landroid/widget/TextView;

    .line 248
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->setListenersForOrderCount()V

    .line 250
    const v8, 0x7f060045

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 251
    .local v4, "paymentid":Landroid/view/View;
    new-instance v8, Lcom/blurb/checkout/MainShadowActivity$2;

    invoke-direct {v8, p0}, Lcom/blurb/checkout/MainShadowActivity$2;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v4, v8}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 264
    const v8, 0x7f060049

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 265
    .local v5, "picturemessage":Landroid/view/View;
    new-instance v8, Lcom/blurb/checkout/MainShadowActivity$3;

    invoke-direct {v8, p0}, Lcom/blurb/checkout/MainShadowActivity$3;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v5, v8}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 278
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 280
    .local v1, "intent":Landroid/content/Intent;
    const-string v8, "episode_id"

    invoke-virtual {v1, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->episodeId:Ljava/lang/String;

    .line 300
    invoke-direct {p0, v1}, Lcom/blurb/checkout/MainShadowActivity;->handleIntent(Landroid/content/Intent;)V

    .line 302
    const v8, 0x7f06004f

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->priceSubtotalView:Landroid/widget/TextView;

    .line 304
    const v8, 0x7f06004a

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->displayTitle:Landroid/widget/TextView;

    .line 305
    const v8, 0x7f06004c

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->displayNumPages:Landroid/widget/TextView;

    .line 306
    const v8, 0x7f06004b

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->displayBookType:Landroid/widget/TextView;

    .line 308
    iget-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->displayTitle:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/blurb/checkout/MainShadowActivity;->title:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    const v8, 0x7f0400b7

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->numPages:Ljava/lang/String;

    aput-object v10, v9, v13

    invoke-virtual {p0, v8, v9}, Lcom/blurb/checkout/MainShadowActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 311
    .local v3, "pageDisplay":Ljava/lang/String;
    iget-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->displayNumPages:Landroid/widget/TextView;

    invoke-virtual {v8, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    iget-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->displayBookType:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/blurb/checkout/MainShadowActivity;->coverType:Ljava/lang/String;

    iget-object v10, p0, Lcom/blurb/checkout/MainShadowActivity;->bookType:Ljava/lang/String;

    invoke-static {p0, v9, v10}, Lcom/blurb/checkout/MainShadowActivity;->getBookType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    const v8, 0x7f06001c

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->displayCoverUri:Landroid/widget/ImageView;

    .line 317
    iget-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->coverData:[B

    if-eqz v8, :cond_1

    .line 318
    iget-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->coverData:[B

    iget-object v9, p0, Lcom/blurb/checkout/MainShadowActivity;->coverData:[B

    array-length v9, v9

    invoke-static {v8, v13, v9}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 319
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 320
    iget-object v8, p0, Lcom/blurb/checkout/MainShadowActivity;->displayCoverUri:Landroid/widget/ImageView;

    invoke-virtual {v8, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 329
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :goto_0
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->loadCountryNames()V

    .line 331
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->startCheckNewerVersion()V

    .line 333
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->startUpload()V

    .line 335
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->updateCountryFields()V

    .line 337
    const v8, 0x7f060046

    invoke-virtual {p0, v8}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lcom/blurb/checkout/ui/EnhancedScrollView;

    new-instance v9, Lcom/blurb/checkout/MainShadowActivity$4;

    invoke-direct {v9, p0}, Lcom/blurb/checkout/MainShadowActivity$4;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v8, v9}, Lcom/blurb/checkout/ui/EnhancedScrollView;->setOnScrollListener(Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;)V

    .line 370
    return-void

    .line 322
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    const-string v8, "BlurbShadowApp"

    const-string v9, "Cover URI could not be decoded!"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 325
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    const-string v8, "BlurbShadowApp"

    const-string v9, "No cover URI!"

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "id"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    const v5, 0x7f040089

    const v4, 0x7f04004a

    const/4 v3, 0x0

    .line 1755
    const/4 v0, 0x0

    .line 1756
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const/4 v1, 0x0

    .line 1758
    .local v1, "dialog":Landroid/app/Dialog;
    packed-switch p1, :pswitch_data_0

    .line 1808
    :goto_0
    if-eqz v1, :cond_0

    .line 1811
    .end local v1    # "dialog":Landroid/app/Dialog;
    :goto_1
    return-object v1

    .line 1760
    .restart local v1    # "dialog":Landroid/app/Dialog;
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1761
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0400b2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/blurb/checkout/MainShadowActivity$20;

    invoke-direct {v3, p0}, Lcom/blurb/checkout/MainShadowActivity$20;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1772
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 1773
    goto :goto_0

    .line 1775
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1776
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f0400b3

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/blurb/checkout/MainShadowActivity$21;

    invoke-direct {v3, p0}, Lcom/blurb/checkout/MainShadowActivity$21;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1787
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 1788
    goto :goto_0

    .line 1790
    :pswitch_2
    new-instance v0, Landroid/app/AlertDialog$Builder;

    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1791
    .restart local v0    # "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f040082

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/blurb/checkout/MainShadowActivity$22;

    invoke-direct {v3, p0}, Lcom/blurb/checkout/MainShadowActivity$22;-><init>(Lcom/blurb/checkout/MainShadowActivity;)V

    invoke-virtual {v2, v4, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1802
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 1803
    goto :goto_0

    .line 1811
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v1

    goto :goto_1

    .line 1758
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1605
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 1606
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity;->finish()V

    .line 1607
    const/4 v0, 0x1

    .line 1609
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 520
    invoke-direct {p0, p1}, Lcom/blurb/checkout/MainShadowActivity;->handleIntent(Landroid/content/Intent;)V

    .line 522
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 523
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->mySSOTokenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/blurb/checkout/MainShadowActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 566
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 567
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 571
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity;->mySSOTokenReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.msc.action.ACCESSTOKEN_V02_RESPONSE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/blurb/checkout/MainShadowActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 573
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 574
    return-void
.end method

.method public sendSsoBroadcast(Ljava/lang/String;)V
    .locals 3
    .param p1, "expiredAccessToken"    # Ljava/lang/String;

    .prologue
    .line 1737
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->getSamsungAccountVersion()I

    move-result v0

    .line 1738
    .local v0, "samsungAccountVersion":I
    if-nez v0, :cond_0

    .line 1740
    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->showDialog(ILandroid/os/Bundle;)Z

    .line 1748
    :goto_0
    return-void

    .line 1743
    :cond_0
    const v1, 0x24ab8

    if-lt v0, v1, :cond_1

    .line 1744
    invoke-direct {p0}, Lcom/blurb/checkout/MainShadowActivity;->startGetAccessTokenActivity()V

    goto :goto_0

    .line 1746
    :cond_1
    invoke-direct {p0, p1}, Lcom/blurb/checkout/MainShadowActivity;->startGetAccessTokenBroadcast(Ljava/lang/String;)V

    goto :goto_0
.end method
