.class public Lcom/blurb/checkout/WebService;
.super Ljava/lang/Object;
.source "WebService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/WebService$HttpResult;
    }
.end annotation


# static fields
.field private static httpClient:Landroid/net/http/AndroidHttpClient;

.field private static httpUserAgent:Ljava/lang/String;

.field private static sDeviceId:Ljava/lang/String;

.field private static sNetworkOperator:Ljava/lang/String;

.field static uploadHttpClient:Landroid/net/http/AndroidHttpClient;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    return-void
.end method

.method public static getDeviceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/blurb/checkout/WebService;->sDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method static getHttpClient()Landroid/net/http/AndroidHttpClient;
    .locals 3

    .prologue
    const/16 v2, 0x7530

    .line 201
    sget-object v1, Lcom/blurb/checkout/WebService;->httpClient:Landroid/net/http/AndroidHttpClient;

    if-nez v1, :cond_0

    .line 202
    sget-object v1, Lcom/blurb/checkout/WebService;->httpUserAgent:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    sput-object v1, Lcom/blurb/checkout/WebService;->httpClient:Landroid/net/http/AndroidHttpClient;

    .line 204
    sget-object v1, Lcom/blurb/checkout/WebService;->httpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 205
    .local v0, "params":Lorg/apache/http/params/HttpParams;
    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 206
    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 209
    :cond_0
    sget-object v1, Lcom/blurb/checkout/WebService;->httpClient:Landroid/net/http/AndroidHttpClient;

    return-object v1
.end method

.method public static getNetworkOperator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    sget-object v0, Lcom/blurb/checkout/WebService;->sNetworkOperator:Ljava/lang/String;

    return-object v0
.end method

.method static getUploadHttpClient()Landroid/net/http/AndroidHttpClient;
    .locals 2

    .prologue
    .line 220
    sget-object v1, Lcom/blurb/checkout/WebService;->uploadHttpClient:Landroid/net/http/AndroidHttpClient;

    if-nez v1, :cond_0

    .line 221
    sget-object v1, Lcom/blurb/checkout/WebService;->httpUserAgent:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v1

    sput-object v1, Lcom/blurb/checkout/WebService;->uploadHttpClient:Landroid/net/http/AndroidHttpClient;

    .line 223
    sget-object v1, Lcom/blurb/checkout/WebService;->uploadHttpClient:Landroid/net/http/AndroidHttpClient;

    invoke-virtual {v1}, Landroid/net/http/AndroidHttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 224
    .local v0, "params":Lorg/apache/http/params/HttpParams;
    const/16 v1, 0x7530

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 225
    const v1, 0x2bf20

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 228
    :cond_0
    sget-object v1, Lcom/blurb/checkout/WebService;->uploadHttpClient:Landroid/net/http/AndroidHttpClient;

    return-object v1
.end method

.method static readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 3
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 121
    const-string v0, ""

    .line 122
    .local v0, "result":Ljava/lang/String;
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 123
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    .line 124
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 126
    :cond_0
    return-object v0
.end method

.method static readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .param p1, "tag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 140
    const/4 v1, 0x2

    sget-object v2, Lcom/blurb/checkout/BlurbAPI;->ns:Ljava/lang/String;

    invoke-interface {p0, v1, v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 142
    invoke-static {p0}, Lcom/blurb/checkout/WebService;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 144
    .local v0, "text":Ljava/lang/String;
    const/4 v1, 0x3

    sget-object v2, Lcom/blurb/checkout/BlurbAPI;->ns:Ljava/lang/String;

    invoke-interface {p0, v1, v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 146
    return-object v0
.end method

.method static setHttpUserAgent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "appVersion"    # Ljava/lang/String;
    .param p1, "osVersion"    # Ljava/lang/String;
    .param p2, "modelId"    # Ljava/lang/String;
    .param p3, "deviceId"    # Ljava/lang/String;
    .param p4, "networkOperator"    # Ljava/lang/String;

    .prologue
    .line 178
    const-string v0, "BlurbCheckout/%1$s (Android %2$s %3$s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const/4 v2, 0x2

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/blurb/checkout/WebService;->httpUserAgent:Ljava/lang/String;

    .line 179
    sput-object p3, Lcom/blurb/checkout/WebService;->sDeviceId:Ljava/lang/String;

    .line 180
    if-nez p4, :cond_0

    .line 181
    const-string p4, ""

    .line 182
    :cond_0
    sput-object p4, Lcom/blurb/checkout/WebService;->sNetworkOperator:Ljava/lang/String;

    .line 183
    return-void
.end method

.method static skipTag(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 157
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 158
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 160
    :cond_0
    const/4 v0, 0x1

    .line 161
    .local v0, "depth":I
    :goto_0
    if-eqz v0, :cond_1

    .line 162
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 167
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 164
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    .line 165
    goto :goto_0

    .line 171
    :cond_1
    return-void

    .line 162
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
