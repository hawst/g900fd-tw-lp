.class Lcom/blurb/checkout/PaymentActivity$12;
.super Ljava/lang/Object;
.source "PaymentActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/PaymentActivity;->popupCountryView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/PaymentActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/PaymentActivity;)V
    .locals 0

    .prologue
    .line 837
    iput-object p1, p0, Lcom/blurb/checkout/PaymentActivity$12;->this$0:Lcom/blurb/checkout/PaymentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 842
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$12;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->countriesListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$600(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 844
    .local v0, "country":Ljava/lang/String;
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$12;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->countryEditText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$700(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 845
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$12;->this$0:Lcom/blurb/checkout/PaymentActivity;

    iget-object v2, p0, Lcom/blurb/checkout/PaymentActivity$12;->this$0:Lcom/blurb/checkout/PaymentActivity;

    invoke-static {v2, v0}, Lcom/blurb/checkout/MainShadowActivity;->getCountryCode(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/blurb/checkout/PaymentActivity;->access$802(Lcom/blurb/checkout/PaymentActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 846
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$12;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # invokes: Lcom/blurb/checkout/PaymentActivity;->updateCountryFields()V
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$900(Lcom/blurb/checkout/PaymentActivity;)V

    .line 848
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$12;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->countryDialog:Landroid/app/Dialog;
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$1000(Lcom/blurb/checkout/PaymentActivity;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 849
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$12;->this$0:Lcom/blurb/checkout/PaymentActivity;

    const/4 v2, 0x0

    # setter for: Lcom/blurb/checkout/PaymentActivity;->countryDialog:Landroid/app/Dialog;
    invoke-static {v1, v2}, Lcom/blurb/checkout/PaymentActivity;->access$1002(Lcom/blurb/checkout/PaymentActivity;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 850
    return-void
.end method
