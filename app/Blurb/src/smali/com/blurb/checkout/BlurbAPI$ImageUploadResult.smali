.class public Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;
.super Lcom/blurb/checkout/BlurbAPI$BlurbResult;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ImageUploadResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/BlurbAPI$ImageUploadResult$ImageUploadResponseParser;
    }
.end annotation


# static fields
.field public static final ERROR_NO_IMAGE:I = -0x64


# instance fields
.field public imageUrl:Ljava/lang/String;

.field public photoId:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1267
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 1268
    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 1270
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 1271
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->setHttpStatus(I)V

    .line 1272
    return-void
.end method


# virtual methods
.method public bridge synthetic getErrors()Ljava/util/List;
    .locals 1

    .prologue
    .line 1232
    invoke-super {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;->getErrors()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getHttpStatus()I
    .locals 1

    .prologue
    .line 1232
    invoke-super {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;->getHttpStatus()I

    move-result v0

    return v0
.end method

.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 7
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 1275
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->setHttpStatus(I)V

    .line 1280
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 1281
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 1282
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->setHttpStatus(I)V

    .line 1308
    :goto_0
    return-void

    .line 1286
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_1

    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0x190

    if-ne v5, v6, :cond_2

    .line 1288
    :cond_1
    :try_start_0
    const-string v5, "UTF-8"

    invoke-static {v1, v5}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1289
    .local v4, "responseString":Ljava/lang/String;
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1290
    .local v3, "reader":Ljava/io/StringReader;
    new-instance v2, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult$ImageUploadResponseParser;

    const/4 v5, 0x0

    invoke-direct {v2, p0, v5}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult$ImageUploadResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;Lcom/blurb/checkout/BlurbAPI$1;)V

    .line 1292
    .local v2, "parser":Lcom/blurb/checkout/BlurbAPI$ImageUploadResult$ImageUploadResponseParser;
    # invokes: Lcom/blurb/checkout/BlurbAPI$ImageUploadResult$ImageUploadResponseParser;->parseXml(Ljava/io/Reader;)V
    invoke-static {v2, v3}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult$ImageUploadResponseParser;->access$1200(Lcom/blurb/checkout/BlurbAPI$ImageUploadResult$ImageUploadResponseParser;Ljava/io/Reader;)V

    .line 1293
    invoke-virtual {v2}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult$ImageUploadResponseParser;->getErrors()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->errors:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1304
    .end local v2    # "parser":Lcom/blurb/checkout/BlurbAPI$ImageUploadResult$ImageUploadResponseParser;
    .end local v3    # "reader":Ljava/io/StringReader;
    .end local v4    # "responseString":Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1305
    :catch_0
    move-exception v0

    .line 1306
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1294
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1295
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v5, -0x2

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->setHttpStatus(I)V

    .line 1296
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1297
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 1298
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 1299
    const/4 v5, -0x4

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$ImageUploadResult;->setHttpStatus(I)V

    goto :goto_1
.end method
