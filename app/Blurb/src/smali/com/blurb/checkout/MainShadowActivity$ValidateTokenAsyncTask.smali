.class Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;
.super Lcom/blurb/checkout/ActivityAsyncTask;
.source "MainShadowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/MainShadowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValidateTokenAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/blurb/checkout/ActivityAsyncTask",
        "<",
        "Landroid/app/Activity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;",
        ">;"
    }
.end annotation


# instance fields
.field private locale:Ljava/lang/String;

.field final synthetic this$0:Lcom/blurb/checkout/MainShadowActivity;

.field private tokenString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/blurb/checkout/MainShadowActivity;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "a"    # Landroid/app/Activity;
    .param p3, "token"    # Ljava/lang/String;
    .param p4, "inLocale"    # Ljava/lang/String;

    .prologue
    .line 1901
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    .line 1902
    invoke-direct {p0, p2}, Lcom/blurb/checkout/ActivityAsyncTask;-><init>(Landroid/app/Activity;)V

    .line 1904
    iput-object p3, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->tokenString:Ljava/lang/String;

    .line 1905
    iput-object p4, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->locale:Ljava/lang/String;

    .line 1906
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/16 v4, 0xc8

    .line 1917
    new-instance v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;-><init>(Lcom/blurb/checkout/MainShadowActivity$1;)V

    .line 1919
    .local v0, "result":Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->tokenString:Ljava/lang/String;

    invoke-static {v1}, Lcom/blurb/checkout/SamsungAPI;->getValidateToken(Ljava/lang/String;)Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    move-result-object v1

    iput-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->tokenResult:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    .line 1920
    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->tokenResult:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->getHttpStatus()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->setHttpStatus(I)V

    .line 1922
    invoke-virtual {v0}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->getHttpStatus()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 1923
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->tokenString:Ljava/lang/String;

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->tokenResult:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    iget-object v2, v2, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->userId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/blurb/checkout/SamsungAPI;->getUserInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    move-result-object v1

    iput-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->userInfoResult:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    .line 1924
    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->userInfoResult:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->getHttpStatus()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->setHttpStatus(I)V

    .line 1926
    invoke-virtual {v0}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->getHttpStatus()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 1927
    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->tokenResult:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    iget-object v1, v1, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->userId:Ljava/lang/String;

    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->tokenString:Ljava/lang/String;

    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->locale:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/blurb/checkout/BlurbAPI;->postCreateUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    move-result-object v1

    iput-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->createUserResult:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    .line 1928
    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->createUserResult:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->getHttpStatus()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->setHttpStatus(I)V

    .line 1930
    invoke-virtual {v0}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->getHttpStatus()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 1931
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->numPages:Ljava/lang/String;
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1500(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    # setter for: Lcom/blurb/checkout/MainShadowActivity;->pageCount:I
    invoke-static {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1402(Lcom/blurb/checkout/MainShadowActivity;I)I

    .line 1933
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->coverType:Ljava/lang/String;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$1600(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->bookType:Ljava/lang/String;
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1700(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->paperType:Ljava/lang/String;
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->access$1800(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->pageCount:I
    invoke-static {v4}, Lcom/blurb/checkout/MainShadowActivity;->access$1400(Lcom/blurb/checkout/MainShadowActivity;)I

    move-result v4

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, Lcom/blurb/checkout/BlurbAPI;->getBookPricing(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    move-result-object v1

    iput-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->bookPricingResult:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    .line 1934
    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->bookPricingResult:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->getHttpStatus()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->setHttpStatus(I)V

    .line 1939
    :cond_0
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1897
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->doInBackground([Ljava/lang/Void;)Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;

    move-result-object v0

    return-object v0
.end method

.method public getProgressMessageId()I
    .locals 1

    .prologue
    .line 1910
    const v0, 0x7f040049

    return v0
.end method

.method protected onPostExecute(Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;)V
    .locals 20
    .param p1, "result"    # Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;

    .prologue
    .line 1947
    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->hideProgress()V

    .line 1949
    if-eqz p1, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1950
    invoke-virtual/range {p1 .. p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->getHttpStatus()I

    move-result v1

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->getHttpStatus()I

    move-result v1

    const/16 v2, 0x190

    if-ne v1, v2, :cond_16

    .line 1951
    :cond_0
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->tokenResult:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    if-eqz v1, :cond_2

    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->tokenResult:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->getHttpStatus()I

    move-result v1

    const/16 v2, 0x190

    if-ne v1, v2, :cond_2

    const-string v1, "LIC_4102"

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->tokenResult:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    iget-object v2, v2, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->errorCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1953
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->tokenString:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->sendSsoBroadcast(Ljava/lang/String;)V

    .line 2065
    :cond_1
    :goto_0
    return-void

    .line 1957
    :cond_2
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->userInfoResult:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    if-eqz v1, :cond_3

    .line 1958
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->userInfoResult:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->getHttpStatus()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_3

    .line 1961
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->userInfoResult:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    iget-object v2, v2, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->givenName:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->userInfoResult:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    iget-object v3, v3, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->familyName:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->userInfoResult:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    iget-object v4, v4, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->postalCodeText:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->userInfoResult:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    iget-object v5, v5, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->countryCode:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->userInfoResult:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    iget-object v6, v6, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->loginID:Ljava/lang/String;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->setUserInformation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v1 .. v6}, Lcom/blurb/checkout/MainShadowActivity;->access$1900(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1964
    :cond_3
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->bookPricingResult:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    if-eqz v1, :cond_8

    .line 1965
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->bookPricingResult:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    iget-object v0, v1, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->prices:Ljava/util/List;

    move-object/from16 v17, v0

    .line 1966
    .local v17, "resultPrices":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbAPI$Price;>;"
    if-eqz v17, :cond_9

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_9

    .line 1970
    const/16 v16, 0x0

    .line 1971
    .local v16, "price":Lcom/blurb/checkout/BlurbAPI$Price;
    const/16 v19, 0x0

    .line 1973
    .local v19, "usdPrice":Lcom/blurb/checkout/BlurbAPI$Price;
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/blurb/checkout/BlurbAPI$Price;

    .line 1974
    .local v7, "aPrice":Lcom/blurb/checkout/BlurbAPI$Price;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->currencyId:Ljava/lang/String;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$200(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v7, Lcom/blurb/checkout/BlurbAPI$Price;->currency:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1975
    move-object/from16 v16, v7

    .line 1977
    :cond_5
    const-string v1, "USD"

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1978
    move-object/from16 v19, v7

    goto :goto_1

    .line 1981
    .end local v7    # "aPrice":Lcom/blurb/checkout/BlurbAPI$Price;
    :cond_6
    if-nez v16, :cond_7

    .line 1982
    move-object/from16 v16, v19

    .line 1985
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Price;->value:Ljava/lang/String;

    # setter for: Lcom/blurb/checkout/MainShadowActivity;->singlePrice:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->access$2002(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1987
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->priceSubtotalView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$300(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/TextView;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->getOrderTotal()D
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->access$100(Lcom/blurb/checkout/MainShadowActivity;)D

    move-result-wide v2

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/blurb/checkout/BlurbAPI$Price;->currency:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1996
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v16    # "price":Lcom/blurb/checkout/BlurbAPI$Price;
    .end local v17    # "resultPrices":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbAPI$Price;>;"
    .end local v19    # "usdPrice":Lcom/blurb/checkout/BlurbAPI$Price;
    :cond_8
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->createUserResult:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    if-eqz v1, :cond_1

    .line 1997
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->createUserResult:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->getErrors()Ljava/util/List;

    move-result-object v11

    .line 1998
    .local v11, "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    if-eqz v11, :cond_a

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_a

    .line 1999
    const/4 v1, 0x0

    invoke-interface {v11, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;

    .line 2001
    .local v9, "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    iget-object v2, v9, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->message:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/blurb/checkout/BlurbAPI;->codeToMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2002
    .local v10, "errorMessage":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const/4 v2, 0x1

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->showError(Ljava/lang/String;Z)V
    invoke-static {v1, v10, v2}, Lcom/blurb/checkout/MainShadowActivity;->access$000(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 1989
    .end local v9    # "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    .end local v10    # "errorMessage":Ljava/lang/String;
    .end local v11    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    .restart local v17    # "resultPrices":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbAPI$Price;>;"
    :cond_9
    const-string v1, "BlurbShadowApp"

    const-string v2, "BookPricingApiAsyncTask.onPostExecute no prices found"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1990
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/blurb/checkout/MainShadowActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto/16 :goto_0

    .line 2004
    .end local v17    # "resultPrices":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbAPI$Price;>;"
    .restart local v11    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    :cond_a
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->createUserResult:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    iget-object v2, v2, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->sessionId:Ljava/lang/String;

    # setter for: Lcom/blurb/checkout/MainShadowActivity;->sessionId:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->access$2102(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2008
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->createUserResult:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    iget-object v0, v1, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->shippingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

    move-object/from16 v18, v0

    .line 2009
    .local v18, "shippingAddress":Lcom/blurb/checkout/BlurbAPI$Address;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->createUserResult:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    iget-object v2, v2, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->billingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

    iput-object v2, v1, Lcom/blurb/checkout/MainShadowActivity;->billingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

    .line 2010
    if-eqz v18, :cond_13

    .line 2011
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    if-eqz v1, :cond_b

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_b

    .line 2012
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    const/4 v8, 0x1

    .line 2013
    .local v8, "countryCodeChanged":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    # setter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1002(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 2014
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryEditText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$900(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/TextView;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/blurb/checkout/MainShadowActivity;->getCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2016
    if-eqz v8, :cond_b

    .line 2017
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->updateCountryFields()V
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$1100(Lcom/blurb/checkout/MainShadowActivity;)V

    .line 2020
    .end local v8    # "countryCodeChanged":Z
    :cond_b
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    if-eqz v1, :cond_c

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_c

    .line 2021
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2200(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2022
    :cond_c
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    if-eqz v1, :cond_d

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_d

    .line 2023
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2300(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2025
    :cond_d
    const-string v1, "us"

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "ca"

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "au"

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 2029
    :cond_e
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    if-eqz v1, :cond_f

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_f

    .line 2030
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2400(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2036
    :cond_f
    :goto_3
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    if-eqz v1, :cond_10

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_10

    .line 2037
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2039
    :cond_10
    const-string v1, "kr"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    const-string v1, "jp"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 2040
    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const v2, 0x7f060008

    invoke-virtual {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/EditText;

    .line 2041
    .local v12, "et":Landroid/widget/EditText;
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    if-eqz v1, :cond_12

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_12

    if-eqz v12, :cond_12

    .line 2042
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    invoke-virtual {v12, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2044
    :cond_12
    const-string v1, "kr"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->phoneNumber:Ljava/lang/String;

    if-eqz v1, :cond_13

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_13

    .line 2045
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const v2, 0x7f06000d

    invoke-virtual {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/EditText;

    .line 2046
    .local v15, "phoneEditText":Landroid/widget/EditText;
    if-eqz v15, :cond_13

    .line 2047
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v15, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2053
    .end local v12    # "et":Landroid/widget/EditText;
    .end local v15    # "phoneEditText":Landroid/widget/EditText;
    :cond_13
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->setFirstFocus()V
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2600(Lcom/blurb/checkout/MainShadowActivity;)V

    goto/16 :goto_0

    .line 2012
    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 2032
    :cond_15
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateName:Ljava/lang/String;

    if-eqz v1, :cond_f

    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_f

    .line 2033
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2400(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2059
    .end local v11    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    .end local v18    # "shippingAddress":Lcom/blurb/checkout/BlurbAPI$Address;
    :cond_16
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;->getMessageForStatus(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    .line 2060
    .local v14, "message":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const/4 v2, 0x1

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->showError(Ljava/lang/String;Z)V
    invoke-static {v1, v14, v2}, Lcom/blurb/checkout/MainShadowActivity;->access$000(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1897
    check-cast p1, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAsyncTask;->onPostExecute(Lcom/blurb/checkout/MainShadowActivity$ValidateTokenAndCreateUserResult;)V

    return-void
.end method
