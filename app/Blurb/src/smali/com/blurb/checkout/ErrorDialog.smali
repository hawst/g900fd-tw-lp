.class public Lcom/blurb/checkout/ErrorDialog;
.super Landroid/app/Dialog;
.source "ErrorDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"


# instance fields
.field private buttonTextId:I

.field private errorText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "errorText"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 19
    const v0, 0x7f040046

    iput v0, p0, Lcom/blurb/checkout/ErrorDialog;->buttonTextId:I

    .line 28
    const v0, 0x7f040089

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/blurb/checkout/ErrorDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 30
    iput-object p2, p0, Lcom/blurb/checkout/ErrorDialog;->errorText:Ljava/lang/String;

    .line 32
    const-string v0, "BlurbShadowApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ErrorDialog: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 33
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/blurb/checkout/ErrorDialog;->dismiss()V

    .line 39
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v2, 0x7f03000a

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/ErrorDialog;->setContentView(I)V

    .line 47
    const v2, 0x7f06001a

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/ErrorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 48
    .local v0, "btn_exit":Landroid/widget/Button;
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    iget v2, p0, Lcom/blurb/checkout/ErrorDialog;->buttonTextId:I

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 51
    const v2, 0x7f060019

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/ErrorDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 53
    .local v1, "error":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/blurb/checkout/ErrorDialog;->errorText:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 54
    iget-object v2, p0, Lcom/blurb/checkout/ErrorDialog;->errorText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    :cond_0
    return-void
.end method

.method public setButtonText(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/blurb/checkout/ErrorDialog;->buttonTextId:I

    .line 23
    return-void
.end method
