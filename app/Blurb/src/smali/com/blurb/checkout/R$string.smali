.class public final Lcom/blurb/checkout/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final NO:I = 0x7f04009d

.field public static final YES:I = 0x7f04009c

.field public static final address:I = 0x7f0400ae

.field public static final address_billing_address_can_not_be_blank:I = 0x7f04006c

.field public static final address_city_can_not_be_blank:I = 0x7f040051

.field public static final address_city_too_long:I = 0x7f040056

.field public static final address_first_name_too_long:I = 0x7f040052

.field public static final address_firstname_can_not_be_blank:I = 0x7f04004e

.field public static final address_invalid_postal_code:I = 0x7f040063

.field public static final address_last_name_too_long:I = 0x7f040053

.field public static final address_lastname_can_not_be_blank:I = 0x7f04004f

.field public static final address_phone_cant_be_blank:I = 0x7f040058

.field public static final address_phone_format_invalid:I = 0x7f04005a

.field public static final address_phone_must_be_less:I = 0x7f040059

.field public static final address_phone_number_must_include_area_code:I = 0x7f040057

.field public static final address_province_cannot_be_blank:I = 0x7f040065

.field public static final address_state_cannot_be_blank:I = 0x7f04005e

.field public static final address_state_cannot_be_blank_au:I = 0x7f04005f

.field public static final address_state_cannot_be_blank_ca:I = 0x7f040060

.field public static final address_state_cannot_be_blank_jp:I = 0x7f040061

.field public static final address_state_cannot_be_blank_kr:I = 0x7f040062

.field public static final address_street1_can_not_be_blank:I = 0x7f040050

.field public static final address_street1_too_long:I = 0x7f040054

.field public static final address_street2_too_long:I = 0x7f040055

.field public static final address_zip_code_does_not_exist:I = 0x7f04005b

.field public static final address_zip_code_does_not_match_province:I = 0x7f040064

.field public static final address_zip_code_does_not_match_state:I = 0x7f04005c

.field public static final address_zip_code_is_not_valid:I = 0x7f04005d

.field public static final app_name:I = 0x7f040000

.field public static final book_book_does_not_exist:I = 0x7f040067

.field public static final books_discount_text:I = 0x7f0400b4

.field public static final books_hint_choose_country:I = 0x7f0400b5

.field public static final booktype_magazine:I = 0x7f0400b0

.field public static final cancel:I = 0x7f04004b

.field public static final checkout_error_retrieve_checkout:I = 0x7f0400de

.field public static final checkout_product_missing:I = 0x7f04006b

.field public static final checkout_shipping_address_invalid:I = 0x7f040069

.field public static final checkout_shipping_address_missing:I = 0x7f040068

.field public static final checkout_shipping_method_missing:I = 0x7f04006a

.field public static final choose_your_country:I = 0x7f040001

.field public static final city:I = 0x7f040094

.field public static final city_town_locality:I = 0x7f0400a3

.field public static final confirm_name:I = 0x7f0400ed

.field public static final confirm_upload_continue:I = 0x7f0400d2

.field public static final country:I = 0x7f040098

.field public static final country_can_not_be_blank:I = 0x7f04007b

.field public static final country_code_ae:I = 0x7f040002

.field public static final country_code_ar:I = 0x7f040003

.field public static final country_code_at:I = 0x7f040004

.field public static final country_code_au:I = 0x7f040005

.field public static final country_code_be:I = 0x7f040006

.field public static final country_code_bg:I = 0x7f040007

.field public static final country_code_bm:I = 0x7f040008

.field public static final country_code_br:I = 0x7f040009

.field public static final country_code_ca:I = 0x7f04000a

.field public static final country_code_ch:I = 0x7f04000b

.field public static final country_code_cl:I = 0x7f04000c

.field public static final country_code_co:I = 0x7f04000d

.field public static final country_code_cy:I = 0x7f04000e

.field public static final country_code_cz:I = 0x7f04000f

.field public static final country_code_de:I = 0x7f040010

.field public static final country_code_dk:I = 0x7f040011

.field public static final country_code_ee:I = 0x7f040012

.field public static final country_code_es:I = 0x7f040013

.field public static final country_code_fi:I = 0x7f040014

.field public static final country_code_fm:I = 0x7f040015

.field public static final country_code_fr:I = 0x7f040016

.field public static final country_code_gb:I = 0x7f040017

.field public static final country_code_gr:I = 0x7f040018

.field public static final country_code_hk:I = 0x7f040019

.field public static final country_code_hr:I = 0x7f04001a

.field public static final country_code_hu:I = 0x7f04001b

.field public static final country_code_id:I = 0x7f04001c

.field public static final country_code_ie:I = 0x7f04001d

.field public static final country_code_il:I = 0x7f04001e

.field public static final country_code_in:I = 0x7f04001f

.field public static final country_code_is:I = 0x7f040020

.field public static final country_code_it:I = 0x7f040021

.field public static final country_code_jm:I = 0x7f040022

.field public static final country_code_jp:I = 0x7f040023

.field public static final country_code_kr:I = 0x7f040024

.field public static final country_code_ky:I = 0x7f040025

.field public static final country_code_lt:I = 0x7f040026

.field public static final country_code_lu:I = 0x7f040027

.field public static final country_code_lv:I = 0x7f040028

.field public static final country_code_mo:I = 0x7f040029

.field public static final country_code_mt:I = 0x7f04002a

.field public static final country_code_mx:I = 0x7f04002b

.field public static final country_code_my:I = 0x7f04002c

.field public static final country_code_nl:I = 0x7f04002d

.field public static final country_code_no:I = 0x7f04002e

.field public static final country_code_nz:I = 0x7f04002f

.field public static final country_code_pe:I = 0x7f040030

.field public static final country_code_ph:I = 0x7f040031

.field public static final country_code_pl:I = 0x7f040032

.field public static final country_code_pt:I = 0x7f040033

.field public static final country_code_py:I = 0x7f040034

.field public static final country_code_ro:I = 0x7f040035

.field public static final country_code_rs:I = 0x7f040036

.field public static final country_code_ru:I = 0x7f040037

.field public static final country_code_sa:I = 0x7f040038

.field public static final country_code_se:I = 0x7f040039

.field public static final country_code_sg:I = 0x7f04003a

.field public static final country_code_si:I = 0x7f04003b

.field public static final country_code_sk:I = 0x7f04003c

.field public static final country_code_th:I = 0x7f04003d

.field public static final country_code_tr:I = 0x7f04003e

.field public static final country_code_tw:I = 0x7f04003f

.field public static final country_code_ua:I = 0x7f040040

.field public static final country_code_us:I = 0x7f040041

.field public static final country_code_uy:I = 0x7f040042

.field public static final country_code_vg:I = 0x7f040043

.field public static final country_code_vn:I = 0x7f040044

.field public static final country_code_za:I = 0x7f040045

.field public static final county:I = 0x7f0400aa

.field public static final credit_card_card_cv_number_format:I = 0x7f040072

.field public static final credit_card_card_cv_number_missing:I = 0x7f040071

.field public static final credit_card_card_expired:I = 0x7f04007a

.field public static final credit_card_card_first_name_format:I = 0x7f04006e

.field public static final credit_card_card_first_name_missing:I = 0x7f04006d

.field public static final credit_card_card_last_name_format:I = 0x7f040070

.field public static final credit_card_card_last_name_missing:I = 0x7f04006f

.field public static final credit_card_card_month_format:I = 0x7f040077

.field public static final credit_card_card_month_missing:I = 0x7f040076

.field public static final credit_card_card_number_format:I = 0x7f040074

.field public static final credit_card_card_number_missing:I = 0x7f040073

.field public static final credit_card_card_type_not_accepted:I = 0x7f040075

.field public static final credit_card_card_year_format:I = 0x7f040079

.field public static final credit_card_card_year_missing:I = 0x7f040078

.field public static final credit_card_masked_prefix:I = 0x7f0400d1

.field public static final credit_card_number:I = 0x7f0400c1

.field public static final creditcard_separator:I = 0x7f0400c5

.field public static final discount:I = 0x7f040092

.field public static final done:I = 0x7f0400b1

.field public static final error:I = 0x7f040089

.field public static final error_arguments:I = 0x7f04007e

.field public static final error_text:I = 0x7f040047

.field public static final error_unrecoverable_body:I = 0x7f0400f2

.field public static final error_unrecoverable_title:I = 0x7f0400e8

.field public static final exit:I = 0x7f040046

.field public static final family_name:I = 0x7f040096

.field public static final first_name:I = 0x7f040095

.field public static final gateway_address_doesnt_match_billing_address:I = 0x7f0400e4

.field public static final gateway_card_expired:I = 0x7f0400e0

.field public static final gateway_insufficient_funds:I = 0x7f0400e3

.field public static final gateway_postal_code_doesnt_match_billing_address:I = 0x7f0400e6

.field public static final gateway_security_code_declined_by_amex:I = 0x7f0400e2

.field public static final gateway_security_code_declined_by_bank:I = 0x7f0400e1

.field public static final gateway_technical_issues:I = 0x7f0400e7

.field public static final gateway_timeout:I = 0x7f04007f

.field public static final gateway_unable_to_process:I = 0x7f0400df

.field public static final gateway_zipcode_doesnt_match_billing_address:I = 0x7f0400e5

.field public static final http_error_auth:I = 0x7f040088

.field public static final http_error_message_code:I = 0x7f040083

.field public static final http_error_message_failed:I = 0x7f040084

.field public static final http_error_message_io:I = 0x7f040086

.field public static final http_error_message_protocol:I = 0x7f040085

.field public static final http_error_message_xml:I = 0x7f040087

.field public static final image_render_failed:I = 0x7f04008a

.field public static final images_have_not_been_transmitted_yet:I = 0x7f0400c0

.field public static final images_received_from_episode_app:I = 0x7f04008b

.field public static final imagewrap7x7:I = 0x7f04009b

.field public static final internal_server_error:I = 0x7f040081

.field public static final invalid_characters:I = 0x7f04009e

.field public static final invalid_sso_provider:I = 0x7f04007d

.field public static final invalid_upstream_response:I = 0x7f040080

.field public static final island:I = 0x7f0400a9

.field public static final locality:I = 0x7f0400a5

.field public static final max_quantity:I = 0x7f0400b9

.field public static final na:I = 0x7f04004d

.field public static final name:I = 0x7f040097

.field public static final none:I = 0x7f04004c

.field public static final not_authenticated:I = 0x7f04007c

.field public static final notification_progress:I = 0x7f0400ec

.field public static final ok:I = 0x7f04004a

.field public static final order_complete_customer_service:I = 0x7f0400ca

.field public static final order_complete_id_email:I = 0x7f0400c8

.field public static final order_complete_questions:I = 0x7f0400c9

.field public static final order_complete_text:I = 0x7f0400c6

.field public static final order_complete_thanks:I = 0x7f0400c7

.field public static final order_complete_thanks2:I = 0x7f0400cd

.field public static final order_confirmation:I = 0x7f0400dc

.field public static final order_confirmation_send_to:I = 0x7f0400db

.field public static final order_count_default:I = 0x7f0400b6

.field public static final order_information:I = 0x7f0400cb

.field public static final order_quantity_text:I = 0x7f0400cc

.field public static final order_summary:I = 0x7f0400d0

.field public static final payment_billing_address:I = 0x7f0400bb

.field public static final payment_credit_card:I = 0x7f0400bc

.field public static final payment_credit_card_encrypted:I = 0x7f0400bd

.field public static final payment_cvv:I = 0x7f0400c2

.field public static final payment_information:I = 0x7f04009f

.field public static final payment_mm:I = 0x7f0400c3

.field public static final payment_order:I = 0x7f0400bf

.field public static final payment_same:I = 0x7f0400ba

.field public static final payment_send_info:I = 0x7f0400be

.field public static final payment_yy:I = 0x7f0400c4

.field public static final permission_send:I = 0x7f0400eb

.field public static final permission_start:I = 0x7f0400ea

.field public static final phone_number:I = 0x7f0400ad

.field public static final post_town:I = 0x7f0400a6

.field public static final postal_code:I = 0x7f0400a1

.field public static final postcode:I = 0x7f0400a2

.field public static final precinct:I = 0x7f0400ab

.field public static final prefecture:I = 0x7f0400ac

.field public static final province:I = 0x7f0400a7

.field public static final quantity:I = 0x7f0400b8

.field public static final quantity_decrease:I = 0x7f0400ef

.field public static final quantity_increase:I = 0x7f0400ee

.field public static final quantity_invalid:I = 0x7f0400af

.field public static final quantity_minimum:I = 0x7f0400f0

.field public static final save_fail:I = 0x7f0400da

.field public static final save_fail_title:I = 0x7f0400d9

.field public static final service_unavailable:I = 0x7f040082

.field public static final shipping:I = 0x7f040091

.field public static final shipping_information:I = 0x7f0400a0

.field public static final shipping_num_pages:I = 0x7f0400b7

.field public static final sign_in_cancelled:I = 0x7f0400b2

.field public static final sign_in_failed:I = 0x7f0400b3

.field public static final softcover5x5:I = 0x7f040099

.field public static final softcover7x7:I = 0x7f04009a

.field public static final state:I = 0x7f04008d

.field public static final state_city_zip:I = 0x7f0400ce

.field public static final state_territory:I = 0x7f0400a4

.field public static final story_album:I = 0x7f0400e9

.field public static final street_address:I = 0x7f04008c

.field public static final task_authorizing:I = 0x7f040049

.field public static final task_loading:I = 0x7f040048

.field public static final tax:I = 0x7f040090

.field public static final total:I = 0x7f040093

.field public static final town:I = 0x7f0400a8

.field public static final upload_complete:I = 0x7f0400d4

.field public static final upload_complete_title:I = 0x7f0400d3

.field public static final upload_fail:I = 0x7f0400d8

.field public static final upload_fail_title:I = 0x7f0400d7

.field public static final upload_paused:I = 0x7f0400dd

.field public static final upload_progress:I = 0x7f0400d6

.field public static final upload_progress_title:I = 0x7f0400d5

.field public static final user_user_does_not_exist:I = 0x7f040066

.field public static final version_upgrade_available:I = 0x7f0400f1

.field public static final your_email:I = 0x7f0400cf

.field public static final zip:I = 0x7f04008f

.field public static final zip_code:I = 0x7f04008e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
