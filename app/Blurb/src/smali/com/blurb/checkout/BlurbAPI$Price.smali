.class Lcom/blurb/checkout/BlurbAPI$Price;
.super Ljava/lang/Object;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Price"
.end annotation


# instance fields
.field currency:Ljava/lang/String;

.field symbol:Ljava/lang/String;

.field value:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 215
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "c"    # Ljava/lang/String;
    .param p2, "s"    # Ljava/lang/String;
    .param p3, "v"    # Ljava/lang/String;

    .prologue
    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    iput-object p1, p0, Lcom/blurb/checkout/BlurbAPI$Price;->currency:Ljava/lang/String;

    .line 218
    iput-object p2, p0, Lcom/blurb/checkout/BlurbAPI$Price;->symbol:Ljava/lang/String;

    .line 219
    iput-object p3, p0, Lcom/blurb/checkout/BlurbAPI$Price;->value:Ljava/lang/String;

    .line 220
    return-void
.end method
