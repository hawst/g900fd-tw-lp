.class public Lcom/blurb/checkout/CreditCard;
.super Ljava/lang/Object;
.source "CreditCard.java"


# static fields
.field public static final AMERICANEXPRESS:I = 0x3

.field public static final CVV_MISSING:I = -0x3

.field public static final DISCOVER:I = 0x4

.field public static final INVALID_CVV:I = -0x2

.field public static final JCB:I = 0x5

.field public static final MASTERCARD:I = 0x2

.field public static final UNKNOWN:I = -0x1

.field public static final VISA:I = 0x1

.field public static cards:[Lcom/blurb/checkout/CreditCard;


# instance fields
.field private cardType:I

.field private cvvLength:I

.field private maxLength:I

.field private maxPrefix:Ljava/lang/String;

.field private minLength:I

.field private minPrefix:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v7, 0x5

    const/16 v10, 0xf

    const/4 v8, 0x4

    const/16 v11, 0x10

    const/4 v5, 0x3

    .line 75
    const/16 v0, 0xc

    new-array v13, v0, [Lcom/blurb/checkout/CreditCard;

    const/4 v6, 0x0

    new-instance v0, Lcom/blurb/checkout/CreditCard;

    const/4 v1, 0x1

    const-string v2, "4"

    const/16 v3, 0xd

    const/16 v4, 0xd

    invoke-direct/range {v0 .. v5}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;III)V

    aput-object v0, v13, v6

    const/4 v6, 0x1

    new-instance v0, Lcom/blurb/checkout/CreditCard;

    const/4 v1, 0x1

    const-string v2, "4"

    move v3, v11

    move v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;III)V

    aput-object v0, v13, v6

    const/4 v6, 0x2

    new-instance v0, Lcom/blurb/checkout/CreditCard;

    const/4 v1, 0x2

    const-string v2, "51"

    const-string v3, "55"

    move v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v13, v6

    new-instance v0, Lcom/blurb/checkout/CreditCard;

    const-string v1, "6011"

    invoke-direct {v0, v8, v1, v11, v5}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;II)V

    aput-object v0, v13, v5

    new-instance v0, Lcom/blurb/checkout/CreditCard;

    const-string v2, "622126"

    const-string v3, "622925"

    move v1, v8

    move v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v13, v8

    new-instance v0, Lcom/blurb/checkout/CreditCard;

    const-string v2, "644"

    const-string v3, "649"

    move v1, v8

    move v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v13, v7

    const/4 v6, 0x6

    new-instance v0, Lcom/blurb/checkout/CreditCard;

    const-string v2, "65"

    const-string v3, "65"

    move v1, v8

    move v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;Ljava/lang/String;II)V

    aput-object v0, v13, v6

    const/4 v0, 0x7

    new-instance v1, Lcom/blurb/checkout/CreditCard;

    const-string v2, "34"

    invoke-direct {v1, v5, v2, v10, v8}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;II)V

    aput-object v1, v13, v0

    const/16 v0, 0x8

    new-instance v1, Lcom/blurb/checkout/CreditCard;

    const-string v2, "37"

    invoke-direct {v1, v5, v2, v10, v8}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;II)V

    aput-object v1, v13, v0

    const/16 v0, 0x9

    new-instance v6, Lcom/blurb/checkout/CreditCard;

    const-string v8, "3528"

    const-string v9, "3589"

    move v12, v5

    invoke-direct/range {v6 .. v12}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;Ljava/lang/String;III)V

    aput-object v6, v13, v0

    const/16 v6, 0xa

    new-instance v0, Lcom/blurb/checkout/CreditCard;

    const-string v2, "1800"

    move v1, v7

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;III)V

    aput-object v0, v13, v6

    const/16 v6, 0xb

    new-instance v0, Lcom/blurb/checkout/CreditCard;

    const-string v2, "2131"

    move v1, v7

    move v3, v10

    move v4, v11

    invoke-direct/range {v0 .. v5}, Lcom/blurb/checkout/CreditCard;-><init>(ILjava/lang/String;III)V

    aput-object v0, v13, v6

    sput-object v13, Lcom/blurb/checkout/CreditCard;->cards:[Lcom/blurb/checkout/CreditCard;

    .line 102
    return-void
.end method

.method constructor <init>(ILjava/lang/String;II)V
    .locals 0
    .param p1, "c"    # I
    .param p2, "p"    # Ljava/lang/String;
    .param p3, "l"    # I
    .param p4, "cvl"    # I

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p1, p0, Lcom/blurb/checkout/CreditCard;->cardType:I

    .line 46
    iput-object p2, p0, Lcom/blurb/checkout/CreditCard;->minPrefix:Ljava/lang/String;

    .line 47
    iput-object p2, p0, Lcom/blurb/checkout/CreditCard;->maxPrefix:Ljava/lang/String;

    .line 48
    iput p3, p0, Lcom/blurb/checkout/CreditCard;->minLength:I

    .line 49
    iput p3, p0, Lcom/blurb/checkout/CreditCard;->maxLength:I

    .line 50
    iput p4, p0, Lcom/blurb/checkout/CreditCard;->cvvLength:I

    .line 51
    return-void
.end method

.method constructor <init>(ILjava/lang/String;III)V
    .locals 0
    .param p1, "c"    # I
    .param p2, "p"    # Ljava/lang/String;
    .param p3, "min"    # I
    .param p4, "max"    # I
    .param p5, "cvl"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p1, p0, Lcom/blurb/checkout/CreditCard;->cardType:I

    .line 37
    iput-object p2, p0, Lcom/blurb/checkout/CreditCard;->minPrefix:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/blurb/checkout/CreditCard;->maxPrefix:Ljava/lang/String;

    .line 39
    iput p3, p0, Lcom/blurb/checkout/CreditCard;->minLength:I

    .line 40
    iput p4, p0, Lcom/blurb/checkout/CreditCard;->maxLength:I

    .line 41
    iput p5, p0, Lcom/blurb/checkout/CreditCard;->cvvLength:I

    .line 42
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1, "c"    # I
    .param p2, "minP"    # Ljava/lang/String;
    .param p3, "maxP"    # Ljava/lang/String;
    .param p4, "l"    # I
    .param p5, "cvl"    # I

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput p1, p0, Lcom/blurb/checkout/CreditCard;->cardType:I

    .line 64
    iput-object p2, p0, Lcom/blurb/checkout/CreditCard;->minPrefix:Ljava/lang/String;

    .line 65
    iput-object p3, p0, Lcom/blurb/checkout/CreditCard;->maxPrefix:Ljava/lang/String;

    .line 66
    iput p4, p0, Lcom/blurb/checkout/CreditCard;->minLength:I

    .line 67
    iput p4, p0, Lcom/blurb/checkout/CreditCard;->maxLength:I

    .line 68
    iput p5, p0, Lcom/blurb/checkout/CreditCard;->cvvLength:I

    .line 70
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;III)V
    .locals 0
    .param p1, "c"    # I
    .param p2, "minP"    # Ljava/lang/String;
    .param p3, "maxP"    # Ljava/lang/String;
    .param p4, "min"    # I
    .param p5, "max"    # I
    .param p6, "cvl"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lcom/blurb/checkout/CreditCard;->cardType:I

    .line 55
    iput-object p2, p0, Lcom/blurb/checkout/CreditCard;->minPrefix:Ljava/lang/String;

    .line 56
    iput-object p3, p0, Lcom/blurb/checkout/CreditCard;->maxPrefix:Ljava/lang/String;

    .line 57
    iput p4, p0, Lcom/blurb/checkout/CreditCard;->minLength:I

    .line 58
    iput p5, p0, Lcom/blurb/checkout/CreditCard;->maxLength:I

    .line 59
    iput p6, p0, Lcom/blurb/checkout/CreditCard;->cvvLength:I

    .line 60
    return-void
.end method

.method public static checkCardType(Ljava/lang/String;Ljava/lang/String;)I
    .locals 11
    .param p0, "cardNumber"    # Ljava/lang/String;
    .param p1, "cvv"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    .line 106
    .local v6, "length":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_0

    .line 107
    const/4 v9, -0x3

    .line 139
    :goto_0
    return v9

    .line 109
    :cond_0
    sget-object v0, Lcom/blurb/checkout/CreditCard;->cards:[Lcom/blurb/checkout/CreditCard;

    .local v0, "arr$":[Lcom/blurb/checkout/CreditCard;
    array-length v5, v0

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_6

    aget-object v1, v0, v4

    .line 110
    .local v1, "card":Lcom/blurb/checkout/CreditCard;
    iget v9, v1, Lcom/blurb/checkout/CreditCard;->minLength:I

    if-lt v6, v9, :cond_1

    iget v9, v1, Lcom/blurb/checkout/CreditCard;->maxLength:I

    if-le v6, v9, :cond_2

    .line 109
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 113
    :cond_2
    iget-object v9, v1, Lcom/blurb/checkout/CreditCard;->minPrefix:Ljava/lang/String;

    iget-object v10, v1, Lcom/blurb/checkout/CreditCard;->maxPrefix:Ljava/lang/String;

    if-ne v9, v10, :cond_4

    .line 114
    iget-object v9, v1, Lcom/blurb/checkout/CreditCard;->minPrefix:Ljava/lang/String;

    invoke-virtual {p0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 133
    :cond_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v9

    iget v10, v1, Lcom/blurb/checkout/CreditCard;->cvvLength:I

    if-eq v9, v10, :cond_5

    .line 134
    const/4 v9, -0x2

    goto :goto_0

    .line 117
    :cond_4
    iget-object v9, v1, Lcom/blurb/checkout/CreditCard;->minPrefix:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    iget-object v10, v1, Lcom/blurb/checkout/CreditCard;->maxPrefix:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    if-ne v9, v10, :cond_1

    .line 122
    const/4 v9, 0x0

    iget-object v10, v1, Lcom/blurb/checkout/CreditCard;->minPrefix:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {p0, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 124
    .local v3, "cardPrefix":Ljava/lang/String;
    iget-object v9, v1, Lcom/blurb/checkout/CreditCard;->minPrefix:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    .line 125
    .local v8, "minCardInt":I
    iget-object v9, v1, Lcom/blurb/checkout/CreditCard;->maxPrefix:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 126
    .local v7, "maxCardInt":I
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 128
    .local v2, "cardInt":I
    if-lt v2, v8, :cond_1

    if-le v2, v7, :cond_3

    goto :goto_2

    .line 136
    .end local v2    # "cardInt":I
    .end local v3    # "cardPrefix":Ljava/lang/String;
    .end local v7    # "maxCardInt":I
    .end local v8    # "minCardInt":I
    :cond_5
    iget v9, v1, Lcom/blurb/checkout/CreditCard;->cardType:I

    goto :goto_0

    .line 139
    .end local v1    # "card":Lcom/blurb/checkout/CreditCard;
    :cond_6
    const/4 v9, -0x1

    goto :goto_0
.end method
