.class public Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;
.super Lcom/blurb/checkout/WebService$HttpResult;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GenerateBBFResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;
    }
.end annotation


# instance fields
.field public bookId:Ljava/lang/String;

.field public state:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 594
    invoke-direct {p0}, Lcom/blurb/checkout/WebService$HttpResult;-><init>()V

    .line 595
    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 597
    invoke-direct {p0}, Lcom/blurb/checkout/WebService$HttpResult;-><init>()V

    .line 598
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->setHttpStatus(I)V

    .line 599
    return-void
.end method


# virtual methods
.method public bridge synthetic getHttpStatus()I
    .locals 1

    .prologue
    .line 555
    invoke-super {p0}, Lcom/blurb/checkout/WebService$HttpResult;->getHttpStatus()I

    move-result v0

    return v0
.end method

.method public isProcessing()Z
    .locals 2

    .prologue
    .line 591
    const-string v0, "PROCESSING"

    iget-object v1, p0, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 7
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 602
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->setHttpStatus(I)V

    .line 607
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 608
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 609
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->setHttpStatus(I)V

    .line 638
    :goto_0
    return-void

    .line 613
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_1

    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0x190

    if-ne v5, v6, :cond_2

    .line 615
    :cond_1
    :try_start_0
    const-string v5, "UTF-8"

    invoke-static {v1, v5}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 616
    .local v4, "responseString":Ljava/lang/String;
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 621
    .local v3, "reader":Ljava/io/StringReader;
    new-instance v2, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;

    const/4 v5, 0x0

    invoke-direct {v2, p0, v5}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;Lcom/blurb/checkout/BlurbAPI$1;)V

    .line 623
    .local v2, "parser":Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;
    # invokes: Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;->parseXml(Ljava/io/Reader;)V
    invoke-static {v2, v3}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;->access$400(Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    .line 634
    .end local v2    # "parser":Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;
    .end local v3    # "reader":Ljava/io/StringReader;
    .end local v4    # "responseString":Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 635
    :catch_0
    move-exception v0

    .line 636
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 624
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 625
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v5, -0x2

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->setHttpStatus(I)V

    .line 626
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 627
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 628
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 629
    const/4 v5, -0x4

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->setHttpStatus(I)V

    goto :goto_1
.end method
