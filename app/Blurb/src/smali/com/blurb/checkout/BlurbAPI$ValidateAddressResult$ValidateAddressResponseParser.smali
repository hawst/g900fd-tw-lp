.class Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;
.super Lcom/blurb/checkout/BlurbResponseParser;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ValidateAddressResponseParser"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1883
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbResponseParser;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/BlurbAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/BlurbAPI$1;

    .prologue
    .line 1883
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;-><init>()V

    return-void
.end method

.method static synthetic access$2100(Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;Ljava/io/Reader;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;
    .param p1, "x1"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1883
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;->parseXml(Ljava/io/Reader;)V

    return-void
.end method

.method private parseXml(Ljava/io/Reader;)V
    .locals 4
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1885
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 1886
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1887
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 1889
    .local v0, "event":I
    :goto_0
    const/4 v3, 0x1

    if-eq v0, v3, :cond_1

    .line 1892
    packed-switch v0, :pswitch_data_0

    .line 1905
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 1906
    goto :goto_0

    .line 1896
    :pswitch_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1898
    .local v1, "name":Ljava/lang/String;
    const-string v3, "errors"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1899
    invoke-virtual {p0, v2}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult$ValidateAddressResponseParser;->parseErrors(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_1

    .line 1907
    .end local v1    # "name":Ljava/lang/String;
    :cond_1
    return-void

    .line 1892
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
