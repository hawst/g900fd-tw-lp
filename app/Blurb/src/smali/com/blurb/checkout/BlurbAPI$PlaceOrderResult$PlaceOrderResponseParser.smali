.class Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;
.super Lcom/blurb/checkout/BlurbResponseParser;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaceOrderResponseParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;


# direct methods
.method private constructor <init>(Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;)V
    .locals 0

    .prologue
    .line 1527
    iput-object p1, p0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    invoke-direct {p0}, Lcom/blurb/checkout/BlurbResponseParser;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;Lcom/blurb/checkout/BlurbAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
    .param p2, "x1"    # Lcom/blurb/checkout/BlurbAPI$1;

    .prologue
    .line 1527
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;Ljava/io/Reader;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;
    .param p1, "x1"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1527
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->parseXml(Ljava/io/Reader;)V

    return-void
.end method

.method private parseXml(Ljava/io/Reader;)V
    .locals 7
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1529
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 1530
    .local v4, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v4, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1531
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    .line 1533
    .local v1, "event":I
    :goto_0
    const/4 v5, 0x1

    if-eq v1, v5, :cond_8

    .line 1536
    packed-switch v1, :pswitch_data_0

    .line 1570
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 1571
    goto :goto_0

    .line 1540
    :pswitch_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1542
    .local v3, "name":Ljava/lang/String;
    const-string v5, "errors"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1543
    invoke-virtual {p0, v4}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->parseErrors(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_1

    .line 1544
    :cond_1
    const-string v5, "id"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1545
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    invoke-static {v4, v3}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->orderId:Ljava/lang/String;

    goto :goto_1

    .line 1546
    :cond_2
    const-string v5, "product_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1547
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    # invokes: Lcom/blurb/checkout/BlurbAPI;->readOrderPrice(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;
    invoke-static {v4, v3}, Lcom/blurb/checkout/BlurbAPI;->access$1500(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->productTotal:Lcom/blurb/checkout/BlurbAPI$Price;

    goto :goto_1

    .line 1548
    :cond_3
    const-string v5, "shipping_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1549
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    # invokes: Lcom/blurb/checkout/BlurbAPI;->readOrderPrice(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;
    invoke-static {v4, v3}, Lcom/blurb/checkout/BlurbAPI;->access$1500(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->shippingTotal:Lcom/blurb/checkout/BlurbAPI$Price;

    goto :goto_1

    .line 1550
    :cond_4
    const-string v5, "tax_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1551
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    # invokes: Lcom/blurb/checkout/BlurbAPI;->readOrderPrice(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;
    invoke-static {v4, v3}, Lcom/blurb/checkout/BlurbAPI;->access$1500(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->taxTotal:Lcom/blurb/checkout/BlurbAPI$Price;

    goto :goto_1

    .line 1552
    :cond_5
    const-string v5, "discount_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1553
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    # invokes: Lcom/blurb/checkout/BlurbAPI;->readOrderPrice(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;
    invoke-static {v4, v3}, Lcom/blurb/checkout/BlurbAPI;->access$1500(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->discountTotal:Lcom/blurb/checkout/BlurbAPI$Price;

    .line 1556
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 1557
    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 1559
    .local v0, "a":Ljava/lang/String;
    const-string v5, "cents"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1560
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->discountCents:Ljava/lang/String;

    .line 1556
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1563
    .end local v0    # "a":Ljava/lang/String;
    .end local v2    # "i":I
    :cond_7
    const-string v5, "order_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1564
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    # invokes: Lcom/blurb/checkout/BlurbAPI;->readOrderPrice(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;
    invoke-static {v4, v3}, Lcom/blurb/checkout/BlurbAPI;->access$1500(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$Price;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->orderTotal:Lcom/blurb/checkout/BlurbAPI$Price;

    goto/16 :goto_1

    .line 1572
    .end local v3    # "name":Ljava/lang/String;
    :cond_8
    return-void

    .line 1536
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
