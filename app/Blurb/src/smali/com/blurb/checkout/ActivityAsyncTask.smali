.class public abstract Lcom/blurb/checkout/ActivityAsyncTask;
.super Landroid/os/AsyncTask;
.source "ActivityAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ActivityType:",
        "Landroid/app/Activity;",
        "Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TParams;TProgress;TResult;>;"
    }
.end annotation


# instance fields
.field private mProgressDlg:Landroid/app/ProgressDialog;

.field private myActivity:Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TActivityType;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TActivityType;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p0, "this":Lcom/blurb/checkout/ActivityAsyncTask;, "Lcom/blurb/checkout/ActivityAsyncTask<TActivityType;TParams;TProgress;TResult;>;"
    .local p1, "a":Landroid/app/Activity;, "TActivityType;"
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    .line 20
    iput-object p1, p0, Lcom/blurb/checkout/ActivityAsyncTask;->myActivity:Landroid/app/Activity;

    .line 21
    return-void
.end method


# virtual methods
.method public getActivity()Landroid/app/Activity;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TActivityType;"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/blurb/checkout/ActivityAsyncTask;, "Lcom/blurb/checkout/ActivityAsyncTask<TActivityType;TParams;TProgress;TResult;>;"
    iget-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->myActivity:Landroid/app/Activity;

    return-object v0
.end method

.method public getProgressMessage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 53
    .local p0, "this":Lcom/blurb/checkout/ActivityAsyncTask;, "Lcom/blurb/checkout/ActivityAsyncTask<TActivityType;TParams;TProgress;TResult;>;"
    invoke-virtual {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->getProgressMessageId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "progressString":Ljava/lang/String;
    return-object v0
.end method

.method public getProgressMessageId()I
    .locals 1

    .prologue
    .line 49
    .local p0, "this":Lcom/blurb/checkout/ActivityAsyncTask;, "Lcom/blurb/checkout/ActivityAsyncTask<TActivityType;TParams;TProgress;TResult;>;"
    const v0, 0x7f040048

    return v0
.end method

.method protected hideProgress()V
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Lcom/blurb/checkout/ActivityAsyncTask;, "Lcom/blurb/checkout/ActivityAsyncTask<TActivityType;TParams;TProgress;TResult;>;"
    iget-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    invoke-virtual {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 44
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    .line 46
    :cond_1
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 25
    .local p0, "this":Lcom/blurb/checkout/ActivityAsyncTask;, "Lcom/blurb/checkout/ActivityAsyncTask<TActivityType;TParams;TProgress;TResult;>;"
    invoke-virtual {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->showProgress()V

    .line 26
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 27
    return-void
.end method

.method protected showProgress()V
    .locals 3

    .prologue
    .local p0, "this":Lcom/blurb/checkout/ActivityAsyncTask;, "Lcom/blurb/checkout/ActivityAsyncTask<TActivityType;TParams;TProgress;TResult;>;"
    const/4 v2, 0x0

    .line 30
    invoke-virtual {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/blurb/checkout/ui/ProgressDialogEx;

    invoke-virtual {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/blurb/checkout/ui/ProgressDialogEx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    .line 32
    iget-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->getProgressMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 33
    iget-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 34
    iget-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 35
    iget-object v0, p0, Lcom/blurb/checkout/ActivityAsyncTask;->mProgressDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 37
    :cond_0
    return-void
.end method
