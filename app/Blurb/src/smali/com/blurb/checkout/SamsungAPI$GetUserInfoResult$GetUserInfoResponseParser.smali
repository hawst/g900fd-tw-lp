.class Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;
.super Ljava/lang/Object;
.source "SamsungAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetUserInfoResponseParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;


# direct methods
.method private constructor <init>(Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;Lcom/blurb/checkout/SamsungAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;
    .param p2, "x1"    # Lcom/blurb/checkout/SamsungAPI$1;

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;-><init>(Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;)V

    return-void
.end method

.method static synthetic access$300(Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;Ljava/io/Reader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;
    .param p1, "x1"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;->parseXml(Ljava/io/Reader;)Z

    move-result v0

    return v0
.end method

.method private parseXml(Ljava/io/Reader;)Z
    .locals 6
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    const/4 v3, 0x0

    .line 185
    .local v3, "success":Z
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 186
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 187
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 189
    .local v0, "event":I
    :goto_0
    const/4 v4, 0x1

    if-eq v0, v4, :cond_5

    .line 192
    packed-switch v0, :pswitch_data_0

    .line 213
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 214
    goto :goto_0

    .line 196
    :pswitch_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 198
    .local v1, "name":Ljava/lang/String;
    const-string v4, "givenName"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 199
    iget-object v4, p0, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->givenName:Ljava/lang/String;

    goto :goto_1

    .line 200
    :cond_1
    const-string v4, "familyName"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 201
    iget-object v4, p0, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->familyName:Ljava/lang/String;

    goto :goto_1

    .line 202
    :cond_2
    const-string v4, "postalCodeText"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 203
    iget-object v4, p0, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->postalCodeText:Ljava/lang/String;

    goto :goto_1

    .line 204
    :cond_3
    const-string v4, "countryCode"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 205
    iget-object v4, p0, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->countryCode:Ljava/lang/String;

    goto :goto_1

    .line 206
    :cond_4
    const-string v4, "loginID"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 207
    iget-object v4, p0, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->loginID:Ljava/lang/String;

    goto :goto_1

    .line 217
    .end local v1    # "name":Ljava/lang/String;
    :cond_5
    const/4 v3, 0x1

    .line 219
    return v3

    .line 192
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
