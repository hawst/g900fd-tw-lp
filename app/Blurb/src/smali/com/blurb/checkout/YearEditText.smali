.class public Lcom/blurb/checkout/YearEditText;
.super Landroid/widget/EditText;
.source "YearEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/YearEditText$YearInputConnection;
    }
.end annotation


# instance fields
.field private monthEditText:Landroid/widget/EditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/blurb/checkout/YearEditText;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/YearEditText;

    .prologue
    .line 11
    iget-object v0, p0, Lcom/blurb/checkout/YearEditText;->monthEditText:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .prologue
    .line 33
    new-instance v0, Lcom/blurb/checkout/YearEditText$YearInputConnection;

    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcom/blurb/checkout/YearEditText$YearInputConnection;-><init>(Lcom/blurb/checkout/YearEditText;Landroid/view/inputmethod/InputConnection;Z)V

    return-object v0
.end method

.method setMonthEditText(Landroid/widget/EditText;)V
    .locals 0
    .param p1, "month"    # Landroid/widget/EditText;

    .prologue
    .line 28
    iput-object p1, p0, Lcom/blurb/checkout/YearEditText;->monthEditText:Landroid/widget/EditText;

    .line 29
    return-void
.end method
