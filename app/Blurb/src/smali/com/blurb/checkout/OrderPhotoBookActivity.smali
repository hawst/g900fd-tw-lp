.class public Lcom/blurb/checkout/OrderPhotoBookActivity;
.super Landroid/app/Activity;
.source "OrderPhotoBookActivity.java"

# interfaces
.implements Lcom/blurb/checkout/UploadProgressReceiver$IProgress;


# static fields
.field private static final DEBUG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"


# instance fields
.field private discountLine:Landroid/widget/RelativeLayout;

.field private displayBookType:Landroid/widget/TextView;

.field private displayCoverUri:Landroid/widget/ImageView;

.field private displayDiscount:Landroid/widget/TextView;

.field private displayOrderTotal:Landroid/widget/TextView;

.field private displayPages:Landroid/widget/TextView;

.field private displayProductTitle:Landroid/widget/TextView;

.field private displayQuantityText:Landroid/widget/TextView;

.field private displayShipping:Landroid/widget/TextView;

.field private displaySubtotal:Landroid/widget/TextView;

.field private displayTax:Landroid/widget/TextView;

.field private displayTitle:Landroid/widget/TextView;

.field private email:Landroid/widget/TextView;

.field private imageProgressBar:Landroid/widget/ProgressBar;

.field private myCC:Landroid/widget/TextView;

.field private myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

.field private progressSteps:Landroid/widget/TextView;

.field private you_will_receive:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/blurb/checkout/OrderPhotoBookActivity;)Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/OrderPhotoBookActivity;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    return-object v0
.end method

.method private loadFromIntent(Landroid/content/Intent;)V
    .locals 22
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 102
    const-string v2, "orderInfo"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .line 104
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v2, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->episodeId:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 105
    :cond_0
    const-string v2, "BlurbShadowApp"

    const-string v3, "OrderPhotoBookActivity.loadFromIntent: No order info found"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->finish()V

    .line 193
    :cond_1
    :goto_0
    return-void

    .line 110
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->firstName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v4, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->lastName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v5, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->address1:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v6, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->address2:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v7, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->city:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v8, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->state:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v9, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->postalCode:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v10, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->countryCode:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v11, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->phone:Ljava/lang/String;

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v11}, Lcom/blurb/checkout/OrderPhotoBookActivity;->updateAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->email:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->email:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    const v2, 0x7f0400d1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v5, v5, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->lastFourCC:Ljava/lang/String;

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 115
    .local v16, "maskedCC":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myCC:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    invoke-direct/range {p0 .. p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->showHideDiscount()V

    .line 118
    invoke-direct/range {p0 .. p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->showHideTax()V

    .line 120
    const v2, 0x7f0400c8

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v5, v5, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderId:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v5, v5, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->email:Ljava/lang/String;

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    .line 121
    .local v20, "receiveText":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->you_will_receive:Landroid/widget/TextView;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v2, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->cover:[B

    if-eqz v2, :cond_3

    .line 124
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v2, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->cover:[B

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v4, v4, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->cover:[B

    array-length v4, v4

    invoke-static {v2, v3, v4}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 125
    .local v12, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v12, :cond_3

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayCoverUri:Landroid/widget/ImageView;

    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 133
    .end local v12    # "bitmap":Landroid/graphics/Bitmap;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    const v2, 0x7f0400b7

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget v5, v5, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->numPages:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 135
    .local v18, "pageCount":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayPages:Landroid/widget/TextView;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayBookType:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->coverType:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v4, v4, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookType:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v3, v4}, Lcom/blurb/checkout/MainShadowActivity;->getBookType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    const v2, 0x7f0400cc

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v5, v5, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->quantity:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 139
    .local v15, "formattedQuantity":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayQuantityText:Landroid/widget/TextView;

    invoke-virtual {v2, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayOrderTotal:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderTotal:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayShipping:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->shippingTotal:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayTax:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->taxTotal:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayDiscount:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->discountTotal:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayProductTitle:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displaySubtotal:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->subTotal:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->numPages:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x3

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 150
    const-string v2, "uploadCompleted"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v21

    .line 151
    .local v21, "uploadCompleted":Z
    const/4 v2, 0x1

    move/from16 v0, v21

    if-ne v0, v2, :cond_4

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMax()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 153
    const v2, 0x7f0400ec

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getMax()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getMax()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 154
    .local v17, "p":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->progressSteps:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    .end local v17    # "p":Ljava/lang/String;
    :goto_1
    const v2, 0x7f060023

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/ImageView;

    .line 168
    .local v14, "cardView":Landroid/widget/ImageView;
    const/4 v13, -0x1

    .line 170
    .local v13, "cardId":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget v2, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->creditCardType:I

    packed-switch v2, :pswitch_data_0

    .line 191
    :goto_2
    if-lez v13, :cond_1

    .line 192
    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v14, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 156
    .end local v13    # "cardId":I
    .end local v14    # "cardView":Landroid/widget/ImageView;
    :cond_4
    const v2, 0x7f0400d2

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v2, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    invoke-static {v2}, Lcom/blurb/checkout/UploadProgressReceiver;->getProgress(Ljava/lang/String;)Lcom/blurb/checkout/UploadProgressReceiver$Progress;

    move-result-object v19

    .line 159
    .local v19, "progress":Lcom/blurb/checkout/UploadProgressReceiver$Progress;
    if-eqz v19, :cond_5

    .line 160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v2, v2, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v2, v1}, Lcom/blurb/checkout/OrderPhotoBookActivity;->onUploadProgress(Ljava/lang/String;Lcom/blurb/checkout/UploadProgressReceiver$Progress;)V

    goto :goto_1

    .line 162
    :cond_5
    const v2, 0x7f0400ec

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5}, Landroid/widget/ProgressBar;->getMax()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 163
    .restart local v17    # "p":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/OrderPhotoBookActivity;->progressSteps:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 172
    .end local v17    # "p":Ljava/lang/String;
    .end local v19    # "progress":Lcom/blurb/checkout/UploadProgressReceiver$Progress;
    .restart local v13    # "cardId":I
    .restart local v14    # "cardView":Landroid/widget/ImageView;
    :pswitch_0
    const/high16 v13, 0x7f020000

    .line 173
    goto :goto_2

    .line 175
    :pswitch_1
    const v13, 0x7f02001f

    .line 176
    goto :goto_2

    .line 178
    :pswitch_2
    const v13, 0x7f020008

    .line 179
    goto :goto_2

    .line 181
    :pswitch_3
    const v13, 0x7f020004

    .line 182
    goto :goto_2

    .line 184
    :pswitch_4
    const v13, 0x7f020006

    goto :goto_2

    .line 170
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private showHideDiscount()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 257
    const v3, 0x7f06002b

    invoke-virtual {p0, v3}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 259
    .local v1, "sep":Landroid/view/View;
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->discountCents:Ljava/lang/String;

    if-nez v3, :cond_0

    move v0, v2

    .line 261
    .local v0, "hasDiscount":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 262
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 263
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->discountLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 268
    :goto_1
    return-void

    .line 259
    .end local v0    # "hasDiscount":Z
    :cond_0
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->discountCents:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lez v3, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 265
    .restart local v0    # "hasDiscount":Z
    :cond_3
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 266
    iget-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->discountLine:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method private showHideTax()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x0

    .line 271
    const v4, 0x7f060028

    invoke-virtual {p0, v4}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 272
    .local v2, "taxView":Landroid/view/View;
    const v4, 0x7f060027

    invoke-virtual {p0, v4}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 274
    .local v1, "sep":Landroid/view/View;
    const/4 v0, 0x0

    .line 276
    .local v0, "hasTax":Z
    iget-object v4, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v4, v4, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->taxCents:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 278
    :try_start_0
    iget-object v4, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v4, v4, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->taxCents:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-lez v4, :cond_1

    const/4 v0, 0x1

    .line 283
    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 284
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 285
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 290
    :goto_1
    return-void

    :cond_1
    move v0, v3

    .line 278
    goto :goto_0

    .line 287
    :cond_2
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 288
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 279
    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method private updateAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "firstName"    # Ljava/lang/String;
    .param p2, "lastName"    # Ljava/lang/String;
    .param p3, "address1"    # Ljava/lang/String;
    .param p4, "address2"    # Ljava/lang/String;
    .param p5, "city"    # Ljava/lang/String;
    .param p6, "state"    # Ljava/lang/String;
    .param p7, "zip"    # Ljava/lang/String;
    .param p8, "countryCode"    # Ljava/lang/String;
    .param p9, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 196
    const v6, 0x7f060021

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 197
    .local v3, "countryView":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 201
    move-object/from16 v0, p8

    invoke-static {p0, v0}, Lcom/blurb/checkout/MainShadowActivity;->getCountry(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 203
    .local v2, "country":Ljava/lang/String;
    const-string v6, "us"

    move-object/from16 v0, p8

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 204
    invoke-virtual {p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030008

    invoke-virtual {v6, v7, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 240
    .local v1, "address":Landroid/view/View;
    :cond_0
    :goto_0
    const v6, 0x7f060010

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 241
    .local v5, "v":Landroid/widget/TextView;
    const v6, 0x7f0400ed

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p1, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    invoke-virtual {p0, v6, v7}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 242
    .local v4, "s":Ljava/lang/String;
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    const v6, 0x7f060013

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "v":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 245
    .restart local v5    # "v":Landroid/widget/TextView;
    invoke-virtual {v5, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    const v6, 0x7f060011

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "v":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 247
    .restart local v5    # "v":Landroid/widget/TextView;
    invoke-virtual {v5, p5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    const v6, 0x7f060012

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "v":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 249
    .restart local v5    # "v":Landroid/widget/TextView;
    invoke-virtual {v5, p6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    const v6, 0x7f060015

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "v":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 251
    .restart local v5    # "v":Landroid/widget/TextView;
    move-object/from16 v0, p7

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    const v6, 0x7f060016

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "v":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 253
    .restart local v5    # "v":Landroid/widget/TextView;
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    return-void

    .line 205
    .end local v1    # "address":Landroid/view/View;
    .end local v4    # "s":Ljava/lang/String;
    .end local v5    # "v":Landroid/widget/TextView;
    :cond_1
    const-string v6, "jp"

    move-object/from16 v0, p8

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 206
    invoke-virtual {p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030005

    invoke-virtual {v6, v7, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 208
    .restart local v1    # "address":Landroid/view/View;
    const v6, 0x7f060014

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 209
    .restart local v5    # "v":Landroid/widget/TextView;
    invoke-virtual {v5, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_0

    .line 211
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 212
    .end local v1    # "address":Landroid/view/View;
    .end local v5    # "v":Landroid/widget/TextView;
    :cond_2
    const-string v6, "kr"

    move-object/from16 v0, p8

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 213
    invoke-virtual {p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030006

    invoke-virtual {v6, v7, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 215
    .restart local v1    # "address":Landroid/view/View;
    const v6, 0x7f060014

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 216
    .restart local v5    # "v":Landroid/widget/TextView;
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_3

    .line 217
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 220
    :goto_1
    const v6, 0x7f060017

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "v":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 221
    .restart local v5    # "v":Landroid/widget/TextView;
    invoke-virtual/range {p9 .. p9}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_4

    .line 222
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 219
    :cond_3
    invoke-virtual {v5, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 224
    :cond_4
    move-object/from16 v0, p9

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 225
    .end local v1    # "address":Landroid/view/View;
    .end local v5    # "v":Landroid/widget/TextView;
    :cond_5
    const-string v6, "jp"

    move-object/from16 v0, p8

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 226
    invoke-virtual {p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030005

    invoke-virtual {v6, v7, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 228
    .restart local v1    # "address":Landroid/view/View;
    const v6, 0x7f060014

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 229
    .restart local v5    # "v":Landroid/widget/TextView;
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_6

    .line 230
    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 232
    :cond_6
    invoke-virtual {v5, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 233
    .end local v1    # "address":Landroid/view/View;
    .end local v5    # "v":Landroid/widget/TextView;
    :cond_7
    const-string v6, "gb"

    move-object/from16 v0, p8

    invoke-virtual {v0, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 234
    invoke-virtual {p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030007

    invoke-virtual {v6, v7, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 235
    .restart local v1    # "address":Landroid/view/View;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    move-object/from16 v0, p7

    invoke-virtual {v0, v6}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p7

    goto/16 :goto_0

    .line 237
    .end local v1    # "address":Landroid/view/View;
    :cond_8
    invoke-virtual {p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f030007

    invoke-virtual {v6, v7, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .restart local v1    # "address":Landroid/view/View;
    goto/16 :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 294
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 296
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->requestWindowFeature(I)Z

    .line 298
    const v2, 0x7f03000b

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->setContentView(I)V

    .line 300
    const v2, 0x7f06000f

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 301
    .local v1, "titleView":Landroid/widget/TextView;
    const v2, 0x7f0400dc

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 303
    const v2, 0x7f060022

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->email:Landroid/widget/TextView;

    .line 304
    const v2, 0x7f060024

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myCC:Landroid/widget/TextView;

    .line 305
    const v2, 0x7f060030

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    .line 306
    const v2, 0x7f06001d

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayTitle:Landroid/widget/TextView;

    .line 307
    const v2, 0x7f06001f

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayPages:Landroid/widget/TextView;

    .line 308
    const v2, 0x7f06001e

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayBookType:Landroid/widget/TextView;

    .line 309
    const v2, 0x7f060020

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayQuantityText:Landroid/widget/TextView;

    .line 311
    const v2, 0x7f06002e

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayOrderTotal:Landroid/widget/TextView;

    .line 312
    const v2, 0x7f06002a

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayShipping:Landroid/widget/TextView;

    .line 313
    const v2, 0x7f060029

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayTax:Landroid/widget/TextView;

    .line 314
    const v2, 0x7f06002d

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayDiscount:Landroid/widget/TextView;

    .line 315
    const v2, 0x7f060025

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayProductTitle:Landroid/widget/TextView;

    .line 316
    const v2, 0x7f060026

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displaySubtotal:Landroid/widget/TextView;

    .line 317
    const v2, 0x7f06002c

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->discountLine:Landroid/widget/RelativeLayout;

    .line 318
    const v2, 0x7f06001b

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->you_will_receive:Landroid/widget/TextView;

    .line 319
    const v2, 0x7f06001c

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->displayCoverUri:Landroid/widget/ImageView;

    .line 320
    const v2, 0x7f060031

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->progressSteps:Landroid/widget/TextView;

    .line 322
    invoke-virtual {p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 324
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0, v0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->loadFromIntent(Landroid/content/Intent;)V

    .line 326
    const v2, 0x7f06002f

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/blurb/checkout/OrderPhotoBookActivity$1;

    invoke-direct {v3, p0}, Lcom/blurb/checkout/OrderPhotoBookActivity$1;-><init>(Lcom/blurb/checkout/OrderPhotoBookActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 336
    const/4 v2, -0x1

    invoke-virtual {p0, v2}, Lcom/blurb/checkout/OrderPhotoBookActivity;->setResult(I)V

    .line 337
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 341
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 343
    invoke-direct {p0, p1}, Lcom/blurb/checkout/OrderPhotoBookActivity;->loadFromIntent(Landroid/content/Intent;)V

    .line 344
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/blurb/checkout/UploadProgressReceiver;->registerForProgress(Lcom/blurb/checkout/UploadProgressReceiver$IProgress;)V

    .line 350
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 351
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 355
    iget-object v1, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    if-eqz v1, :cond_0

    .line 356
    invoke-static {p0}, Lcom/blurb/checkout/UploadProgressReceiver;->registerForProgress(Lcom/blurb/checkout/UploadProgressReceiver$IProgress;)V

    .line 358
    iget-object v1, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v1, v1, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    invoke-static {v1}, Lcom/blurb/checkout/UploadProgressReceiver;->getProgress(Ljava/lang/String;)Lcom/blurb/checkout/UploadProgressReceiver$Progress;

    move-result-object v0

    .line 359
    .local v0, "progress":Lcom/blurb/checkout/UploadProgressReceiver$Progress;
    if-eqz v0, :cond_0

    .line 360
    iget-object v1, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v1, v1, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->onUploadProgress(Ljava/lang/String;Lcom/blurb/checkout/UploadProgressReceiver$Progress;)V

    .line 363
    .end local v0    # "progress":Lcom/blurb/checkout/UploadProgressReceiver$Progress;
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 364
    return-void
.end method

.method public onUploadProgress(Ljava/lang/String;Lcom/blurb/checkout/UploadProgressReceiver$Progress;)V
    .locals 10
    .param p1, "progressBookId"    # Ljava/lang/String;
    .param p2, "progress"    # Lcom/blurb/checkout/UploadProgressReceiver$Progress;

    .prologue
    const v9, 0x7f0400ec

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 59
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    if-nez v3, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 63
    iget v3, p2, Lcom/blurb/checkout/UploadProgressReceiver$Progress;->progress:F

    const/high16 v4, -0x40800000    # -1.0f

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_2

    iget v3, p2, Lcom/blurb/checkout/UploadProgressReceiver$Progress;->progress:F

    const/high16 v4, -0x40000000    # -2.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    .line 65
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/blurb/checkout/UnrecoverableActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 67
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "orderInfo"

    iget-object v4, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->startActivity(Landroid/content/Intent;)V

    .line 71
    invoke-virtual {p0, v6}, Lcom/blurb/checkout/OrderPhotoBookActivity;->setResult(I)V

    .line 73
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    invoke-static {v3}, Lcom/blurb/checkout/UploadProgressReceiver;->forgetProgress(Ljava/lang/String;)V

    .line 75
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .line 77
    invoke-virtual {p0}, Lcom/blurb/checkout/OrderPhotoBookActivity;->finish()V

    goto :goto_0

    .line 78
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_3
    iget v3, p2, Lcom/blurb/checkout/UploadProgressReceiver$Progress;->progress:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_4

    .line 79
    const v3, 0x7f0400d4

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v5, v5, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->title:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "msg":Ljava/lang/String;
    invoke-static {p0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 83
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getMax()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 84
    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getMax()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getMax()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v9, v3}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 85
    .local v2, "p":Ljava/lang/String;
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->progressSteps:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v3, v3, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    invoke-static {v3}, Lcom/blurb/checkout/UploadProgressReceiver;->forgetProgress(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 89
    .end local v1    # "msg":Ljava/lang/String;
    .end local v2    # "p":Ljava/lang/String;
    :cond_4
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getMax()I

    move-result v4

    int-to-float v4, v4

    iget v5, p2, Lcom/blurb/checkout/UploadProgressReceiver$Progress;->progress:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 90
    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getMax()I

    move-result v4

    int-to-float v4, v4

    iget v5, p2, Lcom/blurb/checkout/UploadProgressReceiver$Progress;->progress:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->imageProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v4}, Landroid/widget/ProgressBar;->getMax()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v9, v3}, Lcom/blurb/checkout/OrderPhotoBookActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 91
    .restart local v2    # "p":Ljava/lang/String;
    iget-object v3, p0, Lcom/blurb/checkout/OrderPhotoBookActivity;->progressSteps:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method
