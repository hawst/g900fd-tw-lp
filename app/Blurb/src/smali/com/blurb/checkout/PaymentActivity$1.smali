.class Lcom/blurb/checkout/PaymentActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "PaymentActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/PaymentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/PaymentActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/PaymentActivity;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Lcom/blurb/checkout/PaymentActivity$1;->this$0:Lcom/blurb/checkout/PaymentActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 157
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.samsung.android.app.episodes.action.IMAGES_READY"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 160
    const-string v2, "resultCode"

    const/16 v3, -0x3e7

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 162
    .local v1, "resultCode":I
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    iget-object v2, p0, Lcom/blurb/checkout/PaymentActivity$1;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->triedToOrder:Z
    invoke-static {v2}, Lcom/blurb/checkout/PaymentActivity;->access$000(Lcom/blurb/checkout/PaymentActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 163
    iget-object v2, p0, Lcom/blurb/checkout/PaymentActivity$1;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->dialogShowing:I
    invoke-static {v2}, Lcom/blurb/checkout/PaymentActivity;->access$100(Lcom/blurb/checkout/PaymentActivity;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    iget-object v2, p0, Lcom/blurb/checkout/PaymentActivity$1;->this$0:Lcom/blurb/checkout/PaymentActivity;

    iget-object v3, p0, Lcom/blurb/checkout/PaymentActivity$1;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->dialogShowing:I
    invoke-static {v3}, Lcom/blurb/checkout/PaymentActivity;->access$100(Lcom/blurb/checkout/PaymentActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/blurb/checkout/PaymentActivity;->dismissDialog(I)V

    .line 165
    iget-object v2, p0, Lcom/blurb/checkout/PaymentActivity$1;->this$0:Lcom/blurb/checkout/PaymentActivity;

    const/4 v3, 0x0

    # setter for: Lcom/blurb/checkout/PaymentActivity;->dialogShowing:I
    invoke-static {v2, v3}, Lcom/blurb/checkout/PaymentActivity;->access$102(Lcom/blurb/checkout/PaymentActivity;I)I

    .line 167
    :cond_0
    iget-object v2, p0, Lcom/blurb/checkout/PaymentActivity$1;->this$0:Lcom/blurb/checkout/PaymentActivity;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/blurb/checkout/PaymentActivity;->showDialog(ILandroid/os/Bundle;)Z

    .line 170
    .end local v1    # "resultCode":I
    :cond_1
    return-void
.end method
