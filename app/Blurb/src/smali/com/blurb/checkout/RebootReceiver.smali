.class public Lcom/blurb/checkout/RebootReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RebootReceiver.java"


# static fields
.field public static final ACTION_REBOOT:Ljava/lang/String; = "android.intent.action.BOOT_COMPLETED"

.field public static final ACTION_START_UPLOAD:Ljava/lang/String; = "com.blurb.checkout.START_UPLOAD"

.field private static final DEBUG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private startUpload(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/blurb/checkout/service/UploadService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 44
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.blurb.checkout.service.UPLOAD_BOOKS"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 47
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 22
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 27
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 28
    const-string v4, "alarm"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 30
    .local v1, "mgr":Landroid/app/AlarmManager;
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/blurb/checkout/RebootReceiver;

    invoke-direct {v3, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 31
    .local v3, "uploadIntent":Landroid/content/Intent;
    const-string v4, "com.blurb.checkout.START_UPLOAD"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    const/4 v4, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {p1, v4, v3, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 35
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    const/4 v4, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    const-wide/32 v7, 0x493e0

    add-long/2addr v5, v7

    invoke-virtual {v1, v4, v5, v6, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 39
    .end local v1    # "mgr":Landroid/app/AlarmManager;
    .end local v2    # "pendingIntent":Landroid/app/PendingIntent;
    .end local v3    # "uploadIntent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 36
    :cond_1
    const-string v4, "com.blurb.checkout.START_UPLOAD"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 37
    invoke-direct {p0, p1}, Lcom/blurb/checkout/RebootReceiver;->startUpload(Landroid/content/Context;)V

    goto :goto_0
.end method
