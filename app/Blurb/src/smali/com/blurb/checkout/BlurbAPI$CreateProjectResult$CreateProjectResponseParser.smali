.class Lcom/blurb/checkout/BlurbAPI$CreateProjectResult$CreateProjectResponseParser;
.super Lcom/blurb/checkout/BlurbResponseParser;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateProjectResponseParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;


# direct methods
.method private constructor <init>(Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;)V
    .locals 0

    .prologue
    .line 1742
    iput-object p1, p0, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult$CreateProjectResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    invoke-direct {p0}, Lcom/blurb/checkout/BlurbResponseParser;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;Lcom/blurb/checkout/BlurbAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;
    .param p2, "x1"    # Lcom/blurb/checkout/BlurbAPI$1;

    .prologue
    .line 1742
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult$CreateProjectResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/blurb/checkout/BlurbAPI$CreateProjectResult$CreateProjectResponseParser;Ljava/io/Reader;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/BlurbAPI$CreateProjectResult$CreateProjectResponseParser;
    .param p1, "x1"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1742
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult$CreateProjectResponseParser;->parseXml(Ljava/io/Reader;)V

    return-void
.end method

.method private parseXml(Ljava/io/Reader;)V
    .locals 5
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1744
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 1745
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1746
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 1748
    .local v0, "event":I
    :goto_0
    const/4 v3, 0x1

    if-eq v0, v3, :cond_2

    .line 1751
    packed-switch v0, :pswitch_data_0

    .line 1766
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 1767
    goto :goto_0

    .line 1755
    :pswitch_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1757
    .local v1, "name":Ljava/lang/String;
    const-string v3, "errors"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1758
    invoke-virtual {p0, v2}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult$CreateProjectResponseParser;->parseErrors(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_1

    .line 1759
    :cond_1
    const-string v3, "id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1760
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult$CreateProjectResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    invoke-static {v2, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->projectId:Ljava/lang/String;

    goto :goto_1

    .line 1768
    .end local v1    # "name":Ljava/lang/String;
    :cond_2
    return-void

    .line 1751
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
