.class Lcom/blurb/checkout/BlurbAPI$BookPricingResult;
.super Lcom/blurb/checkout/BlurbAPI$BlurbResult;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BookPricingResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;
    }
.end annotation


# instance fields
.field bookType:Ljava/lang/String;

.field coverType:Ljava/lang/String;

.field paperType:Ljava/lang/String;

.field prices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/blurb/checkout/BlurbAPI$Price;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1105
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 1106
    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 1108
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 1109
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpStatus(I)V

    .line 1110
    return-void
.end method


# virtual methods
.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 7
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 1113
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpStatus(I)V

    .line 1118
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 1119
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 1120
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpStatus(I)V

    .line 1148
    :goto_0
    return-void

    .line 1124
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_1

    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0x190

    if-ne v5, v6, :cond_2

    .line 1126
    :cond_1
    :try_start_0
    const-string v5, "UTF-8"

    invoke-static {v1, v5}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1127
    .local v4, "responseString":Ljava/lang/String;
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1130
    .local v3, "reader":Ljava/io/StringReader;
    new-instance v2, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;

    const/4 v5, 0x0

    invoke-direct {v2, p0, v5}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$BookPricingResult;Lcom/blurb/checkout/BlurbAPI$1;)V

    .line 1132
    .local v2, "parser":Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;
    # invokes: Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->parseXml(Ljava/io/Reader;)V
    invoke-static {v2, v3}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->access$1000(Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;Ljava/io/Reader;)V

    .line 1133
    invoke-virtual {v2}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->getErrors()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->errors:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1144
    .end local v2    # "parser":Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;
    .end local v3    # "reader":Ljava/io/StringReader;
    .end local v4    # "responseString":Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1145
    :catch_0
    move-exception v0

    .line 1146
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1134
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1135
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v5, -0x2

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpStatus(I)V

    .line 1136
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1137
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 1138
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 1139
    const/4 v5, -0x4

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpStatus(I)V

    goto :goto_1
.end method
