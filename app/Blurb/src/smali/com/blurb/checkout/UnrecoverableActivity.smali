.class public Lcom/blurb/checkout/UnrecoverableActivity;
.super Landroid/app/Activity;
.source "UnrecoverableActivity.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"


# instance fields
.field private myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private loadFromIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 23
    const-string v0, "orderInfo"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iput-object v0, p0, Lcom/blurb/checkout/UnrecoverableActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    .line 25
    iget-object v0, p0, Lcom/blurb/checkout/UnrecoverableActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/blurb/checkout/UnrecoverableActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v0, v0, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->episodeId:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 26
    :cond_0
    const-string v0, "BlurbShadowApp"

    const-string v1, "UnrecoverableActivity.loadFromIntent: No order info found"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 27
    invoke-virtual {p0}, Lcom/blurb/checkout/UnrecoverableActivity;->finish()V

    .line 30
    :cond_1
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 37
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0, v6}, Lcom/blurb/checkout/UnrecoverableActivity;->requestWindowFeature(I)Z

    .line 41
    const v4, 0x7f03000e

    invoke-virtual {p0, v4}, Lcom/blurb/checkout/UnrecoverableActivity;->setContentView(I)V

    .line 43
    const v4, 0x7f06000f

    invoke-virtual {p0, v4}, Lcom/blurb/checkout/UnrecoverableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 44
    .local v2, "titleView":Landroid/widget/TextView;
    const v4, 0x7f0400e8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 46
    invoke-virtual {p0}, Lcom/blurb/checkout/UnrecoverableActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 48
    .local v1, "intent":Landroid/content/Intent;
    invoke-direct {p0, v1}, Lcom/blurb/checkout/UnrecoverableActivity;->loadFromIntent(Landroid/content/Intent;)V

    .line 50
    const v4, 0x7f060051

    invoke-virtual {p0, v4}, Lcom/blurb/checkout/UnrecoverableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 51
    .local v3, "tv":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/blurb/checkout/UnrecoverableActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0400f2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/blurb/checkout/UnrecoverableActivity;->myOrderInfo:Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    iget-object v8, v8, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderId:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "body":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 55
    const v4, 0x7f060052

    invoke-virtual {p0, v4}, Lcom/blurb/checkout/UnrecoverableActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/blurb/checkout/UnrecoverableActivity$1;

    invoke-direct {v5, p0}, Lcom/blurb/checkout/UnrecoverableActivity$1;-><init>(Lcom/blurb/checkout/UnrecoverableActivity;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    return-void
.end method
