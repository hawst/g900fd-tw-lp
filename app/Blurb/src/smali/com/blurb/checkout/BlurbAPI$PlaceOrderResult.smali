.class Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
.super Lcom/blurb/checkout/BlurbAPI$BlurbResult;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PlaceOrderResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;
    }
.end annotation


# instance fields
.field discountCents:Ljava/lang/String;

.field discountTotal:Lcom/blurb/checkout/BlurbAPI$Price;

.field orderId:Ljava/lang/String;

.field orderTotal:Lcom/blurb/checkout/BlurbAPI$Price;

.field productTotal:Lcom/blurb/checkout/BlurbAPI$Price;

.field shippingTotal:Lcom/blurb/checkout/BlurbAPI$Price;

.field taxTotal:Lcom/blurb/checkout/BlurbAPI$Price;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1575
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 1576
    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 1578
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 1579
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpStatus(I)V

    .line 1580
    return-void
.end method


# virtual methods
.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 7
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 1583
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpStatus(I)V

    .line 1588
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 1589
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 1590
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpStatus(I)V

    .line 1619
    :goto_0
    return-void

    .line 1594
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_1

    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0x190

    if-ne v5, v6, :cond_2

    .line 1596
    :cond_1
    :try_start_0
    const-string v5, "UTF-8"

    invoke-static {v1, v5}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1597
    .local v4, "responseString":Ljava/lang/String;
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 1598
    .local v3, "reader":Ljava/io/StringReader;
    new-instance v2, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;

    const/4 v5, 0x0

    invoke-direct {v2, p0, v5}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;Lcom/blurb/checkout/BlurbAPI$1;)V

    .line 1600
    .local v2, "parser":Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;
    # invokes: Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->parseXml(Ljava/io/Reader;)V
    invoke-static {v2, v3}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->access$1700(Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;Ljava/io/Reader;)V

    .line 1601
    invoke-virtual {v2}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;->getErrors()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->errors:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1615
    .end local v2    # "parser":Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult$PlaceOrderResponseParser;
    .end local v3    # "reader":Ljava/io/StringReader;
    .end local v4    # "responseString":Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1616
    :catch_0
    move-exception v0

    .line 1617
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1605
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 1606
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v5, -0x2

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpStatus(I)V

    .line 1607
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 1608
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 1609
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 1610
    const/4 v5, -0x4

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->setHttpStatus(I)V

    goto :goto_1
.end method
