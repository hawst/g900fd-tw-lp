.class Lcom/blurb/checkout/MainShadowActivity$Country;
.super Ljava/lang/Object;
.source "MainShadowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/MainShadowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Country"
.end annotation


# instance fields
.field countryId:I

.field myName:Ljava/lang/String;

.field threeLetterCode:Ljava/lang/String;

.field twoLetterCode:Ljava/lang/String;


# direct methods
.method constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "inCountryId"    # I
    .param p2, "code2"    # Ljava/lang/String;
    .param p3, "code3"    # Ljava/lang/String;

    .prologue
    .line 1480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1481
    iput-object p2, p0, Lcom/blurb/checkout/MainShadowActivity$Country;->twoLetterCode:Ljava/lang/String;

    .line 1482
    iput-object p3, p0, Lcom/blurb/checkout/MainShadowActivity$Country;->threeLetterCode:Ljava/lang/String;

    .line 1483
    iput p1, p0, Lcom/blurb/checkout/MainShadowActivity$Country;->countryId:I

    .line 1484
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1488
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$Country;->myName:Ljava/lang/String;

    return-object v0
.end method
