.class public abstract Lcom/blurb/checkout/BlurbResponseParser;
.super Ljava/lang/Object;
.source "BlurbResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    }
.end annotation


# instance fields
.field protected parseErrors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/blurb/checkout/BlurbResponseParser$BlurbError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/blurb/checkout/BlurbResponseParser;->parseErrors:Ljava/util/ArrayList;

    return-void
.end method

.method private static parseError(Lorg/xmlpull/v1/XmlPullParser;)Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    .locals 4
    .param p0, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;-><init>()V

    .line 43
    .local v0, "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    :cond_0
    :goto_0
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v1

    .line 44
    .local v1, "eventType":I
    const/4 v3, 0x2

    if-ne v1, v3, :cond_2

    .line 45
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 46
    .local v2, "tag":Ljava/lang/String;
    const-string v3, "parameter"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 47
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->parameter:Ljava/lang/String;

    goto :goto_0

    .line 48
    :cond_1
    const-string v3, "message"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 49
    invoke-interface {p0}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->message:Ljava/lang/String;

    goto :goto_0

    .line 51
    .end local v2    # "tag":Ljava/lang/String;
    :cond_2
    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 56
    return-object v0
.end method


# virtual methods
.method public getErrors()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/blurb/checkout/BlurbResponseParser$BlurbError;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/blurb/checkout/BlurbResponseParser;->parseErrors:Ljava/util/ArrayList;

    return-object v0
.end method

.method parseErrors(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 61
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/blurb/checkout/BlurbResponseParser;->parseErrors:Ljava/util/ArrayList;

    .line 64
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    move-result v1

    .line 65
    .local v1, "eventType":I
    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 66
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 67
    .local v2, "tag":Ljava/lang/String;
    const-string v3, "error"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 68
    invoke-static {p1}, Lcom/blurb/checkout/BlurbResponseParser;->parseError(Lorg/xmlpull/v1/XmlPullParser;)Lcom/blurb/checkout/BlurbResponseParser$BlurbError;

    move-result-object v0

    .line 69
    .local v0, "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    iget-object v3, p0, Lcom/blurb/checkout/BlurbResponseParser;->parseErrors:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    .end local v0    # "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    .end local v2    # "tag":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 75
    return-void
.end method
