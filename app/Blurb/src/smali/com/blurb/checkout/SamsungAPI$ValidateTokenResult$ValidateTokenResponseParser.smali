.class Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;
.super Ljava/lang/Object;
.source "SamsungAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValidateTokenResponseParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;


# direct methods
.method private constructor <init>(Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;Lcom/blurb/checkout/SamsungAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;
    .param p2, "x1"    # Lcom/blurb/checkout/SamsungAPI$1;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;-><init>(Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;)V

    return-void
.end method

.method static synthetic access$100(Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;Ljava/io/Reader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;
    .param p1, "x1"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;->parseXml(Ljava/io/Reader;)Z

    move-result v0

    return v0
.end method

.method private parseXml(Ljava/io/Reader;)Z
    .locals 7
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 38
    const/4 v3, 0x0

    .line 39
    .local v3, "success":Z
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 40
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 41
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 43
    .local v0, "event":I
    :goto_0
    if-eq v0, v4, :cond_3

    .line 46
    packed-switch v0, :pswitch_data_0

    .line 63
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 64
    goto :goto_0

    .line 50
    :pswitch_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "name":Ljava/lang/String;
    const-string v5, "authenticateUserID"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 53
    iget-object v5, p0, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->userId:Ljava/lang/String;

    goto :goto_1

    .line 54
    :cond_1
    const-string v5, "code"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 55
    iget-object v5, p0, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->errorCode:Ljava/lang/String;

    goto :goto_1

    .line 56
    :cond_2
    const-string v5, "message"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 57
    iget-object v5, p0, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->errorMessage:Ljava/lang/String;

    goto :goto_1

    .line 66
    .end local v1    # "name":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    iget-object v5, v5, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->userId:Ljava/lang/String;

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;->this$0:Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;

    iget-object v5, v5, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->errorCode:Ljava/lang/String;

    if-eqz v5, :cond_5

    :cond_4
    move v3, v4

    .line 68
    :goto_2
    return v3

    .line 66
    :cond_5
    const/4 v3, 0x0

    goto :goto_2

    .line 46
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
