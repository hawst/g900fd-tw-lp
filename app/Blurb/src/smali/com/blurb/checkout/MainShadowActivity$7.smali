.class Lcom/blurb/checkout/MainShadowActivity$7;
.super Ljava/lang/Object;
.source "MainShadowActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/MainShadowActivity;->setListenersForOrderCount()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/MainShadowActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$7;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 551
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$7;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->getOrderCount()I
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$400(Lcom/blurb/checkout/MainShadowActivity;)I

    move-result v0

    .line 552
    .local v0, "count":I
    if-nez v0, :cond_0

    .line 553
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$7;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 554
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$7;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    .line 555
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$7;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const v2, 0x7f0400f0

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 557
    :cond_0
    return-void
.end method
