.class Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;
.super Lcom/blurb/checkout/WebService$HttpResult;
.source "SamsungAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/SamsungAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ValidateTokenResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;
    }
.end annotation


# instance fields
.field public accessToken:Ljava/lang/String;

.field public errorCode:Ljava/lang/String;

.field public errorMessage:Ljava/lang/String;

.field public userId:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/blurb/checkout/WebService$HttpResult;-><init>()V

    .line 73
    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/blurb/checkout/WebService$HttpResult;-><init>()V

    .line 76
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V

    .line 77
    return-void
.end method


# virtual methods
.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 8
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 80
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V

    .line 85
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 86
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 87
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V

    .line 123
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->getHttpStatus()I

    move-result v6

    const/16 v7, 0xc8

    if-eq v6, v7, :cond_1

    invoke-virtual {p0}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->getHttpStatus()I

    move-result v6

    const/16 v7, 0x190

    if-ne v6, v7, :cond_3

    .line 93
    :cond_1
    :try_start_0
    new-instance v2, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;

    const/4 v6, 0x0

    invoke-direct {v2, p0, v6}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;-><init>(Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;Lcom/blurb/checkout/SamsungAPI$1;)V

    .line 94
    .local v2, "parser":Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;
    const-string v6, "UTF-8"

    invoke-static {v1, v6}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 95
    .local v4, "responseString":Ljava/lang/String;
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 97
    .local v3, "reader":Ljava/io/StringReader;
    # invokes: Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;->parseXml(Ljava/io/Reader;)Z
    invoke-static {v2, v3}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;->access$100(Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;Ljava/io/Reader;)Z

    move-result v5

    .line 98
    .local v5, "success":Z
    if-nez v5, :cond_2

    .line 99
    const/4 v6, -0x6

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    .line 119
    .end local v2    # "parser":Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult$ValidateTokenResponseParser;
    .end local v3    # "reader":Ljava/io/StringReader;
    .end local v4    # "responseString":Ljava/lang/String;
    .end local v5    # "success":Z
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 101
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 102
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v6, -0x2

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V

    .line 103
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 104
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 105
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 106
    const/4 v6, -0x4

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$ValidateTokenResult;->setHttpStatus(I)V

    goto :goto_1

    .line 110
    .end local v0    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_3
    :try_start_2
    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_1

    .line 113
    :catch_3
    move-exception v0

    .line 114
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method
