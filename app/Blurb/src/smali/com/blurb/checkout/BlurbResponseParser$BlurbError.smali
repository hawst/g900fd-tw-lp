.class public Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
.super Ljava/lang/Object;
.source "BlurbResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbResponseParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BlurbError"
.end annotation


# instance fields
.field message:Ljava/lang/String;

.field parameter:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "p"    # Ljava/lang/String;
    .param p2, "m"    # Ljava/lang/String;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->parameter:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->message:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->message:Ljava/lang/String;

    return-object v0
.end method

.method public getParameter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->parameter:Ljava/lang/String;

    return-object v0
.end method
