.class Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;
.super Lcom/blurb/checkout/ActivityAsyncTask;
.source "MainShadowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/MainShadowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckVersionAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/blurb/checkout/ActivityAsyncTask",
        "<",
        "Landroid/app/Activity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private myPackage:Ljava/lang/String;

.field final synthetic this$0:Lcom/blurb/checkout/MainShadowActivity;


# direct methods
.method public constructor <init>(Lcom/blurb/checkout/MainShadowActivity;Landroid/app/Activity;)V
    .locals 1
    .param p2, "a"    # Landroid/app/Activity;

    .prologue
    .line 2340
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    .line 2341
    invoke-direct {p0, p2}, Lcom/blurb/checkout/ActivityAsyncTask;-><init>(Landroid/app/Activity;)V

    .line 2343
    invoke-virtual {p2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->myPackage:Ljava/lang/String;

    .line 2344
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 2351
    invoke-static {}, Lcom/blurb/checkout/BlurbAPI;->getBlurbVersions()Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;

    move-result-object v0

    .line 2353
    .local v0, "result":Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;
    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2337
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->doInBackground([Ljava/lang/Void;)Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;)V
    .locals 8
    .param p1, "result"    # Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;

    .prologue
    .line 2361
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->hideProgress()V

    .line 2363
    const/4 v1, 0x1

    .line 2365
    .local v1, "startSSO":Z
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->isCancelled()Z

    move-result v5

    if-nez v5, :cond_2

    .line 2366
    invoke-virtual {p1}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0xc8

    if-ne v5, v6, :cond_2

    .line 2367
    invoke-virtual {p1}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->getVersions()Ljava/util/Map;

    move-result-object v5

    iget-object v6, p0, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->myPackage:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2369
    .local v3, "version":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 2370
    const-string v5, "BlurbShadowApp"

    const-string v6, "CheckVersionAsyncTask: couldn\'t find package"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2397
    .end local v3    # "version":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 2375
    .restart local v3    # "version":Ljava/lang/String;
    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-virtual {v5}, Lcom/blurb/checkout/MainShadowActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    iget-object v6, p0, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-virtual {v6}, Lcom/blurb/checkout/MainShadowActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget v4, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    .line 2376
    .local v4, "versionCode":I
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2378
    .local v2, "updateVersion":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-le v5, v4, :cond_2

    .line 2379
    iget-object v5, p0, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->handleNewerVersionAvailable()V
    invoke-static {v5}, Lcom/blurb/checkout/MainShadowActivity;->access$3700(Lcom/blurb/checkout/MainShadowActivity;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2380
    const/4 v1, 0x0

    .line 2392
    .end local v2    # "updateVersion":Ljava/lang/Integer;
    .end local v3    # "version":Ljava/lang/String;
    .end local v4    # "versionCode":I
    :cond_2
    :goto_1
    if-eqz v1, :cond_0

    .line 2395
    iget-object v5, p0, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->startSSOSignin()V
    invoke-static {v5}, Lcom/blurb/checkout/MainShadowActivity;->access$3800(Lcom/blurb/checkout/MainShadowActivity;)V

    goto :goto_0

    .line 2382
    .restart local v3    # "version":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2383
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v5, "BlurbShadowApp"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CheckVersionAsyncTask: cannot parse version="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2384
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .line 2385
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v0

    .line 2386
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v5, "BlurbShadowApp"

    const-string v6, "CheckVersionAsyncTask: couldn\'t find package info"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2387
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2337
    check-cast p1, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/MainShadowActivity$CheckVersionAsyncTask;->onPostExecute(Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;)V

    return-void
.end method
