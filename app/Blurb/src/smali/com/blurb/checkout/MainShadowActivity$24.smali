.class Lcom/blurb/checkout/MainShadowActivity$24;
.super Ljava/lang/Object;
.source "MainShadowActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/MainShadowActivity;->handleNewerVersionAvailable()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/MainShadowActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0

    .prologue
    .line 2410
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$24;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 2413
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "market://details?id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/blurb/checkout/MainShadowActivity$24;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-virtual {v4}, Lcom/blurb/checkout/MainShadowActivity;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2414
    .local v2, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2417
    .local v1, "intent":Landroid/content/Intent;
    :try_start_0
    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity$24;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-virtual {v3, v1}, Lcom/blurb/checkout/MainShadowActivity;->startActivity(Landroid/content/Intent;)V

    .line 2420
    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity$24;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-virtual {v3}, Lcom/blurb/checkout/MainShadowActivity;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2424
    :goto_0
    return-void

    .line 2421
    :catch_0
    move-exception v0

    .line 2422
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "BlurbShadowApp"

    const-string v4, "Launch market failed"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
