.class Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;
.super Lcom/blurb/checkout/BlurbResponseParser;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI$BookPricingResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BookPricingResponseParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;


# direct methods
.method private constructor <init>(Lcom/blurb/checkout/BlurbAPI$BookPricingResult;)V
    .locals 0

    .prologue
    .line 1066
    iput-object p1, p0, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    invoke-direct {p0}, Lcom/blurb/checkout/BlurbResponseParser;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/BlurbAPI$BookPricingResult;Lcom/blurb/checkout/BlurbAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/BlurbAPI$BookPricingResult;
    .param p2, "x1"    # Lcom/blurb/checkout/BlurbAPI$1;

    .prologue
    .line 1066
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$BookPricingResult;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;Ljava/io/Reader;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;
    .param p1, "x1"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1066
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->parseXml(Ljava/io/Reader;)V

    return-void
.end method

.method private parseXml(Ljava/io/Reader;)V
    .locals 6
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1069
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 1070
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1071
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 1073
    .local v0, "event":I
    :goto_0
    const/4 v4, 0x1

    if-eq v0, v4, :cond_6

    .line 1076
    packed-switch v0, :pswitch_data_0

    .line 1100
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 1101
    goto :goto_0

    .line 1080
    :pswitch_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1082
    .local v1, "name":Ljava/lang/String;
    const-string v4, "errors"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1083
    invoke-virtual {p0, v2}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->parseErrors(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_1

    .line 1084
    :cond_1
    const-string v4, "cover-type"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1085
    iget-object v4, p0, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    invoke-static {v2, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->coverType:Ljava/lang/String;

    goto :goto_1

    .line 1086
    :cond_2
    const-string v4, "book-type"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1087
    iget-object v4, p0, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    invoke-static {v2, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->bookType:Ljava/lang/String;

    goto :goto_1

    .line 1088
    :cond_3
    const-string v4, "paper-type"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1089
    iget-object v4, p0, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    invoke-static {v2, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->paperType:Ljava/lang/String;

    goto :goto_1

    .line 1090
    :cond_4
    const-string v4, "prices"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1091
    iget-object v4, p0, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v4, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->prices:Ljava/util/List;

    goto :goto_1

    .line 1092
    :cond_5
    const-string v4, "price"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1093
    # invokes: Lcom/blurb/checkout/BlurbAPI;->readPrice(Lorg/xmlpull/v1/XmlPullParser;)Lcom/blurb/checkout/BlurbAPI$Price;
    invoke-static {v2}, Lcom/blurb/checkout/BlurbAPI;->access$800(Lorg/xmlpull/v1/XmlPullParser;)Lcom/blurb/checkout/BlurbAPI$Price;

    move-result-object v3

    .line 1094
    .local v3, "price":Lcom/blurb/checkout/BlurbAPI$Price;
    iget-object v4, p0, Lcom/blurb/checkout/BlurbAPI$BookPricingResult$BookPricingResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    iget-object v4, v4, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->prices:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1102
    .end local v1    # "name":Ljava/lang/String;
    .end local v3    # "price":Lcom/blurb/checkout/BlurbAPI$Price;
    :cond_6
    return-void

    .line 1076
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
