.class public final Lcom/blurb/checkout/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final address_parent_anchor:I = 0x7f060021

.field public static final billing_address:I = 0x7f060002

.field public static final book_pages:I = 0x7f06004c

.field public static final book_title:I = 0x7f06004a

.field public static final book_type:I = 0x7f06004b

.field public static final card_image:I = 0x7f060023

.field public static final choose_country:I = 0x7f06000c

.field public static final city:I = 0x7f060005

.field public static final confirm_address2:I = 0x7f060014

.field public static final confirm_city:I = 0x7f060011

.field public static final confirm_country:I = 0x7f060016

.field public static final confirm_name:I = 0x7f060010

.field public static final confirm_phone:I = 0x7f060017

.field public static final confirm_state:I = 0x7f060012

.field public static final confirm_street:I = 0x7f060013

.field public static final confirm_zip:I = 0x7f060015

.field public static final confirmation_done:I = 0x7f06002f

.field public static final country_chooser:I = 0x7f06000a

.field public static final country_layout:I = 0x7f06000b

.field public static final discount_line:I = 0x7f06002c

.field public static final discount_line_separator:I = 0x7f06002b

.field public static final discount_text:I = 0x7f060050

.field public static final discount_value:I = 0x7f060041

.field public static final emailaddress:I = 0x7f060043

.field public static final error_button:I = 0x7f06001a

.field public static final error_text:I = 0x7f060019

.field public static final family_name:I = 0x7f060004

.field public static final first_name:I = 0x7f060003

.field public static final first_picture:I = 0x7f06001c

.field public static final fourth_email:I = 0x7f06001b

.field public static final header_title:I = 0x7f06000f

.field public static final image_progress_bar:I = 0x7f060030

.field public static final minus_button:I = 0x7f060048

.field public static final next_screen:I = 0x7f060044

.field public static final order_complete_book_type:I = 0x7f06001e

.field public static final order_complete_num_pages:I = 0x7f06001f

.field public static final order_complete_title:I = 0x7f06001d

.field public static final order_count:I = 0x7f06004d

.field public static final order_discount_value:I = 0x7f06002d

.field public static final order_quantity_text:I = 0x7f060020

.field public static final order_shipping_value:I = 0x7f06002a

.field public static final order_steps:I = 0x7f060031

.field public static final order_tax_value:I = 0x7f060029

.field public static final order_total_value:I = 0x7f06002e

.field public static final payment_credit_card_encrypted:I = 0x7f060036

.field public static final payment_scrollable:I = 0x7f060034

.field public static final payment_send_info:I = 0x7f06003c

.field public static final paymentid:I = 0x7f060032

.field public static final paymentinfo_bankcard_cvv:I = 0x7f06003b

.field public static final paymentinfo_bankcard_month:I = 0x7f060038

.field public static final paymentinfo_bankcard_separator:I = 0x7f06003a

.field public static final paymentinfo_bankcard_year:I = 0x7f060039

.field public static final paymentinfo_cardnum:I = 0x7f060037

.field public static final paymentscrollview:I = 0x7f060033

.field public static final picturemessage:I = 0x7f060049

.field public static final plus_button:I = 0x7f06004e

.field public static final price_subtotal:I = 0x7f06004f

.field public static final same_address_switch:I = 0x7f060035

.field public static final select_nations:I = 0x7f060018

.field public static final shipping_address_layout:I = 0x7f060000

.field public static final shipping_information:I = 0x7f060001

.field public static final shipping_scrollable:I = 0x7f060047

.field public static final shipping_scrollview:I = 0x7f060046

.field public static final shipping_value:I = 0x7f060040

.field public static final shippinginfo:I = 0x7f060045

.field public static final state:I = 0x7f060006

.field public static final state_horizontal_layout:I = 0x7f06000e

.field public static final street_address:I = 0x7f060007

.field public static final street_address2:I = 0x7f060008

.field public static final street_phone_number:I = 0x7f06000d

.field public static final tax_line:I = 0x7f060028

.field public static final tax_line_separator:I = 0x7f060027

.field public static final tax_value:I = 0x7f06003f

.field public static final title:I = 0x7f06003d

.field public static final title_product:I = 0x7f060025

.field public static final title_product_value:I = 0x7f060026

.field public static final title_value:I = 0x7f06003e

.field public static final total_value:I = 0x7f060042

.field public static final unrecoverable_body:I = 0x7f060051

.field public static final unrecoverable_done:I = 0x7f060052

.field public static final yourcard:I = 0x7f060024

.field public static final youremail:I = 0x7f060022

.field public static final zip_code:I = 0x7f060009


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
