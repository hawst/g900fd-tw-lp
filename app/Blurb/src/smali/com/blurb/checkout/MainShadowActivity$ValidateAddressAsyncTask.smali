.class Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;
.super Lcom/blurb/checkout/ActivityAsyncTask;
.source "MainShadowActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/MainShadowActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ValidateAddressAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/blurb/checkout/ActivityAsyncTask",
        "<",
        "Landroid/app/Activity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;",
        ">;"
    }
.end annotation


# instance fields
.field city:Ljava/lang/String;

.field firstName:Ljava/lang/String;

.field lastName:Ljava/lang/String;

.field myCountryCode:Ljava/lang/String;

.field private parameters:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field phone:Ljava/lang/String;

.field poBoxOrApo:Ljava/lang/String;

.field state:Ljava/lang/String;

.field street1:Ljava/lang/String;

.field street2:Ljava/lang/String;

.field final synthetic this$0:Lcom/blurb/checkout/MainShadowActivity;

.field zip:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/blurb/checkout/MainShadowActivity;Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2, "a"    # Landroid/app/Activity;
    .param p3, "inFirst"    # Ljava/lang/String;
    .param p4, "inLast"    # Ljava/lang/String;
    .param p5, "inStreet1"    # Ljava/lang/String;
    .param p6, "inStreet2"    # Ljava/lang/String;
    .param p7, "inCity"    # Ljava/lang/String;
    .param p8, "inState"    # Ljava/lang/String;
    .param p9, "inZip"    # Ljava/lang/String;
    .param p10, "inCountryCode"    # Ljava/lang/String;
    .param p11, "inPhone"    # Ljava/lang/String;
    .param p12, "inPoBoxOrApo"    # Ljava/lang/String;

    .prologue
    .line 2147
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    .line 2148
    invoke-direct {p0, p2}, Lcom/blurb/checkout/ActivityAsyncTask;-><init>(Landroid/app/Activity;)V

    .line 2131
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    .line 2134
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "po_box_or_apo"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2135
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "phone"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2136
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "country_id"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2137
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "state_id"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2138
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "state_name"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2139
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "zip"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2140
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "city"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2141
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "street2"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2142
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "street1"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2143
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "first_name"

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2144
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    const-string v1, "last_name"

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2150
    iput-object p3, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->firstName:Ljava/lang/String;

    .line 2151
    iput-object p4, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->lastName:Ljava/lang/String;

    .line 2152
    iput-object p5, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->street1:Ljava/lang/String;

    .line 2153
    iput-object p6, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->street2:Ljava/lang/String;

    .line 2154
    iput-object p7, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->city:Ljava/lang/String;

    .line 2155
    iput-object p8, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->state:Ljava/lang/String;

    .line 2156
    iput-object p9, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->zip:Ljava/lang/String;

    .line 2157
    iput-object p10, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->myCountryCode:Ljava/lang/String;

    .line 2158
    iput-object p11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->phone:Ljava/lang/String;

    .line 2159
    iput-object p12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->poBoxOrApo:Ljava/lang/String;

    .line 2160
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;
    .locals 19
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 2166
    new-instance v18, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;

    const/4 v1, 0x0

    move-object/from16 v0, v18

    invoke-direct {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;-><init>(Lcom/blurb/checkout/MainShadowActivity$1;)V

    .line 2168
    .local v18, "result":Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->firstName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->lastName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->street1:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->street2:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->city:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->zip:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->state:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->myCountryCode:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->phone:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->poBoxOrApo:Ljava/lang/String;

    invoke-static/range {v1 .. v10}, Lcom/blurb/checkout/BlurbAPI;->postValidateAddress(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;

    move-result-object v1

    move-object/from16 v0, v18

    iput-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->validateAddressResult:Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;

    .line 2169
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->validateAddressResult:Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->getHttpStatus()I

    move-result v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->setHttpStatus(I)V

    .line 2171
    invoke-virtual/range {v18 .. v18}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->getHttpStatus()I

    move-result v1

    const/16 v3, 0xc8

    if-ne v1, v3, :cond_1

    .line 2172
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->episodeId:Ljava/lang/String;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2800(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->sessionId:Ljava/lang/String;
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->access$2100(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->bookType:Ljava/lang/String;
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->access$1700(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->coverType:Ljava/lang/String;
    invoke-static {v4}, Lcom/blurb/checkout/MainShadowActivity;->access$1600(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->title:Ljava/lang/String;
    invoke-static {v5}, Lcom/blurb/checkout/MainShadowActivity;->access$2900(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->numPages:Ljava/lang/String;
    invoke-static {v6}, Lcom/blurb/checkout/MainShadowActivity;->access$1500(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v1 .. v6}, Lcom/blurb/checkout/BlurbAPI;->postCreateBook(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$CreateBookResult;

    move-result-object v1

    move-object/from16 v0, v18

    iput-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createBookResult:Lcom/blurb/checkout/BlurbAPI$CreateBookResult;

    .line 2173
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createBookResult:Lcom/blurb/checkout/BlurbAPI$CreateBookResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;->getHttpStatus()I

    move-result v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->setHttpStatus(I)V

    .line 2175
    invoke-virtual/range {v18 .. v18}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->getHttpStatus()I

    move-result v1

    const/16 v3, 0xc8

    if-ne v1, v3, :cond_1

    .line 2179
    const-string v2, ""

    .line 2181
    .local v2, "addressId":Ljava/lang/String;
    const-string v16, ""

    .line 2183
    .local v16, "couponCode":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->orderCountEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 2185
    .local v14, "quantity":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2400(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v9, ""

    .line 2186
    .local v9, "state":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v10, ""

    .line 2188
    .local v10, "zip":Ljava/lang/String;
    :goto_1
    const-string v1, "us"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->myCountryCode:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2189
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v9

    .line 2191
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->sessionId:Ljava/lang/String;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2100(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->firstName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->lastName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->street1:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->street2:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->poBoxOrApo:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->city:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->myCountryCode:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->phone:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v13, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createBookResult:Lcom/blurb/checkout/BlurbAPI$CreateBookResult;

    iget-object v13, v13, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;->bookId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->currencyId:Ljava/lang/String;
    invoke-static {v15}, Lcom/blurb/checkout/MainShadowActivity;->access$200(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->coverType:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/MainShadowActivity;->access$1600(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v1 .. v17}, Lcom/blurb/checkout/BlurbAPI;->postCreateCheckout(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    move-result-object v1

    move-object/from16 v0, v18

    iput-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createCheckoutResult:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    .line 2209
    move-object/from16 v0, v18

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createCheckoutResult:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->getHttpStatus()I

    move-result v1

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->setHttpStatus(I)V

    .line 2213
    .end local v2    # "addressId":Ljava/lang/String;
    .end local v9    # "state":Ljava/lang/String;
    .end local v10    # "zip":Ljava/lang/String;
    .end local v14    # "quantity":I
    .end local v16    # "couponCode":Ljava/lang/String;
    :cond_1
    return-object v18

    .line 2185
    .restart local v2    # "addressId":Ljava/lang/String;
    .restart local v14    # "quantity":I
    .restart local v16    # "couponCode":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2400(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_0

    .line 2186
    .restart local v9    # "state":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$2500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2119
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->doInBackground([Ljava/lang/Void;)Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;)V
    .locals 14
    .param p1, "result"    # Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;

    .prologue
    .line 2221
    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->hideProgress()V

    .line 2223
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->isCancelled()Z

    move-result v11

    if-nez v11, :cond_1

    .line 2224
    invoke-virtual {p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->getHttpStatus()I

    move-result v11

    const/16 v12, 0xc8

    if-eq v11, v12, :cond_0

    invoke-virtual {p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->getHttpStatus()I

    move-result v11

    const/16 v12, 0x190

    if-ne v11, v12, :cond_e

    .line 2225
    :cond_0
    const/4 v1, 0x0

    .line 2227
    .local v1, "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    iget-object v11, p1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createCheckoutResult:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    if-eqz v11, :cond_2

    .line 2228
    iget-object v11, p1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createCheckoutResult:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    invoke-virtual {v11}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->getErrors()Ljava/util/List;

    move-result-object v1

    .line 2235
    :goto_0
    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_5

    .line 2236
    const/4 v11, 0x0

    invoke-interface {v1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;

    .line 2237
    .local v0, "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->parameters:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->getParameter()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    .line 2238
    .local v4, "field":Ljava/lang/Integer;
    invoke-virtual {v0}, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->getMessage()Ljava/lang/String;

    move-result-object v6

    .line 2240
    .local v6, "message":Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 2241
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v12

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->handleValidationError(ILjava/lang/String;)V
    invoke-static {v11, v12, v6}, Lcom/blurb/checkout/MainShadowActivity;->access$3000(Lcom/blurb/checkout/MainShadowActivity;ILjava/lang/String;)V

    .line 2326
    .end local v0    # "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    .end local v1    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    .end local v4    # "field":Ljava/lang/Integer;
    .end local v6    # "message":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 2229
    .restart local v1    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    :cond_2
    iget-object v11, p1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createBookResult:Lcom/blurb/checkout/BlurbAPI$CreateBookResult;

    if-eqz v11, :cond_3

    .line 2230
    iget-object v11, p1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createBookResult:Lcom/blurb/checkout/BlurbAPI$CreateBookResult;

    invoke-virtual {v11}, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;->getErrors()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 2232
    :cond_3
    iget-object v11, p1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->validateAddressResult:Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;

    invoke-virtual {v11}, Lcom/blurb/checkout/BlurbAPI$ValidateAddressResult;->getErrors()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    .line 2243
    .restart local v0    # "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    .restart local v4    # "field":Ljava/lang/Integer;
    .restart local v6    # "message":Ljava/lang/String;
    :cond_4
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v6, v12}, Lcom/blurb/checkout/BlurbAPI;->codeToMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2244
    .local v7, "msg":Ljava/lang/String;
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const/4 v12, 0x0

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->showError(Ljava/lang/String;Z)V
    invoke-static {v11, v7, v12}, Lcom/blurb/checkout/MainShadowActivity;->access$000(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;Z)V

    goto :goto_1

    .line 2247
    .end local v0    # "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    .end local v4    # "field":Ljava/lang/Integer;
    .end local v6    # "message":Ljava/lang/String;
    .end local v7    # "msg":Ljava/lang/String;
    :cond_5
    iget-object v11, p1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createCheckoutResult:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    if-eqz v11, :cond_d

    invoke-virtual {p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->getHttpStatus()I

    move-result v11

    const/16 v12, 0xc8

    if-ne v11, v12, :cond_d

    .line 2258
    new-instance v5, Landroid/content/Intent;

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const-class v12, Lcom/blurb/checkout/PaymentActivity;

    invoke-direct {v5, v11, v12}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2260
    .local v5, "intent":Landroid/content/Intent;
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/blurb/checkout/MainShadowActivity;->access$2500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v11

    if-nez v11, :cond_9

    const-string v10, ""

    .line 2261
    .local v10, "zip":Ljava/lang/String;
    :goto_2
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/blurb/checkout/MainShadowActivity;->access$2400(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v11

    if-nez v11, :cond_b

    const-string v8, ""

    .line 2263
    .local v8, "state":Ljava/lang/String;
    :goto_3
    const-string v11, "firstName"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->firstNameEditText:Landroid/widget/EditText;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$3100(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2264
    const-string v11, "lastName"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->familyNameEditText:Landroid/widget/EditText;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$3200(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2265
    const-string v11, "streetAddress1"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->streetAddressEditText:Landroid/widget/EditText;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$2200(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2267
    const-string v9, ""

    .line 2268
    .local v9, "streetAddress2":Ljava/lang/String;
    const-string v11, "kr"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_6

    const-string v11, "jp"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$1000(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 2269
    :cond_6
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const v12, 0x7f060008

    invoke-virtual {v11, v12}, Lcom/blurb/checkout/MainShadowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 2270
    .local v2, "et":Landroid/widget/EditText;
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    .line 2272
    .end local v2    # "et":Landroid/widget/EditText;
    :cond_7
    const-string v11, "streetAddress2"

    invoke-virtual {v5, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2274
    const-string v11, "cityEditText"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->cityEditText:Landroid/widget/EditText;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$2300(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v12

    invoke-virtual {v12}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2275
    const-string v11, "stateEditText"

    invoke-virtual {v5, v11, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2276
    const-string v11, "postalCode"

    invoke-virtual {v5, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2277
    const-string v11, "country"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->myCountryCode:Ljava/lang/String;

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2279
    const-string v11, "orderCheckout"

    iget-object v12, p1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createCheckoutResult:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v12, v12, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2280
    const-string v11, "sessionId"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->sessionId:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$2100(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2281
    const-string v11, "email"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->email:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$3300(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2282
    const-string v11, "phone"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->phone:Ljava/lang/String;

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2284
    const-string v11, "episode_id"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->episodeId:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$2800(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2285
    const-string v11, "currency_id"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->currencyId:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$200(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2286
    const-string v11, "book_type"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->bookType:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$1700(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2287
    const-string v11, "cover_type"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->coverType:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$1600(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2288
    const-string v11, "title"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->title:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$2900(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2289
    const-string v11, "cover_data"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->coverData:[B
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$3400(Lcom/blurb/checkout/MainShadowActivity;)[B

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 2291
    const-string v11, "num_pages"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->pageCount:I
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$1400(Lcom/blurb/checkout/MainShadowActivity;)I

    move-result v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2292
    const-string v11, "quantity"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->quantity:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$3500(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2294
    const-string v11, "total_price"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->getOrderTotal()D
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$100(Lcom/blurb/checkout/MainShadowActivity;)D

    move-result-wide v12

    invoke-virtual {v5, v11, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 2296
    const-string v11, "bookId"

    iget-object v12, p1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->createBookResult:Lcom/blurb/checkout/BlurbAPI$CreateBookResult;

    iget-object v12, v12, Lcom/blurb/checkout/BlurbAPI$CreateBookResult;->bookId:Ljava/lang/String;

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2297
    const-string v11, "couponCode"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->couponCode:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/MainShadowActivity;->access$3600(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2299
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    iget-object v11, v11, Lcom/blurb/checkout/MainShadowActivity;->billingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

    if-eqz v11, :cond_8

    .line 2300
    const-string v11, "billingAddress"

    iget-object v12, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    iget-object v12, v12, Lcom/blurb/checkout/MainShadowActivity;->billingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2302
    :cond_8
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const/16 v12, 0xb

    invoke-virtual {v11, v5, v12}, Lcom/blurb/checkout/MainShadowActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 2260
    .end local v8    # "state":Ljava/lang/String;
    .end local v9    # "streetAddress2":Ljava/lang/String;
    .end local v10    # "zip":Ljava/lang/String;
    :cond_9
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/blurb/checkout/MainShadowActivity;->access$2500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/EditText;->getVisibility()I

    move-result v11

    if-nez v11, :cond_a

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->zipCodeEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/blurb/checkout/MainShadowActivity;->access$2500(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_2

    :cond_a
    const-string v10, ""

    goto/16 :goto_2

    .line 2261
    .restart local v10    # "zip":Ljava/lang/String;
    :cond_b
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/blurb/checkout/MainShadowActivity;->access$2400(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/EditText;->getVisibility()I

    move-result v11

    if-nez v11, :cond_c

    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->stateEditText:Landroid/widget/EditText;
    invoke-static {v11}, Lcom/blurb/checkout/MainShadowActivity;->access$2400(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/EditText;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_3

    :cond_c
    const-string v8, ""

    goto/16 :goto_3

    .line 2305
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v10    # "zip":Ljava/lang/String;
    :cond_d
    const-string v11, "BlurbShadowApp"

    const-string v12, "ValidateAddressAsyncTask: checkout result failed, but no errors were returned"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2306
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const/4 v12, 0x3

    invoke-virtual {v11, v12}, Lcom/blurb/checkout/MainShadowActivity;->showDialog(I)V

    goto/16 :goto_1

    .line 2310
    .end local v1    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    :cond_e
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-virtual {p1, v11}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->getMessageForStatus(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    .line 2311
    .restart local v6    # "message":Ljava/lang/String;
    const/4 v3, 0x1

    .line 2313
    .local v3, "fatal":Z
    invoke-virtual {p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;->getHttpStatus()I

    move-result v11

    packed-switch v11, :pswitch_data_0

    .line 2321
    :goto_4
    iget-object v11, p0, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->showError(Ljava/lang/String;Z)V
    invoke-static {v11, v6, v3}, Lcom/blurb/checkout/MainShadowActivity;->access$000(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 2317
    :pswitch_0
    const/4 v3, 0x0

    goto :goto_4

    .line 2313
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2119
    check-cast p1, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/MainShadowActivity$ValidateAddressAsyncTask;->onPostExecute(Lcom/blurb/checkout/MainShadowActivity$ValidateAddressCreateBookAndCheckoutResult;)V

    return-void
.end method
