.class Lcom/blurb/checkout/MainShadowActivity$18;
.super Ljava/lang/Object;
.source "MainShadowActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/MainShadowActivity;->popupCountryView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/MainShadowActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0

    .prologue
    .line 1553
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$18;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1558
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$18;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countriesListView:Landroid/widget/ListView;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$800(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1562
    .local v0, "country":Ljava/lang/String;
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$18;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryEditText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$900(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1563
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$18;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    iget-object v2, p0, Lcom/blurb/checkout/MainShadowActivity$18;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-static {v2, v0}, Lcom/blurb/checkout/MainShadowActivity;->getCountryCode(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # setter for: Lcom/blurb/checkout/MainShadowActivity;->countryCode:Ljava/lang/String;
    invoke-static {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1002(Lcom/blurb/checkout/MainShadowActivity;Ljava/lang/String;)Ljava/lang/String;

    .line 1564
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$18;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->updateCountryFields()V
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$1100(Lcom/blurb/checkout/MainShadowActivity;)V

    .line 1566
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$18;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->countryDialog:Landroid/app/Dialog;
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$1200(Lcom/blurb/checkout/MainShadowActivity;)Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    .line 1567
    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$18;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    const/4 v2, 0x0

    # setter for: Lcom/blurb/checkout/MainShadowActivity;->countryDialog:Landroid/app/Dialog;
    invoke-static {v1, v2}, Lcom/blurb/checkout/MainShadowActivity;->access$1202(Lcom/blurb/checkout/MainShadowActivity;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 1568
    return-void
.end method
