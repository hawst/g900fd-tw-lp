.class Lcom/blurb/checkout/MainShadowActivity$6;
.super Ljava/lang/Object;
.source "MainShadowActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/MainShadowActivity;->setListenersForOrderCount()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/MainShadowActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/MainShadowActivity;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$6;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 544
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$6;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->priceSubtotalView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/blurb/checkout/MainShadowActivity;->access$300(Lcom/blurb/checkout/MainShadowActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/blurb/checkout/MainShadowActivity$6;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # invokes: Lcom/blurb/checkout/MainShadowActivity;->getOrderTotal()D
    invoke-static {v1}, Lcom/blurb/checkout/MainShadowActivity;->access$100(Lcom/blurb/checkout/MainShadowActivity;)D

    move-result-wide v1

    iget-object v3, p0, Lcom/blurb/checkout/MainShadowActivity$6;->this$0:Lcom/blurb/checkout/MainShadowActivity;

    # getter for: Lcom/blurb/checkout/MainShadowActivity;->currencyId:Ljava/lang/String;
    invoke-static {v3}, Lcom/blurb/checkout/MainShadowActivity;->access$200(Lcom/blurb/checkout/MainShadowActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPrice(DLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 545
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 540
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 536
    return-void
.end method
