.class Lcom/blurb/checkout/PaymentActivity$2;
.super Ljava/lang/Object;
.source "PaymentActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/PaymentActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/PaymentActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/PaymentActivity;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/blurb/checkout/PaymentActivity$2;->this$0:Lcom/blurb/checkout/PaymentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 247
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$2;->this$0:Lcom/blurb/checkout/PaymentActivity;

    const v2, 0x7f060021

    invoke-virtual {v1, v2}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 248
    .local v0, "address":Landroid/view/View;
    if-eqz p2, :cond_0

    .line 249
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 254
    :goto_0
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$2;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # invokes: Lcom/blurb/checkout/PaymentActivity;->updateBillingAddressFields(Z)V
    invoke-static {v1, p2}, Lcom/blurb/checkout/PaymentActivity;->access$200(Lcom/blurb/checkout/PaymentActivity;Z)V

    .line 255
    return-void

    .line 251
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
