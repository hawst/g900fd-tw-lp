.class Lcom/blurb/checkout/WebService$HttpResult;
.super Ljava/lang/Object;
.source "WebService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/WebService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "HttpResult"
.end annotation


# static fields
.field static final ERROR_AUTHENTICATION:I = -0x6

.field static final ERROR_CLIENT_PROTOCOL:I = -0x1

.field static final ERROR_IO:I = -0x2

.field static final ERROR_IO_INTERRUPTED:I = -0x3

.field static final ERROR_PARSE_EXCEPTION:I = -0x5

.field static final ERROR_UNKNOWN:I = 0x0

.field static final ERROR_XML:I = -0x4


# instance fields
.field private mHttpStatus:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v0, p0, Lcom/blurb/checkout/WebService$HttpResult;->mHttpStatus:I

    .line 103
    iput v0, p0, Lcom/blurb/checkout/WebService$HttpResult;->mHttpStatus:I

    .line 104
    return-void
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/blurb/checkout/WebService$HttpResult;->mHttpStatus:I

    .line 107
    iput p1, p0, Lcom/blurb/checkout/WebService$HttpResult;->mHttpStatus:I

    .line 108
    return-void
.end method

.method static getMessageForStatus(Landroid/content/Context;I)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "code"    # I

    .prologue
    const v3, 0x7f040088

    const v1, 0x7f040082

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 60
    if-lez p1, :cond_0

    .line 61
    packed-switch p1, :pswitch_data_0

    .line 66
    const v1, 0x7f040083

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "message":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 63
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_0
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 64
    .restart local v0    # "message":Ljava/lang/String;
    goto :goto_0

    .line 70
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    packed-switch p1, :pswitch_data_1

    .line 90
    :pswitch_1
    const-string v1, "Internal error: unknown code %1$s"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "message":Ljava/lang/String;
    goto :goto_0

    .line 72
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_2
    const v1, 0x7f040084

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 73
    .restart local v0    # "message":Ljava/lang/String;
    goto :goto_0

    .line 75
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_3
    const v1, 0x7f040085

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    .restart local v0    # "message":Ljava/lang/String;
    goto :goto_0

    .line 78
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_4
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 79
    .restart local v0    # "message":Ljava/lang/String;
    goto :goto_0

    .line 81
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_5
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 82
    .restart local v0    # "message":Ljava/lang/String;
    goto :goto_0

    .line 84
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_6
    const v1, 0x7f040087

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 85
    .restart local v0    # "message":Ljava/lang/String;
    goto :goto_0

    .line 87
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_7
    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 88
    .restart local v0    # "message":Ljava/lang/String;
    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x191
        :pswitch_0
    .end packed-switch

    .line 70
    :pswitch_data_1
    .packed-switch -0x6
        :pswitch_7
        :pswitch_1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getHttpStatus()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lcom/blurb/checkout/WebService$HttpResult;->mHttpStatus:I

    return v0
.end method

.method getMessageForStatus(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/blurb/checkout/WebService$HttpResult;->getHttpStatus()I

    move-result v0

    invoke-static {p1, v0}, Lcom/blurb/checkout/WebService$HttpResult;->getMessageForStatus(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 1
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 54
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/blurb/checkout/WebService$HttpResult;->setHttpStatus(I)V

    .line 55
    return-void
.end method

.method setHttpStatus(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/blurb/checkout/WebService$HttpResult;->mHttpStatus:I

    .line 51
    return-void
.end method
