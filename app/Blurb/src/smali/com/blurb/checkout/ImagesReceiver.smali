.class public Lcom/blurb/checkout/ImagesReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ImagesReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/ImagesReceiver$RenderedImages;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "BlurbShadowApp"

.field private static renderedImages:Lcom/blurb/checkout/ImagesReceiver$RenderedImages;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 14
    return-void
.end method

.method static forgetRenderedImages()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-object v0, Lcom/blurb/checkout/ImagesReceiver;->renderedImages:Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    .line 43
    return-void
.end method

.method static getRenderedImages(Ljava/lang/String;)Lcom/blurb/checkout/ImagesReceiver$RenderedImages;
    .locals 4
    .param p0, "episodeId"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 31
    sget-object v1, Lcom/blurb/checkout/ImagesReceiver;->renderedImages:Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    if-nez v1, :cond_0

    .line 38
    :goto_0
    return-object v0

    .line 34
    :cond_0
    sget-object v1, Lcom/blurb/checkout/ImagesReceiver;->renderedImages:Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    iget-object v1, v1, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->episodeId:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 35
    sget-object v0, Lcom/blurb/checkout/ImagesReceiver;->renderedImages:Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    goto :goto_0

    .line 37
    :cond_1
    const-string v1, "BlurbShadowApp"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRenderedImages: wanted episodeId="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", have="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/blurb/checkout/ImagesReceiver;->renderedImages:Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    iget-object v3, v3, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->episodeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static putImagesResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Z)Lcom/blurb/checkout/ImagesReceiver$RenderedImages;
    .locals 1
    .param p0, "episodeId"    # Ljava/lang/String;
    .param p1, "frontCover"    # Ljava/lang/String;
    .param p2, "backCover"    # Ljava/lang/String;
    .param p3, "spine"    # Ljava/lang/String;
    .param p5, "failed"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/blurb/checkout/ImagesReceiver$RenderedImages;"
        }
    .end annotation

    .prologue
    .line 46
    .local p4, "photos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    invoke-direct {v0}, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;-><init>()V

    .line 48
    .local v0, "images":Lcom/blurb/checkout/ImagesReceiver$RenderedImages;
    iput-object p0, v0, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->episodeId:Ljava/lang/String;

    .line 49
    iput-object p4, v0, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->imageList:Ljava/util/ArrayList;

    .line 50
    iput-object p1, v0, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->frontCover:Ljava/lang/String;

    .line 51
    iput-object p2, v0, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->backCover:Ljava/lang/String;

    .line 52
    iput-object p3, v0, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->spine:Ljava/lang/String;

    .line 53
    iput-boolean p5, v0, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->renderFailed:Z

    .line 55
    sput-object v0, Lcom/blurb/checkout/ImagesReceiver;->renderedImages:Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    .line 56
    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    .line 65
    .local v6, "action":Ljava/lang/String;
    const-string v0, "com.samsung.android.app.episodes.action.IMAGES_READY"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    const-string v0, "resultCode"

    const/16 v2, -0x3e7

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 71
    .local v8, "resultCode":I
    const/4 v0, -0x1

    if-ne v8, v0, :cond_0

    .line 72
    const-string v0, "episode_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "front_cover"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "back_cover"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "spine"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "image_uris"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/blurb/checkout/ImagesReceiver;->putImagesResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Z)Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    .line 100
    .end local v8    # "resultCode":I
    :goto_0
    return-void

    .line 86
    .restart local v8    # "resultCode":I
    :cond_0
    const-string v0, "episode_id"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x1

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    invoke-static/range {v0 .. v5}, Lcom/blurb/checkout/ImagesReceiver;->putImagesResult(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Z)Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    move-result-object v7

    .line 95
    .local v7, "images":Lcom/blurb/checkout/ImagesReceiver$RenderedImages;
    const-string v0, "BlurbShadowApp"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ImagesReceiver Image render failed episode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->episodeId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 98
    .end local v7    # "images":Lcom/blurb/checkout/ImagesReceiver$RenderedImages;
    .end local v8    # "resultCode":I
    :cond_1
    const-string v0, "BlurbShadowApp"

    const-string v1, "ImagesReceiver Unknown broadcast"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
