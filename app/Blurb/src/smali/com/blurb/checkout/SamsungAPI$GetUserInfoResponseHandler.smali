.class Lcom/blurb/checkout/SamsungAPI$GetUserInfoResponseHandler;
.super Ljava/lang/Object;
.source "SamsungAPI.java"

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/SamsungAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetUserInfoResponseHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;
    .locals 1
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 278
    new-instance v0, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    invoke-direct {v0}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;-><init>()V

    .line 280
    .local v0, "result":Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;
    invoke-virtual {v0, p1}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpResponse(Lorg/apache/http/HttpResponse;)V

    .line 282
    return-object v0
.end method

.method public bridge synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 272
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;

    move-result-object v0

    return-object v0
.end method
