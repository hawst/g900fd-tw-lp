.class Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;
.super Lcom/blurb/checkout/BlurbAPI$BlurbResult;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BlurbVersionsResult"
.end annotation


# instance fields
.field private versions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2322
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 2316
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->versions:Ljava/util/HashMap;

    .line 2323
    return-void
.end method

.method constructor <init>(I)V
    .locals 1
    .param p1, "httpStatus"    # I

    .prologue
    .line 2325
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 2316
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->versions:Ljava/util/HashMap;

    .line 2326
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->setHttpStatus(I)V

    .line 2327
    return-void
.end method


# virtual methods
.method public getVersions()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2319
    iget-object v0, p0, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->versions:Ljava/util/HashMap;

    return-object v0
.end method

.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 8
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 2330
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->setHttpStatus(I)V

    .line 2335
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 2336
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 2337
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->setHttpStatus(I)V

    .line 2371
    :goto_0
    return-void

    .line 2341
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->getHttpStatus()I

    move-result v6

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_2

    .line 2343
    :try_start_0
    const-string v6, "UTF-8"

    invoke-static {v1, v6}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2345
    .local v5, "responseString":Ljava/lang/String;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 2346
    .local v3, "json":Lorg/json/JSONObject;
    if-eqz v3, :cond_2

    .line 2347
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .line 2348
    .local v2, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2349
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2351
    .local v4, "pkg":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 2352
    iget-object v6, p0, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->versions:Ljava/util/HashMap;

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 2357
    .end local v2    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v3    # "json":Lorg/json/JSONObject;
    .end local v4    # "pkg":Ljava/lang/String;
    .end local v5    # "responseString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 2358
    .local v0, "e":Ljava/io/IOException;
    const/4 v6, -0x2

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->setHttpStatus(I)V

    .line 2359
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 2367
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    :goto_2
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 2368
    :catch_1
    move-exception v0

    .line 2369
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 2360
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 2361
    .local v0, "e":Lorg/json/JSONException;
    const/4 v6, -0x5

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/BlurbAPI$BlurbVersionsResult;->setHttpStatus(I)V

    .line 2362
    invoke-virtual {v0}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_2
.end method
