.class Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;
.super Lcom/blurb/checkout/BlurbResponseParser;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckoutResponseParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;


# direct methods
.method private constructor <init>(Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    invoke-direct {p0}, Lcom/blurb/checkout/BlurbResponseParser;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;Lcom/blurb/checkout/BlurbAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;
    .param p2, "x1"    # Lcom/blurb/checkout/BlurbAPI$1;

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;Ljava/io/Reader;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;
    .param p1, "x1"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 301
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->parseXml(Ljava/io/Reader;)V

    return-void
.end method

.method private parseXml(Ljava/io/Reader;)V
    .locals 7
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 303
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v4

    .line 304
    .local v4, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v4, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 305
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    .line 307
    .local v1, "event":I
    :goto_0
    const/4 v5, 0x1

    if-eq v1, v5, :cond_12

    .line 310
    packed-switch v1, :pswitch_data_0

    .line 383
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    .line 384
    goto :goto_0

    .line 314
    :pswitch_1
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    .line 316
    .local v3, "name":Ljava/lang/String;
    const-string v5, "errors"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 317
    invoke-virtual {p0, v4}, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->parseErrors(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_1

    .line 318
    :cond_1
    const-string v5, "id"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 319
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->checkoutId:Ljava/lang/String;

    goto :goto_1

    .line 320
    :cond_2
    const-string v5, "product_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 321
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v5

    if-ge v2, v5, :cond_5

    .line 322
    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 323
    .local v0, "a":Ljava/lang/String;
    const-string v5, "currency_id"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 324
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    .line 327
    :cond_3
    const-string v5, "cents"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 328
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->productCents:Ljava/lang/String;

    .line 321
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 332
    .end local v0    # "a":Ljava/lang/String;
    :cond_5
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->productTotal:Ljava/lang/String;

    goto :goto_1

    .line 333
    .end local v2    # "i":I
    :cond_6
    const-string v5, "shipping_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 334
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v5

    if-ge v2, v5, :cond_8

    .line 335
    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 337
    .restart local v0    # "a":Ljava/lang/String;
    const-string v5, "cents"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 338
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->shippingCents:Ljava/lang/String;

    .line 334
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 342
    .end local v0    # "a":Ljava/lang/String;
    :cond_8
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->shippingTotal:Ljava/lang/String;

    goto/16 :goto_1

    .line 344
    .end local v2    # "i":I
    :cond_9
    const-string v5, "tax_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 347
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v5

    if-ge v2, v5, :cond_b

    .line 348
    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 350
    .restart local v0    # "a":Ljava/lang/String;
    const-string v5, "cents"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 351
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->taxCents:Ljava/lang/String;

    .line 347
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 355
    .end local v0    # "a":Ljava/lang/String;
    :cond_b
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->taxTotal:Ljava/lang/String;

    goto/16 :goto_1

    .line 356
    .end local v2    # "i":I
    :cond_c
    const-string v5, "discount_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 359
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_5
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v5

    if-ge v2, v5, :cond_e

    .line 360
    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 362
    .restart local v0    # "a":Ljava/lang/String;
    const-string v5, "cents"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 363
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->discountCents:Ljava/lang/String;

    .line 359
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 367
    .end local v0    # "a":Ljava/lang/String;
    :cond_e
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->discountTotal:Ljava/lang/String;

    goto/16 :goto_1

    .line 368
    .end local v2    # "i":I
    :cond_f
    const-string v5, "order_total"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 369
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_6
    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v5

    if-ge v2, v5, :cond_11

    .line 370
    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    .restart local v0    # "a":Ljava/lang/String;
    const-string v5, "cents"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    .line 373
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->orderCents:Ljava/lang/String;

    .line 369
    :cond_10
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 377
    .end local v0    # "a":Ljava/lang/String;
    :cond_11
    iget-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult$CheckoutResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;

    iget-object v5, v5, Lcom/blurb/checkout/BlurbAPI$CreateCheckoutResult;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;

    invoke-interface {v4}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/blurb/checkout/BlurbAPI$Checkout;->orderTotal:Ljava/lang/String;

    goto/16 :goto_1

    .line 385
    .end local v2    # "i":I
    .end local v3    # "name":Ljava/lang/String;
    :cond_12
    return-void

    .line 310
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
