.class Lcom/blurb/checkout/PaymentActivity$8;
.super Ljava/lang/Object;
.source "PaymentActivity.java"

# interfaces
.implements Lcom/blurb/checkout/ui/EnhancedScrollView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/PaymentActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field focusedTextView:Landroid/widget/EditText;

.field final synthetic this$0:Lcom/blurb/checkout/PaymentActivity;


# direct methods
.method constructor <init>(Lcom/blurb/checkout/PaymentActivity;)V
    .locals 0

    .prologue
    .line 404
    iput-object p1, p0, Lcom/blurb/checkout/PaymentActivity$8;->this$0:Lcom/blurb/checkout/PaymentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEndScroll()V
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity$8;->focusedTextView:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/blurb/checkout/PaymentActivity$8;->focusedTextView:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 420
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity$8;->focusedTextView:Landroid/widget/EditText;

    .line 422
    :cond_0
    return-void
.end method

.method public onStartScroll()V
    .locals 3

    .prologue
    .line 409
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$8;->this$0:Lcom/blurb/checkout/PaymentActivity;

    invoke-virtual {v1}, Lcom/blurb/checkout/PaymentActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 410
    .local v0, "focusedView":Landroid/view/View;
    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 411
    check-cast v0, Landroid/widget/EditText;

    .end local v0    # "focusedView":Landroid/view/View;
    iput-object v0, p0, Lcom/blurb/checkout/PaymentActivity$8;->focusedTextView:Landroid/widget/EditText;

    .line 412
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$8;->this$0:Lcom/blurb/checkout/PaymentActivity;

    const v2, 0x7f060034

    invoke-virtual {v1, v2}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 414
    :cond_0
    return-void
.end method
