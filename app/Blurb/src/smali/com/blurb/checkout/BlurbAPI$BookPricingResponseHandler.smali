.class Lcom/blurb/checkout/BlurbAPI$BookPricingResponseHandler;
.super Ljava/lang/Object;
.source "BlurbAPI.java"

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BookPricingResponseHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Lcom/blurb/checkout/BlurbAPI$BookPricingResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1154
    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/BlurbAPI$BookPricingResult;
    .locals 1
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 1157
    new-instance v0, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;-><init>()V

    .line 1159
    .local v0, "result":Lcom/blurb/checkout/BlurbAPI$BookPricingResult;
    invoke-virtual {v0, p1}, Lcom/blurb/checkout/BlurbAPI$BookPricingResult;->setHttpResponse(Lorg/apache/http/HttpResponse;)V

    .line 1161
    return-object v0
.end method

.method public bridge synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1151
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$BookPricingResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/BlurbAPI$BookPricingResult;

    move-result-object v0

    return-object v0
.end method
