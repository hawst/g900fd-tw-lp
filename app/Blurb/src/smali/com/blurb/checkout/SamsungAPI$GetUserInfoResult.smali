.class Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;
.super Lcom/blurb/checkout/WebService$HttpResult;
.source "SamsungAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/SamsungAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "GetUserInfoResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;
    }
.end annotation


# instance fields
.field public countryCode:Ljava/lang/String;

.field public familyName:Ljava/lang/String;

.field public givenName:Ljava/lang/String;

.field public loginID:Ljava/lang/String;

.field public postalCodeText:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/blurb/checkout/WebService$HttpResult;-><init>()V

    .line 224
    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/blurb/checkout/WebService$HttpResult;-><init>()V

    .line 227
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V

    .line 228
    return-void
.end method


# virtual methods
.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 8
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 231
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V

    .line 236
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 237
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 238
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V

    .line 269
    :goto_0
    return-void

    .line 242
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->getHttpStatus()I

    move-result v6

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_1

    .line 244
    :try_start_0
    const-string v6, "UTF-8"

    invoke-static {v1, v6}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 247
    .local v4, "responseString":Ljava/lang/String;
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 248
    .local v3, "reader":Ljava/io/StringReader;
    new-instance v2, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;

    const/4 v6, 0x0

    invoke-direct {v2, p0, v6}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;-><init>(Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;Lcom/blurb/checkout/SamsungAPI$1;)V

    .line 250
    .local v2, "parser":Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;
    # invokes: Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;->parseXml(Ljava/io/Reader;)Z
    invoke-static {v2, v3}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;->access$300(Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;Ljava/io/Reader;)Z

    move-result v5

    .line 251
    .local v5, "success":Z
    if-nez v5, :cond_1

    .line 253
    const/4 v6, -0x6

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    .line 265
    .end local v2    # "parser":Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult$GetUserInfoResponseParser;
    .end local v3    # "reader":Ljava/io/StringReader;
    .end local v4    # "responseString":Ljava/lang/String;
    .end local v5    # "success":Z
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 255
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 256
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v6, -0x2

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V

    .line 257
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 258
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 259
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 260
    const/4 v6, -0x4

    invoke-virtual {p0, v6}, Lcom/blurb/checkout/SamsungAPI$GetUserInfoResult;->setHttpStatus(I)V

    goto :goto_1
.end method
