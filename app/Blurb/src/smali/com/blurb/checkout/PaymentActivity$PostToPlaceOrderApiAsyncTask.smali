.class Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;
.super Lcom/blurb/checkout/ActivityAsyncTask;
.source "PaymentActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/PaymentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PostToPlaceOrderApiAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/blurb/checkout/ActivityAsyncTask",
        "<",
        "Landroid/app/Activity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;",
        ">;"
    }
.end annotation


# instance fields
.field cardCvv:Ljava/lang/String;

.field cardNumber:Ljava/lang/String;

.field expMonth:Ljava/lang/String;

.field expYear:Ljava/lang/String;

.field final synthetic this$0:Lcom/blurb/checkout/PaymentActivity;

.field wantNewsletter:Z


# direct methods
.method private constructor <init>(Lcom/blurb/checkout/PaymentActivity;Landroid/app/Activity;)V
    .locals 0
    .param p2, "a"    # Landroid/app/Activity;

    .prologue
    .line 1128
    iput-object p1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    .line 1129
    invoke-direct {p0, p2}, Lcom/blurb/checkout/ActivityAsyncTask;-><init>(Landroid/app/Activity;)V

    .line 1130
    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/PaymentActivity;Landroid/app/Activity;Lcom/blurb/checkout/PaymentActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/PaymentActivity;
    .param p2, "x1"    # Landroid/app/Activity;
    .param p3, "x2"    # Lcom/blurb/checkout/PaymentActivity$1;

    .prologue
    .line 1121
    invoke-direct {p0, p1, p2}, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;-><init>(Lcom/blurb/checkout/PaymentActivity;Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;
    .locals 25
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 1149
    new-instance v21, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;

    const/4 v1, 0x0

    move-object/from16 v0, v21

    invoke-direct {v0, v1}, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;-><init>(Lcom/blurb/checkout/PaymentActivity$1;)V

    .line 1151
    .local v21, "result":Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v23

    .line 1155
    .local v23, "startMS":J
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->sessionId:Ljava/lang/String;
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$1400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static {v2}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v2

    iget-object v2, v2, Lcom/blurb/checkout/BlurbAPI$Checkout;->checkoutId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->coupon_code:Ljava/lang/String;
    invoke-static {v3}, Lcom/blurb/checkout/PaymentActivity;->access$1600(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billing_first_name:Ljava/lang/String;
    invoke-static {v4}, Lcom/blurb/checkout/PaymentActivity;->access$1700(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billing_last_name:Ljava/lang/String;
    invoke-static {v5}, Lcom/blurb/checkout/PaymentActivity;->access$1800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billing_street_address:Ljava/lang/String;
    invoke-static {v6}, Lcom/blurb/checkout/PaymentActivity;->access$1900(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billing_street_address2:Ljava/lang/String;
    invoke-static {v7}, Lcom/blurb/checkout/PaymentActivity;->access$2000(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billing_city:Ljava/lang/String;
    invoke-static {v8}, Lcom/blurb/checkout/PaymentActivity;->access$2100(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billing_zip:Ljava/lang/String;
    invoke-static {v9}, Lcom/blurb/checkout/PaymentActivity;->access$2200(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billing_state_id:Ljava/lang/String;
    invoke-static {v10}, Lcom/blurb/checkout/PaymentActivity;->access$2300(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;
    invoke-static {v11}, Lcom/blurb/checkout/PaymentActivity;->access$800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billing_first_name:Ljava/lang/String;
    invoke-static {v12}, Lcom/blurb/checkout/PaymentActivity;->access$1700(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billing_last_name:Ljava/lang/String;
    invoke-static {v13}, Lcom/blurb/checkout/PaymentActivity;->access$1800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->cardNumber:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->cardCvv:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->expMonth:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->expYear:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->wantNewsletter:Z

    move/from16 v18, v0

    invoke-static/range {v1 .. v18}, Lcom/blurb/checkout/BlurbAPI;->postPlaceOrder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    move-result-object v1

    move-object/from16 v0, v21

    iput-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->placeOrderResult:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    .line 1176
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sub-long v19, v1, v23

    .line 1178
    .local v19, "elapsedMS":J
    const/16 v22, 0x0

    .line 1180
    .local v22, "retry":Z
    const-wide/32 v1, 0x1d4c0

    cmp-long v1, v19, v1

    if-gez v1, :cond_2

    .line 1181
    move-object/from16 v0, v21

    iget-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->placeOrderResult:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->getHttpStatus()I

    move-result v1

    const/4 v2, -0x3

    if-eq v1, v2, :cond_1

    move-object/from16 v0, v21

    iget-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->placeOrderResult:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->getHttpStatus()I

    move-result v1

    const/4 v2, -0x2

    if-ne v1, v2, :cond_9

    :cond_1
    const/16 v22, 0x1

    .line 1184
    :cond_2
    :goto_0
    if-eqz v22, :cond_3

    .line 1186
    const-wide/16 v1, 0x7d0

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1190
    :cond_3
    :goto_1
    if-nez v22, :cond_0

    .line 1192
    move-object/from16 v0, v21

    iget-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->placeOrderResult:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->getHttpStatus()I

    move-result v1

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->setHttpStatus(I)V

    .line 1194
    invoke-virtual/range {v21 .. v21}, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->getHttpStatus()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_8

    .line 1195
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v23

    .line 1198
    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->sessionId:Ljava/lang/String;
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$1400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->bookId:Ljava/lang/String;
    invoke-static {v2}, Lcom/blurb/checkout/PaymentActivity;->access$2400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/blurb/checkout/BlurbAPI;->postCreateProject(Ljava/lang/String;Ljava/lang/String;)Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    move-result-object v1

    move-object/from16 v0, v21

    iput-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->createProjectResult:Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    .line 1202
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sub-long v19, v1, v23

    .line 1204
    const/16 v22, 0x0

    .line 1206
    const-wide/32 v1, 0x1d4c0

    cmp-long v1, v19, v1

    if-gez v1, :cond_6

    .line 1207
    move-object/from16 v0, v21

    iget-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->createProjectResult:Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->getHttpStatus()I

    move-result v1

    const/4 v2, -0x3

    if-eq v1, v2, :cond_5

    move-object/from16 v0, v21

    iget-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->createProjectResult:Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->getHttpStatus()I

    move-result v1

    const/4 v2, -0x2

    if-ne v1, v2, :cond_a

    :cond_5
    const/16 v22, 0x1

    .line 1210
    :cond_6
    :goto_2
    if-eqz v22, :cond_7

    .line 1212
    const-wide/16 v1, 0x7d0

    :try_start_1
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1216
    :cond_7
    :goto_3
    if-nez v22, :cond_4

    .line 1218
    move-object/from16 v0, v21

    iget-object v1, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->createProjectResult:Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    invoke-virtual {v1}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->getHttpStatus()I

    move-result v1

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->setHttpStatus(I)V

    .line 1221
    :cond_8
    return-object v21

    .line 1181
    :cond_9
    const/16 v22, 0x0

    goto :goto_0

    .line 1207
    :cond_a
    const/16 v22, 0x0

    goto :goto_2

    .line 1187
    :catch_0
    move-exception v1

    goto :goto_1

    .line 1213
    :catch_1
    move-exception v1

    goto :goto_3
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1121
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->doInBackground([Ljava/lang/Void;)Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;)V
    .locals 20
    .param p1, "result"    # Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;

    .prologue
    .line 1228
    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->hideProgress()V

    .line 1230
    if-eqz p1, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->isCancelled()Z

    move-result v17

    if-nez v17, :cond_3

    .line 1231
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->placeOrderResult:Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;

    move-object/from16 v16, v0

    .line 1232
    .local v16, "placeOrderResult":Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->createProjectResult:Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    .line 1234
    .local v5, "createProjectResult":Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;
    if-eqz v16, :cond_8

    .line 1235
    invoke-virtual/range {v16 .. v16}, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->getErrors()Ljava/util/List;

    move-result-object v8

    .line 1237
    .local v8, "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    if-eqz v8, :cond_5

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v17

    if-lez v17, :cond_5

    .line 1238
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;

    .line 1240
    .local v7, "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    iget-object v0, v7, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->message:Ljava/lang/String;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    iget-object v0, v7, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->message:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_1

    .line 1242
    :cond_0
    iget-object v0, v7, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->parameter:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v0, v7, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->message:Ljava/lang/String;

    .line 1244
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->parameterToFieldMap:Ljava/util/HashMap;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$2500(Lcom/blurb/checkout/PaymentActivity;)Ljava/util/HashMap;

    move-result-object v17

    invoke-virtual {v7}, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->getParameter()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 1245
    .local v9, "field":Ljava/lang/Integer;
    invoke-virtual {v7}, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->getMessage()Ljava/lang/String;

    move-result-object v14

    .line 1247
    .local v14, "message":Ljava/lang/String;
    if-nez v9, :cond_2

    .line 1248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->errorToFieldMap:Ljava/util/HashMap;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$2600(Lcom/blurb/checkout/PaymentActivity;)Ljava/util/HashMap;

    move-result-object v17

    invoke-virtual {v7}, Lcom/blurb/checkout/BlurbResponseParser$BlurbError;->getMessage()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "field":Ljava/lang/Integer;
    check-cast v9, Ljava/lang/Integer;

    .line 1250
    .restart local v9    # "field":Ljava/lang/Integer;
    :cond_2
    if-eqz v9, :cond_4

    .line 1251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v18

    move-object/from16 v0, v17

    move/from16 v1, v18

    # invokes: Lcom/blurb/checkout/PaymentActivity;->handleValidationError(ILjava/lang/String;)V
    invoke-static {v0, v1, v14}, Lcom/blurb/checkout/PaymentActivity;->access$2700(Lcom/blurb/checkout/PaymentActivity;ILjava/lang/String;)V

    .line 1351
    .end local v5    # "createProjectResult":Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;
    .end local v7    # "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    .end local v8    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    .end local v9    # "field":Ljava/lang/Integer;
    .end local v14    # "message":Ljava/lang/String;
    .end local v16    # "placeOrderResult":Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
    :cond_3
    :goto_0
    return-void

    .line 1253
    .restart local v5    # "createProjectResult":Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;
    .restart local v7    # "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    .restart local v8    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    .restart local v9    # "field":Ljava/lang/Integer;
    .restart local v14    # "message":Ljava/lang/String;
    .restart local v16    # "placeOrderResult":Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->billingCountryCode:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/blurb/checkout/PaymentActivity;->access$800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v14, v1}, Lcom/blurb/checkout/BlurbAPI;->codeToMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 1254
    .local v15, "msg":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v15, v1}, Lcom/blurb/checkout/PaymentActivity;->showError(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1256
    .end local v7    # "error":Lcom/blurb/checkout/BlurbResponseParser$BlurbError;
    .end local v9    # "field":Ljava/lang/Integer;
    .end local v14    # "message":Ljava/lang/String;
    .end local v15    # "msg":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->getHttpStatus()I

    move-result v17

    const/16 v18, 0xc8

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_6

    .line 1257
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->getMessageForStatus(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    .line 1258
    .restart local v14    # "message":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v14, v1}, Lcom/blurb/checkout/PaymentActivity;->showError(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1260
    .end local v14    # "message":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->episodeId:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$2800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/ImagesReceiver;->getRenderedImages(Ljava/lang/String;)Lcom/blurb/checkout/ImagesReceiver$RenderedImages;

    move-result-object v10

    .line 1262
    .local v10, "imagesFromEpisodeApp":Lcom/blurb/checkout/ImagesReceiver$RenderedImages;
    if-nez v10, :cond_7

    .line 1263
    const-string v17, "BlurbShadowApp"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Internal error: rendered images not found for episode="

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v19, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->episodeId:Ljava/lang/String;
    invoke-static/range {v19 .. v19}, Lcom/blurb/checkout/PaymentActivity;->access$2800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    const/16 v18, 0x2

    invoke-virtual/range {v17 .. v18}, Lcom/blurb/checkout/PaymentActivity;->showDialog(I)V

    goto/16 :goto_0

    .line 1269
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    invoke-virtual/range {v17 .. v18}, Lcom/blurb/checkout/PaymentActivity;->setResult(I)V

    .line 1270
    invoke-static {}, Lcom/blurb/checkout/ImagesReceiver;->forgetRenderedImages()V

    .line 1271
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->finish()V

    .line 1273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$500(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1274
    .local v3, "cardNum":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$400(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1275
    .local v6, "cvv":Ljava/lang/String;
    invoke-static {v3, v6}, Lcom/blurb/checkout/CreditCard;->checkCardType(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 1277
    .local v4, "cardResult":I
    new-instance v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;

    invoke-direct {v11}, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;-><init>()V

    .line 1279
    .local v11, "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->episodeId:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$2800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->episodeId:Ljava/lang/String;

    .line 1280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->bookId:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$2400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookId:Ljava/lang/String;

    .line 1282
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$PlaceOrderResult;->orderId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderId:Ljava/lang/String;

    .line 1283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->shippingFirstName:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$2900(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->firstName:Ljava/lang/String;

    .line 1284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->shippingLastName:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3000(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->lastName:Ljava/lang/String;

    .line 1285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3100(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->address1:Ljava/lang/String;

    .line 1286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->shippingStreetAddress2:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3200(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->address2:Ljava/lang/String;

    .line 1287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->shippingCity:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3300(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->city:Ljava/lang/String;

    .line 1288
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->shippingState:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->state:Ljava/lang/String;

    .line 1289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->shippingZip:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3500(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->postalCode:Ljava/lang/String;

    .line 1290
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->shippingCountryCode:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3600(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->countryCode:Ljava/lang/String;

    .line 1291
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->shippingPhone:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3700(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->phone:Ljava/lang/String;

    .line 1292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->email:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->email:Ljava/lang/String;

    .line 1293
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->last_four_cc:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$3900(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->lastFourCC:Ljava/lang/String;

    .line 1294
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->title:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$4000(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->title:Ljava/lang/String;

    .line 1295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->coverData:[B
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$4100(Lcom/blurb/checkout/PaymentActivity;)[B

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->cover:[B

    .line 1296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->bookType:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$4200(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->bookType:Ljava/lang/String;

    .line 1297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->coverType:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$4300(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->coverType:Ljava/lang/String;

    .line 1298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->num_Pages:I
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$4400(Lcom/blurb/checkout/PaymentActivity;)I

    move-result v17

    move/from16 v0, v17

    iput v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->numPages:I

    .line 1299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->quantity:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$4500(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->quantity:Ljava/lang/String;

    .line 1303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->orderCents:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v19, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v19 .. v19}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v17 .. v19}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1305
    .local v13, "localizedPrice":Ljava/lang/String;
    iput-object v13, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->orderTotal:Ljava/lang/String;

    .line 1306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->shippingCents:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v19, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v19 .. v19}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v17 .. v19}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1307
    iput-object v13, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->shippingTotal:Ljava/lang/String;

    .line 1308
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->discountCents:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v19, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v19 .. v19}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v17 .. v19}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedDiscountPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1309
    iput-object v13, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->discountTotal:Ljava/lang/String;

    .line 1310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->discountCents:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->discountCents:Ljava/lang/String;

    .line 1311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->taxCents:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v19, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v19 .. v19}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v17 .. v19}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1312
    iput-object v13, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->taxTotal:Ljava/lang/String;

    .line 1313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->taxCents:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->taxCents:Ljava/lang/String;

    .line 1314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->productCents:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v19, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->checkout:Lcom/blurb/checkout/BlurbAPI$Checkout;
    invoke-static/range {v19 .. v19}, Lcom/blurb/checkout/PaymentActivity;->access$1500(Lcom/blurb/checkout/PaymentActivity;)Lcom/blurb/checkout/BlurbAPI$Checkout;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/blurb/checkout/BlurbAPI$Checkout;->currencyId:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-static/range {v17 .. v19}, Lcom/blurb/checkout/PaymentActivity;->getLocalizedPriceFromCents(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1315
    iput-object v13, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->subTotal:Ljava/lang/String;

    .line 1317
    iput v4, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->creditCardType:I

    .line 1318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->coupon_code:Ljava/lang/String;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$1600(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v11, Lcom/blurb/checkout/service/BlurbManager$OrderInfo;->couponCode:Ljava/lang/String;

    .line 1320
    new-instance v2, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    const-class v18, Lcom/blurb/checkout/service/BookService;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1322
    .local v2, "bookIntent":Landroid/content/Intent;
    const-string v17, "com.blurb.checkout.service.PUT_BOOK"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1323
    const-string v17, "sessionId"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->sessionId:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/blurb/checkout/PaymentActivity;->access$1400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1324
    const-string v17, "bookId"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->bookId:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/blurb/checkout/PaymentActivity;->access$2400(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1325
    const-string v17, "projectId"

    iget-object v0, v5, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->projectId:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1326
    const-string v17, "episodeId"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->episodeId:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/blurb/checkout/PaymentActivity;->access$2800(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1327
    const-string v17, "coverType"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->coverType:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/blurb/checkout/PaymentActivity;->access$4300(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1328
    const-string v17, "frontCover"

    iget-object v0, v10, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->frontCover:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1329
    const-string v17, "backCover"

    iget-object v0, v10, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->backCover:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1330
    const-string v17, "spine"

    iget-object v0, v10, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->spine:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1331
    const-string v17, "photos"

    iget-object v0, v10, Lcom/blurb/checkout/ImagesReceiver$RenderedImages;->imageList:Ljava/util/ArrayList;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1332
    const-string v17, "title"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v18, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->title:Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/blurb/checkout/PaymentActivity;->access$4000(Lcom/blurb/checkout/PaymentActivity;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1333
    const-string v17, "orderInfo"

    move-object/from16 v0, v17

    invoke-virtual {v2, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1335
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Lcom/blurb/checkout/PaymentActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1337
    new-instance v12, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    const-class v18, Lcom/blurb/checkout/OrderPhotoBookActivity;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v12, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1339
    .local v12, "intent":Landroid/content/Intent;
    const-string v17, "orderInfo"

    move-object/from16 v0, v17

    invoke-virtual {v12, v0, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lcom/blurb/checkout/PaymentActivity;->startActivity(Landroid/content/Intent;)V

    .line 1344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    # getter for: Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;
    invoke-static/range {v17 .. v17}, Lcom/blurb/checkout/PaymentActivity;->access$500(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;

    move-result-object v17

    const-string v18, ""

    invoke-virtual/range {v17 .. v18}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1347
    .end local v2    # "bookIntent":Landroid/content/Intent;
    .end local v3    # "cardNum":Ljava/lang/String;
    .end local v4    # "cardResult":I
    .end local v6    # "cvv":Ljava/lang/String;
    .end local v8    # "errors":Ljava/util/List;, "Ljava/util/List<Lcom/blurb/checkout/BlurbResponseParser$BlurbError;>;"
    .end local v10    # "imagesFromEpisodeApp":Lcom/blurb/checkout/ImagesReceiver$RenderedImages;
    .end local v11    # "info":Lcom/blurb/checkout/service/BlurbManager$OrderInfo;
    .end local v12    # "intent":Landroid/content/Intent;
    .end local v13    # "localizedPrice":Ljava/lang/String;
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;->getMessageForStatus(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v14

    .line 1348
    .restart local v14    # "message":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v0, v14, v1}, Lcom/blurb/checkout/PaymentActivity;->showError(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1121
    check-cast p1, Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->onPostExecute(Lcom/blurb/checkout/PaymentActivity$PlaceOrderAndCreateProjectResult;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 1134
    invoke-super {p0}, Lcom/blurb/checkout/ActivityAsyncTask;->onPreExecute()V

    .line 1136
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->cardNumEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$500(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->cardNumber:Ljava/lang/String;

    .line 1137
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->cardCvvEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$400(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->cardCvv:Ljava/lang/String;

    .line 1138
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->cardExpirationMonthEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/blurb/checkout/PaymentActivity;->access$1200(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->expMonth:Ljava/lang/String;

    .line 1139
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "20"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    # getter for: Lcom/blurb/checkout/PaymentActivity;->cardExpirationYearEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/blurb/checkout/PaymentActivity;->access$300(Lcom/blurb/checkout/PaymentActivity;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->expYear:Ljava/lang/String;

    .line 1141
    iget-object v1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->this$0:Lcom/blurb/checkout/PaymentActivity;

    const v2, 0x7f06003c

    invoke-virtual {v1, v2}, Lcom/blurb/checkout/PaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1142
    .local v0, "cb":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iput-boolean v1, p0, Lcom/blurb/checkout/PaymentActivity$PostToPlaceOrderApiAsyncTask;->wantNewsletter:Z

    .line 1143
    return-void
.end method
