.class Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;
.super Lcom/blurb/checkout/BlurbResponseParser;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GenerateBBFResponseParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;


# direct methods
.method private constructor <init>(Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;)V
    .locals 0

    .prologue
    .line 559
    iput-object p1, p0, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;

    invoke-direct {p0}, Lcom/blurb/checkout/BlurbResponseParser;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;Lcom/blurb/checkout/BlurbAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;
    .param p2, "x1"    # Lcom/blurb/checkout/BlurbAPI$1;

    .prologue
    .line 559
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;)V

    return-void
.end method

.method static synthetic access$400(Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;Ljava/io/Reader;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;
    .param p1, "x1"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 559
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;->parseXml(Ljava/io/Reader;)V

    return-void
.end method

.method private parseXml(Ljava/io/Reader;)V
    .locals 5
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 561
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 562
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 563
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 565
    .local v0, "event":I
    :goto_0
    const/4 v3, 0x1

    if-eq v0, v3, :cond_3

    .line 568
    packed-switch v0, :pswitch_data_0

    .line 585
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 586
    goto :goto_0

    .line 572
    :pswitch_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 574
    .local v1, "name":Ljava/lang/String;
    const-string v3, "errors"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 575
    invoke-virtual {p0, v2}, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;->parseErrors(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_1

    .line 576
    :cond_1
    const-string v3, "id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 577
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->bookId:Ljava/lang/String;

    goto :goto_1

    .line 578
    :cond_2
    const-string v3, "state"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 579
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult$GenerateBBFResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;

    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->nextText()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$GenerateBBFResult;->state:Ljava/lang/String;

    goto :goto_1

    .line 587
    .end local v1    # "name":Ljava/lang/String;
    :cond_3
    return-void

    .line 568
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
