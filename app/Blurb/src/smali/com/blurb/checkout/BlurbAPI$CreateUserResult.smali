.class public Lcom/blurb/checkout/BlurbAPI$CreateUserResult;
.super Lcom/blurb/checkout/BlurbAPI$BlurbResult;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CreateUserResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;
    }
.end annotation


# instance fields
.field public billingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

.field public countryCode:Ljava/lang/String;

.field public email:Ljava/lang/String;

.field public firstName:Ljava/lang/String;

.field public lastName:Ljava/lang/String;

.field public sessionId:Ljava/lang/String;

.field public shippingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

.field public userName:Ljava/lang/String;

.field public webSessionId:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 907
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 908
    return-void
.end method

.method constructor <init>(I)V
    .locals 0
    .param p1, "httpStatus"    # I

    .prologue
    .line 910
    invoke-direct {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;-><init>()V

    .line 911
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->setHttpStatus(I)V

    .line 912
    return-void
.end method


# virtual methods
.method public bridge synthetic getErrors()Ljava/util/List;
    .locals 1

    .prologue
    .line 791
    invoke-super {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;->getErrors()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getHttpStatus()I
    .locals 1

    .prologue
    .line 791
    invoke-super {p0}, Lcom/blurb/checkout/BlurbAPI$BlurbResult;->getHttpStatus()I

    move-result v0

    return v0
.end method

.method setHttpResponse(Lorg/apache/http/HttpResponse;)V
    .locals 7
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 915
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->setHttpStatus(I)V

    .line 920
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    .line 921
    .local v1, "entity":Lorg/apache/http/HttpEntity;
    if-nez v1, :cond_0

    .line 922
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->setHttpStatus(I)V

    .line 961
    :goto_0
    return-void

    .line 926
    :cond_0
    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0xc8

    if-eq v5, v6, :cond_1

    invoke-virtual {p0}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->getHttpStatus()I

    move-result v5

    const/16 v6, 0x190

    if-ne v5, v6, :cond_2

    .line 928
    :cond_1
    :try_start_0
    new-instance v2, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;

    const/4 v5, 0x0

    invoke-direct {v2, p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$CreateUserResult;Lcom/blurb/checkout/BlurbAPI$1;)V

    .line 929
    .local v2, "parser":Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;
    const-string v5, "UTF-8"

    invoke-static {v1, v5}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 930
    .local v4, "responseString":Ljava/lang/String;
    new-instance v3, Ljava/io/StringReader;

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 936
    .local v3, "reader":Ljava/io/StringReader;
    # invokes: Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->parseXml(Ljava/io/Reader;)V
    invoke-static {v2, v3}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->access$700(Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;Ljava/io/Reader;)V

    .line 937
    invoke-virtual {v2}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->getErrors()Ljava/util/List;

    move-result-object v5

    iput-object v5, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->errors:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_2

    .line 957
    .end local v2    # "parser":Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;
    .end local v3    # "reader":Ljava/io/StringReader;
    .end local v4    # "responseString":Ljava/lang/String;
    :cond_2
    :goto_1
    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->consumeContent()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 958
    :catch_0
    move-exception v0

    .line 959
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 938
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 939
    .restart local v0    # "e":Ljava/io/IOException;
    const/4 v5, -0x2

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->setHttpStatus(I)V

    .line 940
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 941
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 942
    .local v0, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v0}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 943
    const/4 v5, -0x4

    invoke-virtual {p0, v5}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->setHttpStatus(I)V

    goto :goto_1
.end method
