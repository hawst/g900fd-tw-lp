.class Lcom/blurb/checkout/BlurbAPI$CreateProjectResponseHandler;
.super Ljava/lang/Object;
.source "BlurbAPI.java"

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CreateProjectResponseHandler"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1818
    return-void
.end method


# virtual methods
.method public handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;
    .locals 1
    .param p1, "response"    # Lorg/apache/http/HttpResponse;

    .prologue
    .line 1821
    new-instance v0, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;-><init>()V

    .line 1823
    .local v0, "result":Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;
    invoke-virtual {v0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;->setHttpResponse(Lorg/apache/http/HttpResponse;)V

    .line 1825
    return-object v0
.end method

.method public bridge synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpResponse;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1815
    invoke-virtual {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateProjectResponseHandler;->handleResponse(Lorg/apache/http/HttpResponse;)Lcom/blurb/checkout/BlurbAPI$CreateProjectResult;

    move-result-object v0

    return-object v0
.end method
