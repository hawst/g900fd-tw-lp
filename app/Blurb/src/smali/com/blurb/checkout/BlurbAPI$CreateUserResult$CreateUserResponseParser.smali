.class Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;
.super Lcom/blurb/checkout/BlurbResponseParser;
.source "BlurbAPI.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/blurb/checkout/BlurbAPI$CreateUserResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateUserResponseParser"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;


# direct methods
.method private constructor <init>(Lcom/blurb/checkout/BlurbAPI$CreateUserResult;)V
    .locals 0

    .prologue
    .line 802
    iput-object p1, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-direct {p0}, Lcom/blurb/checkout/BlurbResponseParser;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/blurb/checkout/BlurbAPI$CreateUserResult;Lcom/blurb/checkout/BlurbAPI$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/blurb/checkout/BlurbAPI$CreateUserResult;
    .param p2, "x1"    # Lcom/blurb/checkout/BlurbAPI$1;

    .prologue
    .line 802
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;-><init>(Lcom/blurb/checkout/BlurbAPI$CreateUserResult;)V

    return-void
.end method

.method static synthetic access$700(Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;Ljava/io/Reader;)V
    .locals 0
    .param p0, "x0"    # Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;
    .param p1, "x1"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 802
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->parseXml(Ljava/io/Reader;)V

    return-void
.end method

.method private parseXml(Ljava/io/Reader;)V
    .locals 5
    .param p1, "reader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 876
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v2

    .line 877
    .local v2, "parser":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v2, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 878
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v0

    .line 880
    .local v0, "event":I
    :goto_0
    const/4 v3, 0x1

    if-eq v0, v3, :cond_4

    .line 883
    packed-switch v0, :pswitch_data_0

    .line 902
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v0

    .line 903
    goto :goto_0

    .line 887
    :pswitch_1
    invoke-interface {v2}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 889
    .local v1, "name":Ljava/lang/String;
    const-string v3, "errors"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 890
    invoke-virtual {p0, v2}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->parseErrors(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_1

    .line 891
    :cond_1
    const-string v3, "id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 892
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-static {v2, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->sessionId:Ljava/lang/String;

    goto :goto_1

    .line 893
    :cond_2
    const-string v3, "web_session_id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 894
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-static {v2, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->webSessionId:Ljava/lang/String;

    goto :goto_1

    .line 895
    :cond_3
    const-string v3, "user"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 896
    invoke-direct {p0, v2}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->readUser(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_1

    .line 904
    .end local v1    # "name":Ljava/lang/String;
    :cond_4
    return-void

    .line 883
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private readAddress(Lorg/xmlpull/v1/XmlPullParser;)Lcom/blurb/checkout/BlurbAPI$Address;
    .locals 5
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    .line 804
    sget-object v2, Lcom/blurb/checkout/BlurbAPI;->ns:Ljava/lang/String;

    const-string v3, "address"

    invoke-interface {p1, v4, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 806
    new-instance v0, Lcom/blurb/checkout/BlurbAPI$Address;

    invoke-direct {v0}, Lcom/blurb/checkout/BlurbAPI$Address;-><init>()V

    .line 808
    .local v0, "address":Lcom/blurb/checkout/BlurbAPI$Address;
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_c

    .line 809
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 812
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    .line 813
    .local v1, "name":Ljava/lang/String;
    const-string v2, "id"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 814
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressId:Ljava/lang/String;

    goto :goto_0

    .line 815
    :cond_1
    const-string v2, "street1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 816
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet1:Ljava/lang/String;

    goto :goto_0

    .line 817
    :cond_2
    const-string v2, "street2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 818
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStreet2:Ljava/lang/String;

    goto :goto_0

    .line 819
    :cond_3
    const-string v2, "city"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 820
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCity:Ljava/lang/String;

    goto :goto_0

    .line 821
    :cond_4
    const-string v2, "zip"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 822
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressZip:Ljava/lang/String;

    goto :goto_0

    .line 823
    :cond_5
    const-string v2, "email"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 824
    iget-object v2, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->email:Ljava/lang/String;

    goto :goto_0

    .line 825
    :cond_6
    const-string v2, "country_id"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 826
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressCountryCode:Ljava/lang/String;

    goto :goto_0

    .line 827
    :cond_7
    const-string v2, "state_id"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 828
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateId:Ljava/lang/String;

    goto/16 :goto_0

    .line 829
    :cond_8
    const-string v2, "state_name"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 830
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressStateName:Ljava/lang/String;

    goto/16 :goto_0

    .line 831
    :cond_9
    const-string v2, "po_box_or_apo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 832
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->addressPoBoxOrApo:Ljava/lang/String;

    goto/16 :goto_0

    .line 833
    :cond_a
    const-string v2, "phone"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 834
    invoke-static {p1, v1}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/blurb/checkout/BlurbAPI$Address;->phoneNumber:Ljava/lang/String;

    goto/16 :goto_0

    .line 836
    :cond_b
    invoke-static {p1}, Lcom/blurb/checkout/WebService;->skipTag(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_0

    .line 840
    .end local v1    # "name":Ljava/lang/String;
    :cond_c
    return-object v0
.end method

.method private readUser(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 6
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    .line 844
    sget-object v3, Lcom/blurb/checkout/BlurbAPI;->ns:Ljava/lang/String;

    const-string v4, "user"

    invoke-interface {p1, v5, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 846
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    const/4 v4, 0x3

    if-eq v3, v4, :cond_8

    .line 847
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 850
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 851
    .local v2, "name":Ljava/lang/String;
    const-string v3, "username"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 852
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-static {p1, v2}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->userName:Ljava/lang/String;

    goto :goto_0

    .line 853
    :cond_1
    const-string v3, "firstname"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 854
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-static {p1, v2}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->firstName:Ljava/lang/String;

    goto :goto_0

    .line 855
    :cond_2
    const-string v3, "lastname"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 856
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-static {p1, v2}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->lastName:Ljava/lang/String;

    goto :goto_0

    .line 857
    :cond_3
    const-string v3, "email"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 858
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-static {p1, v2}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->email:Ljava/lang/String;

    goto :goto_0

    .line 859
    :cond_4
    const-string v3, "country"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 860
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    invoke-static {p1, v2}, Lcom/blurb/checkout/WebService;->readTextTag(Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->countryCode:Ljava/lang/String;

    goto :goto_0

    .line 861
    :cond_5
    const-string v3, "address"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 862
    const/4 v3, 0x0

    const-string v4, "type"

    invoke-interface {p1, v3, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 864
    .local v1, "addressType":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->readAddress(Lorg/xmlpull/v1/XmlPullParser;)Lcom/blurb/checkout/BlurbAPI$Address;

    move-result-object v0

    .line 865
    .local v0, "a":Lcom/blurb/checkout/BlurbAPI$Address;
    const-string v3, "shipping"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 866
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    iput-object v0, v3, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->shippingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

    goto/16 :goto_0

    .line 867
    :cond_6
    const-string v3, "billing"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 868
    iget-object v3, p0, Lcom/blurb/checkout/BlurbAPI$CreateUserResult$CreateUserResponseParser;->this$0:Lcom/blurb/checkout/BlurbAPI$CreateUserResult;

    iput-object v0, v3, Lcom/blurb/checkout/BlurbAPI$CreateUserResult;->billingAddress:Lcom/blurb/checkout/BlurbAPI$Address;

    goto/16 :goto_0

    .line 870
    .end local v0    # "a":Lcom/blurb/checkout/BlurbAPI$Address;
    .end local v1    # "addressType":Ljava/lang/String;
    :cond_7
    invoke-static {p1}, Lcom/blurb/checkout/WebService;->skipTag(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_0

    .line 873
    .end local v2    # "name":Ljava/lang/String;
    :cond_8
    return-void
.end method
