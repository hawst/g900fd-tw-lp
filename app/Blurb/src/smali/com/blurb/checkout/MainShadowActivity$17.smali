.class final Lcom/blurb/checkout/MainShadowActivity$17;
.super Ljava/lang/Object;
.source "MainShadowActivity.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/blurb/checkout/MainShadowActivity;->getSortedCountries(Landroid/content/Context;)Ljava/util/ArrayList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/blurb/checkout/MainShadowActivity$Country;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$col:Ljava/text/Collator;


# direct methods
.method constructor <init>(Ljava/text/Collator;)V
    .locals 0

    .prologue
    .line 1508
    iput-object p1, p0, Lcom/blurb/checkout/MainShadowActivity$17;->val$col:Ljava/text/Collator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/blurb/checkout/MainShadowActivity$Country;Lcom/blurb/checkout/MainShadowActivity$Country;)I
    .locals 3
    .param p1, "lhs"    # Lcom/blurb/checkout/MainShadowActivity$Country;
    .param p2, "rhs"    # Lcom/blurb/checkout/MainShadowActivity$Country;

    .prologue
    .line 1512
    iget-object v0, p0, Lcom/blurb/checkout/MainShadowActivity$17;->val$col:Ljava/text/Collator;

    iget-object v1, p1, Lcom/blurb/checkout/MainShadowActivity$Country;->myName:Ljava/lang/String;

    iget-object v2, p2, Lcom/blurb/checkout/MainShadowActivity$Country;->myName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 1508
    check-cast p1, Lcom/blurb/checkout/MainShadowActivity$Country;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/blurb/checkout/MainShadowActivity$Country;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/blurb/checkout/MainShadowActivity$17;->compare(Lcom/blurb/checkout/MainShadowActivity$Country;Lcom/blurb/checkout/MainShadowActivity$Country;)I

    move-result v0

    return v0
.end method
