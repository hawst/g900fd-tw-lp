.class public Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;
.super Landroid/app/Service;
.source "DigitalClockEasyService.java"


# static fields
.field private static final ACTION_ENTER_LAUNCHER:Ljava/lang/String; = "com.android.launcher.action.ENTER_EASYLAUNCHER"

.field private static final ACTION_HOME_RESUME:Ljava/lang/String; = "sec.android.intent.action.HOME_RESUME"

.field static final START:Ljava/lang/String; = "digitalclockeasy.action.startservice"

.field static final STOP:Ljava/lang/String; = "digitalclockeasy.action.stopservice"

.field private static final TAG:Ljava/lang/String; = "DigitalClockEasyAppWidgetService"


# instance fields
.field private mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 44
    new-instance v0, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService$1;-><init>(Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;->mReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 68
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 73
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 75
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 76
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.TIME_TICK"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 77
    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    const-string v2, "sec.android.intent.action.HOME_RESUME"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 80
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 81
    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 82
    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 83
    const-string v2, "com.android.launcher.action.ENTER_EASYLAUNCHER"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    iget-object v2, p0, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 87
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 88
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->updateClock(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 89
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 106
    iget-object v1, p0, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 108
    invoke-static {}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->getWidgetRunning()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 109
    const-string v1, "DigitalClockEasyAppWidgetService"

    const-string v2, "onDestroy : The widget is alive. service will be reStarted!!!"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 112
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 117
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 114
    :cond_0
    const-string v1, "DigitalClockEasyAppWidgetService"

    const-string v2, "onDestroy : End"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 94
    const-string v1, "DigitalClockEasyAppWidgetService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStartCommand intent = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    if-nez p1, :cond_0

    .line 98
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 101
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    const/4 v1, 0x1

    return v1
.end method
