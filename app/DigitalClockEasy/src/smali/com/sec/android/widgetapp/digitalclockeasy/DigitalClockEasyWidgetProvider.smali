.class public Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "DigitalClockEasyWidgetProvider.java"


# static fields
.field private static final ACTION_EASY_LAUNCHER:Ljava/lang/String; = "com.android.launcher.action.ENTER_EASYLAUNCHER"

.field public static final ACTION_EASY_MODE:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE"

.field public static LauncherMode:Z = false

.field private static final TAG:Ljava/lang/String; = "DigitalClockEasyAppWidget"

.field public static description:Ljava/lang/String;

.field private static mbWidgetRunning:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->mbWidgetRunning:Z

    .line 56
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->description:Ljava/lang/String;

    .line 58
    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->LauncherMode:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method private static drawAmPm(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;Z)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "lancode"    # Ljava/lang/String;
    .param p4, "b24HourFormat"    # Z

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f080008

    .line 309
    if-eqz p4, :cond_0

    .line 310
    invoke-virtual {p1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 311
    const-string v2, ""

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 336
    :goto_0
    return-void

    .line 315
    :cond_0
    invoke-virtual {p1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 317
    const/4 v0, 0x0

    .line 318
    .local v0, "bAm":Z
    invoke-virtual {p2}, Ljava/util/Date;->getHours()I

    move-result v2

    if-ltz v2, :cond_1

    invoke-virtual {p2}, Ljava/util/Date;->getHours()I

    move-result v2

    const/16 v3, 0xc

    if-ge v2, v3, :cond_1

    .line 319
    const/4 v0, 0x1

    .line 321
    :cond_1
    const-string v2, "zh"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "es"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ja"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "ar"

    invoke-virtual {v2, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 324
    :cond_2
    invoke-virtual {p1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 325
    if-eqz v0, :cond_3

    .line 326
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f060000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 328
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 330
    :cond_4
    invoke-virtual {p1, v4, v5}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 332
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "aaaa"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 333
    .local v1, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v1, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v4, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private static drawColon(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;

    .prologue
    .line 282
    const v0, 0x7f080005

    const v1, 0x7f02000a

    invoke-virtual {p1, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 283
    return-void
.end method

.method private static drawHour(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Z)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "b24HourFormat"    # Z

    .prologue
    const v11, 0x7f080004

    const/16 v7, 0xc

    const/16 v10, 0x8

    const/4 v9, 0x0

    const v8, 0x7f080003

    .line 235
    invoke-virtual {p2}, Ljava/util/Date;->getHours()I

    move-result v2

    .line 236
    .local v2, "hour":I
    const/4 v3, 0x0

    .line 237
    .local v3, "hourStr":Ljava/lang/String;
    move v4, v2

    .line 239
    .local v4, "hours":I
    if-nez p1, :cond_0

    .line 279
    :goto_0
    return-void

    .line 241
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget-object v6, v6, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 243
    .local v5, "lancode":Ljava/lang/String;
    if-eqz p3, :cond_1

    .line 244
    div-int/lit8 v0, v2, 0xa

    .line 245
    .local v0, "hh1":I
    rem-int/lit8 v1, v2, 0xa

    .line 246
    .local v1, "hh2":I
    const v6, 0x7f080008

    invoke-virtual {p1, v6, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 266
    :goto_1
    if-nez p3, :cond_6

    if-nez v0, :cond_6

    .line 267
    invoke-virtual {p1, v8, v10}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 268
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->description:Ljava/lang/String;

    .line 275
    :goto_2
    invoke-virtual {p1, v11, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 276
    invoke-static {v1}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->getClockNumberResourceId(I)I

    move-result v6

    invoke-virtual {p1, v11, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    goto :goto_0

    .line 248
    .end local v0    # "hh1":I
    .end local v1    # "hh2":I
    :cond_1
    if-eqz v2, :cond_2

    if-ne v2, v7, :cond_4

    .line 249
    :cond_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 250
    const/4 v0, 0x0

    .line 251
    .restart local v0    # "hh1":I
    const/4 v1, 0x0

    .restart local v1    # "hh2":I
    goto :goto_1

    .line 253
    .end local v0    # "hh1":I
    .end local v1    # "hh2":I
    :cond_3
    const/4 v0, 0x1

    .line 254
    .restart local v0    # "hh1":I
    const/4 v1, 0x2

    .restart local v1    # "hh2":I
    goto :goto_1

    .line 256
    .end local v0    # "hh1":I
    .end local v1    # "hh2":I
    :cond_4
    if-lez v2, :cond_5

    if-ge v2, v7, :cond_5

    .line 257
    div-int/lit8 v0, v2, 0xa

    .line 258
    .restart local v0    # "hh1":I
    rem-int/lit8 v1, v2, 0xa

    .restart local v1    # "hh2":I
    goto :goto_1

    .line 260
    .end local v0    # "hh1":I
    .end local v1    # "hh2":I
    :cond_5
    add-int/lit8 v2, v2, -0xc

    .line 261
    div-int/lit8 v0, v2, 0xa

    .line 262
    .restart local v0    # "hh1":I
    rem-int/lit8 v1, v2, 0xa

    .restart local v1    # "hh2":I
    goto :goto_1

    .line 270
    :cond_6
    invoke-static {v0}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->getClockNumberResourceId(I)I

    move-result v6

    invoke-virtual {p1, v8, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 271
    invoke-virtual {p1, v8, v9}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 273
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->description:Ljava/lang/String;

    goto :goto_2
.end method

.method private static drawMinute(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "lancode"    # Ljava/lang/String;

    .prologue
    const v8, 0x7f080007

    const/4 v7, 0x0

    const v6, 0x7f080006

    .line 286
    invoke-virtual {p2}, Ljava/util/Date;->getMinutes()I

    move-result v0

    .line 287
    .local v0, "minute":I
    const/4 v1, 0x0

    .line 290
    .local v1, "minuteStr":Ljava/lang/String;
    div-int/lit8 v2, v0, 0xa

    .line 291
    .local v2, "mm1":I
    rem-int/lit8 v3, v0, 0xa

    .line 293
    .local v3, "mm2":I
    const-string v4, "ko"

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 294
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->description:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->description:Ljava/lang/String;

    .line 298
    :goto_0
    invoke-virtual {p1, v6, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 299
    invoke-virtual {p1, v8, v7}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 301
    invoke-static {v2}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->getClockNumberResourceId(I)I

    move-result v4

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 302
    invoke-static {v3}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->getClockNumberResourceId(I)I

    move-result v4

    invoke-virtual {p1, v8, v4}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 304
    sget-object v4, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->description:Ljava/lang/String;

    invoke-virtual {p1, v6, v4}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 305
    return-void

    .line 296
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->description:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->description:Ljava/lang/String;

    goto :goto_0
.end method

.method private static getClockNumberResourceId(I)I
    .locals 1
    .param p0, "number"    # I

    .prologue
    .line 362
    const/high16 v0, 0x7f020000

    add-int/2addr v0, p0

    return v0
.end method

.method public static getWidgetRunning()Z
    .locals 1

    .prologue
    .line 144
    sget-boolean v0, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->mbWidgetRunning:Z

    return v0
.end method

.method private isDigitalClockWidgetServiceRunning(Landroid/content/Context;)Z
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 339
    const/16 v0, 0x64

    .line 340
    .local v0, "MAX_NUM":I
    const/4 v1, 0x0

    .line 342
    .local v1, "found":Z
    const/4 v5, 0x0

    .line 343
    .local v5, "service_list":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    const-string v6, "activity"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager;

    .line 346
    .local v4, "manager":Landroid/app/ActivityManager;
    invoke-virtual {v4, v0}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v5

    .line 347
    if-nez v5, :cond_0

    move v2, v1

    .line 358
    .end local v1    # "found":Z
    .local v2, "found":I
    :goto_0
    return v2

    .line 350
    .end local v2    # "found":I
    .restart local v1    # "found":Z
    :cond_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 351
    .local v3, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 352
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v6, v6, Landroid/app/ActivityManager$RunningServiceInfo;->service:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "com.sec.android.widgetapp.digitalclockeasy.DigitalClockEasyService"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 354
    const/4 v1, 0x1

    :cond_2
    move v2, v1

    .line 358
    .restart local v2    # "found":I
    goto :goto_0
.end method

.method public static isEasyLauncher(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 148
    sget-boolean v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->LauncherMode:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "easy_mode_switch"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 149
    :cond_0
    const-string v1, "DigitalClockEasyAppWidget"

    const-string v2, "EasyMode: true"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :goto_0
    return v0

    .line 152
    :cond_1
    const-string v0, "DigitalClockEasyAppWidget"

    const-string v1, "EasyMode: false"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isJapanSmartPhone()Z
    .locals 2

    .prologue
    .line 158
    const-string v0, "JP"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const/4 v0, 0x1

    .line 160
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setDescription(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;ZLjava/lang/String;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "v"    # Landroid/widget/RemoteViews;
    .param p2, "date"    # Ljava/util/Date;
    .param p3, "is24Hour"    # Z
    .param p4, "lancode"    # Ljava/lang/String;

    .prologue
    .line 368
    const/4 v0, 0x0

    .line 370
    .local v0, "readDesciption":Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 372
    .local v1, "textDate":Ljava/lang/String;
    const-string v2, "ko"

    invoke-virtual {v2, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 373
    if-eqz p3, :cond_0

    .line 374
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "kk:mm"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 383
    :goto_0
    const/high16 v2, 0x7f080000

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 384
    return-void

    .line 376
    :cond_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "aaa KK:mm"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 378
    :cond_1
    if-eqz p3, :cond_2

    .line 379
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "kk mm"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 381
    :cond_2
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "KK mm aaa"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static updateClock(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aw"    # Landroid/appwidget/AppWidgetManager;

    .prologue
    .line 174
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    .local v1, "provider":Landroid/content/ComponentName;
    invoke-virtual {p1, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v2

    .line 179
    .local v2, "widgetIds":[I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 180
    aget v3, v2, v0

    invoke-static {p0, p1, v3}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->updateViews(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_0
    return-void
.end method

.method private static updateViews(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;I)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "aw"    # Landroid/appwidget/AppWidgetManager;
    .param p2, "widgetIds"    # I

    .prologue
    const v12, 0x7f080009

    const v10, 0x7f080002

    const/4 v11, 0x0

    .line 185
    const-string v8, "window"

    invoke-virtual {p0, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    .line 186
    .local v7, "wm":Landroid/view/WindowManager;
    const/4 v6, 0x0

    .line 188
    .local v6, "views":Landroid/widget/RemoteViews;
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v1, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 189
    .local v1, "date":Ljava/util/Date;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget-object v5, v8, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 190
    .local v5, "locale":Ljava/util/Locale;
    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    .line 191
    .local v4, "lancode":Ljava/lang/String;
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    .line 193
    .local v0, "b24HourFormat":Z
    new-instance v6, Landroid/widget/RemoteViews;

    .end local v6    # "views":Landroid/widget/RemoteViews;
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v8

    const/high16 v9, 0x7f030000

    invoke-direct {v6, v8, v9}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 196
    .restart local v6    # "views":Landroid/widget/RemoteViews;
    invoke-static {p0, v6, v1, v0}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->drawHour(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Z)V

    .line 197
    invoke-static {p0, v6}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->drawColon(Landroid/content/Context;Landroid/widget/RemoteViews;)V

    .line 198
    invoke-static {p0, v6, v1, v4}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->drawMinute(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;)V

    .line 199
    invoke-static {p0, v6, v1, v4, v0}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->drawAmPm(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/util/Date;Ljava/lang/String;Z)V

    .line 201
    invoke-virtual {v6, v10, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 202
    const-string v8, ","

    invoke-virtual {v6, v10, v8}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 207
    invoke-virtual {v6, v12, v11}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 208
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f060004

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v12, v8}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 210
    new-instance v3, Landroid/content/Intent;

    const-string v8, "android.intent.action.MAIN"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 211
    .local v3, "i":Landroid/content/Intent;
    new-instance v8, Landroid/content/ComponentName;

    const-string v9, "com.sec.android.app.clockpackage"

    const-string v10, "com.sec.android.app.clockpackage.ClockPackage"

    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 213
    const-string v8, "android.intent.category.LAUNCHER"

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    const/high16 v8, 0x10200000

    invoke-virtual {v3, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 216
    new-instance v2, Landroid/os/DVFSHelper;

    const/16 v8, 0xc

    invoke-direct {v2, p0, v8}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;I)V

    .line 217
    .local v2, "dvfsHelper":Landroid/os/DVFSHelper;
    invoke-virtual {v3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, p0, v8}, Landroid/os/DVFSHelper;->onAppLaunchEvent(Landroid/content/Context;Ljava/lang/String;)V

    .line 219
    const v8, 0x7f080001

    invoke-static {p0, v11, v3, v11}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 222
    if-eqz p1, :cond_0

    .line 223
    invoke-virtual {p1, p2, v6}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 226
    :cond_0
    return-void
.end method

.method private static updateWidget(Landroid/appwidget/AppWidgetManager;[ILandroid/widget/RemoteViews;)V
    .locals 2
    .param p0, "aw"    # Landroid/appwidget/AppWidgetManager;
    .param p1, "widgetIds"    # [I
    .param p2, "views"    # Landroid/widget/RemoteViews;

    .prologue
    .line 164
    if-eqz p0, :cond_0

    .line 165
    if-eqz p1, :cond_0

    .line 166
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 167
    aget v1, p1, v0

    invoke-virtual {p0, v1, p2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    .end local v0    # "i":I
    :cond_0
    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 89
    const-string v0, "DigitalClockEasyAppWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDeleted : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->mbWidgetRunning:Z

    .line 91
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 96
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->mbWidgetRunning:Z

    .line 98
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 100
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 63
    invoke-static {p1}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->isEasyLauncher(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->isJapanSmartPhone()Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    :goto_0
    return-void

    .line 66
    :cond_0
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->mbWidgetRunning:Z

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x0

    .line 104
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 108
    :cond_1
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v0

    .line 110
    .local v0, "awm":Landroid/appwidget/AppWidgetManager;
    new-instance v5, Landroid/content/ComponentName;

    const-class v6, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;

    invoke-direct {v5, p1, v6}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v5}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v4

    .line 113
    .local v4, "widgetIDs":[I
    const-string v5, "DigitalClockEasyAppWidget"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "intent : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    const-string v6, "com.android.launcher.action.ENTER_EASYLAUNCHER"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 116
    const-string v5, "easyMode"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    sput-boolean v5, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->LauncherMode:Z

    .line 117
    const-string v5, "DigitalClockEasyAppWidget"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "EasyLauncher : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->LauncherMode:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_2
    invoke-static {p1}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->isEasyLauncher(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-static {}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->isJapanSmartPhone()Z

    move-result v5

    if-nez v5, :cond_3

    .line 121
    const-string v5, "DigitalClockEasyAppWidget"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "change to standard mode, mbWidgetRunning : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->mbWidgetRunning:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    sput-boolean v8, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->mbWidgetRunning:Z

    .line 123
    new-instance v2, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;

    invoke-direct {v2, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 124
    .local v2, "intent1":Landroid/content/Intent;
    invoke-virtual {p1, v2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 125
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v5, v4

    if-ge v1, v5, :cond_4

    .line 126
    const/4 v5, 0x1

    new-array v5, v5, [I

    aget v6, v4, v1

    aput v6, v5, v8

    invoke-virtual {p0, p1, v5}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 127
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 131
    .end local v1    # "i":I
    .end local v2    # "intent1":Landroid/content/Intent;
    :cond_3
    const-string v5, "DigitalClockEasyAppWidget"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "This is Easymode : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-boolean v7, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->mbWidgetRunning:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    if-eqz v4, :cond_4

    array-length v5, v4

    if-eqz v5, :cond_4

    .line 134
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->isDigitalClockWidgetServiceRunning(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 135
    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;

    invoke-direct {v3, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 136
    .local v3, "intentService":Landroid/content/Intent;
    invoke-virtual {p1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 140
    .end local v3    # "intentService":Landroid/content/Intent;
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 74
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 78
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->updateClock(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    .line 80
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyWidgetProvider;->isDigitalClockWidgetServiceRunning(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 81
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/digitalclockeasy/DigitalClockEasyService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 82
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 84
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
