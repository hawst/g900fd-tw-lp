.class Lcom/android/captiveportallogin/CaptivePortalLoginActivity$2;
.super Ljava/lang/Object;
.source "CaptivePortalLoginActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->testForCaptivePortal()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;


# direct methods
.method constructor <init>(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$2;->this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 212
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 215
    :goto_0
    const/4 v3, 0x0

    .line 216
    .local v3, "urlConnection":Ljava/net/HttpURLConnection;
    const/16 v2, 0x1f4

    .line 218
    .local v2, "httpResponseCode":I
    :try_start_1
    iget-object v4, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$2;->this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    # getter for: Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mURL:Ljava/net/URL;
    invoke-static {v4}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->access$300(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)Ljava/net/URL;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v3, v0

    .line 219
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 220
    const/16 v4, 0x2710

    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 221
    const/16 v4, 0x2710

    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 222
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 223
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    .line 224
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    .line 227
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 229
    :cond_0
    :goto_1
    const/16 v4, 0xcc

    if-ne v2, v4, :cond_1

    .line 230
    iget-object v4, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$2;->this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    const/4 v5, 0x1

    # invokes: Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->done(Z)V
    invoke-static {v4, v5}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->access$000(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;Z)V

    .line 232
    :cond_1
    return-void

    .line 225
    :catch_0
    move-exception v4

    .line 227
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_1

    :catchall_0
    move-exception v4

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    throw v4

    .line 213
    .end local v2    # "httpResponseCode":I
    .end local v3    # "urlConnection":Ljava/net/HttpURLConnection;
    :catch_1
    move-exception v4

    goto :goto_0
.end method
