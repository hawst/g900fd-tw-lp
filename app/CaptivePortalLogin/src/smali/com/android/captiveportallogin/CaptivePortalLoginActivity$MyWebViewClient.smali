.class Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "CaptivePortalLoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/captiveportallogin/CaptivePortalLoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyWebViewClient"
.end annotation


# instance fields
.field private firstPageLoad:Z

.field final synthetic this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;


# direct methods
.method private constructor <init>(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)V
    .locals 1

    .prologue
    .line 236
    iput-object p1, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;->this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 237
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;->firstPageLoad:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;Lcom/android/captiveportallogin/CaptivePortalLoginActivity$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/captiveportallogin/CaptivePortalLoginActivity;
    .param p2, "x1"    # Lcom/android/captiveportallogin/CaptivePortalLoginActivity$1;

    .prologue
    .line 236
    invoke-direct {p0, p1}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;-><init>(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 247
    iget-boolean v0, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;->firstPageLoad:Z

    if-eqz v0, :cond_0

    .line 248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;->firstPageLoad:Z

    .line 251
    iget-object v0, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;->this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    # invokes: Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->setWebViewProxy()V
    invoke-static {v0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->access$500(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)V

    .line 253
    iget-object v0, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;->this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    # getter for: Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mURL:Ljava/net/URL;
    invoke-static {v0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->access$300(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 257
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v0, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;->this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    # invokes: Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->testForCaptivePortal()V
    invoke-static {v0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->access$400(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)V

    goto :goto_0
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "favicon"    # Landroid/graphics/Bitmap;

    .prologue
    .line 241
    iget-boolean v0, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;->firstPageLoad:Z

    if-eqz v0, :cond_0

    .line 243
    :goto_0
    return-void

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;->this$0:Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    # invokes: Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->testForCaptivePortal()V
    invoke-static {v0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->access$400(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)V

    goto :goto_0
.end method
