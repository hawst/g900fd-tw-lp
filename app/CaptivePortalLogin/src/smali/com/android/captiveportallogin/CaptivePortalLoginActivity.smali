.class public Lcom/android/captiveportallogin/CaptivePortalLoginActivity;
.super Landroid/app/Activity;
.source "CaptivePortalLoginActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebChromeClient;,
        Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;
    }
.end annotation


# instance fields
.field private mNetId:I

.field private mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mURL:Ljava/net/URL;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 260
    return-void
.end method

.method static synthetic access$000(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/android/captiveportallogin/CaptivePortalLoginActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->done(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)Ljava/net/URL;
    .locals 1
    .param p0, "x0"    # Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mURL:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->testForCaptivePortal()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/captiveportallogin/CaptivePortalLoginActivity;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->setWebViewProxy()V

    return-void
.end method

.method private done(Z)V
    .locals 3
    .param p1, "use_network"    # Z

    .prologue
    .line 169
    invoke-static {p0}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    invoke-virtual {v1, v2}, Landroid/net/ConnectivityManager;->unregisterNetworkCallback(Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 170
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.net.netmon.captive_portal_logged_in"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 171
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.TEXT"

    iget v2, p0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mNetId:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string v2, "result"

    if-eqz p1, :cond_0

    const-string v1, "1"

    :goto_0
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    invoke-virtual {p0, v0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 174
    invoke-virtual {p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->finish()V

    .line 175
    return-void

    .line 172
    :cond_0
    const-string v1, "0"

    goto :goto_0
.end method

.method private setWebViewProxy()V
    .locals 15

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->getApplication()Landroid/app/Application;

    move-result-object v11

    iget-object v5, v11, Landroid/app/Application;->mLoadedApk:Landroid/app/LoadedApk;

    .line 148
    .local v5, "loadedApk":Landroid/app/LoadedApk;
    :try_start_0
    const-class v11, Landroid/app/LoadedApk;

    const-string v12, "mReceivers"

    invoke-virtual {v11, v12}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    .line 149
    .local v10, "receiversField":Ljava/lang/reflect/Field;
    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 150
    invoke-virtual {v10, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/util/ArrayMap;

    .line 151
    .local v9, "receivers":Landroid/util/ArrayMap;
    invoke-virtual {v9}, Landroid/util/ArrayMap;->values()Ljava/util/Collection;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 152
    .local v8, "receiverMap":Ljava/lang/Object;
    check-cast v8, Landroid/util/ArrayMap;

    .end local v8    # "receiverMap":Ljava/lang/Object;
    invoke-virtual {v8}, Landroid/util/ArrayMap;->keySet()Ljava/util/Set;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    .line 153
    .local v7, "rec":Ljava/lang/Object;
    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 154
    .local v0, "clazz":Ljava/lang/Class;
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "ProxyChangeListener"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 155
    const-string v11, "onReceive"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Class;

    const/4 v13, 0x0

    const-class v14, Landroid/content/Context;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const-class v14, Landroid/content/Intent;

    aput-object v14, v12, v13

    invoke-virtual {v0, v11, v12}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 157
    .local v6, "onReceiveMethod":Ljava/lang/reflect/Method;
    new-instance v4, Landroid/content/Intent;

    const-string v11, "android.intent.action.PROXY_CHANGE"

    invoke-direct {v4, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 158
    .local v4, "intent":Landroid/content/Intent;
    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v4, v11, v12

    invoke-virtual {v6, v7, v11}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    const-string v11, "CaptivePortalLogin"

    const-string v12, "Prompting WebView proxy reload."

    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 163
    .end local v0    # "clazz":Ljava/lang/Class;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v6    # "onReceiveMethod":Ljava/lang/reflect/Method;
    .end local v7    # "rec":Ljava/lang/Object;
    .end local v9    # "receivers":Landroid/util/ArrayMap;
    .end local v10    # "receiversField":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v1

    .line 164
    .local v1, "e":Ljava/lang/Exception;
    const-string v11, "CaptivePortalLogin"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Exception while setting WebView proxy: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_2
    return-void
.end method

.method private testForCaptivePortal()V
    .locals 2

    .prologue
    .line 208
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$2;

    invoke-direct {v1, p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$2;-><init>(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 234
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 2

    .prologue
    .line 185
    const v1, 0x7f070002

    invoke-virtual {p0, v1}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 186
    .local v0, "myWebView":Landroid/webkit/WebView;
    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 191
    :goto_0
    return-void

    .line 189
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 23
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    invoke-virtual/range {p0 .. p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v20

    const-string v21, "captive_portal_server"

    invoke-static/range {v20 .. v21}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 79
    .local v17, "server":Ljava/lang/String;
    if-nez v17, :cond_0

    const-string v17, "clients3.google.com"

    .line 81
    :cond_0
    :try_start_0
    new-instance v20, Ljava/net/URL;

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "http://"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "/generate_204"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mURL:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v20

    const-string v21, "android.intent.extra.TEXT"

    invoke-virtual/range {v20 .. v21}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mNetId:I

    .line 87
    new-instance v12, Landroid/net/Network;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mNetId:I

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-direct {v12, v0}, Landroid/net/Network;-><init>(I)V

    .line 88
    .local v12, "network":Landroid/net/Network;
    invoke-static {v12}, Landroid/net/ConnectivityManager;->setProcessDefaultNetwork(Landroid/net/Network;)Z

    .line 91
    invoke-static/range {p0 .. p0}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/net/ConnectivityManager;->getLinkProperties(Landroid/net/Network;)Landroid/net/LinkProperties;

    move-result-object v10

    .line 92
    .local v10, "lp":Landroid/net/LinkProperties;
    if-eqz v10, :cond_2

    .line 93
    invoke-virtual {v10}, Landroid/net/LinkProperties;->getHttpProxy()Landroid/net/ProxyInfo;

    move-result-object v16

    .line 94
    .local v16, "proxyInfo":Landroid/net/ProxyInfo;
    const-string v7, ""

    .line 95
    .local v7, "host":Ljava/lang/String;
    const-string v15, ""

    .line 96
    .local v15, "port":Ljava/lang/String;
    const-string v6, ""

    .line 97
    .local v6, "exclList":Ljava/lang/String;
    sget-object v14, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 98
    .local v14, "pacFileUrl":Landroid/net/Uri;
    if-eqz v16, :cond_1

    .line 99
    invoke-virtual/range {v16 .. v16}, Landroid/net/ProxyInfo;->getHost()Ljava/lang/String;

    move-result-object v7

    .line 100
    invoke-virtual/range {v16 .. v16}, Landroid/net/ProxyInfo;->getPort()I

    move-result v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v15

    .line 101
    invoke-virtual/range {v16 .. v16}, Landroid/net/ProxyInfo;->getExclusionListAsString()Ljava/lang/String;

    move-result-object v6

    .line 102
    invoke-virtual/range {v16 .. v16}, Landroid/net/ProxyInfo;->getPacFileUrl()Landroid/net/Uri;

    move-result-object v14

    .line 104
    :cond_1
    invoke-static {v7, v15, v6, v14}, Landroid/net/Proxy;->setHttpProxySystemProperty(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 105
    const-string v20, "CaptivePortalLogin"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Set proxy system properties to "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    .end local v6    # "exclList":Ljava/lang/String;
    .end local v7    # "host":Ljava/lang/String;
    .end local v14    # "pacFileUrl":Landroid/net/Uri;
    .end local v15    # "port":Ljava/lang/String;
    .end local v16    # "proxyInfo":Landroid/net/ProxyInfo;
    :cond_2
    const/high16 v20, 0x7f020000

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->setContentView(I)V

    .line 112
    invoke-virtual/range {p0 .. p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 115
    invoke-static/range {p0 .. p0}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/net/ConnectivityManager;->getNetworkCapabilities(Landroid/net/Network;)Landroid/net/NetworkCapabilities;

    move-result-object v13

    .line 117
    .local v13, "networkCapabilities":Landroid/net/NetworkCapabilities;
    if-nez v13, :cond_3

    .line 118
    invoke-virtual/range {p0 .. p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->finish()V

    .line 142
    :goto_1
    return-void

    .line 82
    .end local v10    # "lp":Landroid/net/LinkProperties;
    .end local v12    # "network":Landroid/net/Network;
    .end local v13    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :catch_0
    move-exception v5

    .line 83
    .local v5, "e":Ljava/net/MalformedURLException;
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->done(Z)V

    goto/16 :goto_0

    .line 121
    .end local v5    # "e":Ljava/net/MalformedURLException;
    .restart local v10    # "lp":Landroid/net/LinkProperties;
    .restart local v12    # "network":Landroid/net/Network;
    .restart local v13    # "networkCapabilities":Landroid/net/NetworkCapabilities;
    :cond_3
    new-instance v20, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$1;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$1;-><init>(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;Landroid/net/Network;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 127
    new-instance v4, Landroid/net/NetworkRequest$Builder;

    invoke-direct {v4}, Landroid/net/NetworkRequest$Builder;-><init>()V

    .line 128
    .local v4, "builder":Landroid/net/NetworkRequest$Builder;
    invoke-virtual {v13}, Landroid/net/NetworkCapabilities;->getTransportTypes()[I

    move-result-object v3

    .local v3, "arr$":[I
    array-length v9, v3

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_2
    if-ge v8, v9, :cond_4

    aget v18, v3, v8

    .line 129
    .local v18, "transportType":I
    move/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/net/NetworkRequest$Builder;->addTransportType(I)Landroid/net/NetworkRequest$Builder;

    .line 128
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 131
    .end local v18    # "transportType":I
    :cond_4
    invoke-static/range {p0 .. p0}, Landroid/net/ConnectivityManager;->from(Landroid/content/Context;)Landroid/net/ConnectivityManager;

    move-result-object v20

    invoke-virtual {v4}, Landroid/net/NetworkRequest$Builder;->build()Landroid/net/NetworkRequest;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    move-object/from16 v22, v0

    invoke-virtual/range {v20 .. v22}, Landroid/net/ConnectivityManager;->registerNetworkCallback(Landroid/net/NetworkRequest;Landroid/net/ConnectivityManager$NetworkCallback;)V

    .line 133
    const v20, 0x7f070002

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/webkit/WebView;

    .line 134
    .local v11, "myWebView":Landroid/webkit/WebView;
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/webkit/WebView;->clearCache(Z)V

    .line 135
    invoke-virtual {v11}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v19

    .line 136
    .local v19, "webSettings":Landroid/webkit/WebSettings;
    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 137
    new-instance v20, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebViewClient;-><init>(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;Lcom/android/captiveportallogin/CaptivePortalLoginActivity$1;)V

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 138
    new-instance v20, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebChromeClient;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity$MyWebChromeClient;-><init>(Lcom/android/captiveportallogin/CaptivePortalLoginActivity;Lcom/android/captiveportallogin/CaptivePortalLoginActivity$1;)V

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 141
    const-string v20, ""

    const-string v21, "text/html"

    const/16 v22, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-virtual {v11, v0, v1, v2}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f060000

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 180
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 195
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 196
    .local v0, "id":I
    const v2, 0x7f070004

    if-ne v0, v2, :cond_0

    .line 197
    invoke-direct {p0, v1}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->done(Z)V

    .line 204
    :goto_0
    return v1

    .line 200
    :cond_0
    const v2, 0x7f070003

    if-ne v0, v2, :cond_1

    .line 201
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/captiveportallogin/CaptivePortalLoginActivity;->done(Z)V

    goto :goto_0

    .line 204
    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    goto :goto_0
.end method
