.class Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;
.super Landroid/os/AsyncTask;
.source "MediaServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/MediaServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChangeDeviceNameTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;


# direct methods
.method private constructor <init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V
    .locals 0

    .prologue
    .line 2080
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p2, "x1"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;

    .prologue
    .line 2080
    invoke-direct {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2080
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;->doInBackground([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Void;
    .locals 4
    .param p1, "name"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 2084
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 2085
    .local v0, "deviceName":Ljava/lang/String;
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "ServerTask : doInBackground"

    invoke-static {v1, v2, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2087
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getFriendlyName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2093
    :goto_0
    return-object v3

    .line 2089
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2090
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v1

    const/16 v2, 0xbc3

    invoke-virtual {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessage(I)Z

    .line 2092
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setFriendlyName(Ljava/lang/String;Z)Z

    goto :goto_0
.end method
