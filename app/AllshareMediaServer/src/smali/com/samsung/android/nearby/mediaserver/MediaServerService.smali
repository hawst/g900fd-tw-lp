.class public Lcom/samsung/android/nearby/mediaserver/MediaServerService;
.super Landroid/app/Service;
.source "MediaServerService.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;,
        Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;,
        Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;,
        Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;
    }
.end annotation


# static fields
.field private static final IPV4_PATTERN:Ljava/util/regex/Pattern;


# instance fields
.field private final ACCEPT_LIST_MAX:I

.field private final ACCESS_ALWAYS_ACCEPT:Ljava/lang/String;

.field private final ACCESS_ALWAYS_ASK:Ljava/lang/String;

.field private final EVENT_ACCESS_ASK_POPUP:I

.field private final EVENT_CHECK_ACCESS_ASK:I

.field private final EVENT_CHECK_POPUP_PRIORITY:I

.field private final EVENT_DELAY_FINISH:I

.field private final EVENT_GET_MAC_ADDRESS:I

.field private final EVENT_SERVER_DB_UPDATED:I

.field private final EVENT_SERVER_OFF_CHECK:I

.field private final EVENT_SERVER_ON_CHECK:I

.field private final EVENT_SHOW_TOAST_RESTART:I

.field private final EVENT_SHOW_TOAST_STOP:I

.field private final EVENT_UPDATE_DB_START:I

.field private final EVENT_UPDATE_DB_STOP:I

.field private final EVENT_UPDATE_DOWNLOAD_FILE:I

.field private final EVENT_UPLOAD_ASK_POPUP:I

.field private final FLAG_ACTIVITY:I

.field private final KEY_FRIENDLY_NAME:Ljava/lang/String;

.field private final KEY_SHARE_TYPE:Ljava/lang/String;

.field private final KEY_UDN:Ljava/lang/String;

.field private final LENGTH_DEVICE_NAME_MAX:I

.field private final LENGTH_DEVICE_NAME_MAX_HOMESYNC:I

.field private final NOTIFICATION_ID:I

.field private final PREFERENCE:Ljava/lang/String;

.field private final STR_UNKNOWN_DEVICE:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private final TAGClass:Ljava/lang/String;

.field private final TIME_CHECK_ARP:I

.field private final TIME_CHECK_POPUP:I

.field private final TIME_UPDATE_MEDIA:I

.field private final UPLOAD_ALWAYS_ACCEPT:Ljava/lang/String;

.field private final UPLOAD_ALWAYS_ASK:Ljava/lang/String;

.field private final UPLOAD_ALWAYS_REJECT:Ljava/lang/String;

.field private isAccessAgain:Z

.field private isAccessAllowed:Z

.field private isAskUpload:Z

.field private isCBMModeChecker:Z

.field private isDelayedFinish:Z

.field private isFirstStarted:Z

.field private isRefreshDB:Z

.field private isRegisterReceiver:Z

.field private isScreenOn:Z

.field private isTablet:Z

.field private isUploadPopup:Z

.field private isUploadable:Z

.field private isWifiDisconnected:Z

.field private mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

.field private mAccessPopup:Z

.field private mAddList:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mChangedFlag:I

.field private mContext:Landroid/content/Context;

.field private mCurrentNotificationSub:Ljava/lang/String;

.field private mDBHandlerThread:Landroid/os/HandlerThread;

.field private mDBUpdateHandler:Landroid/os/Handler;

.field mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

.field private mIBinder:Lcom/android/settings/nearby/IMediaServer$Stub;

.field private mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

.field private mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;

.field private mMutex:Ljava/lang/Object;

.field private mNotiCount:I

.field private mNotificationMutex:Ljava/lang/Object;

.field private mNullList:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mP2pSubnet:Ljava/lang/String;

.field private mScheduledFlag:I

.field private mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

.field private mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

.field private mServiceHandlerThread:Landroid/os/HandlerThread;

.field private mSharedContentsFlag:I

.field private mUploadList:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;",
            ">;"
        }
    .end annotation
.end field

.field mUploadedFile:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2640
    const-string v0, "(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->IPV4_PATTERN:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0xbb8

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 92
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 95
    const-string v0, "AllShare(ASF-DMSLIB)"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->TAG:Ljava/lang/String;

    .line 97
    const-string v0, "(Service)MediaServerService: "

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->TAGClass:Ljava/lang/String;

    .line 99
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    .line 101
    iput v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mSharedContentsFlag:I

    .line 103
    iput v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mChangedFlag:I

    .line 105
    iput v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mScheduledFlag:I

    .line 108
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 110
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 112
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 114
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 117
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    .line 119
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;

    .line 121
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;

    .line 124
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadedFile:Ljava/util/List;

    .line 131
    const/16 v0, 0xbb9

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_UPLOAD_ASK_POPUP:I

    .line 133
    const/16 v0, 0xbbb

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_ACCESS_ASK_POPUP:I

    .line 135
    const/16 v0, 0xbbc

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_CHECK_POPUP_PRIORITY:I

    .line 137
    const/16 v0, 0xbbd

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_DELAY_FINISH:I

    .line 139
    const/16 v0, 0xbbe

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_SERVER_ON_CHECK:I

    .line 141
    const/16 v0, 0xbbf

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_SERVER_OFF_CHECK:I

    .line 143
    const/16 v0, 0xbc0

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_SERVER_DB_UPDATED:I

    .line 145
    const/16 v0, 0xbc1

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_GET_MAC_ADDRESS:I

    .line 147
    const/16 v0, 0xbc2

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_CHECK_ACCESS_ASK:I

    .line 149
    const/16 v0, 0xbc3

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_SHOW_TOAST_RESTART:I

    .line 151
    const/16 v0, 0xbc4

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_SHOW_TOAST_STOP:I

    .line 153
    const/16 v0, 0xbc5

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_UPDATE_DOWNLOAD_FILE:I

    .line 155
    const/16 v0, 0xfa1

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_UPDATE_DB_START:I

    .line 157
    const/16 v0, 0xfa2

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->EVENT_UPDATE_DB_STOP:I

    .line 159
    iput v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->TIME_CHECK_POPUP:I

    .line 161
    const/16 v0, 0x1388

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->TIME_CHECK_ARP:I

    .line 163
    iput v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->TIME_UPDATE_MEDIA:I

    .line 166
    const-string v0, "0"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->UPLOAD_ALWAYS_ACCEPT:Ljava/lang/String;

    .line 168
    const-string v0, "1"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->UPLOAD_ALWAYS_ASK:Ljava/lang/String;

    .line 170
    const-string v0, "2"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->UPLOAD_ALWAYS_REJECT:Ljava/lang/String;

    .line 172
    const-string v0, "0"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->ACCESS_ALWAYS_ACCEPT:Ljava/lang/String;

    .line 174
    const-string v0, "1"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->ACCESS_ALWAYS_ASK:Ljava/lang/String;

    .line 176
    const-string v0, "UNKNOWN DEVICE"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->STR_UNKNOWN_DEVICE:Ljava/lang/String;

    .line 178
    const-string v0, "AllShareMediaServer"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->PREFERENCE:Ljava/lang/String;

    .line 180
    const-string v0, "friendly_name"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->KEY_FRIENDLY_NAME:Ljava/lang/String;

    .line 182
    const-string v0, "udn"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->KEY_UDN:Ljava/lang/String;

    .line 184
    const-string v0, "share_type"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->KEY_SHARE_TYPE:Ljava/lang/String;

    .line 188
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mCurrentNotificationSub:Ljava/lang/String;

    .line 190
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNotificationMutex:Ljava/lang/Object;

    .line 193
    iput v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNotiCount:I

    .line 195
    const v0, 0x7f040006

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->NOTIFICATION_ID:I

    .line 197
    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->ACCEPT_LIST_MAX:I

    .line 199
    const/16 v0, 0x37

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->LENGTH_DEVICE_NAME_MAX:I

    .line 201
    const/16 v0, 0x35

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->LENGTH_DEVICE_NAME_MAX_HOMESYNC:I

    .line 203
    const v0, 0x30808000

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->FLAG_ACTIVITY:I

    .line 208
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessPopup:Z

    .line 210
    iput-boolean v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z

    .line 212
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAskUpload:Z

    .line 214
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAllowed:Z

    .line 216
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    .line 218
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRegisterReceiver:Z

    .line 220
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isWifiDisconnected:Z

    .line 222
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isTablet:Z

    .line 224
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z

    .line 226
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRefreshDB:Z

    .line 228
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isDelayedFinish:Z

    .line 230
    iput-boolean v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z

    .line 232
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isCBMModeChecker:Z

    .line 236
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isFirstStarted:Z

    .line 266
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    .line 268
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mP2pSubnet:Ljava/lang/String;

    .line 270
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    .line 272
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 274
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMutex:Ljava/lang/Object;

    .line 276
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBHandlerThread:Landroid/os/HandlerThread;

    .line 278
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateHandler:Landroid/os/Handler;

    .line 280
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandlerThread:Landroid/os/HandlerThread;

    .line 282
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    .line 1006
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mIBinder:Lcom/android/settings/nearby/IMediaServer$Stub;

    .line 1702
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 2080
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startServer()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->shareContents(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mSharedContentsFlag:I

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z

    return p1
.end method

.method static synthetic access$1402(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAskUpload:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAllowed:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAllowed:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mChangedFlag:I

    return v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isCBMModeChecker:Z

    return v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startActivityByUser(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeUserAgentToName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;ZLjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessPopup:Z

    return p1
.end method

.method static synthetic access$302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    return-object p1
.end method

.method static synthetic access$3100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z

    return v0
.end method

.method static synthetic access$3102(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z

    return p1
.end method

.method static synthetic access$3200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getRunningTasks()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isDelayedFinish:Z

    return v0
.end method

.method static synthetic access$3302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isDelayedFinish:Z

    return p1
.end method

.method static synthetic access$3400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->finishService()V

    return-void
.end method

.method static synthetic access$3500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRefreshDB:Z

    return v0
.end method

.method static synthetic access$3502(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Z

    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRefreshDB:Z

    return p1
.end method

.method static synthetic access$3600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mScheduledFlag:I

    return v0
.end method

.method static synthetic access$3700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getMacFromArpCache(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->showToastByUser(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mP2pSubnet:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mP2pSubnet:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getP2pSubnet()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeForgroundInfo(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getAcceptedListSize()I

    move-result v0

    return v0
.end method

.method static synthetic access$4400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isTablet:Z

    return v0
.end method

.method static synthetic access$4500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startNearbyActivity()V

    return-void
.end method

.method static synthetic access$4600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->checkDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/android/settings/nearby/IMediaServer$Stub;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mIBinder:Lcom/android/settings/nearby/IMediaServer$Stub;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startForegroundCompat(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    return-object v0
.end method

.method static synthetic access$702(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Landroid/net/wifi/WifiManager$MulticastLock;)Landroid/net/wifi/WifiManager$MulticastLock;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p1, "x1"    # Landroid/net/wifi/WifiManager$MulticastLock;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    return-object p1
.end method

.method static synthetic access$800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopServer()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopForegroundCompat()V

    return-void
.end method

.method private auditLog(ZLjava/lang/String;)V
    .locals 5
    .param p1, "flag"    # Z
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x5

    .line 2765
    const-string v2, "content://com.sec.knox.provider/AuditLog"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2766
    .local v1, "uri":Landroid/net/Uri;
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2767
    .local v0, "cv":Landroid/content/ContentValues;
    const-string v2, "severity"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2768
    const-string v2, "group"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2769
    const-string v2, "outcome"

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2770
    const-string v2, "uid"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2771
    const-string v2, "component"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2772
    const-string v2, "message"

    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2773
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 2774
    return-void
.end method

.method private changeForgroundInfo(Ljava/lang/String;)V
    .locals 5
    .param p1, "subTitle"    # Ljava/lang/String;

    .prologue
    .line 2215
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mCurrentNotificationSub:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2216
    const-string v0, "AllShare(ASF-DMSLIB)"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mCurrentNotificationSub:Ljava/lang/String;

    .line 2219
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNotificationMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 2220
    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mCurrentNotificationSub:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2221
    const-string v0, "(Service)MediaServerService: "

    const-string v2, "changeForgroundInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Same subtitle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mCurrentNotificationSub:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2222
    monitor-exit v1

    .line 2230
    :goto_0
    return-void

    .line 2223
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2224
    const-string v0, "(Service)MediaServerService: "

    const-string v2, "changeForgroundInfo"

    const-string v3, "Server is not started"

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2225
    monitor-exit v1

    goto :goto_0

    .line 2229
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2227
    :cond_2
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopForegroundCompat()V

    .line 2228
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startForegroundCompat(Ljava/lang/String;Z)V

    .line 2229
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private changeUserAgentToName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 2242
    if-nez p1, :cond_0

    .line 2243
    const-string v0, "UNKNOWN DEVICE"

    .line 2275
    :goto_0
    return-object v0

    .line 2246
    :cond_0
    const-string v0, "\""

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2247
    const-string v0, "\""

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2249
    :cond_1
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2250
    const-string v0, "UNKNOWN DEVICE"

    goto :goto_0

    .line 2253
    :cond_2
    const-string v0, "Microsoft-Windows/6.1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2254
    const-string p1, "Windows-Media-Player"

    move-object v0, p1

    .line 2255
    goto :goto_0

    .line 2258
    :cond_3
    const-string v0, "DLNADOC/1.50"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2259
    const-string v0, "DLNADOC/1.50"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    .line 2262
    :cond_4
    const-string v0, "SEC_HHP_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2263
    const-string v0, "SEC_HHP_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const-string v1, "SEC_HHP_"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 2265
    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2266
    const/4 v0, 0x0

    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    move-object v0, p1

    .line 2268
    goto :goto_0

    .line 2271
    :cond_6
    const-string v0, "MS-DeviceCaps/1024"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2272
    const-string v0, "MS-DeviceCaps/1024"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    :cond_7
    move-object v0, p1

    .line 2275
    goto :goto_0
.end method

.method private checkDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 2412
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "checkDeviceName"

    invoke-static {v5, v6, p1}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2414
    move-object v4, p1

    .line 2417
    .local v4, "modifiedName":Ljava/lang/String;
    if-nez p1, :cond_0

    .line 2418
    const/4 v5, 0x0

    .line 2469
    :goto_0
    return-object v5

    .line 2419
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2420
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2421
    .local v0, "changedName":Ljava/lang/String;
    move-object p1, v0

    .line 2422
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "checkDeviceName"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Remove all \'/\': "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2426
    .end local v0    # "changedName":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    .line 2428
    .local v3, "maxLength":I
    const/16 v3, 0x37

    .line 2430
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    if-lt v5, v3, :cond_2

    .line 2431
    const/4 v5, 0x0

    const/16 v6, 0x36

    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 2432
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "checkDeviceName"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "substring for MAX length: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2437
    :cond_2
    :try_start_0
    const-string v1, "[Mobile]"

    .line 2450
    .local v1, "deviceType":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v6, 0x258

    if-lt v5, v6, :cond_3

    .line 2451
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "checkDeviceName"

    const-string v7, "TABLET Device"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2452
    const-string v1, "[Tablet]"

    .line 2455
    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2456
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .end local v1    # "deviceType":Ljava/lang/String;
    :goto_1
    move-object v5, v4

    .line 2469
    goto/16 :goto_0

    .line 2458
    .restart local v1    # "deviceType":Ljava/lang/String;
    :cond_4
    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 2459
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 2461
    :cond_5
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f04000a

    invoke-virtual {p0, v6}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    goto :goto_1

    .line 2464
    .end local v1    # "deviceType":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 2465
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "checkDeviceName"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2466
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private convertInterfaceMacAddress(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "macAddress"    # Ljava/lang/String;

    .prologue
    .line 2338
    const-string v8, "(Service)MediaServerService: "

    const-string v9, "convertInterfaceMacAddress"

    const-string v10, ""

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2339
    const-string v5, ""

    .line 2340
    .local v5, "result":Ljava/lang/String;
    new-instance v3, Ljava/util/Formatter;

    invoke-direct {v3}, Ljava/util/Formatter;-><init>()V

    .line 2341
    .local v3, "partialMacAddr1":Ljava/util/Formatter;
    new-instance v4, Ljava/util/Formatter;

    invoke-direct {v4}, Ljava/util/Formatter;-><init>()V

    .line 2345
    .local v4, "partialMacAddr2":Ljava/util/Formatter;
    const/4 v8, 0x0

    const/4 v9, 0x2

    :try_start_0
    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 2346
    .local v6, "subString1":Ljava/lang/String;
    const/16 v8, 0x10

    invoke-static {v6, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 2347
    .local v1, "enable1":I
    add-int/lit8 v1, v1, -0x2

    .line 2348
    const-string v8, "%02x"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v3, v8, v9}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 2351
    const/16 v8, 0xc

    const/16 v9, 0xe

    invoke-virtual {p1, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 2352
    .local v7, "subString2":Ljava/lang/String;
    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    .line 2353
    .local v2, "enable2":I
    xor-int/lit16 v2, v2, 0x80

    .line 2354
    const-string v8, "%02x"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v4, v8, v9}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    .line 2356
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x2

    const/16 v10, 0xc

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v9, 0xe

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {p1, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 2363
    if-eqz v3, :cond_0

    .line 2364
    invoke-virtual {v3}, Ljava/util/Formatter;->close()V

    .line 2365
    :cond_0
    if-eqz v4, :cond_1

    .line 2366
    invoke-virtual {v4}, Ljava/util/Formatter;->close()V

    .line 2369
    .end local v1    # "enable1":I
    .end local v2    # "enable2":I
    .end local v6    # "subString1":Ljava/lang/String;
    .end local v7    # "subString2":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v5

    .line 2359
    :catch_0
    move-exception v0

    .line 2360
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v8, "(Service)MediaServerService: "

    const-string v9, "convertInterfaceMacAddress"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2361
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2363
    if-eqz v3, :cond_2

    .line 2364
    invoke-virtual {v3}, Ljava/util/Formatter;->close()V

    .line 2365
    :cond_2
    if-eqz v4, :cond_1

    .line 2366
    invoke-virtual {v4}, Ljava/util/Formatter;->close()V

    goto :goto_0

    .line 2363
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v8

    if-eqz v3, :cond_3

    .line 2364
    invoke-virtual {v3}, Ljava/util/Formatter;->close()V

    .line 2365
    :cond_3
    if-eqz v4, :cond_4

    .line 2366
    invoke-virtual {v4}, Ljava/util/Formatter;->close()V

    :cond_4
    throw v8
.end method

.method private finishService()V
    .locals 6

    .prologue
    .line 2279
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "finishService"

    const-string v5, ""

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2282
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v4, 0xbbc

    invoke-virtual {v3, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 2284
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 2285
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 2286
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .line 2287
    .local v2, "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 2288
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2285
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2291
    .end local v1    # "n":I
    .end local v2    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :catch_0
    move-exception v0

    .line 2292
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "finishService"

    const-string v5, "Exception"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 2293
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2296
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopSelf()V

    .line 2297
    return-void
.end method

.method private getAcceptedListSize()I
    .locals 5

    .prologue
    .line 2099
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    const-string v3, "AllshareAcceptList"

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2101
    .local v1, "pref_device":Landroid/content/SharedPreferences;
    const-string v2, "list_num"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2103
    .local v0, "count":I
    return v0
.end method

.method private getIPv4AddressFromLinkProperties([Ljava/lang/Object;)Ljava/lang/String;
    .locals 8
    .param p1, "linkProperites"    # [Ljava/lang/Object;

    .prologue
    .line 2649
    if-eqz p1, :cond_1

    array-length v4, p1

    if-lez v4, :cond_1

    .line 2650
    move-object v0, p1

    .local v0, "arr$":[Ljava/lang/Object;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v2, v0, v1

    .line 2651
    .local v2, "ip":Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isIPv4Address(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2652
    const-string v4, "(Service)MediaServerService: "

    const-string v5, "getIPv4AddressFromLinkProperties"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "true:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2653
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2659
    .end local v0    # "arr$":[Ljava/lang/Object;
    .end local v1    # "i$":I
    .end local v2    # "ip":Ljava/lang/Object;
    .end local v3    # "len$":I
    :goto_1
    return-object v4

    .line 2655
    .restart local v0    # "arr$":[Ljava/lang/Object;
    .restart local v1    # "i$":I
    .restart local v2    # "ip":Ljava/lang/Object;
    .restart local v3    # "len$":I
    :cond_0
    const-string v4, "(Service)MediaServerService: "

    const-string v5, "getIPv4AddressFromLinkProperties"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "false:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2650
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2659
    .end local v0    # "arr$":[Ljava/lang/Object;
    .end local v1    # "i$":I
    .end local v2    # "ip":Ljava/lang/Object;
    .end local v3    # "len$":I
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private getMacFromArpCache(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "ip"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 2374
    if-nez p1, :cond_0

    .line 2375
    const-string v7, "(Service)MediaServerService: "

    const-string v8, "getMacFromArpCache"

    const-string v9, "IP is NULL"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v6

    .line 2408
    :goto_0
    return-object v4

    .line 2378
    :cond_0
    const-string v7, "(Service)MediaServerService: "

    const-string v8, "getMacFromArpCache: IP - "

    invoke-static {v7, v8, p1}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2380
    const/4 v0, 0x0

    .line 2383
    .local v0, "br":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/FileReader;

    const-string v8, "/proc/net/arp"

    invoke-direct {v7, v8}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2386
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, "line":Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 2387
    const-string v7, " +"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 2388
    .local v5, "splitted":[Ljava/lang/String;
    if-eqz v5, :cond_1

    array-length v7, v5

    const/4 v8, 0x4

    if-lt v7, v8, :cond_1

    const/4 v7, 0x0

    aget-object v7, v5, v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2389
    const/4 v7, 0x3

    aget-object v4, v5, v7

    .line 2390
    .local v4, "mac":Ljava/lang/String;
    const-string v7, "(Service)MediaServerService: "

    const-string v8, "getMacFromArpCache"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "MAC: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2391
    const-string v7, "..:..:..:..:..:.."

    invoke-virtual {v4, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v7

    if-eqz v7, :cond_2

    .line 2403
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 2404
    :catch_0
    move-exception v2

    .line 2405
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 2403
    .end local v2    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_1
    move-object v4, v6

    .line 2406
    goto :goto_0

    .line 2404
    :catch_1
    move-exception v2

    .line 2405
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 2403
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "mac":Ljava/lang/String;
    .end local v5    # "splitted":[Ljava/lang/String;
    :cond_3
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "line":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :goto_2
    move-object v4, v6

    .line 2408
    goto :goto_0

    .line 2404
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 2405
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    move-object v0, v1

    .line 2407
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_2

    .line 2399
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "line":Ljava/lang/String;
    :catch_3
    move-exception v2

    .line 2400
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2403
    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_2

    .line 2404
    :catch_4
    move-exception v2

    .line 2405
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 2402
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 2403
    :goto_4
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 2406
    :goto_5
    throw v6

    .line 2404
    :catch_5
    move-exception v2

    .line 2405
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 2402
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_4

    .line 2399
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_3
.end method

.method private getNotificationIcon()I
    .locals 1

    .prologue
    .line 2233
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2234
    const v0, 0x7f020003

    .line 2236
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020001

    goto :goto_0
.end method

.method private getP2pSubnet()Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v8, 0x0

    .line 2493
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    const-string v10, "connectivity"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2496
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v9, 0xd

    invoke-virtual {v0, v9}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 2499
    .local v4, "netInfoP2p":Landroid/net/NetworkInfo;
    if-eqz v4, :cond_1

    sget-object v9, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 2500
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "getP2pSubnet"

    const-string v11, "P2P NOT CONNECTED"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2527
    :cond_0
    :goto_0
    return-object v8

    .line 2505
    :cond_1
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v6

    .line 2506
    .local v6, "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :cond_2
    invoke-interface {v6}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2508
    invoke-interface {v6}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 2509
    .local v5, "networkInterface":Ljava/net/NetworkInterface;
    if-eqz v5, :cond_2

    .line 2510
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    .line 2511
    .local v2, "iaenum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_3
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 2512
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    .line 2513
    .local v3, "interfaceAddress":Ljava/net/InetAddress;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "p2p"

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isIPv4Address(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 2515
    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    .line 2516
    .local v7, "p2pAddress":Ljava/lang/String;
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "getP2pSubnet"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "FOUND!! "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2518
    const/4 v9, 0x0

    const-string v10, "."

    invoke-virtual {v7, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    invoke-virtual {v7, v9, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_0

    .line 2523
    .end local v2    # "iaenum":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "interfaceAddress":Ljava/net/InetAddress;
    .end local v5    # "networkInterface":Ljava/net/NetworkInterface;
    .end local v6    # "networkInterfaces":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    .end local v7    # "p2pAddress":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 2524
    .local v1, "e":Ljava/lang/Exception;
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "getP2pSubnet"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Exception:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2525
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private getRunningTasks()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2300
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    const-string v4, "activity"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 2302
    .local v0, "activityManager":Landroid/app/ActivityManager;
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v1

    .line 2303
    .local v1, "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 2304
    .local v2, "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v3, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private getSubnetMask(J)J
    .locals 9
    .param p1, "subnet"    # J

    .prologue
    .line 2663
    const-string v3, "(Service)MediaServerService: "

    const-string v6, "getSubnetMask"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "subnet:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2664
    const-wide/16 v6, 0x0

    cmp-long v3, v6, p1

    if-gez v3, :cond_3

    const-wide/16 v6, 0x20

    cmp-long v3, p1, v6

    if-gez v3, :cond_3

    .line 2665
    const-wide/16 v4, 0x0

    .line 2667
    .local v4, "subnetMask":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x4

    if-ge v2, v3, :cond_2

    .line 2668
    const-wide/16 v0, 0x0

    .line 2669
    .local v0, "digit":J
    const-wide/16 v6, 0x0

    cmp-long v3, p1, v6

    if-lez v3, :cond_0

    .line 2670
    const-wide/16 v6, 0x7

    cmp-long v3, p1, v6

    if-lez v3, :cond_1

    .line 2671
    const-wide/16 v0, 0xff

    .line 2678
    :cond_0
    :goto_1
    mul-int/lit8 v3, v2, 0x8

    shl-long v6, v0, v3

    add-long/2addr v4, v6

    .line 2679
    const-wide/16 v6, 0x8

    sub-long/2addr p1, v6

    .line 2667
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2673
    :cond_1
    const/16 v3, 0xff

    const-wide/16 v6, 0x8

    sub-long/2addr v6, p1

    long-to-int v6, v6

    shl-int/2addr v3, v6

    and-int/lit16 v3, v3, 0xff

    int-to-long v0, v3

    goto :goto_1

    .line 2681
    .end local v0    # "digit":J
    :cond_2
    const-string v3, "(Service)MediaServerService: "

    const-string v6, "getSubnetMask"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "subnetMask"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->ipLongToString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2684
    .end local v2    # "i":I
    .end local v4    # "subnetMask":J
    :goto_2
    return-wide v4

    :cond_3
    const-wide/16 v4, 0x0

    goto :goto_2
.end method

.method private invokeMethod(Ljava/lang/reflect/Method;[Ljava/lang/Object;)V
    .locals 4
    .param p1, "method"    # Ljava/lang/reflect/Method;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 2165
    :try_start_0
    invoke-virtual {p1, p0, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2173
    :goto_0
    return-void

    .line 2166
    :catch_0
    move-exception v0

    .line 2168
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "invokeMethod"

    const-string v3, "Unable to invoke method"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 2169
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_1
    move-exception v0

    .line 2171
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "invokeMethod"

    const-string v3, "Unable to invoke method"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private ipLongToString(J)Ljava/lang/String;
    .locals 9
    .param p1, "ip"    # J

    .prologue
    const-wide/16 v6, 0xff

    .line 2724
    :try_start_0
    const-string v1, "%d.%d.%d.%d"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    and-long v4, p1, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0x8

    shr-long v4, p1, v4

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/16 v4, 0x10

    shr-long v4, p1, v4

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/16 v4, 0x18

    shr-long v4, p1, v4

    and-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2729
    :goto_0
    return-object v1

    .line 2726
    :catch_0
    move-exception v0

    .line 2727
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "ipLongToString"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ip("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2728
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2729
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private ipStringTolong(Ljava/lang/String;)J
    .locals 12
    .param p1, "ip"    # Ljava/lang/String;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v11, 0x4

    .line 2689
    :try_start_0
    const-string v8, "/"

    invoke-virtual {p1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 2690
    const/16 v8, 0x2f

    invoke-virtual {p1, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 2694
    :cond_0
    const/4 v8, 0x4

    new-array v0, v8, [J

    .line 2695
    .local v0, "digit":[J
    const-string v8, "\\."

    invoke-virtual {p1, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 2697
    .local v3, "parts":[Ljava/lang/String;
    array-length v8, v3

    if-ne v8, v11, :cond_2

    .line 2699
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v11, :cond_1

    .line 2700
    aget-object v8, v3, v2

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    int-to-long v8, v8

    aput-wide v8, v0, v2

    .line 2699
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2703
    :cond_1
    const-wide/16 v4, 0x0

    .line 2704
    .local v4, "ipNumbers":J
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v11, :cond_3

    .line 2705
    rsub-int/lit8 v8, v2, 0x3

    aget-wide v8, v0, v8

    mul-int/lit8 v10, v2, 0x8

    rsub-int/lit8 v10, v10, 0x18

    shl-long/2addr v8, v10

    add-long/2addr v4, v8

    .line 2704
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2710
    .end local v2    # "i":I
    .end local v4    # "ipNumbers":J
    :cond_2
    const-string v8, "(Service)MediaServerService: "

    const-string v9, "ipStringTolong"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "wrong part length: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    array-length v11, v3

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v4, v6

    .line 2717
    .end local v0    # "digit":[J
    .end local v3    # "parts":[Ljava/lang/String;
    :cond_3
    :goto_2
    return-wide v4

    .line 2714
    :catch_0
    move-exception v1

    .line 2715
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v8, "(Service)MediaServerService: "

    const-string v9, "ipStringTolong"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ip("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")Exception: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2716
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    move-wide v4, v6

    .line 2717
    goto :goto_2
.end method

.method private isIPv4Address(Ljava/lang/String;)Z
    .locals 1
    .param p1, "str"    # Ljava/lang/String;

    .prologue
    .line 2644
    sget-object v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->IPV4_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    return v0
.end method

.method private isP2pAddress(Ljava/lang/String;)Z
    .locals 7
    .param p1, "ip"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 2473
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "isP2pAddress"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mP2pSubnet:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2474
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mP2pSubnet:Ljava/lang/String;

    if-eqz v3, :cond_0

    if-nez p1, :cond_1

    .line 2489
    :cond_0
    :goto_0
    return v2

    .line 2478
    :cond_1
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2479
    .local v0, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v3, 0xd

    invoke-virtual {v0, v3}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    .line 2482
    .local v1, "netInfoP2p":Landroid/net/NetworkInfo;
    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/NetworkInfo$DetailedState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2484
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mP2pSubnet:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2485
    const-string v2, "(Service)MediaServerService: "

    const-string v3, "isP2pAddress"

    const-string v4, "DIRECT CONNECTION"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2486
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private isSingleHopIP(Ljava/lang/String;)Z
    .locals 22
    .param p1, "ip"    # Ljava/lang/String;

    .prologue
    .line 2532
    const-string v16, "connectivity"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/ConnectivityManager;

    .line 2533
    .local v4, "cm":Landroid/net/ConnectivityManager;
    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/net/ConnectivityManager;->getLinkProperties(I)Landroid/net/LinkProperties;

    move-result-object v8

    .line 2535
    .local v8, "prop":Landroid/net/LinkProperties;
    const-string v16, "wifi"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/wifi/WifiManager;

    .line 2537
    .local v9, "wifiManager":Landroid/net/wifi/WifiManager;
    if-eqz v8, :cond_5

    invoke-virtual {v8}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v16

    if-lez v16, :cond_5

    .line 2538
    invoke-virtual {v8}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/List;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getIPv4AddressFromLinkProperties([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2540
    .local v5, "localIP":Ljava/lang/String;
    if-eqz v5, :cond_4

    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 2541
    const-string v16, "/"

    move-object/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v16

    add-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    move/from16 v0, v16

    int-to-long v10, v0

    .line 2542
    .local v10, "subnet":J
    const-string v16, "(Service)MediaServerService: "

    const-string v17, "isSingleHopIP"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "localIP:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " subnet:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2544
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSubnetMask(J)J

    move-result-wide v12

    .line 2546
    .local v12, "subnetMask":J
    const-wide/16 v16, 0x0

    cmp-long v16, v12, v16

    if-eqz v16, :cond_3

    .line 2547
    invoke-virtual {v9}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v16

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    and-long v6, v16, v12

    .line 2551
    .local v6, "networkIP":J
    const-wide/16 v16, 0x0

    cmp-long v16, v6, v16

    if-eqz v16, :cond_2

    .line 2552
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->ipStringTolong(Ljava/lang/String;)J

    move-result-wide v14

    .line 2553
    .local v14, "targetIP":J
    const-string v16, "(Service)MediaServerService: "

    const-string v17, "isSingleHopIP"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "targetIP & subnetMask: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    and-long v20, v14, v12

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->ipLongToString(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " networkIP:"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->ipLongToString(J)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2557
    const-wide/16 v16, 0x0

    cmp-long v16, v14, v16

    if-eqz v16, :cond_1

    .line 2558
    and-long v16, v14, v12

    cmp-long v16, v6, v16

    if-nez v16, :cond_0

    const/16 v16, 0x1

    .line 2637
    .end local v5    # "localIP":Ljava/lang/String;
    .end local v6    # "networkIP":J
    .end local v10    # "subnet":J
    .end local v12    # "subnetMask":J
    .end local v14    # "targetIP":J
    :goto_0
    return v16

    .line 2558
    .restart local v5    # "localIP":Ljava/lang/String;
    .restart local v6    # "networkIP":J
    .restart local v10    # "subnet":J
    .restart local v12    # "subnetMask":J
    .restart local v14    # "targetIP":J
    :cond_0
    const/16 v16, 0x0

    goto :goto_0

    .line 2560
    :cond_1
    const-string v16, "(Service)MediaServerService: "

    const-string v17, "isSingleHopIP"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Wrong targetIP: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2637
    .end local v5    # "localIP":Ljava/lang/String;
    .end local v6    # "networkIP":J
    .end local v10    # "subnet":J
    .end local v12    # "subnetMask":J
    .end local v14    # "targetIP":J
    :goto_1
    const/16 v16, 0x0

    goto :goto_0

    .line 2563
    .restart local v5    # "localIP":Ljava/lang/String;
    .restart local v6    # "networkIP":J
    .restart local v10    # "subnet":J
    .restart local v12    # "subnetMask":J
    :cond_2
    const-string v16, "(Service)MediaServerService: "

    const-string v17, "isSingleHopIP"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Wrong networkIP: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2566
    .end local v6    # "networkIP":J
    :cond_3
    const-string v16, "(Service)MediaServerService: "

    const-string v17, "isSingleHopIP"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Wrong subnetMask: "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2569
    .end local v10    # "subnet":J
    .end local v12    # "subnetMask":J
    :cond_4
    const-string v16, "(Service)MediaServerService: "

    const-string v17, "isSingleHopIP"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "Wrong localIP (no slash\'/\'): "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v16 .. v18}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2571
    .end local v5    # "localIP":Ljava/lang/String;
    :cond_5
    const-string v16, "wifi"

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/net/wifi/WifiManager;

    invoke-virtual/range {v16 .. v16}, Landroid/net/wifi/WifiManager;->getWifiApState()I

    move-result v16

    const/16 v17, 0xd

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_6

    .line 2572
    const-string v16, "(Service)MediaServerService: "

    const-string v17, "isSingleHopIP"

    const-string v18, "WIFI_AP_STATE_ENABLED"

    invoke-static/range {v16 .. v18}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2574
    const/16 v16, 0x1

    goto/16 :goto_0

    .line 2635
    :cond_6
    const-string v16, "(Service)MediaServerService: "

    const-string v17, "isSingleHopIP"

    const-string v18, "prop.getLinkAddresses().size() is null"

    invoke-static/range {v16 .. v18}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private isUserMatchedWithCurrentProcess()Z
    .locals 4

    .prologue
    .line 2754
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2755
    const/4 v0, 0x1

    .line 2760
    :goto_0
    return v0

    .line 2757
    :cond_0
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "isUserMatchedWithCurrentProcess"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "NOT MATCHED - myUserId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", currentUserId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2760
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private resetValues()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1314
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1315
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1316
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    .line 1319
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1320
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1321
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessPopup:Z

    .line 1324
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1325
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1327
    :cond_2
    return-void
.end method

.method private setBroadcastReceiver()V
    .locals 6

    .prologue
    .line 1640
    :try_start_0
    iget-boolean v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRegisterReceiver:Z

    if-nez v2, :cond_0

    .line 1641
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.net.wifi.STATE_CHANGE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1643
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1645
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1647
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.LOCALE_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1649
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.nearby.mediaserver.UPLOAD_ASK_CANCEL"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1650
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.nearby.mediaserver.UPLOAD_ASK_OK"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1651
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.nearby.mediaserver.UPLOAD_ASK_TIME_OVER"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1652
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.nearby.mediaserver.ACCESS_ASK_OK"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1653
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.nearby.mediaserver.ACCESS_ASK_CANCEL"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1654
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.nearby.mediaserver.ACCESS_ASK_AGAIN"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1655
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.nearby.mediaserver.DELETE_ITEMS"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1656
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.nearby.mediaserver.START_NEARBY_SETTINGS"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1658
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.android.settings.DEVICE_NAME_CHANGED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1659
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.SETTINGS_SOFT_RESET"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v4, "com.samsung.android.settings.permission.SOFT_RESET"

    const/4 v5, 0x0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 1662
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1663
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1676
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1677
    .local v1, "mediaFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.MEDIA_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1678
    const-string v2, "android.intent.action.MEDIA_BAD_REMOVAL"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1679
    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1680
    const-string v2, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1681
    const-string v2, "file"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1682
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1684
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRegisterReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1689
    .end local v1    # "mediaFilter":Landroid/content/IntentFilter;
    :cond_0
    :goto_0
    return-void

    .line 1686
    :catch_0
    move-exception v0

    .line 1687
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "(Service)MediaServerService: "

    const-string v3, "setBroadcastReceiver"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private declared-synchronized shareContents(I)V
    .locals 6
    .param p1, "flag"    # I

    .prologue
    .line 839
    monitor-enter p0

    :try_start_0
    const-string v2, "(Service)MediaServerService: "

    const-string v3, "shareContents"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    iget v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mSharedContentsFlag:I

    if-ne v2, p1, :cond_1

    .line 842
    const-string v2, "(Service)MediaServerService: "

    const-string v3, "shareContents"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "flag unchanged: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mSharedContentsFlag:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 872
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 844
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->isUpdating()Z
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->access$000(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 845
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRefreshDB:Z

    .line 846
    iput p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mScheduledFlag:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 839
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 850
    :cond_2
    :try_start_2
    iget v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mSharedContentsFlag:I

    xor-int/2addr v2, p1

    iput v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mChangedFlag:I

    .line 851
    iput p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mSharedContentsFlag:I

    .line 853
    const-string v2, "AllShareMediaServer"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 854
    .local v1, "preference":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 855
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "share_type"

    iget v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mSharedContentsFlag:I

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 856
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 858
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 859
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v3, 0xbc3

    invoke-virtual {v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessage(I)Z

    .line 862
    :cond_3
    iget-boolean v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isFirstStarted:Z

    if-eqz v2, :cond_0

    .line 864
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->isAlive()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->isUpdating()Z
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->access$000(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 866
    const-string v2, "(Service)MediaServerService: "

    const-string v3, "shareContents"

    const-string v4, "init DBUpdateThread"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    .line 870
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateHandler:Landroid/os/Handler;

    const/16 v3, 0xfa1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private showToastByUser(Ljava/lang/String;I)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "duration"    # I

    .prologue
    .line 2748
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUserMatchedWithCurrentProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2749
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2751
    :cond_0
    return-void
.end method

.method private startActivityByUser(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2742
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUserMatchedWithCurrentProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2743
    invoke-virtual {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startActivity(Landroid/content/Intent;)V

    .line 2745
    :cond_0
    return-void
.end method

.method private startForegroundCompat(ILandroid/app/Notification;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "notification"    # Landroid/app/Notification;

    .prologue
    .line 2177
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startForeground(ILandroid/app/Notification;)V

    .line 2178
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.samsung.android.nearby.mediaserver.NEARBY_SERVER_STARTED"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2192
    return-void
.end method

.method private startForegroundCompat(Ljava/lang/String;Z)V
    .locals 10
    .param p1, "subTitle"    # Ljava/lang/String;
    .param p2, "isDefault"    # Z

    .prologue
    const v9, 0x7f040006

    const v8, 0x7f04000a

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2133
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "startForegroundCompat"

    invoke-static {v3, v4, p1}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2134
    if-nez p1, :cond_0

    .line 2135
    iget-boolean v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    if-eqz v3, :cond_2

    .line 2136
    const v3, 0x7f040007

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {p0, v8}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2141
    :cond_0
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.samsung.android.nearby.mediaserver.START_NEARBY_SETTINGS"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2142
    .local v1, "intent":Landroid/content/Intent;
    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getNotificationIcon()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {p0, v8}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {p0, v8}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-static {v4, v6, v1, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 2147
    .local v0, "builder":Landroid/app/Notification$Builder;
    if-eqz p2, :cond_1

    .line 2148
    invoke-virtual {v0, v7}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    .line 2150
    :cond_1
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    .line 2152
    .local v2, "notification":Landroid/app/Notification;
    invoke-direct {p0, v9, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startForegroundCompat(ILandroid/app/Notification;)V

    .line 2154
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mCurrentNotificationSub:Ljava/lang/String;

    .line 2155
    return-void

    .line 2138
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "notification":Landroid/app/Notification;
    :cond_2
    invoke-virtual {p0, v9}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private startNearbyActivity()V
    .locals 3

    .prologue
    .line 2734
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2735
    .local v0, "intentActivity":Landroid/content/Intent;
    const-string v1, "com.android.settings"

    const-string v2, "com.android.settings.nearby.NearbySettings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2737
    const v1, 0x30808000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2738
    invoke-direct {p0, v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startActivityByUser(Landroid/content/Intent;)V

    .line 2739
    return-void
.end method

.method private startServer()V
    .locals 6

    .prologue
    const v5, 0x7f040006

    const v4, 0x7f040012

    const/4 v3, 0x1

    .line 1248
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 1249
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->start()Z

    .line 1250
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1252
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isFirstStarted:Z

    if-nez v0, :cond_0

    .line 1253
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "startServer"

    const-string v2, "First Started"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    iput-boolean v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isFirstStarted:Z

    .line 1257
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateHandler:Landroid/os/Handler;

    const/16 v1, 0xfa1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1259
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    if-nez v0, :cond_1

    .line 1260
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "startServer"

    const-string v2, "Init MediaDB"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    invoke-virtual {p0, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startForegroundCompat(Ljava/lang/String;Z)V

    .line 1272
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->acquireWakeLock()V

    .line 1273
    return-void

    .line 1250
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1263
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->isUpdating()Z
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->access$000(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1264
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "startServer"

    invoke-virtual {p0, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1266
    invoke-virtual {p0, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startForegroundCompat(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1268
    :cond_2
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "startServer"

    invoke-virtual {p0, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1269
    invoke-virtual {p0, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startForegroundCompat(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private stopForegroundCompat()V
    .locals 3

    .prologue
    .line 2158
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "stopForegroundCompat"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2159
    const v0, 0x7f040006

    invoke-direct {p0, v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopForegroundCompat(I)V

    .line 2160
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mCurrentNotificationSub:Ljava/lang/String;

    .line 2161
    return-void
.end method

.method private stopForegroundCompat(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 2196
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopForeground(Z)V

    .line 2197
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.samsung.android.nearby.mediaserver.NEARBY_SERVER_STARTED"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 2212
    return-void
.end method

.method private stopServer()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1277
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    invoke-virtual {v1, v6}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1278
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v2, 0xbbf

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1280
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->resetValues()V

    .line 1282
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateHandler:Landroid/os/Handler;

    const/16 v2, 0xfa2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1284
    const/4 v0, 0x0

    .line 1285
    .local v0, "byebye":Z
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1286
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "stopServer"

    const-string v3, "byebye"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1287
    const/4 v0, 0x1

    .line 1292
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 1293
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->stop(Z)Z

    .line 1294
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1296
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->removeMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    .line 1297
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->removeMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    .line 1298
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->removeMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    .line 1300
    const v1, 0xffffff

    iput v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mChangedFlag:I

    .line 1302
    iput-object v6, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 1304
    iput-object v6, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 1306
    iput-object v6, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 1308
    iput-object v6, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    .line 1310
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->releaseWakeLock()V

    .line 1311
    return-void

    .line 1289
    :cond_0
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "stopServer"

    const-string v3, "stopServer(): no byebye"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1294
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private unregisterBroadcastReceiver()V
    .locals 4

    .prologue
    .line 1693
    :try_start_0
    iget-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRegisterReceiver:Z

    if-eqz v1, :cond_0

    .line 1694
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1695
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRegisterReceiver:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1700
    :cond_0
    :goto_0
    return-void

    .line 1697
    :catch_0
    move-exception v0

    .line 1698
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "unregisterBroadcastReceiver"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public acquireWakeLock()V
    .locals 4

    .prologue
    .line 2109
    :try_start_0
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "acquireWakeLock"

    const-string v3, " "

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2110
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_1

    .line 2111
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2112
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 2114
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2119
    :cond_1
    :goto_0
    return-void

    .line 2116
    :catch_0
    move-exception v0

    .line 2117
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "acquireWakeLock"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public announceDMS()V
    .locals 2

    .prologue
    .line 2308
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/nearby/mediaserver/MediaServerService$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$3;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2313
    .local v0, "announce":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2314
    return-void
.end method

.method public byebyeDMS()V
    .locals 2

    .prologue
    .line 2317
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/nearby/mediaserver/MediaServerService$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$4;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2322
    .local v0, "byebye":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2323
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 514
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "onBind"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mIBinder:Lcom/android/settings/nearby/IMediaServer$Stub;

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 469
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "onConfigurationChanged"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    monitor-enter p0

    .line 473
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    const v1, 0x7f040001

    invoke-virtual {p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->setFriendlyName(Ljava/lang/String;)V

    .line 475
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    if-eqz v0, :cond_1

    .line 476
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    const v1, 0x7f040002

    invoke-virtual {p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->setFriendlyName(Ljava/lang/String;)V

    .line 477
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    if-eqz v0, :cond_2

    .line 478
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    const v1, 0x7f040003

    invoke-virtual {p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->setFriendlyName(Ljava/lang/String;)V

    .line 479
    :cond_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 480
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 481
    return-void

    .line 479
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V
    .locals 12
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .prologue
    const/16 v11, 0xbc2

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 702
    if-nez p1, :cond_1

    .line 819
    :cond_0
    :goto_0
    return-void

    .line 705
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getMacAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeUserAgentToName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 707
    .local v2, "key":Ljava/lang/String;
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    invoke-static {v5, v6, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isP2pAddress(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 714
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getMacAddress()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->convertInterfaceMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeUserAgentToName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 716
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Direct Connection - change key: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 723
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v5

    const-string v6, "SEC_RVF"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 724
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v5, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 725
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "REJECT CONNECTION: Remote Veiw finder: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 717
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isSingleHopIP(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 718
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "NOT Single hop IP: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 730
    :cond_4
    iget-boolean v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAllowed:Z

    if-eqz v5, :cond_5

    .line 732
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Allow All: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v5, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 738
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Incoming connection from device "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " succeeded"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v9, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 742
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getMacAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0xa

    if-ge v5, v6, :cond_8

    .line 743
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MAC is null!!!!"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getMacAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Incoming connection from device "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " failed - invalid MAC address"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v10, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V

    .line 753
    const-string v5, "wifi"

    invoke-virtual {p0, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/wifi/WifiManager;

    .line 754
    .local v4, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v1

    .line 755
    .local v1, "ipAddress":I
    const-string v5, "%d.%d.%d.%d"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    and-int/lit16 v7, v1, 0xff

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    shr-int/lit8 v7, v1, 0x8

    and-int/lit16 v7, v7, 0xff

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    const/4 v7, 0x2

    shr-int/lit8 v8, v1, 0x10

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x3

    shr-int/lit8 v8, v1, 0x18

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 758
    .local v0, "ip":Ljava/lang/String;
    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 759
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "LocalHost IP(ACCEPT): "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 761
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v5, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 813
    .end local v0    # "ip":Ljava/lang/String;
    .end local v1    # "ipAddress":I
    .end local v4    # "wifiManager":Landroid/net/wifi/WifiManager;
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 814
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    invoke-virtual {v5, v11}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 815
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const-wide/16 v6, 0x2710

    invoke-virtual {v5, v11, v6, v7}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 762
    .restart local v0    # "ip":Ljava/lang/String;
    .restart local v1    # "ipAddress":I
    .restart local v4    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 763
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    const-string v7, "Check ARP Table after 5000 seconds"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v6, 0xbc1

    invoke-virtual {v5, v6}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 766
    .local v3, "msg":Landroid/os/Message;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 767
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const-wide/16 v6, 0x1388

    invoke-virtual {v5, v3, v6, v7}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 769
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 771
    .end local v3    # "msg":Landroid/os/Message;
    :cond_7
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    const-string v7, "Waiting for checking ARP Table"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 773
    .end local v0    # "ip":Ljava/lang/String;
    .end local v1    # "ipAddress":I
    .end local v4    # "wifiManager":Landroid/net/wifi/WifiManager;
    :cond_8
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    invoke-virtual {v5, v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->checkRejectedList(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 774
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "on mRejectList:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 775
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v5, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 781
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Incoming connection from device "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " failed - device not allowed"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v10, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V

    goto/16 :goto_1

    .line 783
    :cond_9
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    invoke-virtual {v5, v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->checkAcceptedList(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 784
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "on mAcceptList:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Incoming connection from device "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " succeeded"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v9, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V

    .line 791
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v5, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    goto/16 :goto_1

    .line 792
    :cond_a
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;

    invoke-virtual {v5, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 793
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "on mAddList:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 795
    :cond_b
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "NEW DEVICE: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    iget-boolean v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessPopup:Z

    if-eqz v5, :cond_c

    .line 797
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EXTRA ADD TO LIST:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 799
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;

    invoke-virtual {v5, v2, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 801
    :cond_c
    const-string v5, "(Service)MediaServerService: "

    const-string v6, "onConnectionRequest"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FIRST ADD TO LIST:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    iput-boolean v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessPopup:Z

    .line 804
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;

    invoke-virtual {v5, v2, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 806
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v6, 0xbbb

    invoke-virtual {v5, v6}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v3

    .line 807
    .restart local v3    # "msg":Landroid/os/Message;
    iput-object v2, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 809
    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const-wide/16 v6, 0x1f4

    invoke-virtual {v5, v3, v6, v7}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_1
.end method

.method public onCreate()V
    .locals 10

    .prologue
    .line 286
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 297
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    .line 298
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    .line 300
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "DBUpdate Handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBHandlerThread:Landroid/os/HandlerThread;

    .line 301
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 302
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;

    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateHandler:Landroid/os/Handler;

    .line 304
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "Service Handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandlerThread:Landroid/os/HandlerThread;

    .line 305
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 306
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    .line 309
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 310
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setOnUploadItemListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;)V

    .line 311
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setOnConnectionListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;)V

    .line 312
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setOnServerStartedListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;)V

    .line 314
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 315
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopForegroundCompat()V

    .line 318
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->setBroadcastReceiver()V

    .line 320
    invoke-static {}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isLightTheme()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 321
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x103012b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    .line 328
    :goto_0
    const-string v0, "AllShareMediaServer"

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 330
    .local v9, "preference":Landroid/content/SharedPreferences;
    const-string v0, "friendly_name"

    const-string v1, "Samsung Mobile"

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 331
    .local v4, "friendlyName":Ljava/lang/String;
    const-string v0, "udn"

    const-string v1, ""

    invoke-interface {v9, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 333
    .local v5, "udn":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 334
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 335
    .local v7, "edit":Landroid/content/SharedPreferences$Editor;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "uuid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 336
    const-string v0, "udn"

    invoke-interface {v7, v0, v5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 337
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 341
    .end local v7    # "edit":Landroid/content/SharedPreferences$Editor;
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    invoke-virtual {v0}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->loadPreference()V

    .line 344
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    const v1, 0x7f030002

    const/high16 v2, 0x7f030000

    const v3, 0x7f030001

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->initailize(IIILjava/lang/String;Ljava/lang/String;)V

    .line 346
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 347
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isTablet:Z

    .line 388
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isTablet:Z

    if-eqz v0, :cond_5

    .line 389
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "onCreate"

    const-string v2, "TABLET Device"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 390
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    const v1, 0x7f030009

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->MACROICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setIcon(ILcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V

    .line 391
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    const v1, 0x7f030009

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->MACROICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setIcon(ILcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V

    .line 392
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    const v1, 0x7f03000a

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->SMALLICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setIcon(ILcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V

    .line 393
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    const v1, 0x7f03000a

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->SMALLICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setIcon(ILcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V

    .line 420
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/PowerManager;

    .line 422
    .local v8, "powerManager":Landroid/os/PowerManager;
    const/4 v0, 0x1

    const-string v1, "DMS WakeLock"

    invoke-virtual {v8, v0, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 437
    .end local v4    # "friendlyName":Ljava/lang/String;
    .end local v5    # "udn":Ljava/lang/String;
    .end local v8    # "powerManager":Landroid/os/PowerManager;
    .end local v9    # "preference":Landroid/content/SharedPreferences;
    :goto_2
    return-void

    .line 324
    :cond_4
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1030128

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 432
    :catch_0
    move-exception v6

    .line 433
    .local v6, "e":Ljava/lang/Exception;
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "onCreate"

    const-string v2, "Exception"

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 434
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    .line 395
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v4    # "friendlyName":Ljava/lang/String;
    .restart local v5    # "udn":Ljava/lang/String;
    .restart local v9    # "preference":Landroid/content/SharedPreferences;
    :cond_5
    :try_start_1
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "onCreate"

    const-string v2, "Default Device - Mobile"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    const v1, 0x7f030005

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->MACROICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setIcon(ILcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V

    .line 397
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    const v1, 0x7f030005

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->MACROICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setIcon(ILcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V

    .line 398
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    const v1, 0x7f030006

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->SMALLICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setIcon(ILcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V

    .line 399
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    const v1, 0x7f030006

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->SMALLICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setIcon(ILcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 485
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->unregisterBroadcastReceiver()V

    .line 489
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 491
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBHandlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 495
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandlerThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 496
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 499
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isWifiDisconnected:Z

    if-nez v0, :cond_3

    .line 500
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 501
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopServer()V

    .line 503
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$MulticastLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 504
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$MulticastLock;->release()V

    .line 507
    :cond_3
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 510
    return-void
.end method

.method public onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V
    .locals 7
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .prologue
    const/16 v6, 0xbb9

    const/4 v5, 0x0

    const/16 v4, 0xbc5

    .line 620
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onFailed"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 621
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v2, 0xbbc

    invoke-virtual {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 623
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadedFile:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 624
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onRequest"

    const-string v3, "SET UODATE MEDIA DB MESSAGE AFTER 3000 ms"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    invoke-virtual {v1, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 627
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v4, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 631
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 632
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .line 633
    .local v0, "eldest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getDeviceName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 635
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onFailed"

    const-string v3, "remove eldest uploadList"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 642
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    .line 643
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    invoke-virtual {v1, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 644
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    invoke-virtual {v1, v6}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 645
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v6, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 655
    .end local v0    # "eldest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 656
    const v1, 0x7f040006

    invoke-virtual {p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeForgroundInfo(Ljava/lang/String;)V

    .line 657
    :cond_1
    return-void

    .line 638
    .restart local v0    # "eldest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :cond_2
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onFailed"

    const-string v3, "DO NOT remove eldest"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 647
    :cond_3
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onFailed"

    const-string v3, "no more upload"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 648
    iput-boolean v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    goto :goto_1

    .line 651
    .end local v0    # "eldest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :cond_4
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onFailed"

    const-string v3, "NO MORE UPLOAD"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 652
    iput-boolean v5, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    goto :goto_1
.end method

.method public onFinished(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/io/File;)V
    .locals 14
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    .param p2, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p3, "file"    # Ljava/io/File;

    .prologue
    .line 532
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onFinished"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 616
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_5

    .line 539
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .line 540
    .local v1, "eldest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getTitle()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getDeviceName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getDeviceName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 542
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onFinished"

    const-string v11, "remove eldest uploadList"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    iget-object v10, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v10}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    :goto_1
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v9}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    .line 549
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    .line 550
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v10, 0xbb9

    invoke-virtual {v9, v10}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 551
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v10, 0xbb9

    const-wide/16 v12, 0x3e8

    invoke-virtual {v9, v10, v12, v13}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 561
    .end local v1    # "eldest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :goto_2
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onFinished"

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadedFile:Ljava/util/List;

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 566
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onRequest"

    const-string v11, "SET UODATE MEDIA DB MESSAGE AFTER 3000 ms"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v10, 0xbc5

    invoke-virtual {v9, v10}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 569
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v10, 0xbc5

    const-wide/16 v12, 0xbb8

    invoke-virtual {v9, v10, v12, v13}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 571
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v9, v10, v11, v12}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 578
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v6

    .line 579
    .local v6, "mimeTypeMap":Landroid/webkit/MimeTypeMap;
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 581
    .local v2, "extension":Ljava/lang/String;
    const-string v9, ""

    invoke-virtual {v2, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "."

    invoke-virtual {v9, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 582
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v10

    const-string v11, "."

    invoke-virtual {v10, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 583
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onFinished"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Get Extension Error. get extension manual: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    :cond_2
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 589
    .local v5, "mimeType":Ljava/lang/String;
    new-instance v4, Landroid/content/Intent;

    const-string v9, "android.intent.action.VIEW"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 590
    .local v4, "intentNotification":Landroid/content/Intent;
    invoke-virtual/range {p3 .. p3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v4, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 591
    invoke-static/range {p3 .. p3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v4, v9, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 593
    new-instance v9, Landroid/app/Notification$Builder;

    iget-object v10, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getNotificationIcon()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v9

    const v10, 0x7f04000a

    invoke-virtual {p0, v10}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    const v10, 0x7f04000a

    invoke-virtual {p0, v10}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    const v10, 0x7f040014

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getDeviceName()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v10, v11, v4, v12}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v8

    .line 603
    .local v8, "notification":Landroid/app/Notification;
    const-string v9, "notification"

    invoke-virtual {p0, v9}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/NotificationManager;

    .line 604
    .local v7, "mn":Landroid/app/NotificationManager;
    iget v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNotiCount:I

    add-int/lit8 v10, v9, 0x1

    iput v10, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNotiCount:I

    invoke-virtual {v7, v9, v8}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 605
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onFinished"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "MimeType: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", extension: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 611
    .end local v2    # "extension":Ljava/lang/String;
    .end local v4    # "intentNotification":Landroid/content/Intent;
    .end local v5    # "mimeType":Ljava/lang/String;
    .end local v6    # "mimeTypeMap":Landroid/webkit/MimeTypeMap;
    .end local v7    # "mn":Landroid/app/NotificationManager;
    .end local v8    # "notification":Landroid/app/Notification;
    :goto_3
    new-instance v3, Landroid/content/Intent;

    const-string v9, "com.samsung.android.nearby.mediaserver.REMOVE_UPLOAD_ASK_POPUP"

    invoke-direct {v3, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 612
    .local v3, "intentBroadcast":Landroid/content/Intent;
    invoke-virtual {p0, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 614
    iget-object v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v9}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 615
    const v9, 0x7f040006

    invoke-virtual {p0, v9}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeForgroundInfo(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 545
    .end local v3    # "intentBroadcast":Landroid/content/Intent;
    .restart local v1    # "eldest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :cond_3
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onFinished"

    const-string v11, "DO NOT remove eldest"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 553
    :cond_4
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onFinished"

    const-string v11, "no more upload"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    goto/16 :goto_2

    .line 557
    .end local v1    # "eldest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :cond_5
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onFinished"

    const-string v11, "NO MORE UPLOAD"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    goto/16 :goto_2

    .line 606
    :catch_0
    move-exception v0

    .line 607
    .local v0, "e":Ljava/lang/Exception;
    const-string v9, "(Service)MediaServerService: "

    const-string v10, "onFinished"

    const-string v11, "Exception"

    invoke-static {v9, v10, v11, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 608
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 520
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "onRebind"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 522
    return-void
.end method

.method public onRequested(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V
    .locals 8
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .prologue
    const/4 v7, 0x1

    .line 661
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "onRequested"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "onRequest"

    const-string v5, "REMOVE UPDATE MEDIA DB MESSAGE"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v4, 0xbc5

    invoke-virtual {v3, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 668
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getUploadPath()Ljava/io/File;

    move-result-object v2

    .line 669
    .local v2, "uploadPath":Ljava/io/File;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_1

    .line 671
    :cond_0
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "onRequest"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "make upload directory"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 675
    :cond_1
    iget-boolean v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAskUpload:Z

    if-eqz v3, :cond_3

    .line 676
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 677
    .local v1, "time":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 681
    iget-boolean v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    if-nez v3, :cond_2

    .line 682
    iput-boolean v7, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z

    .line 683
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v4, 0xbb9

    invoke-virtual {v3, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 684
    .local v0, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    invoke-virtual {v3, v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    .line 697
    .end local v0    # "msg":Landroid/os/Message;
    .end local v1    # "time":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 689
    :cond_3
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "onRequested"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Upload: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    iget-boolean v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z

    if-eqz v3, :cond_4

    .line 692
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v3, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->allowUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 693
    const v3, 0x7f040007

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getDeviceName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeForgroundInfo(Ljava/lang/String;)V

    goto :goto_0

    .line 695
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v3, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    goto :goto_0
.end method

.method public onServerStarted(Z)V
    .locals 5
    .param p1, "value"    # Z

    .prologue
    .line 823
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onServerStarted"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.settings.allshare.SERVER_STATE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 826
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "START"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 827
    invoke-virtual {p0, v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 829
    if-eqz p1, :cond_0

    .line 830
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v2, 0xbbe

    invoke-virtual {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 831
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->acquireWakeLock()V

    .line 836
    :goto_0
    return-void

    .line 833
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    const/16 v2, 0xbbf

    invoke-virtual {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 834
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->releaseWakeLock()V

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 442
    iget-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isDelayedFinish:Z

    if-eqz v1, :cond_0

    .line 443
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onStartCommand"

    const-string v3, "Cancel Delayed Finish"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 444
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isDelayedFinish:Z

    .line 448
    :cond_0
    if-eqz p1, :cond_2

    .line 449
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 450
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onStartCommand"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "started server "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 464
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    return v1

    .line 452
    :cond_1
    :try_start_1
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onStartCommand"

    const-string v3, "Stop Foreground"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const v1, 0x7f040006

    invoke-direct {p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopForegroundCompat(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onStartCommand"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 462
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 456
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_2
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "onStartCommand"

    const-string v3, "intent is NULL - Restarted Process, stopSelf"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    const v1, 0x7f040006

    invoke-direct {p0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopForegroundCompat(I)V

    .line 458
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopSelf()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 526
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "onUnbind"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public reannounceDMS()V
    .locals 4

    .prologue
    .line 2326
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "reannounceDMS"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2327
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/nearby/mediaserver/MediaServerService$5;

    invoke-direct {v1, p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$5;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2334
    .local v0, "reannounce":Ljava/lang/Thread;
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 2335
    return-void
.end method

.method public releaseWakeLock()V
    .locals 4

    .prologue
    .line 2123
    :try_start_0
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "releaseWakeLock"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2124
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2125
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2130
    :cond_0
    :goto_0
    return-void

    .line 2127
    :catch_0
    move-exception v0

    .line 2128
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "releaseWakeLock"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
