.class Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$4;
.super Ljava/lang/Object;
.source "MediaServerUploadDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->onCreateDialog(I)Landroid/app/ProgressDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 186
    const-string v1, "(Service)MediaServerUploadDialog: "

    const-string v2, "onCreateDialog"

    const-string v3, "onCancel: UPLOAD_ASK_CANCEL"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.nearby.mediaserver.UPLOAD_ASK_CANCEL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 188
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "KEY"

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    # getter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mKey:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->access$000(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-virtual {v1, v0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->sendBroadcast(Landroid/content/Intent;)V

    .line 190
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    iget-object v1, v1, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->progressThread:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->setState(I)V

    .line 191
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-virtual {v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->popupDestroy()V

    .line 192
    return-void
.end method
