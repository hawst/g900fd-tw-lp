.class Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$2;
.super Landroid/os/Handler;
.source "DMSAutoStartEnabler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$2;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 255
    const-string v2, "DMSAutoStartEnabler"

    const-string v3, "handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :try_start_0
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 270
    const-string v2, "DMSAutoStartEnabler"

    const-string v3, "handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "WRONG MSG:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 278
    :goto_0
    return-void

    .line 261
    :pswitch_0
    :try_start_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.android.nearby.MediaServer.START"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 262
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$2;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    # getter for: Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->access$300(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 263
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$2;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    # getter for: Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->access$300(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$2;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    # getter for: Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mServiceConn:Landroid/content/ServiceConnection;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->access$400(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)Landroid/content/ServiceConnection;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 264
    .end local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 265
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "DMSAutoStartEnabler"

    const-string v3, "InitEnabler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception on bind service:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 273
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 274
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v2, "DMSAutoStartEnabler"

    const-string v3, "handleMessage"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 275
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 258
    :pswitch_data_0
    .packed-switch 0xbb8
        :pswitch_0
    .end packed-switch
.end method
