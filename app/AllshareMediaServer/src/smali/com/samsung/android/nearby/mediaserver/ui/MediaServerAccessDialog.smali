.class public Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;
.super Landroid/app/Activity;
.source "MediaServerAccessDialog.java"


# static fields
.field private static final ASK_POPUP:I = 0xbb8

.field private static final CHECK_POPUP:I = 0xbb9

.field private static final TAG:Ljava/lang/String; = "AllShareDMS"

.field private static final TAGClass:Ljava/lang/String; = "(Service)MediaServerAccessDialog: "

.field private static final TIME_DELAY:I = 0x1388


# instance fields
.field private bAccessAgain:Z

.field private bChecked:Z

.field private bRegister:Z

.field mAskPopupDlg:Landroid/app/AlertDialog;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mDeviceName:Ljava/lang/String;

.field final mHandler:Landroid/os/Handler;

.field private mKey:Ljava/lang/String;

.field mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bRegister:Z

    .line 42
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bAccessAgain:Z

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mDeviceName:Ljava/lang/String;

    .line 46
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mKey:Ljava/lang/String;

    .line 48
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    .line 50
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 52
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bChecked:Z

    .line 91
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 188
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$5;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bChecked:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bChecked:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bAccessAgain:Z

    return p1
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 272
    :cond_0
    return-void
.end method

.method private setBroadcastReceiver()V
    .locals 3

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bRegister:Z

    if-nez v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.samsung.android.nearby.mediaserver.changeto.ACCESS_ALLOWED_ALL"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 277
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 279
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bRegister:Z

    .line 283
    :cond_0
    return-void
.end method

.method private setWakeLock()V
    .locals 6

    .prologue
    .line 252
    const-string v2, "keyguard"

    invoke-virtual {p0, v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 253
    .local v0, "kManager":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bAccessAgain:Z

    if-nez v2, :cond_1

    .line 254
    const-string v2, "(Service)MediaServerAccessDialog: "

    const-string v3, "onResume"

    const-string v4, "Screen locked: awake from sleep mode"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 257
    .local v1, "pm":Landroid/os/PowerManager;
    const v2, 0x3000001a

    const-string v3, "AllShareDMS"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 260
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v2

    if-nez v2, :cond_0

    .line 261
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v4, 0x1770

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 263
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xbb9

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 266
    .end local v1    # "pm":Landroid/os/PowerManager;
    :cond_1
    return-void
.end method

.method private unregisterBroadcastReceiver()V
    .locals 1

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bRegister:Z

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bRegister:Z

    .line 290
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->requestWindowFeature(I)Z

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\u202a"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "DEVICE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u202c"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mDeviceName:Ljava/lang/String;

    .line 60
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mKey:Ljava/lang/String;

    .line 61
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "ACCESSAGAIN"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bAccessAgain:Z

    .line 63
    const-string v0, "(Service)MediaServerAccessDialog: "

    const-string v1, "onCreate"

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mDeviceName:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/AlertDialog;
    .locals 7
    .param p1, "id"    # I

    .prologue
    .line 126
    const-string v2, "(Service)MediaServerAccessDialog: "

    const-string v3, "onCreateDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const/16 v2, 0xbb8

    if-ne p1, v2, :cond_1

    .line 131
    const/4 v1, 0x4

    .line 133
    .local v1, "theme":I
    :try_start_0
    invoke-static {}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isLightTheme()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    const-string v2, "(Service)MediaServerAccessDialog: "

    const-string v3, "onCreateDialog"

    const-string v4, "set light theme"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/4 v1, 0x5

    .line 138
    :cond_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f04000a

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f04000b

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mDeviceName:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f040004

    new-instance v4, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$4;

    invoke-direct {v4, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$4;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f040005

    new-instance v4, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$3;

    invoke-direct {v4, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$3;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$2;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mAskPopupDlg:Landroid/app/AlertDialog;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    .end local v1    # "theme":I
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mAskPopupDlg:Landroid/app/AlertDialog;

    return-object v2

    .line 181
    .restart local v1    # "theme":I
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "(Service)MediaServerAccessDialog: "

    const-string v3, "onCreateDialog"

    const-string v4, "Exception on create popup"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected bridge synthetic onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->onCreateDialog(I)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 226
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 228
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->unregisterBroadcastReceiver()V

    .line 230
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :goto_0
    return-void

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerAccessDialog: "

    const-string v2, "onPause"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v4, 0x54

    .line 83
    if-eq p1, v4, :cond_0

    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 84
    :cond_0
    const-string v0, "(Service)MediaServerAccessDialog: "

    const-string v1, "onKeyDown"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KEYCODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " vs "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->popupDestroy()V

    .line 86
    const/4 v0, 0x1

    .line 88
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 5

    .prologue
    .line 213
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 214
    const-string v1, "(Service)MediaServerAccessDialog: "

    const-string v2, "onPause"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPause : bChecked :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bChecked:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " / mKey :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mKey:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    iget-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bChecked:Z

    if-nez v1, :cond_0

    .line 217
    const-string v1, "(Service)MediaServerAccessDialog: "

    const-string v2, "onPause"

    const-string v3, "Broadcast: ACCESS_ASK_AGAIN"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.nearby.mediaserver.ACCESS_ASK_AGAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 219
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->sendBroadcast(Landroid/content/Intent;)V

    .line 221
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->popupDestroy()V

    .line 222
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    .line 68
    const-string v1, "(Service)MediaServerAccessDialog: "

    const-string v2, "onResume"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "bAccessAgain: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bAccessAgain:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 71
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->setBroadcastReceiver()V

    .line 73
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->setWakeLock()V

    .line 74
    const/16 v1, 0xbb8

    invoke-virtual {p0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->showDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerAccessDialog: "

    const-string v2, "onResume"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected popupDestroy()V
    .locals 4

    .prologue
    .line 240
    :try_start_0
    const-string v1, "(Service)MediaServerAccessDialog: "

    const-string v2, "popupDestroy"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const/16 v1, 0xbb8

    invoke-virtual {p0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->removeDialog(I)V

    .line 243
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->releaseWakeLock()V

    .line 245
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :goto_0
    return-void

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerAccessDialog: "

    const-string v2, "popupDestroy"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
