.class Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
.super Landroid/os/Handler;
.source "MediaServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/MediaServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1367
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .line 1368
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1369
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 22
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1373
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1376
    :try_start_0
    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_0

    .line 1635
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 1378
    :pswitch_1
    invoke-static {}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isVZWFeature()Z

    move-result v18

    if-eqz v18, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isCBMModeChecker:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 1379
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "CBM MODE!! UPLOAD POPUP CANCEL!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1381
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v7, v0, :cond_1

    .line 1382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .line 1383
    .local v17, "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 1381
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 1387
    .end local v17    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2602(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1631
    .end local v7    # "i":I
    :catch_0
    move-exception v5

    .line 1632
    .local v5, "e":Ljava/lang/Exception;
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "Exception"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1633
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1392
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_3

    .line 1393
    new-instance v10, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;

    move-result-object v18

    const-class v19, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1394
    .local v10, "it":Landroid/content/Intent;
    const v18, 0x30808000

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1395
    const-string v19, "KEY"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1396
    const-string v19, "DEVICE"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getDeviceName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1397
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startActivityByUser(Landroid/content/Intent;)V
    invoke-static {v0, v10}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Landroid/content/Intent;)V

    .line 1398
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbbc

    const-wide/16 v20, 0xbb8

    invoke-virtual/range {v18 .. v21}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1401
    .end local v10    # "it":Landroid/content/Intent;
    :cond_3
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "mUploadList is Empty"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1407
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 1408
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "EVENT_SERVER_ON_CHECK: Server START SUCCESS!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1410
    new-instance v8, Landroid/content/Intent;

    const-string v18, "com.android.settings.allshare.SERVER_STATE_CHANGE"

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1412
    .local v8, "intent":Landroid/content/Intent;
    const-string v18, "START"

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1413
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1415
    .end local v8    # "intent":Landroid/content/Intent;
    :cond_4
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "EVENT_SERVER_ON_CHECK: Server START fail!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1421
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v18

    if-nez v18, :cond_5

    .line 1422
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "EVENT_SERVER_OFF_CHECK: Server STOP SUCCESS!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    new-instance v8, Landroid/content/Intent;

    const-string v18, "com.android.settings.allshare.SERVER_STATE_CHANGE"

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1426
    .restart local v8    # "intent":Landroid/content/Intent;
    const-string v18, "START"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1429
    .end local v8    # "intent":Landroid/content/Intent;
    :cond_5
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "EVENT_SERVER_OFF_CHECK: Server STOP fail!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1435
    :pswitch_4
    move-object/from16 v0, p1

    iget-object v13, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v13, Ljava/lang/String;

    .line 1436
    .local v13, "key":Ljava/lang/String;
    const-string v16, ""

    .line 1438
    .local v16, "name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 1439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeUserAgentToName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1445
    invoke-static {}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isVZWFeature()Z

    move-result v18

    if-eqz v18, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isCBMModeChecker:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 1446
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "CBM MODE!! ACCESS POPUP CANCEL!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1448
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v7, v0, :cond_7

    .line 1449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .line 1450
    .local v17, "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1451
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 1458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Incoming connection from device "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " failed"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;ZLjava/lang/String;)V

    .line 1448
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1441
    .end local v7    # "i":I
    .end local v17    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    :cond_6
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "EVENT_ACCESS_ASK_POPUP: NO KEY"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1461
    .restart local v7    # "i":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessPopup:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3002(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto/16 :goto_0

    .line 1468
    .end local v7    # "i":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbc2

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1470
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const-string v19, "keyguard"

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/KeyguardManager;

    .line 1472
    .local v12, "kManager":Landroid/app/KeyguardManager;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAllowed:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v18

    if-eqz v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    if-eqz v18, :cond_9

    .line 1473
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "DO NOT Show Access Popup: "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAllowed:Z
    invoke-static/range {v21 .. v21}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .line 1477
    .restart local v17    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 1484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Incoming connection from device "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " succeeded"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V
    invoke-static/range {v18 .. v20}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;ZLjava/lang/String;)V

    goto/16 :goto_0

    .line 1486
    .end local v17    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    :cond_9
    invoke-virtual {v12}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v18

    if-eqz v18, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v18

    if-eqz v18, :cond_a

    .line 1487
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "DO NOT Show Access Popup: Screen OFF"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbbb

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v15

    .line 1492
    .local v15, "msgAccess":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const-wide/16 v20, 0x1388

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v15, v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1495
    .end local v15    # "msgAccess":Landroid/os/Message;
    :cond_a
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "Show Access Popup:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1496
    new-instance v11, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;

    move-result-object v18

    const-class v19, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1497
    .local v11, "itAccess":Landroid/content/Intent;
    const v18, 0x30808000

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1498
    const-string v18, "KEY"

    move-object/from16 v0, v18

    invoke-virtual {v11, v0, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1499
    const-string v18, "DEVICE"

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1500
    const-string v18, "ACCESSAGAIN"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v11, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1501
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startActivityByUser(Landroid/content/Intent;)V
    invoke-static {v0, v11}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Landroid/content/Intent;)V

    .line 1502
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3102(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto/16 :goto_0

    .line 1508
    .end local v11    # "itAccess":Landroid/content/Intent;
    .end local v12    # "kManager":Landroid/app/KeyguardManager;
    .end local v13    # "key":Ljava/lang/String;
    .end local v16    # "name":Ljava/lang/String;
    :pswitch_5
    invoke-static {}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isVZWFeature()Z

    move-result v18

    if-eqz v18, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isCBMModeChecker:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v18

    if-eqz v18, :cond_c

    .line 1509
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "CBM MODE!! CHECK POPUP CANCEL!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1510
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v7, v0, :cond_b

    .line 1511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .line 1512
    .local v17, "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v19

    invoke-interface/range {v19 .. v19}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1513
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 1510
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 1515
    .end local v17    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2602(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto/16 :goto_0

    .line 1519
    .end local v7    # "i":I
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getRunningTasks()Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/lang/String;

    move-result-object v4

    .line 1520
    .local v4, "className":Ljava/lang/String;
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "HANDLE_CHECK_POPUP_PRIORITY : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1523
    const-string v18, "com.samsung.android.nearby.mediaserver.ui.MediaServerUploadDialog"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_d

    .line 1525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbb9

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1527
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbbc

    const-wide/16 v20, 0xbb8

    invoke-virtual/range {v18 .. v21}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1534
    .end local v4    # "className":Ljava/lang/String;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getRunningTasks()Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/lang/String;

    move-result-object v4

    .line 1535
    .restart local v4    # "className":Ljava/lang/String;
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "EVENT_DELAY_FINISH : "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 1538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    move-object/from16 v18, v0

    if-eqz v18, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    move-object/from16 v18, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->isUpdating()Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->access$000(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;)Z

    move-result v18

    if-eqz v18, :cond_e

    .line 1539
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "WAIT FOR DB UPDATE!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isDelayedFinish:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto/16 :goto_0

    .line 1544
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v18

    if-nez v18, :cond_f

    .line 1545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->finishService()V
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    goto/16 :goto_0

    .line 1547
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopServer()V
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    .line 1548
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbbd

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1549
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbbd

    const-wide/16 v20, 0x3e8

    invoke-virtual/range {v18 .. v21}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1556
    .end local v4    # "className":Ljava/lang/String;
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isDelayedFinish:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v18

    if-eqz v18, :cond_10

    .line 1557
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbbd

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1558
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbbd

    const-wide/16 v20, 0x64

    invoke-virtual/range {v18 .. v21}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1562
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRefreshDB:Z
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 1563
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isRefreshDB:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3502(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mScheduledFlag:I
    invoke-static/range {v19 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)I

    move-result v19

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->shareContents(I)V
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;I)V

    goto/16 :goto_0

    .line 1569
    :pswitch_8
    move-object/from16 v0, p1

    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v9, Ljava/lang/String;

    .line 1571
    .local v9, "ip":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_12

    .line 1572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .line 1573
    .local v17, "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mNullList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1574
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const-string v19, "/"

    const-string v20, ""

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v19

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getMacFromArpCache(Ljava/lang/String;)Ljava/lang/String;
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1575
    .local v14, "mac":Ljava/lang/String;
    if-eqz v14, :cond_11

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v18

    const/16 v19, 0xa

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_11

    .line 1576
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "EVENT_GET_MAC_ADDRESS: ACCEPT - "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1578
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    goto/16 :goto_0

    .line 1580
    :cond_11
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "EVENT_GET_MAC_ADDRESS: REJECT"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    goto/16 :goto_0

    .line 1584
    .end local v14    # "mac":Ljava/lang/String;
    .end local v17    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    :cond_12
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "EVENT_GET_MAC_ADDRESS: no list item"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1590
    .end local v9    # "ip":Ljava/lang/String;
    :pswitch_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getRunningTasks()Ljava/lang/String;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/lang/String;

    move-result-object v18

    const-string v19, "com.samsung.android.nearby.mediaserver.ui.MediaServerAccessDialog"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v18

    if-eqz v18, :cond_13

    .line 1592
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "MediaServerAccessDialog is top"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1594
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    if-eqz v18, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_14

    .line 1595
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "EVENT_CHECK_ACCESS_ASK: ACCESS POPUP recovery!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1598
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbbb

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1600
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z
    invoke-static/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3102(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const/16 v19, 0xbbb

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v15

    .line 1603
    .restart local v15    # "msgAccess":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v15, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1604
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v18

    const-wide/16 v20, 0xbb8

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v15, v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1606
    .end local v15    # "msgAccess":Landroid/os/Message;
    :cond_14
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "mAddList is Empty or null"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1612
    :pswitch_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    const v20, 0x7f040011

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->showToastByUser(Ljava/lang/String;I)V
    invoke-static/range {v18 .. v20}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1616
    :pswitch_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    const v20, 0x7f04000c

    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->showToastByUser(Ljava/lang/String;I)V
    invoke-static/range {v18 .. v20}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 1620
    :pswitch_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadedFile:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_15

    .line 1621
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "UPDATE MEDIA DB!!"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1622
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadedFile:Ljava/util/List;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadedFile:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v18

    check-cast v18, [Ljava/lang/String;

    move-object/from16 v0, v18

    check-cast v0, [Ljava/lang/String;

    move-object v6, v0

    .line 1624
    .local v6, "files":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;

    move-result-object v18

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v6, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    .line 1625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadedFile:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->clear()V

    goto/16 :goto_0

    .line 1627
    .end local v6    # "files":[Ljava/lang/String;
    :cond_15
    const-string v18, "(Service)MediaServerService: "

    const-string v19, "ServiceHandler"

    const-string v20, "mUploadedFile is null"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1376
    nop

    :pswitch_data_0
    .packed-switch 0xbb9
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
