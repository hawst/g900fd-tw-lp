.class Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;
.super Landroid/content/BroadcastReceiver;
.source "MediaServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/MediaServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V
    .locals 0

    .prologue
    .line 1702
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 30
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1706
    :try_start_0
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    .line 1707
    .local v4, "action":Ljava/lang/String;
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "action: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1710
    const-string v23, "android.net.wifi.STATE_CHANGE"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_0

    const-string v23, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_0

    const-string v23, "android.net.ethernet.ETH_STATE_CHANGED"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_0

    const-string v23, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_d

    .line 1715
    :cond_0
    const-string v23, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1

    .line 1717
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v24, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getP2pSubnet()Ljava/lang/String;
    invoke-static/range {v24 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/lang/String;

    move-result-object v24

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mP2pSubnet:Ljava/lang/String;
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4002(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;

    .line 1718
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "mP2pSubnet: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v26, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mP2pSubnet:Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4000(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1721
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->isInitialStickyBroadcast()Z

    move-result v23

    if-eqz v23, :cond_3

    .line 1722
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "isInitialStickyBroadcast: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2077
    .end local v4    # "action":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 1727
    .restart local v4    # "action":Ljava/lang/String;
    :cond_3
    const/16 v17, 0x0

    .line 1729
    .local v17, "networkConnected":Z
    const-string v23, "connectivity"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/ConnectivityManager;

    .line 1732
    .local v8, "connectivityManager":Landroid/net/ConnectivityManager;
    const/16 v20, 0x0

    .line 1733
    .local v20, "state":Ljava/lang/String;
    const-string v23, "android.net.wifi.STATE_CHANGE"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 1734
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v15

    .line 1736
    .local v15, "netInfo":Landroid/net/NetworkInfo;
    const-string v23, "networkInfo"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v15

    .end local v15    # "netInfo":Landroid/net/NetworkInfo;
    check-cast v15, Landroid/net/NetworkInfo;

    .line 1738
    .restart local v15    # "netInfo":Landroid/net/NetworkInfo;
    invoke-virtual {v15}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/net/NetworkInfo$DetailedState;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1739
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;

    move-result-object v23

    const-string v24, "FLAG_INTERFACE_WIFI"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isNetworkConnected(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v17

    .line 1755
    .end local v15    # "netInfo":Landroid/net/NetworkInfo;
    :cond_4
    :goto_1
    if-nez v17, :cond_c

    .line 1756
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getRunningTasks()Ljava/lang/String;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/lang/String;

    move-result-object v6

    .line 1757
    .local v6, "className":Ljava/lang/String;
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "HANDLE_CHECK_POPUP_PRIORITY : "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopForegroundCompat()V
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    .line 1761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->releaseWakeLock()V

    .line 1763
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v23

    if-eqz v23, :cond_6

    .line 1764
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "WIFI STATE CHANGED : DISCONNECTED : stopMediaServer"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v24, v0

    const v25, 0x7f04000c

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->showToastByUser(Ljava/lang/String;I)V
    invoke-static/range {v23 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;I)V

    .line 1769
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v23

    if-eqz v23, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$MulticastLock;->isHeld()Z

    move-result v23

    if-eqz v23, :cond_5

    .line 1770
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/net/wifi/WifiManager$MulticastLock;->release()V

    .line 1772
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopServer()V
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    .line 1775
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v23

    if-nez v23, :cond_7

    const-string v23, "com.android.settings.nearby.NearbySettings"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_7

    const-string v23, "com.android.settings.Settings$NearbySettingsActivity"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 1779
    :cond_7
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "Nearby device is top"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2072
    .end local v4    # "action":Ljava/lang/String;
    .end local v6    # "className":Ljava/lang/String;
    .end local v8    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v17    # "networkConnected":Z
    .end local v20    # "state":Ljava/lang/String;
    :catch_0
    move-exception v11

    .line 2073
    .local v11, "e":Ljava/lang/Exception;
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Exception"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2074
    invoke-virtual {v11}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1741
    .end local v11    # "e":Ljava/lang/Exception;
    .restart local v4    # "action":Ljava/lang/String;
    .restart local v8    # "connectivityManager":Landroid/net/ConnectivityManager;
    .restart local v17    # "networkConnected":Z
    .restart local v20    # "state":Ljava/lang/String;
    :cond_8
    :try_start_1
    const-string v23, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 1742
    const/16 v23, 0xd

    move/from16 v0, v23

    invoke-virtual {v8, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v16

    .line 1745
    .local v16, "netInfoP2p":Landroid/net/NetworkInfo;
    const-string v23, "networkInfo"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v16

    .end local v16    # "netInfoP2p":Landroid/net/NetworkInfo;
    check-cast v16, Landroid/net/NetworkInfo;

    .line 1747
    .restart local v16    # "netInfoP2p":Landroid/net/NetworkInfo;
    invoke-virtual/range {v16 .. v16}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/net/NetworkInfo$DetailedState;->toString()Ljava/lang/String;

    move-result-object v20

    .line 1748
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;

    move-result-object v23

    const-string v24, "FLAG_INTERFACE_P2P"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isNetworkConnected(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v17

    .line 1750
    goto/16 :goto_1

    .end local v16    # "netInfoP2p":Landroid/net/NetworkInfo;
    :cond_9
    const-string v23, "android.net.ethernet.ETH_STATE_CHANGED"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_a

    const-string v23, "android.net.wifi.WIFI_AP_STATE_CHANGED"

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 1752
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v17

    goto/16 :goto_1

    .line 1782
    .restart local v6    # "className":Ljava/lang/String;
    :cond_b
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "Nearby device is not top: SELF FINISH"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1784
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbbd

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1785
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbbd

    const-wide/16 v26, 0x7d0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1787
    .end local v6    # "className":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v23

    if-eqz v23, :cond_2

    .line 1788
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setDiscoveryInfo()V

    .line 1789
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->initSSDPSocket()V

    .line 1790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->announceDMS()V

    goto/16 :goto_0

    .line 1795
    .end local v8    # "connectivityManager":Landroid/net/ConnectivityManager;
    .end local v17    # "networkConnected":Z
    .end local v20    # "state":Ljava/lang/String;
    :cond_d
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v23

    const-string v24, "com.samsung.android.nearby.mediaserver.UPLOAD_ASK_OK"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_f

    .line 1796
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "UPLOAD_ASK_OK"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1798
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbbc

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1799
    const-string v23, "KEY"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1801
    .local v13, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 1802
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .line 1803
    .local v18, "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->allowUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 1804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1805
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v24, v0

    const v25, 0x7f040007

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getDeviceName()Ljava/lang/String;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeForgroundInfo(Ljava/lang/String;)V
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1808
    .end local v18    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :cond_e
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "UPLOAD_ASK_OK: UPLOAD REQUEST NOT FOUND"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1811
    .end local v13    # "key":Ljava/lang/String;
    :cond_f
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v23

    const-string v24, "com.samsung.android.nearby.mediaserver.UPLOAD_ASK_CANCEL"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 1812
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "UPLOAD_ASK_CANCEL"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbbc

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1815
    const-string v23, "KEY"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1817
    .restart local v13    # "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_11

    .line 1818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .line 1819
    .restart local v18    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 1820
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1822
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_10

    .line 1823
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2602(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto/16 :goto_0

    .line 1825
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbb9

    const-wide/16 v26, 0x3e8

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1828
    .end local v18    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :cond_11
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "UPLOAD_ASK_CANCEL: UPLOAD REQUEST NOT FOUND"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1831
    .end local v13    # "key":Ljava/lang/String;
    :cond_12
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v23

    const-string v24, "com.samsung.android.nearby.mediaserver.UPLOAD_ASK_TIME_OVER"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 1832
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "UPLOAD_ASK_TIME_OVER"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1834
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbbc

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1835
    const-string v23, "KEY"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1837
    .restart local v13    # "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 1838
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .line 1840
    .restart local v18    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->allowUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 1841
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v24, v0

    const v25, 0x7f040007

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getDeviceName()Ljava/lang/String;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeForgroundInfo(Ljava/lang/String;)V
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)V

    .line 1843
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mUploadList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_13

    .line 1846
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadPopup:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2602(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto/16 :goto_0

    .line 1848
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbb9

    const-wide/16 v26, 0x3e8

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-wide/from16 v2, v26

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 1854
    .end local v13    # "key":Ljava/lang/String;
    .end local v18    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    :cond_14
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v23

    const-string v24, "com.samsung.android.nearby.mediaserver.ACCESS_ASK_OK"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1c

    .line 1855
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "ACCESS_ASK_OK"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1856
    const-string v23, "KEY"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1859
    .restart local v13    # "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1a

    .line 1860
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .line 1861
    .local v7, "connectionInfo":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    new-instance v9, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getMacAddress()Ljava/lang/String;

    move-result-object v23

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v25, v0

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v26

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeUserAgentToName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static/range {v25 .. v26}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetPort()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1865
    .local v9, "deviceInfo":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    const/16 v23, 0x0

    const/16 v24, 0x11

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getMacAddress()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_15

    .line 1866
    const/16 v23, 0x0

    const/16 v24, 0x11

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->setMac(Ljava/lang/String;)V

    .line 1867
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "MAC INFO CHANGED: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v9}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getMac()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1870
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1872
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getAcceptedListSize()I
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)I

    move-result v23

    const/16 v24, 0xa

    move/from16 v0, v23

    move/from16 v1, v24

    if-lt v0, v1, :cond_17

    .line 1873
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "Accepted Device Count is 10!!"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1880
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Incoming connection from device "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " failed - maximum number of allowed devices reached"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V
    invoke-static/range {v23 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;ZLjava/lang/String;)V

    .line 1882
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v24, v0

    const v25, 0x7f04000e

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    const/16 v28, 0xa

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    invoke-static/range {v23 .. v25}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/widget/Toast;->show()V

    .line 1920
    .end local v7    # "connectionInfo":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    .end local v9    # "deviceInfo":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    :cond_16
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_1b

    .line 1921
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessPopup:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3002(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto/16 :goto_0

    .line 1886
    .restart local v7    # "connectionInfo":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    .restart local v9    # "deviceInfo":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->addAcceptedDevice(Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;)V

    .line 1887
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 1893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "Incoming connection from device "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, " succeeded"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V
    invoke-static/range {v23 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;ZLjava/lang/String;)V

    .line 1895
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const v24, 0x7f04000f

    const/16 v25, 0x1

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    new-instance v27, Ljava/lang/StringBuilder;

    invoke-direct/range {v27 .. v27}, Ljava/lang/StringBuilder;-><init>()V

    const-string v28, "\u202a"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual {v9}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    const-string v28, "\u202c"

    invoke-virtual/range {v27 .. v28}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-virtual/range {v23 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v22

    .line 1896
    .local v22, "toastMessage":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v23

    const-string v24, "iw"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_18

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v23

    const-string v24, "he"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_19

    .line 1897
    :cond_18
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "\u200f"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 1899
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;

    move-result-object v23

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/widget/Toast;->show()V

    .line 1904
    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v23

    const-string v24, "SEC_HHP_"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_16

    .line 1905
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "SEC_HHP_ announceDMS"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1906
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->announceDMS()V

    goto/16 :goto_2

    .line 1914
    .end local v7    # "connectionInfo":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    .end local v9    # "deviceInfo":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    .end local v22    # "toastMessage":Ljava/lang/String;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const-string v25, "Incoming connection from external entity failed - no matching MAC key"

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->auditLog(ZLjava/lang/String;)V
    invoke-static/range {v23 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;ZLjava/lang/String;)V

    .line 1916
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "ACCESS_ASK_OK No matching MAC Key: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1923
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3102(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1924
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbbb

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v14

    .line 1925
    .local v14, "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1927
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const-wide/16 v24, 0x1f4

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-virtual {v0, v14, v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1931
    .end local v13    # "key":Ljava/lang/String;
    .end local v14    # "msg":Landroid/os/Message;
    :cond_1c
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v23

    const-string v24, "com.samsung.android.nearby.mediaserver.ACCESS_ASK_CANCEL"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_20

    .line 1932
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "ACCESS_ASK_CANCEL"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1933
    const-string v23, "KEY"

    move-object/from16 v0, p2

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1936
    .restart local v13    # "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1e

    .line 1937
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .line 1938
    .restart local v7    # "connectionInfo":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    new-instance v9, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getMacAddress()Ljava/lang/String;

    move-result-object v23

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetHost()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v25, v0

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getUserAgent()Ljava/lang/String;

    move-result-object v26

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeUserAgentToName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static/range {v25 .. v26}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, ""

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetPort()I

    move-result v27

    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    move-object/from16 v3, v26

    invoke-direct {v9, v0, v1, v2, v3}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1942
    .restart local v9    # "deviceInfo":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    const/16 v23, 0x0

    const/16 v24, 0x11

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getMacAddress()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_1d

    .line 1943
    const/16 v23, 0x0

    const/16 v24, 0x11

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->setMac(Ljava/lang/String;)V

    .line 1944
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "MAC INFO CHANGED: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual {v9}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getMac()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1947
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1949
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v9}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->addRejectedDevice(Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;)V

    .line 1950
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v7}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->rejectConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 1951
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v24, v0

    const v25, 0x7f040010

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "\u202a"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v9}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, "\u202c"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->showToastByUser(Ljava/lang/String;I)V
    invoke-static/range {v23 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;I)V

    .line 1959
    .end local v7    # "connectionInfo":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    .end local v9    # "deviceInfo":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v23

    if-eqz v23, :cond_1f

    .line 1960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessPopup:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3002(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto/16 :goto_0

    .line 1955
    :cond_1e
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "ACCESS_ASK_CANCEL No matching Key"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1962
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3102(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1963
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbbb

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v14

    .line 1964
    .restart local v14    # "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1966
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const-wide/16 v24, 0x1f4

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-virtual {v0, v14, v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1968
    .end local v13    # "key":Ljava/lang/String;
    .end local v14    # "msg":Landroid/os/Message;
    :cond_20
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v23

    const-string v24, "com.samsung.android.nearby.mediaserver.ACCESS_ASK_AGAIN"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_21

    .line 1969
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x1

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAgain:Z
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3102(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1970
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "ACCESS_ASK_AGAIN"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1971
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const/16 v24, 0xbbb

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v14

    .line 1972
    .restart local v14    # "msg":Landroid/os/Message;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v23

    invoke-interface/range {v23 .. v23}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1974
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v23

    const-wide/16 v24, 0x1388

    move-object/from16 v0, v23

    move-wide/from16 v1, v24

    invoke-virtual {v0, v14, v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    .line 1978
    .end local v14    # "msg":Landroid/os/Message;
    :cond_21
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v23

    const-string v24, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_23

    .line 1979
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "LOCALE_CHANGED"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1981
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->isInitialStickyBroadcast()Z

    move-result v23

    if-eqz v23, :cond_22

    .line 1982
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "isInitialStickyBroadcast: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1987
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->changeForgroundInfo(Ljava/lang/String;)V
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2003
    :cond_23
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v23

    const-string v24, "com.samsung.android.nearby.mediaserver.START_NEARBY_SETTINGS"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_28

    .line 2004
    const/16 v21, 0x0

    .line 2005
    .local v21, "taskCount":I
    const-string v23, "activity"

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager;

    .line 2007
    .local v5, "activityManager":Landroid/app/ActivityManager;
    const/16 v23, 0x1

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v12

    .line 2008
    .local v12, "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    move/from16 v0, v21

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 2009
    .local v19, "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v6

    .line 2011
    .restart local v6    # "className":Ljava/lang/String;
    :goto_4
    const-string v23, "com.samsung.android.nearby.mediaserver"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_25

    .line 2012
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "### current task("

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "): "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2015
    add-int/lit8 v21, v21, 0x1

    .line 2016
    add-int/lit8 v23, v21, 0x1

    move/from16 v0, v23

    invoke-virtual {v5, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v12

    .line 2018
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v23

    move/from16 v0, v23

    move/from16 v1, v21

    if-le v0, v1, :cond_24

    .line 2019
    move/from16 v0, v21

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    .end local v19    # "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    check-cast v19, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 2020
    .restart local v19    # "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v6

    goto :goto_4

    .line 2022
    :cond_24
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "No more tasks"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2027
    :cond_25
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "###  class name: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, "/"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2030
    const-string v23, "com.android.settings.nearby.NearbySettings"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_26

    .line 2031
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "NearbySettings is top"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2032
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isTablet:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v23

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v23

    if-nez v23, :cond_2

    .line 2033
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "NEW: TABLET REFRESH"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2034
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startNearbyActivity()V
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    goto/16 :goto_0

    .line 2036
    :cond_26
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v23

    if-nez v23, :cond_27

    .line 2037
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "NEW: NEW_ACTIVITY"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startNearbyActivity()V
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    goto/16 :goto_0

    .line 2040
    :cond_27
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    const-string v26, "ScreenOn: "

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v26, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z
    invoke-static/range {v26 .. v26}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2042
    const-string v23, "com.android.settings.SubSettings"

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_2

    .line 2043
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "NEW: NOT SubSettings"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2044
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startNearbyActivity()V
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    goto/16 :goto_0

    .line 2048
    .end local v5    # "activityManager":Landroid/app/ActivityManager;
    .end local v6    # "className":Ljava/lang/String;
    .end local v12    # "info":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v19    # "runningTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v21    # "taskCount":I
    :cond_28
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v23

    const-string v24, "com.android.settings.DEVICE_NAME_CHANGED"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_29

    .line 2049
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "DEVICE_NAME_CHANGED"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2050
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    const-string v25, "device_name"

    invoke-static/range {v24 .. v25}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->checkDeviceName(Ljava/lang/String;)Ljava/lang/String;
    invoke-static/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2052
    .local v10, "deviceName":Ljava/lang/String;
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "setMediaServerName"

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-static {v0, v1, v10}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2054
    new-instance v23, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-direct/range {v23 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;)V

    const/16 v24, 0x1

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    aput-object v10, v24, v25

    invoke-virtual/range {v23 .. v24}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ChangeDeviceNameTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 2057
    .end local v10    # "deviceName":Ljava/lang/String;
    :cond_29
    invoke-static {}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isVZWFeature()Z

    move-result v23

    if-eqz v23, :cond_2a

    const-string v23, "android.intent.action.SETTINGS_SOFT_RESET"

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2a

    .line 2059
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "DEVICE_NAME_CHANGED - FinishService"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2060
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->finishService()V
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    goto/16 :goto_0

    .line 2063
    :cond_2a
    const-string v23, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_2b

    const-string v23, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 2065
    :cond_2b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mIBinder:Lcom/android/settings/nearby/IMediaServer$Stub;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/android/settings/nearby/IMediaServer$Stub;

    move-result-object v23

    if-eqz v23, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mIBinder:Lcom/android/settings/nearby/IMediaServer$Stub;
    invoke-static/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$4800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/android/settings/nearby/IMediaServer$Stub;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/android/settings/nearby/IMediaServer$Stub;->isServerStarted()Z

    move-result v23

    if-eqz v23, :cond_2

    .line 2066
    const-string v23, "(Service)MediaServerService: "

    const-string v24, "mBroadcastReceiver"

    const-string v25, "Personal page changed - restart"

    invoke-static/range {v23 .. v25}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2067
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v24, v0

    const v25, 0x7f040013

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v24

    const/16 v25, 0x1

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->showToastByUser(Ljava/lang/String;I)V
    invoke-static/range {v23 .. v25}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$3900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;I)V

    .line 2069
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$2;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->reannounceDMS()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
