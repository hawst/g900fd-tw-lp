.class Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$4;
.super Ljava/lang/Object;
.source "MediaServerAccessDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->onCreateDialog(I)Landroid/app/AlertDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 147
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bChecked:Z
    invoke-static {v1, v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->access$002(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;Z)Z

    .line 148
    const-string v1, "(Service)MediaServerAccessDialog: "

    const-string v2, "onCreateDialog"

    const-string v3, "bChecked:onClick ok: ACCESS_ASK_OK"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.nearby.mediaserver.ACCESS_ASK_OK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 151
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "KEY"

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    # getter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->mKey:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->access$100(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 152
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-virtual {v1, v0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->sendBroadcast(Landroid/content/Intent;)V

    .line 153
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$4;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-virtual {v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->popupDestroy()V

    .line 154
    return-void
.end method
