.class Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$5;
.super Landroid/os/Handler;
.source "MediaServerUploadDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 206
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 233
    :cond_0
    :goto_0
    return-void

    .line 210
    :pswitch_0
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "total"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 211
    .local v1, "total":I
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    iget-object v2, v2, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 213
    const/16 v2, 0x1e

    if-lt v1, v2, :cond_0

    .line 214
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "handleMessage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ASKING_TIME OVER BROADCAST"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.android.nearby.mediaserver.UPLOAD_ASK_TIME_OVER"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 217
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "KEY"

    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    # getter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mKey:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->access$000(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-virtual {v2, v0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->sendBroadcast(Landroid/content/Intent;)V

    .line 219
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->popupDestroy()V

    goto :goto_0

    .line 224
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "total":I
    :pswitch_1
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "handleMessage"

    const-string v4, "start delay false"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bStartDelay:Z
    invoke-static {v2, v3}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->access$102(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;Z)Z

    goto :goto_0

    .line 208
    :pswitch_data_0
    .packed-switch 0xbb9
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
