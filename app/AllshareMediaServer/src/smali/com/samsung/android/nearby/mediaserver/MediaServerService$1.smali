.class Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;
.super Lcom/android/settings/nearby/IMediaServer$Stub;
.source "MediaServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/MediaServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V
    .locals 0

    .prologue
    .line 1006
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-direct {p0}, Lcom/android/settings/nearby/IMediaServer$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public announceServer()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1160
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1161
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-virtual {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->announceDMS()V

    .line 1163
    :cond_0
    return-void
.end method

.method public getAcceptEntry()[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1190
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    const-string v2, "getAcceptEntry"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1192
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->getAcceptEntries()[Ljava/lang/String;

    move-result-object v0

    .line 1195
    :goto_0
    return-object v0

    .line 1194
    :cond_0
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    const-string v2, "getAcceptEntry - return null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1195
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAcceptEntryValue()[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1200
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    const-string v2, "getAcceptEntryValue"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1201
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1202
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->getAcceptEntryValues()[Ljava/lang/String;

    move-result-object v0

    .line 1205
    :goto_0
    return-object v0

    .line 1204
    :cond_0
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    const-string v2, "getAcceptEntryValue - return null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1205
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMediaServerName()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1090
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getFriendlyName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRejectList()[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1210
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    const-string v2, "getRejectList"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1211
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1212
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->getRejectEntries()[Ljava/lang/String;

    move-result-object v0

    .line 1215
    :goto_0
    return-object v0

    .line 1214
    :cond_0
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    const-string v2, "getRejectList - return null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRejectListValue()[Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1220
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    const-string v2, "getRejectListValue"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1222
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->getRejectEntryValues()[Ljava/lang/String;

    move-result-object v0

    .line 1225
    :goto_0
    return-object v0

    .line 1224
    :cond_0
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    const-string v2, "getRejectListValue - return null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSharedMediaType()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mSharedContentsFlag:I
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)I

    move-result v0

    return v0
.end method

.method public getUploadPath()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1148
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getUploadPath()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isContentUploadAllowed()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1143
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Z

    move-result v0

    return v0
.end method

.method public isServerStarted()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1010
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder : isServerStarted"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v0

    return v0
.end method

.method public pause()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v4, 0xbbd

    .line 1180
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    const-string v2, "pause: delayed finish"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1181
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z
    invoke-static {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1802(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1182
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1183
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1184
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1186
    :cond_0
    return-void
.end method

.method public reannounceServer()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1153
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1154
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-virtual {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->reannounceDMS()V

    .line 1156
    :cond_0
    return-void
.end method

.method public removeAcceptList(Ljava/lang/String;)V
    .locals 4
    .param p1, "values"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1230
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeAcceptList: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1231
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1232
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->removeAcceptList(Ljava/lang/String;)V

    .line 1234
    :cond_0
    return-void
.end method

.method public removeRejectList(Ljava/lang/String;)V
    .locals 4
    .param p1, "values"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1238
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "removeRejectList: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1239
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1240
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAccessDeviceList:Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->removeRejectList(Ljava/lang/String;)V

    .line 1242
    :cond_0
    return-void
.end method

.method public resume(Ljava/lang/String;)V
    .locals 4
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1167
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "resume: cancel delayed finish:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    const-string v0, "TYPE_NEARBY"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1170
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z
    invoke-static {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1802(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1175
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v0

    const/16 v1, 0xbbd

    invoke-virtual {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->removeMessages(I)V

    .line 1176
    return-void

    .line 1172
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isScreenOn:Z
    invoke-static {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1802(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto :goto_0
.end method

.method public setContentAccessAllowed(Ljava/lang/String;)V
    .locals 8
    .param p1, "flag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1114
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "mIBinder : setContentAccessAllowed"

    invoke-static {v3, v4, p1}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1116
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1117
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAllowed:Z
    invoke-static {v3, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1502(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1118
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-virtual {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->announceDMS()V

    .line 1119
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1120
    .local v1, "it":Landroid/content/Intent;
    const-string v3, "com.samsung.android.nearby.mediaserver.changeto.UPLOAD_ALLOWED_ALL"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1121
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-virtual {v3, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 1123
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1124
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 1125
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .line 1126
    .local v2, "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAddList:Ljava/util/LinkedHashMap;
    invoke-static {v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->eldest()Ljava/util/Map$Entry;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1127
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 1124
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1131
    .end local v0    # "i":I
    .end local v1    # "it":Landroid/content/Intent;
    .end local v2    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1132
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAccessAllowed:Z
    invoke-static {v3, v7}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1502(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1139
    :cond_1
    :goto_1
    return-void

    .line 1135
    :cond_2
    const-string v3, "(Service)MediaServerService: "

    const-string v4, "mIBinder : setContentAccessAllowed"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setContentAccessAllowed fails "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1137
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z
    invoke-static {v3, v7}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto :goto_1
.end method

.method public setContentUploadAllowed(Ljava/lang/String;)V
    .locals 5
    .param p1, "flag"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1095
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder : setContentUploadAllowed"

    invoke-static {v0, v1, p1}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1097
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1098
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z
    invoke-static {v0, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1099
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAskUpload:Z
    invoke-static {v0, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1402(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1111
    :goto_0
    return-void

    .line 1100
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1101
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z
    invoke-static {v0, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1102
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAskUpload:Z
    invoke-static {v0, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1402(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto :goto_0

    .line 1103
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1104
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z
    invoke-static {v0, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    .line 1105
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isAskUpload:Z
    invoke-static {v0, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1402(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto :goto_0

    .line 1107
    :cond_2
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder : setContentUploadAllowed"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HandleAllowUpload fails "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->isUploadable:Z
    invoke-static {v0, v4}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Z)Z

    goto :goto_0
.end method

.method public setMediaServerName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1072
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder : setMediaServerName"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1074
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getFriendlyName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1080
    .end local p1    # "name":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 1076
    .restart local p1    # "name":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->isServerStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1077
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v0

    const/16 v1, 0xbc3

    invoke-virtual {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessage(I)Z

    .line 1079
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->isServerStarted()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setFriendlyName(Ljava/lang/String;Z)Z

    .line 1080
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getFriendlyName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public setSharedMediaType(I)V
    .locals 3
    .param p1, "flag"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1066
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder : setSharedMediaType"

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->shareContents(I)V
    invoke-static {v0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;I)V

    .line 1068
    return-void
.end method

.method public setUploadPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1050
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "mIBinder : setUploadPath: PATH"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1051
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1053
    .local v0, "filepath":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1054
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1055
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "mIBinder : setUploadPath"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fail to create folder "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1060
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setUploadPath(Ljava/io/File;)Z

    .line 1061
    return-object p1
.end method

.method public startMediaServer()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1030
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "mIBinder : startMediaServer"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1031
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1033
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v1

    const/16 v2, 0xbbe

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 1035
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const-string v2, "wifi"

    invoke-virtual {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 1036
    .local v0, "manager":Landroid/net/wifi/WifiManager;
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 1037
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const-string v2, "AllShare(ASF-DMSLIB)"

    invoke-virtual {v0, v2}, Landroid/net/wifi/WifiManager;->createMulticastLock(Ljava/lang/String;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v2

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$702(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Landroid/net/wifi/WifiManager$MulticastLock;)Landroid/net/wifi/WifiManager$MulticastLock;

    .line 1040
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$MulticastLock;->isHeld()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1041
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager$MulticastLock;->acquire()V

    .line 1044
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startServer()V
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1000(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    .line 1046
    .end local v0    # "manager":Landroid/net/wifi/WifiManager;
    :cond_2
    return-void
.end method

.method public stopMediaServer()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1016
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "mIBinder : stopMediaServer"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1018
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1019
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$MulticastLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1020
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mMulticastLock:Landroid/net/wifi/WifiManager$MulticastLock;
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$700(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$MulticastLock;->release()V

    .line 1022
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopServer()V
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$800(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    .line 1024
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->stopForegroundCompat()V
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$900(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    .line 1026
    return-void
.end method
