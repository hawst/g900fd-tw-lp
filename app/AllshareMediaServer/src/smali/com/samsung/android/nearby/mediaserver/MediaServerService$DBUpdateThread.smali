.class Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;
.super Ljava/lang/Thread;
.source "MediaServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/MediaServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DBUpdateThread"
.end annotation


# instance fields
.field private changedFlag:I

.field private interrupt:Z

.field private sharedFlag:I

.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

.field private updating:Z


# direct methods
.method private constructor <init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 874
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 876
    iput-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->interrupt:Z

    .line 878
    iput-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->updating:Z

    .line 880
    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->sharedFlag:I

    .line 882
    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->changedFlag:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService;
    .param p2, "x1"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;

    .prologue
    .line 874
    invoke-direct {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    .prologue
    .line 874
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->isUpdating()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 874
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->setFlag(II)V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;
    .param p1, "x1"    # Z

    .prologue
    .line 874
    invoke-direct {p0, p1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->setInterrup(Z)V

    return-void
.end method

.method private isUpdating()Z
    .locals 1

    .prologue
    .line 894
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->updating:Z

    return v0
.end method

.method private setFlag(II)V
    .locals 0
    .param p1, "sharedFlag"    # I
    .param p2, "changedFlag"    # I

    .prologue
    .line 889
    iput p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->sharedFlag:I

    .line 890
    iput p2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->changedFlag:I

    .line 891
    return-void
.end method

.method private setInterrup(Z)V
    .locals 0
    .param p1, "interrupt"    # Z

    .prologue
    .line 885
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->interrupt:Z

    .line 886
    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/16 v9, 0xff

    const/high16 v8, 0xff0000

    const v7, 0xff00

    .line 900
    :try_start_0
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "Media DB update Thread RUN!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->updating:Z

    .line 903
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    if-nez v1, :cond_0

    .line 904
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "mServer is Null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    :goto_0
    return-void

    .line 908
    :cond_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 909
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v1

    if-nez v1, :cond_1

    .line 910
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "create mImageDir!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const v5, 0x7f040001

    invoke-virtual {v4, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;Ljava/lang/String;Landroid/net/Uri;)V

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$202(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 914
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v1

    if-nez v1, :cond_2

    .line 915
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "create mVideoDir!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const v5, 0x7f040002

    invoke-virtual {v4, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;Ljava/lang/String;Landroid/net/Uri;)V

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$302(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 919
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v1

    if-nez v1, :cond_3

    .line 920
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "create mAudioDir!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const v5, 0x7f040003

    invoke-virtual {v4, v5}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v6, "is_music = 1"

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    # setter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$402(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 925
    :cond_3
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 928
    :try_start_2
    iget v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->changedFlag:I

    and-int/lit16 v1, v1, 0xff

    if-ne v1, v9, :cond_4

    iget-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->interrupt:Z

    if-nez v1, :cond_4

    .line 930
    iget v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->sharedFlag:I

    and-int/lit16 v1, v1, 0xff

    if-ne v1, v9, :cond_8

    .line 932
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v1

    if-nez v1, :cond_4

    .line 933
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "@@EVENT_SERVER_DB_UPDATE -- ADD AUDIO"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 935
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->addMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    .line 948
    :cond_4
    :goto_1
    iget v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->changedFlag:I

    and-int/2addr v1, v7

    if-ne v1, v7, :cond_5

    iget-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->interrupt:Z

    if-nez v1, :cond_5

    .line 950
    iget v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->sharedFlag:I

    and-int/2addr v1, v7

    if-ne v1, v7, :cond_9

    .line 952
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v1

    if-nez v1, :cond_5

    .line 953
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "@@EVENT_SERVER_DB_UPDATE -- ADD VIDEO"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 955
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->addMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    .line 968
    :cond_5
    :goto_2
    iget v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->changedFlag:I

    and-int/2addr v1, v8

    if-ne v1, v8, :cond_6

    iget-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->interrupt:Z

    if-nez v1, :cond_6

    .line 970
    iget v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->sharedFlag:I

    and-int/2addr v1, v8

    if-ne v1, v8, :cond_a

    .line 972
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v1

    if-nez v1, :cond_6

    .line 973
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "@@EVENT_SERVER_DB_UPDATE -- ADD IMAGE"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->addMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    .line 988
    :cond_6
    :goto_3
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 989
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-virtual {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->byebyeDMS()V

    .line 990
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    invoke-virtual {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->announceDMS()V

    .line 992
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const v3, 0x7f040006

    invoke-virtual {v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->startForegroundCompat(Ljava/lang/String;Z)V
    invoke-static {v1, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$500(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Ljava/lang/String;Z)V

    .line 995
    :cond_7
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "MediaDB build finished"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->updating:Z

    .line 998
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServiceHandler:Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$600(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;

    move-result-object v1

    const/16 v2, 0xbc0

    invoke-virtual {v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$ServiceHandler;->sendEmptyMessage(I)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 1000
    :catch_0
    move-exception v0

    .line 1001
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 925
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1

    .line 939
    :cond_8
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 940
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "@@EVENT_SERVER_DB_UPDATE -- REMOVE AUDIO"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mAudioDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$400(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->removeMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    goto/16 :goto_1

    .line 959
    :cond_9
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 960
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "@@EVENT_SERVER_DB_UPDATE -- REMOVE VIDEO"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mVideoDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$300(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->removeMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    goto/16 :goto_2

    .line 979
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 980
    const-string v1, "(Service)MediaServerService: "

    const-string v2, "DBUpdateThread"

    const-string v3, "@@EVENT_SERVER_DB_UPDATE -- REMOVE IMAGE"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mImageDir:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->removeMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_3
.end method
