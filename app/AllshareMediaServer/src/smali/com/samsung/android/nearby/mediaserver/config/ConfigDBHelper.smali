.class public Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "ConfigDBHelper.java"


# static fields
.field public static DATABASE_CREATE:Ljava/lang/String;

.field public static DB_FILE_NAME:Ljava/lang/String;

.field public static DB_VERSION:I

.field public static KEY_ATTRIBUTE:Ljava/lang/String;

.field public static KEY_ID:Ljava/lang/String;

.field public static KEY_VALUE:Ljava/lang/String;

.field public static TABLE_NAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const/4 v0, 0x2

    sput v0, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->DB_VERSION:I

    .line 22
    const-string v0, "config.db"

    sput-object v0, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->DB_FILE_NAME:Ljava/lang/String;

    .line 24
    const-string v0, "config"

    sput-object v0, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->TABLE_NAME:Ljava/lang/String;

    .line 26
    const-string v0, "_id"

    sput-object v0, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->KEY_ID:Ljava/lang/String;

    .line 28
    const-string v0, "attribute"

    sput-object v0, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->KEY_ATTRIBUTE:Ljava/lang/String;

    .line 30
    const-string v0, "value"

    sput-object v0, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->KEY_VALUE:Ljava/lang/String;

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "create table IF NOT EXISTS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->TABLE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->KEY_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " integer primary key autoincrement, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->KEY_ATTRIBUTE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->KEY_VALUE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text);"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->DATABASE_CREATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    sget-object v0, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->DB_FILE_NAME:Ljava/lang/String;

    const/4 v1, 0x0

    sget v2, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->DB_VERSION:I

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 38
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 42
    sget-object v0, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->DATABASE_CREATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DROP TABLE IF EXISTS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->TABLE_NAME:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0, p1}, Lcom/samsung/android/nearby/mediaserver/config/ConfigDBHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 49
    return-void
.end method
