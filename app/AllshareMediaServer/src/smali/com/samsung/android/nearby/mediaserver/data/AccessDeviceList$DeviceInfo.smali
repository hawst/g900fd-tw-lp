.class public Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
.super Ljava/lang/Object;
.source "AccessDeviceList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeviceInfo"
.end annotation


# instance fields
.field private mIp:Ljava/lang/String;

.field private mMac:Ljava/lang/String;

.field private mName:Ljava/lang/String;

.field private mPort:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "mac"    # Ljava/lang/String;
    .param p2, "ip"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "port"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mMac:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mName:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mIp:Ljava/lang/String;

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mPort:Ljava/lang/String;

    .line 31
    invoke-virtual {p0, p1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->setMac(Ljava/lang/String;)V

    .line 32
    invoke-virtual {p0, p3}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->setName(Ljava/lang/String;)V

    .line 33
    invoke-virtual {p0, p2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->setIp(Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0, p4}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->setPort(Ljava/lang/String;)V

    .line 35
    return-void
.end method


# virtual methods
.method public getIp()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mIp:Ljava/lang/String;

    return-object v0
.end method

.method public getMac()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mMac:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mPort:Ljava/lang/String;

    return-object v0
.end method

.method public setIp(Ljava/lang/String;)V
    .locals 0
    .param p1, "mIp"    # Ljava/lang/String;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mIp:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public setMac(Ljava/lang/String;)V
    .locals 0
    .param p1, "mMac"    # Ljava/lang/String;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mMac:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "mName"    # Ljava/lang/String;

    .prologue
    .line 50
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mName:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public setPort(Ljava/lang/String;)V
    .locals 0
    .param p1, "mPort"    # Ljava/lang/String;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->mPort:Ljava/lang/String;

    .line 67
    return-void
.end method
