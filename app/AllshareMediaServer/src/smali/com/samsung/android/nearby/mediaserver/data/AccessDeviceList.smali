.class public Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;
.super Ljava/lang/Object;
.source "AccessDeviceList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    }
.end annotation


# static fields
.field private static final KEY_DEVICE_INFO:Ljava/lang/String; = "device_"

.field private static final KEY_DEVICE_LIST_NUM:Ljava/lang/String; = "list_num"

.field private static final KEY_SEPARATOR:Ljava/lang/String; = "*--*"

.field private static final PREFERENCE_ACCEPT_LIST:Ljava/lang/String; = "AllshareAcceptList"

.field private static final PREFERENCE_PACKAGE:Ljava/lang/String; = "com.samsung.android.nearby.mediaserver"

.field private static final PREFERENCE_REJECT_LIST:Ljava/lang/String; = "AllshareRejectList"

.field private static final TAG:Ljava/lang/String; = "AllShareDMS"

.field private static final TAGClass:Ljava/lang/String; = "(Service)AccessDeviceList: "


# instance fields
.field private mAcceptList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mRejectList:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mContext:Landroid/content/Context;

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    .line 94
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mContext:Landroid/content/Context;

    .line 95
    return-void
.end method

.method private broadcastRefreshToActivity(Z)V
    .locals 2
    .param p1, "accepted"    # Z

    .prologue
    .line 255
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.android.nearby.mediaserver.REFRESH_DEVICE_LIST"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 256
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "LIST"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 257
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 258
    return-void
.end method

.method private loadDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 17
    .param p2, "prefName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 313
    .local p1, "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;>;"
    const-string v13, "(Service)AccessDeviceList: "

    const-string v14, "loadDeviceList"

    move-object/from16 v0, p2

    invoke-static {v13, v14, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Ljava/util/HashMap;->clear()V

    .line 318
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mContext:Landroid/content/Context;

    const-string v14, "com.samsung.android.nearby.mediaserver"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v10

    .line 320
    .local v10, "other":Landroid/content/Context;
    const/4 v13, 0x4

    move-object/from16 v0, p2

    invoke-virtual {v10, v0, v13}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v12

    .line 322
    .local v12, "pref_device":Landroid/content/SharedPreferences;
    const-string v13, "list_num"

    const/4 v14, 0x0

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 324
    .local v1, "count":I
    if-eqz v1, :cond_7

    .line 325
    const-string v3, ""

    .line 326
    .local v3, "deviceTemp":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "n":I
    :goto_0
    if-ge v8, v1, :cond_7

    .line 327
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "device_"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    const-string v14, ""

    invoke-interface {v12, v13, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 328
    if-eqz v3, :cond_3

    const-string v13, ""

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 332
    const-string v7, ""

    .local v7, "mac":Ljava/lang/String;
    const-string v9, ""

    .local v9, "name":Ljava/lang/String;
    const-string v6, ""

    .local v6, "ip":Ljava/lang/String;
    const-string v11, ""

    .line 333
    .local v11, "port":Ljava/lang/String;
    const/4 v5, 0x0

    .line 335
    .local v5, "index":I
    const-string v13, "*--*"

    invoke-virtual {v3, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 337
    const-string v13, "*--*"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 338
    if-lez v5, :cond_4

    .line 339
    const/4 v13, 0x0

    invoke-virtual {v3, v13, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 340
    const-string v13, "*--*"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const-string v14, "*--*"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v13, v14

    invoke-virtual {v3, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 348
    :cond_0
    :goto_1
    if-eqz v3, :cond_1

    const-string v13, "*--*"

    invoke-virtual {v3, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 350
    const-string v13, "*--*"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 351
    if-lez v5, :cond_5

    .line 352
    const/4 v13, 0x0

    invoke-virtual {v3, v13, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 353
    const-string v13, "*--*"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const-string v14, "*--*"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v13, v14

    invoke-virtual {v3, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 361
    :cond_1
    :goto_2
    if-eqz v3, :cond_2

    const-string v13, "*--*"

    invoke-virtual {v3, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 363
    const-string v13, "*--*"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    .line 364
    if-lez v5, :cond_6

    .line 365
    const/4 v13, 0x0

    invoke-virtual {v3, v13, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    .line 366
    const-string v13, "*--*"

    invoke-virtual {v3, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v13

    const-string v14, "*--*"

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v13, v14

    invoke-virtual {v3, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 374
    :cond_2
    :goto_3
    move-object v11, v3

    .line 378
    const-string v13, "(Service)AccessDeviceList: "

    const-string v14, "loadDeviceList"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    new-instance v2, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    invoke-direct {v2, v7, v6, v9, v11}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    .local v2, "deviceInfo":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getMac()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    .end local v2    # "deviceInfo":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    .end local v5    # "index":I
    .end local v6    # "ip":Ljava/lang/String;
    .end local v7    # "mac":Ljava/lang/String;
    .end local v9    # "name":Ljava/lang/String;
    .end local v11    # "port":Ljava/lang/String;
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0

    .line 343
    .restart local v5    # "index":I
    .restart local v6    # "ip":Ljava/lang/String;
    .restart local v7    # "mac":Ljava/lang/String;
    .restart local v9    # "name":Ljava/lang/String;
    .restart local v11    # "port":Ljava/lang/String;
    :cond_4
    const-string v13, "(Service)AccessDeviceList: "

    const-string v14, "loadDeviceList"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "no mac info:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v13, "*--*"

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v3, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 356
    :cond_5
    const-string v13, "(Service)AccessDeviceList: "

    const-string v14, "loadDeviceList"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "no name info:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const-string v13, "*--*"

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v3, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 369
    :cond_6
    const-string v13, "(Service)AccessDeviceList: "

    const-string v14, "loadDeviceList"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "no userAgent info:"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v13, "*--*"

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v3, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto/16 :goto_3

    .line 386
    .end local v1    # "count":I
    .end local v3    # "deviceTemp":Ljava/lang/String;
    .end local v5    # "index":I
    .end local v6    # "ip":Ljava/lang/String;
    .end local v7    # "mac":Ljava/lang/String;
    .end local v8    # "n":I
    .end local v9    # "name":Ljava/lang/String;
    .end local v10    # "other":Landroid/content/Context;
    .end local v11    # "port":Ljava/lang/String;
    .end local v12    # "pref_device":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v4

    .line 387
    .local v4, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v13, "(Service)AccessDeviceList: "

    const-string v14, "loadDeviceList"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "loadDeviceList :"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    invoke-virtual {v4}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 390
    .end local v4    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_7
    return-void
.end method

.method private saveDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 10
    .param p2, "prefName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 285
    .local p1, "deviceList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;>;"
    const-string v7, "(Service)AccessDeviceList: "

    const-string v8, "saveDeviceList"

    const-string v9, ""

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    iget-object v7, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mContext:Landroid/content/Context;

    const/4 v8, 0x4

    invoke-virtual {v7, p2, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    .line 289
    .local v5, "preference":Landroid/content/SharedPreferences;
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 290
    .local v2, "edit":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 291
    const-string v7, "list_num"

    invoke-virtual {p1}, Ljava/util/HashMap;->size()I

    move-result v8

    invoke-interface {v2, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 293
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    .line 295
    .local v6, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;>;>;"
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 297
    .local v4, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;>;>;"
    const/4 v0, 0x0

    .line 298
    .local v0, "count":I
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 299
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    .line 300
    .local v3, "itor":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    const-string v1, ""

    .line 301
    .local v1, "deviceTemp":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getMac()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "*--*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getIp()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "*--*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "*--*"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getPort()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 303
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "device_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 304
    add-int/lit8 v0, v0, 0x1

    .line 305
    goto :goto_0

    .line 307
    .end local v1    # "deviceTemp":Ljava/lang/String;
    .end local v3    # "itor":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 309
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->loadPreference()V

    .line 310
    return-void
.end method


# virtual methods
.method public addAcceptedDevice(Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    .prologue
    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getMac()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    const-string v2, "AllshareAcceptList"

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->saveDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 190
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->broadcastRefreshToActivity(Z)V

    .line 191
    return-void
.end method

.method public addRejectedDevice(Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;)V
    .locals 3
    .param p1, "device"    # Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    .prologue
    .line 194
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getMac()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    invoke-virtual {v1, v0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    const-string v2, "AllshareRejectList"

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->saveDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 197
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->broadcastRefreshToActivity(Z)V

    .line 198
    return-void
.end method

.method public checkAcceptedList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 261
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 262
    const/4 v0, 0x1

    .line 264
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkRejectedList(Ljava/lang/String;)Z
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    const/4 v0, 0x1

    .line 271
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAcceptEntries()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 394
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 395
    .local v3, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;>;>;"
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    new-array v1, v4, [Ljava/lang/String;

    .line 397
    .local v1, "entry":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 398
    .local v0, "count":I
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 399
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    .line 400
    .local v2, "info":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    .line 401
    add-int/lit8 v0, v0, 0x1

    .line 402
    goto :goto_0

    .line 403
    .end local v2    # "info":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    :cond_0
    return-object v1
.end method

.method public getAcceptEntryValues()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 408
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 409
    .local v3, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;>;>;"
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    new-array v1, v4, [Ljava/lang/String;

    .line 411
    .local v1, "entry":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 412
    .local v0, "count":I
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 413
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    .line 414
    .local v2, "info":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getMac()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    .line 415
    add-int/lit8 v0, v0, 0x1

    .line 416
    goto :goto_0

    .line 417
    .end local v2    # "info":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    :cond_0
    return-object v1
.end method

.method public getRejectEntries()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 422
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 423
    .local v3, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;>;>;"
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    new-array v1, v4, [Ljava/lang/String;

    .line 425
    .local v1, "entry":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 426
    .local v0, "count":I
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 427
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    .line 428
    .local v2, "info":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    .line 429
    add-int/lit8 v0, v0, 0x1

    .line 430
    goto :goto_0

    .line 431
    .end local v2    # "info":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    :cond_0
    return-object v1
.end method

.method public getRejectEntryValues()[Ljava/lang/String;
    .locals 6

    .prologue
    .line 436
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 437
    .local v3, "keys":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;>;>;"
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    new-array v1, v4, [Ljava/lang/String;

    .line 439
    .local v1, "entry":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 440
    .local v0, "count":I
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 441
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;

    .line 442
    .local v2, "info":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getMac()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v0

    .line 443
    add-int/lit8 v0, v0, 0x1

    .line 444
    goto :goto_0

    .line 445
    .end local v2    # "info":Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList$DeviceInfo;
    :cond_0
    return-object v1
.end method

.method public loadPreference()V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    const-string v1, "AllshareAcceptList"

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->loadDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    const-string v1, "AllshareRejectList"

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->loadDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 282
    return-void
.end method

.method public removeAcceptList(Ljava/lang/String;)V
    .locals 8
    .param p1, "values"    # Ljava/lang/String;

    .prologue
    .line 201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .local v0, "deleteItemKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    const-string v4, "*--*"

    invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 205
    const/4 v4, 0x0

    const-string v5, "*--*"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 206
    .local v3, "temp":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const-string v5, "*--*"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const-string v6, "*--*"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    if-eq v4, v5, :cond_0

    .line 209
    const-string v4, "*--*"

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const-string v5, "*--*"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 212
    :goto_1
    const-string v4, "(Service)AccessDeviceList: "

    const-string v5, "removeAcceptList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Accept List delete:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_0
    const-string p1, ""

    goto :goto_1

    .line 215
    .end local v3    # "temp":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 216
    .local v2, "str":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 217
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 219
    :cond_2
    const-string v4, "(Service)AccessDeviceList: "

    const-string v5, "removeAcceptList"

    const-string v6, "removeAcceptList: No Matching Key"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 223
    .end local v2    # "str":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    const-string v5, "AllshareAcceptList"

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->saveDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 225
    return-void
.end method

.method public removeRejectList(Ljava/lang/String;)V
    .locals 8
    .param p1, "values"    # Ljava/lang/String;

    .prologue
    .line 228
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 230
    .local v0, "deleteItemKey":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    const-string v4, "*--*"

    invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 232
    const/4 v4, 0x0

    const-string v5, "*--*"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 233
    .local v3, "temp":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const-string v5, "*--*"

    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    const-string v6, "*--*"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    if-eq v4, v5, :cond_0

    .line 236
    const-string v4, "*--*"

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const-string v5, "*--*"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    .line 239
    :goto_1
    const-string v4, "(Service)AccessDeviceList: "

    const-string v5, "removeRejectList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Reject List delete:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 238
    :cond_0
    const-string p1, ""

    goto :goto_1

    .line 242
    .end local v3    # "temp":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 243
    .local v2, "str":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 244
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    invoke-virtual {v4, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 246
    :cond_2
    const-string v4, "(Service)AccessDeviceList: "

    const-string v5, "removeRejectList"

    const-string v6, "removeAcceptList: No Matching Key"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 250
    .end local v2    # "str":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    const-string v5, "AllshareRejectList"

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->saveDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public savePreference()V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mAcceptList:Ljava/util/HashMap;

    const-string v1, "AllshareAcceptList"

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->saveDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->mRejectList:Ljava/util/HashMap;

    const-string v1, "AllshareRejectList"

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/data/AccessDeviceList;->saveDeviceList(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 277
    return-void
.end method
