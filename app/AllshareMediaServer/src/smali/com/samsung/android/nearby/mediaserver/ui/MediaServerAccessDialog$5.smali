.class Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$5;
.super Landroid/os/Handler;
.source "MediaServerAccessDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 190
    const-string v3, "(Service)MediaServerAccessDialog: "

    const-string v4, "handleMessage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "(Check Ask Popup):"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->what:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :try_start_0
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 208
    :cond_0
    :goto_0
    return-void

    .line 194
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    const/4 v4, 0x1

    # setter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bAccessAgain:Z
    invoke-static {v3, v4}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->access$202(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;Z)Z

    .line 195
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    const-string v4, "keyguard"

    invoke-virtual {v3, v4}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    .line 196
    .local v2, "kManager":Landroid/app/KeyguardManager;
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    # getter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bChecked:Z
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->access$000(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 197
    const-string v3, "(Service)MediaServerAccessDialog: "

    const-string v4, "handleMessage"

    const-string v5, "Broadcast: ACCESS_ASK_AGAIN"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.samsung.android.nearby.mediaserver.ACCESS_ASK_AGAIN"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 199
    .local v1, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-virtual {v3, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->sendBroadcast(Landroid/content/Intent;)V

    .line 200
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$5;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-virtual {v3}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->popupDestroy()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 204
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "kManager":Landroid/app/KeyguardManager;
    :catch_0
    move-exception v0

    .line 205
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v3, "(Service)MediaServerAccessDialog: "

    const-string v4, "handleMessage"

    const-string v5, "SecurityException"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 206
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    goto :goto_0

    .line 192
    nop

    :pswitch_data_0
    .packed-switch 0xbb9
        :pswitch_0
    .end packed-switch
.end method
