.class Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;
.super Ljava/lang/Thread;
.source "MediaServerUploadDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProgressThread"
.end annotation


# static fields
.field static final STATE_DONE:I = 0x0

.field static final STATE_RUNNING:I = 0x1


# instance fields
.field handler:Landroid/os/Handler;

.field state:I

.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

.field total:I


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 248
    iput-object p2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->handler:Landroid/os/Handler;

    .line 249
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 256
    iput v6, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->state:I

    .line 257
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    # getter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mRequestedTime:I
    invoke-static {v3}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->access$200(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->total:I

    .line 258
    :goto_0
    iget v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->state:I

    if-ne v3, v6, :cond_0

    .line 259
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->handler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    .line 260
    .local v2, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 261
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "total"

    iget v4, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->total:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 262
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 263
    const/16 v3, 0xbba

    iput v3, v2, Landroid/os/Message;->what:I

    .line 264
    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 265
    iget v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->total:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->total:I

    .line 267
    const-wide/16 v4, 0x3e8

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 268
    :catch_0
    move-exception v1

    .line 269
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v3, "(Service)MediaServerUploadDialog: "

    const-string v4, "handleMessage"

    const-string v5, "InterruptedException"

    invoke-static {v3, v4, v5, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 273
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method public setState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 252
    iput p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->state:I

    .line 253
    return-void
.end method
