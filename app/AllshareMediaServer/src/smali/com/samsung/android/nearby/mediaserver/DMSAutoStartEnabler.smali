.class public Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;
.super Landroid/app/Service;
.source "DMSAutoStartEnabler.java"


# static fields
.field private static final LENGTH_DEVICE_NAME_MAX:I = 0x37

.field private static final TEXT_DEVICE_TYPE_MOBILE:Ljava/lang/String; = "[Mobile]"


# instance fields
.field private final ACCESS_ACCEPT_ALL:Ljava/lang/String;

.field private final DOWNLOAD_TO_PHONE:Ljava/lang/String;

.field private final HANDLER_DELAYED_START:I

.field private final PATH_DOWNLOAD_FOLDER_NAME:Ljava/lang/String;

.field private final TAGClass:Ljava/lang/String;

.field private final TIME_DELAYED_START:I

.field private final UPLOAD_ALWAYS_ACCEPT:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

.field private mServiceConn:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 24
    iput-object v1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mContext:Landroid/content/Context;

    .line 26
    const-string v0, "DMSAutoStartEnabler"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->TAGClass:Ljava/lang/String;

    .line 29
    const-string v0, "0"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->DOWNLOAD_TO_PHONE:Ljava/lang/String;

    .line 32
    const-string v0, "0"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->UPLOAD_ALWAYS_ACCEPT:Ljava/lang/String;

    .line 35
    const-string v0, "0"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->ACCESS_ACCEPT_ALL:Ljava/lang/String;

    .line 37
    const-string v0, "/Nearby"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->PATH_DOWNLOAD_FOLDER_NAME:Ljava/lang/String;

    .line 40
    iput-object v1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    .line 46
    const/16 v0, 0xbb8

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->HANDLER_DELAYED_START:I

    .line 48
    const/16 v0, 0x2710

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->TIME_DELAYED_START:I

    .line 66
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;-><init>(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mServiceConn:Landroid/content/ServiceConnection;

    .line 253
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$2;-><init>(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)Lcom/android/settings/nearby/IMediaServer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;Lcom/android/settings/nearby/IMediaServer;)Lcom/android/settings/nearby/IMediaServer;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;
    .param p1, "x1"    # Lcom/android/settings/nearby/IMediaServer;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->setDefaultValues()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->checkValues()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)Landroid/content/ServiceConnection;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mServiceConn:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method private checkDeviceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 210
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "checkDeviceName"

    invoke-static {v4, v5, p1}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    move-object v3, p1

    .line 215
    .local v3, "modifiedName":Ljava/lang/String;
    if-eqz p1, :cond_0

    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 216
    const-string v4, "/"

    const-string v5, ""

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    .local v0, "changedName":Ljava/lang/String;
    move-object p1, v0

    .line 218
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "checkDeviceName"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Remove all \'/\': "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    .end local v0    # "changedName":Ljava/lang/String;
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x37

    if-lt v4, v5, :cond_1

    .line 223
    const/4 v4, 0x0

    const/16 v5, 0x36

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 224
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "checkDeviceName"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "substring for MAX length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :cond_1
    :try_start_0
    const-string v1, "[Mobile]"

    .line 231
    .local v1, "deviceType":Ljava/lang/String;
    if-eqz p1, :cond_2

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 232
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 245
    .end local v1    # "deviceType":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 234
    .restart local v1    # "deviceType":Ljava/lang/String;
    :cond_2
    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 235
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 237
    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mContext:Landroid/content/Context;

    const v6, 0x7f04000a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 240
    .end local v1    # "deviceType":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 241
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "checkDeviceName"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private checkValues()V
    .locals 8

    .prologue
    .line 107
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "checkValues"

    const-string v6, ""

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getDeviceName()Ljava/lang/String;

    move-result-object v1

    .line 110
    .local v1, "deviceName":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getSharedContent()I

    move-result v3

    .line 113
    .local v3, "flag":I
    const/4 v0, 0x0

    .line 115
    .local v0, "changed":Z
    if-eqz v1, :cond_0

    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    invoke-interface {v4}, Lcom/android/settings/nearby/IMediaServer;->getMediaServerName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 116
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "checkValues"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "deviceName "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    invoke-interface {v4, v1}, Lcom/android/settings/nearby/IMediaServer;->setMediaServerName(Ljava/lang/String;)Ljava/lang/String;

    .line 118
    const/4 v0, 0x1

    .line 121
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    invoke-interface {v4}, Lcom/android/settings/nearby/IMediaServer;->getSharedMediaType()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 122
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "checkValues"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sharedContents: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    invoke-interface {v4, v3}, Lcom/android/settings/nearby/IMediaServer;->setSharedMediaType(I)V

    .line 124
    const/4 v0, 0x1

    .line 127
    :cond_1
    if-eqz v0, :cond_2

    .line 128
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "checkValues"

    const-string v6, "reannounce dms"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    invoke-interface {v4}, Lcom/android/settings/nearby/IMediaServer;->reannounceServer()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :cond_2
    :goto_0
    return-void

    .line 132
    :catch_0
    move-exception v2

    .line 133
    .local v2, "e":Landroid/os/RemoteException;
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "checkValues"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "RemoteException"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private getDownloadPath()Ljava/lang/String;
    .locals 5

    .prologue
    .line 161
    const-string v0, "0"

    .line 163
    .local v0, "flag":Ljava/lang/String;
    const/4 v1, 0x0

    .line 165
    .local v1, "path":Ljava/lang/String;
    const-string v2, "0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 166
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Nearby"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 167
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "NearbyDownloadTo"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    move-object v2, v1

    .line 173
    :goto_0
    return-object v2

    .line 169
    :cond_0
    const-string v2, "DMSAutoStartEnabler"

    const-string v3, "getDownloadPath"

    const-string v4, "HandleSetUploadPath has incorrect value"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getSharedContent()I
    .locals 5

    .prologue
    .line 177
    const/4 v1, 0x0

    .line 180
    .local v1, "flag":I
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dms_shared_contents"

    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 189
    :goto_0
    return v1

    .line 181
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    const-string v2, "DMSAutoStartEnabler"

    const-string v3, "getSharedContent"

    const-string v4, "SettingNotFoundException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 183
    invoke-virtual {v0}, Landroid/provider/Settings$SettingNotFoundException;->printStackTrace()V

    .line 185
    const v1, 0xffffff

    .line 186
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "dms_shared_contents"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method private initEnabler()V
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xbb8

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 104
    return-void
.end method

.method private setDefaultValues()V
    .locals 7

    .prologue
    .line 139
    const-string v4, "DMSAutoStartEnabler"

    const-string v5, "setDefaultValues"

    const-string v6, ""

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "deviceName":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getSharedContent()I

    move-result v3

    .line 143
    .local v3, "flag":I
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getDownloadPath()Ljava/lang/String;

    move-result-object v1

    .line 146
    .local v1, "downloadPath":Ljava/lang/String;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    invoke-interface {v4, v3}, Lcom/android/settings/nearby/IMediaServer;->setSharedMediaType(I)V

    .line 147
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    invoke-interface {v4, v0}, Lcom/android/settings/nearby/IMediaServer;->setMediaServerName(Ljava/lang/String;)Ljava/lang/String;

    .line 148
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    invoke-interface {v4, v1}, Lcom/android/settings/nearby/IMediaServer;->setUploadPath(Ljava/lang/String;)Ljava/lang/String;

    .line 149
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    const-string v5, "0"

    invoke-interface {v4, v5}, Lcom/android/settings/nearby/IMediaServer;->setContentUploadAllowed(Ljava/lang/String;)V

    .line 150
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    const-string v5, "0"

    invoke-interface {v4, v5}, Lcom/android/settings/nearby/IMediaServer;->setContentAccessAllowed(Ljava/lang/String;)V

    .line 153
    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;

    invoke-interface {v4}, Lcom/android/settings/nearby/IMediaServer;->startMediaServer()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :goto_0
    return-void

    .line 154
    :catch_0
    move-exception v2

    .line 155
    .local v2, "e":Landroid/os/RemoteException;
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getDeviceName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 193
    const-string v1, "DMSAutoStartEnabler"

    const-string v2, "getDeviceName"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "dms_device_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "deviceName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 198
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 199
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 203
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "dms_device_name"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 206
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->checkDeviceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 201
    :cond_1
    const-string v0, "AllshareMediaServer"

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 250
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 52
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 53
    const-string v0, "DMSAutoStartEnabler"

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mContext:Landroid/content/Context;

    .line 56
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->initEnabler()V

    .line 57
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "DMSAutoStartEnabler"

    const-string v1, "onDestroy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 64
    return-void
.end method
