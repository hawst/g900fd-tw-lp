.class public Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;
.super Landroid/app/Activity;
.source "MediaServerUploadDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;
    }
.end annotation


# static fields
.field private static final ASKING_TIME:I = 0x1e

.field private static final ASK_POPUP:I = 0xbb8

.field private static final EXTRA_TOTAL:Ljava/lang/String; = "total"

.field private static final SLEEP_TIME:I = 0x3e8

.field private static final START_DELAY:I = 0xbb9

.field private static final TAG:Ljava/lang/String; = "AllShareDMS"

.field private static final TAGClass:Ljava/lang/String; = "(Service)MediaServerUploadDialog: "

.field private static final UPDATE_TIME:I = 0xbba


# instance fields
.field private bRegister:Z

.field private bStartDelay:Z

.field mAskPopupDlg:Landroid/app/ProgressDialog;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mDeviceName:Ljava/lang/String;

.field final mHandler:Landroid/os/Handler;

.field private mKey:Ljava/lang/String;

.field private mRequestedTime:I

.field mWakeLock:Landroid/os/PowerManager$WakeLock;

.field progressThread:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 48
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bRegister:Z

    .line 50
    iput-boolean v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bStartDelay:Z

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mDeviceName:Ljava/lang/String;

    .line 54
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mKey:Ljava/lang/String;

    .line 56
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 58
    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    .line 62
    iput v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mRequestedTime:I

    .line 100
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$1;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 204
    new-instance v0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$5;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)V

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mHandler:Landroid/os/Handler;

    .line 236
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bStartDelay:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mRequestedTime:I

    return v0
.end method

.method private releaseBroadcastReceiver()V
    .locals 1

    .prologue
    .line 348
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bRegister:Z

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 350
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bRegister:Z

    .line 352
    :cond_0
    return-void
.end method

.method private releaseWakeLock()V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 333
    :cond_0
    return-void
.end method

.method private setBroadcastReceiver()V
    .locals 3

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bRegister:Z

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "com.samsung.android.nearby.mediaserver.REMOVE_UPLOAD_ASK_POPUP"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 338
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 340
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 343
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bRegister:Z

    .line 345
    :cond_0
    return-void
.end method

.method private setWakeLock()V
    .locals 6

    .prologue
    .line 316
    const-string v2, "keyguard"

    invoke-virtual {p0, v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    .line 317
    .local v0, "kManager":Landroid/app/KeyguardManager;
    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-nez v2, :cond_0

    .line 318
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "onResume"

    const-string v4, "Screen locked: awake from sleep mode"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 321
    .local v1, "pm":Landroid/os/PowerManager;
    const v2, 0x3000000a

    const-string v3, "AllShareDMS"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 323
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 325
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bStartDelay:Z

    .line 326
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xbb9

    const-wide/16 v4, 0xbb8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 328
    .end local v1    # "pm":Landroid/os/PowerManager;
    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    const-string v0, "(Service)MediaServerUploadDialog: "

    const-string v1, "onCreate"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 68
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->requestWindowFeature(I)Z

    .line 70
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "DEVICE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mDeviceName:Ljava/lang/String;

    .line 71
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "KEY"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mKey:Ljava/lang/String;

    .line 73
    return-void
.end method

.method protected bridge synthetic onCreateDialog(I)Landroid/app/Dialog;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->onCreateDialog(I)Landroid/app/ProgressDialog;

    move-result-object v0

    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/ProgressDialog;
    .locals 7
    .param p1, "id"    # I

    .prologue
    .line 134
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "onCreateDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    const/16 v2, 0xbb8

    if-ne p1, v2, :cond_1

    .line 137
    const/4 v1, 0x4

    .line 139
    .local v1, "theme":I
    :try_start_0
    invoke-static {}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isLightTheme()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 140
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "onCreateDialog"

    const-string v4, "set light theme"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const/4 v1, 0x5

    .line 144
    :cond_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    .line 145
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 146
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setIcon(I)V

    .line 147
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const v3, 0x7f04000a

    invoke-virtual {p0, v3}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 148
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const v3, 0x7f04000d

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mDeviceName:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/16 v6, 0x1e

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 150
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 151
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgressPercentFormat(Ljava/text/NumberFormat;)V

    .line 152
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const-string v3, "00:%02d"

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 153
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const/16 v3, 0x1e

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 155
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mKey:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mRequestedTime:I

    .line 157
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "onCreateDialog"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "UPLOAD DELAY REQUESTED TIME : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mRequestedTime:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    iget v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mRequestedTime:I

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 161
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const/4 v3, -0x1

    const v4, 0x7f040004

    invoke-virtual {p0, v4}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$2;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 172
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    const/4 v3, -0x2

    const v4, 0x7f040005

    invoke-virtual {p0, v4}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$3;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 184
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    new-instance v3, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$4;

    invoke-direct {v3, p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$4;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)V

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 195
    new-instance v2, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;

    iget-object v3, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mHandler:Landroid/os/Handler;

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;-><init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->progressThread:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;

    .line 196
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->progressThread:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->start()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    .end local v1    # "theme":I
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mAskPopupDlg:Landroid/app/ProgressDialog;

    return-object v2

    .line 197
    .restart local v1    # "theme":I
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "onCreateDialog"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 288
    const-string v1, "(Service)MediaServerUploadDialog: "

    const-string v2, "onDestroy"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 291
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->releaseBroadcastReceiver()V

    .line 293
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :goto_0
    return-void

    .line 296
    :catch_0
    move-exception v0

    .line 297
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerUploadDialog: "

    const-string v2, "onDestroy"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 92
    const/16 v0, 0x54

    if-eq p1, v0, :cond_0

    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 93
    :cond_0
    const-string v0, "(Service)MediaServerUploadDialog: "

    const-string v1, "onKeyDown"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Destroy Popup: KEYCODE : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->popupDestroy()V

    .line 95
    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPause()V
    .locals 3

    .prologue
    .line 278
    const-string v0, "(Service)MediaServerUploadDialog: "

    const-string v1, "onPause"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-boolean v0, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->bStartDelay:Z

    if-nez v0, :cond_0

    .line 281
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->popupDestroy()V

    .line 283
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 284
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 77
    const-string v1, "(Service)MediaServerUploadDialog: "

    const-string v2, "onResume"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 80
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->setBroadcastReceiver()V

    .line 82
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->setWakeLock()V

    .line 84
    const/16 v1, 0xbb8

    invoke-virtual {p0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->showDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :goto_0
    return-void

    .line 85
    :catch_0
    move-exception v0

    .line 86
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerUploadDialog: "

    const-string v2, "onResume"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method protected popupDestroy()V
    .locals 4

    .prologue
    .line 302
    const-string v1, "(Service)MediaServerUploadDialog: "

    const-string v2, "popupDestroy"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    const/16 v1, 0xbb8

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->removeDialog(I)V

    .line 305
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->progressThread:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$ProgressThread;->setState(I)V

    .line 307
    invoke-direct {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->releaseWakeLock()V

    .line 309
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :goto_0
    return-void

    .line 310
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "(Service)MediaServerUploadDialog: "

    const-string v2, "popupDestroy"

    const-string v3, "Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
