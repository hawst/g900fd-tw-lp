.class Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$1;
.super Landroid/content/BroadcastReceiver;
.source "MediaServerUploadDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 103
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "action":Ljava/lang/String;
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "onReceive"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :try_start_0
    const-string v2, "com.samsung.android.nearby.mediaserver.REMOVE_UPLOAD_ASK_POPUP"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 108
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->popupDestroy()V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    const-string v3, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$1;->isInitialStickyBroadcast()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 117
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isInitialStickyBroadcast: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 125
    :catch_0
    move-exception v1

    .line 126
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "(Service)MediaServerUploadDialog: "

    const-string v3, "onReceive"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 127
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->popupDestroy()V

    goto :goto_0

    .line 121
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 122
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerUploadDialog;->popupDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
