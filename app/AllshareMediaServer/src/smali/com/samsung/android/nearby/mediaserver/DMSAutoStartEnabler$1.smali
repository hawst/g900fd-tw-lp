.class Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;
.super Ljava/lang/Object;
.source "DMSAutoStartEnabler.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 75
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    invoke-static {p2}, Lcom/android/settings/nearby/IMediaServer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/settings/nearby/IMediaServer;

    move-result-object v2

    # setter for: Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;
    invoke-static {v1, v2}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->access$002(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;Lcom/android/settings/nearby/IMediaServer;)Lcom/android/settings/nearby/IMediaServer;

    .line 76
    const-string v1, "DMSAutoStartEnabler"

    const-string v2, "onServiceConnected"

    const-string v3, ""

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    # getter for: Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->access$000(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)Lcom/android/settings/nearby/IMediaServer;

    move-result-object v1

    if-nez v1, :cond_0

    .line 79
    const-string v1, "DMSAutoStartEnabler"

    const-string v2, "onServiceConnected"

    const-string v3, "mIMediaServer == null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :goto_0
    return-void

    .line 84
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    # getter for: Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->access$000(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)Lcom/android/settings/nearby/IMediaServer;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/settings/nearby/IMediaServer;->isServerStarted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 85
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    # invokes: Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->setDefaultValues()V
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->access$100(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    invoke-virtual {v1}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->stopSelf()V

    goto :goto_0

    .line 87
    :cond_1
    :try_start_1
    const-string v1, "DMSAutoStartEnabler"

    const-string v2, "onServiceConnected"

    const-string v3, "Server is started"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    # invokes: Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->checkValues()V
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->access$200(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "DMSAutoStartEnabler"

    const-string v2, "onServiceConnected"

    const-string v3, "RemoteException"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 92
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler$1;->this$0:Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->mIMediaServer:Lcom/android/settings/nearby/IMediaServer;
    invoke-static {v0, v1}, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;->access$002(Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;Lcom/android/settings/nearby/IMediaServer;)Lcom/android/settings/nearby/IMediaServer;

    .line 71
    return-void
.end method
