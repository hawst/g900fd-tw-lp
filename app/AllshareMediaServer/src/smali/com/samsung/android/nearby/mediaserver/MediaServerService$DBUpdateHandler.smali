.class Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;
.super Landroid/os/Handler;
.source "MediaServerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/MediaServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DBUpdateHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 1330
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    .line 1331
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1332
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1336
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "DBUpdateHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1338
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 1363
    :cond_0
    :goto_0
    return-void

    .line 1340
    :pswitch_0
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "DBUpdateHandler"

    const-string v2, "EVENT_UPDATE_DB_START"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1341
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    if-nez v0, :cond_1

    .line 1342
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "DBUpdateHandler"

    const-string v2, "DB UPDATE START!!!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1344
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    new-instance v1, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;-><init>(Lcom/samsung/android/nearby/mediaserver/MediaServerService;Lcom/samsung/android/nearby/mediaserver/MediaServerService$1;)V

    iput-object v1, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    .line 1345
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    iget-object v1, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mSharedContentsFlag:I
    invoke-static {v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$1200(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    # getter for: Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mChangedFlag:I
    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->access$2100(Lcom/samsung/android/nearby/mediaserver/MediaServerService;)I

    move-result v2

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->setFlag(II)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->access$2200(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;II)V

    .line 1346
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    invoke-virtual {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->run()V

    goto :goto_0

    .line 1347
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->isUpdating()Z
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->access$000(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1348
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "DBUpdateHandler"

    const-string v2, "NOW DB UPDATING..."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1353
    :pswitch_1
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "DBUpdateHandler"

    const-string v2, "EVENT_UPDATE_DB_STOP"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1354
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->isUpdating()Z
    invoke-static {v0}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->access$000(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1355
    const-string v0, "(Service)MediaServerService: "

    const-string v1, "DBUpdateHandler"

    const-string v2, "DB UPDATE START!!!"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1356
    iget-object v0, p0, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateHandler;->this$0:Lcom/samsung/android/nearby/mediaserver/MediaServerService;

    iget-object v0, v0, Lcom/samsung/android/nearby/mediaserver/MediaServerService;->mDBUpdateThread:Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->setInterrup(Z)V
    invoke-static {v0, v1}, Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;->access$2300(Lcom/samsung/android/nearby/mediaserver/MediaServerService$DBUpdateThread;Z)V

    goto/16 :goto_0

    .line 1338
    nop

    :pswitch_data_0
    .packed-switch 0xfa1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
