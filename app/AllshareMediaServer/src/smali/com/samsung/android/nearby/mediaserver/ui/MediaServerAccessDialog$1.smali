.class Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;
.super Landroid/content/BroadcastReceiver;
.source "MediaServerAccessDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;


# direct methods
.method constructor <init>(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 94
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "action":Ljava/lang/String;
    const-string v2, "(Service)MediaServerAccessDialog: "

    const-string v3, "onReceive"

    invoke-static {v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    :try_start_0
    const-string v2, "com.samsung.android.nearby.mediaserver.changeto.ACCESS_ALLOWED_ALL"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 99
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bChecked:Z
    invoke-static {v2, v3}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->access$002(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;Z)Z

    .line 100
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->popupDestroy()V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;->isInitialStickyBroadcast()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108
    const-string v2, "(Service)MediaServerAccessDialog: "

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isInitialStickyBroadcast: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v1

    .line 118
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "(Service)MediaServerAccessDialog: "

    const-string v3, "mBroadcastReceiver"

    const-string v4, "Exception"

    invoke-static {v2, v3, v4, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 119
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->popupDestroy()V

    goto :goto_0

    .line 112
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/nearby/mediaserver/DMSUtil;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->bChecked:Z
    invoke-static {v2, v3}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->access$002(Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;Z)Z

    .line 114
    iget-object v2, p0, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog$1;->this$0:Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;

    invoke-virtual {v2}, Lcom/samsung/android/nearby/mediaserver/ui/MediaServerAccessDialog;->popupDestroy()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
