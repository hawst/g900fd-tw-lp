.class public Lcom/samsung/android/nearby/mediaserver/DMSReceiver;
.super Landroid/content/BroadcastReceiver;
.source "DMSReceiver.java"


# instance fields
.field private final AutoStartModel:Ljava/lang/String;

.field private final SETTINGS_SYSTEM_SHUTDOWN:Ljava/lang/String;

.field private final TAGClass:Ljava/lang/String;

.field private final TIME_AUTO_DMS_CHECK_INTERVAL:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 18
    const-string v0, "DMSReceiver"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSReceiver;->TAGClass:Ljava/lang/String;

    .line 20
    const-string v0, "SM-V100T"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSReceiver;->AutoStartModel:Ljava/lang/String;

    .line 22
    const v0, 0x493e0

    iput v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSReceiver;->TIME_AUTO_DMS_CHECK_INTERVAL:I

    .line 24
    const-string v0, "shutdown_device"

    iput-object v0, p0, Lcom/samsung/android/nearby/mediaserver/DMSReceiver;->SETTINGS_SYSTEM_SHUTDOWN:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    .line 28
    const-string v2, "DMSReceiver"

    const-string v3, "onReceive"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 32
    .local v7, "action":Ljava/lang/String;
    if-nez v7, :cond_1

    .line 33
    const-string v1, "DMSReceiver"

    const-string v2, "onReceive"

    const-string v3, "action is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 37
    :cond_1
    const-string v2, "SM-V100T"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 38
    const-string v2, "DMSReceiver"

    const-string v3, "onReceive"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Auto Start Model:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 42
    new-instance v10, Landroid/content/Intent;

    const-string v2, "com.samsung.android.nearby.mediaServer.AUTO_START"

    invoke-direct {v10, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 43
    .local v10, "serviceIntent":Landroid/content/Intent;
    invoke-virtual {p1, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 46
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 47
    .local v8, "cal":Ljava/util/Calendar;
    new-instance v9, Landroid/content/Intent;

    const-class v2, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    invoke-direct {v9, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 49
    .local v9, "in":Landroid/content/Intent;
    const/high16 v2, 0x8000000

    invoke-static {p1, v1, v9, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    .line 51
    .local v6, "pi":Landroid/app/PendingIntent;
    const-string v2, "alarm"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 53
    .local v0, "alarms":Landroid/app/AlarmManager;
    invoke-virtual {v8}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x493e0

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 63
    .end local v0    # "alarms":Landroid/app/AlarmManager;
    .end local v6    # "pi":Landroid/app/PendingIntent;
    .end local v8    # "cal":Ljava/util/Calendar;
    .end local v9    # "in":Landroid/content/Intent;
    .end local v10    # "serviceIntent":Landroid/content/Intent;
    :cond_2
    :goto_1
    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 64
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "shutdown_device"

    const-string v3, "true"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 57
    :cond_3
    const-string v1, "com.samsung.android.nearby.mediaserver.AUTO_DMS_INFO_CHANGED"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 58
    new-instance v10, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/nearby/mediaserver/DMSAutoStartEnabler;

    invoke-direct {v10, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 59
    .restart local v10    # "serviceIntent":Landroid/content/Intent;
    invoke-virtual {p1, v10}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1

    .line 66
    .end local v10    # "serviceIntent":Landroid/content/Intent;
    :cond_4
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "shutdown_device"

    const-string v3, "false"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0
.end method
