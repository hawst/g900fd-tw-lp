.class public Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
.super Ljava/lang/Object;
.source "Service.java"


# static fields
.field private static final CONTROL_URL:Ljava/lang/String; = "controlURL"

.field public static final ELEM_NAME:Ljava/lang/String; = "service"

.field private static final EVENT_SUB_URL:Ljava/lang/String; = "eventSubURL"

.field private static final SCPDURL:Ljava/lang/String; = "SCPDURL"

.field private static final SERVICE_ID:Ljava/lang/String; = "serviceId"

.field private static final SERVICE_TYPE:Ljava/lang/String; = "serviceType"


# instance fields
.field private mRetryCount:I

.field private mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 1
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mTag:Ljava/lang/String;

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mRetryCount:I

    .line 148
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 149
    return-void
.end method

.method private getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getParentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 181
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 182
    const/4 v1, 0x0

    .line 183
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getParentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method private getNotifyServiceTypeNT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 691
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getNotifyServiceTypeUSN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 695
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRelativeURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 542
    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 543
    .local v3, "results":[Ljava/lang/String;
    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    .line 544
    .local v1, "l":Z
    if-ne v1, v5, :cond_0

    .line 557
    .end local p1    # "location":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 548
    .restart local p1    # "location":Ljava/lang/String;
    :cond_0
    array-length v4, v3

    if-ge v4, v5, :cond_1

    .line 549
    const-string p1, ""

    goto :goto_0

    .line 551
    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 552
    .local v2, "resultLocation":Ljava/lang/StringBuffer;
    const/4 v0, 0x3

    .local v0, "i":I
    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_2

    .line 553
    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    aget-object v5, v3, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 552
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 557
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    return-object v0
.end method

.method private getSCPDNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 25

    .prologue
    .line 445
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;

    move-result-object v4

    .line 446
    .local v4, "data":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->getSCPDNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v16

    .line 447
    .local v16, "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v16, :cond_0

    move-object/from16 v17, v16

    .line 538
    .end local v16    # "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .local v17, "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :goto_0
    return-object v17

    .line 450
    .end local v17    # "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .restart local v16    # "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_0
    const/4 v11, 0x0

    .line 451
    .local v11, "lastException":Ljava/lang/Exception;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDURL()Ljava/lang/String;

    move-result-object v18

    .line 454
    .local v18, "scpdURLStr":Ljava/lang/String;
    :try_start_0
    new-instance v19, Ljava/net/URL;

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 455
    .local v19, "scpdUrl":Ljava/net/URL;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDNode(Ljava/net/URL;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v16

    .line 520
    .end local v19    # "scpdUrl":Ljava/net/URL;
    :goto_1
    if-nez v16, :cond_2

    .line 523
    if-eqz v11, :cond_2

    instance-of v0, v11, Ljava/net/SocketTimeoutException;

    move/from16 v22, v0

    if-nez v22, :cond_1

    instance-of v0, v11, Ljava/net/SocketException;

    move/from16 v22, v0

    if-eqz v22, :cond_2

    .line 528
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v5

    .line 529
    .local v5, "device":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v5, :cond_2

    .line 530
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v22

    const/16 v23, 0x1

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setForceExpired(Z)V

    .line 531
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->requestDisposer()V

    .line 536
    .end local v5    # "device":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_2
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->setSCPDNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    move-object/from16 v17, v16

    .line 538
    .end local v16    # "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .restart local v17    # "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    goto :goto_0

    .line 456
    .end local v17    # "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .restart local v16    # "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catch_0
    move-exception v7

    .line 457
    .local v7, "e1":Ljava/lang/Exception;
    move-object v11, v7

    .line 458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mTag:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "getSCPDNode"

    const-string v24, "getSCPDNode1  Exception "

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 459
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v15

    .line 460
    .local v15, "rootDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    const/16 v21, 0x0

    .line 461
    .local v21, "urlBaseStr":Ljava/lang/String;
    if-eqz v15, :cond_4

    .line 462
    invoke-virtual {v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getURLBase()Ljava/lang/String;

    move-result-object v21

    .line 464
    if-eqz v21, :cond_3

    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    move-result v22

    if-gtz v22, :cond_4

    .line 465
    :cond_3
    const/16 v22, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v22

    const/16 v23, 0x2f

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_5

    .line 467
    invoke-virtual {v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v12

    .line 468
    .local v12, "location":Ljava/lang/String;
    invoke-static {v12}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getBaseUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 478
    .end local v12    # "location":Ljava/lang/String;
    :cond_4
    :goto_2
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 480
    .local v14, "newScpdURLStr":Ljava/lang/String;
    :try_start_1
    new-instance v13, Ljava/net/URL;

    invoke-direct {v13, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 481
    .local v13, "newScpdURL":Ljava/net/URL;
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDNode(Ljava/net/URL;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v16

    goto/16 :goto_1

    .line 473
    .end local v13    # "newScpdURL":Ljava/net/URL;
    .end local v14    # "newScpdURLStr":Ljava/lang/String;
    :cond_5
    invoke-virtual {v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->removeTillLastSlash(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 474
    .restart local v12    # "location":Ljava/lang/String;
    move-object/from16 v21, v12

    goto :goto_2

    .line 482
    .end local v12    # "location":Ljava/lang/String;
    .restart local v14    # "newScpdURLStr":Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 483
    .local v8, "e2":Ljava/lang/Exception;
    move-object v11, v8

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mTag:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "getSCPDNode"

    const-string v24, "getSCPDNode2 Exception "

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 485
    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getAbsoluteURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 487
    :try_start_2
    new-instance v13, Ljava/net/URL;

    invoke-direct {v13, v14}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 488
    .restart local v13    # "newScpdURL":Ljava/net/URL;
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDNode(Ljava/net/URL;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v16

    goto/16 :goto_1

    .line 489
    .end local v13    # "newScpdURL":Ljava/net/URL;
    :catch_2
    move-exception v9

    .line 490
    .local v9, "e3":Ljava/lang/Exception;
    move-object v11, v9

    .line 491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mTag:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "getSCPDNode"

    const-string v24, "getSCPDNode3 Exception"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 492
    if-eqz v15, :cond_6

    .line 493
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDescriptionFilePath()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 495
    :cond_6
    :try_start_3
    new-instance v22, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v0, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDNode(Ljava/io/File;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v16

    goto/16 :goto_1

    .line 496
    :catch_3
    move-exception v10

    .line 497
    .local v10, "e4":Ljava/lang/Exception;
    move-object v11, v10

    .line 502
    :try_start_4
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRelativeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 503
    .local v20, "serviceURL":Ljava/lang/String;
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 504
    .local v3, "baseScpdURLStr":Ljava/lang/String;
    new-instance v13, Ljava/net/URL;

    invoke-direct {v13, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 505
    .restart local v13    # "newScpdURL":Ljava/net/URL;
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDNode(Ljava/net/URL;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v16

    .line 506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mTag:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "getSCPDNode"

    const-string v24, "getSCPDNode3 - Exception"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2, v10}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_1

    .line 507
    .end local v3    # "baseScpdURLStr":Ljava/lang/String;
    .end local v13    # "newScpdURL":Ljava/net/URL;
    .end local v20    # "serviceURL":Ljava/lang/String;
    :catch_4
    move-exception v6

    .line 508
    .local v6, "e":Ljava/lang/Exception;
    move-object v11, v6

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mTag:Ljava/lang/String;

    move-object/from16 v22, v0

    const-string v23, "getSCPDNode"

    const-string v24, "getSCPDNode4 - Exception"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_1
.end method

.method private getSCPDNode(Ljava/io/File;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2
    .param p1, "scpdFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 404
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v0

    .line 405
    .local v0, "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/io/File;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    return-object v1
.end method

.method private getSCPDNode(Ljava/net/URL;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 3
    .param p1, "scpdUrl"    # Ljava/net/URL;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;,
            Ljava/net/SocketTimeoutException;,
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 393
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v0

    .line 395
    .local v0, "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    invoke-static {}, Landroid/net/Proxy;->getDefaultHost()Ljava/lang/String;

    move-result-object v1

    .line 397
    .local v1, "proxyHost":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 398
    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/net/URL;Z)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 400
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/net/URL;Z)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    goto :goto_0
.end method

.method private getServiceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;
    .locals 2

    .prologue
    .line 676
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 677
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;

    .line 678
    .local v1, "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;
    if-nez v1, :cond_0

    .line 679
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;

    .end local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;
    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;-><init>()V

    .line 680
    .restart local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 681
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->setNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 683
    :cond_0
    return-object v1
.end method

.method public static isServiceNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 172
    const-string v0, "service"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isURL(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "referenceUrl"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 241
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v2, v3

    .line 250
    :cond_1
    :goto_0
    return v2

    .line 243
    :cond_2
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 244
    .local v1, "ret":Z
    if-eq v1, v2, :cond_1

    .line 246
    invoke-static {p1, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->toRelativeURL(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "relativeRefUrl":Ljava/lang/String;
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 248
    if-eq v1, v2, :cond_1

    move v2, v3

    .line 250
    goto :goto_0
.end method

.method private notify(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;)Z
    .locals 5
    .param p1, "sub"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    .param p2, "stateTable"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;

    .prologue
    .line 945
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryHost()Ljava/lang/String;

    move-result-object v0

    .line 946
    .local v0, "host":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryPort()I

    move-result v2

    .line 949
    .local v2, "port":I
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;-><init>()V

    .line 950
    .local v1, "notifyReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;
    invoke-virtual {v1, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;)Z

    .line 952
    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->post(Ljava/lang/String;I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v3

    .line 953
    .local v3, "res":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->isSuccessful()Z

    move-result v4

    if-nez v4, :cond_0

    .line 954
    const/4 v4, 0x0

    .line 958
    :goto_0
    return v4

    .line 956
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->incrementNotifyCount()V

    .line 958
    const/4 v4, 0x1

    goto :goto_0
.end method

.method private notify(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;)Z
    .locals 8
    .param p1, "sub"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    .param p2, "stateVar"    # Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    .prologue
    const/4 v6, 0x0

    .line 834
    if-nez p1, :cond_1

    .line 853
    :cond_0
    :goto_0
    return v6

    .line 837
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v5

    .line 838
    .local v5, "varName":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getValue()Ljava/lang/String;

    move-result-object v4

    .line 840
    .local v4, "value":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryHost()Ljava/lang/String;

    move-result-object v0

    .line 841
    .local v0, "host":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryPort()I

    move-result v2

    .line 844
    .local v2, "port":I
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;-><init>()V

    .line 845
    .local v1, "notifyReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;
    invoke-virtual {v1, p1, v5, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;Ljava/lang/String;Ljava/lang/String;)Z

    .line 847
    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->post(Ljava/lang/String;I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v3

    .line 848
    .local v3, "res":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->isSuccessful()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 851
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->incrementNotifyCount()V

    .line 853
    const/4 v6, 0x1

    goto :goto_0
.end method

.method private removeTillLastSlash(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 409
    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 410
    .local v3, "results":[Ljava/lang/String;
    const-string v4, "/"

    invoke-virtual {p1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    .line 411
    .local v1, "l":Z
    if-ne v1, v5, :cond_0

    .line 424
    .end local p1    # "location":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 415
    .restart local p1    # "location":Ljava/lang/String;
    :cond_0
    array-length v4, v3

    if-ge v4, v5, :cond_1

    .line 416
    const-string p1, ""

    goto :goto_0

    .line 418
    :cond_1
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 419
    .local v2, "resultLocation":Ljava/lang/StringBuffer;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_2

    .line 420
    aget-object v4, v3, v0

    invoke-virtual {v2, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 419
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 424
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public addSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;)V
    .locals 2
    .param p1, "sub"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    .prologue
    .line 792
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSubscriberList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

    move-result-object v0

    .line 793
    .local v0, "subscribeList":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;
    monitor-enter v0

    .line 794
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;->add(Ljava/lang/Object;)Z

    .line 795
    monitor-exit v0

    .line 796
    return-void

    .line 795
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public announce(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V
    .locals 7
    .param p1, "ssdpSock"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .param p2, "bindAddr"    # Ljava/net/InetAddress;

    .prologue
    .line 704
    if-nez p1, :cond_0

    .line 730
    :goto_0
    return-void

    .line 708
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v2

    .line 709
    .local v2, "rootDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    const-string v1, ""

    .line 710
    .local v1, "devLocation":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 711
    invoke-virtual {v2, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocationURL(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v1

    .line 712
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getNotifyServiceTypeNT()Ljava/lang/String;

    move-result-object v3

    .line 713
    .local v3, "serviceNT":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getNotifyServiceTypeUSN()Ljava/lang/String;

    move-result-object v4

    .line 715
    .local v4, "serviceUSN":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 717
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;-><init>(Z)V

    .line 718
    .local v5, "ssdpReq":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getServerName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setServer(Ljava/lang/String;)V

    .line 720
    if-eqz v0, :cond_2

    .line 721
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLeaseTime()I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setLeaseTime(I)V

    .line 723
    :cond_2
    invoke-virtual {v5, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setLocation(Ljava/lang/String;)V

    .line 724
    const-string v6, "ssdp:alive"

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 725
    invoke-virtual {v5, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 726
    invoke-virtual {v5, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 728
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->notifyWait()V

    .line 729
    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z

    goto :goto_0
.end method

.method public byebye(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V
    .locals 4
    .param p1, "ssdpSock"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .param p2, "bindAddr"    # Ljava/net/InetAddress;

    .prologue
    .line 733
    if-nez p1, :cond_0

    .line 746
    :goto_0
    return-void

    .line 736
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getNotifyServiceTypeNT()Ljava/lang/String;

    move-result-object v0

    .line 737
    .local v0, "devNT":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getNotifyServiceTypeUSN()Ljava/lang/String;

    move-result-object v1

    .line 739
    .local v1, "devUSN":Ljava/lang/String;
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;-><init>(Z)V

    .line 740
    .local v2, "ssdpReq":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;
    const-string v3, "ssdp:byebye"

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 741
    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 742
    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 744
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->notifyWait()V

    .line 745
    invoke-virtual {p1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z

    goto :goto_0
.end method

.method public clearSID()V
    .locals 2

    .prologue
    .line 978
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setSID(Ljava/lang/String;)V

    .line 979
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setTimeout(J)V

    .line 980
    return-void
.end method

.method public clearSubscriber()V
    .locals 2

    .prologue
    .line 806
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSubscriberList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

    move-result-object v0

    .line 807
    .local v0, "subscribeList":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;
    monitor-enter v0

    .line 808
    :try_start_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;->clear()V

    .line 809
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 811
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->clearSID()V

    .line 812
    return-void

    .line 809
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public getAction(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .locals 7
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 597
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getActionList()Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;

    move-result-object v1

    .line 598
    .local v1, "actionList":Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;->size()I

    move-result v3

    .line 599
    .local v3, "nActions":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 600
    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;->getAction(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    move-result-object v0

    .line 601
    .local v0, "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v4

    .line 602
    .local v4, "name":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 599
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 604
    :cond_1
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 607
    .end local v0    # "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .end local v4    # "name":Ljava/lang/String;
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getActionList()Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;
    .locals 9

    .prologue
    .line 577
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;-><init>()V

    .line 578
    .local v1, "actionList":Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v6

    .line 579
    .local v6, "scdpNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v6, :cond_1

    .line 593
    :cond_0
    return-object v1

    .line 581
    :cond_1
    const-string v8, "actionList"

    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 582
    .local v2, "actionListNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v2, :cond_0

    .line 584
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v7

    .line 585
    .local v7, "serviceNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v4

    .line 586
    .local v4, "nNode":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 587
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    .line 588
    .local v5, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->isActionNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 586
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 590
    :cond_2
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    invoke-direct {v0, v7, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 591
    .local v0, "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getControlURL()Ljava/lang/String;
    .locals 7

    .prologue
    .line 285
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v1

    .line 286
    .local v1, "d":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-nez v1, :cond_0

    .line 287
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    const-string v6, "controlURL"

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 304
    :goto_0
    return-object v5

    .line 290
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getURLBase()Ljava/lang/String;

    move-result-object v0

    .line 291
    .local v0, "baseURL":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    .line 294
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 295
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    const-string v6, "controlURL"

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 296
    .local v3, "path":Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 298
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 299
    .local v2, "location":Ljava/lang/String;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 301
    .restart local v4    # "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    const-string v6, "controlURL"

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 302
    .restart local v3    # "path":Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    .line 304
    invoke-static {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 3

    .prologue
    .line 195
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    return-object v0
.end method

.method public getEventSubURL()Ljava/lang/String;
    .locals 7

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v1

    .line 321
    .local v1, "d":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-nez v1, :cond_0

    .line 322
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    const-string v6, "eventSubURL"

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 339
    :goto_0
    return-object v5

    .line 325
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getURLBase()Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "baseURL":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_1

    .line 329
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 330
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    const-string v6, "eventSubURL"

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 331
    .local v3, "path":Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 333
    .end local v3    # "path":Ljava/lang/String;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 334
    .local v2, "location":Ljava/lang/String;
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 336
    .restart local v4    # "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    const-string v6, "eventSubURL"

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 337
    .restart local v3    # "path":Ljava/lang/String;
    invoke-static {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    .line 339
    invoke-static {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public getRetryCount()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mRetryCount:I

    return v0
.end method

.method public getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    return-object v0
.end method

.method public getSCPDData()[B
    .locals 4

    .prologue
    .line 561
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 562
    .local v1, "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v1, :cond_0

    .line 563
    const/4 v2, 0x0

    new-array v2, v2, [B

    .line 569
    :goto_0
    return-object v2

    .line 565
    :cond_0
    const-string v0, ""

    .line 566
    .local v0, "desc":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "<?xml version=\"1.0\"?>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 567
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 568
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 569
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    goto :goto_0
.end method

.method public getSCPDURL()Ljava/lang/String;
    .locals 6

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 261
    .local v0, "d":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-nez v0, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v4

    const-string v5, "SCPDURL"

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 271
    :goto_0
    return-object v4

    .line 265
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 268
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v4

    const-string v5, "SCPDURL"

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "path":Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/samsung/android/allshare/stack/upnp/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    .line 271
    invoke-static {v3, v2}, Lcom/samsung/android/allshare/stack/upnp/util/UriBuilder;->buildUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public getSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 970
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->getSID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServiceID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "serviceId"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getServiceStateTable()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;
    .locals 9

    .prologue
    .line 615
    new-instance v6, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;

    invoke-direct {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;-><init>()V

    .line 616
    .local v6, "stateTable":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    .line 617
    .local v3, "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v3, :cond_1

    .line 634
    :cond_0
    return-object v6

    .line 621
    :cond_1
    const-string v8, "serviceStateTable"

    invoke-virtual {v3, v8}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v7

    .line 622
    .local v7, "stateTableNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v7, :cond_0

    .line 625
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v4

    .line 626
    .local v4, "serviceNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v1

    .line 627
    .local v1, "nNode":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 628
    invoke-virtual {v7, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 629
    .local v2, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->isStateVariableNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 627
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 631
    :cond_2
    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    invoke-direct {v5, v4, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 632
    .local v5, "serviceVar":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    invoke-virtual {v6, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getServiceType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 215
    .local v0, "serviceNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 216
    const-string v1, ""

    .line 217
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "serviceType"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getStateVariable(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 638
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceStateTable()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;

    move-result-object v1

    .line 639
    .local v1, "stateTable":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;->size()I

    move-result v2

    .line 640
    .local v2, "tableSize":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 641
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;->getStateVariable(I)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v3

    .line 642
    .local v3, "var":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v4

    .line 643
    .local v4, "varName":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 640
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 645
    :cond_1
    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 648
    .end local v3    # "var":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    .end local v4    # "varName":Ljava/lang/String;
    :goto_1
    return-object v3

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getSubscriber(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 815
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSubscriberList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

    move-result-object v3

    .line 817
    .local v3, "subList":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;
    monitor-enter v3

    .line 818
    :try_start_0
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;->size()I

    move-result v4

    .line 819
    .local v4, "subListCnt":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v4, :cond_2

    .line 820
    invoke-virtual {v3, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;->getSubscriber(I)Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    move-result-object v2

    .line 821
    .local v2, "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    if-nez v2, :cond_1

    .line 819
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 823
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getSID()Ljava/lang/String;

    move-result-object v1

    .line 824
    .local v1, "sid":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 826
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 827
    monitor-exit v3

    .line 830
    .end local v1    # "sid":Ljava/lang/String;
    .end local v2    # "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    :goto_1
    return-object v2

    .line 829
    :cond_2
    monitor-exit v3

    .line 830
    const/4 v2, 0x0

    goto :goto_1

    .line 829
    .end local v0    # "n":I
    .end local v4    # "subListCnt":I
    :catchall_0
    move-exception v5

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public getSubscriberList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;
    .locals 1

    .prologue
    .line 788
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->getSubscriberList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

    move-result-object v0

    return-object v0
.end method

.method public getTimeout()J
    .locals 2

    .prologue
    .line 995
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->getTimeout()J

    move-result-wide v0

    return-wide v0
.end method

.method public hasSID()Z
    .locals 1

    .prologue
    .line 983
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/util/StringUtil;->hasData(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasStateVariable(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 652
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getStateVariable(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public increaseRetryCount()V
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mRetryCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mRetryCount:I

    .line 133
    return-void
.end method

.method public isControlURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getControlURL()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isURL(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isEventSubURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getEventSubURL()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isURL(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSCPDURL(Ljava/lang/String;)Z
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDURL()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isURL(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isService(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 660
    if-nez p1, :cond_1

    .line 668
    :cond_0
    :goto_0
    return v0

    .line 664
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-ne v2, v1, :cond_2

    move v0, v1

    .line 665
    goto :goto_0

    .line 666
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    .line 667
    goto :goto_0
.end method

.method public isSubscribed()Z
    .locals 1

    .prologue
    .line 987
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->hasSID()Z

    move-result v0

    return v0
.end method

.method public loadSCPD(Ljava/io/InputStream;)Z
    .locals 4
    .param p1, "fis"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 382
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v1

    .line 383
    .local v1, "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 384
    .local v2, "scpdNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v2, :cond_0

    .line 385
    const/4 v3, 0x0

    .line 388
    :goto_0
    return v3

    .line 386
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;

    move-result-object v0

    .line 387
    .local v0, "data":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;
    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->setSCPDNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 388
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public notifyAllStateVariablesToSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;)V
    .locals 1
    .param p1, "sub"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    .prologue
    .line 888
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceStateTable()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;

    move-result-object v0

    .line 889
    .local v0, "stateTable":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;
    invoke-direct {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->notify(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;)Z

    .line 890
    return-void
.end method

.method public notifyState(Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;)V
    .locals 7
    .param p1, "stateVar"    # Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    .prologue
    .line 857
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSubscriberList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

    move-result-object v2

    .line 862
    .local v2, "subList":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;
    monitor-enter v2

    .line 863
    :try_start_0
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;->size()I

    move-result v3

    .line 864
    .local v3, "subListCnt":I
    new-array v4, v3, [Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    .line 865
    .local v4, "subs":[Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 866
    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;->getSubscriber(I)Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    move-result-object v5

    aput-object v5, v4, v0

    .line 865
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 867
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 868
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_2

    .line 869
    aget-object v1, v4, v0

    .line 870
    .local v1, "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->isExpired()Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 871
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->removeSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;)V

    .line 868
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 867
    .end local v0    # "n":I
    .end local v1    # "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    .end local v3    # "subListCnt":I
    .end local v4    # "subs":[Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .line 875
    .restart local v0    # "n":I
    .restart local v3    # "subListCnt":I
    .restart local v4    # "subs":[Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    :cond_2
    monitor-enter v2

    .line 876
    :try_start_2
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;->size()I

    move-result v3

    .line 877
    new-array v4, v3, [Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    .line 878
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v3, :cond_3

    .line 879
    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;->getSubscriber(I)Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    move-result-object v5

    aput-object v5, v4, v0

    .line 878
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 880
    :cond_3
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 881
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v3, :cond_4

    .line 882
    aget-object v1, v4, v0

    .line 883
    .restart local v1    # "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    invoke-direct {p0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->notify(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;)Z

    .line 881
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 880
    .end local v1    # "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    :catchall_1
    move-exception v5

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5

    .line 885
    :cond_4
    return-void
.end method

.method public removeSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;)V
    .locals 2
    .param p1, "sub"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSubscriberList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

    move-result-object v0

    .line 800
    .local v0, "subscribeList":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;
    monitor-enter v0

    .line 801
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;->remove(Ljava/lang/Object;)Z

    .line 802
    monitor-exit v0

    .line 803
    return-void

    .line 802
    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public resetRetryCount()V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->mRetryCount:I

    .line 137
    return-void
.end method

.method public serviceSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)Z
    .locals 7
    .param p1, "ssdpPacket"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    const/4 v5, 0x1

    .line 749
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getST()Ljava/lang/String;

    move-result-object v4

    .line 751
    .local v4, "ssdpST":Ljava/lang/String;
    if-nez v4, :cond_1

    .line 752
    const/4 v5, 0x0

    .line 767
    :cond_0
    :goto_0
    return v5

    .line 754
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 756
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getNotifyServiceTypeNT()Ljava/lang/String;

    move-result-object v1

    .line 757
    .local v1, "serviceNT":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getNotifyServiceTypeUSN()Ljava/lang/String;

    move-result-object v3

    .line 759
    .local v3, "serviceUSN":Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ST;->isAllDevice(Ljava/lang/String;)Z

    move-result v6

    if-ne v6, v5, :cond_2

    .line 760
    invoke-virtual {v0, p1, v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->postSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 761
    :cond_2
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ST;->isURNService(Ljava/lang/String;)Z

    move-result v6

    if-ne v6, v5, :cond_0

    .line 762
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v2

    .line 763
    .local v2, "serviceType":Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v5, :cond_0

    .line 764
    invoke-virtual {v0, p1, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->postSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    .prologue
    .line 1007
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getActionList()Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;

    move-result-object v1

    .line 1008
    .local v1, "actionList":Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;->size()I

    move-result v3

    .line 1009
    .local v3, "nActions":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 1010
    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;->getAction(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    move-result-object v0

    .line 1011
    .local v0, "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    .line 1009
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1013
    .end local v0    # "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    :cond_0
    return-void
.end method

.method public setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;)V
    .locals 4
    .param p1, "queryListener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

    .prologue
    .line 775
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceStateTable()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;

    move-result-object v1

    .line 776
    .local v1, "stateTable":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;->size()I

    move-result v2

    .line 777
    .local v2, "tableSize":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 778
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;->getStateVariable(I)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v3

    .line 779
    .local v3, "var":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    invoke-virtual {v3, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;)V

    .line 777
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 781
    .end local v3    # "var":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    :cond_0
    return-void
.end method

.method public setSID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 974
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->setSID(Ljava/lang/String;)V

    .line 975
    return-void
.end method

.method public setTimeout(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 999
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->setTimeout(J)V

    .line 1000
    return-void
.end method
