.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "ArgumentList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;",
        ">;"
    }
.end annotation


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "argumentList"

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 45
    return-void
.end method


# virtual methods
.method public getArgument(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    return-object v0
.end method

.method public getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->size()I

    move-result v3

    .line 57
    .local v3, "nArgs":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 58
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->getArgument(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 59
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "argName":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 57
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 62
    :cond_1
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 65
    .end local v0    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .end local v1    # "argName":Ljava/lang/String;
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setArgumentList(Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;)V
    .locals 6
    .param p1, "inArgList"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->size()I

    move-result v4

    .line 73
    .local v4, "nInArgs":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 74
    invoke-virtual {p1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->getArgument(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v1

    .line 75
    .local v1, "inArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-nez v1, :cond_1

    .line 73
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "inArgName":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 79
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    goto :goto_1

    .line 83
    .end local v0    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .end local v1    # "inArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .end local v2    # "inArgName":Ljava/lang/String;
    :cond_2
    return-void
.end method
