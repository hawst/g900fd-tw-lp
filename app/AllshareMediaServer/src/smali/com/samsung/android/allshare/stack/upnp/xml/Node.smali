.class public Lcom/samsung/android/allshare/stack/upnp/xml/Node;
.super Ljava/lang/Object;
.source "Node.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Node"


# instance fields
.field private mAttrList:Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;

.field private mName:Ljava/lang/String;

.field private mNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

.field private mParentNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private mUserData:Ljava/lang/Object;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mParentNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 101
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mName:Ljava/lang/String;

    .line 124
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mValue:Ljava/lang/String;

    .line 142
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mAttrList:Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;

    .line 236
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    .line 337
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mUserData:Ljava/lang/Object;

    .line 54
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 55
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setParentNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>()V

    .line 60
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setName(Ljava/lang/String;)V

    .line 61
    return-void
.end method


# virtual methods
.method public addAttribute(Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;)V
    .locals 1
    .param p1, "attr"    # Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mAttrList:Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;->add(Ljava/lang/Object;)Z

    .line 158
    return-void
.end method

.method public addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 1
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 278
    if-eqz p1, :cond_0

    .line 279
    invoke-virtual {p1, p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setParentNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 280
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->add(Ljava/lang/Object;)Z

    .line 282
    :cond_0
    return-void
.end method

.method public getAttribute(I)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mAttrList:Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;->getAttribute(I)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    move-result-object v0

    return-object v0
.end method

.method public getAttribute(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mAttrList:Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;->getAttribute(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    move-result-object v0

    return-object v0
.end method

.method public getAttributeIntegerValue(Ljava/lang/String;)I
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 215
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 217
    .local v1, "val":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 221
    :goto_0
    return v2

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Node"

    const-string v3, "getAttributeIntegerValue"

    const-string v4, "getAttributeIntegerValue Exception "

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 221
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getAttributeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 208
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttribute(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    move-result-object v0

    .line 209
    .local v0, "attr":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    if-eqz v0, :cond_0

    .line 210
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 211
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getIndentLevelString(I)Ljava/lang/String;
    .locals 3
    .param p1, "nIndentLevel"    # I

    .prologue
    .line 352
    new-array v0, p1, [C

    .line 353
    .local v0, "indentString":[C
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 354
    const/16 v2, 0x9

    aput-char v2, v0, v1

    .line 353
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 355
    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    return-object v2
.end method

.method public getNAttributes()I
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mAttrList:Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;->size()I

    move-result v0

    return v0
.end method

.method public getNNodes()I
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 243
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    return-object v0
.end method

.method public getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 247
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    return-object v0
.end method

.method public getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getEndsWith(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    return-object v0
.end method

.method public getNodeList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 251
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v8, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNodeList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 253
    .local v4, "nodeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/upnp/xml/Node;>;"
    if-nez v4, :cond_1

    .line 254
    const/4 v5, 0x0

    .line 270
    :cond_0
    return-object v5

    .line 256
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 258
    .local v5, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Bundle;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 259
    .local v3, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 260
    .local v6, "subtitleBundle":Landroid/os/Bundle;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 261
    .local v7, "value":Ljava/lang/String;
    const-string v8, "SUBTITLE_URI"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 263
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v2

    .line 264
    .local v2, "length":I
    add-int/lit8 v8, v2, -0x3

    invoke-virtual {v7, v8, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 265
    .local v0, "extension":Ljava/lang/String;
    const-string v8, "SUBTITLE_TYPE"

    invoke-virtual {v6, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getNodeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 327
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 328
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 330
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getParentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mParentNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2

    .prologue
    .line 88
    const/4 v1, 0x0

    .line 89
    .local v1, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getParentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 90
    .local v0, "parentNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :goto_0
    if-eqz v0, :cond_0

    .line 91
    move-object v1, v0

    .line 92
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getParentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    goto :goto_0

    .line 94
    :cond_0
    return-object v1
.end method

.method public getUserData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mUserData:Ljava/lang/Object;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public hasNodes()Z
    .locals 1

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v0

    if-lez v0, :cond_0

    .line 307
    const/4 v0, 0x1

    .line 308
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public output(Ljava/io/PrintWriter;IZ)V
    .locals 8
    .param p1, "ps"    # Ljava/io/PrintWriter;
    .param p2, "indentLevel"    # I
    .param p3, "hasChildNode"    # Z

    .prologue
    .line 367
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getIndentLevelString(I)Ljava/lang/String;

    move-result-object v1

    .line 369
    .local v1, "indentString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v4

    .line 370
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 372
    .local v5, "value":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->hasNodes()Z

    move-result v6

    if-eqz v6, :cond_0

    if-nez p3, :cond_3

    .line 373
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 374
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->outputAttributes(Ljava/io/PrintWriter;)V

    .line 376
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 378
    :cond_1
    const-string v6, " />"

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 397
    :goto_0
    return-void

    .line 380
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v5}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 386
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 387
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->outputAttributes(Ljava/io/PrintWriter;)V

    .line 388
    const/16 v6, 0x3e

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(C)V

    .line 390
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v3

    .line 391
    .local v3, "nChildNodes":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_1
    if-ge v2, v3, :cond_4

    .line 392
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 393
    .local v0, "cnode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    add-int/lit8 v6, p2, 0x1

    const/4 v7, 0x1

    invoke-virtual {v0, p1, v6, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->output(Ljava/io/PrintWriter;IZ)V

    .line 391
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 396
    .end local v0    # "cnode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public outputAttributes(Ljava/io/PrintWriter;)V
    .locals 5
    .param p1, "ps"    # Ljava/io/PrintWriter;

    .prologue
    .line 359
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNAttributes()I

    move-result v2

    .line 360
    .local v2, "nAttributes":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 361
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttribute(I)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    move-result-object v0

    .line 362
    .local v0, "attr":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 360
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 364
    .end local v0    # "attr":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    :cond_0
    return-void
.end method

.method public outputCamera(Ljava/io/PrintWriter;IZ)V
    .locals 8
    .param p1, "ps"    # Ljava/io/PrintWriter;
    .param p2, "indentLevel"    # I
    .param p3, "hasChildNode"    # Z

    .prologue
    .line 444
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getIndentLevelString(I)Ljava/lang/String;

    move-result-object v1

    .line 446
    .local v1, "indentString":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v4

    .line 447
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 449
    .local v5, "value":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->hasNodes()Z

    move-result v6

    if-eqz v6, :cond_0

    if-nez p3, :cond_3

    .line 450
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 451
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->outputAttributes(Ljava/io/PrintWriter;)V

    .line 453
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_2

    .line 455
    :cond_1
    const-string v6, " />"

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 474
    :goto_0
    return-void

    .line 457
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 463
    :cond_3
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "<"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 464
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->outputAttributes(Ljava/io/PrintWriter;)V

    .line 465
    const/16 v6, 0x3e

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(C)V

    .line 467
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v3

    .line 468
    .local v3, "nChildNodes":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_1
    if-ge v2, v3, :cond_4

    .line 469
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 470
    .local v0, "cnode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    add-int/lit8 v6, p2, 0x1

    const/4 v7, 0x1

    invoke-virtual {v0, p1, v6, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->outputCamera(Ljava/io/PrintWriter;IZ)V

    .line 468
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 473
    .end local v0    # "cnode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_4
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "</"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ">"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public removeAllAttribute()V
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mAttrList:Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;

    .line 187
    return-void
.end method

.method public removeNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z
    .locals 1
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 290
    if-nez p1, :cond_0

    .line 291
    const/4 v0, 0x0

    .line 293
    :goto_0
    return v0

    .line 292
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setParentNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 293
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->remove(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public setAttribute(Ljava/lang/String;I)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 204
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    return-void
.end method

.method public setAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 194
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttribute(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    move-result-object v0

    .line 195
    .local v0, "attr":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->setValue(Ljava/lang/String;)V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    .end local v0    # "attr":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    invoke-direct {v0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    .restart local v0    # "attr":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addAttribute(Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;)V

    goto :goto_0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mName:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mName:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public setNameSpace(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "ns"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 229
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xmlns:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    return-void
.end method

.method public setNode(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 316
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 317
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 318
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 324
    :goto_0
    return-void

    .line 321
    :cond_0
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .end local v0    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 322
    .restart local v0    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    goto :goto_0
.end method

.method public setParentNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mParentNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 77
    return-void
.end method

.method public setUserData(Ljava/lang/Object;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 340
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mUserData:Ljava/lang/Object;

    .line 341
    return-void
.end method

.method public setValue(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 131
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->mValue:Ljava/lang/String;

    .line 128
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 410
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->toString(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString(Z)Ljava/lang/String;
    .locals 3
    .param p1, "hasChildNode"    # Z

    .prologue
    .line 400
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 401
    .local v0, "byteOut":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 402
    .local v1, "pr":Ljava/io/PrintWriter;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->output(Ljava/io/PrintWriter;IZ)V

    .line 403
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    .line 404
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 405
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public toStringCamera()Ljava/lang/String;
    .locals 1

    .prologue
    .line 486
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->toStringCamera(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toStringCamera(Z)Ljava/lang/String;
    .locals 3
    .param p1, "hasChildNode"    # Z

    .prologue
    .line 477
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 478
    .local v0, "byteOut":Ljava/io/ByteArrayOutputStream;
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;)V

    .line 479
    .local v1, "pr":Ljava/io/PrintWriter;
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->outputCamera(Ljava/io/PrintWriter;IZ)V

    .line 480
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    .line 481
    invoke-virtual {v1}, Ljava/io/PrintWriter;->close()V

    .line 482
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
