.class Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;
.super Landroid/database/ContentObserver;
.source "MediaStoreDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaStoreContentObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)V
    .locals 1

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .line 96
    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getMediaUpdateHandler()Landroid/os/Handler;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 97
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 4
    .param p1, "selfChange"    # Z

    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 103
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getMediaUpdateHandler()Landroid/os/Handler;

    move-result-object v1

    # setter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$202(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;Landroid/os/Handler;)Landroid/os/Handler;

    .line 105
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$200(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isUpdateWorkerAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const-string v0, "MediaStoreDirectory"

    const-string v1, "onChange"

    const-string v2, "mUpdateDelayHandler post msg to mUpdateDelayTask."

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$200(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayTask:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$300(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 108
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$200(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayTask:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$300(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;

    move-result-object v1

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->UPDATE_DELAY_MILLIS:I
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$400()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 110
    :cond_0
    return-void
.end method
