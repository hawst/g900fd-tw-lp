.class Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty$ThumbnailCache;
.super Ljava/util/LinkedHashMap;
.source "ImageThumbnailProperty.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ThumbnailCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<",
        "Landroid/net/Uri;",
        "[B>;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x34f724e0ef382dd7L


# instance fields
.field mCapacity:I


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .param p1, "capacity"    # I

    .prologue
    .line 48
    const/high16 v0, 0x3f000000    # 0.5f

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty$ThumbnailCache;->mCapacity:I

    .line 49
    iput p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty$ThumbnailCache;->mCapacity:I

    .line 50
    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<",
            "Landroid/net/Uri;",
            "[B>;)Z"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "eldest":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Landroid/net/Uri;[B>;"
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty$ThumbnailCache;->size()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty$ThumbnailCache;->mCapacity:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
