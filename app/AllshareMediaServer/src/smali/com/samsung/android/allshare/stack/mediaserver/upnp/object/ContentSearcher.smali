.class public final Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;
.super Ljava/lang/Object;
.source "ContentSearcher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$1;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;
    }
.end annotation


# static fields
.field private static DQUOTE:C

.field private static ESCAPE_CHAR:C

.field private static PARENS:Ljava/lang/String;

.field public static final SEARCHCAP_LIST:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->SEARCHCAP_LIST:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;

    .line 37
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->SEARCHCAP_LIST:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;

    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/AllSearchCap;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/AllSearchCap;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->SEARCHCAP_LIST:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;

    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/IdSearchCap;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/IdSearchCap;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->SEARCHCAP_LIST:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;

    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/TitleSearchCap;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/TitleSearchCap;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->SEARCHCAP_LIST:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;

    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/UPnPClassSearchCap;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/UPnPClassSearchCap;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    const/16 v0, 0x5c

    sput-char v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->ESCAPE_CHAR:C

    .line 128
    const/16 v0, 0x22

    sput-char v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->DQUOTE:C

    .line 130
    const-string v0, "()"

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->PARENS:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method public static checkSearchCriteriaSyntax(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .param p0, "searchCriteria"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 419
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 421
    .local v1, "searchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    if-nez p0, :cond_1

    move-object v3, v1

    .line 452
    :cond_0
    :goto_0
    return-object v3

    .line 424
    :cond_1
    const-string v4, "@refID exists false"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v5, :cond_2

    .line 425
    const-string p0, "*"

    .line 427
    :cond_2
    const-string v4, "*"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eq v4, v5, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_4

    .line 429
    :cond_3
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;-><init>()V

    .line 430
    .local v0, "searchCri":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    const-string v3, "*"

    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setProperty(Ljava/lang/String;)V

    .line 431
    const-string v3, "exists"

    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setOperation(Ljava/lang/String;)V

    .line 432
    const-string v3, "true"

    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setValue(Ljava/lang/String;)V

    .line 433
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v3, v1

    .line 434
    goto :goto_0

    .line 438
    .end local v0    # "searchCri":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :cond_4
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->WCHARS:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->tokenizeSearchCriteria(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 439
    .local v2, "tokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    .line 443
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->createSearchList(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 444
    if-eqz v1, :cond_0

    .line 448
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->convertPostfix(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    .line 449
    if-eqz v1, :cond_0

    move-object v3, v1

    .line 452
    goto :goto_0
.end method

.method private static compare(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/util/ArrayList;)Z
    .locals 13
    .param p0, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "postfix":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    const/4 v11, 0x0

    const/4 v12, 0x1

    .line 351
    new-instance v9, Ljava/util/Stack;

    invoke-direct {v9}, Ljava/util/Stack;-><init>()V

    .line 352
    .local v9, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Ljava/lang/Boolean;>;"
    const/4 v7, 0x0

    .line 355
    .local v7, "result":Z
    :try_start_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    .line 356
    .local v3, "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isLogic()Z

    move-result v10

    if-eqz v10, :cond_4

    .line 357
    invoke-virtual {v9}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 358
    .local v0, "b1":Z
    invoke-virtual {v9}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 360
    .local v1, "b2":Z
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isLogicalAND()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 361
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    move v10, v12

    :goto_1
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 383
    .end local v0    # "b1":Z
    .end local v1    # "b2":Z
    .end local v3    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    .end local v5    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v4

    .line 384
    .local v4, "e":Ljava/lang/Exception;
    const/4 v7, 0x0

    .line 387
    .end local v4    # "e":Ljava/lang/Exception;
    :goto_2
    return v7

    .restart local v0    # "b1":Z
    .restart local v1    # "b2":Z
    .restart local v3    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    .restart local v5    # "i$":Ljava/util/Iterator;
    :cond_0
    move v10, v11

    .line 361
    goto :goto_1

    .line 363
    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    move v10, v12

    :goto_3
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    move v10, v11

    goto :goto_3

    .line 365
    .end local v0    # "b1":Z
    .end local v1    # "b2":Z
    :cond_4
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getProperty()Ljava/lang/String;

    move-result-object v6

    .line 366
    .local v6, "property":Ljava/lang/String;
    if-nez v6, :cond_5

    .line 367
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setResult(Z)V

    goto :goto_0

    .line 371
    :cond_5
    sget-object v10, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->SEARCHCAP_LIST:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;

    invoke-virtual {v10, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;->getSearchCap(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;

    move-result-object v8

    .line 372
    .local v8, "searchCap":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;
    if-nez v8, :cond_6

    .line 373
    const/4 v10, 0x1

    invoke-virtual {v3, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setResult(Z)V

    goto :goto_0

    .line 377
    :cond_6
    invoke-interface {v8, v3, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;->compare(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)Z

    move-result v2

    .line 378
    .local v2, "cmpResult":Z
    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setResult(Z)V

    .line 379
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 382
    .end local v2    # "cmpResult":Z
    .end local v3    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    .end local v6    # "property":Ljava/lang/String;
    .end local v8    # "searchCap":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;
    :cond_7
    invoke-virtual {v9}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    goto :goto_2
.end method

.method private static convertPostfix(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "infix":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    const/4 v6, 0x0

    .line 294
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 295
    .local v4, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    new-instance v5, Ljava/util/Stack;

    invoke-direct {v5}, Ljava/util/Stack;-><init>()V

    .line 298
    .local v5, "stack":Ljava/util/Stack;, "Ljava/util/Stack<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    :try_start_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    .line 299
    .local v1, "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isLeftParentheses()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 300
    invoke-virtual {v5, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 338
    .end local v1    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    .end local v3    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    move-object v4, v6

    .line 342
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v4    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    :cond_1
    :goto_1
    return-object v4

    .line 301
    .restart local v1    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isRightParentheses()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 303
    :goto_2
    invoke-virtual {v5}, Ljava/util/Stack;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    move-object v4, v6

    .line 304
    goto :goto_1

    .line 306
    :cond_3
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    .line 308
    .local v0, "cr":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isLeftParentheses()Z

    move-result v7

    if-nez v7, :cond_0

    .line 311
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 313
    .end local v0    # "cr":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :cond_4
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isLogic()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 314
    invoke-virtual {v5}, Ljava/util/Stack;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_5

    .line 315
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    .line 317
    .restart local v0    # "cr":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isLogic()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 318
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    .end local v0    # "cr":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :cond_5
    :goto_3
    invoke-virtual {v5, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 320
    .restart local v0    # "cr":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :cond_6
    invoke-virtual {v5, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 325
    .end local v0    # "cr":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :cond_7
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 335
    :cond_8
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    .end local v1    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :cond_9
    invoke-virtual {v5}, Ljava/util/Stack;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 330
    invoke-virtual {v5}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    .line 332
    .restart local v1    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isLeftParentheses()Z

    move-result v7

    if-nez v7, :cond_a

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isRightParentheses()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-eqz v7, :cond_8

    :cond_a
    move-object v4, v6

    .line 333
    goto :goto_1
.end method

.method private static createSearchList(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "tokenList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 456
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 458
    .local v6, "searchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-ge v1, v10, :cond_4

    .line 459
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 461
    .local v7, "token":Ljava/lang/String;
    const-string v10, "("

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    const-string v10, ")"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 462
    :cond_0
    new-instance v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    invoke-direct {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;-><init>()V

    .line 463
    .local v3, "param":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v3, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setParentheses(Ljava/lang/String;)V

    .line 464
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 458
    .end local v3    # "param":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 465
    :cond_1
    const-string v10, "and"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    const-string v10, "or"

    invoke-virtual {v7, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 466
    :cond_2
    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;-><init>()V

    .line 467
    .local v2, "logic":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v2, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setLogic(Ljava/lang/String;)V

    .line 468
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 470
    .end local v2    # "logic":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :cond_3
    move-object v4, v7

    .line 472
    .local v4, "prop":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    .line 473
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v1, v10, :cond_5

    move-object v6, v9

    .line 498
    .end local v4    # "prop":Ljava/lang/String;
    .end local v6    # "searchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    .end local v7    # "token":Ljava/lang/String;
    :cond_4
    :goto_2
    return-object v6

    .line 476
    .restart local v4    # "prop":Ljava/lang/String;
    .restart local v6    # "searchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    .restart local v7    # "token":Ljava/lang/String;
    :cond_5
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 478
    .local v0, "binOp":Ljava/lang/String;
    add-int/lit8 v1, v1, 0x1

    .line 479
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-lt v1, v10, :cond_6

    move-object v6, v9

    .line 480
    goto :goto_2

    .line 482
    :cond_6
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 484
    .local v8, "value":Ljava/lang/String;
    new-instance v5, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    invoke-direct {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;-><init>()V

    .line 485
    .local v5, "searchCri":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    const-string v10, "@refID"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    const-string v10, "exists"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    const-string v10, "false"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 486
    const-string v10, "*"

    invoke-virtual {v5, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setProperty(Ljava/lang/String;)V

    .line 487
    const-string v10, "exists"

    invoke-virtual {v5, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setOperation(Ljava/lang/String;)V

    .line 488
    const-string v10, "true"

    invoke-virtual {v5, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setValue(Ljava/lang/String;)V

    .line 494
    :goto_3
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 490
    :cond_7
    invoke-virtual {v5, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setProperty(Ljava/lang/String;)V

    .line 491
    invoke-virtual {v5, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setOperation(Ljava/lang/String;)V

    .line 492
    invoke-virtual {v5, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setValue(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static getSearchCapabilities()Ljava/lang/String;
    .locals 4

    .prologue
    .line 391
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 395
    .local v1, "sb":Ljava/lang/StringBuffer;
    sget-object v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->SEARCHCAP_LIST:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;

    .line 396
    .local v2, "searchCap":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;
    invoke-interface {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;->getPropertyName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 397
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 400
    .end local v2    # "searchCap":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 401
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    .line 403
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getSearchItemTypes(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    .local p0, "postfixSearchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v5, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;>;"
    const/4 v0, 0x0

    .line 85
    .local v0, "checkAudio":Z
    const/4 v2, 0x0

    .line 86
    .local v2, "checkVideo":Z
    const/4 v1, 0x0

    .line 88
    .local v1, "checkImage":Z
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    .line 89
    .local v3, "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isDerivedFrom()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 90
    if-nez v0, :cond_1

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getValue()Ljava/lang/String;

    move-result-object v6

    const-string v7, "audio"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 91
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;->AUDIO:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    const/4 v0, 0x1

    goto :goto_0

    .line 93
    :cond_1
    if-nez v2, :cond_2

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getValue()Ljava/lang/String;

    move-result-object v6

    const-string v7, "video"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 94
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;->VIDEO:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    const/4 v2, 0x1

    goto :goto_0

    .line 96
    :cond_2
    if-nez v1, :cond_0

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getValue()Ljava/lang/String;

    move-result-object v6

    const-string v7, "image"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 97
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;->IMAGE:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    const/4 v1, 0x1

    goto :goto_0

    .line 103
    .end local v3    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_4

    if-eqz v0, :cond_5

    if-eqz v2, :cond_5

    if-eqz v1, :cond_5

    .line 104
    :cond_4
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 105
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;->WILD_CARD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_5
    return-object v5
.end method

.method private static isSupported(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 407
    .local p0, "searchList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    .line 408
    .local v0, "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isLogic()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isParentheses()Z

    move-result v2

    if-nez v2, :cond_0

    .line 411
    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->SEARCHCAP_LIST:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getProperty()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;->getSearchCap(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;

    move-result-object v2

    if-nez v2, :cond_0

    .line 412
    const/4 v2, 0x0

    .line 415
    .end local v0    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static needSearch(Ljava/util/ArrayList;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "postfixSearchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    .line 70
    .local v0, "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isDerivedFrom()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, "video"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, "audio"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, "image"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 73
    :cond_1
    const/4 v2, 0x1

    .line 77
    .end local v0    # "criteria":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static search(Ljava/util/List;Ljava/util/ArrayList;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    .local p0, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    .local p1, "postfix":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->isSupported(Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 114
    :cond_0
    const/4 v2, 0x0

    .line 123
    :cond_1
    return-object v2

    .line 117
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .line 120
    .local v1, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    invoke-static {v1, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->compare(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/util/ArrayList;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 121
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static tokenizeSearchCriteria(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "searchCriteria"    # Ljava/lang/String;
    .param p1, "wChars"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v8, -0x1

    .line 145
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 146
    .local v2, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 147
    .local v3, "sBuffer":Ljava/lang/StringBuffer;
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->START:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 149
    .local v4, "state":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v1, v6, :cond_19

    .line 150
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 152
    .local v0, "c":C
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$1;->$SwitchMap$com$samsung$android$allshare$stack$mediaserver$upnp$object$ContentSearcher$TokenState:[I

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    move-object v2, v5

    .line 286
    .end local v0    # "c":C
    .end local v2    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    :goto_1
    return-object v2

    .line 155
    .restart local v0    # "c":C
    .restart local v2    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :pswitch_0
    const/4 v3, 0x0

    .line 156
    new-instance v3, Ljava/lang/StringBuffer;

    .end local v3    # "sBuffer":Ljava/lang/StringBuffer;
    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 158
    .restart local v3    # "sBuffer":Ljava/lang/StringBuffer;
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->ESCAPE_CHAR:C

    if-ne v0, v6, :cond_1

    move-object v2, v5

    .line 159
    goto :goto_1

    .line 160
    :cond_1
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->DQUOTE:C

    if-ne v0, v6, :cond_3

    .line 161
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->QUOTWORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 149
    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_4

    .line 163
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->START:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    goto :goto_2

    .line 164
    :cond_4
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->PARENS:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_6

    .line 165
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->PAREN:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 171
    const/16 v6, 0x29

    if-ne v0, v6, :cond_5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ne v1, v6, :cond_5

    .line 172
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 173
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    const/4 v3, 0x0

    goto :goto_2

    .line 176
    :cond_5
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 178
    :cond_6
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->WORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 179
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 185
    :pswitch_1
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->ESCAPE_CHAR:C

    if-ne v0, v6, :cond_7

    move-object v2, v5

    .line 186
    goto :goto_1

    .line 187
    :cond_7
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->DQUOTE:C

    if-ne v0, v6, :cond_8

    move-object v2, v5

    .line 188
    goto :goto_1

    .line 189
    :cond_8
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_9

    .line 190
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->START:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    goto :goto_2

    .line 191
    :cond_9
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->PARENS:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_a

    .line 192
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->PAREN:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 193
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 195
    :cond_a
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->WORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 196
    if-eqz v3, :cond_2

    .line 197
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 203
    :pswitch_2
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->ESCAPE_CHAR:C

    if-ne v0, v6, :cond_b

    move-object v2, v5

    .line 204
    goto/16 :goto_1

    .line 205
    :cond_b
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->DQUOTE:C

    if-ne v0, v6, :cond_c

    move-object v2, v5

    .line 206
    goto/16 :goto_1

    .line 207
    :cond_c
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_e

    .line 208
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->START:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 209
    if-eqz v2, :cond_d

    .line 210
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 213
    :cond_e
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->PARENS:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_f

    .line 214
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->PAREN:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 215
    if-eqz v2, :cond_2

    .line 216
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    const/4 v3, 0x0

    .line 218
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 221
    :cond_f
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->WORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 222
    if-eqz v3, :cond_2

    .line 223
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 229
    :pswitch_3
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->ESCAPE_CHAR:C

    if-ne v0, v6, :cond_10

    .line 230
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->ESCAPECHAR:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    goto/16 :goto_2

    .line 231
    :cond_10
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->DQUOTE:C

    if-ne v0, v6, :cond_12

    .line 232
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->START:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 233
    if-eqz v2, :cond_11

    if-eqz v3, :cond_11

    .line 234
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 236
    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 237
    :cond_12
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_13

    .line 238
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->QUOTWORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 239
    if-eqz v3, :cond_2

    .line 240
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 242
    :cond_13
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->PARENS:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_14

    .line 243
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->QUOTWORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 244
    if-eqz v3, :cond_2

    .line 245
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 248
    :cond_14
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->QUOTWORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 249
    if-eqz v3, :cond_2

    .line 250
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 256
    :pswitch_4
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->ESCAPE_CHAR:C

    if-ne v0, v6, :cond_15

    .line 257
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->QUOTWORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 258
    if-eqz v3, :cond_2

    .line 259
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 261
    :cond_15
    sget-char v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->DQUOTE:C

    if-ne v0, v6, :cond_16

    .line 262
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->QUOTWORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 263
    if-eqz v3, :cond_2

    .line 264
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto/16 :goto_2

    .line 266
    :cond_16
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_17

    move-object v2, v5

    .line 267
    goto/16 :goto_1

    .line 268
    :cond_17
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->PARENS:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-eq v6, v8, :cond_18

    move-object v2, v5

    .line 269
    goto/16 :goto_1

    :cond_18
    move-object v2, v5

    .line 271
    goto/16 :goto_1

    .line 280
    .end local v0    # "c":C
    :cond_19
    if-eqz v3, :cond_1a

    .line 281
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    :cond_1a
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-gtz v6, :cond_0

    move-object v2, v5

    .line 284
    goto/16 :goto_1

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
