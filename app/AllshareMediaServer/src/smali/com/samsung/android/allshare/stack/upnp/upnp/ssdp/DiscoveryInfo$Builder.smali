.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;
.super Ljava/lang/Object;
.source "DiscoveryInfo.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mLinkLocal:Z

.field private mLoopback:Z

.field private mMList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNI:Ljava/lang/String;

.field private mNIType:Ljava/lang/String;

.field private mSList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mNIType:Ljava/lang/String;

    .line 101
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mNI:Ljava/lang/String;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mMList:Ljava/util/ArrayList;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mSList:Ljava/util/ArrayList;

    .line 107
    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mLoopback:Z

    .line 109
    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mLinkLocal:Z

    return-void
.end method


# virtual methods
.method public addMulticastAddress(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;
    .locals 1
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mMList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mMList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    :cond_0
    return-object p0
.end method

.method public addSearchTarget(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;
    .locals 1
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mSList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mSList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    :cond_0
    return-object p0
.end method

.method public build()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 185
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;-><init>()V

    .line 186
    .local v0, "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mNIType:Ljava/lang/String;

    # setter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceType:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->access$002(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Ljava/lang/String;)Ljava/lang/String;

    .line 187
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mNI:Ljava/lang/String;

    # setter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->access$102(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Ljava/lang/String;)Ljava/lang/String;

    .line 188
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mMList:Ljava/util/ArrayList;

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    # setter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mMulticastAddressList:[Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->access$202(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;[Ljava/lang/String;)[Ljava/lang/String;

    .line 189
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mSList:Ljava/util/ArrayList;

    new-array v2, v3, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    # setter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSearchDeviceList:[Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->access$302(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;[Ljava/lang/String;)[Ljava/lang/String;

    .line 190
    iget-boolean v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mLinkLocal:Z

    # setter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSupportLinkLocal:Z
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->access$402(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Z)Z

    .line 191
    iget-boolean v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mLoopback:Z

    # setter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSupportLoopback:Z
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->access$502(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Z)Z

    .line 192
    return-object v0
.end method

.method public setNI(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;
    .locals 0
    .param p1, "ni"    # Ljava/lang/String;

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mNI:Ljava/lang/String;

    .line 113
    return-object p0
.end method

.method public setNIType(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 118
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mNIType:Ljava/lang/String;

    .line 119
    return-object p0
.end method

.method public setSupportLinkLocal(Z)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;
    .locals 0
    .param p1, "isSupport"    # Z

    .prologue
    .line 175
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mLinkLocal:Z

    .line 176
    return-object p0
.end method

.method public setSupportLoopback(Z)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;
    .locals 0
    .param p1, "isSupport"    # Z

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->mLoopback:Z

    .line 163
    return-object p0
.end method
