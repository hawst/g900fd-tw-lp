.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
.super Ljava/lang/Object;
.source "SearchCriteria.java"


# static fields
.field private static final AND:Ljava/lang/String; = "and"

.field private static final CONTAINS:Ljava/lang/String; = "contains"

.field private static final DERIVEDFROM:Ljava/lang/String; = "derivedfrom"

.field private static final DOESNOTCONTAIN:Ljava/lang/String; = "doesNotContain"

.field private static final EQ:Ljava/lang/String; = "="

.field private static final EXISTS:Ljava/lang/String; = "exists"

.field private static final FALSE:Ljava/lang/String; = "false"

.field private static final GE:Ljava/lang/String; = ">="

.field private static final GT:Ljava/lang/String; = ">"

.field private static final LE:Ljava/lang/String; = "<="

.field private static final LEFT_PAREN:Ljava/lang/String; = "("

.field private static final LT:Ljava/lang/String; = "<"

.field private static final NEQ:Ljava/lang/String; = "!="

.field private static final OR:Ljava/lang/String; = "or"

.field private static final RIGHT_PAREN:Ljava/lang/String; = ")"

.field private static final TRUE:Ljava/lang/String; = "true"

.field public static WCHARS:Ljava/lang/String;


# instance fields
.field private mIsLogic:Z

.field private mIsParentheses:Z

.field private mLogic:Ljava/lang/String;

.field private mOperation:Ljava/lang/String;

.field private mParentheses:Ljava/lang/String;

.field private mProperty:Ljava/lang/String;

.field private mResult:Z

.field private mValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    const-string v1, ""

    sput-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->WCHARS:Ljava/lang/String;

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 26
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 27
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 28
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 29
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 30
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->WCHARS:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mIsLogic:Z

    .line 227
    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mIsParentheses:Z

    .line 74
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setProperty(Ljava/lang/String;)V

    .line 75
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setOperation(Ljava/lang/String;)V

    .line 76
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setValue(Ljava/lang/String;)V

    .line 77
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setLogic(Ljava/lang/String;)V

    .line 78
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setParentheses(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;)V
    .locals 1
    .param p1, "searchCri"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mIsLogic:Z

    .line 227
    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mIsParentheses:Z

    .line 82
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getProperty()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setProperty(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getOperation()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setOperation(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setValue(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getLogic()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setLogic(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getParentheses()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setParentheses(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getResult()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setResult(Z)V

    .line 88
    return-void
.end method

.method private setIsLogic(Z)V
    .locals 0
    .param p1, "isLogic"    # Z

    .prologue
    .line 194
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mIsLogic:Z

    .line 195
    return-void
.end method

.method private setIsParentheses(Z)V
    .locals 0
    .param p1, "isParen"    # Z

    .prologue
    .line 230
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mIsParentheses:Z

    .line 231
    return-void
.end method


# virtual methods
.method public getLogic()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mLogic:Ljava/lang/String;

    return-object v0
.end method

.method public getOperation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    return-object v0
.end method

.method public getParentheses()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mParentheses:Ljava/lang/String;

    return-object v0
.end method

.method public getProperty()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mProperty:Ljava/lang/String;

    return-object v0
.end method

.method public getResult()Z
    .locals 1

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mResult:Z

    return v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public isContains()Z
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, "contains"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDerivedFrom()Z
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, "derivedfrom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDoesNotContain()Z
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, "doesNotContain"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEQ()Z
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isExists()Z
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, "exists"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFalseValue()Z
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mValue:Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGE()Z
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, ">="

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGT()Z
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLE()Z
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, "<="

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLT()Z
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLeftParentheses()Z
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mParentheses:Ljava/lang/String;

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLogic()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mIsLogic:Z

    return v0
.end method

.method public isLogicalAND()Z
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mLogic:Ljava/lang/String;

    const-string v1, "and"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLogicalOR()Z
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mLogic:Ljava/lang/String;

    const-string v1, "or"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNEQ()Z
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    const-string v1, "!="

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isParentheses()Z
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mIsParentheses:Z

    return v0
.end method

.method public isRightParentheses()Z
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mParentheses:Ljava/lang/String;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTrueValue()Z
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mValue:Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLogic(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mLogic:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mLogic:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 201
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setIsLogic(Z)V

    .line 204
    :goto_0
    return-void

    .line 203
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setIsLogic(Z)V

    goto :goto_0
.end method

.method public setOperation(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mOperation:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public setParentheses(Ljava/lang/String;)V
    .locals 1
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mParentheses:Ljava/lang/String;

    .line 236
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mParentheses:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_0

    .line 237
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setIsParentheses(Z)V

    .line 240
    :goto_0
    return-void

    .line 239
    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->setIsParentheses(Z)V

    goto :goto_0
.end method

.method public setProperty(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mProperty:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public setResult(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 265
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mResult:Z

    .line 266
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "val"    # Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->mValue:Ljava/lang/String;

    .line 171
    return-void
.end method
