.class public interface abstract Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;
.super Ljava/lang/Object;
.source "IUploadItemListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    }
.end annotation


# virtual methods
.method public abstract onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V
.end method

.method public abstract onFinished(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/io/File;)V
.end method

.method public abstract onRequested(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V
.end method
