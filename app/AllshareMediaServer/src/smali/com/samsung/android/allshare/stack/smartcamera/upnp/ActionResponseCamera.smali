.class public Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;
.source "ActionResponseCamera.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public setContentCamera(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 3
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 52
    const-string v0, ""

    .line 53
    .local v0, "conStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 54
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->toStringCamera()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;->setContent(Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public setResponseCamera(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V
    .locals 4
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 38
    const/16 v3, 0xc8

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;->setStatusCode(I)V

    .line 40
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 41
    .local v0, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-super {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->createResponseNode(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 42
    .local v2, "resNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 46
    .local v1, "envNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;->setContentCamera(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 47
    return-void
.end method
