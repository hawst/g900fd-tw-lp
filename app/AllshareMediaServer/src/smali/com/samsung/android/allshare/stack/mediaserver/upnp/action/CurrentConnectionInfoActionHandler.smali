.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CurrentConnectionInfoActionHandler;
.super Ljava/lang/Object;
.source "CurrentConnectionInfoActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# static fields
.field private static final AV_TRNSPORT_ID:Ljava/lang/String; = "AVTransportID"

.field private static final CONNECTION_ID:Ljava/lang/String; = "ConnectionID"

.field private static final DIRECTION:Ljava/lang/String; = "Direction"

.field private static final OUTPUT:Ljava/lang/String; = "Output"

.field private static final PEER_CONNECTION_ID:Ljava/lang/String; = "PeerConnectionID"

.field private static final PEER_CONNECTION_MANAGER:Ljava/lang/String; = "PeerConnectionManager"

.field private static final RCS_ID:Ljava/lang/String; = "RcsID"

.field private static final STATUS:Ljava/lang/String; = "Status"

.field private static final UNKNOWN:Ljava/lang/String; = "Unknown"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getCurrentConnectionInfo(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)Z
    .locals 10
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    const/4 v9, -0x1

    const/16 v8, 0x192

    const/4 v5, 0x0

    .line 49
    if-nez p1, :cond_0

    .line 100
    :goto_0
    return v5

    .line 52
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionRequest()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 54
    .local v1, "args":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 57
    invoke-virtual {p1, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 61
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    .line 64
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ConnectionID"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 65
    :cond_3
    invoke-virtual {p1, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 70
    .end local v0    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :cond_4
    const-string v6, "ConnectionID"

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 72
    .local v2, "argument":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-nez v2, :cond_5

    .line 73
    invoke-virtual {p1, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 77
    :cond_5
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getIntegerValue()I

    move-result v4

    .line 78
    .local v4, "id":I
    if-eqz v4, :cond_6

    .line 79
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid connection reference ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v8, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_0

    .line 82
    :cond_6
    const-string v5, "RcsID"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 83
    if-eqz v2, :cond_7

    .line 84
    invoke-virtual {v2, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 85
    :cond_7
    const-string v5, "AVTransportID"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 86
    if-eqz v2, :cond_8

    .line 87
    invoke-virtual {v2, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 88
    :cond_8
    const-string v5, "PeerConnectionManager"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 89
    if-eqz v2, :cond_9

    .line 90
    const-string v5, ""

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 91
    :cond_9
    const-string v5, "PeerConnectionID"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 92
    if-eqz v2, :cond_a

    .line 93
    invoke-virtual {v2, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 94
    :cond_a
    const-string v5, "Direction"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 95
    if-eqz v2, :cond_b

    .line 96
    const-string v5, "Output"

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 97
    :cond_b
    const-string v5, "Status"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 98
    if-eqz v2, :cond_c

    .line 99
    const-string v5, "Unknown"

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 100
    :cond_c
    const/4 v5, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 1
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CurrentConnectionInfoActionHandler;->getCurrentConnectionInfo(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)Z

    move-result v0

    return v0
.end method
