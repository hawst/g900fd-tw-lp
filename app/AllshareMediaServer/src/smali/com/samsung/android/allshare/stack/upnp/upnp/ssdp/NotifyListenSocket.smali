.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;
.source "NotifyListenSocket.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "NotifyListenSocket"


# instance fields
.field private mDeviceNotifyThread:Ljava/lang/Thread;

.field private mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

.field private mNotifyListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

.field private mSSDPJoinAddress:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;-><init>()V

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mSSDPJoinAddress:Ljava/lang/String;

    .line 29
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mNotifyListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .line 31
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mDeviceNotifyThread:Ljava/lang/Thread;

    .line 116
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    return-void
.end method


# virtual methods
.method public getDiscoveryInfo()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    return-object v0
.end method

.method protected declared-synchronized notifyEvent(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mNotifyListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mNotifyListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;->onSSDPReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    :cond_0
    monitor-exit p0

    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public open(Ljava/lang/String;ILjava/net/InetAddress;)Z
    .locals 3
    .param p1, "joinAddress"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "bindAddr"    # Ljava/net/InetAddress;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mSSDPJoinAddress:Ljava/lang/String;

    .line 49
    const-string v0, "NotifyListenSocket"

    const-string v1, "open"

    const-string v2, "Open Discovery Listen: "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->open(Ljava/lang/String;ILjava/net/InetAddress;)Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 71
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 72
    .local v1, "thisThread":Ljava/lang/Thread;
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 75
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mDeviceNotifyThread:Ljava/lang/Thread;

    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mDeviceNotifyThread:Ljava/lang/Thread;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->receive()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 78
    .local v0, "packet":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    if-eqz v0, :cond_0

    .line 84
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getHost()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    .line 85
    const-string v2, "NotifyListenSocket"

    const-string v3, "run"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid SSDP HOST: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getHost()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->getNetworkInterface()Ljava/net/NetworkInterface;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    .line 90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->setTimeStamp(J)V

    .line 92
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->notifyEvent(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    goto :goto_0

    .line 94
    .end local v0    # "packet":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    :cond_2
    return-void
.end method

.method public setDiscoveryInfo(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 120
    return-void
.end method

.method public declared-synchronized setNotifiyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .prologue
    .line 38
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mNotifyListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 39
    monitor-exit p0

    return-void

    .line 38
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 97
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mDeviceNotifyThread:Ljava/lang/Thread;

    .line 98
    const-string v0, "NotifyListenSocket"

    const-string v1, "start"

    const-string v2, "Start Discovery Listen: "

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mDeviceNotifyThread:Ljava/lang/Thread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Discovery Listen"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mBindAddress:Ljava/net/InetAddress;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mSSDPJoinAddress:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mDeviceNotifyThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 104
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->mDeviceNotifyThread:Ljava/lang/Thread;

    .line 109
    const-string v0, "NotifyListenSocket"

    const-string v1, "stop"

    const-string v2, "Call: HTTPMUSocket.close()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->close()Z

    .line 111
    return-void
.end method
