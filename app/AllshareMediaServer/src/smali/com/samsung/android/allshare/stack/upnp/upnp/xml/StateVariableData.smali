.class public Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;
.source "StateVariableData.java"


# instance fields
.field private mQueryListener:Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

.field private mQueryRes:Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;-><init>()V

    .line 39
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->mValue:Ljava/lang/String;

    .line 53
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->mQueryListener:Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

    .line 67
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->mQueryRes:Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;

    .line 33
    return-void
.end method


# virtual methods
.method public getQueryListener()Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->mQueryListener:Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

    return-object v0
.end method

.method public getQueryResponse()Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->mQueryRes:Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;)V
    .locals 0
    .param p1, "queryListener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->mQueryListener:Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

    .line 61
    return-void
.end method

.method public setQueryResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;)V
    .locals 0
    .param p1, "res"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->mQueryRes:Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;

    .line 75
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->mValue:Ljava/lang/String;

    .line 47
    return-void
.end method
