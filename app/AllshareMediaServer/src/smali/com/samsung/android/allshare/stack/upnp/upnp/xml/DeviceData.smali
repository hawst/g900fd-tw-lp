.class public Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;
.source "DeviceData.java"


# instance fields
.field private mActionMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/upnp/upnp/Action;",
            ">;"
        }
    .end annotation
.end field

.field mActionMapMutex:Ljava/lang/Object;

.field private mAdvertiser:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

.field private mBoundInterfaceAddress:Ljava/lang/String;

.field private mBoundInterfaceName:Ljava/lang/String;

.field private mDescriptionFile:Ljava/io/File;

.field private mDescriptionURI:Ljava/lang/String;

.field private mDisposer:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

.field private mHttpPort:I

.field private mHttpServerList:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

.field private mID:Ljava/lang/String;

.field private mIsForceExpired:Z

.field private mIsRealExpired:Z

.field private mLeaseTime:I

.field private mLocation:Ljava/lang/String;

.field private mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;-><init>()V

    .line 46
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mDescriptionURI:Ljava/lang/String;

    .line 48
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mDescriptionFile:Ljava/io/File;

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mLocation:Ljava/lang/String;

    .line 84
    const/16 v0, 0x708

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mLeaseTime:I

    .line 98
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mHttpServerList:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    .line 108
    const/16 v0, 0xfa4

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mHttpPort:I

    .line 122
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .line 136
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mAdvertiser:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    .line 150
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mDisposer:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    .line 163
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mIsForceExpired:Z

    .line 173
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mIsRealExpired:Z

    .line 186
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mBoundInterfaceAddress:Ljava/lang/String;

    .line 199
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mBoundInterfaceName:Ljava/lang/String;

    .line 212
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mID:Ljava/lang/String;

    .line 222
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMapMutex:Ljava/lang/Object;

    .line 224
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    .line 40
    return-void
.end method


# virtual methods
.method public cleanActionMap()V
    .locals 2

    .prologue
    .line 252
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMapMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 253
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    .line 257
    :cond_0
    monitor-exit v1

    .line 258
    return-void

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getActionFromMap(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .locals 3
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 239
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMapMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 240
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    if-nez v2, :cond_0

    .line 241
    monitor-exit v1

    .line 246
    :goto_0
    return-object v0

    .line 243
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 244
    monitor-exit v1

    goto :goto_0

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 246
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getAdvertiser()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mAdvertiser:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    return-object v0
.end method

.method public getBoundInterfaceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mBoundInterfaceAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getBoundInterfaceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mBoundInterfaceName:Ljava/lang/String;

    return-object v0
.end method

.method public getDescriptionFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mDescriptionFile:Ljava/io/File;

    return-object v0
.end method

.method public getDescriptionURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mDescriptionURI:Ljava/lang/String;

    return-object v0
.end method

.method public getDisposer()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mDisposer:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    return-object v0
.end method

.method public getHTTPPort()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mHttpPort:I

    return v0
.end method

.method public getHTTPServerList()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mHttpServerList:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mID:Ljava/lang/String;

    return-object v0
.end method

.method public getIsForceExpired()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mIsForceExpired:Z

    return v0
.end method

.method public getIsRealExpired()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mIsRealExpired:Z

    return v0
.end method

.method public getLeaseTime()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mLeaseTime:I

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mLocation:Ljava/lang/String;

    return-object v0
.end method

.method public getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    return-object v0
.end method

.method public registerAction(Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V
    .locals 3
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 227
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMapMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 229
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 232
    monitor-exit v1

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mActionMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setAdvertiser(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;)V
    .locals 0
    .param p1, "adv"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mAdvertiser:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    .line 140
    return-void
.end method

.method public setBoundInterfaceAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mBoundInterfaceAddress:Ljava/lang/String;

    .line 190
    return-void
.end method

.method public setBoundInterfaceName(Ljava/lang/String;)V
    .locals 0
    .param p1, "addr"    # Ljava/lang/String;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mBoundInterfaceName:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public setDescriptionFile(Ljava/io/File;)V
    .locals 0
    .param p1, "descriptionFile"    # Ljava/io/File;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mDescriptionFile:Ljava/io/File;

    .line 60
    return-void
.end method

.method public setDescriptionURI(Ljava/lang/String;)V
    .locals 0
    .param p1, "descriptionURI"    # Ljava/lang/String;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mDescriptionURI:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public setDisposer(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;)V
    .locals 0
    .param p1, "disposer"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    .prologue
    .line 153
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mDisposer:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    .line 154
    return-void
.end method

.method public setHTTPPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mHttpPort:I

    .line 116
    return-void
.end method

.method public setID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 215
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mID:Ljava/lang/String;

    .line 216
    return-void
.end method

.method public setIsForceExpired(Z)V
    .locals 0
    .param p1, "isForceExpired"    # Z

    .prologue
    .line 166
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mIsForceExpired:Z

    .line 167
    return-void
.end method

.method public setIsRealExpired(Z)V
    .locals 0
    .param p1, "isRealExpired"    # Z

    .prologue
    .line 176
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mIsRealExpired:Z

    .line 177
    return-void
.end method

.method public setLeaseTime(I)V
    .locals 0
    .param p1, "val"    # I

    .prologue
    .line 91
    iput p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mLeaseTime:I

    .line 92
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 0
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mLocation:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public setSSDPPacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 0
    .param p1, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .line 130
    return-void
.end method
