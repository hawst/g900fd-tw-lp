.class public Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;
.super Ljava/lang/Exception;
.source "UPnPException.java"


# static fields
.field private static final serialVersionUID:J = -0x4f5c679824eb3fcbL


# instance fields
.field private mErrorCode:I

.field private mMsg:Ljava/lang/String;

.field private mStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x0

    const-string v1, "No Error"

    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;-><init>()V

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;-><init>(ILjava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;)V

    .line 36
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error with Value: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;-><init>(ILjava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;)V

    .line 45
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;)V
    .locals 1
    .param p1, "errorCode"    # I
    .param p2, "msg"    # Ljava/lang/String;
    .param p3, "status"    # Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .prologue
    .line 55
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;->mErrorCode:I

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;->mStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .line 56
    iput p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;->mErrorCode:I

    .line 57
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;->mMsg:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;->mStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .line 59
    return-void
.end method


# virtual methods
.method public getErrorCode()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;->mErrorCode:I

    return v0
.end method

.method public getMsg()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;->mMsg:Ljava/lang/String;

    return-object v0
.end method

.method public getUPnPStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPException;->mStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    return-object v0
.end method
