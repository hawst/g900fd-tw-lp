.class public Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetInfomationActionHandler;
.super Ljava/lang/Object;
.source "GetInfomationActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# instance fields
.field private mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V
    .locals 1
    .param p1, "cameraListener"    # Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetInfomationActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 27
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetInfomationActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 28
    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 7
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/16 v6, 0x192

    const/4 v4, 0x0

    .line 32
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetInfomationActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v5}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onGetInformation_GetinformationResult_Request()Ljava/lang/String;

    move-result-object v3

    .line 34
    .local v3, "strInformationResult":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetInfomationActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v5}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onGetInformation_GetIPResult_Request()Ljava/lang/String;

    move-result-object v2

    .line 36
    .local v2, "strIPResult":Ljava/lang/String;
    const-string v5, "GETINFORMATIONRESULT"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 37
    .local v0, "informationResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 44
    const-string v5, "GETIPRESULT"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v1

    .line 45
    .local v1, "ipResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v1, :cond_1

    .line 46
    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 52
    const/4 v4, 0x1

    .end local v1    # "ipResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :goto_0
    return v4

    .line 40
    :cond_0
    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 48
    .restart local v1    # "ipResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :cond_1
    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0
.end method
