.class public Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;
.super Ljava/lang/Object;
.source "HTTPPacket.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;
    }
.end annotation


# instance fields
.field private mContent:[B

.field private mContentInput:Ljava/io/InputStream;

.field private mFileContent:Ljava/io/File;

.field private mFirstLine:Ljava/lang/String;

.field private mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;",
            ">;"
        }
    .end annotation
.end field

.field private mHttpServer:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

.field private mMBFileContent:Z

.field private mRand:Ljava/util/Random;

.field private mReader:Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;

.field private final mTag:Ljava/lang/String;

.field private mVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    .line 151
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpServer:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    .line 291
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mRand:Ljava/util/Random;

    .line 424
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mMBFileContent:Z

    .line 454
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mReader:Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;

    .line 546
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    .line 575
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 730
    new-array v0, v2, [B

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    .line 844
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContentInput:Ljava/io/InputStream;

    .line 119
    const-string v0, "1.1"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setVersion(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContentInputStream(Ljava/io/InputStream;)V

    .line 121
    return-void
.end method

.method private setFirstLine(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 549
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    .line 550
    return-void
.end method


# virtual methods
.method public addHeader(Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;)V
    .locals 1
    .param p1, "header"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    .prologue
    .line 582
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    return-void
.end method

.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 586
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    return-void
.end method

.method public clearHeaders()V
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->clear()V

    .line 614
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 615
    return-void
.end method

.method public getConnection()Ljava/lang/String;
    .locals 1

    .prologue
    .line 930
    const-string v0, "Connection"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContent()Ljava/io/InputStream;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 803
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasFileContent()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 804
    const/4 v1, 0x0

    .line 808
    .local v1, "fis":Ljava/io/FileInputStream;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getFileContent()Ljava/io/File;

    move-result-object v2

    .line 809
    .local v2, "tempFile":Ljava/io/File;
    if-nez v2, :cond_0

    move-object v1, v3

    .line 818
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "tempFile":Ljava/io/File;
    :goto_0
    return-object v1

    .line 811
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "tempFile":Ljava/io/File;
    :cond_0
    new-instance v1, Ljava/io/FileInputStream;

    .end local v1    # "fis":Ljava/io/FileInputStream;
    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 812
    .restart local v1    # "fis":Ljava/io/FileInputStream;
    goto :goto_0

    .line 813
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "tempFile":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 814
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v5, "getContent"

    const-string v6, "getContent  - Exception"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v1, v3

    .line 815
    goto :goto_0

    .line 818
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    new-instance v1, Ljava/io/ByteArrayInputStream;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0
.end method

.method public getContentInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 851
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContentInput:Ljava/io/InputStream;

    return-object v0
.end method

.method public getContentLength()J
    .locals 2

    .prologue
    .line 905
    const-string v0, "Content-Length"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getLongHeaderValue(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getContentRange()[J
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 969
    const/4 v7, 0x3

    new-array v4, v7, [J

    .line 970
    .local v4, "range":[J
    const-wide/16 v8, 0x0

    aput-wide v8, v4, v12

    aput-wide v8, v4, v11

    aput-wide v8, v4, v10

    .line 971
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasContentRange()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1023
    :cond_0
    :goto_0
    return-object v4

    .line 973
    :cond_1
    const-string v7, "Content-Range"

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 975
    .local v5, "rangeLine":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-gtz v7, :cond_2

    .line 976
    const-string v7, "Range"

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 977
    :cond_2
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 981
    const-string v7, "bytes"

    invoke-virtual {v5, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 982
    const-string v7, "-1"

    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v10

    goto :goto_0

    .line 987
    :cond_3
    new-instance v6, Ljava/util/StringTokenizer;

    const-string v7, " ="

    invoke-direct {v6, v5, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    .local v6, "strToken":Ljava/util/StringTokenizer;
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 991
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 994
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 997
    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 998
    .local v1, "firstPosStr":Ljava/lang/String;
    const-string v7, " "

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "="

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 999
    :cond_4
    invoke-virtual {v1, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1002
    :cond_5
    const/4 v7, 0x0

    :try_start_0
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v7
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1007
    :goto_1
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1009
    const-string v7, "-/"

    invoke-virtual {v6, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1011
    .local v2, "lastPosStr":Ljava/lang/String;
    const/4 v7, 0x1

    :try_start_1
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v7
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1015
    :goto_2
    invoke-virtual {v6}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1017
    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1019
    .local v3, "lengthStr":Ljava/lang/String;
    const/4 v7, 0x2

    :try_start_2
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    aput-wide v8, v4, v7
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 1020
    :catch_0
    move-exception v0

    .line 1021
    .local v0, "e":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v8, "getContentRange"

    const-string v9, "getContentRange  - numberFormatException cached(3)"

    invoke-static {v7, v8, v9, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 1003
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .end local v2    # "lastPosStr":Ljava/lang/String;
    .end local v3    # "lengthStr":Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 1004
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v8, "getContentRange"

    const-string v9, "getContentRange  - numberFormatException cached(1)...."

    invoke-static {v7, v8, v9, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 1012
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v2    # "lastPosStr":Ljava/lang/String;
    :catch_2
    move-exception v0

    .line 1013
    .restart local v0    # "e":Ljava/lang/NumberFormatException;
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v8, "getContentRange"

    const-string v9, "getContentRange  - numberFormatException cached(2)"

    invoke-static {v7, v8, v9, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2
.end method

.method public getContentRangeFirstPosition()J
    .locals 4

    .prologue
    .line 1027
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getContentRange()[J

    move-result-object v0

    .line 1028
    .local v0, "range":[J
    const/4 v1, 0x0

    aget-wide v2, v0, v1

    return-wide v2
.end method

.method public getContentRangeLastPosition()J
    .locals 4

    .prologue
    .line 1032
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getContentRange()[J

    move-result-object v0

    .line 1033
    .local v0, "range":[J
    const/4 v1, 0x1

    aget-wide v2, v0, v1

    return-wide v2
.end method

.method getContentString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 825
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 826
    const-string v0, "This Packet Contains a file content, so can not be converted to String"

    .line 828
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public getCurrentReceviedContentLength()J
    .locals 2

    .prologue
    .line 909
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 910
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getFileContent()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 914
    :goto_0
    return-wide v0

    .line 911
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    if-eqz v0, :cond_1

    .line 912
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    array-length v0, v0

    int-to-long v0, v0

    goto :goto_0

    .line 914
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public getFileContent()Ljava/io/File;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFileContent:Ljava/io/File;

    return-object v0
.end method

.method protected getFirstLine()Ljava/lang/String;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    return-object v0
.end method

.method protected getFirstLineToken(I)Ljava/lang/String;
    .locals 5
    .param p1, "num"    # I

    .prologue
    .line 557
    new-instance v2, Ljava/util/StringTokenizer;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    const-string v4, " "

    invoke-direct {v2, v3, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    .local v2, "st":Ljava/util/StringTokenizer;
    const-string v0, ""

    .line 559
    .local v0, "lastToken":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-gt v1, p1, :cond_0

    .line 560
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-nez v3, :cond_1

    .line 561
    const-string v0, ""

    .line 564
    .end local v0    # "lastToken":Ljava/lang/String;
    :cond_0
    return-object v0

    .line 562
    .restart local v0    # "lastToken":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    .line 559
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getHeader(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 591
    const/4 v0, 0x0

    .line 593
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-gt p1, v1, :cond_0

    .line 594
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    .line 596
    .restart local v0    # "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    :cond_0
    return-object v0
.end method

.method public getHeader(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 600
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getNHeaders()I

    move-result v3

    .line 601
    .local v3, "nHeaders":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 602
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeader(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 603
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-nez v0, :cond_1

    .line 601
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 605
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v1

    .line 606
    .local v1, "headerName":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 609
    .end local v0    # "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    .end local v1    # "headerName":Ljava/lang/String;
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getHeaderString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 716
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 717
    .local v3, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getNHeaders()I

    move-result v2

    .line 718
    .local v2, "nHeaders":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 719
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeader(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 720
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 721
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 718
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 723
    .end local v0    # "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getHeaderValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 647
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 648
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-nez v0, :cond_0

    .line 649
    const-string v1, ""

    .line 650
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getHttpServer()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpServer:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    return-object v0
.end method

.method public getLongHeaderValue(Ljava/lang/String;)J
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 705
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 706
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-nez v0, :cond_0

    .line 707
    const-wide/16 v2, 0x0

    .line 708
    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/util/StringUtil;->toLong(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_0
.end method

.method public getNHeaders()I
    .locals 1

    .prologue
    .line 578
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpHeaderList:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getStringHeaderValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 684
    const-string v0, "\""

    const-string v1, "\""

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getStringHeaderValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringHeaderValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "startWidth"    # Ljava/lang/String;
    .param p3, "endWidth"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 673
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 674
    .local v0, "headerValue":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 675
    const/4 v1, 0x0

    .line 680
    :goto_0
    return-object v1

    .line 676
    :cond_0
    invoke-virtual {v0, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 677
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 678
    :cond_1
    invoke-virtual {v0, p3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 679
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_2
    move-object v1, v0

    .line 680
    goto :goto_0
.end method

.method public getTransferEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1122
    const-string v0, "Transfer-Encoding"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mVersion:Ljava/lang/String;

    return-object v0
.end method

.method handleExpect100Continue()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 269
    const-string v3, "Expect"

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 270
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 272
    .local v1, "headerValue":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 273
    const-string v3, "100-continue"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 274
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getContentLength()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasEnoughStorage(J)Z

    move-result v3

    if-nez v3, :cond_1

    .line 275
    check-cast p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .end local p0    # "this":Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;
    const/16 v2, 0x1a1

    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnResponse(I)Z

    .line 276
    const/4 v2, 0x0

    .line 284
    .end local v1    # "headerValue":Ljava/lang/String;
    :cond_0
    :goto_0
    return v2

    .line 278
    .restart local v1    # "headerValue":Ljava/lang/String;
    .restart local p0    # "this":Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;
    :cond_1
    check-cast p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .end local p0    # "this":Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;
    const/16 v3, 0x64

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnResponse(I)Z

    goto :goto_0
.end method

.method public hasConnection()Z
    .locals 1

    .prologue
    .line 922
    const-string v0, "Connection"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasContent()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 832
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasFileContent()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 837
    :cond_0
    :goto_0
    return v0

    .line 834
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    if-eqz v2, :cond_2

    .line 835
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    array-length v2, v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 837
    goto :goto_0
.end method

.method public hasContentInputStream()Z
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContentInput:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasContentRange()Z
    .locals 1

    .prologue
    .line 956
    const-string v0, "Content-Range"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Range"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasEnoughStorage(J)Z
    .locals 9
    .param p1, "size"    # J

    .prologue
    .line 884
    new-instance v2, Landroid/os/StatFs;

    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getCacheDirectory()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 885
    .local v2, "stat":Landroid/os/StatFs;
    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v3

    int-to-long v6, v3

    mul-long v0, v4, v6

    .line 886
    .local v0, "space":J
    cmp-long v3, v0, p1

    if-lez v3, :cond_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public hasFileContent()Z
    .locals 1

    .prologue
    .line 427
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mMBFileContent:Z

    return v0
.end method

.method public hasFirstLine()Z
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFirstLine:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasHeader(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 618
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasTransferEncoding()Z
    .locals 1

    .prologue
    .line 1113
    const-string v0, "Transfer-Encoding"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 128
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setFirstLine(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->clearHeaders()V

    .line 130
    const/4 v0, 0x0

    new-array v0, v0, [B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent([BZ)V

    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContentInputStream(Ljava/io/InputStream;)V

    .line 132
    return-void
.end method

.method public isChunked()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1126
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasTransferEncoding()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1131
    :cond_0
    :goto_0
    return v1

    .line 1128
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getTransferEncoding()Ljava/lang/String;

    move-result-object v0

    .line 1129
    .local v0, "transEnc":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1131
    const-string v1, "Chunked"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isCloseConnection()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 934
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasConnection()Z

    move-result v2

    if-nez v2, :cond_1

    .line 939
    :cond_0
    :goto_0
    return v1

    .line 936
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getConnection()Ljava/lang/String;

    move-result-object v0

    .line 937
    .local v0, "connection":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 939
    const-string v1, "close"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isFileStreamNeeded()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 873
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/Android/data/com.samsung.android.allshare"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 875
    .local v0, "sdcard":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 879
    :cond_0
    :goto_0
    return v1

    .line 877
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->isChunked()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getContentLength()J

    move-result-wide v2

    const-wide/32 v4, 0x300000

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 878
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isKeepAliveConnection()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 943
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasConnection()Z

    move-result v2

    if-nez v2, :cond_1

    .line 948
    :cond_0
    :goto_0
    return v1

    .line 945
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getConnection()Ljava/lang/String;

    move-result-object v0

    .line 946
    .local v0, "connection":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 948
    const-string v1, "Keep-Alive"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public performReceivedListener(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHttpServer()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 421
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHttpServer()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->performReceivedListener(Ljava/lang/String;)V

    .line 422
    :cond_0
    return-void
.end method

.method public read(Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;)Z
    .locals 1
    .param p1, "httpSock"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 534
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 536
    :cond_0
    const/4 v0, 0x0

    .line 539
    :goto_0
    return v0

    .line 538
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->init()V

    .line 539
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;)Z

    move-result v0

    goto :goto_0
.end method

.method readChunkLength(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;)J
    .locals 4
    .param p1, "reader"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NumberFormatException;,
            Ljava/net/SocketTimeoutException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 372
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    .line 373
    .local v0, "contentLen":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 376
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 381
    :cond_1
    return-wide v0
.end method

.method readContentInFile(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;)V
    .locals 26
    .param p1, "reader"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 294
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->isChunked()Z

    move-result v13

    .line 295
    .local v13, "isChunkedRequest":Z
    const-wide/16 v8, 0x0

    .line 296
    .local v8, "contentLen":J
    if-nez p1, :cond_1

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v13, v0, :cond_2

    .line 300
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->readChunkLength(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;)J

    move-result-wide v8

    .line 305
    :goto_1
    const-wide/16 v24, 0x0

    cmp-long v23, v8, v24

    if-nez v23, :cond_3

    .line 306
    long-to-int v0, v8

    move/from16 v23, v0

    move/from16 v0, v23

    new-array v7, v0, [B

    .line 307
    .local v7, "content":[B
    const/16 v23, 0x0

    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mMBFileContent:Z

    .line 308
    const/16 v23, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v23

    invoke-virtual {v0, v7, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent([BZ)V

    goto :goto_0

    .line 303
    .end local v7    # "content":[B
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getContentLength()J

    move-result-wide v8

    goto :goto_1

    .line 316
    :cond_3
    const/4 v11, 0x0

    .line 319
    .local v11, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v22, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getCacheDirectory()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mRand:Ljava/util/Random;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Ljava/util/Random;->nextLong()J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ".tmp"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-direct/range {v22 .. v23}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 321
    .local v22, "tempFile":Ljava/io/File;
    new-instance v12, Ljava/io/FileOutputStream;

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .local v12, "fos":Ljava/io/FileOutputStream;
    const/16 v23, 0x1

    :try_start_1
    move/from16 v0, v23

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mMBFileContent:Z

    .line 324
    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFileContent:Ljava/io/File;

    .line 326
    const v6, 0x4b000

    .line 328
    .local v6, "chunkSize":I
    new-array v14, v6, [B

    .line 330
    .local v14, "readBuf":[B
    :goto_2
    const-wide/16 v24, 0x0

    cmp-long v23, v24, v8

    if-gez v23, :cond_a

    .line 331
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-static {v14, v0}, Ljava/util/Arrays;->fill([BB)V

    .line 332
    const-wide/16 v16, 0x0

    .line 333
    .local v16, "readCnt":J
    :goto_3
    cmp-long v23, v16, v8

    if-gez v23, :cond_5

    .line 334
    sub-long v4, v8, v16

    .line 335
    .local v4, "bufReadLen":J
    int-to-long v0, v6

    move-wide/from16 v24, v0

    cmp-long v23, v24, v4

    if-gez v23, :cond_4

    .line 336
    int-to-long v4, v6

    .line 337
    :cond_4
    const/16 v23, 0x0

    long-to-int v0, v4

    move/from16 v24, v0

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-virtual {v0, v14, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;->read([BII)I

    move-result v15

    .line 338
    .local v15, "readLen":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->performReceivedListener(Ljava/lang/String;)V

    .line 339
    if-gez v15, :cond_7

    .line 346
    .end local v4    # "bufReadLen":J
    .end local v15    # "readLen":I
    :cond_5
    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v13, v0, :cond_9

    .line 348
    const-wide/16 v20, 0x0

    .line 351
    .local v20, "skipLen":J
    :cond_6
    const-string v23, "\r\n"

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    sub-long v24, v24, v20

    move-object/from16 v0, p1

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;->skip(J)J

    move-result-wide v18

    .line 352
    .local v18, "skipCnt":J
    const-wide/16 v24, 0x0

    cmp-long v23, v18, v24

    if-gez v23, :cond_8

    .line 356
    :goto_4
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->readChunkLength(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;)J

    move-result-wide v8

    .line 359
    goto :goto_2

    .line 341
    .end local v18    # "skipCnt":J
    .end local v20    # "skipLen":J
    .restart local v4    # "bufReadLen":J
    .restart local v15    # "readLen":I
    :cond_7
    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v12, v14, v0, v15}, Ljava/io/FileOutputStream;->write([BII)V

    .line 342
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->flush()V

    .line 343
    int-to-long v0, v15

    move-wide/from16 v24, v0

    add-long v16, v16, v24

    .line 344
    goto :goto_3

    .line 354
    .end local v4    # "bufReadLen":J
    .end local v15    # "readLen":I
    .restart local v18    # "skipCnt":J
    .restart local v20    # "skipLen":J
    :cond_8
    add-long v20, v20, v18

    .line 355
    const-string v23, "\r\n"

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v23

    move/from16 v0, v23

    int-to-long v0, v0

    move-wide/from16 v24, v0

    cmp-long v23, v20, v24

    if-ltz v23, :cond_6

    goto :goto_4

    .line 360
    .end local v18    # "skipCnt":J
    .end local v20    # "skipLen":J
    :cond_9
    const-wide/16 v8, 0x0

    goto/16 :goto_2

    .line 365
    .end local v16    # "readCnt":J
    :cond_a
    if-eqz v12, :cond_0

    .line 366
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    goto/16 :goto_0

    .line 362
    .end local v6    # "chunkSize":I
    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .end local v14    # "readBuf":[B
    .end local v22    # "tempFile":Ljava/io/File;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v10

    .line 363
    .local v10, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_2
    throw v10
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 365
    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v23

    :goto_6
    if-eqz v11, :cond_b

    .line 366
    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    :cond_b
    throw v23

    .line 365
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v22    # "tempFile":Ljava/io/File;
    :catchall_1
    move-exception v23

    move-object v11, v12

    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 362
    .end local v11    # "fos":Ljava/io/FileOutputStream;
    .restart local v12    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v10

    move-object v11, v12

    .end local v12    # "fos":Ljava/io/FileOutputStream;
    .restart local v11    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5
.end method

.method readContentInMemory(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;)V
    .locals 13
    .param p1, "reader"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 386
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getContentLength()J

    move-result-wide v4

    .line 389
    .local v4, "contentLen":J
    const-wide/16 v10, 0x0

    cmp-long v9, v4, v10

    if-gez v9, :cond_0

    .line 390
    new-instance v9, Ljava/lang/NegativeArraySizeException;

    const-string v10, "Fail to create content buffer"

    invoke-direct {v9, v10}, Ljava/lang/NegativeArraySizeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 391
    :cond_0
    long-to-int v9, v4

    new-array v2, v9, [B

    .line 394
    .local v2, "content":[B
    const-wide/16 v6, 0x0

    .line 395
    .local v6, "readCnt":J
    :goto_0
    cmp-long v9, v6, v4

    if-gez v9, :cond_1

    .line 397
    sub-long v0, v4, v6

    .line 398
    .local v0, "bufReadLen":J
    long-to-int v9, v6

    long-to-int v10, v0

    :try_start_0
    invoke-virtual {p1, v2, v9, v10}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;->read([BII)I

    move-result v8

    .line 399
    .local v8, "readLen":I
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->performReceivedListener(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 400
    if-gez v8, :cond_2

    .line 412
    .end local v0    # "bufReadLen":J
    .end local v8    # "readLen":I
    :cond_1
    :goto_1
    cmp-long v9, v4, v6

    if-eqz v9, :cond_3

    .line 413
    new-instance v9, Ljava/io/IOException;

    const-string v10, "Fail to get All contents"

    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 402
    .restart local v0    # "bufReadLen":J
    .restart local v8    # "readLen":I
    :cond_2
    int-to-long v10, v8

    add-long/2addr v6, v10

    goto :goto_0

    .line 403
    .end local v8    # "readLen":I
    :catch_0
    move-exception v3

    .line 405
    .local v3, "e":Ljava/net/SocketTimeoutException;
    goto :goto_1

    .line 406
    .end local v3    # "e":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v3

    .line 407
    .local v3, "e":Ljava/lang/Exception;
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v10, "readContentInMemory"

    const-string v11, "readContentInMemory  Exception :"

    invoke-static {v9, v10, v11, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 415
    .end local v0    # "bufReadLen":J
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_3
    iput-boolean v12, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mMBFileContent:Z

    .line 416
    invoke-virtual {p0, v2, v12}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent([BZ)V

    .line 417
    return-void
.end method

.method readHeaders(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;)Z
    .locals 6
    .param p1, "reader"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 239
    if-nez p1, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v3

    .line 241
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "firstLineOfPacket":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 245
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setFirstLine(Ljava/lang/String;)V

    .line 247
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    .line 248
    .local v2, "headerLine":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 250
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    .line 251
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    invoke-direct {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;-><init>(Ljava/lang/String;)V

    .line 252
    .local v1, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->hasName()Z

    move-result v3

    if-ne v3, v4, :cond_2

    .line 253
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;)V

    .line 254
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;->readALine()Ljava/lang/String;

    move-result-object v2

    .line 255
    goto :goto_1

    .end local v1    # "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    :cond_3
    move v3, v4

    .line 257
    goto :goto_0
.end method

.method public removeContent()V
    .locals 2

    .prologue
    .line 437
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 438
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->removeTempFile()V

    .line 441
    :goto_0
    return-void

    .line 440
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    goto :goto_0
.end method

.method public removeTempFile()V
    .locals 4

    .prologue
    .line 444
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mMBFileContent:Z

    .line 445
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFileContent:Ljava/io/File;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFileContent:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 447
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFileContent:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 448
    :catch_0
    move-exception v0

    .line 449
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v2, "removeTempFile"

    const-string v3, "fail to removeTempFile"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;)V
    .locals 4
    .param p1, "httpPacket"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;

    .prologue
    .line 515
    if-nez p1, :cond_0

    .line 526
    :goto_0
    return-void

    .line 517
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setFirstLine(Ljava/lang/String;)V

    .line 519
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->clearHeaders()V

    .line 520
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getNHeaders()I

    move-result v2

    .line 521
    .local v2, "nHeaders":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_1
    if-ge v1, v2, :cond_1

    .line 522
    invoke-virtual {p1, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeader(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 523
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->addHeader(Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;)V

    .line 521
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 525
    .end local v0    # "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;)V

    goto :goto_0
.end method

.method protected set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;)Z
    .locals 1
    .param p1, "httpSock"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 511
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->set(Ljava/io/InputStream;)Z

    move-result v0

    return v0
.end method

.method protected set(Ljava/io/InputStream;)Z
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 504
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->set(Ljava/io/InputStream;Z)Z

    move-result v0

    return v0
.end method

.method protected set(Ljava/io/InputStream;Z)Z
    .locals 6
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "onlyHeaders"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 458
    if-nez p1, :cond_1

    .line 500
    :cond_0
    :goto_0
    return v1

    .line 460
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mReader:Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;

    if-nez v3, :cond_2

    .line 461
    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;

    invoke-direct {v3, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;-><init>(Ljava/io/InputStream;)V

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mReader:Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;

    .line 465
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mReader:Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->readHeaders(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 468
    if-ne p2, v2, :cond_3

    .line 469
    const-string v3, ""

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent(Ljava/lang/String;Z)V

    move v1, v2

    .line 470
    goto :goto_0

    .line 463
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v4, "set"

    const-string v5, "Something Left. Read input stream again!!!"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 485
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Ljava/net/SocketTimeoutException;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->removeTempFile()V

    .line 488
    new-instance v1, Ljava/net/SocketTimeoutException;

    invoke-virtual {v0}, Ljava/net/SocketTimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 474
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :cond_3
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getFirstLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->performReceivedListener(Ljava/lang/String;)V

    .line 477
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->handleExpect100Continue()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 481
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->isFileStreamNeeded()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 482
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mReader:Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->readContentInFile(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;)V

    :goto_2
    move v1, v2

    .line 500
    goto :goto_0

    .line 484
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mReader:Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->readContentInMemory(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;)V
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_2

    .line 489
    :catch_1
    move-exception v0

    .line 491
    .local v0, "e":Ljava/io/IOException;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v3, "set"

    const-string v4, "set  -IOException "

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 492
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->removeTempFile()V

    goto :goto_0

    .line 494
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 497
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->removeTempFile()V

    goto :goto_0
.end method

.method public setCacheControl(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1057
    const-string v0, "max-age"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setCacheControl(Ljava/lang/String;I)V

    .line 1058
    return-void
.end method

.method public setCacheControl(Ljava/lang/String;I)V
    .locals 3
    .param p1, "directive"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 1052
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1053
    .local v0, "strVal":Ljava/lang/String;
    const-string v1, "Cache-Control"

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    return-void
.end method

.method public setConnection(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 926
    const-string v0, "Connection"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    return-void
.end method

.method public setContent(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;)V
    .locals 2
    .param p1, "packet"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;

    .prologue
    .line 740
    if-nez p1, :cond_0

    .line 746
    :goto_0
    return-void

    .line 742
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->hasFileContent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 743
    iget-object v0, p1, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent([BZ)V

    goto :goto_0

    .line 745
    :cond_1
    iget-object v0, p1, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFileContent:Ljava/io/File;

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent(Ljava/io/File;)V

    goto :goto_0
.end method

.method public setContent(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 753
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->removeTempFile()V

    .line 754
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mFileContent:Ljava/io/File;

    .line 755
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mMBFileContent:Z

    .line 756
    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    .line 757
    return-void
.end method

.method public setContent(Ljava/lang/String;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/String;

    .prologue
    .line 775
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent(Ljava/lang/String;Z)V

    .line 776
    return-void
.end method

.method public setContent(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "updateWithContentLength"    # Z

    .prologue
    .line 760
    if-eqz p1, :cond_0

    .line 763
    :try_start_0
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent([BZ)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 772
    :cond_0
    :goto_0
    return-void

    .line 764
    :catch_0
    move-exception v0

    .line 765
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent([BZ)V

    .line 766
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v2, "setContent"

    const-string v3, "setContent  UnsupportedEncodingException"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 768
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v0

    .line 769
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mTag:Ljava/lang/String;

    const-string v2, "setContent"

    const-string v3, "setContent  Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public setContent([B)V
    .locals 1
    .param p1, "content"    # [B

    .prologue
    .line 749
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContent([BZ)V

    .line 750
    return-void
.end method

.method public setContent([BZ)V
    .locals 2
    .param p1, "data"    # [B
    .param p2, "updateWithContentLength"    # Z

    .prologue
    .line 733
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContent:[B

    .line 734
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mMBFileContent:Z

    .line 735
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    if-eqz p1, :cond_0

    .line 736
    array-length v0, p1

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setContentLength(J)V

    .line 737
    :cond_0
    return-void
.end method

.method public setContentInputStream(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 847
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mContentInput:Ljava/io/InputStream;

    .line 848
    return-void
.end method

.method public setContentLength(J)V
    .locals 1
    .param p1, "len"    # J

    .prologue
    .line 901
    const-string v0, "Content-Length"

    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setLongHeader(Ljava/lang/String;J)V

    .line 902
    return-void
.end method

.method public setContentRange(JJJ)V
    .locals 7
    .param p1, "firstPos"    # J
    .param p3, "lastPos"    # J
    .param p5, "length"    # J

    .prologue
    .line 960
    const-string v0, ""

    .line 961
    .local v0, "rangeStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bytes "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 962
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 963
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 964
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-wide/16 v4, 0x0

    cmp-long v1, v4, p5

    if-gez v1, :cond_0

    invoke-static {p5, p6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 965
    const-string v1, "Content-Range"

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    return-void

    .line 964
    :cond_0
    const-string v1, "*"

    goto :goto_0
.end method

.method public setContentType(Ljava/lang/String;)V
    .locals 1
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 863
    const-string v0, "Content-Type"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    return-void
.end method

.method public setDate(Ljava/util/Calendar;)V
    .locals 3
    .param p1, "cal"    # Ljava/util/Calendar;

    .prologue
    .line 1099
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/http/Date;

    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/Date;-><init>(Ljava/util/Calendar;)V

    .line 1100
    .local v0, "date":Lcom/samsung/android/allshare/stack/upnp/http/Date;
    const-string v1, "Date"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/Date;->getDateString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    return-void
.end method

.method public setHeader(Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;)V
    .locals 2
    .param p1, "header"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    .prologue
    .line 641
    if-nez p1, :cond_0

    .line 644
    :goto_0
    return-void

    .line 643
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 622
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getHeader(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 623
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 624
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->setValue(Ljava/lang/String;)V

    .line 628
    :goto_0
    return-void

    .line 627
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setHost(Ljava/lang/String;I)V
    .locals 4
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 1083
    move-object v0, p1

    .line 1084
    .local v0, "hostAddr":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->isIPv6Address(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1085
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1086
    :cond_0
    const-string v1, "HOST"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1087
    return-void
.end method

.method public setHttpServer(Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;)V
    .locals 0
    .param p1, "server"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mHttpServer:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    .line 155
    return-void
.end method

.method public setLongHeader(Ljava/lang/String;J)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # J

    .prologue
    .line 693
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    return-void
.end method

.method public setServer(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1070
    const-string v0, "Server"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1071
    return-void
.end method

.method public setStringHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 669
    const-string v0, "\""

    const-string v1, "\""

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setStringHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    return-void
.end method

.method public setStringHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "startWidth"    # Ljava/lang/String;
    .param p4, "endWidth"    # Ljava/lang/String;

    .prologue
    .line 658
    if-nez p2, :cond_0

    .line 666
    :goto_0
    return-void

    .line 660
    :cond_0
    move-object v0, p2

    .line 661
    .local v0, "headerValue":Ljava/lang/String;
    invoke-virtual {v0, p3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 662
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 663
    :cond_1
    invoke-virtual {v0, p4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 664
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 665
    :cond_2
    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "ver"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->mVersion:Ljava/lang/String;

    .line 142
    return-void
.end method
