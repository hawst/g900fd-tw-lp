.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/PrepareForConnectionActionHandler;
.super Ljava/lang/Object;
.source "PrepareForConnectionActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# static fields
.field private static final AV_TRNSPORT_ID:Ljava/lang/String; = "AVTransportID"

.field private static final CONNECTION_ID:Ljava/lang/String; = "ConnectionID"

.field private static final RCS_ID:Ljava/lang/String; = "RcsID"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 5
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/16 v4, 0x192

    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 42
    const-string v2, "ConnectionID"

    invoke-virtual {p1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 44
    .local v0, "argument":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v0, :cond_0

    .line 45
    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 52
    const-string v2, "AVTransportID"

    invoke-virtual {p1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 53
    if-eqz v0, :cond_1

    .line 54
    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 61
    const-string v2, "RcsID"

    invoke-virtual {p1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_2

    .line 63
    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 70
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 47
    :cond_0
    invoke-virtual {p1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {p1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 65
    :cond_2
    invoke-virtual {p1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0
.end method
