.class public Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
.source "MediaStoreDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$1;,
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;,
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;,
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;
    }
.end annotation


# static fields
.field private static final DATE_FORMAT:Ljava/lang/String; = "yyyy-MM-dd\'T\'HH:mm:ss"

.field public static final TAG:Ljava/lang/String; = "MediaStoreDirectory"

.field private static UPDATE_DELAY_MILLIS:I


# instance fields
.field private mContentUri:Landroid/net/Uri;

.field private mCurrentUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;

.field private mDateFormat:Ljava/text/SimpleDateFormat;

.field private mInfoUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;

.field private mIsUpdating:Z

.field private mNeedMoreUpdate:Z

.field private mNonSupportFileExtensionList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mNonSupportMimeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

.field private mUpdateDelayHandler:Landroid/os/Handler;

.field private mUpdateDelayTask:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;

.field private mWhere:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/16 v0, 0x7d0

    sput v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->UPDATE_DELAY_MILLIS:I

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1
    .param p1, "server"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 91
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 6
    .param p1, "server"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "where"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 59
    invoke-virtual {p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "*/*"

    invoke-direct {p0, v1, p2, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mContentUri:Landroid/net/Uri;

    .line 42
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 44
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mWhere:Ljava/lang/String;

    .line 46
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mInfoUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;

    .line 51
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;

    .line 53
    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$1;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayTask:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;

    .line 120
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mCurrentUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;

    .line 122
    iput-boolean v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mIsUpdating:Z

    .line 124
    iput-boolean v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNeedMoreUpdate:Z

    .line 313
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    .line 315
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".pyv"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 316
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".dcf"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".imelody"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".dm"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".pvv"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 320
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".midi"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".amr"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".ogg"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".application"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".gif"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 325
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".bmp"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".wbmp"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    const-string v2, ".webp"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    .line 334
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "video/pyv"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "audio/pyv"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 336
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "audio/dcf"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "audio/imelody"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 338
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "video/pvv"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "audio/pvv"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 340
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "audio/midi"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "audio/amr"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "audio/ogg"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "audio/application"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "video/application"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "image/application"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 346
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "image/gif"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "image/bmp"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v2, "image/webp"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mContentUri:Landroid/net/Uri;

    .line 62
    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;

    invoke-direct {v1, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mInfoUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;

    .line 63
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 64
    iput-object p4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mWhere:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "type":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 68
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mContentUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 70
    :cond_0
    if-nez v0, :cond_2

    .line 88
    :cond_1
    :goto_0
    return-void

    .line 72
    :cond_2
    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 73
    const-string v1, "video/*"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->setType(Ljava/lang/String;)V

    .line 83
    :goto_1
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 84
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x1

    new-instance v3, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;

    invoke-direct {v3, p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreContentObserver;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)V

    invoke-virtual {v1, p3, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    goto :goto_0

    .line 74
    :cond_3
    const-string v1, "image"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 75
    const-string v1, "image/*"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->setType(Ljava/lang/String;)V

    goto :goto_1

    .line 76
    :cond_4
    const-string v1, "audio"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 77
    const-string v1, "audio/*"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->setType(Ljava/lang/String;)V

    goto :goto_1

    .line 79
    :cond_5
    const-string v1, "MediaStoreDirectory"

    const-string v2, "MediaStoreDirectory"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "can\'t setType..this type is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/util/TreeMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getItemMap()Ljava/util/TreeMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/util/TreeMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getItemMap()Ljava/util/TreeMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/util/TreeMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getItemMap()Ljava/util/TreeMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/util/TreeMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getItemMap()Ljava/util/TreeMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .param p1, "x1"    # I

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->setChildCount(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->notifiyDirectoryChanged()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .param p1, "x1"    # Landroid/os/Handler;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayTask:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;

    return-object v0
.end method

.method static synthetic access$400()I
    .locals 1

    .prologue
    .line 37
    sget v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->UPDATE_DELAY_MILLIS:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mWhere:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mCurrentUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;J)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .param p1, "x1"    # J

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getDateTimeStr(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/net/Uri;
    .param p3, "x3"    # Landroid/database/Cursor;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->convertCursorToItemNode(Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    move-result-object v0

    return-object v0
.end method

.method private convertAudioCursorToItemNode(Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    .locals 18
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 441
    if-nez p3, :cond_0

    .line 442
    const/4 v7, 0x0

    .line 464
    :goto_0
    return-object v7

    .line 444
    :cond_0
    const-string v14, "_size"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 445
    .local v4, "idxSize":I
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 446
    .local v10, "size":J
    const-wide/16 v14, 0x0

    cmp-long v14, v10, v14

    if-nez v14, :cond_1

    .line 447
    const/4 v7, 0x0

    goto :goto_0

    .line 449
    :cond_1
    const-string v14, "title"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 450
    .local v5, "idxTitle":I
    const-string v14, "mime_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 452
    .local v6, "idxType":I
    const-string v14, "date_modified"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 453
    .local v3, "idxModifyDate":I
    const-string v14, "date_added"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 454
    .local v2, "idxCreateDate":I
    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 456
    .local v8, "lMaxDate":J
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 457
    .local v12, "title":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 460
    .local v13, "upnpClass":Ljava/lang/String;
    new-instance v7, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v7, v0, v12, v13, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 462
    .local v7, "node":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getDateTimeStr(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setDate(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private convertCursorToItemNode(Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    .locals 9
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x0

    .line 268
    const/4 v3, 0x0

    .line 269
    .local v3, "item":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    if-nez p3, :cond_1

    .line 309
    :cond_0
    :goto_0
    return-object v5

    .line 272
    :cond_1
    const-string v6, "mime_type"

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 273
    .local v2, "idxMime":I
    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 275
    .local v4, "type":Ljava/lang/String;
    const-string v6, "_data"

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 276
    .local v1, "idxData":I
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 279
    .local v0, "data":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 282
    invoke-virtual {p0, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->isSupportedMimeType(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 286
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->isSupportedFileExtension(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 290
    invoke-virtual {p0, v0, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->isDRMMedia(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_0

    .line 293
    const-string v5, "video"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 294
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->convertVideoCursorToItemNode(Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    move-result-object v3

    .line 303
    :goto_1
    if-eqz v3, :cond_2

    .line 304
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mInfoUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;

    invoke-virtual {v3, v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setContentResourceUpdater(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;)V

    .line 305
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->update()V

    :cond_2
    move-object v5, v3

    .line 309
    goto :goto_0

    .line 295
    :cond_3
    const-string v5, "image"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 296
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->convertImageCursorToItemNode(Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    move-result-object v3

    goto :goto_1

    .line 297
    :cond_4
    const-string v5, "audio"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 298
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->convertAudioCursorToItemNode(Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    move-result-object v3

    goto :goto_1

    .line 300
    :cond_5
    const-string v5, "MediaStoreDirectory"

    const-string v6, "convertCursorToItemNode"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "can\'t setType..this type is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private convertImageCursorToItemNode(Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    .locals 18
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 411
    if-nez p3, :cond_0

    .line 412
    const/4 v7, 0x0

    .line 436
    :goto_0
    return-object v7

    .line 414
    :cond_0
    const-string v14, "_size"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 415
    .local v4, "idxSize":I
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 416
    .local v10, "size":J
    const-wide/16 v14, 0x0

    cmp-long v14, v10, v14

    if-nez v14, :cond_1

    .line 417
    const/4 v7, 0x0

    goto :goto_0

    .line 419
    :cond_1
    const-string v14, "title"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 420
    .local v5, "idxTitle":I
    const-string v14, "mime_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 422
    .local v6, "idxType":I
    const-string v14, "date_modified"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 423
    .local v3, "idxModifyDate":I
    const-string v14, "date_added"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 424
    .local v2, "idxCreateDate":I
    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 426
    .local v8, "lMaxDate":J
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 427
    .local v12, "title":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 430
    .local v13, "upnpClass":Ljava/lang/String;
    new-instance v7, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v7, v0, v12, v13, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 431
    .local v7, "item":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setTitle(Ljava/lang/String;)V

    .line 433
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getDateTimeStr(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setDate(Ljava/lang/String;)V

    .line 434
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setUPnPClass(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private convertVideoCursorToItemNode(Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    .locals 18
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "contentUri"    # Landroid/net/Uri;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 469
    if-nez p3, :cond_0

    .line 470
    const/4 v7, 0x0

    .line 496
    :goto_0
    return-object v7

    .line 472
    :cond_0
    const-string v14, "_size"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 473
    .local v4, "idxSize":I
    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 474
    .local v10, "size":J
    const-wide/16 v14, 0x0

    cmp-long v14, v10, v14

    if-nez v14, :cond_1

    .line 475
    const/4 v7, 0x0

    goto :goto_0

    .line 477
    :cond_1
    const-string v14, "title"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 478
    .local v5, "idxTitle":I
    const-string v14, "mime_type"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 480
    .local v6, "idxType":I
    const-string v14, "date_modified"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 481
    .local v3, "idxModifyDate":I
    const-string v14, "date_added"

    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 482
    .local v2, "idxCreateDate":I
    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 484
    .local v8, "lMaxDate":J
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 485
    .local v12, "title":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 488
    .local v13, "upnpClass":Ljava/lang/String;
    new-instance v7, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v7, v0, v12, v13, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 490
    .local v7, "item":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setTitle(Ljava/lang/String;)V

    .line 492
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v9}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getDateTimeStr(J)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setDate(Ljava/lang/String;)V

    .line 494
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v14

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setUPnPClass(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private getDateTimeStr(J)Ljava/lang/String;
    .locals 7
    .param p1, "dateLong"    # J

    .prologue
    .line 511
    const-wide/16 v4, 0xa

    cmp-long v3, p1, v4

    if-gez v3, :cond_1

    .line 512
    const-string v0, ""

    .line 526
    :cond_0
    :goto_0
    return-object v0

    .line 514
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mDateFormat:Ljava/text/SimpleDateFormat;

    if-nez v3, :cond_2

    .line 515
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd\'T\'HH:mm:ss"

    invoke-direct {v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mDateFormat:Ljava/text/SimpleDateFormat;

    .line 516
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mDateFormat:Ljava/text/SimpleDateFormat;

    const-string v4, "UTC"

    invoke-static {v4}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 518
    :cond_2
    new-instance v2, Ljava/util/Date;

    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p1

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 519
    .local v2, "tempDate":Ljava/util/Date;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v3, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 521
    .local v0, "dateStr":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 522
    const-string v0, ""
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 524
    .end local v0    # "dateStr":Ljava/lang/String;
    .end local v2    # "tempDate":Ljava/util/Date;
    :catch_0
    move-exception v1

    .line 525
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "MediaStoreDirectory"

    const-string v4, "getDateTimeStr"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getDateTimeStr Exception : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 526
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method isDRMMedia(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 398
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getDrmManagerClient()Landroid/drm/DrmManagerClient;

    move-result-object v0

    .line 399
    .local v0, "client":Landroid/drm/DrmManagerClient;
    if-nez v0, :cond_0

    .line 400
    const/4 v1, 0x0

    .line 406
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p1, p2}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method isSupportedFileExtension(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 357
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 370
    :cond_0
    :goto_0
    return v2

    .line 360
    :cond_1
    const-string v4, "."

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 361
    .local v0, "dotIdx":I
    if-lez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v0, v4, :cond_3

    :cond_2
    move v2, v3

    .line 363
    goto :goto_0

    .line 365
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 367
    .local v1, "fileExt":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportFileExtensionList:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v2, v3

    .line 370
    goto :goto_0
.end method

.method isSupportedMimeType(Ljava/lang/String;)Z
    .locals 3
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 377
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 387
    :cond_0
    :goto_0
    return v1

    .line 380
    :cond_1
    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 381
    .local v0, "slashIdx":I
    if-lez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 384
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNonSupportMimeList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 387
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public update()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 236
    iget-boolean v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mIsUpdating:Z

    if-eqz v3, :cond_0

    .line 237
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNeedMoreUpdate:Z

    .line 238
    const-string v2, "MediaStoreDirectory"

    const-string v3, "update"

    const-string v4, "DMS DB is already updating..."

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :goto_0
    return v1

    .line 243
    :cond_0
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mIsUpdating:Z

    .line 245
    monitor-enter p0

    .line 246
    :try_start_0
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$1;)V

    .line 248
    .local v0, "updater":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mCurrentUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;

    .line 249
    const-string v3, "MediaStoreDirectory"

    const-string v4, "update"

    const-string v5, "start DMS DB update"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->update()V

    .line 251
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mIsUpdating:Z

    .line 255
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getMediaUpdateHandler()Landroid/os/Handler;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;

    .line 257
    iget-boolean v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNeedMoreUpdate:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isUpdateWorkerAlive()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 258
    const-string v3, "MediaStoreDirectory"

    const-string v4, "update"

    const-string v5, "mUpdateDelayHandler post msg to mUpdateDelayTask."

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayTask:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 260
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mUpdateDelayTask:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$UpdateDelayTask;

    sget v5, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->UPDATE_DELAY_MILLIS:I

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 262
    :cond_1
    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mNeedMoreUpdate:Z

    move v1, v2

    .line 264
    goto :goto_0

    .line 251
    .end local v0    # "updater":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
