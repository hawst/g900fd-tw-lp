.class Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ManageDevice"
.end annotation


# instance fields
.field private mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

.field private mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private mSSDPPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

.field final synthetic this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V
    .locals 1
    .param p2, "ssdpPacket"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    .param p3, "rootNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .param p4, "discoveryInfo"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .prologue
    const/4 v0, 0x0

    .line 280
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 272
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mSSDPPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .line 274
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 276
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 281
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mSSDPPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .line 282
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 283
    iput-object p4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 284
    return-void
.end method


# virtual methods
.method public run()V
    .locals 19

    .prologue
    .line 287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mSSDPPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v17, v0

    if-nez v17, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mSSDPPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v13

    .line 291
    .local v13, "ssdpUSN":Ljava/lang/String;
    invoke-static {v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/USN;->getUUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 293
    .local v14, "ssdpUUID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getNetworkInterfaceName()Ljava/lang/String;

    move-result-object v7

    .line 294
    .local v7, "interfaceName":Ljava/lang/String;
    if-eqz v7, :cond_2

    const-string v17, "*"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 296
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mSSDPPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getNetworkInterface()Ljava/net/NetworkInterface;

    move-result-object v8

    .line 297
    .local v8, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v8}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v7

    .line 300
    .end local v8    # "ni":Ljava/net/NetworkInterface;
    :cond_3
    invoke-static {v14, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->generateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 302
    .local v9, "requestId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-object/from16 v17, v0

    if-nez v17, :cond_4

    .line 303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V
    invoke-static {v0, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$000(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;)V

    goto :goto_0

    .line 307
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V
    invoke-static {v0, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$000(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;)V

    .line 309
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-object/from16 v18, v0

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$100(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v10

    .line 311
    .local v10, "rootDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v10, :cond_0

    .line 314
    invoke-virtual {v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/USN;->getUUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 315
    .local v12, "rootUUID":Ljava/lang/String;
    invoke-static {v12, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->generateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 317
    .local v11, "rootId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mSSDPPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setSSDPPacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    .line 318
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v17, v0

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceDisposer:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$200(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setDeviceDisposer(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;)V

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v2

    .line 323
    .local v2, "devFromNode":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v2, :cond_5

    invoke-virtual {v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getID()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 324
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mSSDPPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setSSDPPacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    goto/16 :goto_0

    .line 327
    :cond_5
    if-nez v2, :cond_0

    .line 328
    invoke-virtual {v10, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setBoundInterfaceName(Ljava/lang/String;)V

    .line 329
    invoke-virtual {v10, v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setID(Ljava/lang/String;)V

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-object/from16 v18, v0

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->addDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    invoke-static/range {v17 .. v18}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$300(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->performAddDeviceListener(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    .line 334
    invoke-virtual {v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v4

    .line 336
    .local v4, "embDeviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 337
    .local v3, "embDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v15

    .line 338
    .local v15, "udn":Ljava/lang/String;
    invoke-static {v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/USN;->getUUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 339
    .local v16, "uuid":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-static {v0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->generateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 342
    .local v5, "embId":Ljava/lang/String;
    invoke-virtual {v3, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setBoundInterfaceName(Ljava/lang/String;)V

    .line 343
    invoke-virtual {v3, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setID(Ljava/lang/String;)V

    .line 345
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->performAddDeviceListener(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    goto :goto_1
.end method
