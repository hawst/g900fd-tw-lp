.class public Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
.super Ljava/lang/Object;
.source "HTTPServer.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final DEFAULT_PORT:I = 0x50

.field public static final NAME:Ljava/lang/String; = "DoaHTTP"

.field public static final VERSION:Ljava/lang/String; = "1.1"


# instance fields
.field private bHttpServerThreadFlag:Z

.field private mAcceptSocketList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Ljava/net/Socket;",
            ">;>;"
        }
    .end annotation
.end field

.field private mBindPort:I

.field private mHttpReceivedListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

.field private mHttpRequestListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

.field private mHttpServerThread:Ljava/lang/Thread;

.field private mServerSock:Ljava/net/ServerSocket;

.field private final mTAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    .line 98
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 100
    iput v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mBindPort:I

    .line 197
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpRequestListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    .line 199
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpReceivedListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    .line 250
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    .line 252
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->bHttpServerThreadFlag:Z

    .line 254
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    .line 91
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 92
    return-void
.end method

.method public static getName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 81
    const-string v2, "os.name"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "osName":Ljava/lang/String;
    const-string v2, "os.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    .local v1, "osVer":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "DoaHTTP"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public accept()Ljava/net/Socket;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 159
    const/4 v2, 0x0

    .line 160
    .local v2, "sock":Ljava/net/Socket;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->isClosed()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-object v3

    .line 163
    :cond_1
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    invoke-virtual {v4}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    move-result-object v2

    .line 164
    const v4, 0x88b8

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 165
    const v4, 0x4b000

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setReceiveBufferSize(I)V

    .line 166
    const v4, 0x4b000

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSendBufferSize(I)V

    .line 168
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "open"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "accept :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/net/Socket;->getSendBufferSize()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v3, v2

    .line 169
    goto :goto_0

    .line 170
    :catch_0
    move-exception v1

    .line 171
    .local v1, "e1":Ljava/net/SocketTimeoutException;
    goto :goto_0

    .line 172
    .end local v1    # "e1":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/Exception;
    if-eqz v2, :cond_2

    .line 175
    :try_start_1
    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 180
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "open"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "accept Exception :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 176
    :catch_2
    move-exception v1

    .line 177
    .local v1, "e1":Ljava/io/IOException;
    move-object v0, v1

    goto :goto_1
.end method

.method public addReceivedListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpReceivedListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 228
    return-void
.end method

.method public addRequestListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpRequestListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 206
    return-void
.end method

.method public close()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 141
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    if-nez v3, :cond_0

    .line 152
    :goto_0
    return v1

    .line 144
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    invoke-virtual {v3}, Ljava/net/ServerSocket;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    iput-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 150
    iput v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mBindPort:I

    goto :goto_0

    .line 145
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v3, "open"

    const-string v4, "Close  - Exception :"

    invoke-static {v1, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 149
    iput-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 150
    iput v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mBindPort:I

    move v1, v2

    goto :goto_0

    .line 149
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iput-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 150
    iput v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mBindPort:I

    throw v1
.end method

.method public isOpened()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public open(I)Z
    .locals 5
    .param p1, "port"    # I

    .prologue
    const/4 v1, 0x1

    .line 120
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    if-eqz v2, :cond_0

    .line 134
    :goto_0
    return v1

    .line 123
    :cond_0
    :try_start_0
    iput p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mBindPort:I

    .line 127
    new-instance v2, Ljava/net/ServerSocket;

    iget v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mBindPort:I

    const/16 v4, 0x200

    invoke-direct {v2, v3, v4}, Ljava/net/ServerSocket;-><init>(II)V

    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    .line 128
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    const v3, 0x88b8

    invoke-virtual {v2, v3}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    .line 129
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mServerSock:Ljava/net/ServerSocket;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/net/ServerSocket;->setReuseAddress(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 130
    :catch_0
    move-exception v0

    .line 131
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v2, "open"

    const-string v3, "Open - IOException"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 132
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public performReceivedListener(Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 238
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpReceivedListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->size()I

    move-result v1

    .line 239
    .local v1, "listenerSize":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 240
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpReceivedListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;

    .line 242
    .local v0, "listener":Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;
    invoke-interface {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;->httptRecieved(Ljava/lang/String;)V

    .line 239
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 244
    .end local v0    # "listener":Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;
    :cond_0
    return-void
.end method

.method public performRequestListener(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 4
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 216
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpRequestListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->size()I

    move-result v1

    .line 217
    .local v1, "listenerSize":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 218
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpRequestListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;

    .line 219
    .local v0, "listener":Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;
    invoke-interface {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;->httpRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 217
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 221
    .end local v0    # "listener":Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;
    :cond_0
    return-void
.end method

.method public removeReceivedListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpReceivedListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->remove(Ljava/lang/Object;)Z

    .line 232
    return-void
.end method

.method public removeRequestListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpRequestListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->remove(Ljava/lang/Object;)Z

    .line 210
    return-void
.end method

.method public run()V
    .locals 8

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->isOpened()Z

    move-result v5

    if-nez v5, :cond_0

    .line 310
    :goto_0
    return-void

    .line 261
    :cond_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    .line 262
    .local v2, "exeutor":Ljava/util/concurrent/ExecutorService;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    .line 264
    .local v4, "thisThread":Ljava/lang/Thread;
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v6, "run"

    const-string v7, "http server started!!!!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    :cond_1
    :goto_1
    iget-boolean v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->bHttpServerThreadFlag:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    if-eqz v5, :cond_5

    if-eqz v4, :cond_5

    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    invoke-virtual {v5, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 268
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 269
    const/4 v3, 0x0

    .line 272
    .local v3, "sock":Ljava/net/Socket;
    :goto_2
    :try_start_0
    sget v5, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->usedServerSocket:I

    const/16 v6, 0x64

    if-le v5, v6, :cond_3

    .line 273
    invoke-static {}, Ljava/lang/Thread;->yield()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 293
    :catch_0
    move-exception v0

    .line 294
    .local v0, "e":Ljava/lang/Exception;
    if-eqz v3, :cond_2

    .line 296
    :try_start_1
    invoke-virtual {v3}, Ljava/net/Socket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 301
    :cond_2
    :goto_3
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v6, "run"

    const-string v7, "run - IOException"

    invoke-static {v5, v6, v7, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 276
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    :try_start_2
    iget-object v6, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    monitor-enter v6
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 277
    :try_start_3
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->accept()Ljava/net/Socket;

    move-result-object v3

    .line 278
    if-eqz v3, :cond_4

    .line 284
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    new-instance v7, Ljava/lang/ref/WeakReference;

    invoke-direct {v7, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;

    invoke-direct {v5, p0, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;Ljava/net/Socket;)V

    invoke-interface {v2, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 287
    sget v5, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->usedServerSocket:I

    add-int/lit8 v5, v5, 0x1

    sput v5, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->usedServerSocket:I

    .line 289
    :cond_4
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 291
    :try_start_4
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->isOpened()Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    move-result v5

    if-nez v5, :cond_1

    .line 307
    .end local v3    # "sock":Ljava/net/Socket;
    :cond_5
    invoke-interface {v2}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 308
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v6, "run"

    const-string v7, "http server stopped!!!!"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 289
    .restart local v3    # "sock":Ljava/net/Socket;
    :catchall_0
    move-exception v5

    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v5
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 297
    .restart local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 298
    .local v1, "e1":Ljava/io/IOException;
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v6, "run"

    const-string v7, "run - IOException"

    invoke-static {v5, v6, v7, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3
.end method

.method public declared-synchronized start()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 313
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    .line 314
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HTTP Accept Thread:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mBindPort:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 316
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 317
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setPriority(I)V

    .line 318
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->bHttpServerThreadFlag:Z

    .line 319
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    monitor-exit p0

    return v3

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()Z
    .locals 8

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->close()Z

    .line 326
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->bHttpServerThreadFlag:Z

    .line 328
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "stop"

    const-string v6, "Wait start for finishing thread !!!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->isAlive()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v4

    if-eqz v4, :cond_0

    .line 331
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    invoke-virtual {v4}, Ljava/lang/Thread;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 336
    :cond_0
    :goto_0
    :try_start_2
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "stop"

    const-string v6, "Wait done for finishing thread !!!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mHttpServerThread:Ljava/lang/Thread;

    .line 343
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 344
    :try_start_3
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/ref/WeakReference;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 346
    .local v3, "sockRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Ljava/net/Socket;>;"
    :try_start_4
    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/net/Socket;

    .line 347
    .local v2, "sock":Ljava/net/Socket;
    if-eqz v2, :cond_1

    .line 348
    invoke-virtual {v2}, Ljava/net/Socket;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 350
    .end local v2    # "sock":Ljava/net/Socket;
    :catch_0
    move-exception v0

    .line 351
    .local v0, "e":Ljava/io/IOException;
    :try_start_5
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v6, "stop"

    const-string v7, "Error: socket - IOException"

    invoke-static {v4, v6, v7, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 356
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "sockRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Ljava/net/Socket;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 324
    :catchall_1
    move-exception v4

    monitor-exit p0

    throw v4

    .line 332
    :catch_1
    move-exception v0

    .line 333
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_7
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mTAG:Ljava/lang/String;

    const-string v5, "stop"

    const-string v6, "Thread Join InterruptedException"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    .line 355
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_2
    :try_start_8
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->mAcceptSocketList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 356
    monitor-exit v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 358
    const/4 v4, 0x1

    monitor-exit p0

    return v4
.end method
