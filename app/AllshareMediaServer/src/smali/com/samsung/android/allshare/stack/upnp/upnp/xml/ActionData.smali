.class public Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;
.source "ActionData.java"


# instance fields
.field private mActionListener:Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

.field private mCtrlRes:Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->mActionListener:Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    .line 50
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->mCtrlRes:Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;

    .line 30
    return-void
.end method


# virtual methods
.method public getActionListener()Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->mActionListener:Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    return-object v0
.end method

.method public getControlResponse()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->mCtrlRes:Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;

    return-object v0
.end method

.method public setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V
    .locals 0
    .param p1, "actionListener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->mActionListener:Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    .line 44
    return-void
.end method

.method public setControlResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;)V
    .locals 0
    .param p1, "res"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->mCtrlRes:Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;

    .line 58
    return-void
.end method
