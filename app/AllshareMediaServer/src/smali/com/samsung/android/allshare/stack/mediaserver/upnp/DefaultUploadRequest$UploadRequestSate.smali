.class public final enum Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;
.super Ljava/lang/Enum;
.source "DefaultUploadRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UploadRequestSate"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

.field public static final enum ALLOWED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

.field public static final enum REJECTED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

.field public static final enum UNDEFINED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->UNDEFINED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    const-string v1, "ALLOWED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->ALLOWED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->REJECTED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->UNDEFINED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->ALLOWED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->REJECTED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->$VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 20
    const-class v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->$VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    return-object v0
.end method
