.class public abstract Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
.source "AbstractFileResourceProperty.java"


# instance fields
.field private mContentFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "contentFile"    # Ljava/io/File;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;->mContentFile:Ljava/io/File;

    .line 27
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;->mContentFile:Ljava/io/File;

    .line 28
    if-eqz p3, :cond_0

    .line 29
    const-string v0, "size"

    invoke-virtual {p3}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 31
    :cond_0
    return-void
.end method


# virtual methods
.method public deleteContent()V
    .locals 1

    .prologue
    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;->mContentFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getContentLength()J
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;->mContentFile:Ljava/io/File;

    if-nez v0, :cond_0

    .line 36
    const-wide/16 v0, 0x0

    .line 37
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;->mContentFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;->mContentFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;->mContentFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 44
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;->mContentFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :goto_0
    return-object v1

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/io/FileNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method
