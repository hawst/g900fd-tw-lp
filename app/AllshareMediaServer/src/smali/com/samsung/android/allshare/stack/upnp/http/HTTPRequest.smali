.class public Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
.super Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;
.source "HTTPRequest.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HTTPRequest"


# instance fields
.field private mHttpSocket:Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

.field private mMethod:Ljava/lang/String;

.field private mPostSocket:Ljava/net/Socket;

.field private mRequestHost:Ljava/lang/String;

.field private mRequestPort:I

.field private mUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;-><init>()V

    .line 145
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mMethod:Ljava/lang/String;

    .line 192
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mUri:Ljava/lang/String;

    .line 266
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mRequestHost:Ljava/lang/String;

    .line 276
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mRequestPort:I

    .line 315
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mHttpSocket:Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    .line 477
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 120
    const-string v0, "USER-AGENT"

    sget-object v1, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->DEVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2
    .param p1, "addUserAgent"    # Z

    .prologue
    const/4 v1, 0x0

    .line 123
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;-><init>()V

    .line 145
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mMethod:Ljava/lang/String;

    .line 192
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mUri:Ljava/lang/String;

    .line 266
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mRequestHost:Ljava/lang/String;

    .line 276
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mRequestPort:I

    .line 315
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mHttpSocket:Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    .line 477
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 124
    if-eqz p1, :cond_0

    .line 125
    const-string v0, "USER-AGENT"

    sget-object v1, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->DEVICE_NAME:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    :cond_0
    return-void
.end method

.method private closeInputStream(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V
    .locals 3
    .param p1, "httpRes"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    .prologue
    .line 459
    if-nez p1, :cond_1

    .line 471
    :cond_0
    :goto_0
    return-void

    .line 462
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getContentInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 463
    .local v1, "is":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    .line 465
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getContentInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 466
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getFirstLineString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 369
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getMethod()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHTTPVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHTTPVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->hasFirstLine()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 364
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getFirstLineToken(I)Ljava/lang/String;

    move-result-object v0

    .line 365
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HTTP/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeader()Ljava/lang/String;
    .locals 3

    .prologue
    .line 377
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 379
    .local v1, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getFirstLineString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 381
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeaderString()Ljava/lang/String;

    move-result-object v0

    .line 382
    .local v0, "headerString":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 384
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public getLocalAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getSocket()Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->getLocalAddress()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mMethod:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mMethod:Ljava/lang/String;

    .line 154
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getFirstLineToken(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getParameterList()Lcom/samsung/android/allshare/stack/upnp/http/ParameterList;
    .locals 10

    .prologue
    .line 227
    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/http/ParameterList;

    invoke-direct {v5}, Lcom/samsung/android/allshare/stack/upnp/http/ParameterList;-><init>()V

    .line 228
    .local v5, "paramList":Lcom/samsung/android/allshare/stack/upnp/http/ParameterList;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v6

    .line 229
    .local v6, "uri":Ljava/lang/String;
    if-nez v6, :cond_1

    .line 245
    :cond_0
    return-object v5

    .line 232
    :cond_1
    const/16 v8, 0x3f

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 233
    .local v4, "paramIdx":I
    if-ltz v4, :cond_0

    .line 235
    :goto_0
    if-lez v4, :cond_0

    .line 236
    const/16 v8, 0x3d

    add-int/lit8 v9, v4, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 237
    .local v0, "eqIdx":I
    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v6, v8, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 238
    .local v1, "name":Ljava/lang/String;
    const/16 v8, 0x26

    add-int/lit8 v9, v0, 0x1

    invoke-virtual {v6, v8, v9}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 239
    .local v2, "nextParamIdx":I
    add-int/lit8 v9, v0, 0x1

    if-lez v2, :cond_2

    move v8, v2

    :goto_1
    invoke-virtual {v6, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    .line 241
    .local v7, "value":Ljava/lang/String;
    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/http/Parameter;

    invoke-direct {v3, v1, v7}, Lcom/samsung/android/allshare/stack/upnp/http/Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    .local v3, "param":Lcom/samsung/android/allshare/stack/upnp/http/Parameter;
    invoke-virtual {v5, v3}, Lcom/samsung/android/allshare/stack/upnp/http/ParameterList;->add(Ljava/lang/Object;)Z

    .line 243
    move v4, v2

    .line 244
    goto :goto_0

    .line 239
    .end local v3    # "param":Lcom/samsung/android/allshare/stack/upnp/http/Parameter;
    .end local v7    # "value":Ljava/lang/String;
    :cond_2
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    goto :goto_1
.end method

.method public getRequestAddress()Ljava/net/InetAddress;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 288
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mHttpSocket:Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    if-nez v2, :cond_1

    .line 295
    :cond_0
    :goto_0
    return-object v1

    .line 291
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mHttpSocket:Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    .line 292
    .local v0, "sock":Ljava/net/Socket;
    if-eqz v0, :cond_0

    .line 295
    invoke-virtual {v0}, Ljava/net/Socket;->getInetAddress()Ljava/net/InetAddress;

    move-result-object v1

    goto :goto_0
.end method

.method public getRequestHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mRequestHost:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestPort()I
    .locals 1

    .prologue
    .line 283
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mRequestPort:I

    return v0
.end method

.method public getSocket()Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mHttpSocket:Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    return-object v0
.end method

.method public getTargetHostPort()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 301
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mHttpSocket:Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    if-nez v2, :cond_1

    .line 308
    :cond_0
    :goto_0
    return v1

    .line 304
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mHttpSocket:Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->getSocket()Ljava/net/Socket;

    move-result-object v0

    .line 305
    .local v0, "sock":Ljava/net/Socket;
    if-eqz v0, :cond_0

    .line 308
    invoke-virtual {v0}, Ljava/net/Socket;->getPort()I

    move-result v1

    goto :goto_0
.end method

.method public getURI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 210
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mUri:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 211
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mUri:Ljava/lang/String;

    .line 219
    :cond_0
    :goto_0
    return-object v0

    .line 213
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getFirstLineToken(I)Ljava/lang/String;

    move-result-object v0

    .line 215
    .local v0, "uriTemp":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->isAbsoluteURL(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 216
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->toRelativeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 217
    goto :goto_0
.end method

.method public isGetRequest()Z
    .locals 1

    .prologue
    .line 165
    const-string v0, "GET"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isHeadRequest()Z
    .locals 1

    .prologue
    .line 173
    const-string v0, "HEAD"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isKeepAlive()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 392
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isCloseConnection()Z

    move-result v4

    if-ne v4, v3, :cond_1

    .line 400
    :cond_0
    :goto_0
    return v2

    .line 394
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isKeepAliveConnection()Z

    move-result v4

    if-ne v4, v3, :cond_2

    move v2, v3

    .line 395
    goto :goto_0

    .line 396
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHTTPVersion()Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "httpVer":Ljava/lang/String;
    const-string v4, "1.0"

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_3

    move v1, v3

    .line 398
    .local v1, "isHTTP10":Z
    :goto_1
    if-eq v1, v3, :cond_0

    move v2, v3

    .line 400
    goto :goto_0

    .end local v1    # "isHTTP10":Z
    :cond_3
    move v1, v2

    .line 397
    goto :goto_1
.end method

.method public isMethod(Ljava/lang/String;)Z
    .locals 2
    .param p1, "method"    # Ljava/lang/String;

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getMethod()Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "headerMethod":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 160
    const/4 v1, 0x0

    .line 161
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isNotifyRequest()Z
    .locals 1

    .prologue
    .line 185
    const-string v0, "NOTIFY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPostRequest()Z
    .locals 1

    .prologue
    .line 169
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSOAPAction()Z
    .locals 1

    .prologue
    .line 259
    const-string v0, "SOAPACTION"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->hasHeader(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isSubscribeRequest()Z
    .locals 1

    .prologue
    .line 177
    const-string v0, "SUBSCRIBE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isUnsubscribeRequest()Z
    .locals 1

    .prologue
    .line 181
    const-string v0, "UNSUBSCRIBE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isMethod(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public post(Ljava/lang/String;I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 577
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->post(Ljava/lang/String;IZ)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v0

    return-object v0
.end method

.method public post(Ljava/lang/String;IZ)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    .locals 18
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "isKeepAlive"    # Z

    .prologue
    .line 486
    new-instance v7, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    invoke-direct {v7}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;-><init>()V

    .line 488
    .local v7, "httpRes":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    const/4 v14, 0x1

    move/from16 v0, p3

    if-ne v0, v14, :cond_9

    const-string v14, "Keep-Alive"

    :goto_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->setConnection(Ljava/lang/String;)V

    .line 490
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isHeadRequest()Z

    move-result v10

    .line 492
    .local v10, "isHeaderRequest":Z
    const/4 v11, 0x0

    .line 493
    .local v11, "out":Ljava/io/OutputStream;
    const/4 v8, 0x0

    .line 494
    .local v8, "in":Ljava/io/InputStream;
    const/4 v12, 0x0

    .line 496
    .local v12, "pout":Ljava/io/PrintStream;
    :try_start_0
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getIPAsByteArray(Ljava/lang/String;)[B

    move-result-object v14

    invoke-static {v14}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v2

    .line 498
    .local v2, "addr":Ljava/net/InetAddress;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    if-nez v14, :cond_0

    .line 500
    new-instance v14, Ljava/net/Socket;

    move/from16 v0, p2

    invoke-direct {v14, v2, v0}, Ljava/net/Socket;-><init>(Ljava/net/InetAddress;I)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 501
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/net/Socket;->setTcpNoDelay(Z)V

    .line 503
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    const v15, 0x1d4c0

    invoke-virtual {v14, v15}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 504
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/net/Socket;->setReuseAddress(Z)V

    .line 505
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v11

    .line 506
    new-instance v13, Ljava/io/PrintStream;

    invoke-direct {v13, v11}, Ljava/io/PrintStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_8
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 507
    .end local v12    # "pout":Ljava/io/PrintStream;
    .local v13, "pout":Ljava/io/PrintStream;
    :try_start_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 508
    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 510
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isChunked()Z

    move-result v9

    .line 512
    .local v9, "isChunkedRequest":Z
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getContentString()Ljava/lang/String;

    move-result-object v4

    .line 513
    .local v4, "content":Ljava/lang/String;
    const/4 v5, 0x0

    .line 514
    .local v5, "contentLength":I
    if-eqz v4, :cond_1

    .line 515
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 517
    :cond_1
    if-lez v5, :cond_3

    .line 518
    const/4 v14, 0x1

    if-ne v9, v14, :cond_2

    .line 519
    int-to-long v14, v5

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    .line 520
    .local v3, "chunSizeBuf":Ljava/lang/String;
    invoke-virtual {v13, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 521
    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 523
    .end local v3    # "chunSizeBuf":Ljava/lang/String;
    :cond_2
    invoke-virtual {v13, v4}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 524
    const/4 v14, 0x1

    if-ne v9, v14, :cond_3

    .line 525
    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 528
    :cond_3
    const/4 v14, 0x1

    if-ne v9, v14, :cond_4

    .line 529
    const/16 v14, 0x30

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(C)V

    .line 530
    const-string v14, "\r\n"

    invoke-virtual {v13, v14}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 533
    :cond_4
    invoke-virtual {v13}, Ljava/io/PrintStream;->flush()V

    .line 535
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->isConnected()Z

    move-result v14

    if-eqz v14, :cond_5

    .line 538
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    .line 539
    invoke-virtual {v7, v8, v10}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->set(Ljava/io/InputStream;Z)Z
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_e
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_d
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 551
    :cond_5
    if-nez p3, :cond_7

    .line 552
    if-eqz v8, :cond_6

    .line 554
    :try_start_2
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 559
    :cond_6
    :goto_1
    if-eqz v11, :cond_7

    .line 561
    :try_start_3
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 562
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 566
    :goto_2
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 569
    :cond_7
    if-eqz v13, :cond_13

    .line 570
    invoke-virtual {v13}, Ljava/io/PrintStream;->close()V

    move-object v12, v13

    .line 573
    .end local v2    # "addr":Ljava/net/InetAddress;
    .end local v4    # "content":Ljava/lang/String;
    .end local v5    # "contentLength":I
    .end local v9    # "isChunkedRequest":Z
    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    :cond_8
    :goto_3
    return-object v7

    .line 488
    .end local v8    # "in":Ljava/io/InputStream;
    .end local v10    # "isHeaderRequest":Z
    .end local v11    # "out":Ljava/io/OutputStream;
    .end local v12    # "pout":Ljava/io/PrintStream;
    :cond_9
    const-string v14, "close"

    goto/16 :goto_0

    .line 555
    .restart local v2    # "addr":Ljava/net/InetAddress;
    .restart local v4    # "content":Ljava/lang/String;
    .restart local v5    # "contentLength":I
    .restart local v8    # "in":Ljava/io/InputStream;
    .restart local v9    # "isChunkedRequest":Z
    .restart local v10    # "isHeaderRequest":Z
    .restart local v11    # "out":Ljava/io/OutputStream;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catch_0
    move-exception v6

    .line 556
    .local v6, "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post - in.close - Exception"

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 563
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v6

    .line 564
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post -out/postScket .close - Exception"

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 541
    .end local v2    # "addr":Ljava/net/InetAddress;
    .end local v4    # "content":Ljava/lang/String;
    .end local v5    # "contentLength":I
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v9    # "isChunkedRequest":Z
    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    :catch_2
    move-exception v6

    .line 542
    .local v6, "e":Ljava/net/SocketTimeoutException;
    :goto_4
    const/16 v14, 0x19a

    :try_start_4
    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 543
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post -SocketTimeoutException "

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 551
    if-nez p3, :cond_b

    .line 552
    if-eqz v8, :cond_a

    .line 554
    :try_start_5
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 559
    .end local v6    # "e":Ljava/net/SocketTimeoutException;
    :cond_a
    :goto_5
    if-eqz v11, :cond_b

    .line 561
    :try_start_6
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 562
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 566
    :goto_6
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 569
    :cond_b
    if-eqz v12, :cond_8

    .line 570
    invoke-virtual {v12}, Ljava/io/PrintStream;->close()V

    goto :goto_3

    .line 555
    .restart local v6    # "e":Ljava/net/SocketTimeoutException;
    :catch_3
    move-exception v6

    .line 556
    .local v6, "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post - in.close - Exception"

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_5

    .line 563
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v6

    .line 564
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post -out/postScket .close - Exception"

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_6

    .line 544
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v6

    .line 545
    .local v6, "e":Ljava/net/SocketException;
    :goto_7
    const/16 v14, 0x19a

    :try_start_7
    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 546
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post -SocketException "

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 551
    if-nez p3, :cond_d

    .line 552
    if-eqz v8, :cond_c

    .line 554
    :try_start_8
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6

    .line 559
    .end local v6    # "e":Ljava/net/SocketException;
    :cond_c
    :goto_8
    if-eqz v11, :cond_d

    .line 561
    :try_start_9
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 562
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    .line 566
    :goto_9
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 569
    :cond_d
    if-eqz v12, :cond_8

    .line 570
    invoke-virtual {v12}, Ljava/io/PrintStream;->close()V

    goto/16 :goto_3

    .line 555
    .restart local v6    # "e":Ljava/net/SocketException;
    :catch_6
    move-exception v6

    .line 556
    .local v6, "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post - in.close - Exception"

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_8

    .line 563
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_7
    move-exception v6

    .line 564
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post -out/postScket .close - Exception"

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_9

    .line 547
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_8
    move-exception v6

    .line 548
    .restart local v6    # "e":Ljava/lang/Exception;
    :goto_a
    const/16 v14, 0x1f4

    :try_start_a
    invoke-virtual {v7, v14}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 549
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post - Exception"

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 551
    if-nez p3, :cond_f

    .line 552
    if-eqz v8, :cond_e

    .line 554
    :try_start_b
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_9

    .line 559
    :cond_e
    :goto_b
    if-eqz v11, :cond_f

    .line 561
    :try_start_c
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 562
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v14}, Ljava/net/Socket;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_a

    .line 566
    :goto_c
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 569
    :cond_f
    if-eqz v12, :cond_8

    .line 570
    invoke-virtual {v12}, Ljava/io/PrintStream;->close()V

    goto/16 :goto_3

    .line 555
    :catch_9
    move-exception v6

    .line 556
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post - in.close - Exception"

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_b

    .line 563
    :catch_a
    move-exception v6

    .line 564
    const-string v14, "HTTPRequest"

    const-string v15, "post"

    const-string v16, "post -out/postScket .close - Exception"

    move-object/from16 v0, v16

    invoke-static {v14, v15, v0, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_c

    .line 551
    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v14

    :goto_d
    if-nez p3, :cond_11

    .line 552
    if-eqz v8, :cond_10

    .line 554
    :try_start_d
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_b

    .line 559
    :cond_10
    :goto_e
    if-eqz v11, :cond_11

    .line 561
    :try_start_e
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    .line 562
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    invoke-virtual {v15}, Ljava/net/Socket;->close()V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_c

    .line 566
    :goto_f
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mPostSocket:Ljava/net/Socket;

    .line 569
    :cond_11
    if-eqz v12, :cond_12

    .line 570
    invoke-virtual {v12}, Ljava/io/PrintStream;->close()V

    :cond_12
    throw v14

    .line 555
    :catch_b
    move-exception v6

    .line 556
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v15, "HTTPRequest"

    const-string v16, "post"

    const-string v17, "post - in.close - Exception"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v15, v0, v1, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_e

    .line 563
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_c
    move-exception v6

    .line 564
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v15, "HTTPRequest"

    const-string v16, "post"

    const-string v17, "post -out/postScket .close - Exception"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-static {v15, v0, v1, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_f

    .line 551
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v2    # "addr":Ljava/net/InetAddress;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catchall_1
    move-exception v14

    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto :goto_d

    .line 547
    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catch_d
    move-exception v6

    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto/16 :goto_a

    .line 544
    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catch_e
    move-exception v6

    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto/16 :goto_7

    .line 541
    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :catch_f
    move-exception v6

    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto/16 :goto_4

    .end local v12    # "pout":Ljava/io/PrintStream;
    .restart local v4    # "content":Ljava/lang/String;
    .restart local v5    # "contentLength":I
    .restart local v9    # "isChunkedRequest":Z
    .restart local v13    # "pout":Ljava/io/PrintStream;
    :cond_13
    move-object v12, v13

    .end local v13    # "pout":Ljava/io/PrintStream;
    .restart local v12    # "pout":Ljava/io/PrintStream;
    goto/16 :goto_3
.end method

.method public post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z
    .locals 17
    .param p1, "httpRes"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    .prologue
    .line 423
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getSocket()Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    move-result-object v10

    .line 424
    .local v10, "httpSock":Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;
    const-wide/16 v12, 0x0

    .line 425
    .local v12, "offset":J
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getContentLength()J

    move-result-wide v8

    .line 426
    .local v8, "length":J
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->hasContentRange()Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-eqz v2, :cond_5

    .line 427
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getContentRangeFirstPosition()J

    move-result-wide v4

    .line 428
    .local v4, "firstPos":J
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getContentRangeLastPosition()J

    move-result-wide v6

    .line 430
    .local v6, "lastPos":J
    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-gtz v2, :cond_0

    .line 431
    const-wide/16 v2, 0x1

    sub-long v6, v8, v2

    .line 434
    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-gez v2, :cond_1

    .line 435
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->closeInputStream(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V

    .line 436
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    move-result v2

    .line 455
    .end local v4    # "firstPos":J
    .end local v6    # "lastPos":J
    :goto_0
    return v2

    .line 439
    .restart local v4    # "firstPos":J
    .restart local v6    # "lastPos":J
    :cond_1
    cmp-long v2, v4, v8

    if-gtz v2, :cond_2

    cmp-long v2, v6, v8

    if-lez v2, :cond_3

    .line 440
    :cond_2
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->closeInputStream(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V

    .line 441
    const/16 v2, 0x1a0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnResponse(I)Z

    move-result v2

    goto :goto_0

    .line 444
    :cond_3
    cmp-long v2, v4, v6

    if-lez v2, :cond_4

    .line 445
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->closeInputStream(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V

    .line 446
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    move-result v2

    goto :goto_0

    :cond_4
    move-object/from16 v3, p1

    .line 449
    invoke-virtual/range {v3 .. v9}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentRange(JJJ)V

    .line 450
    const/16 v2, 0xce

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 452
    move-wide v12, v4

    .line 453
    sub-long v2, v6, v4

    const-wide/16 v14, 0x1

    add-long v8, v2, v14

    .line 455
    .end local v4    # "firstPos":J
    .end local v6    # "lastPos":J
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isHeadRequest()Z

    move-result v16

    move-object/from16 v11, p1

    move-wide v14, v8

    invoke-virtual/range {v10 .. v16}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;JJZ)Z

    move-result v2

    goto :goto_0
.end method

.method public print()V
    .locals 3

    .prologue
    .line 630
    const-string v0, "HTTPRequest"

    const-string v1, "print"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    return-void
.end method

.method public readData()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 408
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getSocket()Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;->read(Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;)Z

    move-result v0

    return v0
.end method

.method public returnBadRequest()Z
    .locals 1

    .prologue
    .line 605
    const/16 v0, 0x190

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnResponse(I)Z

    move-result v0

    return v0
.end method

.method public returnNotFoundRequest()Z
    .locals 1

    .prologue
    .line 609
    const/16 v0, 0x194

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnResponse(I)Z

    move-result v0

    return v0
.end method

.method public returnOK()Z
    .locals 1

    .prologue
    .line 601
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnResponse(I)Z

    move-result v0

    return v0
.end method

.method public returnResponse(I)Z
    .locals 4
    .param p1, "statusCode"    # I

    .prologue
    .line 594
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;-><init>()V

    .line 595
    .local v0, "httpRes":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 596
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentLength(J)V

    .line 597
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    move-result v1

    return v1
.end method

.method public set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 1
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 585
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;)V

    .line 586
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getSocket()Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->setSocket(Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;)V

    .line 587
    return-void
.end method

.method public setMethod(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mMethod:Ljava/lang/String;

    .line 149
    return-void
.end method

.method protected setRequestHost(Ljava/lang/String;)V
    .locals 0
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 269
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mRequestHost:Ljava/lang/String;

    .line 270
    return-void
.end method

.method protected setRequestPort(I)V
    .locals 0
    .param p1, "host"    # I

    .prologue
    .line 279
    iput p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mRequestPort:I

    .line 280
    return-void
.end method

.method public setSocket(Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;)V
    .locals 0
    .param p1, "value"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    .prologue
    .line 318
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mHttpSocket:Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    .line 319
    return-void
.end method

.method public setURI(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 203
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->setURI(Ljava/lang/String;Z)V

    .line 204
    return-void
.end method

.method public setURI(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "isCheckRelativeURL"    # Z

    .prologue
    .line 195
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mUri:Ljava/lang/String;

    .line 196
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mUri:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->toRelativeURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->mUri:Ljava/lang/String;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 621
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 623
    .local v0, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 624
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 625
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 626
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
