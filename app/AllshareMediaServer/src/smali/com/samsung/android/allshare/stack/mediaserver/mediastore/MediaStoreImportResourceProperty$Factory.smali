.class public Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$Factory;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$DefaultImportResourcePropertyFactory;
.source "MediaStoreImportResourceProperty.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V
    .locals 1
    .param p1, "mediaStoreMediaServer"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$DefaultImportResourcePropertyFactory;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$Factory;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 34
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$Factory;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 35
    return-void
.end method


# virtual methods
.method public create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    .locals 3
    .param p1, "path"    # Ljava/io/File;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "parent"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .param p4, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .param p5, "tempFile"    # Ljava/io/File;

    .prologue
    .line 40
    invoke-super/range {p0 .. p5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$DefaultImportResourcePropertyFactory;->create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;

    move-result-object v0

    .line 42
    .local v0, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    if-nez v0, :cond_0

    .line 43
    const/4 v1, 0x0

    .line 45
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$Factory;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-direct {v1, v0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V

    goto :goto_0
.end method

.method public create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    .locals 3
    .param p1, "path"    # Ljava/io/File;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "parent"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .param p4, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .param p5, "is"    # Ljava/io/InputStream;

    .prologue
    .line 51
    invoke-super/range {p0 .. p5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$DefaultImportResourcePropertyFactory;->create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;

    move-result-object v0

    .line 53
    .local v0, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    if-nez v0, :cond_0

    .line 54
    const/4 v1, 0x0

    .line 56
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$Factory;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-direct {v1, v0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V

    goto :goto_0
.end method

.method public create(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "parent"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .param p3, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .param p4, "file"    # Ljava/io/File;

    .prologue
    .line 62
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$DefaultImportResourcePropertyFactory;->create(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;

    move-result-object v0

    .line 64
    .local v0, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    if-nez v0, :cond_0

    .line 65
    const/4 v1, 0x0

    .line 67
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$Factory;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-direct {v1, v0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V

    goto :goto_0
.end method
