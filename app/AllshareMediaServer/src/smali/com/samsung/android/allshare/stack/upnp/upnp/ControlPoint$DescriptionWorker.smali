.class Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DescriptionWorker"
.end annotation


# instance fields
.field private mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

.field private mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

.field final synthetic this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V
    .locals 1
    .param p2, "ssdpPacket"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    .param p3, "discoveryInfo"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .prologue
    const/4 v0, 0x0

    .line 903
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 899
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .line 901
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 904
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .line 905
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 906
    return-void
.end method


# virtual methods
.method public run()V
    .locals 17

    .prologue
    .line 909
    const/4 v9, 0x0

    .line 910
    .local v9, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    invoke-virtual {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 911
    .local v2, "location":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-virtual {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getNetworkInterfaceType()Ljava/lang/String;

    move-result-object v5

    .line 914
    .local v5, "nicType":Ljava/lang/String;
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 915
    .local v3, "locationUrl":Ljava/net/URL;
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v6

    .line 917
    .local v6, "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    const-string v11, "DEFAULT_WIFI_INTERFACE"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 918
    const-string v11, "ControlPoint"

    const-string v12, "run"

    const-string v13, "nicType: WIFI_INTERFACE"

    invoke-static {v11, v12, v13}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    const/4 v11, 0x1

    invoke-virtual {v6, v3, v11}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/net/URL;Z)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 941
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 942
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    new-instance v12, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-direct {v12, v13, v14, v9, v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 945
    .end local v3    # "locationUrl":Ljava/net/URL;
    .end local v6    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :cond_1
    :goto_1
    return-void

    .line 920
    .restart local v3    # "locationUrl":Ljava/net/URL;
    .restart local v6    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :cond_2
    :try_start_1
    const-string v11, "DEFAULT_P2P_INTERFACE"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 921
    invoke-static {}, Landroid/net/Proxy;->getDefaultHost()Ljava/lang/String;

    move-result-object v8

    .line 923
    .local v8, "proxyHost":Ljava/lang/String;
    if-nez v8, :cond_3

    .line 924
    const-string v11, "ControlPoint"

    const-string v12, "run"

    const-string v13, "nicType: P2P_INTERFACE : no proxy"

    invoke-static {v11, v12, v13}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 925
    const/4 v11, 0x1

    invoke-virtual {v6, v3, v11}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/net/URL;Z)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v9

    goto :goto_0

    .line 927
    :cond_3
    const-string v11, "ControlPoint"

    const-string v12, "run"

    const-string v13, "nicType: P2P_INTERFACE : proxy"

    invoke-static {v11, v12, v13}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 928
    const/4 v11, 0x0

    invoke-virtual {v6, v3, v11}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/net/URL;Z)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v9

    goto :goto_0

    .line 932
    .end local v3    # "locationUrl":Ljava/net/URL;
    .end local v6    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    .end local v8    # "proxyHost":Ljava/lang/String;
    :catch_0
    move-exception v10

    .line 933
    .local v10, "se":Ljava/net/SocketTimeoutException;
    :try_start_2
    const-string v11, "ControlPoint"

    const-string v12, "run"

    const-string v13, "run SocketTimeoutException"

    invoke-static {v11, v12, v13, v10}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 941
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 942
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    new-instance v12, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-direct {v12, v13, v14, v9, v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 934
    .end local v10    # "se":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v1

    .line 935
    .local v1, "e":Ljava/net/SocketException;
    :try_start_3
    const-string v11, "ControlPoint"

    const-string v12, "run"

    const-string v13, "run SocketException"

    invoke-static {v11, v12, v13, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 941
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 942
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    new-instance v12, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-direct {v12, v13, v14, v9, v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 936
    .end local v1    # "e":Ljava/net/SocketException;
    :catch_2
    move-exception v4

    .line 937
    .local v4, "me":Ljava/net/MalformedURLException;
    :try_start_4
    const-string v11, "ControlPoint"

    const-string v12, "run"

    const-string v13, "run MalformedURLException"

    invoke-static {v11, v12, v13, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 941
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 942
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    new-instance v12, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-direct {v12, v13, v14, v9, v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 938
    .end local v4    # "me":Ljava/net/MalformedURLException;
    :catch_3
    move-exception v7

    .line 939
    .local v7, "pe":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    :try_start_5
    const-string v11, "ControlPoint"

    const-string v12, "run"

    const-string v13, "run ParserException"

    invoke-static {v11, v12, v13, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 941
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    if-eqz v11, :cond_1

    .line 942
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v11

    new-instance v12, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-direct {v12, v13, v14, v9, v15}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    invoke-virtual {v11, v12}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_1

    .line 941
    .end local v7    # "pe":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    :catchall_0
    move-exception v11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v12

    if-eqz v12, :cond_4

    .line 942
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v12

    new-instance v13, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mSsdpPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v13, v14, v15, v9, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    invoke-virtual {v12, v13}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_4
    throw v11
.end method
