.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;
.super Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
.source "SSDPRequest.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;-><init>()V

    .line 37
    const-string v0, "1.1"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->setVersion(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "addUserAgent"    # Z

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;-><init>(Z)V

    .line 42
    const-string v0, "1.1"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->setVersion(Ljava/lang/String;)V

    .line 43
    return-void
.end method


# virtual methods
.method public getLeaseTime()I
    .locals 2

    .prologue
    .line 102
    const-string v1, "Cache-Control"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "cacheCtrl":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDP;->getLeaseTime(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public getLocation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const-string v0, "LOCATION"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "NT"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNTS()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string v0, "NTS"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUSN()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const-string v0, "USN"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setLeaseTime(I)V
    .locals 3
    .param p1, "len"    # I

    .prologue
    .line 98
    const-string v0, "Cache-Control"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "max-age="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 74
    const-string v0, "LOCATION"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public setNT(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 50
    const-string v0, "NT"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public setNTS(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 62
    const-string v0, "NTS"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public setUSN(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 86
    const-string v0, "USN"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method
