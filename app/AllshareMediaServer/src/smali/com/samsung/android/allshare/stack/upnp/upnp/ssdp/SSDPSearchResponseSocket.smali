.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;
.source "SSDPSearchResponseSocket.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mDeviceSearchResponseThread:Ljava/lang/Thread;

.field private mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

.field private mSearchResponseListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 44
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mSearchResponseListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .line 61
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mDeviceSearchResponseThread:Ljava/lang/Thread;

    .line 114
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 45
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mSearchResponseListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .line 46
    return-void
.end method


# virtual methods
.method public getDiscoveryInfo()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    return-object v0
.end method

.method protected notifySearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 2
    .param p1, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mSearchResponseListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mSearchResponseListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;->onSSDPReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    .line 55
    :cond_0
    return-void
.end method

.method public post(Ljava/net/InetAddress;ILcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;)Z
    .locals 1
    .param p1, "dest"    # Ljava/net/InetAddress;
    .param p2, "port"    # I
    .param p3, "req"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;

    .prologue
    .line 108
    invoke-virtual {p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->post(Ljava/net/InetAddress;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public post(Ljava/net/InetAddress;ILcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;)Z
    .locals 1
    .param p1, "dest"    # Ljava/net/InetAddress;
    .param p2, "port"    # I
    .param p3, "res"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;

    .prologue
    .line 104
    invoke-virtual {p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->getHeader()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->post(Ljava/net/InetAddress;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 64
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 67
    .local v1, "thisThread":Ljava/lang/Thread;
    :goto_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mDeviceSearchResponseThread:Ljava/lang/Thread;

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mDeviceSearchResponseThread:Ljava/lang/Thread;

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 68
    invoke-static {}, Ljava/lang/Thread;->yield()V

    .line 69
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->receive()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 70
    .local v0, "packet":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    if-nez v0, :cond_1

    .line 75
    .end local v0    # "packet":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    :cond_0
    return-void

    .line 73
    .restart local v0    # "packet":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    :cond_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->notifySearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    goto :goto_0
.end method

.method public setDiscoveryInfo(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 118
    return-void
.end method

.method public setSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mSearchResponseListener:Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .line 50
    return-void
.end method

.method public start(I)V
    .locals 5
    .param p1, "mx"    # I

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->getSocket()Ljava/net/DatagramSocket;

    move-result-object v1

    .line 80
    .local v1, "sock":Ljava/net/DatagramSocket;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/net/DatagramSocket;->isClosed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 94
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    mul-int/lit16 v2, p1, 0x3e8

    :try_start_0
    invoke-virtual {v1, v2}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    .line 85
    new-instance v2, Ljava/lang/Thread;

    invoke-direct {v2, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mDeviceSearchResponseThread:Ljava/lang/Thread;

    .line 86
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mDeviceSearchResponseThread:Ljava/lang/Thread;

    const-string v3, "Search Response Listener"

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 90
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mDeviceSearchResponseThread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/net/SocketException;
    const-string v2, "SSDPSearchResponseSocket"

    const-string v3, "start"

    const-string v4, "SocketException - "

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->mDeviceSearchResponseThread:Ljava/lang/Thread;

    .line 98
    return-void
.end method
