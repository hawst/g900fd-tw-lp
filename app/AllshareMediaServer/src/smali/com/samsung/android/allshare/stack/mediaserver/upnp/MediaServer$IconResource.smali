.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;
.super Ljava/lang/Object;
.source "MediaServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "IconResource"
.end annotation


# instance fields
.field private mByteArray:[B

.field private mMimeType:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 702
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 703
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mByteArray:[B

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$1;

    .prologue
    .line 702
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;

    .prologue
    .line 702
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 702
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mMimeType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;)[B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;

    .prologue
    .line 702
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mByteArray:[B

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;
    .param p1, "x1"    # [B

    .prologue
    .line 702
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mByteArray:[B

    return-object p1
.end method
