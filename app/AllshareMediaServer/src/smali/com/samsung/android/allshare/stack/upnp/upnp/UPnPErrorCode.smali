.class public Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPErrorCode;
.super Ljava/lang/Object;
.source "UPnPErrorCode.java"


# static fields
.field public static final ACTIONFAILEDRESPONSE:I = 0x1f5

.field public static final APINOTSUPPORTED:I = -0x2

.field public static final BADRESPONSE:I = 0x7

.field public static final BUSY:I = 0xe

.field public static final CONTENTSNOTSELECTED:I = 0x11

.field public static final DEVICEGONE:I = 0x3

.field public static final DEVICENOTACTIVATED:I = 0x4

.field public static final DMRGONE:I = 0x9

.field public static final DMRNOTSELECTED:I = 0xc

.field public static final DMSGONE:I = 0xa

.field public static final DMSNOTSELECTED:I = 0xb

.field public static final DMSRUNNING:I = 0xd

.field public static final INVALIDACTION:I = 0x191

.field public static final INVALIDINDEX:I = 0x1

.field public static final INVALIDPARAM:I = 0x2

.field public static final INVALIDUPNPDEVICE:I = 0x5

.field public static final INVALIDVAR:I = 0x194

.field public static final NOERROR:I = 0x0

.field public static final NORESPONSE:I = 0x6

.field public static final OUTOFMEMORY:I = 0x8

.field public static final OUTOFSYNC:I = 0x193

.field public static final PRECONDITIONFAILED:I = 0x19c

.field public static final RESNOTFOUND:I = 0xf

.field public static final UNKNOWN:I = -0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
