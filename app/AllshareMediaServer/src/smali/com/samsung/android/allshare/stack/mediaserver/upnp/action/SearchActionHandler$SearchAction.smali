.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;
.source "SearchActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchAction"
.end annotation


# static fields
.field public static final CONTAINER_ID:Ljava/lang/String; = "ContainerID"

.field public static final SEARCH_CRITERIA:Ljava/lang/String; = "SearchCriteria"


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V
    .locals 0
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 64
    return-void
.end method


# virtual methods
.method public getContainerID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const-string v0, "ContainerID"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSearchCriteria()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string v0, "SearchCriteria"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
