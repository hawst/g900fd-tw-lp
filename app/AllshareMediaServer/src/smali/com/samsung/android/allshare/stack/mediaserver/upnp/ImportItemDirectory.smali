.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
.source "ImportItemDirectory.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;
    }
.end annotation


# instance fields
.field private mAutoDestroyWorker:Ljava/lang/Thread;

.field private mDestroyListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;

.field private mMutex:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)V
    .locals 4
    .param p1, "cdir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .prologue
    const/4 v3, 0x0

    .line 34
    const-string v0, "import"

    const-string v1, "import"

    const-string v2, "*/*"

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mMutex:Ljava/lang/Object;

    .line 31
    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mDestroyListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;

    .line 72
    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mAutoDestroyWorker:Ljava/lang/Thread;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;)Ljava/lang/Thread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mAutoDestroyWorker:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;
    .param p1, "x1"    # Ljava/lang/Thread;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mAutoDestroyWorker:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mMutex:Ljava/lang/Object;

    return-object v0
.end method

.method private startAutoDestroy()V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mAutoDestroyWorker:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mAutoDestroyWorker:Ljava/lang/Thread;

    .line 142
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mAutoDestroyWorker:Ljava/lang/Thread;

    const-string v1, "ImportItem Auoto Destroyer"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mAutoDestroyWorker:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 145
    :cond_0
    return-void
.end method


# virtual methods
.method public addItemObject(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V
    .locals 2
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .prologue
    .line 53
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    if-eqz v0, :cond_0

    .line 54
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 55
    :try_start_0
    invoke-super {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->addItemObject(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V

    .line 56
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->startAutoDestroy()V

    .line 57
    monitor-exit v1

    .line 59
    :cond_0
    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected notifyItemDestroyEvent(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mDestroyListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mDestroyListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;->onDestroy(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V

    .line 44
    :cond_0
    return-void
.end method

.method public removeItemObject(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V
    .locals 1
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .prologue
    .line 63
    if-nez p1, :cond_1

    .line 70
    .end local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    :cond_0
    :goto_0
    return-void

    .line 66
    .restart local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->removeItemObject(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V

    .line 68
    instance-of v0, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    if-eqz v0, :cond_0

    .line 69
    check-cast p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .end local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->notifyItemDestroyEvent(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;)V

    goto :goto_0
.end method

.method public setImportItemDestroyListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mDestroyListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;

    .line 39
    return-void
.end method

.method public update()Z
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return v0
.end method
