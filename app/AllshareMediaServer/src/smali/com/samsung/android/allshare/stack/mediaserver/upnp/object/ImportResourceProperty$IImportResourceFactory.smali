.class public interface abstract Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;
.super Ljava/lang/Object;
.source "ImportResourceProperty.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IImportResourceFactory"
.end annotation


# virtual methods
.method public abstract create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
.end method

.method public abstract create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
.end method

.method public abstract create(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
.end method

.method public abstract createFileName(Ljava/io/File;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Ljava/lang/String;
.end method
