.class public final Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
.super Ljava/lang/Object;
.source "ContentDirectoryService.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;


# static fields
.field private static final CHECK_CONTENTS:I = 0x3ea

.field private static final CHECK_SYSTEM_ID:I = 0x3e9

.field private static final DEFAULT_SYSTEMUPDATEID_INTERVAL:I = 0x7d0

.field public static final SERVICE_TYPE:Ljava/lang/String; = "urn:schemas-upnp-org:service:ContentDirectory"


# instance fields
.field private mCdsEventHandler:Landroid/os/Handler;

.field private mCdsEventThread:Landroid/os/HandlerThread;

.field private mDirList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;",
            ">;"
        }
    .end annotation
.end field

.field private mImportItemContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

.field private mLastSystemUpdateID:I

.field private mLastSystemUpdateTime:J

.field private mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

.field private mRootContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

.field private mSoapActionHandlers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mSystemUpdateID:I

.field private mSystemUpdateIDInterval:J

.field private mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

.field private mVarSystemUpdateID:Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V
    .locals 4
    .param p1, "server"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    .line 108
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .line 148
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mImportItemContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mDirList:Ljava/util/ArrayList;

    .line 266
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventThread:Landroid/os/HandlerThread;

    .line 268
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    .line 270
    iput v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateID:I

    .line 272
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateTime:J

    .line 274
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mVarSystemUpdateID:Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    .line 54
    if-nez p1, :cond_0

    .line 65
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->setMediaServer(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V

    .line 58
    const-string v0, "urn:schemas-upnp-org:service:ContentDirectory"

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getService(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->setUPnPService(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V

    .line 60
    iput v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSystemUpdateID:I

    .line 61
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSystemUpdateIDInterval:J

    .line 63
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->initRootDirectory()V

    .line 64
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->initActionHandlers()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateTime:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .param p1, "x1"    # J

    .prologue
    .line 38
    iput-wide p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateTime:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateID:I

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .param p1, "x1"    # I

    .prologue
    .line 38
    iput p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateID:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mVarSystemUpdateID:Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    return-object v0
.end method

.method private initActionHandlers()V
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "Browse"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getMediaServer()Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "Search"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getMediaServer()Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "GetSearchCapabilities"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchCapabilitiesActionHandler;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchCapabilitiesActionHandler;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "GetSortCapabilities"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SortCapabilitiesActionHandler;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SortCapabilitiesActionHandler;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "GetSystemUpdateID"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SystemUpdateIdActionHandler;

    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SystemUpdateIdActionHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "CreateObject"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getMediaServer()Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "DestroyObject"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/DestroyObjectActionHandler;

    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/DestroyObjectActionHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "X_GetFeatureList"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;

    invoke-direct {v2, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    return-void
.end method

.method private initRootDirectory()V
    .locals 2

    .prologue
    .line 151
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    const-string v1, "Root"

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mRootContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    .line 152
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mImportItemContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    .line 153
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mRootContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mImportItemContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;->addDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)V

    .line 154
    return-void
.end method

.method private setMediaServer(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V
    .locals 0
    .param p1, "mserver"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 99
    return-void
.end method

.method private setUPnPService(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V
    .locals 0
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .line 112
    if-eqz p1, :cond_0

    .line 113
    invoke-virtual {p1, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    .line 114
    :cond_0
    return-void
.end method


# virtual methods
.method public actionControlHandled(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 1
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 343
    const/4 v0, 0x0

    return v0
.end method

.method public actionControlReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 4
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/4 v2, 0x0

    .line 243
    if-nez p1, :cond_1

    .line 251
    :cond_0
    :goto_0
    return v2

    .line 245
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "actionName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;

    .line 248
    .local v1, "handler":Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;
    if-eqz v1, :cond_0

    .line 249
    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;->handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z

    move-result v2

    goto :goto_0
.end method

.method public addDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z
    .locals 2
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .prologue
    const/4 v0, 0x0

    .line 167
    if-nez p1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return v0

    .line 169
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mDirList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 172
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mDirList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mRootContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;->addDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)V

    .line 174
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->update()Z

    .line 175
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .locals 2
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getRootDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;->getDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v0

    .line 225
    .local v0, "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mImportItemContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 226
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mImportItemContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    .line 227
    :cond_0
    return-object v0
.end method

.method public findDirectoryByMimeType(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getRootDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getRootDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;->getDirectoryByMimeType(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v0

    .line 234
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findObjectByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 203
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_2

    :cond_0
    move-object v1, v2

    .line 213
    :cond_1
    :goto_0
    return-object v1

    .line 205
    :cond_2
    const/4 v1, 0x0

    .line 206
    .local v1, "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getRootDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;->getItemByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-result-object v1

    .line 207
    if-nez v1, :cond_1

    .line 209
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getRootDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;->getDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v0

    .line 210
    .local v0, "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-eqz v0, :cond_3

    .line 211
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->toContentNode()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v1, v2

    .line 213
    goto :goto_0
.end method

.method public getActionHandler(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;
    .locals 1
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;

    return-object v0
.end method

.method public getImportDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mImportItemContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    return-object v0
.end method

.method public getMediaServer()Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    return-object v0
.end method

.method public getRootDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mRootContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    return-object v0
.end method

.method public declared-synchronized getSystemUpdateID()I
    .locals 1

    .prologue
    .line 139
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSystemUpdateID:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSystemUpdateIDInterval()J
    .locals 2

    .prologue
    .line 263
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSystemUpdateIDInterval:J

    return-wide v0
.end method

.method public isContainedDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z
    .locals 1
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .prologue
    .line 192
    if-nez p1, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 195
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mDirList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V
    .locals 1
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "handler"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSoapActionHandlers:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    return-void
.end method

.method public removeDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z
    .locals 2
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .prologue
    const/4 v0, 0x0

    .line 179
    if-nez p1, :cond_1

    .line 188
    :cond_0
    :goto_0
    return v0

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mDirList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mDirList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 183
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mRootContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/RootDirectory;->removeDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)V

    .line 184
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->updateSystemUpdateID()V

    .line 185
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setImportItemDestroyListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mImportItemContainer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->setImportItemDestroyListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;)V

    .line 121
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 278
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "CDS Eventing"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventThread:Landroid/os/HandlerThread;

    .line 280
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    if-nez v0, :cond_1

    .line 322
    :cond_0
    :goto_0
    return-void

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    const-string v1, "SystemUpdateID"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getStateVariable(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mVarSystemUpdateID:Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    .line 284
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mVarSystemUpdateID:Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mVarSystemUpdateID:Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setValue(I)V

    .line 288
    iput v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateID:I

    .line 289
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 290
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    .line 321
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 328
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    const/16 v1, 0x3ea

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 329
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 333
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    if-eqz v0, :cond_2

    .line 337
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->clearSubscriber()V

    .line 338
    :cond_2
    return-void
.end method

.method public updateDirectories()V
    .locals 3

    .prologue
    .line 347
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mDirList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .line 348
    .local v0, "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->update()Z

    goto :goto_0

    .line 350
    .end local v0    # "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    :cond_0
    return-void
.end method

.method public declared-synchronized updateSystemUpdateID()V
    .locals 2

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSystemUpdateID:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mSystemUpdateID:I

    .line 131
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->start()V

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 135
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mCdsEventHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    monitor-exit p0

    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
