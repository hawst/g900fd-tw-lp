.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SystemUpdateIdActionHandler;
.super Ljava/lang/Object;
.source "SystemUpdateIdActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# instance fields
.field private mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)V
    .locals 1
    .param p1, "cds"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SystemUpdateIdActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .line 28
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SystemUpdateIdActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .line 29
    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 8
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/16 v7, 0x192

    const/4 v4, 0x0

    .line 33
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionRequest()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 34
    .local v1, "args":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    .line 37
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 38
    :cond_1
    invoke-virtual {p1, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    .line 49
    .end local v0    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :goto_0
    return v4

    .line 42
    :cond_2
    const-string v5, "Id"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v3

    .line 43
    .local v3, "idArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v3, :cond_3

    .line 44
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SystemUpdateIdActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getSystemUpdateID()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 49
    const/4 v4, 0x1

    goto :goto_0

    .line 46
    :cond_3
    invoke-virtual {p1, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0
.end method
