.class public Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
.super Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
.source "SubscriptionResponse.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;-><init>()V

    .line 35
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getServerName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setServer(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V
    .locals 0
    .param p1, "httpRes"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V

    .line 40
    return-void
.end method


# virtual methods
.method public getSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    const-string v0, "SID"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTimeout()J
    .locals 2

    .prologue
    .line 82
    const-string v0, "TIMEOUT"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscription;->getTimeout(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public setErrorResponse(I)V
    .locals 2
    .param p1, "code"    # I

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setStatusCode(I)V

    .line 58
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setContentLength(J)V

    .line 59
    return-void
.end method

.method public setSID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 66
    const-string v0, "SID"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method public setTimeout(J)V
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 78
    const-string v0, "TIMEOUT"

    invoke-static {p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscription;->toTimeoutHeaderString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method
