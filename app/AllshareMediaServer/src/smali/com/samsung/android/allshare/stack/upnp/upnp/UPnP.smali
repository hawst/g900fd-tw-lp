.class public Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;
.super Ljava/lang/Object;
.source "UPnP.java"


# static fields
.field public static final DEFAULT_EXPIRED_DEVICE_EXTRA_TIME:I = 0x1

.field public static final INMPR03:Ljava/lang/String; = "INMPR03"

.field public static final INMPR03_DISCOVERY_OVER_WIRELESS_COUNT:I = 0x4

.field public static final INMPR03_VERSION:Ljava/lang/String; = "1.0"

.field private static IP4_MULTIGROUPADDR:Ljava/net/InetAddress; = null

.field private static IP6_MULTIGROUPADDR:Ljava/net/InetAddress; = null

.field public static final NAME:Ljava/lang/String; = "Samsung DOA"

.field public static final SERVER_RETRY_COUNT:I = 0x64

.field public static final USE_IPV6_ADMINISTRATIVE_SCOPE:I = 0x5

.field public static final USE_IPV6_GLOBAL_SCOPE:I = 0x7

.field public static final USE_IPV6_LINK_LOCAL_SCOPE:I = 0x3

.field public static final USE_IPV6_SITE_LOCAL_SCOPE:I = 0x6

.field public static final USE_IPV6_SUBNET_SCOPE:I = 0x4

.field public static final USE_LOOPBACK_ADDR:I = 0x2

.field public static final USE_ONLY_IPV4_ADDR:I = 0x9

.field public static final USE_ONLY_IPV6_ADDR:I = 0x1

.field public static final USE_SSDP_SEARCHRESPONSE_MULTIPLE_INTERFACES:I = 0x8

.field public static final VERSION:Ljava/lang/String; = "1.5"

.field public static final XML_DECLARATION:Ljava/lang/String; = "<?xml version=\"1.0\"?>"

.field private static xmlParser:Lcom/samsung/android/allshare/stack/upnp/xml/Parser;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/parser/DoaXMLParser;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/parser/DoaXMLParser;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->xmlParser:Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    .line 156
    :try_start_0
    const-string v0, "239.255.255.250"

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getIPAsByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->IP4_MULTIGROUPADDR:Ljava/net/InetAddress;

    .line 158
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDP;->getIPv6Address()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getIPAsByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->IP6_MULTIGROUPADDR:Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 162
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generateRandomUPnPUUID()Ljava/lang/String;
    .locals 10

    .prologue
    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 111
    .local v0, "time1":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-double v6, v6

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-long v2, v6

    .line 112
    .local v2, "time2":J
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-wide/32 v6, 0xffff

    and-long/2addr v6, v0

    long-to-int v6, v6

    invoke-static {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->toUUID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/32 v6, 0xfff0

    and-long/2addr v6, v0

    long-to-int v6, v6

    invoke-static {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->toUUID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x20

    shr-long v6, v0, v6

    const-wide/32 v8, 0xa000

    or-long/2addr v6, v8

    long-to-int v6, v6

    const v7, 0xffff

    and-int/2addr v6, v7

    invoke-static {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->toUUID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x20

    shr-long v6, v0, v6

    const-wide/32 v8, 0xa000

    or-long/2addr v6, v8

    long-to-int v6, v6

    const v7, 0xfff1

    and-int/2addr v6, v7

    invoke-static {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->toUUID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-wide/32 v6, 0xffff

    and-long/2addr v6, v2

    long-to-int v6, v6

    invoke-static {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->toUUID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x20

    shr-long v6, v2, v6

    const-wide/32 v8, 0xe000

    or-long/2addr v6, v8

    long-to-int v6, v6

    const v7, 0xfff1

    and-int/2addr v6, v7

    invoke-static {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->toUUID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x20

    shr-long v6, v2, v6

    const-wide/32 v8, 0xe000

    or-long/2addr v6, v8

    long-to-int v6, v6

    const v7, 0xfff2

    and-int/2addr v6, v7

    invoke-static {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->toUUID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x20

    shr-long v6, v2, v6

    const-wide/32 v8, 0xe000

    or-long/2addr v6, v8

    long-to-int v6, v6

    const v7, 0xffff

    and-int/2addr v6, v7

    invoke-static {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->toUUID(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 120
    .local v4, "uuid":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "uuid:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-static {v6}, Ljava/util/UUID;->nameUUIDFromBytes([B)Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static generateUPnPUUID(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "keyword"    # Ljava/lang/String;

    .prologue
    .line 124
    if-nez p0, :cond_0

    .line 125
    const-string p0, ""

    .line 127
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uuid:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-static {v2}, Ljava/util/UUID;->nameUUIDFromBytes([B)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "result":Ljava/lang/String;
    return-object v0
.end method

.method public static final getHostURL(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "host"    # Ljava/lang/String;
    .param p1, "port"    # I
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 177
    move-object v0, p0

    .line 178
    .local v0, "hostAddr":Ljava/lang/String;
    invoke-static {p0}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->isIPv6Address(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 180
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static final getIP4MultiGroupAddr()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->IP4_MULTIGROUPADDR:Ljava/net/InetAddress;

    return-object v0
.end method

.method public static final getIP6MultiGroupAddr()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 169
    sget-object v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->IP6_MULTIGROUPADDR:Ljava/net/InetAddress;

    return-object v0
.end method

.method public static final getServerName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 60
    const-string v2, "os.name"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "osName":Ljava/lang/String;
    const-string v2, "os.version"

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "osVer":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " UPnP/1.0 "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Samsung DOA"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "1.5"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static final getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->xmlParser:Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    return-object v0
.end method

.method public static final setXMLParser(Lcom/samsung/android/allshare/stack/upnp/xml/Parser;)V
    .locals 0
    .param p0, "parser"    # Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    .prologue
    .line 138
    sput-object p0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->xmlParser:Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    .line 139
    invoke-static {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAP;->setXMLParser(Lcom/samsung/android/allshare/stack/upnp/xml/Parser;)V

    .line 140
    return-void
.end method

.method private static final toUUID(I)Ljava/lang/String;
    .locals 6
    .param p0, "seed"    # I

    .prologue
    .line 100
    const v4, 0xffff

    and-int/2addr v4, p0

    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "id":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    .line 102
    .local v1, "idLen":I
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 103
    .local v3, "uuid":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    rsub-int/lit8 v4, v1, 0x4

    if-ge v2, v4, :cond_0

    .line 104
    const-string v4, "0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 103
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 105
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 106
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
