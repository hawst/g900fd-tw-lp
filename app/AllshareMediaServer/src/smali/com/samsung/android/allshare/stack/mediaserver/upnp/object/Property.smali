.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
.super Ljava/lang/Object;
.source "Property.java"


# instance fields
.field private mAttributeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mName:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mValue:Ljava/lang/String;

    .line 134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mAttributeMap:Ljava/util/HashMap;

    .line 52
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mName:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mValue:Ljava/lang/String;

    .line 134
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mAttributeMap:Ljava/util/HashMap;

    .line 40
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mName:Ljava/lang/String;

    .line 41
    if-nez p2, :cond_0

    .line 42
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mValue:Ljava/lang/String;

    .line 45
    :goto_0
    return-void

    .line 44
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mValue:Ljava/lang/String;

    goto :goto_0
.end method

.method public static createProperty(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .locals 6
    .param p0, "propNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 61
    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>()V

    .line 63
    .local v1, "cp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setName(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setValue(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNAttributes()I

    move-result v3

    .line 67
    .local v3, "nAttr":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 68
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttribute(I)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    move-result-object v0

    .line 69
    .local v0, "attr":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 70
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 67
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 73
    .end local v0    # "attr":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    :cond_1
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ValueValidator;->isValidContentProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 76
    .end local v1    # "cp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    :goto_1
    return-object v1

    .restart local v1    # "cp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getAttributeValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 169
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->hasAttribute(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mAttributeMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 172
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public getAttributes()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mAttributeMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public hasAttribute(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mAttributeMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public hasAttributes()Z
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mAttributeMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeAttribute(Ljava/lang/String;)Z
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mAttributeMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 156
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    :cond_0
    const/4 v0, 0x0

    .line 159
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mAttributeMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method protected setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 91
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mName:Ljava/lang/String;

    goto :goto_0
.end method

.method protected setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 113
    if-nez p1, :cond_0

    .line 117
    :goto_0
    return-void

    .line 116
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->mValue:Ljava/lang/String;

    goto :goto_0
.end method
