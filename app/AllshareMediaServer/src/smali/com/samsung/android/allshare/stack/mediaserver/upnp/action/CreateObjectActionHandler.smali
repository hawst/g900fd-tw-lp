.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;
.super Ljava/lang/Object;
.source "CreateObjectActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$1;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;
    }
.end annotation


# static fields
.field private static final SEC_DEVICENAME:Ljava/lang/String; = "sec:uploadDeviceName"


# instance fields
.field private mOnUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

.field private mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V
    .locals 1
    .param p1, "mediaServer"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .prologue
    const/4 v0, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 96
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mOnUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    .line 99
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 103
    instance-of v0, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    if-eqz v0, :cond_0

    .line 104
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mOnUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    .line 105
    :cond_0
    return-void
.end method

.method private createActionReceived(Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;Ljava/net/InetAddress;)Z
    .locals 16
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 132
    if-nez p1, :cond_0

    .line 133
    const/4 v13, 0x0

    .line 246
    :goto_0
    return v13

    .line 136
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->isAnyContainer()Z

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_8

    .line 137
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v9

    .line 139
    .local v9, "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->getElements()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v9, v13}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v8

    .line 140
    .local v8, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const/4 v7, 0x0

    .line 141
    .local v7, "itemNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v8, :cond_1

    .line 142
    const-string v13, "item"

    invoke-virtual {v8, v13}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v7

    .line 143
    :cond_1
    if-nez v7, :cond_2

    .line 144
    const/16 v13, 0x2c8

    const-string v14, "Bad Metadata"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->setStatus(ILjava/lang/String;)V

    .line 145
    const/4 v13, 0x0

    goto :goto_0

    .line 148
    :cond_2
    invoke-static {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->createImportItemNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    move-result-object v6

    .line 150
    .local v6, "importItemLeaf":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    if-nez v6, :cond_3

    .line 151
    const/16 v13, 0x2c8

    const-string v14, "Bad Metadata"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->setStatus(ILjava/lang/String;)V

    .line 152
    const/4 v13, 0x0

    goto :goto_0

    .line 155
    :cond_3
    invoke-virtual/range {p2 .. p2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v13

    invoke-static {v7, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->getDeviceName(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 158
    .local v3, "deviceName":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getTitle()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->hashCode()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 162
    .local v10, "randomID":Ljava/lang/String;
    new-instance v13, Ljava/lang/Object;

    invoke-direct {v13}, Ljava/lang/Object;-><init>()V

    invoke-static {v3, v10, v6, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->create(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/Object;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    move-result-object v12

    .line 165
    .local v12, "uploadRequest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    if-nez v12, :cond_4

    .line 166
    const/4 v13, 0x0

    goto :goto_0

    .line 168
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mOnUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    invoke-interface {v13, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;->onRequested(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 171
    const-wide/16 v14, 0x7530

    invoke-virtual {v12, v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->waitEvent(J)V

    .line 173
    sget-object v13, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$1;->$SwitchMap$com$samsung$android$allshare$stack$mediaserver$upnp$DefaultUploadRequest$UploadRequestSate:[I

    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->getState()Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    move-result-object v14

    invoke-virtual {v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->ordinal()I

    move-result v14

    aget v13, v13, v14

    packed-switch v13, :pswitch_data_0

    .line 191
    :pswitch_0
    invoke-static {v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getContentURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v6, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->setImportResourceUri(Ljava/lang/String;)V

    .line 194
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->update()V

    .line 204
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->hasEnoughStorage(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;)Z

    move-result v13

    if-nez v13, :cond_5

    .line 205
    const/16 v13, 0x1f5

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->setStatus(I)V

    .line 206
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mOnUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    invoke-interface {v13, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 207
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 181
    :pswitch_1
    const/16 v13, 0x192

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->setStatus(I)V

    .line 186
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mOnUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    invoke-interface {v13, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 187
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 211
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getImportDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v4

    .line 212
    .local v4, "directory":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-nez v4, :cond_6

    .line 213
    const/16 v13, 0x192

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->setStatus(I)V

    .line 214
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mOnUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    invoke-interface {v13, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 215
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 218
    :cond_6
    invoke-virtual {v4, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->addItemObject(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V

    .line 220
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v2

    .line 221
    .local v2, "addr":Ljava/net/InetAddress;
    const-string v11, ""

    .line 222
    .local v11, "result":Ljava/lang/String;
    if-eqz v2, :cond_7

    .line 223
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getHTTPPort()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v2}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getStreamingPort(Ljava/lang/String;)I

    move-result v14

    move-object/from16 v0, p2

    invoke-static {v6, v0, v13, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter;->print(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/net/InetAddress;II)Ljava/lang/String;

    move-result-object v11

    .line 227
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->setObjectID(Ljava/lang/String;)V

    .line 228
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->setResult(Ljava/lang/String;)V

    .line 229
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->updateSystemUpdateID()V

    .line 231
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "/media/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v6, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->setDownloadUri(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 234
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 235
    .end local v2    # "addr":Ljava/net/InetAddress;
    .end local v3    # "deviceName":Ljava/lang/String;
    .end local v4    # "directory":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .end local v6    # "importItemLeaf":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .end local v7    # "itemNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v8    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v10    # "randomID":Ljava/lang/String;
    .end local v11    # "result":Ljava/lang/String;
    .end local v12    # "uploadRequest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    :catch_0
    move-exception v5

    .line 236
    .local v5, "e":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;->printStackTrace()V

    .line 244
    .end local v5    # "e":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    .end local v9    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :goto_1
    const/16 v13, 0x192

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->setStatus(I)V

    .line 246
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 237
    .restart local v9    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :catch_1
    move-exception v5

    .line 238
    .local v5, "e":Ljava/lang/Exception;
    invoke-virtual {v5}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 241
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v9    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :cond_8
    const/16 v13, 0x2c9

    const-string v14, "Restricted Parent Object"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;->setStatus(ILjava/lang/String;)V

    goto :goto_1

    .line 173
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private static getDeviceName(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "itemNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .param p1, "defaultName"    # Ljava/lang/String;

    .prologue
    .line 252
    const-string v2, "sec:uploadDeviceName"

    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 253
    .local v1, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v1, :cond_1

    .line 260
    .end local p1    # "defaultName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .line 256
    .restart local p1    # "defaultName":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 257
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move-object p1, v0

    .line 260
    goto :goto_0
.end method

.method private hasEnoughStorage(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;)Z
    .locals 10
    .param p1, "importItem"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .prologue
    .line 117
    const-string v6, "res"

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v0

    .line 119
    .local v0, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 120
    .local v1, "resSize":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 121
    const-string v6, "size"

    invoke-virtual {v0, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 122
    .local v5, "strSize":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    .line 123
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 126
    .end local v5    # "strSize":Ljava/lang/String;
    :cond_0
    new-instance v4, Landroid/os/StatFs;

    iget-object v6, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getUploadPath()Ljava/io/File;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 127
    .local v4, "stat":Landroid/os/StatFs;
    invoke-virtual {v4}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v4}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v8

    int-to-long v8, v8

    mul-long v2, v6, v8

    .line 128
    .local v2, "space":J
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v6, v2, v6

    if-lez v6, :cond_1

    const/4 v6, 0x1

    :goto_0
    return v6

    :cond_1
    const/4 v6, 0x0

    goto :goto_0
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 2
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 109
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;

    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 112
    .local v0, "create":Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler;->createActionReceived(Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CreateObjectActionHandler$CreateObjectAction;Ljava/net/InetAddress;)Z

    move-result v1

    return v1
.end method
