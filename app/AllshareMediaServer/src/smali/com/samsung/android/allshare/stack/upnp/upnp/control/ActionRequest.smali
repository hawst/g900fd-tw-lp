.class public Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;
.source "ActionRequest.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;-><init>()V

    .line 42
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 0
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;-><init>()V

    .line 45
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 46
    return-void
.end method

.method private createContentNode(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 8
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .param p2, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p3, "argList"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    .prologue
    .line 120
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v0

    .line 121
    .local v0, "actionName":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v6

    .line 123
    .local v6, "serviceType":Ljava/lang/String;
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>()V

    .line 124
    .local v1, "actionNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const-string v7, "u"

    invoke-virtual {v1, v7, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setName(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    const-string v7, "u"

    invoke-virtual {v1, v7, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNameSpace(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-virtual {p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->size()I

    move-result v3

    .line 128
    .local v3, "argListCnt":I
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_0
    if-ge v5, v3, :cond_0

    .line 129
    invoke-virtual {p3, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->getArgument(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 130
    .local v2, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>()V

    .line 131
    .local v4, "argNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setName(Ljava/lang/String;)V

    .line 132
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 133
    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 128
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 136
    .end local v2    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .end local v4    # "argNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public getActionName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 63
    .local v2, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v2, :cond_0

    .line 64
    const-string v3, ""

    .line 71
    :goto_0
    return-object v3

    .line 65
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "name":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 67
    const-string v3, ""

    goto :goto_0

    .line 68
    :cond_1
    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 69
    .local v0, "idx":I
    if-gez v0, :cond_2

    .line 70
    const-string v3, ""

    goto :goto_0

    .line 71
    :cond_2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 54
    .local v0, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_1

    .line 58
    :cond_0
    :goto_0
    return-object v1

    .line 56
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->hasNodes()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 58
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method public getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    .locals 7

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 77
    .local v0, "actNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const/4 v5, 0x0

    .line 78
    .local v5, "nArgNodes":I
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v5

    .line 81
    :cond_0
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;-><init>()V

    .line 82
    .local v2, "argList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-ge v4, v5, :cond_1

    .line 83
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;-><init>()V

    .line 84
    .local v1, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    .line 85
    .local v3, "argNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setName(Ljava/lang/String;)V

    .line 86
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 87
    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->add(Ljava/lang/Object;)Z

    .line 82
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 89
    .end local v1    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .end local v3    # "argNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_1
    return-object v2
.end method

.method public post()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;
    .locals 3

    .prologue
    .line 144
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getRequestHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getRequestPort()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->postMessage(Ljava/lang/String;I)Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;

    move-result-object v0

    .line 145
    .local v0, "soapRes":Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;

    invoke-direct {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;-><init>(Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;)V

    return-object v1
.end method

.method public setRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;)V
    .locals 9
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "argList"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    .prologue
    .line 97
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 99
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {p0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->setRequestHostFromService(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V

    .line 101
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAP;->createEnvelopeBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->setEnvelopeNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 102
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    .line 103
    .local v3, "envNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 104
    .local v2, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {p0, v4, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->createContentNode(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 105
    .local v1, "argNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v2, :cond_0

    .line 106
    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 107
    :cond_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->setContent(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 109
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v5

    .line 110
    .local v5, "serviceType":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v0

    .line 111
    .local v0, "actionName":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "#"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 112
    .local v6, "soapAction":Ljava/lang/String;
    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->setSOAPAction(Ljava/lang/String;)V

    .line 113
    return-void
.end method
