.class public Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;
.source "ServiceData.java"


# instance fields
.field private mScpdNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private mSid:Ljava/lang/String;

.field private mSubscriberList:Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

.field private mTimeout:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mScpdNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 64
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mSubscriberList:Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mSid:Ljava/lang/String;

    .line 88
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mTimeout:J

    .line 34
    return-void
.end method


# virtual methods
.method public getSCPDNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mScpdNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mSid:Ljava/lang/String;

    return-object v0
.end method

.method public getSubscriberList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mSubscriberList:Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriberList;

    return-object v0
.end method

.method public getTimeout()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mTimeout:J

    return-wide v0
.end method

.method public setSCPDNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mScpdNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 58
    return-void
.end method

.method public setSID(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mSid:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setTimeout(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 95
    iput-wide p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ServiceData;->mTimeout:J

    .line 96
    return-void
.end method
