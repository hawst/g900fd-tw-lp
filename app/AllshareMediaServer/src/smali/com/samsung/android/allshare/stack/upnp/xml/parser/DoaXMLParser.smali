.class public Lcom/samsung/android/allshare/stack/upnp/xml/parser/DoaXMLParser;
.super Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
.source "DoaXMLParser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method public parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 17
    .param p1, "inStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v15, 0x0

    .line 60
    .local v15, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v13, 0x0

    .line 61
    .local v13, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const/4 v4, 0x0

    .line 62
    .local v4, "currNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const/4 v8, 0x0

    .line 64
    .local v8, "inReader":Ljava/io/InputStreamReader;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v7

    .line 65
    .local v7, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    invoke-virtual {v7}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v15

    .line 66
    new-instance v9, Ljava/io/InputStreamReader;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    .end local v8    # "inReader":Ljava/io/InputStreamReader;
    .local v9, "inReader":Ljava/io/InputStreamReader;
    :try_start_1
    invoke-interface {v15, v9}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 68
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v6

    .line 69
    .local v6, "eventType":I
    :goto_0
    const/16 v16, 0x1

    move/from16 v0, v16

    if-eq v6, v0, :cond_4

    .line 70
    packed-switch v6, :pswitch_data_0

    .line 105
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v6

    goto :goto_0

    .line 74
    :pswitch_1
    new-instance v11, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {v11}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>()V

    .line 75
    .local v11, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v12

    .line 76
    .local v12, "nodeName":Ljava/lang/String;
    invoke-virtual {v11, v12}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setName(Ljava/lang/String;)V

    .line 77
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v3

    .line 78
    .local v3, "attrsLen":I
    const/4 v10, 0x0

    .local v10, "n":I
    :goto_2
    if-ge v10, v3, :cond_1

    .line 79
    invoke-interface {v15, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "attrName":Ljava/lang/String;
    invoke-interface {v15, v10}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    .line 81
    .local v2, "attrValue":Ljava/lang/String;
    invoke-virtual {v11, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 84
    .end local v1    # "attrName":Ljava/lang/String;
    .end local v2    # "attrValue":Ljava/lang/String;
    :cond_1
    if-eqz v4, :cond_2

    .line 85
    invoke-virtual {v4, v11}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 86
    :cond_2
    move-object v4, v11

    .line 87
    if-nez v13, :cond_0

    .line 88
    move-object v13, v11

    goto :goto_1

    .line 92
    .end local v3    # "attrsLen":I
    .end local v10    # "n":I
    .end local v11    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v12    # "nodeName":Ljava/lang/String;
    :pswitch_2
    invoke-interface {v15}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v14

    .line 93
    .local v14, "value":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 94
    invoke-virtual {v4, v14}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 107
    .end local v6    # "eventType":I
    .end local v14    # "value":Ljava/lang/String;
    :catch_0
    move-exception v5

    move-object v8, v9

    .line 108
    .end local v7    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v9    # "inReader":Ljava/io/InputStreamReader;
    .local v5, "e":Ljava/lang/Exception;
    .restart local v8    # "inReader":Ljava/io/InputStreamReader;
    :goto_3
    :try_start_2
    new-instance v16, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;-><init>(Ljava/lang/Exception;)V

    throw v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 111
    .end local v5    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v16

    .line 112
    :goto_4
    if-eqz v8, :cond_3

    .line 113
    :try_start_3
    invoke-virtual {v8}, Ljava/io/InputStreamReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 116
    :cond_3
    :goto_5
    throw v16

    .line 98
    .end local v8    # "inReader":Ljava/io/InputStreamReader;
    .restart local v6    # "eventType":I
    .restart local v7    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v9    # "inReader":Ljava/io/InputStreamReader;
    :pswitch_3
    if-eqz v4, :cond_0

    .line 99
    :try_start_4
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getParentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result-object v4

    goto :goto_1

    .line 112
    :cond_4
    if-eqz v9, :cond_5

    .line 113
    :try_start_5
    invoke-virtual {v9}, Ljava/io/InputStreamReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 119
    :cond_5
    :goto_6
    return-object v13

    .line 114
    :catch_1
    move-exception v5

    .line 115
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 114
    .end local v5    # "e":Ljava/io/IOException;
    .end local v6    # "eventType":I
    .end local v7    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v9    # "inReader":Ljava/io/InputStreamReader;
    .restart local v8    # "inReader":Ljava/io/InputStreamReader;
    :catch_2
    move-exception v5

    .line 115
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 111
    .end local v5    # "e":Ljava/io/IOException;
    .end local v8    # "inReader":Ljava/io/InputStreamReader;
    .restart local v7    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v9    # "inReader":Ljava/io/InputStreamReader;
    :catchall_1
    move-exception v16

    move-object v8, v9

    .end local v9    # "inReader":Ljava/io/InputStreamReader;
    .restart local v8    # "inReader":Ljava/io/InputStreamReader;
    goto :goto_4

    .line 107
    .end local v7    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    :catch_3
    move-exception v5

    goto :goto_3

    .line 70
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public parse(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2
    .param p1, "info"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 125
    .local v0, "is":Ljava/io/ByteArrayInputStream;
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/parser/DoaXMLParser;->parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    return-object v1
.end method
