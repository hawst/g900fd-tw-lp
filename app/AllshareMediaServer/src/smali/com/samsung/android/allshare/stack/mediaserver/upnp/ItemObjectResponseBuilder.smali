.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;
.super Ljava/lang/Object;
.source "ItemObjectResponseBuilder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$TransferModeHeaderHandler;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$TimeSeekHeaderHandler;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$GetContentsFeatureHeaderHandler;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$CaptionRequestHeaderHandler;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;
    }
.end annotation


# static fields
.field private static mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;


# instance fields
.field private mHttpHeaderHandler:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V
    .locals 4
    .param p1, "server"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .prologue
    const/4 v3, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mHttpHeaderHandler:Ljava/util/HashMap;

    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mHttpHeaderHandler:Ljava/util/HashMap;

    const-string v1, "getCaptionInfo.sec"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$CaptionRequestHeaderHandler;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$CaptionRequestHeaderHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mHttpHeaderHandler:Ljava/util/HashMap;

    const-string v1, "getcontentFeatures.dlna.org"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$GetContentsFeatureHeaderHandler;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$GetContentsFeatureHeaderHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mHttpHeaderHandler:Ljava/util/HashMap;

    const-string v1, "TimeSeekRange.dlna.org"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$TimeSeekHeaderHandler;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$TimeSeekHeaderHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mHttpHeaderHandler:Ljava/util/HashMap;

    const-string v1, "transferMode.dlna.org"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$TransferModeHeaderHandler;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$TransferModeHeaderHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sput-object p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 50
    return-void
.end method

.method static synthetic access$400()Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    return-object v0
.end method


# virtual methods
.method public buildResponse(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    .locals 10
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    .param p2, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p3, "res"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    .prologue
    const/16 v8, 0xc8

    .line 53
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->buildHTTPResponse(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v5

    .line 54
    .local v5, "resp":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    if-eqz v5, :cond_0

    .line 55
    invoke-virtual {v5, v8}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 58
    :cond_0
    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    if-nez v1, :cond_2

    .line 90
    :cond_1
    :goto_0
    return-object v5

    .line 61
    :cond_2
    const/4 v0, 0x0

    .line 62
    .local v0, "handler":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getNHeaders()I

    move-result v7

    .line 63
    .local v7, "nHeaders":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, v7, :cond_5

    .line 64
    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeader(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v2

    .line 65
    .local v2, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-nez v2, :cond_4

    .line 66
    const-string v1, "ItemObjectResponseBuilder"

    const-string v3, "buildResponse"

    const-string v4, "HTTPHeader is null!"

    invoke-static {v1, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 70
    :cond_4
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mHttpHeaderHandler:Ljava/util/HashMap;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "handler":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;
    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;

    .line 71
    .restart local v0    # "handler":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;
    if-eqz v0, :cond_3

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    .line 74
    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;->handle(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 79
    .end local v2    # "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    :cond_5
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getStatusCode()I

    move-result v1

    if-ne v1, v8, :cond_6

    .line 80
    invoke-virtual {p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getContentLength()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentLength(J)V

    .line 82
    invoke-virtual {p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentInputStream(Ljava/io/InputStream;)V

    goto :goto_0

    .line 85
    :cond_6
    if-eqz v0, :cond_1

    .line 86
    const-string v1, "ItemObjectResponseBuilder"

    const-string v3, "buildResponse"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Invalid Http Request in : "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
