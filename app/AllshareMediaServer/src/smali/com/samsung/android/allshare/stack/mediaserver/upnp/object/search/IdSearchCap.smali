.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/IdSearchCap;
.super Ljava/lang/Object;
.source "IdSearchCap.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;


# static fields
.field private static final ID:Ljava/lang/String; = "@id"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method


# virtual methods
.method public compare(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)Z
    .locals 5
    .param p1, "searchCri"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;
    .param p2, "conNode"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 30
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 31
    .local v1, "searchCriID":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getID()Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "conID":Ljava/lang/String;
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v3

    .line 34
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isExists()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 36
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isTrueValue()Z

    move-result v4

    if-eqz v4, :cond_2

    move v3, v2

    .line 37
    goto :goto_0

    .line 38
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isFalseValue()Z

    move-result v4

    if-nez v4, :cond_0

    .line 41
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;->isEQ()Z

    move-result v4

    if-ne v4, v2, :cond_0

    .line 42
    invoke-virtual {v1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_4

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_1
.end method

.method public getPropertyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    const-string v0, "@id"

    return-object v0
.end method
