.class public Lcom/samsung/android/allshare/stack/smartcamera/upnp/CameraUPnPDefines;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/UPnPDefines;
.source "CameraUPnPDefines.java"


# static fields
.field public static final AF:Ljava/lang/String; = "AF"

.field public static final AFRELEASE:Ljava/lang/String; = "AFRELEASE"

.field public static final GET_INFOMATION:Ljava/lang/String; = "GetInfomation"

.field public static final GET_IP:Ljava/lang/String; = "GetIP"

.field public static final GET_ZOOM:Ljava/lang/String; = "GetZoom"

.field public static final MULTIAF:Ljava/lang/String; = "MULTIAF"

.field public static final RELEASE_SELF_TIMER:Ljava/lang/String; = "ReleaseSelfTimer"

.field public static final SET_FLASH:Ljava/lang/String; = "SetFlash"

.field public static final SET_LED:Ljava/lang/String; = "SetLED"

.field public static final SET_RESOLUTION:Ljava/lang/String; = "SetResolution"

.field public static final SET_ZOOM:Ljava/lang/String; = "SetZoom"

.field public static final SHUTTER_UP:Ljava/lang/String; = "ShutterUp"

.field public static final Shot:Ljava/lang/String; = "Shot"

.field public static final ShotWithGPS:Ljava/lang/String; = "ShotWithGPS"

.field public static final ZOOMIN:Ljava/lang/String; = "ZoomIN"

.field public static final ZOOMOUT:Ljava/lang/String; = "ZoomOUT"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UPnPDefines;-><init>()V

    .line 25
    return-void
.end method
