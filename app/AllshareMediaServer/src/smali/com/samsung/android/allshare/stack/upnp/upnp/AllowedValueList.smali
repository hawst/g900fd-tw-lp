.class public Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "AllowedValueList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;",
        ">;"
    }
.end annotation


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "allowedValueList"

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public getAllowedValue(I)Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;

    return-object v0
.end method
