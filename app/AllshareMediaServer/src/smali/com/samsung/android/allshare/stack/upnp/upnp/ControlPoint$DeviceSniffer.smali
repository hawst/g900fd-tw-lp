.class Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeviceSniffer"
.end annotation


# instance fields
.field private mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

.field private mRoot:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field final synthetic this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    .locals 1
    .param p2, "root"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .param p3, "device"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    const/4 v0, 0x0

    .line 1122
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1118
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mRoot:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 1120
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 1123
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mRoot:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 1124
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 1125
    return-void
.end method

.method private checkRsponse4MSearch()Z
    .locals 5

    .prologue
    .line 1167
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopyLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2300(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1168
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopy:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2400(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1170
    const-string v0, "ControlPoint"

    const-string v2, "checkRsponse4MSearch"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "checkRsponse4MSearch found the reponse, uuid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ip = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1174
    const/4 v0, 0x1

    monitor-exit v1

    .line 1178
    :goto_0
    return v0

    .line 1176
    :cond_0
    monitor-exit v1

    .line 1178
    const/4 v0, 0x0

    goto :goto_0

    .line 1176
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 1128
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mRoot:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    if-nez v0, :cond_1

    .line 1159
    :cond_0
    :goto_0
    return-void

    .line 1131
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->checkDeviceAvailable(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)Z
    invoke-static {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1800(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1132
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->checkRsponse4MSearch()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1133
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1134
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1135
    const-string v0, "ControlPoint"

    const-string v2, "run"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DeviceSniffer.RemoveDevice: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", curDev.id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1139
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->mRoot:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    invoke-static {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2000(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 1140
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1144
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCountLock:[B
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2100(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)[B

    move-result-object v1

    monitor-enter v1

    .line 1145
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2200(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2200(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    # setter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;
    invoke-static {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2202(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/Integer;)Ljava/lang/Integer;

    .line 1146
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1147
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2200(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_0

    .line 1149
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopyLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2300(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1150
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopy:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2400(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1151
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1153
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$2500(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1154
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeActivityList()V
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1000(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)V

    .line 1157
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->search()V

    goto/16 :goto_0

    .line 1140
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1146
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1151
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method
