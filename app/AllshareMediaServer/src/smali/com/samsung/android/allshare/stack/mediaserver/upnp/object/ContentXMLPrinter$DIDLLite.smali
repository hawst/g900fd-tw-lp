.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;
.super Ljava/lang/Object;
.source "ContentXMLPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DIDLLite"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;
    }
.end annotation


# static fields
.field private static final NAME:Ljava/lang/String; = "DIDL-Lite"

.field private static final XMLNS:Ljava/lang/String; = "xmlns"

.field private static final XMLNS_DC:Ljava/lang/String; = "xmlns:dc"

.field private static final XMLNS_DC_URL:Ljava/lang/String; = "http://purl.org/dc/elements/1.1/"

.field private static final XMLNS_DLNA:Ljava/lang/String; = "xmlns:dlna"

.field private static final XMLNS_DLNA_URL:Ljava/lang/String; = "urn:schemas-dlna-org:metadata-1-0/"

.field private static final XMLNS_SEC:Ljava/lang/String; = "xmlns:sec"

.field private static final XMLNS_SEC_URL:Ljava/lang/String; = "http://www.sec.co.kr/"

.field private static final XMLNS_UPNP:Ljava/lang/String; = "xmlns:upnp"

.field private static final XMLNS_UPNP_URL:Ljava/lang/String; = "urn:schemas-upnp-org:metadata-1-0/upnp/"

.field private static final XMLNS_URL:Ljava/lang/String; = "urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/"


# instance fields
.field private mContentList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;"
        }
    .end annotation
.end field

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->mTag:Ljava/lang/String;

    .line 161
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->mContentList:Ljava/util/ArrayList;

    .line 133
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->mContentList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getIndentLevelString(I)Ljava/lang/String;
    .locals 3
    .param p1, "nIndentLevel"    # I

    .prologue
    .line 241
    new-array v0, p1, [C

    .line 242
    .local v0, "indentString":[C
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 243
    const/16 v2, 0x9

    aput-char v2, v0, v1

    .line 242
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 244
    :cond_0
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([C)V

    return-object v2
.end method

.method private output(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "ps"    # Ljava/io/PrintWriter;

    .prologue
    .line 169
    const-string v4, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 171
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;-><init>()V

    .line 173
    .local v0, "didlNode":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->getName()Ljava/lang/String;

    move-result-object v2

    .line 175
    .local v2, "name":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 176
    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->outputAttributes(Ljava/io/PrintWriter;)V
    invoke-static {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;Ljava/io/PrintWriter;)V

    .line 177
    const/16 v4, 0x3e

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(C)V

    .line 179
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->mContentList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .line 180
    .local v3, "object":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    const/4 v4, 0x1

    invoke-direct {p0, p1, v3, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->output(Ljava/io/PrintWriter;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;I)V

    goto :goto_0

    .line 183
    .end local v3    # "object":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "</"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 184
    return-void
.end method

.method private output(Ljava/io/PrintWriter;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;I)V
    .locals 12
    .param p1, "ps"    # Ljava/io/PrintWriter;
    .param p2, "obj"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .param p3, "indentLevel"    # I

    .prologue
    .line 194
    invoke-direct {p0, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->getIndentLevelString(I)Ljava/lang/String;

    move-result-object v3

    .line 195
    .local v3, "indentString":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getName()Ljava/lang/String;

    move-result-object v4

    .line 196
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getValue()Ljava/lang/String;

    move-result-object v9

    .line 197
    .local v9, "value":Ljava/lang/String;
    invoke-static {v9}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199
    .local v1, "escapeValue":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->hasProperties()Z

    move-result v10

    if-nez v10, :cond_0

    .line 200
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "<"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 201
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->outputPropertyAttributes(Ljava/io/PrintWriter;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)V

    .line 202
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 229
    :goto_0
    return-void

    .line 206
    :cond_0
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "<"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 207
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->outputPropertyAttributes(Ljava/io/PrintWriter;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)V

    .line 208
    const/16 v10, 0x3e

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->println(C)V

    .line 210
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getPropertyList()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 211
    .local v5, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    add-int/lit8 v10, p3, 0x1

    invoke-direct {p0, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->getIndentLevelString(I)Ljava/lang/String;

    move-result-object v6

    .line 212
    .local v6, "propIndentString":Ljava/lang/String;
    instance-of v10, v5, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    if-nez v10, :cond_1

    .line 215
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v7

    .line 216
    .local v7, "propName":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getValue()Ljava/lang/String;

    move-result-object v8

    .line 217
    .local v8, "propValue":Ljava/lang/String;
    invoke-static {v8}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "escapePropValue":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->hasAttributes()Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_2

    .line 220
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "<"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 221
    invoke-direct {p0, p1, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->outputPropertyAttributes(Ljava/io/PrintWriter;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)V

    .line 222
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 224
    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "<"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 228
    .end local v0    # "escapePropValue":Ljava/lang/String;
    .end local v5    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v6    # "propIndentString":Ljava/lang/String;
    .end local v7    # "propName":Ljava/lang/String;
    .end local v8    # "propValue":Ljava/lang/String;
    :cond_3
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "</"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ">"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private outputPropertyAttributes(Ljava/io/PrintWriter;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)V
    .locals 6
    .param p1, "ps"    # Ljava/io/PrintWriter;
    .param p2, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .prologue
    .line 232
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributes()Ljava/util/Set;

    move-result-object v1

    .line 234
    .local v1, "attrs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 235
    .local v0, "attr":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 236
    .local v2, "escapeValue":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 238
    .end local v0    # "attr":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "escapeValue":Ljava/lang/String;
    :cond_0
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 138
    :try_start_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 139
    .local v0, "byteOut":Ljava/io/ByteArrayOutputStream;
    new-instance v3, Ljava/io/OutputStreamWriter;

    const-string v4, "UTF-8"

    invoke-direct {v3, v0, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 140
    .local v3, "writer":Ljava/io/Writer;
    new-instance v2, Ljava/io/PrintWriter;

    invoke-direct {v2, v3}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 141
    .local v2, "pr":Ljava/io/PrintWriter;
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->output(Ljava/io/PrintWriter;)V

    .line 142
    invoke-virtual {v2}, Ljava/io/PrintWriter;->flush()V

    .line 143
    invoke-virtual {v2}, Ljava/io/PrintWriter;->close()V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 145
    :try_start_1
    invoke-virtual {v3}, Ljava/io/Writer;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 150
    :try_start_2
    const-string v4, "UTF-8"

    invoke-virtual {v0, v4}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 154
    .end local v0    # "byteOut":Ljava/io/ByteArrayOutputStream;
    .end local v2    # "pr":Ljava/io/PrintWriter;
    .end local v3    # "writer":Ljava/io/Writer;
    :goto_0
    return-object v4

    .line 146
    .restart local v0    # "byteOut":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "pr":Ljava/io/PrintWriter;
    .restart local v3    # "writer":Ljava/io/Writer;
    :catch_0
    move-exception v1

    .line 147
    .local v1, "e":Ljava/io/IOException;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->mTag:Ljava/lang/String;

    const-string v5, "toString"

    const-string v6, "IOException"

    invoke-static {v4, v5, v6, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 148
    const-string v4, ""
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 151
    .end local v0    # "byteOut":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "pr":Ljava/io/PrintWriter;
    .end local v3    # "writer":Ljava/io/Writer;
    :catch_1
    move-exception v1

    .line 152
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->mTag:Ljava/lang/String;

    const-string v5, "toString"

    const-string v6, "UnsupportedEncodingException"

    invoke-static {v4, v5, v6, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 154
    const-string v4, ""

    goto :goto_0
.end method
