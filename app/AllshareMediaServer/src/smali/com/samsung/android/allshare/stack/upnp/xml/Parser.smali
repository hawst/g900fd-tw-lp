.class public abstract Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
.super Ljava/lang/Object;
.source "Parser.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Parser"

.field static retryCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method

.method private retryParse(Ljava/net/URL;ZLjava/net/ConnectException;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 4
    .param p1, "locationURL"    # Ljava/net/URL;
    .param p2, "isDefault"    # Z
    .param p3, "e"    # Ljava/net/ConnectException;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;,
            Ljava/net/SocketTimeoutException;,
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 236
    sget v0, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 238
    sget v0, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    .line 239
    const-string v0, "Parser"

    const-string v1, "retryParse"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "retryCount: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/net/URL;Z)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 248
    :goto_0
    return-object v0

    .line 244
    :cond_0
    const-string v0, "Parser"

    const-string v1, "retryParse"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "retryFail, retryCount:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    .line 248
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public parse(Landroid/net/Uri;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 8
    .param p1, "scpdURL"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 205
    const/4 v0, 0x0

    .line 207
    .local v0, "client":Landroid/net/http/AndroidHttpClient;
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 208
    .local v3, "locationURL":Ljava/net/URL;
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getUserAgentValue()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    .line 209
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {v3}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 215
    .local v2, "get":Lorg/apache/http/client/methods/HttpGet;
    const-string v6, "USER-AGENT"

    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getUserAgentValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-virtual {v0, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v4

    .line 217
    .local v4, "res":Lorg/apache/http/HttpResponse;
    const/4 v5, 0x0

    .line 218
    .local v5, "rootElem":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v4, :cond_0

    .line 219
    invoke-interface {v4}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    .line 221
    :cond_0
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    if-eqz v0, :cond_1

    .line 228
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_1
    return-object v5

    .line 224
    .end local v2    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v3    # "locationURL":Ljava/net/URL;
    .end local v4    # "res":Lorg/apache/http/HttpResponse;
    .end local v5    # "rootElem":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catch_0
    move-exception v1

    .line 225
    .local v1, "e":Ljava/lang/Exception;
    :try_start_1
    new-instance v6, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;

    invoke-direct {v6, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;-><init>(Ljava/lang/Exception;)V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    if-eqz v0, :cond_2

    .line 228
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    :cond_2
    throw v6
.end method

.method public parse(Ljava/io/File;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 8
    .param p1, "descriptionFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
        }
    .end annotation

    .prologue
    .line 186
    const/4 v1, 0x0

    .line 188
    .local v1, "fileIn":Ljava/io/InputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    .end local v1    # "fileIn":Ljava/io/InputStream;
    .local v2, "fileIn":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 196
    .local v3, "root":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v2, :cond_0

    .line 197
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 200
    :cond_0
    :goto_0
    return-object v3

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "Parser"

    const-string v5, "parse"

    const-string v6, "parse IOException"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 192
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "fileIn":Ljava/io/InputStream;
    .end local v3    # "root":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .restart local v1    # "fileIn":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    .line 193
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;

    invoke-direct {v4, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;-><init>(Ljava/lang/Exception;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 195
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v4

    .line 196
    :goto_2
    if-eqz v1, :cond_1

    .line 197
    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 200
    :cond_1
    :goto_3
    throw v4

    .line 198
    :catch_2
    move-exception v0

    .line 199
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "Parser"

    const-string v6, "parse"

    const-string v7, "parse IOException"

    invoke-static {v5, v6, v7, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_3

    .line 195
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "fileIn":Ljava/io/InputStream;
    .restart local v2    # "fileIn":Ljava/io/InputStream;
    :catchall_1
    move-exception v4

    move-object v1, v2

    .end local v2    # "fileIn":Ljava/io/InputStream;
    .restart local v1    # "fileIn":Ljava/io/InputStream;
    goto :goto_2

    .line 192
    .end local v1    # "fileIn":Ljava/io/InputStream;
    .restart local v2    # "fileIn":Ljava/io/InputStream;
    :catch_3
    move-exception v0

    move-object v1, v2

    .end local v2    # "fileIn":Ljava/io/InputStream;
    .restart local v1    # "fileIn":Ljava/io/InputStream;
    goto :goto_1
.end method

.method public abstract parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
        }
    .end annotation
.end method

.method public abstract parse(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
        }
    .end annotation
.end method

.method public parse(Ljava/net/URL;Z)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 11
    .param p1, "locationURL"    # Ljava/net/URL;
    .param p2, "isDefault"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;,
            Ljava/net/SocketTimeoutException;,
            Ljava/net/SocketException;
        }
    .end annotation

    .prologue
    .line 77
    if-nez p1, :cond_1

    .line 78
    const/4 v4, 0x0

    .line 176
    :cond_0
    :goto_0
    return-object v4

    .line 80
    :cond_1
    if-eqz p2, :cond_3

    .line 81
    const/4 v0, 0x0

    .line 83
    .local v0, "client":Landroid/net/http/AndroidHttpClient;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getUserAgentValue()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/http/AndroidHttpClient;->newInstance(Ljava/lang/String;)Landroid/net/http/AndroidHttpClient;

    move-result-object v0

    .line 84
    new-instance v2, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 90
    .local v2, "get":Lorg/apache/http/client/methods/HttpGet;
    const-string v7, "User-Agent"

    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getUserAgentValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v7, "TIMEOUT"

    const-string v8, "3000"

    invoke-virtual {v2, v7, v8}, Lorg/apache/http/client/methods/HttpGet;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-virtual {v0, v2}, Landroid/net/http/AndroidHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    .line 93
    .local v3, "res":Lorg/apache/http/HttpResponse;
    const/4 v4, 0x0

    .line 94
    .local v4, "rootElem":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v3, :cond_2

    .line 95
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    invoke-interface {v7}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v4

    .line 97
    :cond_2
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    .line 100
    const/4 v7, 0x0

    sput v7, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/ConnectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    goto :goto_0

    .line 103
    .end local v2    # "get":Lorg/apache/http/client/methods/HttpGet;
    .end local v3    # "res":Lorg/apache/http/HttpResponse;
    .end local v4    # "rootElem":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catch_0
    move-exception v1

    .line 104
    .local v1, "e":Ljava/net/SocketTimeoutException;
    :try_start_1
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 117
    .end local v1    # "e":Ljava/net/SocketTimeoutException;
    :catchall_0
    move-exception v7

    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    throw v7

    .line 105
    :catch_1
    move-exception v1

    .line 106
    .local v1, "e":Ljava/net/ConnectException;
    :try_start_2
    const-string v7, "Parser"

    const-string v8, "parse"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parse ConnectException Fail to connect:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryParse(Ljava/net/URL;ZLjava/net/ConnectException;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 117
    invoke-virtual {v0}, Landroid/net/http/AndroidHttpClient;->close()V

    goto :goto_0

    .line 112
    .end local v1    # "e":Ljava/net/ConnectException;
    :catch_2
    move-exception v1

    .line 113
    .local v1, "e":Ljava/net/SocketException;
    :try_start_3
    throw v1

    .line 114
    .end local v1    # "e":Ljava/net/SocketException;
    :catch_3
    move-exception v1

    .line 115
    .local v1, "e":Ljava/lang/Exception;
    new-instance v7, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;

    invoke-direct {v7, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;-><init>(Ljava/lang/Exception;)V

    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 120
    .end local v0    # "client":Landroid/net/http/AndroidHttpClient;
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    const/4 v6, 0x0

    .line 124
    .local v6, "urlIn":Ljava/io/InputStream;
    :try_start_4
    sget-object v7, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual {p1, v7}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;

    .line 126
    .local v5, "urlCon":Ljava/net/HttpURLConnection;
    const/16 v7, 0xbb8

    invoke-virtual {v5, v7}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 127
    const/16 v7, 0x61a8

    invoke-virtual {v5, v7}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 128
    const-string v7, "GET"

    invoke-virtual {v5, v7}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 134
    const-string v7, "USER-AGENT"

    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getUserAgentValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    .line 136
    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v4

    .line 137
    .restart local v4    # "rootElem":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 140
    const/4 v7, 0x0

    sput v7, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/net/ConnectException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_9
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 171
    if-eqz v6, :cond_0

    .line 173
    :try_start_5
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 174
    :catch_4
    move-exception v1

    .line 175
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "Parser"

    const-string v8, "parse"

    const-string v9, "IOException - "

    invoke-static {v7, v8, v9, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 143
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "rootElem":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v5    # "urlCon":Ljava/net/HttpURLConnection;
    :catch_5
    move-exception v1

    .line 144
    .local v1, "e":Ljava/net/SocketTimeoutException;
    :try_start_6
    const-string v7, "Parser"

    const-string v8, "parse"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parse SocketTimeoutException(HttpURLConnection.getInputStream):"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    throw v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 171
    .end local v1    # "e":Ljava/net/SocketTimeoutException;
    :catchall_1
    move-exception v7

    if-eqz v6, :cond_4

    .line 173
    :try_start_7
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_a

    .line 176
    :cond_4
    :goto_1
    throw v7

    .line 147
    :catch_6
    move-exception v1

    .line 148
    .local v1, "e":Ljava/net/ConnectException;
    :try_start_8
    const-string v7, "Parser"

    const-string v8, "parse"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "parse ConnectException Fail to connect:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    throw v1

    .line 151
    .end local v1    # "e":Ljava/net/ConnectException;
    :catch_7
    move-exception v1

    .line 152
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "Parser"

    const-string v8, "parse"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SocketException - "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    sget v7, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    const/4 v8, 0x3

    if-ge v7, v8, :cond_5

    .line 156
    sget v7, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    add-int/lit8 v7, v7, 0x1

    sput v7, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    .line 157
    const-string v7, "Parser"

    const-string v8, "parse"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "retryCount: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/net/URL;Z)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result-object v4

    .line 171
    if-eqz v6, :cond_0

    .line 173
    :try_start_9
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    goto/16 :goto_0

    .line 174
    :catch_8
    move-exception v1

    .line 175
    const-string v7, "Parser"

    const-string v8, "parse"

    const-string v9, "IOException - "

    invoke-static {v7, v8, v9, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 162
    :cond_5
    :try_start_a
    const-string v7, "Parser"

    const-string v8, "parse"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "retryFail, retryCount:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget v10, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    const/4 v7, 0x0

    sput v7, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->retryCount:I

    .line 166
    new-instance v7, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;

    invoke-direct {v7, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;-><init>(Ljava/lang/Exception;)V

    throw v7

    .line 168
    .end local v1    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v1

    .line 169
    .local v1, "e":Ljava/lang/Exception;
    new-instance v7, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;

    invoke-direct {v7, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;-><init>(Ljava/lang/Exception;)V

    throw v7
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 174
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_a
    move-exception v1

    .line 175
    .local v1, "e":Ljava/io/IOException;
    const-string v8, "Parser"

    const-string v9, "parse"

    const-string v10, "IOException - "

    invoke-static {v8, v9, v10, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_1
.end method
