.class public Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;
.super Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;
.source "ControlResponse.java"


# static fields
.field public static final FAULT_CODE:Ljava/lang/String; = "Client"

.field public static final FAULT_STRING:Ljava/lang/String; = "UPnPError"

.field private static final TAG:Ljava/lang/String; = "ControlResponse"


# instance fields
.field private mUpnpErr:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;-><init>()V

    .line 121
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->mUpnpErr:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .line 45
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getServerName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->setServer(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;)V
    .locals 1
    .param p1, "soapRes"    # Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;-><init>(Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;)V

    .line 121
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->mUpnpErr:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .line 50
    return-void
.end method

.method private createFaultResponseNode(ILjava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 9
    .param p1, "errCode"    # I
    .param p2, "errDescr"    # Ljava/lang/String;

    .prologue
    .line 78
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v7, "s:Fault"

    invoke-direct {v4, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 81
    .local v4, "faultNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v7, "faultcode"

    invoke-direct {v3, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 82
    .local v3, "faultCodeNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const-string v7, "s:Client"

    invoke-virtual {v3, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 86
    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v7, "faultstring"

    invoke-direct {v5, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 87
    .local v5, "faultStringNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const-string v7, "UPnPError"

    invoke-virtual {v5, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 91
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v7, "detail"

    invoke-direct {v0, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 92
    .local v0, "detailNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 95
    new-instance v6, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v7, "UPnPError"

    invoke-direct {v6, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 96
    .local v6, "upnpErrorNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const-string v7, "xmlns"

    const-string v8, "urn:schemas-upnp-org:control-1-0"

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    invoke-virtual {v0, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 100
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v7, "errorCode"

    invoke-direct {v1, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 101
    .local v1, "errorCodeNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(I)V

    .line 102
    invoke-virtual {v6, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 105
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v7, "errorDescription"

    invoke-direct {v2, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 106
    .local v2, "errorDesctiprionNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v2, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 107
    invoke-virtual {v6, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 109
    return-object v4
.end method

.method private getUPnPErrorCodeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getUPnPErrorNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 132
    .local v0, "errorNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 133
    const/4 v1, 0x0

    .line 134
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "errorCode"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method private getUPnPErrorDescriptionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2

    .prologue
    .line 138
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getUPnPErrorNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 139
    .local v0, "errorNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 140
    const/4 v1, 0x0

    .line 141
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "errorDescription"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method private getUPnPErrorNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getFaultDetailNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 125
    .local v0, "detailNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 126
    const/4 v1, 0x0

    .line 127
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "UPnPError"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public getUPnPError()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;
    .locals 3

    .prologue
    .line 165
    const/4 v0, 0x0

    .line 166
    .local v0, "code":I
    const-string v1, ""

    .line 167
    .local v1, "desc":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getUPnPErrorCode()I

    move-result v0

    .line 168
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getUPnPErrorDescription()Ljava/lang/String;

    move-result-object v1

    .line 169
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->mUpnpErr:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->setCode(I)V

    .line 170
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->mUpnpErr:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->setDescription(Ljava/lang/String;)V

    .line 171
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->mUpnpErr:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    return-object v2
.end method

.method public getUPnPErrorCode()I
    .locals 7

    .prologue
    const/4 v3, -0x1

    .line 145
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getUPnPErrorCodeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 146
    .local v1, "errorCodeNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v1, :cond_0

    .line 153
    :goto_0
    return v3

    .line 148
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v2

    .line 150
    .local v2, "errorCodeStr":Ljava/lang/String;
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "ControlResponse"

    const-string v5, "getUPnPErrorCode"

    const-string v6, "getUPnPErrorCode Exception"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public getUPnPErrorDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getUPnPErrorDescriptionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 159
    .local v0, "errorDescNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 160
    const-string v1, ""

    .line 161
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public setFaultResponse(I)V
    .locals 1
    .param p1, "errCode"    # I

    .prologue
    .line 69
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->setFaultResponse(ILjava/lang/String;)V

    .line 70
    return-void
.end method

.method public setFaultResponse(ILjava/lang/String;)V
    .locals 4
    .param p1, "errCode"    # I
    .param p2, "errDescr"    # Ljava/lang/String;

    .prologue
    .line 57
    const/16 v3, 0x1f4

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->setStatusCode(I)V

    .line 59
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 60
    .local v0, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->createFaultResponseNode(ILjava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 61
    .local v2, "faultNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 65
    .local v1, "envNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->setContent(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 66
    return-void
.end method
