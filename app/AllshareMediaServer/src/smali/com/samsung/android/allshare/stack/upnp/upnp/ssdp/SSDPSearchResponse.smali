.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPResponse;
.source "SSDPSearchResponse.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPResponse;-><init>()V

    .line 23
    const/16 v0, 0xc8

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->setStatusCode(I)V

    .line 24
    const/16 v0, 0x708

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->setCacheControl(I)V

    .line 25
    const-string v0, "Server"

    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getServerName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    const-string v0, "EXT"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    return-void
.end method
