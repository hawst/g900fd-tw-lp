.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;
.super Ljava/util/ArrayList;
.source "ContentSearcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SearchCapList"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x2fded7ef10f2173eL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method public getSearchCap(I)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;

    return-object v0
.end method

.method public getSearchCap(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;
    .locals 5
    .param p1, "propName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 54
    if-nez p1, :cond_1

    move-object v2, v3

    .line 63
    :cond_0
    :goto_0
    return-object v2

    .line 57
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;->size()I

    move-result v1

    .line 58
    .local v1, "nLists":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 59
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchCapList;->getSearchCap(I)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;

    move-result-object v2

    .line 60
    .local v2, "scap":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;
    invoke-interface {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;->getPropertyName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v2    # "scap":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/ISearchCap;
    :cond_2
    move-object v2, v3

    .line 63
    goto :goto_0
.end method
