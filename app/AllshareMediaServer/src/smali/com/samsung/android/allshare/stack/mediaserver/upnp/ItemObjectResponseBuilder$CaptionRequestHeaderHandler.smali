.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$CaptionRequestHeaderHandler;
.super Ljava/lang/Object;
.source "ItemObjectResponseBuilder.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CaptionRequestHeaderHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$CaptionRequestHeaderHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handle(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z
    .locals 8
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    .param p2, "header"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    .param p3, "object"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p4, "res"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .param p5, "resp"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    .prologue
    .line 97
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getRequestAddress()Ljava/net/InetAddress;

    move-result-object v4

    .line 98
    .local v4, "reqAddress":Ljava/net/InetAddress;
    if-nez v4, :cond_0

    .line 99
    const/4 v6, 0x0

    .line 120
    :goto_0
    return v6

    .line 102
    :cond_0
    invoke-virtual {p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getResoureList()Ljava/util/ArrayList;

    move-result-object v3

    .line 103
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 104
    .local v1, "cp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    instance-of v6, v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;

    if-eqz v6, :cond_1

    .line 106
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 107
    .local v5, "wildcardAddress":Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v0

    .line 109
    .local v0, "address":Ljava/net/InetAddress;
    if-eqz v0, :cond_1

    .line 112
    const-string v6, "$X@Z@Z@X"

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 114
    const-string v6, "$Y@Z@Z@Y"

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->access$400()Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getHTTPPort()I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 117
    const-string v6, "CaptionInfo.sec"

    invoke-virtual {p5, v6, v5}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 120
    .end local v0    # "address":Ljava/net/InetAddress;
    .end local v1    # "cp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v5    # "wildcardAddress":Ljava/lang/String;
    :cond_2
    const/4 v6, 0x1

    goto :goto_0
.end method
