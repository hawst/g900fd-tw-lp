.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/UPnPDefines;
.super Ljava/lang/Object;
.source "UPnPDefines.java"


# static fields
.field public static final ALBUM:Ljava/lang/String; = "upnp:album"

.field public static final ALBUMART:Ljava/lang/String; = "upnp:albumArtURI"

.field public static final ARTIST:Ljava/lang/String; = "upnp:artist"

.field public static final BAD_GET_CONTENTS_FEATURES:Ljava/lang/String; = "getcontentFeatures.dlna.org: 2"

.field public static final BROWSE:Ljava/lang/String; = "Browse"

.field public static final CAPTION_INFO:Ljava/lang/String; = "sec:CaptionInfo"

.field public static final CAPTION_INFO_EX:Ljava/lang/String; = "sec:CaptionInfoEx"

.field public static final CAPTION_REQUEST:Ljava/lang/String; = "getCaptionInfo.sec"

.field public static final CAPTION_RESPONSE:Ljava/lang/String; = "CaptionInfo.sec"

.field public static final CLASS:Ljava/lang/String; = "upnp:class"

.field public static final CONNECTION_COMPLETE:Ljava/lang/String; = "ConnectionComplete"

.field public static final CONTAINER:Ljava/lang/String; = "container"

.field public static final CONTENTS_FEATURES:Ljava/lang/String; = "contentFeatures.dlna.org"

.field public static final CONTENT_FEATURE_VERSION:Ljava/lang/String; = "1"

.field public static final CONTENT_ID:Ljava/lang/String; = "id"

.field public static final CREATEOBJECT:Ljava/lang/String; = "CreateObject"

.field public static final CREATOR:Ljava/lang/String; = "dc:creator"

.field public static final CURRENT_CONNECTION_IDS:Ljava/lang/String; = "CurrentConnectionIDs"

.field public static final DATE:Ljava/lang/String; = "dc:date"

.field public static final DESCRIPTION:Ljava/lang/String; = "dc:description"

.field public static final DESTROYOBJECT:Ljava/lang/String; = "DestroyObject"

.field public static final DLNAMANAGED:Ljava/lang/String; = "dlna:dlnaManaged"

.field public static final FEATURE_LIST:Ljava/lang/String; = "FeatureList"

.field public static final GENRE:Ljava/lang/String; = "upnp:genre"

.field public static final GET_CONTENTS_FEATURES:Ljava/lang/String; = "getcontentFeatures.dlna.org"

.field public static final GET_CURRENT_CONNECTION_IDS:Ljava/lang/String; = "GetCurrentConnectionIDs"

.field public static final GET_CURRENT_CONNECTION_INFO:Ljava/lang/String; = "GetCurrentConnectionInfo"

.field public static final GET_PROTOCOL_INFO:Ljava/lang/String; = "GetProtocolInfo"

.field public static final GET_SEARCH_CAPABILITIES:Ljava/lang/String; = "GetSearchCapabilities"

.field public static final GET_SORT_CAPABILITIES:Ljava/lang/String; = "GetSortCapabilities"

.field public static final GET_SYSTEM_UPDATE_ID:Ljava/lang/String; = "GetSystemUpdateID"

.field public static final ID:Ljava/lang/String; = "Id"

.field public static final OBJECT_CONTAINER:Ljava/lang/String; = "object.container"

.field public static final OBJECT_ITEM:Ljava/lang/String; = "object.item"

.field public static final OBJECT_ITEM_AUDIOITEM:Ljava/lang/String; = "object.item.audioItem"

.field public static final OBJECT_ITEM_AUDIOITEM_MUSICTRACK:Ljava/lang/String; = "object.item.audioItem.musicTrack"

.field public static final OBJECT_ITEM_IMAGEITEM:Ljava/lang/String; = "object.item.imageItem"

.field public static final OBJECT_ITEM_IMAGEITEM_PHOTO:Ljava/lang/String; = "object.item.imageItem.photo"

.field public static final OBJECT_ITEM_VIDEOITEM:Ljava/lang/String; = "object.item.videoItem"

.field public static final OBJECT_ITEM_VIDEOITEM_MOVIE:Ljava/lang/String; = "object.item.videoItem.movie"

.field public static final PARENTID:Ljava/lang/String; = "parentID"

.field public static final PREPARE_FOR_CONNECTION:Ljava/lang/String; = "PrepareForConnection"

.field public static final REAL_TIME_INFO:Ljava/lang/String; = "realTimeInfo.dlna.org"

.field public static final RES:Ljava/lang/String; = "res"

.field public static final RESTICTED:Ljava/lang/String; = "restricted"

.field public static final RESULT:Ljava/lang/String; = "Result"

.field public static final RES_DLNARESUMEUPLOAD:Ljava/lang/String; = "dlna:resumeUpload"

.field public static final RES_DURATION:Ljava/lang/String; = "duration"

.field public static final RES_IMPORT_URI:Ljava/lang/String; = "importUri"

.field public static final RES_PROTOCOLINFO:Ljava/lang/String; = "protocolInfo"

.field public static final RES_RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final RES_SIZE:Ljava/lang/String; = "size"

.field public static final SEARCH:Ljava/lang/String; = "Search"

.field public static final SEARCHABLE:Ljava/lang/String; = "searchable"

.field public static final SEARCH_CAPS:Ljava/lang/String; = "SearchCaps"

.field public static final SEC_TYPE:Ljava/lang/String; = "sec:type"

.field public static final SINK_PROTOCOL_INFO:Ljava/lang/String; = "SinkProtocolInfo"

.field public static final SORT_CAPS:Ljava/lang/String; = "SortCaps"

.field public static final SOURCE_PROTOCOL_INFO:Ljava/lang/String; = "SourceProtocolInfo"

.field public static final STORAGE_MEDIUM:Ljava/lang/String; = "upnp:storageMedium"

.field public static final STORAGE_USED:Ljava/lang/String; = "upnp:storageUsed"

.field public static final SYSTEM_UPDATE_ID:Ljava/lang/String; = "SystemUpdateID"

.field public static final TIME_SEEK:Ljava/lang/String; = "TimeSeekRange.dlna.org"

.field public static final TITLE:Ljava/lang/String; = "dc:title"

.field public static final TRANSFER_MODE:Ljava/lang/String; = "transferMode.dlna.org"

.field public static final TRANSFER_MODE_BACKGROUND:Ljava/lang/String; = "Background"

.field public static final TRANSFER_MODE_INTERACTIVE:Ljava/lang/String; = "Interactive"

.field public static final TRANSFER_MODE_STREAMING:Ljava/lang/String; = "Streaming"

.field public static final WRITE_STATUS:Ljava/lang/String; = "upnp:writeStatus"

.field public static final XGETFEATURELIST:Ljava/lang/String; = "X_GetFeatureList"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    return-void
.end method
