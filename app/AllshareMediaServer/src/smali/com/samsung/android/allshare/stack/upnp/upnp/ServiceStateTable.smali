.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "ServiceStateTable.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;",
        ">;"
    }
.end annotation


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "serviceStateTable"

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 31
    return-void
.end method


# virtual methods
.method public getStateVariable(I)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    return-object v0
.end method
