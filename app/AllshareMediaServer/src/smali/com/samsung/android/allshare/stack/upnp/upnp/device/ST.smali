.class public Lcom/samsung/android/allshare/stack/upnp/upnp/device/ST;
.super Ljava/lang/Object;
.source "ST.java"


# static fields
.field public static final ALL_DEVICE:Ljava/lang/String; = "ssdp:all"

.field public static final PMR_DEVICE:Ljava/lang/String; = "urn:samsung.com:device:PersonalMessageReceiver:1"

.field public static final PMR_SERVICE:Ljava/lang/String; = "urn:samsung.com:service:MessageBoxService:1"

.field public static final ROOT_DEVICE:Ljava/lang/String; = "upnp:rootdevice"

.field public static final URN_DEVICE:Ljava/lang/String; = "urn:"

.field public static final URN_SERVICE:Ljava/lang/String; = "urn:"

.field public static final UUID_DEVICE:Ljava/lang/String; = "uuid"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final isAllDevice(Ljava/lang/String;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 41
    if-nez p0, :cond_1

    .line 42
    const/4 v0, 0x0

    .line 45
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    const-string v1, "ssdp:all"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 45
    const-string v0, "\"ssdp:all\""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isPMRDevice(Ljava/lang/String;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 57
    if-nez p0, :cond_1

    .line 58
    const/4 v0, 0x0

    .line 61
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    const-string v1, "urn:samsung.com:device:PersonalMessageReceiver:1"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 61
    const-string v0, "\"urn:samsung.com:device:PersonalMessageReceiver:1\""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isRootDevice(Ljava/lang/String;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 49
    if-nez p0, :cond_1

    .line 50
    const/4 v0, 0x0

    .line 53
    :cond_0
    :goto_0
    return v0

    .line 51
    :cond_1
    const-string v1, "upnp:rootdevice"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 53
    const-string v0, "\"upnp:rootdevice\""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isURNDevice(Ljava/lang/String;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 73
    if-nez p0, :cond_1

    .line 74
    const/4 v0, 0x0

    .line 77
    :cond_0
    :goto_0
    return v0

    .line 75
    :cond_1
    const-string v1, "urn:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 77
    const-string v0, "\"urn:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isURNService(Ljava/lang/String;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 81
    if-nez p0, :cond_1

    .line 82
    const/4 v0, 0x0

    .line 85
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    const-string v1, "urn:"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 85
    const-string v0, "\"urn:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isUUIDDevice(Ljava/lang/String;)Z
    .locals 2
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 65
    if-nez p0, :cond_1

    .line 66
    const/4 v0, 0x0

    .line 69
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    const-string v1, "uuid"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 69
    const-string v0, "\"uuid"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public isPMRService(Ljava/lang/String;)Z
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 89
    if-nez p1, :cond_1

    .line 90
    const/4 v0, 0x0

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    const-string v1, "urn:samsung.com:service:MessageBoxService:1"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 93
    const-string v0, "\"urn:samsung.com:service:MessageBoxService:1"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
