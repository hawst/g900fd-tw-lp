.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;
.super Ljava/lang/Object;
.source "HTTPUSocket.java"


# instance fields
.field private mNetworkInterface:Ljava/net/NetworkInterface;

.field private mSsdpUniSock:Ljava/net/DatagramSocket;

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mTag:Ljava/lang/String;

    .line 49
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    .line 78
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mNetworkInterface:Ljava/net/NetworkInterface;

    .line 61
    return-void
.end method


# virtual methods
.method public close()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 133
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    if-nez v2, :cond_0

    .line 144
    :goto_0
    return v1

    .line 137
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    invoke-virtual {v2}, Ljava/net/DatagramSocket;->close()V

    .line 138
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mTag:Ljava/lang/String;

    const-string v2, "close"

    const-string v3, "close Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 141
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLocalAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    if-nez v0, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    invoke-virtual {v0}, Ljava/net/DatagramSocket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v0

    goto :goto_0
.end method

.method public getNetworkInterface()Ljava/net/NetworkInterface;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mNetworkInterface:Ljava/net/NetworkInterface;

    return-object v0
.end method

.method protected getSocket()Ljava/net/DatagramSocket;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    return-object v0
.end method

.method public open(Ljava/net/InetAddress;I)Z
    .locals 6
    .param p1, "bindAddr"    # Ljava/net/InetAddress;
    .param p2, "bindPort"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->close()Z

    .line 96
    :try_start_0
    new-instance v4, Ljava/net/DatagramSocket;

    invoke-direct {v4, p2, p1}, Ljava/net/DatagramSocket;-><init>(ILjava/net/InetAddress;)V

    iput-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    .line 99
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/net/DatagramSocket;->setReuseAddress(Z)V
    :try_end_0
    .catch Ljava/net/BindException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 107
    :goto_0
    return v2

    .line 100
    :catch_0
    move-exception v0

    .local v0, "be":Ljava/net/BindException;
    move v2, v3

    .line 101
    goto :goto_0

    .line 102
    .end local v0    # "be":Ljava/net/BindException;
    :catch_1
    move-exception v1

    .line 103
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mTag:Ljava/lang/String;

    const-string v4, "open"

    const-string v5, "open Exception"

    invoke-static {v2, v4, v5, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move v2, v3

    .line 104
    goto :goto_0
.end method

.method public post(Ljava/net/InetAddress;ILjava/lang/String;)Z
    .locals 6
    .param p1, "dest"    # Ljava/net/InetAddress;
    .param p2, "port"    # I
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 154
    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 155
    .local v2, "packet":[B
    new-instance v0, Ljava/net/DatagramPacket;

    array-length v3, v2

    invoke-direct {v0, v2, v3, p1, p2}, Ljava/net/DatagramPacket;-><init>([BILjava/net/InetAddress;I)V

    .line 157
    .local v0, "dgmPacket":Ljava/net/DatagramPacket;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    if-eqz v3, :cond_0

    .line 158
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    invoke-virtual {v3, v0}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :cond_0
    const/4 v3, 0x1

    .end local v0    # "dgmPacket":Ljava/net/DatagramPacket;
    .end local v2    # "packet":[B
    :goto_0
    return v3

    .line 160
    :catch_0
    move-exception v1

    .line 166
    .local v1, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mTag:Ljava/lang/String;

    const-string v4, "post"

    const-string v5, "post Exception"

    invoke-static {v3, v4, v5, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 167
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public receive()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 177
    const/16 v4, 0x400

    new-array v2, v4, [B

    .line 178
    .local v2, "ssdvRecvBuf":[B
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    array-length v4, v2

    invoke-direct {v1, v2, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;-><init>([BI)V

    .line 181
    .local v1, "recvPacket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mSsdpUniSock:Ljava/net/DatagramSocket;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDatagramPacket()Ljava/net/DatagramPacket;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/net/DatagramSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 182
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->setTimeStamp(J)V

    .line 183
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->getNetworkInterface()Ljava/net/NetworkInterface;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->setNetworkInterface(Ljava/net/NetworkInterface;)V
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 191
    .end local v1    # "recvPacket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    :goto_0
    return-object v1

    .line 184
    .restart local v1    # "recvPacket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/net/SocketTimeoutException;
    move-object v1, v3

    .line 186
    goto :goto_0

    .line 187
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mTag:Ljava/lang/String;

    const-string v5, "receive"

    const-string v6, "receive Exception"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v1, v3

    .line 189
    goto :goto_0
.end method

.method public setNetworkInterface(Ljava/net/NetworkInterface;)V
    .locals 0
    .param p1, "ni"    # Ljava/net/NetworkInterface;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;->mNetworkInterface:Ljava/net/NetworkInterface;

    .line 86
    return-void
.end method
