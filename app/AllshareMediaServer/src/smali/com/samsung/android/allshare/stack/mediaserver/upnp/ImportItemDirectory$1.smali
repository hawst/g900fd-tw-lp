.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;
.super Ljava/lang/Thread;
.source "ImportItemDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->startAutoDestroy()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final DEFAULT_SLEEP_TIME:I = 0x3e8


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    .line 82
    const-string v9, "CDS Autodestroy Worker"

    invoke-virtual {p0, v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;->setName(Ljava/lang/String;)V

    .line 85
    :goto_0
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mAutoDestroyWorker:Ljava/lang/Thread;
    invoke-static {v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;)Ljava/lang/Thread;

    move-result-object v9

    if-nez v9, :cond_0

    .line 106
    :goto_1
    return-void

    .line 88
    :cond_0
    const/4 v6, 0x0

    .line 90
    .local v6, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;>;"
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mMutex:Ljava/lang/Object;
    invoke-static {v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;)Ljava/lang/Object;

    move-result-object v10

    monitor-enter v10

    .line 91
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    invoke-virtual {v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->getItems()Ljava/util/Collection;

    move-result-object v6

    .line 92
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 93
    .local v7, "iteratorItems":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;>;"
    const/4 v3, 0x0

    .line 95
    .local v3, "hasRemovableItem":Z
    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    .line 96
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .line 98
    .local v5, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->isRemovable()Z

    move-result v9

    if-ne v9, v12, :cond_1

    .line 99
    const/4 v3, 0x1

    .line 104
    .end local v5    # "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    :cond_2
    if-nez v3, :cond_3

    .line 105
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    const/4 v11, 0x0

    # setter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->mAutoDestroyWorker:Ljava/lang/Thread;
    invoke-static {v9, v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->access$002(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 106
    monitor-exit v10

    goto :goto_1

    .line 108
    .end local v3    # "hasRemovableItem":Z
    .end local v7    # "iteratorItems":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;>;"
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    .restart local v3    # "hasRemovableItem":Z
    .restart local v7    # "iteratorItems":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;>;"
    :cond_3
    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 110
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 112
    .local v0, "currTime":J
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    invoke-virtual {v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->getModifiableItems()Ljava/util/Collection;

    move-result-object v6

    .line 113
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 114
    .local v4, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;>;"
    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 115
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .line 119
    .restart local v5    # "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getRestricted()Z

    move-result v9

    if-eq v9, v12, :cond_4

    .line 122
    invoke-virtual {v5, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->reduceLifeTime(J)V

    .line 123
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->isPostAble()Z

    move-result v9

    if-nez v9, :cond_4

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->isRemovable()Z

    move-result v9

    if-ne v9, v12, :cond_4

    .line 124
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getDefaultResource()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    move-result-object v8

    .line 125
    .local v8, "resourceProperty":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    if-eqz v8, :cond_5

    .line 126
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->deleteContent()V

    .line 127
    :cond_5
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    .line 129
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;

    invoke-virtual {v9, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory;->notifyItemDestroyEvent(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;)V

    goto :goto_2

    .line 134
    .end local v5    # "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .end local v8    # "resourceProperty":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    :cond_6
    const-wide/16 v10, 0x3e8

    :try_start_2
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 135
    :catch_0
    move-exception v2

    .line 136
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0
.end method
