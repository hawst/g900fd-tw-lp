.class public Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
.source "MediaStoreImportResourceProperty.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;,
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$Factory;
    }
.end annotation


# static fields
.field private static DELAY_TIME:I = 0x0

.field private static MAX_DELAY_TIME:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MediaStoreImportResourceProperty"


# instance fields
.field private mDelayDeleteHandler:Landroid/os/Handler;

.field private mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 121
    const/16 v0, 0x2710

    sput v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->MAX_DELAY_TIME:I

    .line 123
    const/16 v0, 0x3e8

    sput v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->DELAY_TIME:I

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V
    .locals 4
    .param p1, "property"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    .param p2, "mediaServer"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .prologue
    const/4 v3, 0x0

    .line 75
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->getMediaFile()Ljava/io/File;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 71
    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 125
    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mDelayDeleteHandler:Landroid/os/Handler;

    .line 77
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 79
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getMediaUpdateHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mDelayDeleteHandler:Landroid/os/Handler;

    .line 80
    return-void
.end method

.method static synthetic access$000()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->DELAY_TIME:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    return-object v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->MAX_DELAY_TIME:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mDelayDeleteHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public deleteContent()V
    .locals 8

    .prologue
    .line 84
    invoke-super {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->deleteContent()V

    .line 86
    const/4 v2, 0x0

    .line 87
    .local v2, "uri":Landroid/net/Uri;
    const-string v3, ""

    .line 88
    .local v3, "where":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->getMimeType()Ljava/lang/String;

    move-result-object v1

    .line 90
    .local v1, "type":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    const-string v4, "audio"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 94
    sget-object v2, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 95
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_data=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->getMediaFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 107
    :goto_1
    const-string v4, "MediaStoreImportResourceProperty"

    const-string v5, "deleteContent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "*********where : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const/4 v0, 0x0

    .line 110
    .local v0, "nDeletedCount":I
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 111
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 114
    :cond_2
    const-string v4, "MediaStoreImportResourceProperty"

    const-string v5, "deleteContent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "*********nDeletedCount : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mDelayDeleteHandler:Landroid/os/Handler;

    if-eqz v4, :cond_0

    if-gtz v0, :cond_0

    .line 117
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mDelayDeleteHandler:Landroid/os/Handler;

    new-instance v5, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;

    sget v6, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->DELAY_TIME:I

    invoke-direct {v5, p0, v2, v3, v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;Landroid/net/Uri;Ljava/lang/String;I)V

    sget v6, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->DELAY_TIME:I

    int-to-long v6, v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 96
    .end local v0    # "nDeletedCount":I
    :cond_3
    const-string v4, "video"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 97
    sget-object v2, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 98
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_data=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->getMediaFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 99
    :cond_4
    const-string v4, "image"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 100
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 101
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_data=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->getMediaFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 103
    :cond_5
    const-string v4, "MediaStoreImportResourceProperty"

    const-string v5, "deleteContent"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "deleteContent() type : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
