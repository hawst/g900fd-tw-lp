.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
.source "ContentXMLPrinter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DIDLLiteNode"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 110
    const-string v0, "DIDL-Lite"

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    const-string v0, "xmlns"

    const-string v1, "urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 112
    const-string v0, "xmlns:dc"

    const-string v1, "http://purl.org/dc/elements/1.1/"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 113
    const-string v0, "xmlns:upnp"

    const-string v1, "urn:schemas-upnp-org:metadata-1-0/upnp/"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 114
    const-string v0, "xmlns:dlna"

    const-string v1, "urn:schemas-dlna-org:metadata-1-0/"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 115
    const-string v0, "xmlns:sec"

    const-string v1, "http://www.sec.co.kr/"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 116
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;Ljava/io/PrintWriter;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;
    .param p1, "x1"    # Ljava/io/PrintWriter;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->outputAttributes(Ljava/io/PrintWriter;)V

    return-void
.end method

.method private outputAttributes(Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "ps"    # Ljava/io/PrintWriter;

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite$DIDLLiteNode;->getAttributes()Ljava/util/Set;

    move-result-object v1

    .line 121
    .local v1, "attrs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 122
    .local v0, "attr":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 123
    .local v2, "escapeValue":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "=\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 125
    .end local v0    # "attr":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v2    # "escapeValue":Ljava/lang/String;
    :cond_0
    return-void
.end method
