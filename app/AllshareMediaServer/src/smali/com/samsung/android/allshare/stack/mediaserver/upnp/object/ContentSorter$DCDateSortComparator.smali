.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;
.super Ljava/lang/Object;
.source "ContentSorter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DCDateSortComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
        ">;"
    }
.end annotation


# instance fields
.field mIsASC:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "ascSeq"    # Z

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;->mIsASC:Z

    .line 150
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;->mIsASC:Z

    .line 151
    return-void
.end method


# virtual methods
.method public compare(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)I
    .locals 10
    .param p1, "conNode1"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .param p2, "conNode2"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v6, -0x1

    .line 155
    instance-of v9, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    if-eqz v9, :cond_0

    instance-of v9, p2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    if-nez v9, :cond_2

    :cond_0
    move v6, v8

    .line 168
    :cond_1
    :goto_0
    return v6

    :cond_2
    move-object v0, p1

    .line 158
    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .local v0, "itemNode1":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    move-object v1, p2

    .line 159
    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .line 160
    .local v1, "itemNode2":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getDateTime()J

    move-result-wide v2

    .line 161
    .local v2, "itemTime1":J
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getDateTime()J

    move-result-wide v4

    .line 162
    .local v4, "itemTime2":J
    cmp-long v9, v2, v4

    if-nez v9, :cond_3

    move v6, v8

    .line 163
    goto :goto_0

    .line 165
    :cond_3
    iget-boolean v8, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;->mIsASC:Z

    if-eqz v8, :cond_4

    .line 166
    cmp-long v8, v2, v4

    if-ltz v8, :cond_1

    move v6, v7

    goto :goto_0

    .line 168
    :cond_4
    cmp-long v8, v2, v4

    if-gez v8, :cond_5

    :goto_1
    move v6, v7

    goto :goto_0

    :cond_5
    move v7, v6

    goto :goto_1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 145
    check-cast p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;->compare(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)I

    move-result v0

    return v0
.end method
