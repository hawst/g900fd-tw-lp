.class public Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;
.super Ljava/lang/Object;
.source "NetworkUtils.java"


# static fields
.field public static final IPADDRESS_WILDCARD:Ljava/lang/String; = "@@##$$!!%%^^"

.field private static final TAG:Ljava/lang/String; = "NetworkUtils"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    return-void
.end method

.method public static final getAddressByNIC(Ljava/net/NetworkInterface;)Ljava/util/Collection;
    .locals 4
    .param p0, "ni"    # Ljava/net/NetworkInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/NetworkInterface;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/net/InetAddress;",
            "Ljava/net/NetworkInterface;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 91
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    if-nez p0, :cond_1

    .line 102
    :cond_0
    return-object v1

    .line 96
    :cond_1
    invoke-virtual {p0}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v0

    .line 98
    .local v0, "it":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 99
    new-instance v2, Landroid/util/Pair;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v3, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static final getAllAddress()Ljava/util/Collection;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 46
    .local v5, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/InetAddress;>;"
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getNetworkInterfaceList()Ljava/util/ArrayList;

    move-result-object v4

    .line 48
    .local v4, "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 62
    .end local v4    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :cond_0
    :goto_0
    return-object v5

    .line 51
    .restart local v4    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/NetworkInterface;

    .line 52
    .local v3, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    .line 54
    .local v2, "it":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 55
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 58
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "it":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "ni":Ljava/net/NetworkInterface;
    .end local v4    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "NetworkUtils"

    const-string v7, "getAllAddress"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getAllAddress Exception : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static final getAllAddressNetworkInterfacePair()Ljava/util/Collection;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/net/InetAddress;",
            "Ljava/net/NetworkInterface;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 69
    .local v5, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getNetworkInterfaceList()Ljava/util/ArrayList;

    move-result-object v4

    .line 71
    .local v4, "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_1

    .line 86
    .end local v4    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :cond_0
    :goto_0
    return-object v5

    .line 74
    .restart local v4    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/NetworkInterface;

    .line 75
    .local v3, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v2

    .line 77
    .local v2, "it":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :goto_1
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 78
    new-instance v6, Landroid/util/Pair;

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v7

    invoke-direct {v6, v7, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 81
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "it":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "ni":Ljava/net/NetworkInterface;
    .end local v4    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "NetworkUtils"

    const-string v7, "getAllAddressNetworkInterfacePair"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getAllAddressNetworkInterfacePair Exception : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;
    .locals 16
    .param p0, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 106
    if-nez p0, :cond_0

    .line 107
    const/4 v12, 0x0

    .line 142
    :goto_0
    return-object v12

    .line 110
    :cond_0
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v2

    .line 111
    .local v2, "clientBytes":[B
    if-eqz v2, :cond_1

    array-length v12, v2

    if-nez v12, :cond_2

    .line 112
    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    .line 114
    :cond_2
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getNetworkInterfaceList()Ljava/util/ArrayList;

    move-result-object v10

    .line 116
    .local v10, "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    if-eqz v10, :cond_3

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_4

    .line 117
    :cond_3
    const/4 v12, 0x0

    goto :goto_0

    .line 119
    :cond_4
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/net/NetworkInterface;

    .line 120
    .local v9, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v9}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v1

    .line 121
    .local v1, "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InterfaceAddress;

    .line 122
    .local v0, "addr":Ljava/net/InterfaceAddress;
    invoke-virtual {v0}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v7

    .line 123
    .local v7, "iAddr":Ljava/net/InetAddress;
    if-eqz v7, :cond_6

    .line 126
    invoke-virtual {v0}, Ljava/net/InterfaceAddress;->getNetworkPrefixLength()S

    move-result v12

    div-int/lit8 v8, v12, 0x8

    .line 127
    .local v8, "len":I
    invoke-virtual {v7}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v11

    .line 129
    .local v11, "serverBytes":[B
    if-eqz v11, :cond_6

    .line 132
    invoke-static {v2, v8}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v3

    .line 133
    .local v3, "clientOffset":[B
    invoke-static {v11, v8}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v11

    .line 135
    invoke-static {v11, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 136
    invoke-virtual {v0}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    goto :goto_0

    .line 139
    .end local v0    # "addr":Ljava/net/InterfaceAddress;
    .end local v1    # "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    .end local v2    # "clientBytes":[B
    .end local v3    # "clientOffset":[B
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v7    # "iAddr":Ljava/net/InetAddress;
    .end local v8    # "len":I
    .end local v9    # "ni":Ljava/net/NetworkInterface;
    .end local v10    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    .end local v11    # "serverBytes":[B
    :catch_0
    move-exception v4

    .line 140
    .local v4, "e":Ljava/lang/Exception;
    const-string v12, "NetworkUtils"

    const-string v13, "getBoundInetAddress"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "getBoundInetAddress Exception : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v4}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method public static getIPAsByteArray(Ljava/lang/String;)[B
    .locals 5
    .param p0, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 190
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    .line 191
    .local v1, "inetAddress":Ljava/net/InetAddress;
    invoke-virtual {v1}, Ljava/net/InetAddress;->getAddress()[B
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 195
    .end local v1    # "inetAddress":Ljava/net/InetAddress;
    :goto_0
    return-object v2

    .line 192
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/net/UnknownHostException;
    const-string v2, "NetworkUtils"

    const-string v3, "getIPAsByteArray"

    const-string v4, "getIPAsByteArray Got UnknownHostException : "

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 195
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getInterface(Ljava/lang/String;)Ljava/net/InetAddress;
    .locals 12
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 277
    const/4 v6, 0x0

    .line 280
    .local v6, "result":Ljava/net/InetAddress;
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getNetworkInterfaceList()Ljava/util/ArrayList;

    move-result-object v5

    .line 282
    .local v5, "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    move-object v7, v6

    .line 299
    .end local v5    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    .end local v6    # "result":Ljava/net/InetAddress;
    .local v7, "result":Ljava/net/InetAddress;
    :goto_0
    return-object v7

    .line 285
    .end local v7    # "result":Ljava/net/InetAddress;
    .restart local v5    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    .restart local v6    # "result":Ljava/net/InetAddress;
    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    .line 286
    .local v4, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 287
    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v3

    .line 289
    .local v3, "it":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 290
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, Ljava/net/InetAddress;

    move-object v6, v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 295
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "it":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v4    # "ni":Ljava/net/NetworkInterface;
    .end local v5    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v1

    .line 296
    .local v1, "e":Ljava/lang/Exception;
    const-string v8, "NetworkUtils"

    const-string v9, "getInterface"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "getInterface Exception : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    move-object v7, v6

    .line 299
    .end local v6    # "result":Ljava/net/InetAddress;
    .restart local v7    # "result":Ljava/net/InetAddress;
    goto :goto_0
.end method

.method public static getInterfaceName(Ljava/net/InetAddress;)Ljava/lang/String;
    .locals 11
    .param p0, "boundAddress"    # Ljava/net/InetAddress;

    .prologue
    .line 248
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getNetworkInterfaceList()Ljava/util/ArrayList;

    move-result-object v6

    .line 250
    .local v6, "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 251
    :cond_0
    const-string v4, ""

    .line 272
    .end local v6    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :goto_0
    return-object v4

    .line 253
    .restart local v6    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 254
    .local v5, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v4

    .line 256
    .local v4, "in":Ljava/lang/String;
    if-nez v4, :cond_3

    .line 257
    const-string v4, ""

    goto :goto_0

    .line 259
    :cond_3
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    .line 261
    .local v1, "addresses":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    :cond_4
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 262
    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 264
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-virtual {v0, p0}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-eqz v7, :cond_4

    goto :goto_0

    .line 268
    .end local v0    # "addr":Ljava/net/InetAddress;
    .end local v1    # "addresses":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/InetAddress;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "in":Ljava/lang/String;
    .end local v5    # "ni":Ljava/net/NetworkInterface;
    .end local v6    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v2

    .line 269
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "NetworkUtils"

    const-string v8, "getInterfaceName"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getInterfaceName Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_5
    const-string v4, ""

    goto :goto_0
.end method

.method public static getMacAddrFromArpTable(Ljava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "ipAddr"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 205
    if-nez p0, :cond_1

    move-object v4, v6

    .line 243
    :cond_0
    :goto_0
    return-object v4

    .line 208
    :cond_1
    const/4 v0, 0x0

    .line 209
    .local v0, "br":Ljava/io/BufferedReader;
    const-string v7, "/"

    const-string v8, ""

    invoke-virtual {p0, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 212
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v7, Ljava/io/FileReader;

    const-string v8, "/proc/net/arp"

    invoke-direct {v7, v8}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    .end local v0    # "br":Ljava/io/BufferedReader;
    .local v1, "br":Ljava/io/BufferedReader;
    const/4 v3, 0x0

    .line 216
    .local v3, "line":Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 217
    if-nez v3, :cond_5

    .line 235
    if-eqz v1, :cond_3

    .line 236
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .end local v3    # "line":Ljava/lang/String;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    :cond_4
    :goto_1
    move-object v4, v6

    .line 243
    goto :goto_0

    .line 220
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    :cond_5
    :try_start_3
    const-string v7, " +"

    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 221
    .local v5, "splitted":[Ljava/lang/String;
    if-eqz v5, :cond_2

    array-length v7, v5

    const/4 v8, 0x4

    if-lt v7, v8, :cond_2

    const/4 v7, 0x0

    aget-object v7, v5, v7

    invoke-virtual {p0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 222
    const/4 v7, 0x3

    aget-object v4, v5, v7

    .line 223
    .local v4, "mac":Ljava/lang/String;
    const-string v7, "..:..:..:..:..:.."

    invoke-virtual {v4, v7}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 224
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_6
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v4

    .line 235
    if-eqz v1, :cond_0

    .line 236
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    .line 238
    :catch_0
    move-exception v2

    .line 239
    .local v2, "e":Ljava/io/IOException;
    const-string v6, "NetworkUtils"

    const-string v7, "getMacAddrFromArpTable"

    const-string v8, "getMacAddrFromArpTable Got IOException : "

    invoke-static {v6, v7, v8, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 235
    .end local v2    # "e":Ljava/io/IOException;
    :cond_6
    if-eqz v1, :cond_7

    .line 236
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    :cond_7
    :goto_2
    move-object v4, v6

    .line 241
    goto :goto_0

    .line 238
    :catch_1
    move-exception v2

    .line 239
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "NetworkUtils"

    const-string v8, "getMacAddrFromArpTable"

    const-string v9, "getMacAddrFromArpTable Got IOException : "

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 238
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "mac":Ljava/lang/String;
    .end local v5    # "splitted":[Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 239
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "NetworkUtils"

    const-string v8, "getMacAddrFromArpTable"

    const-string v9, "getMacAddrFromArpTable Got IOException : "

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v1

    .line 242
    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_1

    .line 231
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "line":Ljava/lang/String;
    :catch_3
    move-exception v2

    .line 232
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    :try_start_6
    const-string v7, "NetworkUtils"

    const-string v8, "getMacAddrFromArpTable"

    const-string v9, "getMacAddrFromArpTable Got Exception : "

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 235
    if-eqz v0, :cond_4

    .line 236
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_1

    .line 238
    :catch_4
    move-exception v2

    .line 239
    .local v2, "e":Ljava/io/IOException;
    const-string v7, "NetworkUtils"

    const-string v8, "getMacAddrFromArpTable"

    const-string v9, "getMacAddrFromArpTable Got IOException : "

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 234
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 235
    :goto_4
    if-eqz v0, :cond_8

    .line 236
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 241
    :cond_8
    :goto_5
    throw v6

    .line 238
    :catch_5
    move-exception v2

    .line 239
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v7, "NetworkUtils"

    const-string v8, "getMacAddrFromArpTable"

    const-string v9, "getMacAddrFromArpTable Got IOException : "

    invoke-static {v7, v8, v9, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_5

    .line 234
    .end local v0    # "br":Ljava/io/BufferedReader;
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    .restart local v3    # "line":Ljava/lang/String;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_4

    .line 231
    .end local v0    # "br":Ljava/io/BufferedReader;
    .restart local v1    # "br":Ljava/io/BufferedReader;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1    # "br":Ljava/io/BufferedReader;
    .restart local v0    # "br":Ljava/io/BufferedReader;
    goto :goto_3
.end method

.method private static getNetworkInterfaceList()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/net/NetworkInterface;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v1, "mNetworkInterfaceList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    const/4 v3, 0x0

    .line 308
    .local v3, "nis":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/net/NetworkInterface;>;"
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 312
    :goto_0
    if-nez v3, :cond_1

    .line 320
    :cond_0
    return-object v1

    .line 309
    :catch_0
    move-exception v0

    .line 310
    .local v0, "e":Ljava/net/SocketException;
    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    goto :goto_0

    .line 315
    .end local v0    # "e":Ljava/net/SocketException;
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 316
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/net/NetworkInterface;

    .line 317
    .local v2, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static isIPv6Address(Ljava/lang/String;)Z
    .locals 1
    .param p0, "host"    # Ljava/lang/String;

    .prologue
    .line 199
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 200
    :cond_0
    const/4 v0, 0x0

    .line 201
    :goto_0
    return v0

    :cond_1
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isLocalIPAddress(Ljava/lang/String;)Z
    .locals 12
    .param p0, "address"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 168
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getNetworkInterfaceList()Ljava/util/ArrayList;

    move-result-object v6

    .line 170
    .local v6, "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-nez v9, :cond_1

    :cond_0
    move v7, v8

    .line 183
    .end local v6    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :goto_0
    return v7

    .line 173
    .restart local v6    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :cond_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/net/NetworkInterface;

    .line 174
    .local v5, "ni":Ljava/net/NetworkInterface;
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v1

    .line 175
    .local v1, "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InterfaceAddress;

    .line 176
    .local v0, "addr":Ljava/net/InterfaceAddress;
    invoke-virtual {v0}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v9

    invoke-virtual {v9}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v9

    if-ne v9, v7, :cond_3

    goto :goto_0

    .line 180
    .end local v0    # "addr":Ljava/net/InterfaceAddress;
    .end local v1    # "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "ni":Ljava/net/NetworkInterface;
    .end local v6    # "nis":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/NetworkInterface;>;"
    :catch_0
    move-exception v2

    .line 181
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "NetworkUtils"

    const-string v9, "isLocalIPAddress"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isLocalIPAddress Exception : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v9, v10}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .end local v2    # "e":Ljava/lang/Exception;
    :cond_4
    move v7, v8

    .line 183
    goto :goto_0
.end method

.method public static replaceWildCardIPAddress(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "urlString"    # Ljava/lang/String;
    .param p1, "targetClient"    # Ljava/lang/String;

    .prologue
    .line 146
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 163
    .end local p0    # "urlString":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 152
    .restart local p0    # "urlString":Ljava/lang/String;
    :cond_1
    :try_start_0
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getIPAsByteArray(Ljava/lang/String;)[B

    move-result-object v4

    invoke-static {v4}, Ljava/net/InetAddress;->getByAddress([B)Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 158
    .local v3, "targetAddr":Ljava/net/InetAddress;
    invoke-static {v3}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v0

    .line 159
    .local v0, "bindAddress":Ljava/net/InetAddress;
    if-eqz v0, :cond_0

    .line 162
    const-string v4, "@@##$$!!%%^^"

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .local v2, "result":Ljava/lang/String;
    move-object p0, v2

    .line 163
    goto :goto_0

    .line 153
    .end local v0    # "bindAddress":Ljava/net/InetAddress;
    .end local v2    # "result":Ljava/lang/String;
    .end local v3    # "targetAddr":Ljava/net/InetAddress;
    :catch_0
    move-exception v1

    .line 154
    .local v1, "e":Ljava/net/UnknownHostException;
    goto :goto_0
.end method
