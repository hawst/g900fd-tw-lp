.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDP;
.super Ljava/lang/Object;
.source "SSDP.java"


# static fields
.field public static final ADDRESS:Ljava/lang/String; = "239.255.255.250"

.field public static final DEFAULT_MSEARCH_MX:I = 0x5

.field private static IPV6_ADDRESS:Ljava/lang/String; = null

.field public static final IPV6_ADMINISTRATIVE_ADDRESS:Ljava/lang/String; = "FF04::C"

.field public static final IPV6_GLOBAL_ADDRESS:Ljava/lang/String; = "FF0E::C"

.field public static final IPV6_LINK_LOCAL_ADDRESS:Ljava/lang/String; = "FF02::C"

.field public static final IPV6_SITE_LOCAL_ADDRESS:Ljava/lang/String; = "FF05::C"

.field public static final IPV6_SUBNET_ADDRESS:Ljava/lang/String; = "FF03::C"

.field public static final P2P_ADDRESS:Ljava/lang/String; = "239.255.255.177"

.field public static final PORT:I = 0x76c

.field public static final RECV_MESSAGE_BUFSIZE:I = 0x400

.field private static final TAG:Ljava/lang/String; = "SSDP"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-string v0, "FF02::C"

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDP;->setIPv6Address(Ljava/lang/String;)V

    .line 77
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final getIPv6Address()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDP;->IPV6_ADDRESS:Ljava/lang/String;

    return-object v0
.end method

.method public static final getLeaseTime(Ljava/lang/String;)I
    .locals 9
    .param p0, "cacheCont"    # Ljava/lang/String;

    .prologue
    .line 84
    const/16 v5, 0x3d

    invoke-virtual {p0, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 85
    .local v1, "equIdx":I
    const/4 v2, 0x0

    .line 86
    .local v2, "mx":I
    const/4 v3, 0x0

    .line 88
    .local v3, "mxStr":Ljava/lang/String;
    :try_start_0
    new-instance v4, Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v8, v1, 0x1

    sub-int/2addr v7, v8

    invoke-direct {v4, v5, v6, v7}, Ljava/lang/String;-><init>([BII)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    .end local v3    # "mxStr":Ljava/lang/String;
    .local v4, "mxStr":Ljava/lang/String;
    :try_start_1
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 91
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    :cond_0
    move-object v3, v4

    .line 96
    .end local v4    # "mxStr":Ljava/lang/String;
    .restart local v3    # "mxStr":Ljava/lang/String;
    :goto_0
    return v2

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    const-string v5, "SSDP"

    const-string v6, "getLeaseTime"

    const-string v7, "getLeaseTime - Exception "

    invoke-static {v5, v6, v7, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 93
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v3    # "mxStr":Ljava/lang/String;
    .restart local v4    # "mxStr":Ljava/lang/String;
    :catch_1
    move-exception v0

    move-object v3, v4

    .end local v4    # "mxStr":Ljava/lang/String;
    .restart local v3    # "mxStr":Ljava/lang/String;
    goto :goto_1
.end method

.method public static final setIPv6Address(Ljava/lang/String;)V
    .locals 0
    .param p0, "addr"    # Ljava/lang/String;

    .prologue
    .line 60
    sput-object p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDP;->IPV6_ADDRESS:Ljava/lang/String;

    .line 61
    return-void
.end method
