.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;
.super Ljava/lang/Object;
.source "ContentSorter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DCTitleSortComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
        ">;"
    }
.end annotation


# instance fields
.field mIsASC:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1
    .param p1, "ascSeq"    # Z

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;->mIsASC:Z

    .line 101
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;->mIsASC:Z

    .line 102
    return-void
.end method


# virtual methods
.method public compare(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)I
    .locals 3
    .param p1, "conNode1"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .param p2, "conNode2"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .prologue
    const/4 v2, 0x0

    .line 106
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v2

    .line 108
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "title1":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 110
    .local v1, "title2":Ljava/lang/String;
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 113
    iget-boolean v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;->mIsASC:Z

    if-eqz v2, :cond_2

    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    neg-int v2, v2

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 96
    check-cast p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;->compare(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)I

    move-result v0

    return v0
.end method
