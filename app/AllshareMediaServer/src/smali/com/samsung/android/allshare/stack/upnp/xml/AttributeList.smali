.class public Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "AttributeList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 21
    return-void
.end method


# virtual methods
.method public getAttribute(I)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    return-object v0
.end method

.method public getAttribute(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 28
    if-nez p1, :cond_1

    move-object v0, v3

    .line 37
    :cond_0
    :goto_0
    return-object v0

    .line 31
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;->size()I

    move-result v2

    .line 32
    .local v2, "nLists":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_1
    if-ge v1, v2, :cond_2

    .line 33
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/AttributeList;->getAttribute(I)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    move-result-object v0

    .line 34
    .local v0, "elem":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-eqz v4, :cond_0

    .line 32
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "elem":Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
    :cond_2
    move-object v0, v3

    .line 37
    goto :goto_0
.end method
