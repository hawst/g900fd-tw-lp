.class public final Lcom/samsung/android/allshare/stack/upnp/util/FileUtil;
.super Ljava/lang/Object;
.source "FileUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final isXMLFileName(Ljava/lang/String;)Z
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 61
    invoke-static {p0}, Lcom/samsung/android/allshare/stack/upnp/util/StringUtil;->hasData(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    const/4 v1, 0x0

    .line 64
    :goto_0
    return v1

    .line 63
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "lowerName":Ljava/lang/String;
    const-string v1, "xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method
