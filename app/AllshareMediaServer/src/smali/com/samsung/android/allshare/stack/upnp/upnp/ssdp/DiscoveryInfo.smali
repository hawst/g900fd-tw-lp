.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
.super Ljava/lang/Object;
.source "DiscoveryInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;
    }
.end annotation


# instance fields
.field private mMulticastAddressList:[Ljava/lang/String;

.field private mNetworkInterfaceName:Ljava/lang/String;

.field private mNetworkInterfaceType:Ljava/lang/String;

.field private mSearchDeviceList:[Ljava/lang/String;

.field private mSupportLinkLocal:Z

.field private mSupportLoopback:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceType:Ljava/lang/String;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceName:Ljava/lang/String;

    .line 27
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSearchDeviceList:[Ljava/lang/String;

    .line 29
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mMulticastAddressList:[Ljava/lang/String;

    .line 31
    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSupportLoopback:Z

    .line 33
    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSupportLinkLocal:Z

    .line 98
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceType:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$102(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$202(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mMulticastAddressList:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$302(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSearchDeviceList:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSupportLinkLocal:Z

    return p1
.end method

.method static synthetic access$502(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSupportLoopback:Z

    return p1
.end method

.method private compareDeviceTypeToCandinate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "deviceType"    # Ljava/lang/String;
    .param p2, "candinate"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 83
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v1

    .line 86
    :cond_1
    const-string v2, ":"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 88
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 91
    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 92
    invoke-virtual {p2, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public getMulticastAddressList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mMulticastAddressList:[Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkInterfaceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceName:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkInterfaceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceType:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchDeviceTargetList()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSearchDeviceList:[Ljava/lang/String;

    return-object v0
.end method

.method public isSearchTarget(Ljava/lang/String;)Z
    .locals 5
    .param p1, "candinate"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSearchDeviceList:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 76
    .local v3, "str":Ljava/lang/String;
    invoke-direct {p0, v3, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->compareDeviceTypeToCandinate(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 77
    const/4 v4, 0x1

    .line 79
    .end local v3    # "str":Ljava/lang/String;
    :goto_1
    return v4

    .line 75
    .restart local v3    # "str":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 79
    .end local v3    # "str":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public setNetworkInterfaceName(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p1, "networInterfaceName"    # Ljava/lang/String;

    .prologue
    .line 42
    if-nez p1, :cond_0

    .line 43
    iget-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceName:Ljava/lang/String;

    .line 44
    .end local p1    # "networInterfaceName":Ljava/lang/String;
    :goto_0
    return-object p1

    .restart local p1    # "networInterfaceName":Ljava/lang/String;
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mNetworkInterfaceName:Ljava/lang/String;

    goto :goto_0
.end method

.method public supportLinkLocal()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSupportLinkLocal:Z

    return v0
.end method

.method public supportLoopback()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->mSupportLoopback:Z

    return v0
.end method
