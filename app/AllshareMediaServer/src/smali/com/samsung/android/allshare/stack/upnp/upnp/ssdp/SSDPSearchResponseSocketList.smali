.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;
.super Ljava/util/ArrayList;
.source "SSDPSearchResponseSocketList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 49
    return-void
.end method

.method private post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;Ljava/lang/String;I)Z
    .locals 5
    .param p1, "sock"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    .param p2, "req"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;
    .param p3, "destAddress"    # Ljava/lang/String;
    .param p4, "port"    # I

    .prologue
    const/4 v3, 0x0

    .line 127
    if-nez p2, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v3

    .line 129
    :cond_1
    :try_start_0
    invoke-static {p3}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    .line 130
    .local v0, "dest":Ljava/net/InetAddress;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v2

    .line 131
    .local v2, "local":Ljava/net/InetAddress;
    instance-of v4, v0, Ljava/net/Inet4Address;

    if-eqz v4, :cond_2

    instance-of v4, v2, Ljava/net/Inet4Address;

    if-eqz v4, :cond_2

    .line 132
    invoke-virtual {p2, p3, p4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;->setHost(Ljava/lang/String;I)V

    .line 133
    invoke-virtual {p1, v0, p4, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->post(Ljava/net/InetAddress;ILcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;)Z

    move-result v3

    goto :goto_0

    .line 134
    :cond_2
    instance-of v4, v0, Ljava/net/Inet6Address;

    if-eqz v4, :cond_0

    instance-of v4, v2, Ljava/net/Inet6Address;

    if-eqz v4, :cond_0

    .line 135
    invoke-virtual {p2, p3, p4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;->setHost(Ljava/lang/String;I)V

    .line 136
    invoke-virtual {p1, v0, p4, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->post(Ljava/net/InetAddress;ILcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;)Z
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    goto :goto_0

    .line 140
    .end local v0    # "dest":Ljava/net/InetAddress;
    .end local v2    # "local":Ljava/net/InetAddress;
    :catch_0
    move-exception v1

    .line 141
    .local v1, "e":Ljava/net/UnknownHostException;
    goto :goto_0
.end method


# virtual methods
.method public post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;)Z
    .locals 10
    .param p1, "req"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;

    .prologue
    .line 79
    if-nez p1, :cond_1

    .line 80
    const/4 v7, 0x0

    .line 94
    :cond_0
    return v7

    .line 81
    :cond_1
    const/4 v7, 0x1

    .line 82
    .local v7, "ret":Z
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;

    .line 83
    .local v8, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    if-eqz v8, :cond_2

    .line 87
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->getDiscoveryInfo()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-result-object v4

    .line 88
    .local v4, "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getMulticastAddressList()[Ljava/lang/String;

    move-result-object v6

    .line 90
    .local v6, "multicastAddrs":[Ljava/lang/String;
    move-object v1, v6

    .local v1, "arr$":[Ljava/lang/String;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v0, v1, v3

    .line 91
    .local v0, "addr":Ljava/lang/String;
    const/16 v9, 0x76c

    invoke-direct {p0, v8, p1, v0, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;Ljava/lang/String;I)Z

    move-result v7

    .line 90
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public postAllSearchTarget()Z
    .locals 19

    .prologue
    .line 98
    const/4 v13, 0x0

    .line 99
    .local v13, "ret":Z
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;

    .line 100
    .local v14, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    if-eqz v14, :cond_0

    .line 104
    invoke-virtual {v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->getDiscoveryInfo()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-result-object v8

    .line 105
    .local v8, "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getSearchDeviceTargetList()[Ljava/lang/String;

    move-result-object v16

    .line 106
    .local v16, "targetList":[Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getMulticastAddressList()[Ljava/lang/String;

    move-result-object v11

    .line 108
    .local v11, "multicastAddrs":[Ljava/lang/String;
    move-object v3, v11

    .local v3, "arr$":[Ljava/lang/String;
    array-length v9, v3

    .local v9, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v7, v6

    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v9    # "len$":I
    .local v7, "i$":I
    :goto_0
    if-ge v7, v9, :cond_0

    aget-object v2, v3, v7

    .line 109
    .local v2, "addr":Ljava/lang/String;
    new-instance v12, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;

    const-string v17, "upnp:rootdevice"

    const/16 v18, 0x5

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-direct {v12, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;-><init>(Ljava/lang/String;I)V

    .line 112
    .local v12, "req":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;
    const/16 v17, 0x76c

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v14, v12, v2, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;Ljava/lang/String;I)Z

    move-result v13

    .line 114
    move-object/from16 v4, v16

    .local v4, "arr$":[Ljava/lang/String;
    array-length v10, v4

    .local v10, "len$":I
    const/4 v6, 0x0

    .end local v7    # "i$":I
    .restart local v6    # "i$":I
    :goto_1
    if-ge v6, v10, :cond_1

    aget-object v15, v4, v6

    .line 115
    .local v15, "target":Ljava/lang/String;
    new-instance v12, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;

    .end local v12    # "req":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;
    const/16 v17, 0x5

    move/from16 v0, v17

    invoke-direct {v12, v15, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;-><init>(Ljava/lang/String;I)V

    .line 117
    .restart local v12    # "req":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;
    const/16 v17, 0x76c

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v14, v12, v2, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;Ljava/lang/String;I)Z

    move-result v13

    .line 114
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 108
    .end local v15    # "target":Ljava/lang/String;
    :cond_1
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    .end local v6    # "i$":I
    .restart local v7    # "i$":I
    goto :goto_0

    .line 121
    .end local v2    # "addr":Ljava/lang/String;
    .end local v4    # "arr$":[Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v8    # "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .end local v10    # "len$":I
    .end local v11    # "multicastAddrs":[Ljava/lang/String;
    .end local v12    # "req":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;
    .end local v14    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    .end local v16    # "targetList":[Ljava/lang/String;
    :cond_2
    return v13
.end method

.method public setSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;

    .line 57
    .local v1, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->setSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    goto :goto_0

    .line 58
    .end local v1    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    :cond_0
    return-void
.end method

.method public start(I)V
    .locals 3
    .param p1, "mx"    # I

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;

    .line 66
    .local v1, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->start(I)V

    goto :goto_0

    .line 67
    .end local v1    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;

    .line 71
    .local v1, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->stop()V

    goto :goto_0

    .line 72
    .end local v1    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    :cond_0
    return-void
.end method
