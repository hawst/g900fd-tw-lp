.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;
.super Ljava/lang/Object;
.source "FeatureListActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# static fields
.field private static final FEATURE_END_TAG:Ljava/lang/String; = "</Feature>"

.field private static final FEATURE_START_TAG:Ljava/lang/String; = "<Feature name=\"samsung.com_BASICVIEW\" version=\"1\">"

.field private static final XMLNS_FEATURE:Ljava/lang/String; = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Features xmlns=\"urn:schemas-upnp-org:av:avs\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:schemas-upnp-org:av:avs http://www.upnp.org/schemas/av/avs.xsd\">\n"

.field private static final XMLNS_FEATURE_END_TAG:Ljava/lang/String; = "</Features>"


# instance fields
.field private mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)V
    .locals 1
    .param p1, "cds"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .line 43
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .line 44
    return-void
.end method

.method private getFeatureList(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)Z
    .locals 5
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 55
    .local v0, "containerList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    const-string v4, "image/*"

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->findDirectoryByMimeType(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v2

    .line 56
    .local v2, "parentDir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-eqz v2, :cond_0

    .line 57
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "object.item.imageItem"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    const-string v4, "audio/*"

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->findDirectoryByMimeType(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v2

    .line 60
    if-eqz v2, :cond_1

    .line 61
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "object.item.audioItem"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    const-string v4, "video/*"

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->findDirectoryByMimeType(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v2

    .line 64
    if-eqz v2, :cond_2

    .line 65
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getID()Ljava/lang/String;

    move-result-object v3

    const-string v4, "object.item.videoItem"

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    :cond_2
    const-string v3, "FeatureList"

    invoke-virtual {p1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v1

    .line 68
    .local v1, "featureArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v1, :cond_3

    .line 69
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;->makeFeatureList(Ljava/util/HashMap;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 76
    const/4 v3, 0x1

    :goto_0
    return v3

    .line 72
    :cond_3
    const/16 v3, 0x192

    invoke-virtual {p1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    .line 74
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private makeFeatureList(Ljava/util/HashMap;)Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 90
    .local p1, "containerList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 91
    .local v4, "sb":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 92
    .local v3, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 93
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 94
    .local v1, "data":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<container id=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\" type=\""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\"/>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "container":Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 100
    .end local v0    # "container":Ljava/lang/String;
    .end local v1    # "data":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .end local v4    # "sb":Ljava/lang/StringBuilder;
    :catch_0
    move-exception v2

    .line 101
    .local v2, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v2}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    .line 103
    const-string v5, ""

    .end local v2    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :goto_1
    return-object v5

    .line 98
    .restart local v3    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    .restart local v4    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    :try_start_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Features xmlns=\"urn:schemas-upnp-org:av:avs\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:schemas-upnp-org:av:avs http://www.upnp.org/schemas/av/avs.xsd\">\n<Feature name=\"samsung.com_BASICVIEW\" version=\"1\">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</Feature>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "</Features>"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    goto :goto_1
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 1
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/FeatureListActionHandler;->getFeatureList(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)Z

    move-result v0

    return v0
.end method
