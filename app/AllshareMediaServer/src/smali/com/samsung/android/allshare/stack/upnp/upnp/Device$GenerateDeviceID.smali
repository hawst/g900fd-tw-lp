.class Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;
.super Ljava/lang/Object;
.source "Device.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GenerateDeviceID"
.end annotation


# instance fields
.field private mUdn:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;Ljava/lang/String;)V
    .locals 1
    .param p2, "udn"    # Ljava/lang/String;

    .prologue
    .line 2186
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;->mUdn:Ljava/lang/String;

    .line 2187
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;->mUdn:Ljava/lang/String;

    .line 2188
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 2192
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;->mUdn:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2216
    :goto_0
    return-void

    .line 2196
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;->mUdn:Ljava/lang/String;

    invoke-static {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/USN;->getUUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2198
    .local v4, "uuid":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getBoundInterfaceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2199
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAllAddress()Ljava/util/Collection;

    move-result-object v2

    .line 2200
    .local v2, "addressList":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/net/InetAddress;>;"
    const-string v1, ""

    .line 2201
    .local v1, "addrName":Ljava/lang/String;
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 2202
    .local v0, "addr":Ljava/net/InetAddress;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_1

    .line 2205
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/allshare/stack/util/DLog$AddressChecker;->isIPv4Address(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2206
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getInterfaceName(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v1

    .line 2211
    .end local v0    # "addr":Ljava/net/InetAddress;
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "+"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setID(Ljava/lang/String;)V

    goto :goto_0

    .line 2213
    .end local v1    # "addrName":Ljava/lang/String;
    .end local v2    # "addressList":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/net/InetAddress;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "+"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getBoundInterfaceName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setID(Ljava/lang/String;)V

    goto :goto_0
.end method
