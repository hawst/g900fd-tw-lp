.class public Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;
.super Ljava/lang/Object;
.source "Attribute.java"


# instance fields
.field private mName:Ljava/lang/String;

.field private mValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->mName:Ljava/lang/String;

    .line 15
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->mValue:Ljava/lang/String;

    .line 22
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->setName(Ljava/lang/String;)V

    .line 23
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->setValue(Ljava/lang/String;)V

    .line 24
    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->mValue:Ljava/lang/String;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    if-nez p1, :cond_0

    .line 34
    :goto_0
    return-void

    .line 33
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->mName:Ljava/lang/String;

    goto :goto_0
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->mName:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 48
    :goto_0
    return-void

    .line 47
    :cond_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->mValue:Ljava/lang/String;

    goto :goto_0
.end method
