.class public Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;
.source "QueryRequest.java"


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 0
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;-><init>()V

    .line 41
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;->set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 42
    return-void
.end method

.method private getVarNameNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 50
    .local v0, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-object v2

    .line 52
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->hasNodes()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 54
    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 55
    .local v1, "queryStateVarNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v1, :cond_0

    .line 57
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->hasNodes()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 59
    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public getVarName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;->getVarNameNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 64
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 65
    const-string v1, ""

    .line 66
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
