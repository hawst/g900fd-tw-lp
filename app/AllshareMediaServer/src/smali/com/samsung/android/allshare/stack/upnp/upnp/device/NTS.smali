.class public Lcom/samsung/android/allshare/stack/upnp/upnp/device/NTS;
.super Ljava/lang/Object;
.source "NTS.java"


# static fields
.field public static final ALIVE:Ljava/lang/String; = "ssdp:alive"

.field public static final BYEBYE:Ljava/lang/String; = "ssdp:byebye"

.field public static final PROPCHANGE:Ljava/lang/String; = "upnp:propchange"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final isAlive(Ljava/lang/String;)Z
    .locals 1
    .param p0, "ntsValue"    # Ljava/lang/String;

    .prologue
    .line 20
    if-nez p0, :cond_0

    .line 21
    const/4 v0, 0x0

    .line 22
    :goto_0
    return v0

    :cond_0
    const-string v0, "ssdp:alive"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isByeBye(Ljava/lang/String;)Z
    .locals 1
    .param p0, "ntsValue"    # Ljava/lang/String;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 28
    :goto_0
    return v0

    :cond_0
    const-string v0, "ssdp:byebye"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static final isEvent(Ljava/lang/String;)Z
    .locals 1
    .param p0, "ntsValue"    # Ljava/lang/String;

    .prologue
    .line 34
    if-nez p0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 36
    :goto_0
    return v0

    :cond_0
    const-string v0, "upnp:propchange"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
