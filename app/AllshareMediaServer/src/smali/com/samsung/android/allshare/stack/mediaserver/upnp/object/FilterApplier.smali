.class public final Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/FilterApplier;
.super Ljava/lang/Object;
.source "FilterApplier.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method public static final apply(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .locals 28
    .param p0, "originalObject"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .param p1, "filter"    # Ljava/lang/String;

    .prologue
    .line 59
    const-string v25, "*"

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->isEmpty()Z

    move-result v25

    if-eqz v25, :cond_2

    :cond_0
    move-object/from16 v9, p0

    .line 176
    :cond_1
    :goto_0
    return-object v9

    .line 62
    :cond_2
    const/4 v9, 0x0

    .line 66
    .local v9, "newObject":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getID()Ljava/lang/String;

    move-result-object v8

    .line 67
    .local v8, "id":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getParentID()Ljava/lang/String;

    move-result-object v12

    .line 68
    .local v12, "parentId":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getRestricted()Z

    move-result v17

    .line 69
    .local v17, "restrict":Z
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getTitle()Ljava/lang/String;

    move-result-object v22

    .line 70
    .local v22, "title":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getUPnPClass()Ljava/lang/String;

    move-result-object v23

    .line 72
    .local v23, "upnpClass":Ljava/lang/String;
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    move/from16 v25, v0

    if-eqz v25, :cond_4

    .line 73
    new-instance v10, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    move-object/from16 v0, v22

    invoke-direct {v10, v8, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .end local v9    # "newObject":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .local v10, "newObject":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    move-object v9, v10

    .line 78
    .end local v10    # "newObject":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .restart local v9    # "newObject":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_3
    :goto_1
    if-nez v9, :cond_5

    .line 79
    const/4 v9, 0x0

    goto :goto_0

    .line 74
    :cond_4
    move-object/from16 v0, p0

    instance-of v0, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move/from16 v25, v0

    if-eqz v25, :cond_3

    .line 75
    new-instance v10, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-direct {v10, v8, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .end local v9    # "newObject":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .restart local v10    # "newObject":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    move-object v9, v10

    .end local v10    # "newObject":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .restart local v9    # "newObject":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    goto :goto_1

    .line 81
    :cond_5
    invoke-virtual {v9, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setParentID(Ljava/lang/String;)V

    .line 82
    move/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setRestricted(Z)V

    .line 84
    const-string v25, "searchable"

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->removeAttribute(Ljava/lang/String;)Z

    .line 89
    new-instance v19, Ljava/util/StringTokenizer;

    const-string v25, ","

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .local v19, "st":Ljava/util/StringTokenizer;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v5, "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_2
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_6

    .line 93
    invoke-virtual/range {v19 .. v19}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v21

    .line 94
    .local v21, "temp":Ljava/lang/String;
    move-object/from16 v0, v21

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 173
    .end local v5    # "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8    # "id":Ljava/lang/String;
    .end local v12    # "parentId":Ljava/lang/String;
    .end local v17    # "restrict":Z
    .end local v19    # "st":Ljava/util/StringTokenizer;
    .end local v21    # "temp":Ljava/lang/String;
    .end local v22    # "title":Ljava/lang/String;
    .end local v23    # "upnpClass":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 174
    .local v4, "e":Ljava/lang/Exception;
    const-string v25, "FilterApplier"

    const-string v26, "apply"

    const-string v27, "Got exception in apply: "

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    move-object/from16 v2, v27

    invoke-static {v0, v1, v2, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0

    .line 97
    .end local v4    # "e":Ljava/lang/Exception;
    .restart local v5    # "filterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8    # "id":Ljava/lang/String;
    .restart local v12    # "parentId":Ljava/lang/String;
    .restart local v17    # "restrict":Z
    .restart local v19    # "st":Ljava/util/StringTokenizer;
    .restart local v22    # "title":Ljava/lang/String;
    .restart local v23    # "upnpClass":Ljava/lang/String;
    :cond_6
    :try_start_1
    const-string v6, ""

    .line 99
    .local v6, "firstStringOfSF":Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 101
    .local v15, "resAttrList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_7
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/String;

    .line 102
    .local v18, "selectedFilter":Ljava/lang/String;
    const/16 v25, 0x0

    const/16 v26, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v25

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 106
    const-string v25, "@"

    move-object/from16 v0, v25

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 108
    const/16 v25, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v18

    .line 110
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 111
    .local v24, "value":Ljava/lang/String;
    if-eqz v24, :cond_7

    .line 112
    move-object/from16 v0, v18

    move-object/from16 v1, v24

    invoke-virtual {v9, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3

    .line 113
    .end local v24    # "value":Ljava/lang/String;
    :cond_8
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/FilterApplier;->isResFilter(Ljava/lang/String;)Z

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_9

    .line 116
    invoke-static/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/FilterApplier;->getResFilterToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 120
    :cond_9
    new-instance v20, Ljava/util/StringTokenizer;

    const-string v25, "@"

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .local v20, "stFilter":Ljava/util/StringTokenizer;
    const-string v14, ""

    .line 122
    .local v14, "propName":Ljava/lang/String;
    const-string v3, ""

    .line 125
    .local v3, "attName":Ljava/lang/String;
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_a

    .line 126
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v14

    .line 129
    :cond_a
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v25

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_b

    .line 130
    invoke-virtual/range {v20 .. v20}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 134
    :cond_b
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v13

    .line 136
    .local v13, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v13, :cond_7

    instance-of v0, v13, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    move/from16 v25, v0

    if-nez v25, :cond_7

    .line 137
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v25

    if-eqz v25, :cond_d

    .line 138
    invoke-virtual {v13, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 140
    .restart local v24    # "value":Ljava/lang/String;
    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v11

    .line 143
    .local v11, "newProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-nez v11, :cond_c

    .line 144
    new-instance v11, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .end local v11    # "newProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getValue()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-direct {v11, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    .restart local v11    # "newProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    :goto_4
    move-object/from16 v0, v24

    invoke-virtual {v11, v3, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 154
    invoke-virtual {v9, v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->addProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    goto/16 :goto_3

    .line 148
    :cond_c
    invoke-virtual {v9, v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->removeProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    goto :goto_4

    .line 161
    .end local v11    # "newProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v24    # "value":Ljava/lang/String;
    :cond_d
    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v25

    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getValue()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    invoke-virtual {v9, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 168
    .end local v3    # "attName":Ljava/lang/String;
    .end local v13    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v14    # "propName":Ljava/lang/String;
    .end local v18    # "selectedFilter":Ljava/lang/String;
    .end local v20    # "stFilter":Ljava/util/StringTokenizer;
    :cond_e
    move-object/from16 v0, p0

    invoke-static {v0, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/FilterApplier;->buildResList(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v16

    .line 169
    .local v16, "resList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;>;"
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 170
    .restart local v13    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-virtual {v9, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->addProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5
.end method

.method public static final apply(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "filter"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .local v2, "newContents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .line 42
    .local v0, "content":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    invoke-static {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/FilterApplier;->apply(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v3

    .line 43
    .local v3, "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    if-eqz v3, :cond_0

    .line 44
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 47
    .end local v0    # "content":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .end local v3    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_1
    return-object v2
.end method

.method private static buildResList(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "originalNode"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;",
            ">;"
        }
    .end annotation

    .prologue
    .line 187
    .local p1, "filterList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v5, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_1

    .line 215
    :cond_0
    return-object v5

    .line 192
    :cond_1
    const-string v7, "protocol"

    invoke-interface {p1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    const-string v7, "protocolInfo"

    invoke-interface {p1, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 196
    const/4 v7, 0x0

    const-string v8, "protocolInfo"

    invoke-interface {p1, v7, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 198
    :cond_2
    instance-of v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    if-eqz v7, :cond_0

    .line 200
    const-string v7, "res"

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getPropertyList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 201
    .local v6, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    instance-of v7, v6, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    if-nez v7, :cond_3

    .line 204
    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v7, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    .local v2, "clone":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributes()Ljava/util/Set;

    move-result-object v1

    .line 207
    .local v1, "attrs":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 208
    .local v0, "attr":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {p1, v7}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 209
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v2, v7, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 212
    .end local v0    # "attr":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_5
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static getResFilterToken(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "filter"    # Ljava/lang/String;

    .prologue
    .line 220
    const-string v2, "res@"

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 221
    new-instance v1, Ljava/util/StringTokenizer;

    invoke-direct {v1, p0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    .line 222
    .local v1, "s":Ljava/util/StringTokenizer;
    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    .line 223
    const-string v2, "@"

    invoke-virtual {v1, v2}, Ljava/util/StringTokenizer;->nextToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 227
    .end local v1    # "s":Ljava/util/StringTokenizer;
    :goto_0
    return-object v0

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method private static isResFilter(Ljava/lang/String;)Z
    .locals 2
    .param p0, "filter"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 180
    const-string v1, "res"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    const-string v1, "res@"

    invoke-virtual {p0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
