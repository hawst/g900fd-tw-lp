.class public Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "NodeList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/android/allshare/stack/upnp/xml/Node;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "NodeList"

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 26
    return-void
.end method


# virtual methods
.method public getEndsWith(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 76
    if-nez p1, :cond_0

    move-object v2, v4

    .line 88
    :goto_0
    return-object v2

    .line 79
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v1

    .line 80
    .local v1, "nLists":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 81
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 82
    .local v2, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, "nodeName":Ljava/lang/String;
    if-nez v3, :cond_2

    .line 80
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 85
    :cond_2
    invoke-virtual {v3, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    goto :goto_0

    .end local v2    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v3    # "nodeName":Ljava/lang/String;
    :cond_3
    move-object v2, v4

    .line 88
    goto :goto_0
.end method

.method public getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 4
    .param p1, "n"    # I

    .prologue
    .line 36
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    :goto_0
    return-object v1

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "NodeList"

    const-string v2, "getNode"

    const-string v3, "getNode Exception"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 39
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 6
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 44
    if-nez p1, :cond_1

    move-object v2, v4

    .line 54
    :cond_0
    :goto_0
    return-object v2

    .line 47
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v1

    .line 48
    .local v1, "nLists":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 49
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 50
    .local v2, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v3

    .line 51
    .local v3, "nodeName":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {p1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    .line 48
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v2    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v3    # "nodeName":Ljava/lang/String;
    :cond_3
    move-object v2, v4

    .line 54
    goto :goto_0
.end method

.method public getNodeList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/upnp/xml/Node;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 58
    if-nez p1, :cond_1

    move-object v3, v5

    .line 72
    :cond_0
    :goto_0
    return-object v3

    .line 61
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v3, "nodeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/upnp/xml/Node;>;"
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v1

    .line 63
    .local v1, "nLists":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_1
    if-ge v0, v1, :cond_3

    .line 64
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 65
    .local v2, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v4

    .line 66
    .local v4, "nodeName":Ljava/lang/String;
    if-eqz v4, :cond_2

    invoke-virtual {p1, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_2

    .line 67
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 69
    .end local v2    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v4    # "nodeName":Ljava/lang/String;
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-nez v6, :cond_0

    move-object v3, v5

    .line 70
    goto :goto_0
.end method
