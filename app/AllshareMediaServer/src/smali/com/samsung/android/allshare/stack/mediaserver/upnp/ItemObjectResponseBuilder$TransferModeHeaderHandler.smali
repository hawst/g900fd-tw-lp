.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$TransferModeHeaderHandler;
.super Ljava/lang/Object;
.source "ItemObjectResponseBuilder.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TransferModeHeaderHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$TransferModeHeaderHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handle(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z
    .locals 6
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    .param p2, "header"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    .param p3, "object"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p4, "res"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .param p5, "resp"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    .prologue
    const/16 v4, 0x196

    const/16 v5, 0x190

    const/4 v2, 0x0

    .line 157
    if-eqz p2, :cond_0

    if-nez p4, :cond_1

    .line 198
    :cond_0
    :goto_0
    return v2

    .line 160
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 161
    .local v1, "transferMode":Ljava/lang/String;
    invoke-virtual {p4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getMimeType()Ljava/lang/String;

    move-result-object v0

    .line 164
    .local v0, "mimeType":Ljava/lang/String;
    const-string v3, "Streaming"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 167
    const-string v3, "image"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 168
    invoke-virtual {p5, v4}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    goto :goto_0

    .line 171
    :cond_2
    const-string v2, "transferMode.dlna.org"

    const-string v3, "Streaming"

    invoke-virtual {p5, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 172
    :cond_3
    const-string v3, "Interactive"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 175
    const-string v3, "image"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 176
    invoke-virtual {p5, v4}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    goto :goto_0

    .line 182
    :cond_4
    invoke-virtual {p5}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getHeader()Ljava/lang/String;

    move-result-object v3

    const-string v4, "realTimeInfo.dlna.org"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 183
    invoke-virtual {p5, v5}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    goto :goto_0

    .line 186
    :cond_5
    const-string v2, "transferMode.dlna.org"

    const-string v3, "Interactive"

    invoke-virtual {p5, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 187
    :cond_6
    const-string v3, "Background"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 190
    const-string v2, "transferMode.dlna.org"

    const-string v3, "Background"

    invoke-virtual {p5, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 194
    :cond_7
    invoke-virtual {p5, v5}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    goto :goto_0
.end method
