.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "ServiceList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "serviceList"

.field private static final TAG:Ljava/lang/String; = "ServiceList"

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 5
    .param p1, "n"    # I

    .prologue
    .line 43
    const/4 v1, 0x0

    .line 45
    .local v1, "obj":Ljava/lang/Object;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->get(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 49
    .end local v1    # "obj":Ljava/lang/Object;
    :goto_0
    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    return-object v1

    .line 46
    .restart local v1    # "obj":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ServiceList"

    const-string v3, "getService"

    const-string v4, "getService - Exception "

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
