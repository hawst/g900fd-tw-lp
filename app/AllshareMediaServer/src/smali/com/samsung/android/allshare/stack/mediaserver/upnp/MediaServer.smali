.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
.source "MediaServer.java"

# interfaces
.implements Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$1;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;
    }
.end annotation


# static fields
.field public static final DEVICE_TYPE:Ljava/lang/String; = "urn:schemas-upnp-org:device:MediaServer:1"

.field private static final TAG:Ljava/lang/String; = "MediaServer"


# instance fields
.field private mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

.field private mConMan:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

.field protected mConnectionListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;

.field private mDefaultPath:Ljava/io/File;

.field private mIconResource:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;",
            ">;"
        }
    .end annotation
.end field

.field private mImportResourceFactory:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;

.field private mIsStart:Z

.field private mMutex:Ljava/lang/Object;

.field private mObjectResponseBuilder:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;

.field private mServerInfoListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

.field private mUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

.field private mUploadPath:Ljava/io/File;

.field private mUploadRequestStore:Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 131
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;-><init>()V

    .line 108
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mMutex:Ljava/lang/Object;

    .line 110
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    .line 112
    new-instance v0, Ljava/io/File;

    const-string v1, "."

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mDefaultPath:Ljava/io/File;

    .line 114
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadRequestStore:Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;

    .line 116
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    .line 118
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mObjectResponseBuilder:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;

    .line 120
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConnectionListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;

    .line 122
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mServerInfoListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

    .line 239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    .line 687
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIconResource:Ljava/util/HashMap;

    .line 1060
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$DefaultImportResourcePropertyFactory;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$DefaultImportResourcePropertyFactory;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mImportResourceFactory:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;

    .line 132
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mObjectResponseBuilder:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;

    .line 133
    return-void
.end method

.method private contentImportProgressRecieved(Ljava/lang/String;)V
    .locals 8
    .param p1, "firstLine"    # Ljava/lang/String;

    .prologue
    .line 919
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v7, " "

    invoke-direct {v4, p1, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 920
    .local v4, "st":Ljava/util/StringTokenizer;
    const-string v6, ""

    .line 922
    .local v6, "uriString":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    const/4 v7, 0x2

    if-ge v3, v7, :cond_2

    .line 923
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-nez v7, :cond_1

    .line 947
    :cond_0
    :goto_1
    return-void

    .line 925
    :cond_1
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    .line 922
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 928
    :cond_2
    if-eqz v6, :cond_0

    .line 932
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 934
    .local v5, "uri":Landroid/net/Uri;
    if-eqz v5, :cond_0

    .line 938
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getImportDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v0

    .line 940
    .local v0, "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-eqz v0, :cond_0

    .line 943
    invoke-virtual {v5}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 944
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getItemByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    move-object v2, v7

    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .line 945
    .local v2, "importItem":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    if-eqz v2, :cond_0

    .line 946
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->rebirth()V

    goto :goto_1
.end method

.method private getIdFromUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 1400
    if-nez p1, :cond_0

    .line 1401
    const/4 v1, 0x0

    .line 1404
    :goto_0
    return-object v1

    .line 1403
    :cond_0
    const/16 v1, 0x2f

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    .line 1404
    .local v0, "lastPathIndex":I
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private isValidHTTPRequest(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)Z
    .locals 4
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 956
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v1

    .line 957
    .local v1, "header":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "HOST"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 958
    const-string v2, "getCaptionInfo.sec"

    invoke-virtual {p1, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeader(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 959
    .local v0, "capHeader":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-nez v0, :cond_0

    .line 962
    const/4 v2, 0x0

    .line 966
    .end local v0    # "capHeader":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .prologue
    .line 561
    if-nez p1, :cond_1

    .line 566
    .end local p1    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    :cond_0
    :goto_0
    return-void

    .line 564
    .restart local p1    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    :cond_1
    instance-of v0, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;

    if-eqz v0, :cond_0

    .line 565
    check-cast p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;

    .end local p1    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->accept()V
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;)V

    goto :goto_0
.end method

.method public addMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z
    .locals 2
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 374
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 375
    :cond_0
    const/4 v1, 0x0

    .line 378
    :goto_0
    return v1

    .line 377
    :cond_1
    invoke-virtual {p1, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->setOnDirectoryChangedListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;)V

    .line 378
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->addDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    move-result v1

    goto :goto_0
.end method

.method public allowUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .prologue
    .line 851
    instance-of v1, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 852
    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    .line 853
    .local v0, "dRequest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->ALLOWED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->setState(Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;)V

    .line 854
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->notifyEvent()V

    .line 861
    .end local v0    # "dRequest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    :cond_0
    return-void
.end method

.method public findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .locals 2
    .param p1, "objID"    # Ljava/lang/String;

    .prologue
    .line 431
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 432
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    if-eqz v0, :cond_0

    .line 433
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v1

    .line 435
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public findObjectByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .locals 2
    .param p1, "objID"    # Ljava/lang/String;

    .prologue
    .line 417
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 418
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    if-eqz v0, :cond_0

    .line 419
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->findObjectByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v1

    .line 421
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getConnectionManager()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConMan:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    return-object v0
.end method

.method public getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    return-object v0
.end method

.method protected getDMSItem(Ljava/lang/String;)Lcom/samsung/android/allshare/dms/DMSItem;
    .locals 16
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 1222
    if-nez p1, :cond_0

    .line 1223
    const/4 v7, 0x0

    .line 1280
    :goto_0
    return-object v7

    .line 1225
    :cond_0
    const/16 v12, 0x2f

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v12

    add-int/lit8 v5, v12, 0x1

    .line 1226
    .local v5, "lastPathIndex":I
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 1228
    .local v3, "id":Ljava/lang/String;
    const-string v12, "MediaServer"

    const-string v13, "getDMSItem"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "id: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1230
    if-eqz v3, :cond_1

    .line 1235
    new-instance v10, Ljava/util/StringTokenizer;

    const-string v12, "."

    invoke-direct {v10, v3, v12}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1236
    .local v10, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 1237
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    .line 1241
    .end local v10    # "st":Ljava/util/StringTokenizer;
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v12, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->findObjectByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v8

    .line 1242
    .local v8, "object":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    if-eqz v8, :cond_2

    instance-of v12, v8, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    if-nez v12, :cond_3

    .line 1243
    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    :cond_3
    move-object v4, v8

    .line 1246
    check-cast v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .line 1248
    .local v4, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    move-object/from16 v11, p1

    .line 1250
    .local v11, "uriString":Ljava/lang/String;
    const-string v12, ".smi"

    invoke-virtual {v11, v12}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_4

    const-string v12, "subtitle/"

    invoke-virtual {v11, v12}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_4

    .line 1251
    const-string v12, "media/"

    const-string v13, "media/subtitle/"

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    .line 1255
    :cond_4
    invoke-virtual {v4, v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->findResPropertyByPath(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    move-result-object v9

    .line 1256
    .local v9, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    if-nez v9, :cond_5

    .line 1257
    const/4 v7, 0x0

    goto :goto_0

    .line 1260
    :cond_5
    new-instance v7, Lcom/samsung/android/allshare/dms/DMSItem;

    invoke-direct {v7}, Lcom/samsung/android/allshare/dms/DMSItem;-><init>()V

    .line 1261
    .local v7, "mDMSItem":Lcom/samsung/android/allshare/dms/DMSItem;
    invoke-virtual {v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getMimeType()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/samsung/android/allshare/dms/DMSItem;->mime_type:Ljava/lang/String;

    .line 1262
    invoke-virtual {v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getProtocol()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/samsung/android/allshare/dms/DMSItem;->protocol_info:Ljava/lang/String;

    .line 1263
    invoke-virtual {v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getFilePath()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/samsung/android/allshare/dms/DMSItem;->file_path:Ljava/lang/String;

    .line 1266
    instance-of v12, v9, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;

    if-nez v12, :cond_7

    .line 1267
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getResoureList()Ljava/util/ArrayList;

    move-result-object v6

    .line 1268
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 1269
    .local v1, "cp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    instance-of v12, v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;

    if-eqz v12, :cond_6

    .line 1270
    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;

    .end local v1    # "cp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;->getFilePath()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v7, Lcom/samsung/android/allshare/dms/DMSItem;->subtitle_path:Ljava/lang/String;

    goto :goto_1

    .line 1275
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;>;"
    :cond_7
    const-string v12, "MediaServer"

    const-string v13, "getDMSItem"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "mime: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v7, Lcom/samsung/android/allshare/dms/DMSItem;->mime_type:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1276
    const-string v12, "MediaServer"

    const-string v13, "getDMSItem"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "protocol_info: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v7, Lcom/samsung/android/allshare/dms/DMSItem;->protocol_info:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1277
    const-string v12, "MediaServer"

    const-string v13, "getDMSItem"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "file path: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v7, Lcom/samsung/android/allshare/dms/DMSItem;->file_path:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1278
    const-string v12, "MediaServer"

    const-string v13, "getDMSItem"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "subtitle path: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v7, Lcom/samsung/android/allshare/dms/DMSItem;->subtitle_path:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v13, v14}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public getImportDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .locals 2

    .prologue
    .line 403
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 404
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    if-eqz v0, :cond_0

    .line 405
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getImportDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v1

    .line 407
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getStreamingPort(Ljava/lang/String;)I
    .locals 6
    .param p1, "ipaddress"    # Ljava/lang/String;

    .prologue
    .line 1201
    new-instance v0, Lcom/samsung/android/allshare/dms/PortID;

    invoke-direct {v0}, Lcom/samsung/android/allshare/dms/PortID;-><init>()V

    .line 1202
    .local v0, "port_id":Lcom/samsung/android/allshare/dms/PortID;
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v2

    invoke-virtual {v2, p1, v0}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getStreamingPort(Ljava/lang/String;Lcom/samsung/android/allshare/dms/PortID;)I

    move-result v1

    .line 1203
    .local v1, "result_code":I
    if-nez v1, :cond_0

    .line 1204
    const-string v2, "MediaServer"

    const-string v3, "getStreamingPort"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getStreamingPort- ip: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", port: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/samsung/android/allshare/dms/PortID;->port_number:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    iget v2, v0, Lcom/samsung/android/allshare/dms/PortID;->port_number:I

    .line 1209
    :goto_0
    return v2

    .line 1208
    :cond_0
    const-string v2, "MediaServer"

    const-string v3, "getStreamingPort"

    const-string v4, "getStreamingPort failed"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1209
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSystemUpdateID()I
    .locals 2

    .prologue
    .line 793
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 794
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    if-eqz v0, :cond_0

    .line 795
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getSystemUpdateID()I

    move-result v1

    .line 797
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUploadPath()Ljava/io/File;
    .locals 1

    .prologue
    .line 782
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    return-object v0
.end method

.method protected handleSoapAction(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V
    .locals 6
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    .param p2, "serviceType"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    .prologue
    .line 1171
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isPostRequest()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isSOAPAction()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1172
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    invoke-direct {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 1173
    .local v2, "req":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getActionName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getAction(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    move-result-object v0

    .line 1174
    .local v0, "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    if-eqz v0, :cond_1

    .line 1175
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 1176
    .local v1, "originalArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v3

    .line 1177
    .local v3, "reqArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->setArgumentList(Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;)V

    .line 1178
    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->performActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;)Z

    .line 1185
    .end local v0    # "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .end local v1    # "originalArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    .end local v2    # "req":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    .end local v3    # "reqArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    :cond_0
    :goto_0
    return-void

    .line 1181
    .restart local v0    # "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .restart local v2    # "req":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    :cond_1
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getActionName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    .local v4, "res":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;
    invoke-virtual {v2, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method public httpRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 17
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 579
    if-nez p1, :cond_1

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 582
    :cond_1
    const/4 v10, 0x0

    .line 583
    .local v10, "uri":Landroid/net/Uri;
    const-string v11, ""

    .line 586
    .local v11, "uriStr":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v11

    .line 587
    invoke-static {v11}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 593
    const-string v13, "MediaServer"

    const-string v14, "httpRequestRecieved"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "httpRequestRecieved: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    invoke-static {v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->match(Landroid/net/Uri;)I

    move-result v7

    .line 598
    .local v7, "match":I
    sparse-switch v7, :sswitch_data_0

    .line 626
    const/4 v5, 0x1

    .line 628
    .local v5, "isAllow":Z
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isGetRequest()Z

    move-result v13

    if-nez v13, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isHeadRequest()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 629
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConnectionListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;

    if-eqz v13, :cond_3

    const-string v13, "description.xml"

    invoke-virtual {v11, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 630
    const-string v13, "User-Agent"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 642
    .local v12, "userAgent":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getRequestAddress()Ljava/net/InetAddress;

    move-result-object v2

    .line 643
    .local v2, "addr":Ljava/net/InetAddress;
    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v4

    .line 645
    .local v4, "host":Ljava/lang/String;
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getTargetHostPort()I

    move-result v8

    .line 647
    .local v8, "port":I
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getMacAddrFromArpTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 648
    .local v6, "macAddress":Ljava/lang/String;
    if-eqz v6, :cond_7

    .line 650
    :goto_2
    new-instance v9, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;

    invoke-direct {v9, v6, v12, v4, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 652
    .local v9, "req":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConnectionListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;

    invoke-interface {v13, v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;->onConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 653
    const-wide/16 v14, 0x7530

    invoke-virtual {v9, v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->waitEvent(J)V

    .line 655
    iget-boolean v5, v9, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mAllowed:Z

    .line 659
    .end local v2    # "addr":Ljava/net/InetAddress;
    .end local v4    # "host":Ljava/lang/String;
    .end local v6    # "macAddress":Ljava/lang/String;
    .end local v8    # "port":I
    .end local v9    # "req":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;
    .end local v12    # "userAgent":Ljava/lang/String;
    :cond_3
    if-eqz v5, :cond_8

    .line 660
    invoke-super/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->httpRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    goto :goto_0

    .line 588
    .end local v5    # "isAllow":Z
    .end local v7    # "match":I
    :catch_0
    move-exception v3

    .line 590
    .local v3, "e":Ljava/lang/Exception;
    goto/16 :goto_0

    .line 601
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v7    # "match":I
    :sswitch_0
    const-string v13, "urn:schemas-upnp-org:service:ContentDirectory"

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->handleSoapAction(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    goto/16 :goto_0

    .line 606
    :sswitch_1
    const-string v13, "urn:schemas-upnp-org:service:ConnectionManager:1"

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getConnectionManager()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->handleSoapAction(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    goto/16 :goto_0

    .line 612
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isGetRequest()Z

    move-result v13

    if-nez v13, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isHeadRequest()Z

    move-result v13

    if-eqz v13, :cond_5

    .line 613
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->sendContent(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 614
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isPostRequest()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 615
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->receiveContent(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 620
    :sswitch_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->sendIcon(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 643
    .restart local v2    # "addr":Ljava/net/InetAddress;
    .restart local v5    # "isAllow":Z
    .restart local v12    # "userAgent":Ljava/lang/String;
    :cond_6
    const-string v4, ""

    goto :goto_1

    .line 648
    .restart local v4    # "host":Ljava/lang/String;
    .restart local v6    # "macAddress":Ljava/lang/String;
    .restart local v8    # "port":I
    :cond_7
    const-string v6, ""

    goto :goto_2

    .line 662
    .end local v2    # "addr":Ljava/net/InetAddress;
    .end local v4    # "host":Ljava/lang/String;
    .end local v6    # "macAddress":Ljava/lang/String;
    .end local v8    # "port":I
    .end local v12    # "userAgent":Ljava/lang/String;
    :cond_8
    const/16 v13, 0x193

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnResponse(I)Z

    goto/16 :goto_0

    .line 598
    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_3
        0xc9 -> :sswitch_2
        0x12d -> :sswitch_0
        0x12e -> :sswitch_1
    .end sparse-switch
.end method

.method public httptRecieved(Ljava/lang/String;)V
    .locals 2
    .param p1, "firstLine"    # Ljava/lang/String;

    .prologue
    .line 674
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 675
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 680
    :cond_0
    :goto_0
    return-void

    .line 677
    :cond_1
    const-string v1, "media/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "POST"

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 678
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->contentImportProgressRecieved(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initSSDPSocket()V
    .locals 0

    .prologue
    .line 1196
    invoke-super {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->initSSDPSocket()V

    .line 1197
    return-void
.end method

.method public initailize(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "desc"    # Ljava/io/InputStream;
    .param p2, "cds"    # Ljava/io/InputStream;
    .param p3, "cms"    # Ljava/io/InputStream;
    .param p4, "friendlyName"    # Ljava/lang/String;
    .param p5, "udn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;
        }
    .end annotation

    .prologue
    .line 149
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->loadDescription(Ljava/io/InputStream;)Z

    .line 151
    if-eqz p4, :cond_0

    .line 152
    const/4 v3, 0x0

    invoke-virtual {p0, p4, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->setFriendlyName(Ljava/lang/String;Z)Z

    .line 154
    :cond_0
    if-eqz p5, :cond_1

    .line 155
    invoke-virtual {p0, p5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->setUDN(Ljava/lang/String;)V

    .line 157
    :cond_1
    const-string v3, "urn:schemas-upnp-org:service:ContentDirectory"

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getService(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v1

    .line 158
    .local v1, "servConDir":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    const-string v3, "urn:schemas-upnp-org:service:ConnectionManager:1"

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getService(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v2

    .line 161
    .local v2, "servConMan":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-eqz v1, :cond_2

    .line 162
    :try_start_0
    invoke-virtual {v1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->loadSCPD(Ljava/io/InputStream;)Z

    .line 163
    new-instance v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-direct {v3, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .line 164
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v3, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->setImportItemDestroyListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ImportItemDirectory$IItemDestroyListener;)V

    .line 166
    :cond_2
    if-eqz v2, :cond_3

    .line 167
    invoke-virtual {v2, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->loadSCPD(Ljava/io/InputStream;)Z

    .line 168
    new-instance v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    invoke-direct {v3, p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConMan:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
    :try_end_0
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :cond_3
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConMan:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    invoke-static {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->setConnectionManager(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;)V

    .line 177
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->setDiscoveryInfo()V

    .line 180
    return-void

    .line 171
    :catch_0
    move-exception v0

    .line 172
    .local v0, "e":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    const-string v3, "MediaServer"

    const-string v4, "initailize"

    const-string v5, "Fail to parse xml - ParserException"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public isServerStarted()Z
    .locals 2

    .prologue
    .line 357
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 358
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    monitor-exit v1

    return v0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onChange()V
    .locals 0

    .prologue
    .line 811
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->updateSystemUpdateID()V

    .line 812
    return-void
.end method

.method public onDestroy(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V
    .locals 4
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .prologue
    .line 1189
    const-string v1, "MediaServer"

    const-string v2, "onDestroy"

    const-string v3, "MediaServer::onDestroy()"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1191
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadRequestStore:Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;->remove(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    move-result-object v0

    .line 1192
    .local v0, "req":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 1193
    return-void
.end method

.method public onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .prologue
    .line 893
    const-string v0, "MediaServer"

    const-string v1, "onFailed"

    const-string v2, "MediaServer::onFailed"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 895
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 896
    :cond_0
    return-void
.end method

.method public onFileReceiveCompleted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "filepath"    # Ljava/lang/String;

    .prologue
    .line 1311
    const-string v7, "MediaServer"

    const-string v8, "onFileReceiveCompleted"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onFileDownloadFinished- uri: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", filepath: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1315
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1358
    :cond_0
    :goto_0
    return-void

    .line 1318
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getIdFromUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1321
    .local v2, "id":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getImportDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getItemByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .line 1323
    .local v3, "importItem":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    if-eqz v3, :cond_0

    .line 1326
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadRequestStore:Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;

    invoke-virtual {v7, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;->remove(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    move-result-object v5

    .line 1327
    .local v5, "req":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    if-eqz v5, :cond_0

    .line 1330
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1331
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1332
    const-string v7, "MediaServer"

    const-string v8, "onFileReceiveCompleted"

    const-string v9, "file doesn\'t exit"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1333
    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    goto :goto_0

    .line 1337
    :cond_2
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getContentURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1339
    .local v0, "contentUri":Ljava/lang/String;
    const-string v7, "res"

    invoke-virtual {v3, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v4

    .line 1340
    .local v4, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    const/4 v6, 0x0

    .line 1341
    .local v6, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mImportResourceFactory:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;

    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-interface {v7, v0, v3, v4, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;->create(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;

    move-result-object v6

    .line 1343
    if-eqz v6, :cond_3

    .line 1344
    invoke-virtual {v6, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->setUri(Ljava/lang/String;)V

    .line 1347
    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->removeProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    .line 1348
    invoke-virtual {v3, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V

    .line 1349
    const/4 v7, 0x0

    invoke-virtual {v3, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->setRemovable(Z)V

    .line 1351
    const-string v7, "MediaServer"

    const-string v8, "onFileReceiveCompleted"

    const-string v9, "finished"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1353
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->getMediaFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {p0, v5, v3, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFinished(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/io/File;)V

    goto :goto_0

    .line 1355
    :cond_3
    const-string v7, "MediaServer"

    const-string v8, "onFileReceiveCompleted"

    const-string v9, "failed"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1356
    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    goto :goto_0
.end method

.method public onFileReceiveFailed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "filepath"    # Ljava/lang/String;

    .prologue
    .line 1362
    const-string v3, "MediaServer"

    const-string v4, "onFileReceiveFailed"

    const-string v5, "onFileReceiveFailed"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1363
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1379
    :cond_0
    :goto_0
    return-void

    .line 1366
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getIdFromUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1369
    .local v0, "id":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getImportDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getItemByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .line 1371
    .local v1, "importItem":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    if-eqz v1, :cond_0

    .line 1374
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadRequestStore:Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;

    invoke-virtual {v3, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;->remove(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    move-result-object v2

    .line 1375
    .local v2, "req":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    if-eqz v2, :cond_0

    .line 1378
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    goto :goto_0
.end method

.method public onFileReceiveProgressUpdated(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "filepath"    # Ljava/lang/String;
    .param p3, "received_size"    # J
    .param p5, "total_size"    # J

    .prologue
    .line 1384
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1397
    :cond_0
    :goto_0
    return-void

    .line 1387
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getIdFromUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1389
    .local v0, "id":Ljava/lang/String;
    const-string v2, "MediaServer"

    const-string v3, "onFileReceiveProgressUpdated"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onFileReceiveProgressUpdated- id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", received: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", total: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1393
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getImportDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getItemByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .line 1395
    .local v1, "importItem":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    if-eqz v1, :cond_0

    .line 1396
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->rebirth()V

    goto :goto_0
.end method

.method public onFinished(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/io/File;)V
    .locals 4
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    .param p2, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p3, "file"    # Ljava/io/File;

    .prologue
    .line 880
    if-eqz p1, :cond_0

    .line 881
    const-string v0, "MediaServer"

    const-string v1, "onFinished"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFinished-request: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 882
    :cond_0
    if-eqz p2, :cond_1

    .line 883
    const-string v0, "MediaServer"

    const-string v1, "onFinished"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFinished-item: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    :cond_1
    if-eqz p3, :cond_2

    .line 885
    const-string v0, "MediaServer"

    const-string v1, "onFinished"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFinished-file: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    .line 888
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;->onFinished(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/io/File;)V

    .line 889
    :cond_3
    return-void
.end method

.method public onRequestReceived(Ljava/lang/String;)Lcom/samsung/android/allshare/dms/DMSItem;
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 1216
    const-string v0, "MediaServer"

    const-string v1, "onRequestReceived"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onRequestReceived: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getDMSItem(Ljava/lang/String;)Lcom/samsung/android/allshare/dms/DMSItem;

    move-result-object v0

    return-object v0
.end method

.method public onRequested(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V
    .locals 3
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .prologue
    .line 904
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    if-eqz v1, :cond_0

    .line 905
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    invoke-interface {v1, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;->onRequested(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 907
    :cond_0
    if-eqz p1, :cond_1

    instance-of v1, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    if-eqz v1, :cond_1

    move-object v0, p1

    .line 908
    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    .line 909
    .local v0, "dr":Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadRequestStore:Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->getImportItemObject()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;->put(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .line 911
    .end local v0    # "dr":Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    :cond_1
    return-void
.end method

.method protected receiveContent(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Landroid/net/Uri;)V
    .locals 17
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1074
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 1158
    :cond_0
    :goto_0
    return-void

    .line 1076
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v12

    .line 1078
    .local v12, "id":Ljava/lang/String;
    const-string v2, "MediaServer"

    const-string v3, "receiveContent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "receiveContent: id: "

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v3, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    const-string v2, "MediaServer"

    const-string v3, "receiveContent"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "receiveContent: httpReq: "

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v3, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1082
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getImportDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v2

    invoke-virtual {v2, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getItemByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    .line 1084
    .local v5, "importItem":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadRequestStore:Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;->remove(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    move-result-object v13

    .line 1087
    .local v13, "req":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->isValidHTTPRequest(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1088
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    .line 1089
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    goto :goto_0

    .line 1094
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getContentLength()J

    move-result-wide v8

    .line 1096
    .local v8, "conLen":J
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->hasEnoughStorage(J)Z

    move-result v11

    .line 1097
    .local v11, "hasEnoughStorage":Z
    if-nez v11, :cond_3

    .line 1098
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    goto :goto_0

    .line 1102
    :cond_3
    const-wide/16 v2, 0x0

    cmp-long v2, v8, v2

    if-lez v2, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getCurrentReceviedContentLength()J

    move-result-wide v2

    cmp-long v2, v8, v2

    if-eqz v2, :cond_4

    .line 1104
    const/4 v5, 0x0

    .line 1107
    :cond_4
    if-nez v5, :cond_6

    .line 1108
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->hasFileContent()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1109
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getFileContent()Ljava/io/File;

    move-result-object v15

    .line 1110
    .local v15, "tempFile":Ljava/io/File;
    if-eqz v15, :cond_5

    .line 1112
    :try_start_0
    invoke-virtual {v15}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1120
    .end local v15    # "tempFile":Ljava/io/File;
    :cond_5
    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    .line 1121
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    goto/16 :goto_0

    .line 1113
    .restart local v15    # "tempFile":Ljava/io/File;
    :catch_0
    move-exception v10

    .line 1114
    .local v10, "e":Ljava/lang/Exception;
    const-string v2, "MediaServer"

    const-string v3, "receiveContent"

    const-string v7, "Fail to delete tempFile"

    invoke-static {v2, v3, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1126
    .end local v10    # "e":Ljava/lang/Exception;
    .end local v15    # "tempFile":Ljava/io/File;
    :cond_6
    const-string v2, "res"

    invoke-virtual {v5, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v6

    .line 1127
    .local v6, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    const/4 v14, 0x0

    .line 1129
    .local v14, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    invoke-static {v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getContentURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1131
    .local v4, "contentUri":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_8

    .line 1133
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 1136
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->hasFileContent()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mImportResourceFactory:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getFileContent()Ljava/io/File;

    move-result-object v7

    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;->create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;

    move-result-object v14

    .line 1143
    :goto_2
    if-eqz v14, :cond_a

    .line 1144
    invoke-virtual {v14, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->setUri(Ljava/lang/String;)V

    .line 1147
    invoke-virtual {v5, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->removeProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    .line 1148
    invoke-virtual {v5, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V

    .line 1149
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->setRemovable(Z)V

    .line 1152
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnOK()Z

    .line 1153
    invoke-virtual {v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->getMediaFile()Ljava/io/File;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v5, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFinished(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/io/File;)V

    goto/16 :goto_0

    .line 1140
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mImportResourceFactory:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getContent()Ljava/io/InputStream;

    move-result-object v7

    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;->create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;

    move-result-object v14

    goto :goto_2

    .line 1155
    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    .line 1156
    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->onFailed(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V

    goto/16 :goto_0
.end method

.method public rejectConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V
    .locals 1
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .prologue
    .line 546
    if-nez p1, :cond_1

    .line 551
    .end local p1    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    :cond_0
    :goto_0
    return-void

    .line 549
    .restart local p1    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    :cond_1
    instance-of v0, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;

    if-eqz v0, :cond_0

    .line 550
    check-cast p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;

    .end local p1    # "request":Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->reject()V
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;)V

    goto :goto_0
.end method

.method public rejectUploadRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)V
    .locals 2
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .prologue
    .line 867
    instance-of v1, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 868
    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    .line 869
    .local v0, "dRequest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->REJECTED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->setState(Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;)V

    .line 870
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->notifyEvent()V

    .line 872
    .end local v0    # "dRequest":Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    :cond_0
    return-void
.end method

.method public removeMediaDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z
    .locals 2
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .prologue
    .line 388
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 389
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 390
    :cond_0
    const/4 v1, 0x0

    .line 391
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->removeDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Z

    move-result v1

    goto :goto_0
.end method

.method protected sendContent(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Landroid/net/Uri;)V
    .locals 11
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1002
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->isValidHTTPRequest(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 1003
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    .line 1058
    :cond_0
    :goto_0
    return-void

    .line 1008
    :cond_1
    if-eqz p2, :cond_0

    .line 1010
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    .line 1011
    .local v1, "id":Ljava/lang/String;
    const-string v7, "MediaServer"

    const-string v8, "sendContent"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1013
    if-eqz v1, :cond_2

    .line 1018
    new-instance v5, Ljava/util/StringTokenizer;

    const-string v7, "."

    invoke-direct {v5, v1, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    .local v5, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1020
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    .line 1024
    .end local v5    # "st":Ljava/util/StringTokenizer;
    :cond_2
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConDir:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v7, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->findObjectByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v3

    .line 1025
    .local v3, "object":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    if-eqz v3, :cond_3

    instance-of v7, v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    if-nez v7, :cond_4

    .line 1026
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnNotFoundRequest()Z

    goto :goto_0

    :cond_4
    move-object v2, v3

    .line 1030
    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .line 1032
    .local v2, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1034
    .local v6, "uriString":Ljava/lang/String;
    const-string v7, ".smi"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "subtitle/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 1035
    const-string v7, "media/"

    const-string v8, "media/subtitle/"

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    .line 1039
    :cond_5
    invoke-virtual {v2, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->findResPropertyByPath(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    move-result-object v4

    .line 1040
    .local v4, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    if-nez v4, :cond_6

    .line 1041
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnNotFoundRequest()Z

    goto :goto_0

    .line 1044
    :cond_6
    const-string v7, "MediaServer"

    const-string v8, "sendContent"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "sendContent()- uri: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mObjectResponseBuilder:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;

    invoke-virtual {v7, p1, v2, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;->buildResponse(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v0

    .line 1051
    .local v0, "httpRes":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    goto/16 :goto_0
.end method

.method protected sendIcon(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Landroid/net/Uri;)V
    .locals 5
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 976
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIconResource:Ljava/util/HashMap;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;

    .line 977
    .local v1, "icon":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;
    if-nez v1, :cond_0

    .line 991
    :goto_0
    return-void

    .line 980
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->buildHTTPResponse(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v0

    .line 981
    .local v0, "httpRes":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    if-nez v0, :cond_1

    .line 982
    const-string v2, "MediaServer"

    const-string v3, "sendIcon"

    const-string v4, "HTTPResponse is null!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 986
    :cond_1
    const/16 v2, 0xc8

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 987
    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mMimeType:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->access$300(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 988
    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mByteArray:[B
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->access$400(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;)[B

    move-result-object v2

    array-length v2, v2

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentLength(J)V

    .line 989
    new-instance v2, Ljava/io/ByteArrayInputStream;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mByteArray:[B
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->access$400(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;)[B

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentInputStream(Ljava/io/InputStream;)V

    .line 990
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method public setDiscoveryInfo()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 184
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;-><init>()V

    .line 185
    .local v0, "builder":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;
    const-string v1, "*"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->setNI(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;

    .line 186
    const-string v1, "239.255.255.250"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->addMulticastAddress(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;

    .line 188
    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->setSupportLinkLocal(Z)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;

    .line 189
    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->setSupportLoopback(Z)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;

    .line 190
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo$Builder;->build()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->setDiscoveryInfos([Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    .line 193
    return-void
.end method

.method public setDownloadUri(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Ljava/lang/String;)V
    .locals 8
    .param p1, "importItem"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .param p2, "contentUri"    # Ljava/lang/String;

    .prologue
    .line 1284
    const-string v4, "MediaServer"

    const-string v5, "setDownloadUri"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setDownloadUri()-uri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    const-string v4, "res"

    invoke-virtual {p1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v1

    .line 1288
    .local v1, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1290
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->mkdir()Z

    .line 1293
    :cond_1
    const-string v4, "MediaServer"

    const-string v5, "setDownloadUri"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setDownloadUri()-mediauri: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1294
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mImportResourceFactory:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;

    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-interface {v4, v5, p1, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;->createFileName(Ljava/io/File;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Ljava/lang/String;

    move-result-object v0

    .line 1295
    .local v0, "filename":Ljava/lang/String;
    const-string v4, "MediaServer"

    const-string v5, "setDownloadUri"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setDownloadUri()-filepath: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1297
    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1298
    .local v2, "resSize":Ljava/lang/Long;
    if-eqz v1, :cond_2

    .line 1299
    const-string v4, "size"

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1300
    .local v3, "strSize":Ljava/lang/String;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_2

    .line 1301
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 1303
    .end local v3    # "strSize":Ljava/lang/String;
    :cond_2
    const-string v4, "MediaServer"

    const-string v5, "setDownloadUri"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setDownloadUri()-size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1305
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, p2, v5, v6, v7}, Lcom/samsung/android/allshare/dms/DmsStreaming;->setDownloadUri(Ljava/lang/String;Ljava/lang/String;J)I

    .line 1307
    return-void
.end method

.method public setFriendlyName(Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "needAnnounce"    # Z

    .prologue
    const/4 v0, 0x1

    .line 228
    invoke-super {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setFriendlyName(Ljava/lang/String;Z)Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 229
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->setDeviceName(Ljava/lang/String;)Z

    .line 232
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIcon([BLcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V
    .locals 5
    .param p1, "bitmap"    # [B
    .param p2, "type"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    .prologue
    .line 717
    const/4 v2, 0x0

    .line 719
    .local v2, "keyUri":Landroid/net/Uri;
    :try_start_0
    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$1;)V

    .line 720
    .local v1, "icon":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v3, 0x4e20

    invoke-direct {v0, v3}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 721
    .local v0, "bos":Ljava/io/ByteArrayOutputStream;
    if-nez p2, :cond_0

    .line 749
    .end local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "icon":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;
    :goto_0
    return-void

    .line 723
    .restart local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "icon":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;
    :cond_0
    sget-object v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$1;->$SwitchMap$com$samsung$android$allshare$stack$mediaserver$upnp$MediaServer$ICON_TYPE:[I

    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 741
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    goto :goto_0

    .line 747
    .end local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v1    # "icon":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;
    :catch_0
    move-exception v3

    goto :goto_0

    .line 725
    .restart local v0    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v1    # "icon":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;
    :pswitch_0
    const-string v3, "icon//micro.jpg"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 726
    const-string v3, "image/jpeg"

    # setter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mMimeType:Ljava/lang/String;
    invoke-static {v1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->access$302(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;Ljava/lang/String;)Ljava/lang/String;

    .line 745
    :goto_1
    # setter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mByteArray:[B
    invoke-static {v1, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->access$402(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;[B)[B

    .line 746
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIconResource:Ljava/util/HashMap;

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 729
    :pswitch_1
    const-string v3, "icon//small.jpg"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 730
    const-string v3, "image/jpeg"

    # setter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mMimeType:Ljava/lang/String;
    invoke-static {v1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->access$302(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 733
    :pswitch_2
    const-string v3, "icon//micro.png"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 734
    const-string v3, "image/png"

    # setter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mMimeType:Ljava/lang/String;
    invoke-static {v1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->access$302(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 737
    :pswitch_3
    const-string v3, "icon//small.png"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 738
    const-string v3, "image/png"

    # setter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->mMimeType:Ljava/lang/String;
    invoke-static {v1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;->access$302(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$IconResource;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 723
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setImportResourceFactory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;)V
    .locals 0
    .param p1, "resourceFactory"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;

    .prologue
    .line 1063
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mImportResourceFactory:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;

    .line 1064
    return-void
.end method

.method public setOnConnectionListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;

    .prologue
    .line 824
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mConnectionListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;

    .line 825
    return-void
.end method

.method public setOnServerStartedListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

    .prologue
    .line 833
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 834
    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mServerInfoListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

    .line 835
    monitor-exit v1

    .line 836
    return-void

    .line 835
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setOnUploadItemListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    .prologue
    .line 844
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadItemListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener;

    .line 845
    return-void
.end method

.method public final setUploadPath(Ljava/io/File;)Z
    .locals 2
    .param p1, "path"    # Ljava/io/File;

    .prologue
    const/4 v0, 0x0

    .line 762
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_1

    .line 763
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mDefaultPath:Ljava/io/File;

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    .line 773
    :goto_0
    return v0

    .line 767
    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_3

    .line 768
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mDefaultPath:Ljava/io/File;

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    goto :goto_0

    .line 772
    :cond_3
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mUploadPath:Ljava/io/File;

    .line 773
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public start()Z
    .locals 9

    .prologue
    const/4 v3, 0x1

    .line 249
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 250
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 251
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getConnectionManager()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    move-result-object v1

    .line 252
    .local v1, "cms":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 253
    :cond_0
    const/4 v3, 0x0

    monitor-exit v4

    .line 277
    :goto_0
    return v3

    .line 257
    :cond_1
    iget-boolean v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    if-nez v5, :cond_2

    .line 258
    const-string v5, "MediaServer"

    const-string v6, "start"

    const-string v7, "starting cds & cms"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 259
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->start()V

    .line 260
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->start()V

    .line 261
    invoke-super {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->start()Z

    .line 264
    const-string v5, "MediaServer"

    const-string v6, "start"

    const-string v7, "starting DmsStreaming"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/allshare/dms/DmsStreaming;->initialize()I

    .line 266
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/allshare/dms/DmsStreaming;->startServer()I

    move-result v2

    .line 267
    .local v2, "result":I
    const-string v5, "MediaServer"

    const-string v6, "start"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "result: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v5

    invoke-virtual {v5, p0}, Lcom/samsung/android/allshare/dms/DmsStreaming;->setRequestReceivedListener(Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;)V

    .line 270
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    .line 274
    .end local v2    # "result":I
    :cond_2
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mServerInfoListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

    if-eqz v5, :cond_3

    .line 275
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mServerInfoListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;->onServerStarted(Z)V

    .line 277
    :cond_3
    monitor-exit v4

    goto :goto_0

    .line 278
    .end local v0    # "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .end local v1    # "cms":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public stop(Z)Z
    .locals 7
    .param p1, "doByeBye"    # Z

    .prologue
    .line 288
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 289
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 290
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    const-string v3, "MediaServer"

    const-string v5, "stop"

    const-string v6, "stopping cds & cms"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->stop()V

    .line 295
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getConnectionManager()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    move-result-object v1

    .line 296
    .local v1, "cms":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
    if-eqz v1, :cond_1

    .line 297
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->stop()V

    .line 299
    :cond_1
    const-string v3, "MediaServer"

    const-string v5, "stop"

    const-string v6, "stopping DmsStreaming"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/dms/DmsStreaming;->stopServer()I

    .line 301
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/dms/DmsStreaming;->terminate()I

    .line 303
    invoke-super {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->stop(Z)Z

    move-result v2

    .line 306
    .local v2, "result":Z
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mServerInfoListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    const/4 v5, 0x1

    if-ne v3, v5, :cond_2

    .line 307
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    .line 308
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mServerInfoListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;->onServerStarted(Z)V

    .line 311
    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    .line 313
    monitor-exit v4

    return v2

    .line 314
    .end local v0    # "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .end local v1    # "cms":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
    .end local v2    # "result":Z
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public stop(ZZ)Z
    .locals 7
    .param p1, "doByeBye"    # Z
    .param p2, "removeCache"    # Z

    .prologue
    .line 319
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mMutex:Ljava/lang/Object;

    monitor-enter v4

    .line 320
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 321
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    const-string v3, "MediaServer"

    const-string v5, "stop"

    const-string v6, "stopping cds & cms"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    if-eqz v0, :cond_0

    .line 324
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->stop()V

    .line 326
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getConnectionManager()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    move-result-object v1

    .line 327
    .local v1, "cms":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
    if-eqz v1, :cond_1

    .line 328
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->stop()V

    .line 330
    :cond_1
    if-eqz p2, :cond_3

    .line 331
    const-string v3, "MediaServer"

    const-string v5, "stop"

    const-string v6, "stopping DmsStreaming"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/dms/DmsStreaming;->stopServer()I

    .line 333
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/allshare/dms/DmsStreaming;->terminate()I

    .line 337
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->stop(Z)Z

    move-result v2

    .line 340
    .local v2, "result":Z
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mServerInfoListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

    if-eqz v3, :cond_2

    iget-boolean v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    const/4 v5, 0x1

    if-ne v3, v5, :cond_2

    .line 341
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    .line 342
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mServerInfoListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;

    const/4 v5, 0x0

    invoke-interface {v3, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IServerInfoListener;->onServerStarted(Z)V

    .line 345
    :cond_2
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->mIsStart:Z

    .line 347
    monitor-exit v4

    return v2

    .line 335
    .end local v2    # "result":Z
    :cond_3
    const-string v3, "MediaServer"

    const-string v5, "stop"

    const-string v6, "stop before starting"

    invoke-static {v3, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 348
    .end local v0    # "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .end local v1    # "cms":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public updateSoruceProtocolInfo()V
    .locals 1

    .prologue
    .line 818
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getConnectionManager()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    move-result-object v0

    .line 819
    .local v0, "cms":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
    if-eqz v0, :cond_0

    .line 820
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->updateSoruceProtocolInfo()V

    .line 821
    :cond_0
    return-void
.end method

.method public updateSystemUpdateID()V
    .locals 1

    .prologue
    .line 804
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    .line 805
    .local v0, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    if-eqz v0, :cond_0

    .line 806
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->updateSystemUpdateID()V

    .line 807
    :cond_0
    return-void
.end method
