.class public Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;
.super Ljava/util/concurrent/CopyOnWriteArrayList;
.source "HTTPServerList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/concurrent/CopyOnWriteArrayList",
        "<",
        "Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 52
    return-void
.end method


# virtual methods
.method public addReceivedListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->size()I

    move-result v1

    .line 68
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 69
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    move-result-object v2

    .line 70
    .local v2, "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    invoke-virtual {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->addReceivedListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;)V

    .line 68
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    .end local v2    # "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    :cond_0
    return-void
.end method

.method public addRequestListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->size()I

    move-result v1

    .line 60
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 61
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    move-result-object v2

    .line 62
    .local v2, "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    invoke-virtual {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->addRequestListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;)V

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    .end local v2    # "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    :cond_0
    return-void
.end method

.method public close()Z
    .locals 5

    .prologue
    .line 83
    const/4 v0, 0x1

    .line 84
    .local v0, "isClosed":Z
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->size()I

    move-result v2

    .line 85
    .local v2, "nServers":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 86
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    move-result-object v3

    .line 87
    .local v3, "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->close()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    .line 85
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 89
    .end local v3    # "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    :cond_1
    return v0
.end method

.method public getHTTPServer(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    .locals 1
    .param p1, "n"    # I

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    return-object v0
.end method

.method public open(I)Z
    .locals 2
    .param p1, "port"    # I

    .prologue
    .line 95
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;-><init>()V

    .line 96
    .local v0, "httpServer":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->open(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->close()Z

    .line 98
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->clear()V

    .line 99
    const/4 v1, 0x0

    .line 103
    :goto_0
    return v1

    .line 101
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->add(Ljava/lang/Object;)Z

    .line 103
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->size()I

    move-result v1

    .line 112
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 113
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    move-result-object v2

    .line 114
    .local v2, "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->start()Z

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    .end local v2    # "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->size()I

    move-result v1

    .line 120
    .local v1, "nServers":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 121
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->getHTTPServer(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    move-result-object v2

    .line 122
    .local v2, "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->stop()Z

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    .end local v2    # "server":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    :cond_0
    return-void
.end method
