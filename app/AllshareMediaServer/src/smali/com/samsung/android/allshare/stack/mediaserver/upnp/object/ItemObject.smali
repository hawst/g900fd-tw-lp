.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
.source "ItemObject.java"


# static fields
.field public static final COLOR_DEPTH:Ljava/lang/String; = "colorDepth"

.field public static final CONTENTFEATURES:Ljava/lang/String; = "contentFeatures"

.field private static final DATE_FORMAT:Ljava/lang/String; = "yyyy-MM-dd"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final IMPORT_URI:Ljava/lang/String; = "importUri"

.field public static final NAME:Ljava/lang/String; = "item"

.field public static final RES:Ljava/lang/String; = "res"

.field public static final RESOLUTION:Ljava/lang/String; = "resolution"

.field public static final SIZE:Ljava/lang/String; = "size"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "upnpClass"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;-><init>()V

    .line 50
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setID(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setTitle(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setUPnPClass(Ljava/lang/String;)V

    .line 53
    const-string v0, "item"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setName(Ljava/lang/String;)V

    .line 54
    return-void
.end method


# virtual methods
.method public addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V
    .locals 1
    .param p1, "resProp"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    .prologue
    .line 139
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ValueValidator;->isValidContentProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->addProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    .line 141
    :cond_0
    return-void
.end method

.method public findResPropertyByPath(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 144
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    move-object v3, v6

    .line 163
    :cond_1
    :goto_0
    return-object v3

    .line 147
    :cond_2
    invoke-static {p1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "decodedPath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getResoureList()Ljava/util/ArrayList;

    move-result-object v4

    .line 150
    .local v4, "resList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    .line 151
    .local v3, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 153
    .local v5, "uri":Ljava/lang/String;
    if-eqz v5, :cond_3

    .line 154
    const/4 v1, -0x1

    .line 155
    .local v1, "i":I
    const-string v7, "?rtt="

    invoke-virtual {v5, v7}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 156
    if-lez v1, :cond_4

    .line 157
    const/4 v7, 0x0

    invoke-virtual {v5, v7, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 158
    :cond_4
    invoke-virtual {v5, p1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v5, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    goto :goto_0

    .end local v1    # "i":I
    .end local v3    # "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v5    # "uri":Ljava/lang/String;
    :cond_5
    move-object v3, v6

    .line 163
    goto :goto_0
.end method

.method public getAlbum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    const-string v0, "upnp:album"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumArtUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    const-string v0, "upnp:albumArtURI"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCreator()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    const-string v0, "dc:creator"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-string v0, "dc:date"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDateTime()J
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getDate()Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "dateStr":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    const/16 v7, 0xa

    if-ge v6, v7, :cond_1

    .line 70
    :cond_0
    :goto_0
    return-wide v4

    .line 62
    :cond_1
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy-MM-dd"

    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 64
    .local v2, "df":Ljava/text/DateFormat;
    :try_start_0
    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    .line 65
    .local v0, "date":Ljava/util/Date;
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 68
    .end local v0    # "date":Ljava/util/Date;
    :catch_0
    move-exception v3

    .line 69
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public getDefaultResource()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .locals 3

    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getPropertyList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 168
    .local v1, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    if-eqz v2, :cond_0

    .line 169
    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    .line 171
    .end local v1    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getResoureList()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 176
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;>;"
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getPropertyList()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 177
    .local v2, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v2, :cond_0

    instance-of v3, v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    if-eqz v3, :cond_0

    .line 178
    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    .end local v2    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 180
    :cond_1
    return-object v1
.end method

.method public getStorageMedium()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    const-string v0, "upnp:storageMedium"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAlbum(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    const-string v0, "upnp:album"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public setCreator(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 79
    const-string v0, "dc:creator"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public setDate(Ljava/lang/String;)V
    .locals 1
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 115
    const-string v0, "dc:date"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method public setStorageMedium(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 127
    const-string v0, "upnp:storageMedium"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    return-void
.end method

.method public setUPnPAlbumArtUri(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 103
    const-string v0, "upnp:albumArtURI"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-void
.end method

.method public update()V
    .locals 0

    .prologue
    .line 187
    return-void
.end method
