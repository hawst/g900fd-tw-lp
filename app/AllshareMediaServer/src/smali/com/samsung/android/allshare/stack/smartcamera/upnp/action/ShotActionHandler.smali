.class public Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotActionHandler;
.super Ljava/lang/Object;
.source "ShotActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# instance fields
.field private mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V
    .locals 1
    .param p1, "cameraListener"    # Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 27
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 28
    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 7
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/16 v6, 0x192

    const/4 v4, 0x0

    .line 32
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v5}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onShotRequest_GetAFShotResult_Request()Ljava/lang/String;

    move-result-object v3

    .line 33
    .local v3, "strAFShotResult":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v5}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onShotRequest_GetAvailShots_Request()I

    move-result v2

    .line 35
    .local v2, "nAvailShots":I
    const-string v5, "AFSHOTRESULT"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 36
    .local v0, "afShotResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 43
    const-string v5, "AVAILSHOTS"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v1

    .line 44
    .local v1, "availShotsResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v1, :cond_1

    .line 45
    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 51
    const/4 v4, 0x1

    .end local v1    # "availShotsResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :goto_0
    return v4

    .line 39
    :cond_0
    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 47
    .restart local v1    # "availShotsResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :cond_1
    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0
.end method
