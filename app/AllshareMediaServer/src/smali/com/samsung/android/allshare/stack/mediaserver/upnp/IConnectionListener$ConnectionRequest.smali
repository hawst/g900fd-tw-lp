.class public abstract Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
.super Ljava/lang/Object;
.source "IConnectionListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ConnectionRequest"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getMacAddress()Ljava/lang/String;
.end method

.method public abstract getTargetHost()Ljava/lang/String;
.end method

.method public abstract getTargetInetAddr()Ljava/net/InetAddress;
.end method

.method public abstract getTargetPort()I
.end method

.method public abstract getUserAgent()Ljava/lang/String;
.end method
