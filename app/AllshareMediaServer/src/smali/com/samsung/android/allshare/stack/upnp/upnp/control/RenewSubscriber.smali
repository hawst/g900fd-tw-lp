.class public Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;
.super Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;
.source "RenewSubscriber.java"


# static fields
.field public static final INTERVAL:J = 0x78L

.field private static final TAG:Ljava/lang/String; = "RenewSubscriber"


# instance fields
.field private mCtrlPoint:Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;)V
    .locals 0
    .param p1, "ctrlp"    # Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;-><init>()V

    .line 26
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;->setControlPoint(Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;)V

    .line 27
    return-void
.end method


# virtual methods
.method public getControlPoint()Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;->mCtrlPoint:Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    return-object v0
.end method

.method public run()V
    .locals 7

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;->getControlPoint()Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    move-result-object v0

    .line 50
    .local v0, "ctrlp":Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;
    const-wide/32 v2, 0x1d4c0

    .line 51
    .local v2, "renewInterval":J
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;->isRunnable()Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 53
    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 54
    const-string v4, "RenewSubscriber"

    const-string v5, "run"

    const-string v6, "call renewSubscriberService()"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    invoke-interface {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;->renewSubscriberService()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v1

    .line 57
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v4, "RenewSubscriber"

    const-string v5, "run"

    const-string v6, "run InterruptedException "

    invoke-static {v4, v5, v6, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 60
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_0
    return-void
.end method

.method public setControlPoint(Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;)V
    .locals 0
    .param p1, "ctrlp"    # Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;->mCtrlPoint:Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    .line 37
    return-void
.end method
