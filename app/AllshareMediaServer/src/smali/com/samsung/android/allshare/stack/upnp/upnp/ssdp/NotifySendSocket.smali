.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;
.source "NotifySendSocket.java"


# instance fields
.field private mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

.field private mJoinAddr:Ljava/net/InetAddress;

.field private mJoinAddressStr:Ljava/lang/String;

.field private mJoinPort:I


# direct methods
.method private constructor <init>(Ljava/lang/String;ILjava/net/InetAddress;)V
    .locals 3
    .param p1, "joinAddr"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "bindAddr"    # Ljava/net/InetAddress;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPUSocket;-><init>()V

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinAddressStr:Ljava/lang/String;

    .line 52
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinAddr:Ljava/net/InetAddress;

    .line 54
    iput v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinPort:I

    .line 102
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 61
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinAddressStr:Ljava/lang/String;

    .line 62
    iput p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinPort:I

    .line 63
    invoke-virtual {p0, p3, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->open(Ljava/net/InetAddress;I)Z

    .line 64
    return-void
.end method

.method public static createSSDPNotifySocket(Ljava/lang/String;ILjava/net/InetAddress;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .locals 4
    .param p0, "joinAddr"    # Ljava/lang/String;
    .param p1, "port"    # I
    .param p2, "bindAddr"    # Ljava/net/InetAddress;

    .prologue
    const/4 v2, 0x0

    .line 68
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;-><init>(Ljava/lang/String;ILjava/net/InetAddress;)V

    .line 71
    .local v1, "socket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :try_start_0
    invoke-static {p0}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinAddr:Ljava/net/InetAddress;

    .line 74
    iget-object v3, v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinAddr:Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->isMulticastAddress()Z

    move-result v3

    if-nez v3, :cond_1

    move-object v1, v2

    .line 87
    .end local v1    # "socket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :cond_0
    :goto_0
    return-object v1

    .line 77
    .restart local v1    # "socket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :cond_1
    iget-object v3, v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinAddr:Ljava/net/InetAddress;

    instance-of v3, v3, Ljava/net/Inet4Address;

    if-eqz v3, :cond_2

    instance-of v3, p2, Ljava/net/Inet4Address;

    if-nez v3, :cond_2

    move-object v1, v2

    .line 78
    goto :goto_0

    .line 80
    :cond_2
    iget-object v3, v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinAddr:Ljava/net/InetAddress;

    instance-of v3, v3, Ljava/net/Inet6Address;

    if-eqz v3, :cond_0

    instance-of v3, p2, Ljava/net/Inet6Address;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v3, :cond_0

    move-object v1, v2

    .line 81
    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/net/UnknownHostException;
    move-object v1, v2

    .line 84
    goto :goto_0
.end method


# virtual methods
.method public getDiscoveryInfo()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    return-object v0
.end method

.method public post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z
    .locals 3
    .param p1, "req"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinAddressStr:Ljava/lang/String;

    iget v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinPort:I

    invoke-virtual {p1, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setHost(Ljava/lang/String;I)V

    .line 96
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinAddr:Ljava/net/InetAddress;

    iget v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mJoinPort:I

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Ljava/net/InetAddress;ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setDiscoveryInfo(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V
    .locals 0
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->mInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 106
    return-void
.end method
