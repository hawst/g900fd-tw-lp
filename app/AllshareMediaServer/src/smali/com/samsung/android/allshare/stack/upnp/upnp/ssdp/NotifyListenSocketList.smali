.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
.super Ljava/util/ArrayList;
.source "NotifyListenSocketList.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList",
        "<",
        "Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    return-void
.end method


# virtual methods
.method public setNotifiyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;

    .line 37
    .local v1, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->setNotifiyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    goto :goto_0

    .line 38
    .end local v1    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    :cond_0
    return-void
.end method

.method public start()V
    .locals 3

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;

    .line 42
    .local v1, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->start()V

    goto :goto_0

    .line 43
    .end local v1    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;

    .line 47
    .local v1, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->stop()V

    goto :goto_0

    .line 48
    .end local v1    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    :cond_0
    return-void
.end method
