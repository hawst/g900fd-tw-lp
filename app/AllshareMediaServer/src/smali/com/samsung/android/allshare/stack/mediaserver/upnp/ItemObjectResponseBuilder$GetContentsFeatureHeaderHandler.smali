.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$GetContentsFeatureHeaderHandler;
.super Ljava/lang/Object;
.source "ItemObjectResponseBuilder.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$IHandleHeader;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetContentsFeatureHeaderHandler"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$1;

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ItemObjectResponseBuilder$GetContentsFeatureHeaderHandler;-><init>()V

    return-void
.end method


# virtual methods
.method public handle(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z
    .locals 3
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    .param p2, "header"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    .param p3, "object"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p4, "res"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .param p5, "resp"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    .prologue
    .line 129
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    invoke-virtual {p4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->get4thfield()Ljava/lang/String;

    move-result-object v0

    .line 132
    .local v0, "feature":Ljava/lang/String;
    const-string v1, "contentFeatures.dlna.org"

    invoke-virtual {p5, v1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const/4 v1, 0x1

    .line 136
    .end local v0    # "feature":Ljava/lang/String;
    :goto_0
    return v1

    .line 135
    :cond_0
    const/16 v1, 0x190

    invoke-virtual {p5, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 136
    const/4 v1, 0x0

    goto :goto_0
.end method
