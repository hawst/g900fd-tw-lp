.class public Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;
.source "QueryResponse.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;-><init>()V

    .line 35
    return-void
.end method

.method private createResponseNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 4
    .param p1, "var"    # Ljava/lang/String;

    .prologue
    .line 89
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>()V

    .line 90
    .local v0, "queryResNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const-string v2, "u"

    const-string v3, "QueryStateVariableResponse"

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setName(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v2, "u"

    const-string v3, "urn:schemas-upnp-org:control-1-0"

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNameSpace(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>()V

    .line 94
    .local v1, "returnNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const-string v2, "return"

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setName(Ljava/lang/String;)V

    .line 95
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 96
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 98
    return-object v0
.end method


# virtual methods
.method public setResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;)V
    .locals 5
    .param p1, "stateVar"    # Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    .prologue
    .line 74
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getValue()Ljava/lang/String;

    move-result-object v3

    .line 76
    .local v3, "var":Ljava/lang/String;
    const/16 v4, 0xc8

    invoke-virtual {p0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;->setStatusCode(I)V

    .line 78
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 79
    .local v0, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;->createResponseNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 80
    .local v2, "resNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 84
    .local v1, "envNodee":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;->setContent(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 86
    return-void
.end method
