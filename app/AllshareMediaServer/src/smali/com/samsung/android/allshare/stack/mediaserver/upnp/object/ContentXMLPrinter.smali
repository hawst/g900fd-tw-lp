.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter;
.super Ljava/lang/Object;
.source "ContentXMLPrinter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method

.method public static print(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/net/InetAddress;II)Ljava/lang/String;
    .locals 3
    .param p0, "content"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .param p1, "target"    # Ljava/net/InetAddress;
    .param p2, "serverPort"    # I
    .param p3, "streamingPort"    # I

    .prologue
    .line 39
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;-><init>()V

    .line 40
    .local v0, "didlLite":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;
    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->mContentList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->toString()Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "result":Ljava/lang/String;
    invoke-static {v1, p1, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter;->replaceWildcard(Ljava/lang/String;Ljava/net/InetAddress;II)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static print(Ljava/util/Collection;Ljava/net/InetAddress;II)Ljava/lang/String;
    .locals 3
    .param p1, "target"    # Ljava/net/InetAddress;
    .param p2, "serverPort"    # I
    .param p3, "streamingPort"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;",
            "Ljava/net/InetAddress;",
            "II)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 48
    .local p0, "contents":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;-><init>()V

    .line 49
    .local v0, "didlLite":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;
    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->mContentList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 50
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter$DIDLLite;->toString()Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "result":Ljava/lang/String;
    invoke-static {v1, p1, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter;->replaceWildcard(Ljava/lang/String;Ljava/net/InetAddress;II)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private static replaceWildcard(Ljava/lang/String;Ljava/net/InetAddress;II)Ljava/lang/String;
    .locals 4
    .param p0, "result"    # Ljava/lang/String;
    .param p1, "target"    # Ljava/net/InetAddress;
    .param p2, "serverPort"    # I
    .param p3, "streamingPort"    # I

    .prologue
    .line 57
    const/4 v0, 0x0

    .line 58
    .local v0, "source":Ljava/net/InetAddress;
    if-eqz p1, :cond_0

    .line 59
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v0

    .line 61
    :cond_0
    if-eqz v0, :cond_1

    .line 62
    const-string v1, "$X@Z@Z@X:$Y@Z@Z@Y"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 65
    const-string v1, "$X@Z@Z@X:$W@Z@Z@W"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    .line 73
    :cond_1
    return-object p0
.end method
