.class public abstract Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
.super Ljava/lang/Object;
.source "Directory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;
    }
.end annotation


# instance fields
.field private mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

.field mDirectoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;",
            ">;"
        }
    .end annotation
.end field

.field private mItemMap:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;",
            ">;"
        }
    .end annotation
.end field

.field private mListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;

.field private mMimeType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    .line 107
    const-string v0, "*/*"

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mMimeType:Ljava/lang/String;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mDirectoryList:Ljava/util/ArrayList;

    .line 201
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mItemMap:Ljava/util/TreeMap;

    .line 278
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;

    .line 35
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mMimeType:Ljava/lang/String;

    .line 36
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-direct {v0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    .line 38
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setChildCount(I)V

    .line 39
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setTitle(Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setID(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    const-string v1, "-1"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setParentID(Ljava/lang/String;)V

    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    const-string v1, "object.container"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setUPnPClass(Ljava/lang/String;)V

    .line 44
    return-void
.end method


# virtual methods
.method public final addDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)V
    .locals 1
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->setParentID(Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mDirectoryList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->adjustChildCount(I)V

    .line 126
    return-void
.end method

.method public addItemObject(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V
    .locals 2
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .prologue
    .line 248
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setParentID(Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mItemMap:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 250
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->adjustChildCount(I)V

    .line 254
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->notifiyDirectoryChanged()V

    .line 255
    return-void
.end method

.method protected final declared-synchronized adjustChildCount(I)V
    .locals 3
    .param p1, "difference"    # I

    .prologue
    .line 262
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->getChildCount()I

    move-result v0

    .line 263
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    add-int v2, v0, p1

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setChildCount(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    monitor-exit p0

    return-void

    .line 262
    .end local v0    # "count":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public final getDirectories()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mDirectoryList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getDirectoriesByMimeType(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .param p1, "mime"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 161
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 162
    :cond_0
    const/4 v5, 0x0

    .line 175
    :cond_1
    return-object v5

    .line 164
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 166
    .local v5, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;>;"
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getDirectories()Ljava/util/ArrayList;

    move-result-object v1

    .line 167
    .local v1, "dirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .line 168
    .local v0, "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getMimeType()Ljava/lang/String;

    move-result-object v4

    .line 169
    .local v4, "majorType":Ljava/lang/String;
    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 170
    .local v3, "index":I
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 171
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 172
    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 173
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final getDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .locals 6
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 141
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    move-object p0, v4

    .line 157
    .end local p0    # "this":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    :cond_1
    :goto_0
    return-object p0

    .line 144
    .restart local p0    # "this":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 147
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getDirectories()Ljava/util/ArrayList;

    move-result-object v2

    .line 148
    .local v2, "dirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .line 149
    .local v0, "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move-object p0, v0

    .line 150
    goto :goto_0

    .line 152
    :cond_4
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v1

    .line 153
    .local v1, "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-eqz v1, :cond_3

    move-object p0, v1

    .line 154
    goto :goto_0

    .end local v0    # "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .end local v1    # "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    :cond_5
    move-object p0, v4

    .line 157
    goto :goto_0
.end method

.method public final getDirectoryByMimeType(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .locals 7
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 179
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    move-object v0, v5

    .line 194
    :cond_1
    :goto_0
    return-object v0

    .line 182
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getDirectories()Ljava/util/ArrayList;

    move-result-object v1

    .line 183
    .local v1, "dirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .line 184
    .local v0, "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    instance-of v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    if-nez v6, :cond_3

    .line 187
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getMimeType()Ljava/lang/String;

    move-result-object v4

    .line 188
    .local v4, "majorType":Ljava/lang/String;
    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 189
    .local v3, "index":I
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 190
    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 191
    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "*"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_0

    .end local v0    # "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .end local v3    # "index":I
    .end local v4    # "majorType":Ljava/lang/String;
    :cond_4
    move-object v0, v5

    .line 194
    goto :goto_0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getItemByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .locals 4
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 205
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mItemMap:Ljava/util/TreeMap;

    invoke-virtual {v3, p1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .line 206
    .local v2, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 216
    :goto_0
    return-object v3

    .line 210
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getDirectories()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .line 211
    .local v0, "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getItemByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-result-object v2

    .line 212
    if-eqz v2, :cond_1

    move-object v3, v2

    .line 213
    goto :goto_0

    .line 216
    .end local v0    # "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected final getItemMap()Ljava/util/TreeMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mItemMap:Ljava/util/TreeMap;

    return-object v0
.end method

.method public final getItems()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mItemMap:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 227
    .local v0, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;>;"
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v1

    return-object v1
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method protected final getModifiableItems()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mItemMap:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getParentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->getParentID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUPnPClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->getUPnPClass()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected notifiyDirectoryChanged()V
    .locals 4

    .prologue
    .line 285
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;

    if-eqz v1, :cond_0

    .line 287
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;

    invoke-interface {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;->onChange()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 288
    :catch_0
    move-exception v0

    .line 289
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "Directory"

    const-string v2, "notifiyDirectoryChanged"

    const-string v3, "Got Exception when IDirectoryChangedListener onchange : "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 291
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/Error;
    const-string v1, "Directory"

    const-string v2, "notifiyDirectoryChanged"

    const-string v3, "Got ERROR when IDirectoryChangedListener onchange : "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0
.end method

.method public final removeDirectory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)V
    .locals 1
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mDirectoryList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 133
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->adjustChildCount(I)V

    .line 134
    return-void
.end method

.method public removeItemObject(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V
    .locals 2
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mItemMap:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 241
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->adjustChildCount(I)V

    .line 243
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->notifiyDirectoryChanged()V

    .line 245
    :cond_0
    return-void
.end method

.method protected final declared-synchronized setChildCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 267
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setChildCount(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    monitor-exit p0

    return-void

    .line 267
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final setFriendlyName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setTitle(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public final setID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setID(Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public final setOnDirectoryChangedListener(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory$IDirectoryChangedListener;

    .line 282
    return-void
.end method

.method public final setParentID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setParentID(Ljava/lang/String;)V

    .line 76
    return-void
.end method

.method public final setRestricted(Z)V
    .locals 1
    .param p1, "restrict"    # Z

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setRestricted(Z)V

    .line 98
    return-void
.end method

.method public final setSearchable(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setSearchable(I)V

    .line 102
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mMimeType:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public toContentNode()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->mContentNode:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;

    return-object v0
.end method

.method public abstract update()Z
.end method
