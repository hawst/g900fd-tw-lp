.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;
.super Ljava/lang/Object;
.source "MediaServerUris.java"


# static fields
.field public static final CODE_CDS_ACTION_URI:I = 0x12d

.field public static final CODE_CMS_ACTION_URI:I = 0x12e

.field public static final CODE_CONTENT_URI:I = 0xc9

.field public static final CODE_ICON:I = 0x65

.field public static final IP_WILDCARD:Ljava/lang/String; = "$X@Z@Z@X"

.field private static final PATH_ALBUM_URI:Ljava/lang/String; = "media/albumart/"

.field public static final PATH_CDS_ACTION_URI:Ljava/lang/String; = "control/cds"

.field public static final PATH_CMS_ACTION_URI:Ljava/lang/String; = "control/cms"

.field public static final PATH_CONTENT_URI:Ljava/lang/String; = "media/"

.field public static final PATH_ICON:Ljava/lang/String; = "icon/"

.field public static final PATH_ICON_MICRO_JPEG:Ljava/lang/String; = "icon//micro.jpg"

.field public static final PATH_ICON_MICRO_PNG:Ljava/lang/String; = "icon//micro.png"

.field public static final PATH_ICON_SMALL_JPEG:Ljava/lang/String; = "icon//small.jpg"

.field public static final PATH_ICON_SMALL_PNG:Ljava/lang/String; = "icon//small.png"

.field private static final PATH_MICRO_THUMBNAILS_URI:Ljava/lang/String; = "media/thumb/"

.field private static final PATH_SMALL_CONTENT_URI:Ljava/lang/String; = "media/small/"

.field private static final PATH_SUBTITLE_URI:Ljava/lang/String; = "media/subtitle/"

.field public static final PORT_WILDCARD:Ljava/lang/String; = "$Y@Z@Z@Y"

.field public static final PORT_WILDCARD_FOR_STREAMING:Ljava/lang/String; = "$W@Z@Z@W"

.field private static final URI_MATCHER:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0xc9

    .line 83
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 85
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "*"

    const-string v2, "control/cds"

    const/16 v3, 0x12d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "*"

    const-string v2, "control/cms"

    const/16 v3, 0x12e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 97
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "*"

    const-string v2, "media/thumb/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 98
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "*"

    const-string v2, "media/small/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 99
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "*"

    const-string v2, "media/subtitle/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 100
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "*"

    const-string v2, "media/albumart/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 101
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "*"

    const-string v2, "media/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 103
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "*"

    const-string v2, "icon/*"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 104
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static getContentAlbumArtExportURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 160
    const-string v0, "media/albumart/"

    invoke-static {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getContentMicroThumbnailExportURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 138
    const-string v0, "media/thumb/"

    invoke-static {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getContentSmallContentExportURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 128
    const-string v0, "media/small/"

    invoke-static {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getContentSubtitleExportURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "extension"    # Ljava/lang/String;

    .prologue
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "media/subtitle/"

    invoke-static {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getURLforStreamingServer(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getContentURL(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # Ljava/lang/String;

    .prologue
    .line 118
    const-string v0, "media/"

    invoke-static {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getURLforStreamingServer(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "basePath"    # Ljava/lang/String;

    .prologue
    .line 174
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://$X@Z@Z@X:$Y@Z@Z@Y/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getURLforStreamingServer(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # Ljava/lang/String;
    .param p1, "basePath"    # Ljava/lang/String;

    .prologue
    .line 178
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://$X@Z@Z@X:$W@Z@Z@W/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static match(Landroid/net/Uri;)I
    .locals 1
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 170
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    return v0
.end method
