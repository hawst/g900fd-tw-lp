.class public Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
.super Ljava/lang/Object;
.source "Device.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;
.implements Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;
.implements Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;
    }
.end annotation


# static fields
.field private static final ASF_FRAMEWORK_VERSION:Ljava/lang/String; = "asf:X_firmwareVersion"

.field private static final ASF_PORT_NUMBER:Ljava/lang/String; = "asf:X_portNumber"

.field private static final ASF_PRODUCTTYPE:Ljava/lang/String; = "asf:X_productType"

.field public static final DEFAULT_DESCRIPTION_URI:Ljava/lang/String; = "/description.xml"

.field public static final DEFAULT_DISCOVERY_WAIT_TIME:I = 0x64

.field public static final DEFAULT_LEASE_TIME:I = 0x708

.field public static final DEFAULT_STARTUP_WAIT_TIME:I = 0x3e8

.field private static final DEVICE_ID:Ljava/lang/String; = "sec:deviceID"

.field private static final DEVICE_TYPE:Ljava/lang/String; = "deviceType"

.field public static final ELEM_NAME:Ljava/lang/String; = "device"

.field private static final FRIENDLY_NAME:Ljava/lang/String; = "friendlyName"

.field public static final HTTP_DEFAULT_PORT:I = 0xfa4

.field private static final MANUFACTURE:Ljava/lang/String; = "manufacturer"

.field private static final MANUFACTURE_URL:Ljava/lang/String; = "manufactureURL"

.field private static final MAX_FRIENDLY_NAME_LENGTH:I = 0x40

.field private static final MODEL_DESCRIPTION:Ljava/lang/String; = "modelDescription"

.field private static final MODEL_NAME:Ljava/lang/String; = "modelName"

.field private static final MODEL_NUMBER:Ljava/lang/String; = "modelNumber"

.field private static final PRODUCT_CAP:Ljava/lang/String; = "sec:ProductCap"

.field private static final UDN:Ljava/lang/String; = "UDN"

.field public static final UPNP_ROOTDEVICE:Ljava/lang/String; = "upnp:rootdevice"

.field private static final URLBASE_NAME:Ljava/lang/String; = "URLBase"


# instance fields
.field private mDescriptionCache:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation
.end field

.field private mDevUUID:Ljava/lang/String;

.field private mDeviceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private mExecutor:Ljava/util/concurrent/ExecutorService;

.field private mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field protected mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    .line 190
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 253
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    .line 1608
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDescriptionCache:Ljava/util/LinkedHashMap;

    .line 229
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    .line 230
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->generateUPnPUUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setUUID(Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 1
    .param p1, "device"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 224
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 225
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 2
    .param p1, "root"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .param p2, "device"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    const/4 v1, 0x0

    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    .line 190
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 253
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    .line 1608
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDescriptionCache:Ljava/util/LinkedHashMap;

    .line 218
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 219
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDeviceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 220
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->deviceSearchResponse(Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    return-void
.end method

.method private clearDescriptionCache()V
    .locals 2

    .prologue
    .line 1611
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDescriptionCache:Ljava/util/LinkedHashMap;

    monitor-enter v1

    .line 1612
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDescriptionCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 1613
    monitor-exit v1

    .line 1614
    return-void

    .line 1613
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private deviceActionControlRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V
    .locals 5
    .param p1, "ctlReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    .param p2, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .prologue
    .line 1756
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 1771
    :cond_0
    :goto_0
    return-void

    .line 1758
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->print()V

    .line 1759
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getActionName()Ljava/lang/String;

    move-result-object v2

    .line 1760
    .local v2, "actionName":Ljava/lang/String;
    invoke-virtual {p2, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getAction(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    move-result-object v0

    .line 1761
    .local v0, "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    if-nez v0, :cond_2

    .line 1762
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->invalidActionControlRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;)V

    goto :goto_0

    .line 1766
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 1767
    .local v1, "actionArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v3

    .line 1768
    .local v3, "reqArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->setArgumentList(Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;)V

    .line 1769
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->performActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1770
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->invalidActionControlRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;)V

    goto :goto_0
.end method

.method private deviceControlRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V
    .locals 2
    .param p1, "ctlReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;
    .param p2, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .prologue
    .line 1739
    if-nez p1, :cond_0

    .line 1745
    :goto_0
    return-void

    .line 1741
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;->isQueryControl()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1742
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;

    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->deviceQueryControlRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V

    goto :goto_0

    .line 1744
    :cond_1
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->deviceActionControlRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V

    goto :goto_0
.end method

.method private deviceEventNewSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;)V
    .locals 13
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .param p2, "subReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;

    .prologue
    .line 1860
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1916
    :cond_0
    :goto_0
    return-void

    .line 1862
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v10, "deviceEventNewSubscriptionRecieved"

    const-string v11, "Subscription : deviceEventNewSubscriptionRecieved"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1864
    const-string v9, "NT"

    invoke-virtual {p2, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1865
    .local v2, "nt":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getCallback()Ljava/lang/String;

    move-result-object v0

    .line 1867
    .local v0, "callback":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_3

    .line 1868
    :cond_2
    const/16 v9, 0x190

    invoke-direct {p0, p2, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1872
    :cond_3
    const-string v9, "upnp:event"

    invoke-virtual {v2, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1873
    const/16 v9, 0x19c

    invoke-direct {p0, p2, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1878
    :cond_4
    :try_start_0
    new-instance v8, Ljava/net/URL;

    invoke-direct {v8, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1879
    .local v8, "url":Ljava/net/URL;
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v10, "deviceEventNewSubscriptionRecieved"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "deviceEventNewSubscriptionRecieved"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1888
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getTimeout()J

    move-result-wide v6

    .line 1889
    .local v6, "timeOut":J
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscription;->createSID()Ljava/lang/String;

    move-result-object v3

    .line 1891
    .local v3, "sid":Ljava/lang/String;
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    invoke-direct {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;-><init>()V

    .line 1892
    .local v4, "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->setDeliveryURL(Ljava/lang/String;)V

    .line 1893
    invoke-virtual {v4, v6, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->setTimeOut(J)V

    .line 1894
    invoke-virtual {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->setSID(Ljava/lang/String;)V

    .line 1895
    invoke-virtual {p1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->addSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;)V

    .line 1897
    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;

    invoke-direct {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;-><init>()V

    .line 1898
    .local v5, "subRes":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    const/16 v9, 0xc8

    invoke-virtual {v5, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setStatusCode(I)V

    .line 1899
    invoke-virtual {v5, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setSID(Ljava/lang/String;)V

    .line 1900
    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setTimeout(J)V

    .line 1901
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->print()V

    .line 1902
    invoke-virtual {p2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;)V

    .line 1904
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->isKeepAliveConnection()Z

    move-result v9

    if-nez v9, :cond_5

    .line 1905
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getSocket()Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->close()Z

    .line 1912
    :cond_5
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->print()V

    .line 1913
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v10, "deviceEventNewSubscriptionRecieved"

    const-string v11, "Subscription : Notify To Subscriber"

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1914
    invoke-virtual {p1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->notifyAllStateVariablesToSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;)V

    goto/16 :goto_0

    .line 1881
    .end local v3    # "sid":Ljava/lang/String;
    .end local v4    # "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    .end local v5    # "subRes":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    .end local v6    # "timeOut":J
    .end local v8    # "url":Ljava/net/URL;
    :catch_0
    move-exception v1

    .line 1882
    .local v1, "e":Ljava/lang/Exception;
    const/16 v9, 0x19c

    invoke-direct {p0, p2, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    .line 1883
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v10, "deviceEventNewSubscriptionRecieved"

    const-string v11, "deviceEventNewSubscriptionRecieved Exception"

    invoke-static {v9, v10, v11, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto/16 :goto_0
.end method

.method private deviceEventRenewSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;)V
    .locals 6
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .param p2, "subReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;

    .prologue
    .line 1919
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1950
    :cond_0
    :goto_0
    return-void

    .line 1921
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getSID()Ljava/lang/String;

    move-result-object v0

    .line 1922
    .local v0, "sid":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSubscriber(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    move-result-object v1

    .line 1924
    .local v1, "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    if-nez v1, :cond_2

    .line 1925
    const/16 v3, 0x19c

    invoke-direct {p0, p2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1934
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->isExpired()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1935
    const/16 v3, 0x1f4

    invoke-direct {p0, p2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1939
    :cond_3
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getTimeout()J

    move-result-wide v4

    .line 1940
    .local v4, "timeOut":J
    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->setTimeOut(J)V

    .line 1941
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->renew()V

    .line 1943
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;-><init>()V

    .line 1944
    .local v2, "subRes":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    const/16 v3, 0xc8

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setStatusCode(I)V

    .line 1945
    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setSID(Ljava/lang/String;)V

    .line 1946
    invoke-virtual {v2, v4, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setTimeout(J)V

    .line 1947
    invoke-virtual {p2, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;)V

    .line 1949
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->print()V

    goto :goto_0
.end method

.method private deviceEventSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;)V
    .locals 7
    .param p1, "subReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;

    .prologue
    const/16 v6, 0x19c

    const/16 v5, 0x190

    const/4 v4, 0x1

    .line 1801
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getURI()Ljava/lang/String;

    move-result-object v1

    .line 1802
    .local v1, "uri":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceByEventSubURL(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v0

    .line 1803
    .local v0, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-nez v0, :cond_0

    .line 1804
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->returnBadRequest()Z

    .line 1851
    :goto_0
    return-void

    .line 1807
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->hasCallback()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->hasSID()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1808
    invoke-direct {p0, p1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1812
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->hasNT()Z

    move-result v2

    if-ne v2, v4, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getNT()Ljava/lang/String;

    move-result-object v2

    const-string v3, "upnp:event"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1813
    invoke-direct {p0, p1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1818
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->isUnsubscribeRequest()Z

    move-result v2

    if-ne v2, v4, :cond_5

    .line 1820
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->hasNT()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->hasCallback()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1821
    :cond_3
    invoke-direct {p0, p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1823
    :cond_4
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->deviceEventUnsubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;)V

    goto :goto_0

    .line 1829
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->hasCallback()Z

    move-result v2

    if-ne v2, v4, :cond_6

    .line 1830
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->deviceEventNewSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;)V

    goto :goto_0

    .line 1841
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->hasSID()Z

    move-result v2

    if-ne v2, v4, :cond_9

    .line 1842
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->hasNT()Z

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->hasCallback()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1843
    :cond_7
    invoke-direct {p0, p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1845
    :cond_8
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->deviceEventRenewSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;)V

    goto :goto_0

    .line 1850
    :cond_9
    invoke-direct {p0, p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0
.end method

.method private deviceEventUnsubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;)V
    .locals 4
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .param p2, "subReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;

    .prologue
    .line 1953
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1972
    :cond_0
    :goto_0
    return-void

    .line 1955
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getSID()Ljava/lang/String;

    move-result-object v0

    .line 1956
    .local v0, "sid":Ljava/lang/String;
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSubscriber(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;

    move-result-object v1

    .line 1958
    .local v1, "sub":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    if-nez v1, :cond_2

    .line 1959
    const/16 v3, 0x19c

    invoke-direct {p0, p2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V

    goto :goto_0

    .line 1963
    :cond_2
    monitor-enter p0

    .line 1964
    :try_start_0
    invoke-virtual {p1, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->removeSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;)V

    .line 1965
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1967
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;-><init>()V

    .line 1968
    .local v2, "subRes":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    const/16 v3, 0xc8

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setStatusCode(I)V

    .line 1969
    invoke-virtual {p2, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;)V

    .line 1971
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->print()V

    goto :goto_0

    .line 1965
    .end local v2    # "subRes":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3
.end method

.method private deviceQueryControlRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V
    .locals 3
    .param p1, "ctlReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;
    .param p2, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .prologue
    .line 1774
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 1785
    :cond_0
    :goto_0
    return-void

    .line 1776
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;->print()V

    .line 1777
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;->getVarName()Ljava/lang/String;

    move-result-object v1

    .line 1778
    .local v1, "varName":Ljava/lang/String;
    invoke-virtual {p2, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->hasStateVariable(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1779
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->invalidActionControlRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;)V

    goto :goto_0

    .line 1782
    :cond_2
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getStateVariable(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v0

    .line 1783
    .local v0, "stateVar":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    if-eqz v0, :cond_3

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->performQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1784
    :cond_3
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->invalidActionControlRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;)V

    goto :goto_0
.end method

.method private deviceSearchResponse(Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 18
    .param p1, "nicName"    # Ljava/lang/String;
    .param p2, "ssdpPacket"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 1404
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->isValid()Z

    move-result v15

    if-nez v15, :cond_1

    .line 1462
    :cond_0
    return-void

    .line 1407
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getST()Ljava/lang/String;

    move-result-object v14

    .line 1409
    .local v14, "ssdpST":Ljava/lang/String;
    if-eqz v14, :cond_0

    .line 1412
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v9

    .line 1414
    .local v9, "isRootDevice":Z
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v8

    .line 1415
    .local v8, "devUSN":Ljava/lang/String;
    const/4 v15, 0x1

    if-ne v9, v15, :cond_2

    .line 1416
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "::upnp:rootdevice"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1418
    :cond_2
    invoke-static {v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ST;->isAllDevice(Ljava/lang/String;)Z

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_4

    .line 1419
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v16, "deviceSearchResponse"

    const-string v17, "SSDP : All Device"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1420
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v7

    .line 1421
    .local v7, "devUDN":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v7, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->postSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1422
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyDeviceNT()Ljava/lang/String;

    move-result-object v5

    .line 1423
    .local v5, "devNT":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v5, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->postSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1424
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyDeviceTypeNT()Ljava/lang/String;

    move-result-object v5

    .line 1425
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyDeviceTypeUSN()Ljava/lang/String;

    move-result-object v8

    .line 1426
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v5, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->postSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1449
    .end local v5    # "devNT":Ljava/lang/String;
    .end local v7    # "devUDN":Ljava/lang/String;
    :cond_3
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v13

    .line 1450
    .local v13, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v12

    .line 1451
    .local v12, "serviceCnt":I
    const/4 v10, 0x0

    .local v10, "n":I
    :goto_1
    if-ge v10, v12, :cond_7

    .line 1452
    invoke-virtual {v13, v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v11

    .line 1453
    .local v11, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    move-object/from16 v0, p2

    invoke-virtual {v11, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->serviceSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)Z

    .line 1451
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 1427
    .end local v10    # "n":I
    .end local v11    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .end local v12    # "serviceCnt":I
    .end local v13    # "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    :cond_4
    invoke-static {v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ST;->isRootDevice(Ljava/lang/String;)Z

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_5

    .line 1428
    const/4 v15, 0x1

    if-ne v9, v15, :cond_3

    .line 1429
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v16, "deviceSearchResponse"

    const-string v17, "SSDP : Root Device"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    const-string v15, "upnp:rootdevice"

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v15, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->postSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 1432
    :cond_5
    invoke-static {v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ST;->isUUIDDevice(Ljava/lang/String;)Z

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_6

    .line 1433
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v7

    .line 1434
    .restart local v7    # "devUDN":Ljava/lang/String;
    invoke-virtual {v14, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 1435
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v16, "deviceSearchResponse"

    const-string v17, "SSDP : UUID Device"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1436
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v7, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->postSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    .line 1439
    .end local v7    # "devUDN":Ljava/lang/String;
    :cond_6
    invoke-static {v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ST;->isURNDevice(Ljava/lang/String;)Z

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 1440
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v6

    .line 1441
    .local v6, "devType":Ljava/lang/String;
    invoke-virtual {v6, v14}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 1443
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v16, "deviceSearchResponse"

    const-string v17, "SSDP : URN Device"

    invoke-static/range {v15 .. v17}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "::"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1445
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v6, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->postSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 1456
    .end local v6    # "devType":Ljava/lang/String;
    .restart local v10    # "n":I
    .restart local v12    # "serviceCnt":I
    .restart local v13    # "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v4

    .line 1457
    .local v4, "childDeviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v3

    .line 1458
    .local v3, "childDeviceCnt":I
    const/4 v10, 0x0

    :goto_2
    if-ge v10, v3, :cond_0

    .line 1459
    invoke-virtual {v4, v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v2

    .line 1460
    .local v2, "childDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->deviceSearchResponse(Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    .line 1458
    add-int/lit8 v10, v10, 0x1

    goto :goto_2
.end method

.method public static generateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "uuid"    # Ljava/lang/String;
    .param p1, "boundInterfaceName"    # Ljava/lang/String;

    .prologue
    .line 470
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "+"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getAdvertiser()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;
    .locals 1

    .prologue
    .line 1992
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getAdvertiser()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized getDescriptionData(Ljava/lang/String;)[B
    .locals 4
    .param p1, "host"    # Ljava/lang/String;

    .prologue
    .line 1571
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 1572
    .local v1, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v1, :cond_0

    .line 1573
    const/4 v2, 0x0

    new-array v2, v2, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1581
    :goto_0
    monitor-exit p0

    return-object v2

    .line 1576
    :cond_0
    :try_start_1
    const-string v0, ""

    .line 1577
    .local v0, "desc":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "<?xml version=\"1.0\"?>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1578
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1579
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1581
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    goto :goto_0

    .line 1571
    .end local v0    # "desc":Ljava/lang/String;
    .end local v1    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private getDescriptionURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 347
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getDescriptionURI()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;
    .locals 2

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 321
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    .line 322
    .local v1, "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;
    if-nez v1, :cond_0

    .line 323
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    .end local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;
    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;-><init>()V

    .line 324
    .restart local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 325
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 327
    :cond_0
    return-object v1
.end method

.method private getHTTPServerList()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;
    .locals 1

    .prologue
    .line 1979
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getHTTPServerList()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    move-result-object v0

    return-object v0
.end method

.method private getUUID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDevUUID:Ljava/lang/String;

    return-object v0
.end method

.method private httpGetRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 6
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 1650
    if-nez p1, :cond_0

    .line 1674
    :goto_0
    return-void

    .line 1652
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v3

    .line 1654
    .local v3, "uri":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 1655
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnNotFoundRequest()Z

    goto :goto_0

    .line 1659
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getLocalAddress()Ljava/lang/String;

    move-result-object v2

    .line 1661
    .local v2, "localAddr":Ljava/lang/String;
    invoke-direct {p0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->requestDescriptionData(Ljava/lang/String;Ljava/lang/String;)[B

    move-result-object v1

    .line 1663
    .local v1, "fileByte":[B
    if-nez v1, :cond_2

    .line 1664
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnNotFoundRequest()Z

    goto :goto_0

    .line 1668
    :cond_2
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->buildHTTPResponse(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v0

    .line 1669
    .local v0, "descResoponse":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    invoke-static {v3}, Lcom/samsung/android/allshare/stack/upnp/util/FileUtil;->isXMLFileName(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 1670
    const-string v4, "text/xml; charset=\"utf-8\""

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 1671
    :cond_3
    const/16 v4, 0xc8

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 1672
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContent([B)V

    .line 1673
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method private httpHostRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 3
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 1546
    if-nez p1, :cond_0

    .line 1560
    :goto_0
    return-void

    .line 1548
    :cond_0
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->buildHTTPResponse(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v1

    .line 1549
    .local v1, "httpRes":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeader()Ljava/lang/String;

    move-result-object v0

    .line 1550
    .local v0, "header":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1551
    const-string v2, "contentFeatures.dlna.org"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1552
    const/16 v2, 0x190

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 1558
    :cond_1
    invoke-virtual {p1, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method private httpPostRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 2
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 1684
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isSOAPAction()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1687
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->soapActionRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 1691
    :goto_0
    return-void

    .line 1690
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_0
.end method

.method private initializeLoadedDescription()Z
    .locals 1

    .prologue
    .line 387
    const-string v0, "/description.xml"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setDescriptionURI(Ljava/lang/String;)V

    .line 388
    const/16 v0, 0x708

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setLeaseTime(I)V

    .line 389
    const/16 v0, 0xfa4

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setHTTPPort(I)V

    .line 392
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->hasUDN()Z

    move-result v0

    if-nez v0, :cond_0

    .line 393
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->updateUDN()V

    .line 395
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method private invalidActionControlRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;)V
    .locals 2
    .param p1, "ctlReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;

    .prologue
    .line 1748
    if-nez p1, :cond_0

    .line 1753
    :goto_0
    return-void

    .line 1750
    :cond_0
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;-><init>()V

    .line 1751
    .local v0, "actRes":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;
    const/16 v1, 0x191

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->setFaultResponse(I)V

    .line 1752
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method private isDescriptionURI(Ljava/lang/String;)Z
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 351
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDescriptionURI()Ljava/lang/String;

    move-result-object v0

    .line 352
    .local v0, "descriptionURI":Ljava/lang/String;
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 353
    :cond_0
    const/4 v1, 0x0

    .line 354
    :goto_0
    return v1

    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public static isDeviceNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 403
    const-string v0, "device"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static final notifyWait()V
    .locals 1

    .prologue
    .line 1166
    const/16 v0, 0x64

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/util/TimerUtil;->waitRandom(I)V

    .line 1167
    return-void
.end method

.method private requestDescriptionData(Ljava/lang/String;Ljava/lang/String;)[B
    .locals 7
    .param p1, "localAddr"    # Ljava/lang/String;
    .param p2, "uri"    # Ljava/lang/String;

    .prologue
    .line 1618
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDescriptionCache:Ljava/util/LinkedHashMap;

    monitor-enter v5

    .line 1619
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDescriptionCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 1621
    .local v0, "buffer":[B
    if-eqz v0, :cond_0

    .line 1622
    monitor-exit v5

    move-object v1, v0

    .line 1636
    .end local v0    # "buffer":[B
    .local v1, "buffer":[B
    :goto_0
    return-object v1

    .line 1627
    .end local v1    # "buffer":[B
    .restart local v0    # "buffer":[B
    :cond_0
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isDescriptionURI(Ljava/lang/String;)Z

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_3

    .line 1628
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDescriptionData(Ljava/lang/String;)[B

    move-result-object v0

    .line 1634
    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 1635
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDescriptionCache:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, p2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1636
    :cond_2
    monitor-exit v5

    move-object v1, v0

    .end local v0    # "buffer":[B
    .restart local v1    # "buffer":[B
    goto :goto_0

    .line 1629
    .end local v1    # "buffer":[B
    .restart local v0    # "buffer":[B
    :cond_3
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceByDescriptionURI(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v2

    .local v2, "embDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v2, :cond_4

    .line 1630
    invoke-direct {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDescriptionData(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_1

    .line 1631
    :cond_4
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceBySCPDURL(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v3

    .local v3, "embService":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-eqz v3, :cond_1

    .line 1632
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSCPDData()[B

    move-result-object v0

    goto :goto_1

    .line 1638
    .end local v0    # "buffer":[B
    .end local v2    # "embDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v3    # "embService":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method private setAdvertiser(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;)V
    .locals 1
    .param p1, "adv"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    .prologue
    .line 1988
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setAdvertiser(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;)V

    .line 1989
    return-void
.end method

.method private setDescriptionFile(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setDescriptionFile(Ljava/io/File;)V

    .line 336
    return-void
.end method

.method private setDescriptionURI(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 343
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setDescriptionURI(Ljava/lang/String;)V

    .line 344
    return-void
.end method

.method private setUUID(Ljava/lang/String;)V
    .locals 0
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 273
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDevUUID:Ljava/lang/String;

    .line 274
    return-void
.end method

.method private soapActionRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 3
    .param p1, "soapReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 1712
    if-nez p1, :cond_0

    .line 1732
    :goto_0
    return-void

    .line 1714
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v2

    .line 1715
    .local v2, "uri":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceByControlURL(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v1

    .line 1716
    .local v1, "ctlService":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-eqz v1, :cond_1

    .line 1717
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 1718
    .local v0, "crlReq":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->deviceControlRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V

    goto :goto_0

    .line 1731
    .end local v0    # "crlReq":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->soapBadActionRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    goto :goto_0
.end method

.method private soapBadActionRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 2
    .param p1, "soapReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 1701
    if-nez p1, :cond_0

    .line 1706
    :goto_0
    return-void

    .line 1703
    :cond_0
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;-><init>()V

    .line 1704
    .local v0, "soapRes":Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;
    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setStatusCode(I)V

    .line 1705
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method private updateUDN()V
    .locals 2

    .prologue
    .line 281
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "uuid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUUID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setUDN(Ljava/lang/String;)V

    .line 282
    return-void
.end method

.method private upnpBadSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;I)V
    .locals 1
    .param p1, "subReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;
    .param p2, "code"    # I

    .prologue
    .line 1792
    if-nez p1, :cond_0

    .line 1797
    :goto_0
    return-void

    .line 1794
    :cond_0
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;-><init>()V

    .line 1795
    .local v0, "subRes":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->setErrorResponse(I)V

    .line 1796
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;)V

    goto :goto_0
.end method


# virtual methods
.method public announce()V
    .locals 11

    .prologue
    .line 1179
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPAnnounceCount()I

    move-result v7

    .line 1180
    .local v7, "ssdpCount":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_0
    if-ge v3, v7, :cond_1

    .line 1181
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->createNotifySendSocketList()[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;

    move-result-object v6

    .line 1182
    .local v6, "sockets":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    move-object v0, v6

    .local v0, "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v4, :cond_0

    aget-object v5, v0, v2

    .line 1183
    .local v5, "s":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->announce(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V

    .line 1184
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->close()Z

    .line 1188
    const-wide/16 v8, 0xc8

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1182
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1189
    :catch_0
    move-exception v1

    .line 1190
    .local v1, "e":Ljava/lang/InterruptedException;
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v9, "announce"

    const-string v10, "Got interruptedException : "

    invoke-static {v8, v9, v10, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 1180
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v5    # "s":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1195
    .end local v0    # "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .end local v2    # "i$":I
    .end local v4    # "len$":I
    .end local v6    # "sockets":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :cond_1
    return-void
.end method

.method public announce(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V
    .locals 16
    .param p1, "socket"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .param p2, "bindAddr"    # Ljava/net/InetAddress;

    .prologue
    .line 1236
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocationURL(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v5

    .line 1238
    .local v5, "devLocation":Ljava/lang/String;
    new-instance v13, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;

    const/4 v14, 0x0

    invoke-direct {v13, v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;-><init>(Z)V

    .line 1239
    .local v13, "ssdpReq":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getServerName()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setServer(Ljava/lang/String;)V

    .line 1240
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLeaseTime()I

    move-result v14

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setLeaseTime(I)V

    .line 1241
    invoke-virtual {v13, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setLocation(Ljava/lang/String;)V

    .line 1242
    const-string v14, "ssdp:alive"

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 1245
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v7

    .line 1246
    .local v7, "devUDN":Ljava/lang/String;
    invoke-virtual {v13, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1248
    invoke-virtual {v13, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1249
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1252
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v14

    const/4 v15, 0x1

    if-ne v14, v15, :cond_0

    .line 1253
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyDeviceNT()Ljava/lang/String;

    move-result-object v6

    .line 1254
    .local v6, "devNT":Ljava/lang/String;
    invoke-virtual {v13, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1255
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyRootDeviceUSN()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1256
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1260
    .end local v6    # "devNT":Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyDeviceTypeUSN()Ljava/lang/String;

    move-result-object v8

    .line 1261
    .local v8, "devUSN":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyDeviceTypeNT()Ljava/lang/String;

    move-result-object v6

    .line 1262
    .restart local v6    # "devNT":Ljava/lang/String;
    invoke-virtual {v13, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1263
    invoke-virtual {v13, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1264
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1267
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v12

    .line 1268
    .local v12, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v11

    .line 1269
    .local v11, "serviceCnt":I
    const/4 v9, 0x0

    .local v9, "n":I
    :goto_0
    if-ge v9, v11, :cond_1

    .line 1270
    invoke-virtual {v12, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v10

    .line 1271
    .local v10, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v10, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->announce(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V

    .line 1269
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1274
    .end local v10    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v4

    .line 1275
    .local v4, "childDeviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v3

    .line 1276
    .local v3, "childDeviceCnt":I
    const/4 v9, 0x0

    :goto_1
    if-ge v9, v3, :cond_2

    .line 1277
    invoke-virtual {v4, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v2

    .line 1278
    .local v2, "childDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->announce(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V

    .line 1276
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1280
    .end local v2    # "childDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_2
    return-void
.end method

.method public byebye()V
    .locals 11

    .prologue
    .line 1203
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->clearDescriptionCache()V

    .line 1205
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->notifyWait()V

    .line 1208
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPAnnounceCount()I

    move-result v7

    .line 1209
    .local v7, "ssdpCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_1

    .line 1210
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->createNotifySendSocketList()[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;

    move-result-object v6

    .line 1211
    .local v6, "sockets":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    move-object v0, v6

    .local v0, "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v5, v0, v3

    .line 1212
    .local v5, "s":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->getLocalAddress()Ljava/net/InetAddress;

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->byebye(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V

    .line 1213
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->close()Z

    .line 1217
    const-wide/16 v8, 0xc8

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1211
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1218
    :catch_0
    move-exception v1

    .line 1219
    .local v1, "e":Ljava/lang/InterruptedException;
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v9, "byebye"

    const-string v10, "Got interruptedException : "

    invoke-static {v8, v9, v10, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 1209
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v5    # "s":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1223
    .end local v0    # "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v6    # "sockets":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :cond_1
    return-void
.end method

.method public byebye(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V
    .locals 12
    .param p1, "sock"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .param p2, "bindAddr"    # Ljava/net/InetAddress;

    .prologue
    .line 1293
    new-instance v9, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;

    const/4 v10, 0x0

    invoke-direct {v9, v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;-><init>(Z)V

    .line 1294
    .local v9, "ssdpReq":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;
    const-string v10, "ssdp:byebye"

    invoke-virtual {v9, v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 1296
    const-string v4, ""

    .line 1297
    .local v4, "devUSN":Ljava/lang/String;
    const-string v3, ""

    .line 1300
    .local v3, "devNT":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    .line 1301
    const-string v3, "upnp:rootdevice"

    .line 1302
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyRootDeviceUSN()Ljava/lang/String;

    move-result-object v4

    .line 1303
    invoke-virtual {v9, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1304
    invoke-virtual {v9, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1305
    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1308
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v4

    .line 1309
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v3

    .line 1310
    invoke-virtual {v9, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1311
    invoke-virtual {v9, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1312
    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1315
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyDeviceTypeNT()Ljava/lang/String;

    move-result-object v3

    .line 1316
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getNotifyDeviceTypeUSN()Ljava/lang/String;

    move-result-object v4

    .line 1317
    invoke-virtual {v9, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 1318
    invoke-virtual {v9, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 1319
    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 1321
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v8

    .line 1322
    .local v8, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v7

    .line 1323
    .local v7, "serviceCnt":I
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_0
    if-ge v5, v7, :cond_1

    .line 1324
    invoke-virtual {v8, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v6

    .line 1325
    .local v6, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v6, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->byebye(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V

    .line 1323
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 1328
    .end local v6    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 1329
    .local v2, "childDeviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 1330
    .local v1, "childDeviceCnt":I
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v1, :cond_2

    .line 1331
    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 1332
    .local v0, "childDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->byebye(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;Ljava/net/InetAddress;)V

    .line 1330
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1334
    .end local v0    # "childDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_2
    return-void
.end method

.method public cleanActionMap()V
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->cleanActionMap()V

    .line 243
    return-void
.end method

.method public getASFFirmwareVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 635
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "asf:X_firmwareVersion"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getASFPortNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "asf:X_portNumber"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getASFProductType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 643
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "asf:X_productType"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAction(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .locals 14
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1068
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v11

    .line 1069
    .local v11, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v10

    .line 1070
    .local v10, "serviceCnt":I
    const/4 v8, 0x0

    .local v8, "n":I
    :goto_0
    if-ge v8, v10, :cond_4

    .line 1071
    invoke-virtual {v11, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v9

    .line 1072
    .local v9, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getActionList()Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;

    move-result-object v2

    .line 1073
    .local v2, "actionList":Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;->size()I

    move-result v1

    .line 1075
    .local v1, "actionCnt":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v1, :cond_3

    .line 1076
    invoke-virtual {v2, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;->getAction(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    move-result-object v0

    .line 1077
    .local v0, "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1078
    .local v3, "actionName":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 1075
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 1080
    :cond_1
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_0

    .line 1093
    .end local v0    # "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .end local v1    # "actionCnt":I
    .end local v2    # "actionList":Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;
    .end local v3    # "actionName":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v9    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_2
    :goto_2
    return-object v0

    .line 1070
    .restart local v1    # "actionCnt":I
    .restart local v2    # "actionList":Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;
    .restart local v7    # "i":I
    .restart local v9    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1084
    .end local v1    # "actionCnt":I
    .end local v2    # "actionList":Lcom/samsung/android/allshare/stack/upnp/upnp/ActionList;
    .end local v7    # "i":I
    .end local v9    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v6

    .line 1085
    .local v6, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v5

    .line 1086
    .local v5, "devCnt":I
    const/4 v8, 0x0

    :goto_3
    if-ge v8, v5, :cond_5

    .line 1087
    invoke-virtual {v6, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v4

    .line 1088
    .local v4, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v4, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getAction(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    move-result-object v0

    .line 1089
    .restart local v0    # "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    if-nez v0, :cond_2

    .line 1086
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1093
    .end local v0    # "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .end local v4    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public getActionFromMap(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .locals 1
    .param p1, "actionName"    # Ljava/lang/String;

    .prologue
    .line 238
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getActionFromMap(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    move-result-object v0

    return-object v0
.end method

.method public getBoundInterfaceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 454
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getBoundInterfaceName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionData()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1593
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 1594
    .local v0, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 1595
    const-string v2, ""

    .line 1605
    :goto_0
    return-object v2

    .line 1598
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1599
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "<?xml version=\"1.0\"?>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1605
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getDescriptionFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 339
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getDescriptionFile()Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public getDescriptionFilePath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDescriptionFile()Ljava/io/File;

    move-result-object v0

    .line 359
    .local v0, "descriptionFile":Ljava/io/File;
    if-nez v0, :cond_0

    .line 360
    const-string v1, ""

    .line 361
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 850
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v3

    .line 851
    .local v3, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v2

    .line 852
    .local v2, "devCnt":I
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-ge v4, v2, :cond_2

    .line 853
    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v1

    .line 855
    .local v1, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isDevice(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 861
    .end local v1    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :goto_1
    return-object v1

    .line 857
    .restart local v1    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_0
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 858
    .local v0, "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 859
    goto :goto_1

    .line 852
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 861
    .end local v0    # "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v1    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getDeviceByDescriptionURI(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 7
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 865
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v3

    .line 866
    .local v3, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v2

    .line 867
    .local v2, "devCnt":I
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-ge v4, v2, :cond_2

    .line 868
    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v1

    .line 869
    .local v1, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-direct {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isDescriptionURI(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 875
    .end local v1    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :goto_1
    return-object v1

    .line 871
    .restart local v1    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_0
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceByDescriptionURI(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 872
    .local v0, "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 873
    goto :goto_1

    .line 867
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 875
    .end local v0    # "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v1    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    .locals 11

    .prologue
    .line 815
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;-><init>()V

    .line 816
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v9

    const-string v10, "deviceList"

    invoke-virtual {v9, v10}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    .line 817
    .local v3, "devListNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v3, :cond_1

    .line 830
    :cond_0
    return-object v2

    .line 819
    :cond_1
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v7

    .line 820
    .local v7, "nNode":I
    const/4 v6, 0x0

    .local v6, "n":I
    :goto_0
    if-ge v6, v7, :cond_0

    .line 821
    invoke-virtual {v3, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v8

    .line 822
    .local v8, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-static {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isDeviceNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 820
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 824
    :cond_3
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-direct {v1, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 825
    .local v1, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    .line 826
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v4

    .line 827
    .local v4, "embList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 828
    .local v0, "d":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDeviceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getDeviceType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 653
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "deviceType"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDiscoveryInfos()[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->getDiscoveryInfo()[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-result-object v0

    return-object v0
.end method

.method public getElapsedTime()J
    .locals 4

    .prologue
    .line 517
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getTimeStamp()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public getForceExpired()Z
    .locals 2

    .prologue
    .line 531
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 532
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getIsForceExpired()Z

    move-result v1

    .line 539
    :goto_0
    return v1

    .line 534
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 535
    .local v0, "root":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_1

    .line 536
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getForceExpired()Z

    move-result v1

    goto :goto_0

    .line 539
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 680
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 681
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 682
    const-string v1, ""

    .line 683
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "friendlyName"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getHTTPPort()I
    .locals 1

    .prologue
    .line 1496
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getHTTPPort()I

    move-result v0

    return v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 466
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIcon(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;
    .locals 2
    .param p1, "n"    # I

    .prologue
    .line 1117
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getIconList()Lcom/samsung/android/allshare/stack/upnp/upnp/IconList;

    move-result-object v0

    .line 1118
    .local v0, "iconList":Lcom/samsung/android/allshare/stack/upnp/upnp/IconList;
    if-gez p1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/IconList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v1, p1, :cond_0

    .line 1119
    const/4 v1, 0x0

    .line 1120
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/IconList;->getIcon(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;

    move-result-object v1

    goto :goto_0
.end method

.method public getIconList()Lcom/samsung/android/allshare/stack/upnp/upnp/IconList;
    .locals 8

    .prologue
    .line 1101
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/IconList;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/IconList;-><init>()V

    .line 1102
    .local v1, "iconList":Lcom/samsung/android/allshare/stack/upnp/upnp/IconList;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v6

    const-string v7, "iconList"

    invoke-virtual {v6, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 1103
    .local v2, "iconListNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v2, :cond_1

    .line 1113
    :cond_0
    return-object v1

    .line 1105
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v4

    .line 1106
    .local v4, "nNode":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 1107
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    .line 1108
    .local v5, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;->isIconNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1106
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1110
    :cond_2
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;

    invoke-direct {v0, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 1111
    .local v0, "icon":Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/IconList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getLeaseTime()I
    .locals 1

    .prologue
    .line 502
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getLeaseTime()I

    move-result v0

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 2

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 483
    .local v0, "packet":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    if-eqz v0, :cond_0

    .line 484
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 485
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getLocation()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getLocationURL(Ljava/net/InetAddress;)Ljava/lang/String;
    .locals 4
    .param p1, "bindAddress"    # Ljava/net/InetAddress;

    .prologue
    .line 1128
    if-nez p1, :cond_0

    .line 1129
    const-string v2, ""

    .line 1137
    :goto_0
    return-object v2

    .line 1131
    :cond_0
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    .line 1133
    .local v0, "host":Ljava/lang/String;
    const/16 v2, 0x25

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 1134
    .local v1, "tailIdx":I
    if-lez v1, :cond_1

    .line 1135
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1137
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getHTTPPort()I

    move-result v2

    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDescriptionURI()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getHostURL(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public getManufacture()Ljava/lang/String;
    .locals 2

    .prologue
    .line 693
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "manufacturer"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getManufactureURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 703
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "manufactureURL"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModelDescription()Ljava/lang/String;
    .locals 2

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "modelDescription"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "modelName"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getModelNumber()Ljava/lang/String;
    .locals 2

    .prologue
    .line 747
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "modelNumber"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNotifyDeviceNT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1141
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1142
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v0

    .line 1143
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "upnp:rootdevice"

    goto :goto_0
.end method

.method protected getNotifyDeviceTypeNT()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1154
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNotifyDeviceTypeUSN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getNotifyRootDeviceUSN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "::"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "upnp:rootdevice"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRealExpired()Z
    .locals 2

    .prologue
    .line 566
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 567
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getIsRealExpired()Z

    move-result v1

    .line 573
    :goto_0
    return v1

    .line 569
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 570
    .local v0, "root":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_1

    .line 571
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRealExpired()Z

    move-result v1

    goto :goto_0

    .line 573
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 289
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 290
    .local v1, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v1, :cond_1

    .line 295
    :cond_0
    :goto_0
    return-object v2

    .line 292
    :cond_1
    const-string v3, "device"

    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 293
    .local v0, "devNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 295
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-direct {v2, v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    goto :goto_0
.end method

.method public getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 205
    :goto_0
    return-object v0

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDeviceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    if-nez v0, :cond_1

    .line 204
    const/4 v0, 0x0

    goto :goto_0

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDeviceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    goto :goto_0
.end method

.method public getSECDeviceID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 616
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "sec:deviceID"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSECProductCapability()Ljava/lang/String;
    .locals 2

    .prologue
    .line 606
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "sec:ProductCap"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSSDPAnnounceCount()I
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x2

    return v0
.end method

.method public getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    .locals 2

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v1

    if-nez v1, :cond_0

    .line 424
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 425
    .local v0, "root":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_0

    .line 426
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v1

    .line 428
    .end local v0    # "root":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v1

    goto :goto_0
.end method

.method public getService(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 9
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 899
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v6

    .line 900
    .local v6, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v5

    .line 901
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 902
    invoke-virtual {v6, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 903
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v4, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isService(Ljava/lang/String;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 916
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_0
    :goto_1
    return-object v4

    .line 901
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 907
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 908
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 909
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_3

    .line 910
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 911
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getService(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 912
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-nez v4, :cond_0

    .line 909
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 916
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getServiceByControlURL(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 9
    .param p1, "searchUrl"    # Ljava/lang/String;

    .prologue
    .line 941
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v6

    .line 942
    .local v6, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v5

    .line 943
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 944
    invoke-virtual {v6, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 945
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v4, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isControlURL(Ljava/lang/String;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 958
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_0
    :goto_1
    return-object v4

    .line 943
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 949
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 950
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 951
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_3

    .line 952
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 953
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceByControlURL(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 954
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-nez v4, :cond_0

    .line 951
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 958
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getServiceByEventSubURL(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 9
    .param p1, "searchUrl"    # Ljava/lang/String;

    .prologue
    .line 962
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v6

    .line 963
    .local v6, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v5

    .line 964
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 965
    invoke-virtual {v6, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 966
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v4, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isEventSubURL(Ljava/lang/String;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 979
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_0
    :goto_1
    return-object v4

    .line 964
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 970
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 971
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 972
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_3

    .line 973
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 974
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceByEventSubURL(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 975
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-nez v4, :cond_0

    .line 972
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 979
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getServiceBySCPDURL(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 9
    .param p1, "searchUrl"    # Ljava/lang/String;

    .prologue
    .line 920
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v6

    .line 921
    .local v6, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v5

    .line 922
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 923
    invoke-virtual {v6, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 924
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v4, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isSCPDURL(Ljava/lang/String;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 937
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_0
    :goto_1
    return-object v4

    .line 922
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 928
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 929
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 930
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_3

    .line 931
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 932
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceBySCPDURL(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 933
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-nez v4, :cond_0

    .line 930
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 937
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getServiceBySID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 9
    .param p1, "sid"    # Ljava/lang/String;

    .prologue
    .line 983
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v6

    .line 984
    .local v6, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v5

    .line 985
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 986
    invoke-virtual {v6, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 987
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 1000
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_0
    :goto_1
    return-object v4

    .line 985
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 991
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 992
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 993
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_3

    .line 994
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 995
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceBySID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 996
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-nez v4, :cond_0

    .line 993
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1000
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    .locals 8

    .prologue
    .line 883
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    invoke-direct {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;-><init>()V

    .line 884
    .local v4, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v6

    const-string v7, "serviceList"

    invoke-virtual {v6, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    .line 885
    .local v5, "serviceListNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v5, :cond_1

    .line 895
    :cond_0
    return-object v4

    .line 887
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v1

    .line 888
    .local v1, "nNode":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 889
    invoke-virtual {v5, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 890
    .local v2, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isServiceNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 888
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 892
    :cond_2
    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    invoke-direct {v3, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 893
    .local v3, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getStateVariable(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1060
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getStateVariable(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v0

    return-object v0
.end method

.method public getStateVariable(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    .locals 10
    .param p1, "serviceType"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 1030
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    move-object v7, v8

    .line 1056
    :cond_0
    :goto_0
    return-object v7

    .line 1033
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v6

    .line 1034
    .local v6, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v5

    .line 1035
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_1
    if-ge v3, v5, :cond_4

    .line 1036
    invoke-virtual {v6, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 1038
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-eqz p1, :cond_3

    .line 1039
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 1035
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1042
    :cond_3
    invoke-virtual {v4, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getStateVariable(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v7

    .line 1043
    .local v7, "stateVar":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    if-eqz v7, :cond_2

    goto :goto_0

    .line 1047
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .end local v7    # "stateVar":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 1048
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 1049
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_5

    .line 1050
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 1051
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getStateVariable(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v7

    .line 1052
    .restart local v7    # "stateVar":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    if-nez v7, :cond_0

    .line 1049
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v7    # "stateVar":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    :cond_5
    move-object v7, v8

    .line 1056
    goto :goto_0
.end method

.method public getSubscriberService(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 10
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 1004
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v6

    .line 1005
    .local v6, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v5

    .line 1006
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 1007
    invoke-virtual {v6, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 1008
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v7

    .line 1009
    .local v7, "sid":Ljava/lang/String;
    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 1022
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .end local v7    # "sid":Ljava/lang/String;
    :cond_0
    :goto_1
    return-object v4

    .line 1006
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .restart local v7    # "sid":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1013
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .end local v7    # "sid":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 1014
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 1015
    .local v1, "devCnt":I
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v1, :cond_3

    .line 1016
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 1017
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSubscriberService(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 1018
    .restart local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-nez v4, :cond_0

    .line 1015
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1022
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_3
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public getTimeStamp()J
    .locals 4

    .prologue
    .line 510
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 511
    .local v0, "packet":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getTimeStamp()J

    move-result-wide v2

    .line 513
    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getUDN()Ljava/lang/String;
    .locals 2

    .prologue
    .line 797
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 798
    .local v0, "deviceNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 799
    const-string v1, ""

    .line 800
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "UDN"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getURLBase()Ljava/lang/String;
    .locals 3

    .prologue
    .line 591
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 592
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 593
    .local v0, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 594
    const-string v1, "URLBase"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 596
    .end local v0    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public hasUDN()Z
    .locals 2

    .prologue
    .line 804
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getUDN()Ljava/lang/String;

    move-result-object v0

    .line 805
    .local v0, "udn":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 806
    :cond_0
    const/4 v1, 0x0

    .line 807
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public httpRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 3
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    const/4 v2, 0x1

    .line 1512
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isHeadRequest()Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 1513
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->httpHostRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 1533
    :goto_0
    return-void

    .line 1517
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isGetRequest()Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 1518
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->httpGetRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    goto :goto_0

    .line 1521
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isPostRequest()Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 1522
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->httpPostRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    goto :goto_0

    .line 1526
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isSubscribeRequest()Z

    move-result v1

    if-eq v1, v2, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isUnsubscribeRequest()Z

    move-result v1

    if-ne v1, v2, :cond_4

    .line 1527
    :cond_3
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;

    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 1528
    .local v0, "subReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->deviceEventSubscriptionRecieved(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;)V

    goto :goto_0

    .line 1532
    .end local v0    # "subReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_0
.end method

.method public httptRecieved(Ljava/lang/String;)V
    .locals 0
    .param p1, "firstLine"    # Ljava/lang/String;

    .prologue
    .line 1536
    return-void
.end method

.method public initSSDPSocket()V
    .locals 3

    .prologue
    .line 2000
    const/4 v0, 0x0

    .line 2002
    .local v0, "list":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    const/16 v2, 0x76c

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->getNotifyListenSocketList(I)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;

    move-result-object v0

    .line 2003
    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->setNotifiyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    .line 2004
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->start()V

    .line 2005
    return-void
.end method

.method public isDevice(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 834
    if-nez p1, :cond_1

    .line 840
    :cond_0
    :goto_0
    return v0

    .line 837
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    .line 838
    goto :goto_0
.end method

.method public isExpired()Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 543
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getForceExpired()Z

    move-result v5

    if-ne v5, v4, :cond_1

    .line 552
    :cond_0
    :goto_0
    return v4

    .line 546
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getElapsedTime()J

    move-result-wide v0

    .line 547
    .local v0, "elipsedTime":J
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLeaseTime()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    int-to-long v2, v5

    .line 549
    .local v2, "leaseTime":J
    cmp-long v5, v2, v0

    if-ltz v5, :cond_0

    .line 552
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isRealExpired()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 578
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRealExpired()Z

    move-result v1

    if-ne v1, v0, :cond_0

    .line 581
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRootDevice()Z
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public loadDescription(Ljava/io/InputStream;)Z
    .locals 4
    .param p1, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;
        }
    .end annotation

    .prologue
    .line 367
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v1

    .line 368
    .local v1, "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 369
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    if-nez v2, :cond_0

    .line 370
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;

    const-string v3, "Couldn\'t find a root node"

    invoke-direct {v2, v3, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    throw v2
    :try_end_0
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    .line 374
    .end local v1    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :catch_0
    move-exception v0

    .line 375
    .local v0, "e":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;

    invoke-direct {v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;-><init>(Ljava/lang/Exception;)V

    throw v2

    .line 371
    .end local v0    # "e":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    .restart local v1    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v3, "device"

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDeviceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 372
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mDeviceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    if-nez v2, :cond_1

    .line 373
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;

    const-string v3, "Couldn\'t find a root device node"

    invoke-direct {v2, v3, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;-><init>(Ljava/lang/String;Ljava/io/InputStream;)V

    throw v2
    :try_end_1
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_1 .. :try_end_1} :catch_0

    .line 378
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->initializeLoadedDescription()Z

    move-result v2

    if-nez v2, :cond_2

    .line 379
    const/4 v2, 0x0

    .line 383
    :goto_0
    return v2

    .line 381
    :cond_2
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setDescriptionFile(Ljava/io/File;)V

    .line 383
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onSSDPReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 4
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "ssdpPacket"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 1471
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1472
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$1;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$1;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1485
    :cond_0
    :goto_0
    return-void

    .line 1480
    :catch_0
    move-exception v0

    .line 1483
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mTag:Ljava/lang/String;

    const-string v2, "onSSDPReceived"

    const-string v3, "Got exception : "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public postSearchResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 12
    .param p1, "ssdpPacket"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    .param p2, "st"    # Ljava/lang/String;
    .param p3, "usn"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    .line 1350
    if-nez p1, :cond_1

    .line 1388
    :cond_0
    :goto_0
    return v10

    .line 1352
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v3

    .line 1353
    .local v3, "remoteAddr":Ljava/net/InetAddress;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v5

    .line 1354
    .local v5, "rootDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    const-string v6, ""

    .line 1356
    .local v6, "rootDevLocation":Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v1

    .line 1358
    .local v1, "localAddr":Ljava/net/InetAddress;
    if-eqz v5, :cond_0

    .line 1359
    invoke-virtual {v5, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocationURL(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v6

    .line 1364
    new-instance v8, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;

    invoke-direct {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;-><init>()V

    .line 1365
    .local v8, "ssdpRes":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLeaseTime()I

    move-result v11

    invoke-virtual {v8, v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->setLeaseTime(I)V

    .line 1366
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v11

    invoke-virtual {v8, v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->setDate(Ljava/util/Calendar;)V

    .line 1367
    invoke-virtual {v8, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->setST(Ljava/lang/String;)V

    .line 1368
    invoke-virtual {v8, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->setUSN(Ljava/lang/String;)V

    .line 1369
    invoke-virtual {v8, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->setLocation(Ljava/lang/String;)V

    .line 1373
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getMX()I

    move-result v2

    .line 1374
    .local v2, "mx":I
    mul-int/lit16 v11, v2, 0xc8

    invoke-static {v11}, Lcom/samsung/android/allshare/stack/upnp/util/TimerUtil;->waitRandom(I)V

    .line 1376
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getRemotePort()I

    move-result v4

    .line 1379
    .local v4, "remotePort":I
    new-instance v9, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;

    invoke-direct {v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;-><init>()V

    .line 1380
    .local v9, "ssdpResSock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    invoke-virtual {v9, v1, v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->open(Ljava/net/InetAddress;I)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1383
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;->print()V

    .line 1384
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPAnnounceCount()I

    move-result v7

    .line 1385
    .local v7, "ssdpCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v7, :cond_2

    .line 1386
    invoke-virtual {v9, v3, v4, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->post(Ljava/net/InetAddress;ILcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponse;)Z

    .line 1385
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1388
    :cond_2
    const/4 v10, 0x1

    goto :goto_0
.end method

.method public registerAction(Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V
    .locals 1
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->registerAction(Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 235
    return-void
.end method

.method public requestDisposer()V
    .locals 2

    .prologue
    .line 2177
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->getDisposer()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    move-result-object v0

    .line 2178
    .local v0, "disposer":Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;
    if-eqz v0, :cond_0

    .line 2179
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;->interrupted()V

    .line 2180
    :cond_0
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    .prologue
    .line 2125
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v3

    .line 2126
    .local v3, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v1

    .line 2127
    .local v1, "nServices":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 2128
    invoke-virtual {v3, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v2

    .line 2129
    .local v2, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    .line 2127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2131
    .end local v2    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_0
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;Z)V
    .locals 5
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;
    .param p2, "includeSubDevices"    # Z

    .prologue
    const/4 v4, 0x1

    .line 2148
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    .line 2149
    if-ne p2, v4, :cond_0

    .line 2150
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 2151
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 2152
    .local v1, "devCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 2153
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 2154
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;Z)V

    .line 2152
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2157
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    .end local v3    # "n":I
    :cond_0
    return-void
.end method

.method public setBoundInterfaceName(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 450
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setBoundInterfaceName(Ljava/lang/String;)V

    .line 451
    return-void
.end method

.method public setDeviceDisposer(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;)V
    .locals 1
    .param p1, "disposer"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    .prologue
    .line 2173
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setDisposer(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;)V

    .line 2174
    return-void
.end method

.method public setDiscoveryInfos([Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V
    .locals 1
    .param p1, "infos"    # [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->setDiscoveryInfo([Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    .line 260
    return-void
.end method

.method public setExpiredTime(I)V
    .locals 6
    .param p1, "time"    # I

    .prologue
    .line 521
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v0

    .line 522
    .local v0, "packet":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    if-eqz v0, :cond_0

    .line 523
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x1b7740

    sub-long/2addr v2, v4

    int-to-long v4, p1

    add-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->setTimeStamp(J)V

    .line 524
    :cond_0
    return-void
.end method

.method public setForceExpired(Z)V
    .locals 1
    .param p1, "isForceExpired"    # Z

    .prologue
    .line 527
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setIsForceExpired(Z)V

    .line 528
    return-void
.end method

.method public setFriendlyName(Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "needAnnounce"    # Z

    .prologue
    const/4 v1, 0x0

    .line 665
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x40

    if-ge v2, v3, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 676
    :cond_0
    :goto_0
    return v1

    .line 668
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 669
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 671
    const-string v1, "friendlyName"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    if-eqz p2, :cond_2

    .line 673
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->byebye()V

    .line 674
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->announce()V

    .line 676
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setHTTPPort(I)V
    .locals 1
    .param p1, "port"    # I

    .prologue
    .line 1492
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setHTTPPort(I)V

    .line 1493
    return-void
.end method

.method public setID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 462
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setID(Ljava/lang/String;)V

    .line 463
    return-void
.end method

.method public setLeaseTime(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 493
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setLeaseTime(I)V

    .line 494
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getAdvertiser()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    move-result-object v0

    .line 495
    .local v0, "adv":Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;
    if-eqz v0, :cond_0

    .line 496
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->announce()V

    .line 497
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->restart()V

    .line 499
    :cond_0
    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 478
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setLocation(Ljava/lang/String;)V

    .line 479
    return-void
.end method

.method public setModelDescription(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 713
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "modelDescription"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    return-void
.end method

.method public setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

    .prologue
    .line 2134
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v3

    .line 2135
    .local v3, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v1

    .line 2136
    .local v1, "nServices":I
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 2137
    invoke-virtual {v3, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v2

    .line 2138
    .local v2, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;)V

    .line 2136
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2140
    .end local v2    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_0
    return-void
.end method

.method public setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;Z)V
    .locals 5
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;
    .param p2, "includeSubDevices"    # Z

    .prologue
    const/4 v4, 0x1

    .line 2161
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;)V

    .line 2162
    if-ne p2, v4, :cond_0

    .line 2163
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 2164
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 2165
    .local v1, "devCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 2166
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 2167
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0, p1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;Z)V

    .line 2165
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2170
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v1    # "devCnt":I
    .end local v2    # "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    .end local v3    # "n":I
    :cond_0
    return-void
.end method

.method public setRealExpired(Z)V
    .locals 2
    .param p1, "isRealExpired"    # Z

    .prologue
    .line 556
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 557
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setIsRealExpired(Z)V

    .line 563
    :cond_0
    :goto_0
    return-void

    .line 559
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 560
    .local v0, "root":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_0

    .line 561
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setRealExpired(Z)V

    goto :goto_0
.end method

.method public setSSDPPacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 1
    .param p1, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 419
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/DeviceData;->setSSDPPacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    .line 420
    return-void
.end method

.method public setUDN(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 789
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    const-string v2, "UDN"

    invoke-virtual {v1, v2, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 792
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;Ljava/lang/String;)V

    .line 793
    .local v0, "deviceId":Lcom/samsung/android/allshare/stack/upnp/upnp/Device$GenerateDeviceID;
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 794
    return-void
.end method

.method public start()Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2008
    invoke-virtual {p0, v6, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->stop(ZZ)Z

    .line 2013
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 2015
    const/4 v3, 0x0

    .line 2016
    .local v3, "retryCnt":I
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getHTTPPort()I

    move-result v1

    .line 2017
    .local v1, "bindPort":I
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getHTTPServerList()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    move-result-object v2

    .line 2018
    .local v2, "httpServerList":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;
    :goto_0
    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->open(I)Z

    move-result v7

    if-nez v7, :cond_1

    .line 2019
    add-int/lit8 v3, v3, 0x1

    .line 2020
    const/16 v7, 0x64

    if-ge v7, v3, :cond_0

    .line 2052
    :goto_1
    return v5

    .line 2022
    :cond_0
    add-int/lit8 v7, v1, 0x1

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setHTTPPort(I)V

    .line 2023
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getHTTPPort()I

    move-result v1

    goto :goto_0

    .line 2026
    :cond_1
    invoke-virtual {v2, p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->addRequestListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;)V

    .line 2027
    invoke-virtual {v2, p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->addReceivedListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPReceivedListener;)V

    .line 2029
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->start()V

    .line 2031
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->initSSDPSocket()V

    .line 2036
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$2;

    invoke-direct {v5, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device$2;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 2042
    .local v4, "t":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 2048
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    .line 2049
    .local v0, "adv":Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ADVERTISER :"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getFriendlyName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->setName(Ljava/lang/String;)V

    .line 2050
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setAdvertiser(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;)V

    .line 2051
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->start()V

    move v5, v6

    .line 2052
    goto :goto_1
.end method

.method public stop(Z)Z
    .locals 5
    .param p1, "doByeBye"    # Z

    .prologue
    const/4 v4, 0x0

    .line 2057
    const/4 v2, 0x0

    .line 2060
    .local v2, "isHttpClosed":Z
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2061
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 2062
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 2067
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getHTTPServerList()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    move-result-object v1

    .line 2068
    .local v1, "httpServerList":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->stop()V

    .line 2069
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->close()Z

    move-result v2

    .line 2070
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->clear()V

    .line 2072
    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    .line 2073
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->byebye()V

    .line 2075
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->cleanup()V

    .line 2077
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getAdvertiser()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    move-result-object v0

    .line 2078
    .local v0, "adv":Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;
    if-eqz v0, :cond_2

    .line 2079
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->stop()V

    .line 2080
    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setAdvertiser(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;)V

    .line 2083
    :cond_2
    return v2
.end method

.method public stop(ZZ)Z
    .locals 5
    .param p1, "doByeBye"    # Z
    .param p2, "removeCache"    # Z

    .prologue
    const/4 v4, 0x0

    .line 2087
    const/4 v2, 0x0

    .line 2090
    .local v2, "isHttpClosed":Z
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->isShutdown()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2091
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 2092
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mExecutor:Ljava/util/concurrent/ExecutorService;

    .line 2097
    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getHTTPServerList()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    move-result-object v1

    .line 2098
    .local v1, "httpServerList":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->stop()V

    .line 2099
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->close()Z

    move-result v2

    .line 2100
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->clear()V

    .line 2102
    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    .line 2103
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->byebye()V

    .line 2105
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->cleanup()V

    .line 2107
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getAdvertiser()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;

    move-result-object v0

    .line 2108
    .local v0, "adv":Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;
    if-eqz v0, :cond_2

    .line 2109
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->stop()V

    .line 2110
    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setAdvertiser(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;)V

    .line 2113
    :cond_2
    return v2
.end method
