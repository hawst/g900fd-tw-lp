.class public Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShutterUpActionHandler;
.super Ljava/lang/Object;
.source "ShutterUpActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# instance fields
.field private mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V
    .locals 1
    .param p1, "cameraListener"    # Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShutterUpActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 25
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShutterUpActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 26
    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 1
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 31
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionRequest()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    move-result-object v0

    if-nez v0, :cond_1

    .line 32
    :cond_0
    const/4 v0, 0x0

    .line 35
    :goto_0
    return v0

    .line 34
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShutterUpActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onShutterUp()Z

    .line 35
    const/4 v0, 0x1

    goto :goto_0
.end method
