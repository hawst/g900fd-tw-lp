.class public Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;
.super Ljava/lang/Object;
.source "Icon.java"


# static fields
.field private static final DEPTH:Ljava/lang/String; = "depth"

.field public static final ELEM_NAME:Ljava/lang/String; = "icon"

.field private static final HEIGHT:Ljava/lang/String; = "height"

.field private static final MIME_TYPE:Ljava/lang/String; = "mimetype"

.field private static final URL:Ljava/lang/String; = "url"

.field private static final WIDTH:Ljava/lang/String; = "width"


# instance fields
.field private mIconNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;->mIconNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 52
    return-void
.end method

.method public static isIconNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 59
    const-string v0, "icon"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getDepth()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;->getIconNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "depth"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;->getIconNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "height"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIconNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;->mIconNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;->getIconNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "mimetype"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getURL()Ljava/lang/String;
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;->getIconNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "url"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWidth()Ljava/lang/String;
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Icon;->getIconNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "width"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
