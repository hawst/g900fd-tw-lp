.class Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TaskCheckExpiredDevice"
.end annotation


# instance fields
.field private mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

.field private mResult:Z

.field final synthetic this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    .locals 1
    .param p2, "device"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    .line 566
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 562
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 564
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mResult:Z

    .line 567
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 568
    return-void
.end method

.method private getDeviceDescription(Ljava/lang/String;)Z
    .locals 10
    .param p1, "location"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 594
    const/4 v2, 0x0

    .line 596
    .local v2, "in":Ljava/io/InputStream;
    :try_start_0
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 598
    .local v4, "url":Ljava/net/URL;
    sget-object v7, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    invoke-virtual {v4, v7}, Ljava/net/URL;->openConnection(Ljava/net/Proxy;)Ljava/net/URLConnection;

    move-result-object v5

    .line 601
    .local v5, "urlConnection":Ljava/net/URLConnection;
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getRefreshMode()Z
    invoke-static {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$400(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Z

    move-result v7

    if-ne v7, v6, :cond_1

    .line 602
    const/16 v7, 0x7d0

    invoke-virtual {v5, v7}, Ljava/net/URLConnection;->setConnectTimeout(I)V

    .line 606
    :goto_0
    invoke-virtual {v5}, Ljava/net/URLConnection;->connect()V

    .line 607
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-virtual {v5}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 608
    .end local v2    # "in":Ljava/io/InputStream;
    .local v3, "in":Ljava/io/InputStream;
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->read()I
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    .line 609
    .local v1, "i":I
    const/4 v7, -0x1

    if-eq v1, v7, :cond_3

    .line 616
    if-eqz v3, :cond_0

    .line 617
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_0
    move-object v2, v3

    .line 619
    .end local v1    # "i":I
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v4    # "url":Ljava/net/URL;
    .end local v5    # "urlConnection":Ljava/net/URLConnection;
    .restart local v2    # "in":Ljava/io/InputStream;
    :goto_1
    return v6

    .line 604
    .restart local v4    # "url":Ljava/net/URL;
    .restart local v5    # "urlConnection":Ljava/net/URLConnection;
    :cond_1
    const v7, 0x88b8

    :try_start_2
    invoke-virtual {v5, v7}, Ljava/net/URLConnection;->setConnectTimeout(I)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 612
    .end local v4    # "url":Ljava/net/URL;
    .end local v5    # "urlConnection":Ljava/net/URLConnection;
    :catch_0
    move-exception v0

    .line 613
    .local v0, "e":Ljava/net/MalformedURLException;
    :goto_2
    :try_start_3
    const-string v6, "ControlPoint"

    const-string v7, "getDeviceDescription"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getDeviceDescription MalformedURLException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 616
    if-eqz v2, :cond_2

    .line 617
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 619
    .end local v0    # "e":Ljava/net/MalformedURLException;
    :cond_2
    :goto_3
    const/4 v6, 0x0

    goto :goto_1

    .line 616
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v1    # "i":I
    .restart local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "url":Ljava/net/URL;
    .restart local v5    # "urlConnection":Ljava/net/URLConnection;
    :cond_3
    if-eqz v3, :cond_5

    .line 617
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 616
    .end local v1    # "i":I
    .end local v4    # "url":Ljava/net/URL;
    .end local v5    # "urlConnection":Ljava/net/URLConnection;
    :catchall_0
    move-exception v6

    :goto_4
    if-eqz v2, :cond_4

    .line 617
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_4
    throw v6

    .line 616
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    .restart local v4    # "url":Ljava/net/URL;
    .restart local v5    # "urlConnection":Ljava/net/URLConnection;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_4

    .line 612
    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v3    # "in":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_2

    .end local v2    # "in":Ljava/io/InputStream;
    .restart local v1    # "i":I
    .restart local v3    # "in":Ljava/io/InputStream;
    :cond_5
    move-object v2, v3

    .end local v3    # "in":Ljava/io/InputStream;
    .restart local v2    # "in":Ljava/io/InputStream;
    goto :goto_3
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 572
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    if-nez v2, :cond_0

    .line 590
    :goto_0
    return-void

    .line 575
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->getDeviceDescription(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mResult:Z

    .line 576
    iget-boolean v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mResult:Z

    if-ne v2, v5, :cond_1

    .line 577
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setRealExpired(Z)V

    .line 579
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    const v3, 0x1b7740

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setExpiredTime(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 583
    :catch_0
    move-exception v0

    .line 584
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "ControlPoint"

    const-string v3, "run"

    const-string v4, "TaskCheckExpiredDevice IOException"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 585
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setRealExpired(Z)V

    goto :goto_0

    .line 581
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setRealExpired(Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 586
    :catch_1
    move-exception v1

    .line 587
    .local v1, "e1":Ljava/lang/Exception;
    const-string v2, "ControlPoint"

    const-string v3, "run"

    const-string v4, "TaskCheckExpiredDevice Exception"

    invoke-static {v2, v3, v4, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 588
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setRealExpired(Z)V

    goto :goto_0
.end method
