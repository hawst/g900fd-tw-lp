.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;
.super Ljava/lang/Object;
.source "BrowseActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;
    }
.end annotation


# instance fields
.field private mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V
    .locals 1
    .param p1, "mediaServer"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 85
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 86
    return-void
.end method

.method private browseDirectChildrenActionReceived(Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;Ljava/net/InetAddress;)Z
    .locals 20
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 151
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getObjectID()Ljava/lang/String;

    move-result-object v11

    .line 153
    .local v11, "objID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v8

    .line 154
    .local v8, "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-nez v8, :cond_0

    .line 155
    const/16 v17, 0x2bd

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setStatus(I)V

    .line 156
    const/16 v17, 0x0

    .line 216
    :goto_0
    return v17

    .line 159
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 161
    .local v6, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getDirectories()Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .line 162
    .local v7, "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    instance-of v0, v7, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    move/from16 v17, v0

    if-nez v17, :cond_1

    .line 164
    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->toContentNode()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 167
    .end local v7    # "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    :cond_2
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getItems()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_3
    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .line 168
    .local v4, "c":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    instance-of v0, v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    move/from16 v17, v0

    if-nez v17, :cond_3

    .line 170
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 174
    .end local v4    # "c":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getSortCriteria()Ljava/lang/String;

    move-result-object v14

    .line 175
    .local v14, "sortCriteria":Ljava/lang/String;
    invoke-static {v6, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->sort(Ljava/util/List;Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_5

    .line 177
    const/16 v17, 0x2c5

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setStatus(I)V

    .line 178
    const/16 v17, 0x0

    goto :goto_0

    .line 181
    :cond_5
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v16

    .line 184
    .local v16, "totalMatch":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getStartingIndex()I

    move-result v15

    .line 185
    .local v15, "startingIndex":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getRequestedCount()I

    move-result v12

    .line 186
    .local v12, "requestCount":I
    invoke-static {v6, v15, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentListSpliter;->split(Ljava/util/List;II)Ljava/util/List;

    move-result-object v6

    .line 187
    if-nez v6, :cond_6

    .line 188
    const/16 v17, 0x192

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setStatus(I)V

    .line 189
    const/16 v17, 0x0

    goto :goto_0

    .line 193
    :cond_6
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .line 194
    .local v5, "content":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->update()V

    goto :goto_3

    .line 197
    .end local v5    # "content":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_7
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getFilter()Ljava/lang/String;

    move-result-object v9

    .line 198
    .local v9, "filterCondition":Ljava/lang/String;
    invoke-static {v6, v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/FilterApplier;->apply(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    .line 204
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v3

    .line 205
    .local v3, "addr":Ljava/net/InetAddress;
    const-string v13, ""

    .line 206
    .local v13, "result":Ljava/lang/String;
    if-eqz v3, :cond_8

    .line 207
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getHTTPPort()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    move-object/from16 v18, v0

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getStreamingPort(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p2

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-static {v6, v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter;->print(Ljava/util/Collection;Ljava/net/InetAddress;II)Ljava/lang/String;

    move-result-object v13

    .line 212
    :cond_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setResult(Ljava/lang/String;)V

    .line 213
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setNumberReturned(I)V

    .line 214
    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setTotalMaches(I)V

    .line 215
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getSystemUpdateID()I

    move-result v17

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setUpdateID(I)V

    .line 216
    const/16 v17, 0x1

    goto/16 :goto_0
.end method

.method private browseMetadataActionReceived(Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;Ljava/net/InetAddress;)Z
    .locals 9
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 106
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getObjectID()Ljava/lang/String;

    move-result-object v3

    .line 110
    .local v3, "objID":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v7, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->findObjectByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v2

    .line 111
    .local v2, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    if-eqz v2, :cond_0

    instance-of v7, v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    if-eqz v7, :cond_1

    .line 112
    :cond_0
    const/16 v6, 0x2bd

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setStatus(I)V

    .line 141
    :goto_0
    return v5

    .line 116
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->update()V

    .line 119
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getFilter()Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "filterCondition":Ljava/lang/String;
    invoke-static {v2, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/FilterApplier;->apply(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v2

    .line 122
    if-nez v2, :cond_2

    .line 123
    const/16 v6, 0x192

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setStatus(I)V

    goto :goto_0

    .line 128
    :cond_2
    invoke-static {p2}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v0

    .line 129
    .local v0, "addr":Ljava/net/InetAddress;
    const-string v4, ""

    .line 130
    .local v4, "result":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 131
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getHTTPPort()I

    move-result v5

    iget-object v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getStreamingPort(Ljava/lang/String;)I

    move-result v7

    invoke-static {v2, p2, v5, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter;->print(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;Ljava/net/InetAddress;II)Ljava/lang/String;

    move-result-object v4

    .line 136
    :cond_3
    invoke-virtual {p1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setResult(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setNumberReturned(I)V

    .line 138
    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setTotalMaches(I)V

    .line 139
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getSystemUpdateID()I

    move-result v5

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->setUpdateID(I)V

    move v5, v6

    .line 141
    goto :goto_0
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 3
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/4 v2, 0x1

    .line 90
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;

    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 92
    .local v0, "browse":Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->isMetadata()Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 93
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->browseMetadataActionReceived(Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;Ljava/net/InetAddress;)Z

    move-result v1

    .line 98
    :goto_0
    return v1

    .line 94
    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->isDirectChildren()Z

    move-result v1

    if-ne v1, v2, :cond_1

    .line 95
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;->browseDirectChildrenActionReceived(Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;Ljava/net/InetAddress;)Z

    move-result v1

    goto :goto_0

    .line 97
    :cond_1
    const/16 v1, 0x192

    invoke-virtual {p1, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    .line 98
    const/4 v1, 0x0

    goto :goto_0
.end method
