.class public Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;
.source "StateVariable.java"


# static fields
.field private static final DATATYPE:Ljava/lang/String; = "dataType"

.field public static final ELEM_NAME:Ljava/lang/String; = "stateVariable"

.field private static final NAME:Ljava/lang/String; = "name"

.field private static final SENDEVENTS:Ljava/lang/String; = "sendEvents"

.field private static final SENDEVENTS_NO:Ljava/lang/String; = "no"

.field private static final SENDEVENTS_YES:Ljava/lang/String; = "yes"


# instance fields
.field private mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private mStateVariableNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;-><init>()V

    .line 349
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 101
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mStateVariableNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 102
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 1
    .param p1, "serviceNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .param p2, "stateVarNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/NodeData;-><init>()V

    .line 349
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .line 105
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 106
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mStateVariableNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 107
    return-void
.end method

.method public static isStateVariableNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 114
    const-string v0, "stateVariable"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getAllowedValueList()Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;
    .locals 8

    .prologue
    .line 235
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;

    invoke-direct {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;-><init>()V

    .line 236
    .local v4, "valueList":Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v6

    const-string v7, "allowedValueList"

    invoke-virtual {v6, v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    .line 237
    .local v5, "valueListNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v5, :cond_1

    .line 248
    :cond_0
    return-object v4

    .line 240
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v2

    .line 241
    .local v2, "nNode":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 242
    invoke-virtual {v5, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    .line 243
    .local v3, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-static {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;->isAllowedValueNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 241
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 245
    :cond_2
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;

    invoke-direct {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 246
    .local v0, "allowedVal":Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;
    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getAllowedValueRange()Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;
    .locals 3

    .prologue
    .line 261
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    const-string v2, "allowedValueRange"

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 262
    .local v0, "valueRangeNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 263
    const/4 v1, 0x0

    .line 264
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;

    invoke-direct {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    goto :goto_0
.end method

.method public getDataType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 142
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "dataType"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQueryListener()Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;
    .locals 1

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->getQueryListener()Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

    move-result-object v0

    return-object v0
.end method

.method public getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 86
    .local v0, "serviceNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 87
    const/4 v1, 0x0

    .line 88
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    invoke-direct {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    goto :goto_0
.end method

.method public getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getStateVariableData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 186
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;

    .line 187
    .local v1, "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;
    if-nez v1, :cond_0

    .line 188
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;

    .end local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;
    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;-><init>()V

    .line 189
    .restart local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 190
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->setNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 192
    :cond_0
    return-object v1
.end method

.method public getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mStateVariableNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasAllowedValueList()Z
    .locals 2

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getAllowedValueList()Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;

    move-result-object v0

    .line 253
    .local v0, "valueList":Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasAllowedValueRange()Z
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getAllowedValueRange()Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSendEvents()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 161
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    const-string v4, "sendEvents"

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "state":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v1

    .line 164
    :cond_1
    const-string v3, "yes"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-ne v3, v2, :cond_0

    move v1, v2

    .line 165
    goto :goto_0
.end method

.method public performQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;)Z
    .locals 7
    .param p1, "queryReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;

    .prologue
    const/4 v4, 0x1

    .line 284
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getQueryListener()Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

    move-result-object v0

    .line 285
    .local v0, "listener":Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;
    if-nez v0, :cond_0

    .line 286
    const/4 v4, 0x0

    .line 299
    :goto_0
    return v4

    .line 287
    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;-><init>()V

    .line 288
    .local v1, "queryRes":Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;-><init>()V

    .line 289
    .local v2, "retVar":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    invoke-virtual {v2, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->set(Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;)V

    .line 290
    const-string v5, ""

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    .line 291
    const/16 v5, 0x194

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setStatus(I)V

    .line 292
    invoke-interface {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;->queryControlReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;)Z

    move-result v5

    if-ne v5, v4, :cond_1

    .line 293
    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;->setResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;)V

    .line 298
    :goto_1
    invoke-virtual {p1, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    goto :goto_0

    .line 295
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    move-result-object v3

    .line 296
    .local v3, "upnpStatus":Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->getCode()I

    move-result v5

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/QueryResponse;->setFaultResponse(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public set(Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;)V
    .locals 1
    .param p1, "stateVar"    # Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    .prologue
    .line 174
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setName(Ljava/lang/String;)V

    .line 175
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    .line 176
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getDataType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setDataType(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->isSendEvents()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setSendEvents(Z)V

    .line 178
    return-void
.end method

.method public setDataType(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "dataType"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method public setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->setQueryListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IQueryListener;)V

    .line 281
    return-void
.end method

.method public setSendEvents(Z)V
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 156
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    const-string v2, "sendEvents"

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const-string v0, "yes"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    return-void

    .line 156
    :cond_0
    const-string v0, "no"

    goto :goto_0
.end method

.method public setStatus(I)V
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 357
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setStatus(ILjava/lang/String;)V

    .line 358
    return-void
.end method

.method public setStatus(ILjava/lang/String;)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "descr"    # Ljava/lang/String;

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->setCode(I)V

    .line 353
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->setDescription(Ljava/lang/String;)V

    .line 354
    return-void
.end method

.method public setValue(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 218
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    .line 219
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->getValue()Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "currValue":Ljava/lang/String;
    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getStateVariableData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/StateVariableData;->setValue(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v1

    .line 210
    .local v1, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-eqz v1, :cond_0

    .line 212
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->isSendEvents()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 214
    invoke-virtual {v1, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->notifyState(Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;)V

    goto :goto_0
.end method
