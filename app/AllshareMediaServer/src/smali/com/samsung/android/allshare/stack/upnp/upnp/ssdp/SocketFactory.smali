.class public final Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;
.super Ljava/lang/Object;
.source "SocketFactory.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SocketFactory"


# instance fields
.field private mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

.field private mSSDPListenSocketPool:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mSSDPListenSocketPool:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public declared-synchronized cleanup()V
    .locals 6

    .prologue
    .line 226
    monitor-enter p0

    :try_start_0
    const-string v3, "SocketFactory"

    const-string v4, "cleanup"

    const-string v5, "Do cleanup()..."

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mSSDPListenSocketPool:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 228
    .local v0, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;>;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 229
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 230
    .local v1, "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;

    .line 231
    .local v2, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    if-eqz v2, :cond_0

    .line 232
    const-string v3, "SocketFactory"

    const-string v4, "cleanup"

    const-string v5, "Call: NotifyListenSocket.stop()"

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->stop()V

    .line 235
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 226
    .end local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;>;>;"
    .end local v1    # "ref":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;>;"
    .end local v2    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 239
    .restart local v0    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/ref/WeakReference<Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;>;>;"
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized closeSSDPListenSockets()V
    .locals 3

    .prologue
    .line 254
    monitor-enter p0

    :try_start_0
    const-string v0, "SocketFactory"

    const-string v1, "closeSSDPListenSockets"

    const-string v2, "Call: SocketFactory.cleanup()"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->cleanup()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 256
    monitor-exit p0

    return-void

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized createNotifySendSocketList()[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .locals 23

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 96
    .local v17, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-object/from16 v19, v0

    if-nez v19, :cond_0

    .line 97
    const-string v19, "SocketFactory"

    const-string v20, "createNotifySendSocketList"

    const-string v21, "DiscoveryInfo is not set correctly"

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v19

    check-cast v19, [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    :goto_0
    monitor-exit p0

    return-object v19

    .line 101
    :cond_0
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .local v2, "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    array-length v11, v2

    .local v11, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    move v7, v5

    .end local v2    # "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .end local v5    # "i$":I
    .end local v11    # "len$":I
    .local v7, "i$":I
    :goto_1
    if-ge v7, v11, :cond_8

    aget-object v8, v2, v7

    .line 102
    .local v8, "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    const/4 v14, 0x0

    .line 103
    .local v14, "ni":Ljava/net/NetworkInterface;
    const/16 v16, 0x0

    .line 105
    .local v16, "pairs":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getNetworkInterfaceName()Ljava/lang/String;

    move-result-object v13

    .line 106
    .local v13, "name":Ljava/lang/String;
    const-string v19, "*"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 107
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAllAddressNetworkInterfacePair()Ljava/util/Collection;

    move-result-object v16

    .line 118
    :goto_2
    invoke-interface/range {v16 .. v16}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .end local v7    # "i$":I
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/util/Pair;

    .line 120
    .local v15, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    iget-object v0, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Ljava/net/InetAddress;

    invoke-virtual/range {v19 .. v19}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v19

    if-eqz v19, :cond_2

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->supportLoopback()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 123
    :cond_2
    iget-object v0, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Ljava/net/InetAddress;

    invoke-virtual/range {v19 .. v19}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v19

    if-eqz v19, :cond_3

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->supportLinkLocal()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 126
    :cond_3
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getMulticastAddressList()[Ljava/lang/String;

    move-result-object v3

    .local v3, "arr$":[Ljava/lang/String;
    array-length v12, v3

    .local v12, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_3
    if-ge v6, v12, :cond_1

    aget-object v10, v3, v6

    .line 127
    .local v10, "joinAddr":Ljava/lang/String;
    const/16 v20, 0x76c

    iget-object v0, v15, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Ljava/net/InetAddress;

    move/from16 v0, v20

    move-object/from16 v1, v19

    invoke-static {v10, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->createSSDPNotifySocket(Ljava/lang/String;ILjava/net/InetAddress;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;

    move-result-object v18

    .line 129
    .local v18, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    if-eqz v18, :cond_5

    .line 130
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Ljava/net/NetworkInterface;

    invoke-virtual/range {v19 .. v19}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v9

    .line 132
    .local v9, "interfaceName":Ljava/lang/String;
    const-string v19, "wlan"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_4

    const-string v19, "p2p"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-nez v19, :cond_4

    const-string v19, "eth"

    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 134
    :cond_4
    const-string v19, "SocketFactory"

    const-string v20, "createNotifySendSocketList"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "success:"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v19 .. v21}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->setDiscoveryInfo(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    .line 136
    iget-object v0, v15, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Ljava/net/NetworkInterface;

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    .line 137
    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    .end local v9    # "interfaceName":Ljava/lang/String;
    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 110
    .end local v3    # "arr$":[Ljava/lang/String;
    .end local v6    # "i$":I
    .end local v10    # "joinAddr":Ljava/lang/String;
    .end local v12    # "len$":I
    .end local v15    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    .end local v18    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .restart local v7    # "i$":I
    :cond_6
    :try_start_2
    invoke-static {v13}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v14

    .line 114
    :try_start_3
    invoke-static {v14}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAddressByNIC(Ljava/net/NetworkInterface;)Ljava/util/Collection;

    move-result-object v16

    goto/16 :goto_2

    .line 111
    :catch_0
    move-exception v4

    .line 101
    .end local v7    # "i$":I
    :cond_7
    add-int/lit8 v5, v7, 0x1

    .restart local v5    # "i$":I
    move v7, v5

    .end local v5    # "i$":I
    .restart local v7    # "i$":I
    goto/16 :goto_1

    .line 144
    .end local v8    # "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .end local v13    # "name":Ljava/lang/String;
    .end local v14    # "ni":Ljava/net/NetworkInterface;
    .end local v16    # "pairs":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    :cond_8
    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;

    move-object/from16 v19, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v19

    check-cast v19, [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 94
    .end local v7    # "i$":I
    .end local v17    # "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;>;"
    :catchall_0
    move-exception v19

    monitor-exit p0

    throw v19
.end method

.method public declared-synchronized createSearchSendSocketList()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;
    .locals 17

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    new-instance v8, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;

    invoke-direct {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;-><init>()V

    .line 50
    .local v8, "list":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    if-nez v14, :cond_1

    .line 51
    const-string v14, "SocketFactory"

    const-string v15, "SSDPSearchResponseSocketList"

    const-string v16, "DiscoveryInfo is not set correctly"

    invoke-static/range {v14 .. v16}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :cond_0
    monitor-exit p0

    return-object v8

    .line 55
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .local v2, "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    array-length v7, v2

    .local v7, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v5, v4

    .end local v4    # "i$":I
    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v6, v2, v5

    .line 56
    .local v6, "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    const/4 v10, 0x0

    .line 58
    .local v10, "ni":Ljava/net/NetworkInterface;
    const/4 v12, 0x0

    .line 59
    .local v12, "pairs":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getNetworkInterfaceName()Ljava/lang/String;

    move-result-object v9

    .line 60
    .local v9, "name":Ljava/lang/String;
    const-string v14, "*"

    invoke-virtual {v9, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 61
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAllAddressNetworkInterfacePair()Ljava/util/Collection;

    move-result-object v12

    .line 72
    :goto_1
    invoke-interface {v12}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .end local v5    # "i$":I
    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/util/Pair;

    .line 73
    .local v11, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    iget-object v1, v11, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/net/InetAddress;

    .line 75
    .local v1, "addr":Ljava/net/InetAddress;
    invoke-virtual {v1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->supportLoopback()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 78
    :cond_3
    invoke-virtual {v1}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v14

    if-eqz v14, :cond_4

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->supportLinkLocal()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 81
    :cond_4
    new-instance v13, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;

    invoke-direct {v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;-><init>()V

    .line 82
    .local v13, "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    const/4 v14, 0x0

    invoke-virtual {v13, v1, v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->open(Ljava/net/InetAddress;I)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 83
    iget-object v14, v11, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v14, Ljava/net/NetworkInterface;

    invoke-virtual {v13, v14}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    .line 84
    invoke-virtual {v13, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;->setDiscoveryInfo(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    .line 85
    invoke-virtual {v8, v13}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 48
    .end local v1    # "addr":Ljava/net/InetAddress;
    .end local v2    # "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .end local v7    # "len$":I
    .end local v8    # "list":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;
    .end local v9    # "name":Ljava/lang/String;
    .end local v10    # "ni":Ljava/net/NetworkInterface;
    .end local v11    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    .end local v12    # "pairs":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    .end local v13    # "sock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocket;
    :catchall_0
    move-exception v14

    monitor-exit p0

    throw v14

    .line 64
    .restart local v2    # "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .restart local v5    # "i$":I
    .restart local v6    # "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .restart local v7    # "len$":I
    .restart local v8    # "list":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;
    .restart local v9    # "name":Ljava/lang/String;
    .restart local v10    # "ni":Ljava/net/NetworkInterface;
    .restart local v12    # "pairs":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    :cond_5
    :try_start_2
    invoke-static {v9}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;
    :try_end_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v10

    .line 68
    :try_start_3
    invoke-static {v10}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAddressByNIC(Ljava/net/NetworkInterface;)Ljava/util/Collection;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v12

    goto :goto_1

    .line 65
    :catch_0
    move-exception v3

    .line 55
    .end local v5    # "i$":I
    :cond_6
    add-int/lit8 v4, v5, 0x1

    .local v4, "i$":I
    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto :goto_0
.end method

.method public declared-synchronized getDiscoveryInfo()[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .locals 1

    .prologue
    .line 44
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getNotifyListenSocketList(I)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
    .locals 24
    .param p1, "port"    # I

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    new-instance v19, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;

    invoke-direct/range {v19 .. v19}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;-><init>()V

    .line 161
    .local v19, "result":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-object/from16 v21, v0

    if-nez v21, :cond_1

    .line 162
    const-string v21, "SocketFactory"

    const-string v22, "getNotifyListenSocketList"

    const-string v23, "DiscoveryInfo is not set correctly"

    invoke-static/range {v21 .. v23}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    :cond_0
    monitor-exit p0

    return-object v19

    .line 166
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .local v4, "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    array-length v13, v4

    .local v13, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    move v11, v9

    .end local v4    # "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .end local v9    # "i$":I
    .end local v13    # "len$":I
    .local v11, "i$":I
    :goto_0
    if-ge v11, v13, :cond_0

    aget-object v12, v4, v11

    .line 167
    .local v12, "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    const/16 v18, 0x0

    .line 168
    .local v18, "pairs":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getNetworkInterfaceName()Ljava/lang/String;

    move-result-object v15

    .line 169
    .local v15, "name":Ljava/lang/String;
    const-string v21, "*"

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 170
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAllAddressNetworkInterfacePair()Ljava/util/Collection;

    move-result-object v18

    .line 182
    :goto_1
    invoke-interface/range {v18 .. v18}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .end local v11    # "i$":I
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_9

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/util/Pair;

    .line 183
    .local v17, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    move-object/from16 v0, v17

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/net/InetAddress;

    .line 187
    .local v3, "addr":Ljava/net/InetAddress;
    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v21

    if-eqz v21, :cond_3

    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->supportLoopback()Z

    move-result v21

    if-eqz v21, :cond_2

    .line 190
    :cond_3
    invoke-virtual {v3}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v21

    if-eqz v21, :cond_4

    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->supportLinkLocal()Z

    move-result v21

    if-eqz v21, :cond_2

    .line 193
    :cond_4
    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getMulticastAddressList()[Ljava/lang/String;

    move-result-object v5

    .local v5, "arr$":[Ljava/lang/String;
    array-length v14, v5

    .local v14, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_2
    if-ge v10, v14, :cond_2

    aget-object v8, v5, v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    .local v8, "groupAddrString":Ljava/lang/String;
    :try_start_2
    invoke-static {v8}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v7

    .line 197
    .local v7, "groupAddr":Ljava/net/InetAddress;
    if-eqz v7, :cond_7

    invoke-virtual {v7}, Ljava/net/InetAddress;->isMulticastAddress()Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_7

    .line 199
    instance-of v0, v3, Ljava/net/Inet4Address;

    move/from16 v21, v0

    if-eqz v21, :cond_5

    instance-of v0, v7, Ljava/net/Inet4Address;

    move/from16 v21, v0

    if-nez v21, :cond_6

    :cond_5
    instance-of v0, v3, Ljava/net/Inet6Address;

    move/from16 v21, v0

    if-eqz v21, :cond_7

    instance-of v0, v7, Ljava/net/Inet6Address;

    move/from16 v21, v0

    if-eqz v21, :cond_7

    .line 201
    :cond_6
    new-instance v20, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;

    invoke-direct/range {v20 .. v20}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;-><init>()V

    .line 203
    .local v20, "socket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    invoke-virtual {v7}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    move/from16 v2, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->open(Ljava/lang/String;ILjava/net/InetAddress;)Z

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_7

    .line 204
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->setDiscoveryInfo(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    .line 205
    move-object/from16 v0, v17

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v21, v0

    check-cast v21, Ljava/net/NetworkInterface;

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    .line 206
    invoke-virtual/range {v19 .. v20}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->add(Ljava/lang/Object;)Z

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mSSDPListenSocketPool:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    new-instance v22, Ljava/lang/ref/WeakReference;

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 193
    .end local v7    # "groupAddr":Ljava/net/InetAddress;
    .end local v20    # "socket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    :cond_7
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 174
    .end local v3    # "addr":Ljava/net/InetAddress;
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v8    # "groupAddrString":Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v14    # "len$":I
    .end local v17    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    .restart local v11    # "i$":I
    :cond_8
    :try_start_3
    invoke-static {v15}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;
    :try_end_3
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v16

    .line 178
    .local v16, "ni":Ljava/net/NetworkInterface;
    :try_start_4
    invoke-static/range {v16 .. v16}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAddressByNIC(Ljava/net/NetworkInterface;)Ljava/util/Collection;

    move-result-object v18

    goto/16 :goto_1

    .line 175
    .end local v16    # "ni":Ljava/net/NetworkInterface;
    :catch_0
    move-exception v6

    .line 166
    .end local v11    # "i$":I
    :cond_9
    add-int/lit8 v9, v11, 0x1

    .restart local v9    # "i$":I
    move v11, v9

    .end local v9    # "i$":I
    .restart local v11    # "i$":I
    goto/16 :goto_0

    .line 216
    .end local v11    # "i$":I
    .restart local v3    # "addr":Ljava/net/InetAddress;
    .restart local v5    # "arr$":[Ljava/lang/String;
    .restart local v8    # "groupAddrString":Ljava/lang/String;
    .restart local v10    # "i$":I
    .restart local v14    # "len$":I
    .restart local v17    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    :catch_1
    move-exception v6

    .line 217
    .local v6, "e":Ljava/net/UnknownHostException;
    const-string v21, "SocketFactory"

    const-string v22, "getNotifyListenSocketList"

    const-string v23, "UnknownHostException - "

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    .line 159
    .end local v3    # "addr":Ljava/net/InetAddress;
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v6    # "e":Ljava/net/UnknownHostException;
    .end local v8    # "groupAddrString":Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v12    # "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .end local v14    # "len$":I
    .end local v15    # "name":Ljava/lang/String;
    .end local v17    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    .end local v18    # "pairs":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    .end local v19    # "result":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
    :catchall_0
    move-exception v21

    monitor-exit p0

    throw v21
.end method

.method public declared-synchronized openSSDPListenSockets(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .prologue
    .line 243
    monitor-enter p0

    const/16 v3, 0x76c

    :try_start_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->getNotifyListenSocketList(I)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;

    move-result-object v1

    .line 244
    .local v1, "notifyListenSocketList":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->setNotifiyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    .line 245
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->start()V

    .line 247
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;

    .line 248
    .local v2, "ns":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 243
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "notifyListenSocketList":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
    .end local v2    # "ns":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocket;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 250
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "notifyListenSocketList":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized setDiscoveryInfo([Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V
    .locals 1
    .param p1, "discoveryInfoList"    # [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .prologue
    .line 40
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->mDiscoveryInfoList:[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 41
    monitor-exit p0

    return-void

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
