.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
.super Ljava/lang/Object;
.source "ResourceProtocolParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$1;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
    }
.end annotation


# static fields
.field public static final AAC_ADTS_320:Ljava/lang/String; = "AAC_ADTS_320"

.field public static final AAC_ISO_320:Ljava/lang/String; = "AAC_ISO_320"

.field public static final AVC_MP4_BL_CIF15_AAC_520:Ljava/lang/String; = "AVC_MP4_BL_CIF15_AAC_520"

.field public static final FILE_EXTENSION_3GP:Ljava/lang/String; = ".3gp"

.field public static final FILE_EXTENSION_3GPP:Ljava/lang/String; = ".3gpp"

.field public static final FILE_EXTENSION_AAC:Ljava/lang/String; = ".aac"

.field public static final FILE_EXTENSION_ADTS:Ljava/lang/String; = ".adts"

.field public static final FILE_EXTENSION_ASF:Ljava/lang/String; = ".asf"

.field public static final FILE_EXTENSION_AVI:Ljava/lang/String; = ".avi"

.field public static final FILE_EXTENSION_JPEG:Ljava/lang/String; = ".jpeg"

.field public static final FILE_EXTENSION_LPCM:Ljava/lang/String; = ".lpcm"

.field public static final FILE_EXTENSION_MP3:Ljava/lang/String; = ".mp3"

.field public static final FILE_EXTENSION_MP4:Ljava/lang/String; = ".mp4"

.field public static final FILE_EXTENSION_MPEG:Ljava/lang/String; = ".mpeg"

.field public static final FILE_EXTENSION_PNG:Ljava/lang/String; = ".png"

.field public static final FILE_EXTENSION_RAW:Ljava/lang/String; = ".raw"

.field public static final FILE_EXTENSION_WMA:Ljava/lang/String; = ".wma"

.field public static final FILE_EXTENSION_WMV:Ljava/lang/String; = ".wmv"

.field private static final HTTP_GET:Ljava/lang/String; = "http-get"

.field public static final JPEG_LRG:Ljava/lang/String; = "JPEG_LRG"

.field public static final JPEG_MED:Ljava/lang/String; = "JPEG_MED"

.field public static final JPEG_SM:Ljava/lang/String; = "JPEG_SM"

.field public static final JPEG_TN:Ljava/lang/String; = "JPEG_TN"

.field public static final LPCM:Ljava/lang/String; = "LPCM"

.field public static final MIME_AUDIO_3GPP:Ljava/lang/String; = "audio/3gpp"

.field public static final MIME_AUDIO_AAC:Ljava/lang/String; = "audio/aac"

.field public static final MIME_AUDIO_ADTS:Ljava/lang/String; = "audio/vnd.dlna.adts"

.field public static final MIME_AUDIO_L16:Ljava/lang/String; = "audio/L16"

.field public static final MIME_AUDIO_MP4:Ljava/lang/String; = "audio/mp4"

.field public static final MIME_AUDIO_MPEG:Ljava/lang/String; = "audio/mpeg"

.field public static final MIME_AUDIO_RAW:Ljava/lang/String; = "audio/vnd.dolby.dd-raw"

.field public static final MIME_AUDIO_SONY_OMA:Ljava/lang/String; = "audio/x-sony-oma"

.field public static final MIME_AUDIO_WMA:Ljava/lang/String; = "audio/x-ms-wma"

.field public static final MIME_IMAGE_JPEG:Ljava/lang/String; = "image/jpeg"

.field public static final MIME_IMAGE_PNG:Ljava/lang/String; = "image/png"

.field public static final MIME_IMAGE_YUV:Ljava/lang/String; = "image/x-ycbcr-yuv420"

.field public static final MIME_VIDEO_3GPP:Ljava/lang/String; = "video/3gpp"

.field public static final MIME_VIDEO_ASF:Ljava/lang/String; = "video/x-ms-asf"

.field public static final MIME_VIDEO_AVI:Ljava/lang/String; = "video/avi"

.field public static final MIME_VIDEO_DIVX:Ljava/lang/String; = "video/x-divx"

.field public static final MIME_VIDEO_MP4:Ljava/lang/String; = "video/mp4"

.field public static final MIME_VIDEO_MPEG:Ljava/lang/String; = "video/mpeg"

.field public static final MIME_VIDEO_TTS:Ljava/lang/String; = "video/vnd.dlna.mpeg-tts"

.field public static final MIME_VIDEO_WMV:Ljava/lang/String; = "video/x-ms-wmv"

.field public static final MP3:Ljava/lang/String; = "MP3"

.field public static final PNG_LRG:Ljava/lang/String; = "PNG_LRG"

.field public static final PNG_TN:Ljava/lang/String; = "PNG_TN"

.field public static final PROTOCOL_TYPE_MICRO:I = 0x2

.field public static final PROTOCOL_TYPE_ORIGINAL:I = 0x0

.field public static final PROTOCOL_TYPE_SMALL:I = 0x1

.field public static final WMABASE:Ljava/lang/String; = "WMABASE"

.field public static final WMAFULL:Ljava/lang/String; = "WMAFULL"

.field public static final WMAPRO:Ljava/lang/String; = "WMAPRO"

.field public static final WMVHIGH_FULL:Ljava/lang/String; = "WMVHIGH_FULL"

.field public static final WMVHIGH_PRO:Ljava/lang/String; = "WMVHIGH_PRO"

.field public static final WMVMED_BASE:Ljava/lang/String; = "WMVMED_BASE"

.field public static final WMVMED_FULL:Ljava/lang/String; = "WMVMED_FULL"

.field public static final WMVMED_PRO:Ljava/lang/String; = "WMVMED_PRO"

.field private static mConMgr:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

.field private static parser:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;


# instance fields
.field private mPostfixDictionary:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;",
            ">;"
        }
    .end annotation
.end field

.field private mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->parser:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    .line 521
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mConMgr:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    .line 479
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 165
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "image/jpeg"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "image/png"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "image/x-ycbcr-yuv420"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->YUV:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/vnd.dolby.dd-raw"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->RAW:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/vnd.dlna.adts"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ADTS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-ms-asf"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ASF:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/3gpp"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->GPP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/3gpp"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->GPP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/L16"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->LPCM:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/mp4"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP4:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/mp4"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP4:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/mpeg"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/mpeg"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/vnd.dlna.mpeg-tts"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/x-ms-wma"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->WMA:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-ms-wmv"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->WMV:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/avi"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AVI:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "video/x-divx"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AVI:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    const-string v1, "audio/aac"

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AAC:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_SM"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_MED"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_LRG"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/mpeg:DLNA.ORG_PN=MP3;DLNA.ORG_FLAGS=01700000000000000000000000000000"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:video/mp4:DLNA.ORG_PN=AVC_MP4_BL_CIF15_AAC_520;DLNA.ORG_FLAGS=01700000000000000000000000000000"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:video/mp4:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:video/3gpp:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:video/avi:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:video/x-ms-wmv:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 208
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/mp4:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/mpeg:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 210
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/3gpp:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/x-ms-wma:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:audio/L16:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/jpeg:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/png:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:image/x-ycbcr-yuv420:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 220
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const-string v1, "http-get:*:*:*"

    invoke-virtual {v0, v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    return-void
.end method

.method private addNewSourceProtocol(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "protocol"    # Ljava/lang/String;
    .param p2, "atFront"    # Z

    .prologue
    .line 529
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 541
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 533
    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 534
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(ILjava/lang/Object;)V

    .line 538
    :goto_1
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mConMgr:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    if-eqz v0, :cond_0

    .line 539
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mConMgr:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->updateSoruceProtocolInfo()V

    goto :goto_0

    .line 536
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private attachAdditionalFlags(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .param p1, "protocol"    # Ljava/lang/String;
    .param p2, "type"    # I

    .prologue
    .line 601
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 602
    :cond_0
    const-string v0, ""

    .line 624
    :goto_0
    return-object v0

    .line 604
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->get4thField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 605
    const-string v0, "jpeg"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "png"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move-object v0, p1

    .line 607
    goto :goto_0

    .line 608
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 609
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "DLNA.ORG_OP=01"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 613
    :cond_4
    packed-switch p2, :pswitch_data_0

    .line 624
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";DLNA.ORG_OP=01;DLNA.ORG_CI=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 615
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";DLNA.ORG_OP=01;DLNA.ORG_CI=0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 613
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private build1stField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 346
    const-string v0, "http-get"

    return-object v0
.end method

.method private build3rdField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 354
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 355
    :cond_0
    const-string p1, "*"

    .line 357
    .end local p1    # "mime":Ljava/lang/String;
    :cond_1
    return-object p1
.end method

.method private buildAudioDLNAProfileName(Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p1, "mime"    # Ljava/lang/String;
    .param p2, "audioMaxBitRate"    # I

    .prologue
    .line 397
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    .line 398
    .local v0, "type":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
    if-nez v0, :cond_0

    .line 399
    const-string v2, ""

    .line 421
    :goto_0
    return-object v2

    .line 401
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 402
    .local v1, "upnpClass":Ljava/lang/String;
    const-string v2, "object.item.audioItem"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 403
    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$1;->$SwitchMap$com$samsung$android$allshare$stack$mediaserver$upnp$object$ResourceProtocolParser$FILE_EXTENSITON_TYPE:[I

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    .line 415
    const-string v2, ""

    goto :goto_0

    .line 405
    :sswitch_0
    const-string v2, "MP3"

    goto :goto_0

    .line 407
    :sswitch_1
    const-string v2, "AAC_ISO_320"

    goto :goto_0

    .line 409
    :sswitch_2
    const-string v2, "LPCM"

    goto :goto_0

    .line 411
    :sswitch_3
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getWMADLNAType(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 413
    :sswitch_4
    const-string v2, "AAC_ADTS_320"

    goto :goto_0

    .line 417
    :cond_1
    const-string v2, "ResourceProtocolParser"

    const-string v3, "buildAudioDLNAProfileName"

    const-string v4, "invalid method for build DLNA profile type of none av file!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    const-string v2, ""

    goto :goto_0

    .line 403
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x3 -> :sswitch_1
        0x6 -> :sswitch_2
        0x7 -> :sswitch_0
        0xc -> :sswitch_3
    .end sparse-switch
.end method

.method private buildImageDLNAProfileName(Ljava/lang/String;III)Ljava/lang/String;
    .locals 7
    .param p1, "mime"    # Ljava/lang/String;
    .param p2, "proto_type"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/16 v6, 0x1000

    const/4 v5, 0x2

    const/16 v4, 0xa0

    .line 361
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    .line 362
    .local v0, "type":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    .line 363
    const-string v2, ""

    .line 393
    :goto_0
    return-object v2

    .line 365
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 366
    .local v1, "upnpClass":Ljava/lang/String;
    const-string v2, "object.item.imageItem"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 367
    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$1;->$SwitchMap$com$samsung$android$allshare$stack$mediaserver$upnp$object$ResourceProtocolParser$FILE_EXTENSITON_TYPE:[I

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    .line 388
    :cond_1
    const-string v2, ""

    goto :goto_0

    .line 369
    :sswitch_0
    if-eqz p3, :cond_2

    if-nez p4, :cond_3

    .line 370
    :cond_2
    const-string v2, ""

    goto :goto_0

    .line 371
    :cond_3
    if-lt v4, p3, :cond_5

    if-lt v4, p4, :cond_5

    .line 372
    if-ne p2, v5, :cond_4

    const-string v2, "JPEG_TN"

    goto :goto_0

    :cond_4
    const-string v2, "JPEG_SM"

    goto :goto_0

    .line 373
    :cond_5
    const/16 v2, 0x280

    if-lt v2, p3, :cond_6

    const/16 v2, 0x1e0

    if-lt v2, p4, :cond_6

    .line 374
    const-string v2, "JPEG_SM"

    goto :goto_0

    .line 375
    :cond_6
    const/16 v2, 0x400

    if-lt v2, p3, :cond_7

    const/16 v2, 0x300

    if-lt v2, p4, :cond_7

    .line 376
    const-string v2, "JPEG_MED"

    goto :goto_0

    .line 377
    :cond_7
    if-lt v6, p3, :cond_1

    if-lt v6, p4, :cond_1

    .line 378
    const-string v2, "JPEG_LRG"

    goto :goto_0

    .line 381
    :sswitch_1
    if-eqz p3, :cond_8

    if-nez p4, :cond_9

    .line 382
    :cond_8
    const-string v2, ""

    goto :goto_0

    .line 383
    :cond_9
    if-lez p3, :cond_b

    if-lt v4, p3, :cond_b

    if-lez p4, :cond_b

    if-lt v4, p4, :cond_b

    .line 384
    if-ne p2, v5, :cond_a

    const-string v2, "PNG_TN"

    goto :goto_0

    :cond_a
    const-string v2, "PNG_LRG"

    goto :goto_0

    .line 386
    :cond_b
    const-string v2, "PNG_LRG"

    goto :goto_0

    .line 390
    :cond_c
    const-string v2, "ResourceProtocolParser"

    const-string v3, "buildImageDLNAProfileName"

    const-string v4, "invalid method for build DLNA profile type of none image file!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    const-string v2, ""

    goto :goto_0

    .line 367
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method private buildProtocol(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;
    .locals 4
    .param p1, "mime"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "profileName"    # Ljava/lang/String;
    .param p4, "isNotImage"    # Z

    .prologue
    const/16 v3, 0x3a

    .line 497
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, ""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 498
    .local v1, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->build1stField()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 499
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 500
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->bulid2ndField()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 501
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 502
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->build3rdField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 505
    const-string v0, ""

    .line 506
    .local v0, "protocol":Ljava/lang/String;
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 507
    :cond_0
    const/16 v2, 0x2a

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 508
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 509
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->addNewSourceProtocol(Ljava/lang/String;Z)V

    .line 518
    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->attachAdditionalFlags(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 511
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DLNA.ORG_PN="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    if-eqz p4, :cond_2

    .line 513
    const-string v2, ";DLNA.ORG_FLAGS=01700000000000000000000000000000"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 516
    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->addNewSourceProtocol(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private buildVideoDLNAProfileName(Ljava/lang/String;III)Ljava/lang/String;
    .locals 5
    .param p1, "mime"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "audioBitRate"    # I

    .prologue
    .line 425
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    .line 426
    .local v0, "type":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
    if-nez v0, :cond_0

    .line 427
    const-string v2, ""

    .line 443
    :goto_0
    return-object v2

    .line 429
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 430
    .local v1, "upnpClass":Ljava/lang/String;
    const-string v2, "object.item.videoItem"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 431
    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$1;->$SwitchMap$com$samsung$android$allshare$stack$mediaserver$upnp$object$ResourceProtocolParser$FILE_EXTENSITON_TYPE:[I

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    .line 437
    const-string v2, ""

    goto :goto_0

    .line 433
    :sswitch_0
    const-string v2, "AVC_MP4_BL_CIF15_AAC_520"

    goto :goto_0

    .line 435
    :sswitch_1
    invoke-direct {p0, p4, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getWMVDLNAType(III)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 439
    :cond_1
    const-string v2, "ResourceProtocolParser"

    const-string v3, "buildVideoDLNAProfileName"

    const-string v4, "invalid method for build DLNA profile type of none av file!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    const-string v2, ""

    goto :goto_0

    .line 431
    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method private bulid2ndField()Ljava/lang/String;
    .locals 1

    .prologue
    .line 350
    const-string v0, "*"

    return-object v0
.end method

.method private getDefaultFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 273
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    :cond_0
    const-string v0, ".tmp"

    .line 284
    :goto_0
    return-object v0

    .line 276
    :cond_1
    const-string v0, "image"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v1, :cond_2

    .line 277
    const-string v0, ".jpeg"

    goto :goto_0

    .line 278
    :cond_2
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v1, :cond_3

    .line 279
    const-string v0, ".mp3"

    goto :goto_0

    .line 280
    :cond_3
    const-string v0, "video"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v1, :cond_4

    .line 281
    const-string v0, ".avi"

    goto :goto_0

    .line 284
    :cond_4
    const-string v0, ".tmp"

    goto :goto_0
.end method

.method private getFileExtension(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;)Ljava/lang/String;
    .locals 2
    .param p1, "type"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    .prologue
    .line 224
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$1;->$SwitchMap$com$samsung$android$allshare$stack$mediaserver$upnp$object$ResourceProtocolParser$FILE_EXTENSITON_TYPE:[I

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 256
    const-string v0, ""

    :goto_0
    return-object v0

    .line 226
    :pswitch_0
    const-string v0, ".adts"

    goto :goto_0

    .line 228
    :pswitch_1
    const-string v0, ".asf"

    goto :goto_0

    .line 230
    :pswitch_2
    const-string v0, ".3gpp"

    goto :goto_0

    .line 232
    :pswitch_3
    const-string v0, ".3gp"

    goto :goto_0

    .line 234
    :pswitch_4
    const-string v0, ".jpeg"

    goto :goto_0

    .line 236
    :pswitch_5
    const-string v0, ".lpcm"

    goto :goto_0

    .line 238
    :pswitch_6
    const-string v0, ".mp3"

    goto :goto_0

    .line 240
    :pswitch_7
    const-string v0, ".mp4"

    goto :goto_0

    .line 242
    :pswitch_8
    const-string v0, ".mpeg"

    goto :goto_0

    .line 244
    :pswitch_9
    const-string v0, ".png"

    goto :goto_0

    .line 246
    :pswitch_a
    const-string v0, ".raw"

    goto :goto_0

    .line 248
    :pswitch_b
    const-string v0, ".wma"

    goto :goto_0

    .line 250
    :pswitch_c
    const-string v0, ".wmv"

    goto :goto_0

    .line 252
    :pswitch_d
    const-string v0, ".avi"

    goto :goto_0

    .line 254
    :pswitch_e
    const-string v0, ".aac"

    goto :goto_0

    .line 224
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static declared-synchronized getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .locals 2

    .prologue
    .line 158
    const-class v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->parser:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private getWMADLNAType(I)Ljava/lang/String;
    .locals 1
    .param p1, "audioMaxBitRate"    # I

    .prologue
    .line 447
    const/16 v0, 0xc0

    if-gt p1, v0, :cond_0

    .line 448
    const-string v0, "WMABASE"

    .line 452
    :goto_0
    return-object v0

    .line 449
    :cond_0
    const/16 v0, 0x180

    if-lt p1, v0, :cond_1

    .line 450
    const-string v0, "WMAPRO"

    goto :goto_0

    .line 452
    :cond_1
    const-string v0, "WMAFULL"

    goto :goto_0
.end method

.method private getWMVDLNAType(III)Ljava/lang/String;
    .locals 2
    .param p1, "audioBitRate"    # I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/16 v1, 0x180

    .line 456
    const/16 v0, 0x780

    if-lt p2, v0, :cond_1

    const/16 v0, 0x438

    if-lt p3, v0, :cond_1

    .line 457
    if-lt p1, v1, :cond_0

    .line 458
    const-string v0, "WMVHIGH_PRO"

    .line 468
    :goto_0
    return-object v0

    .line 460
    :cond_0
    const-string v0, "WMVHIGH_FULL"

    goto :goto_0

    .line 463
    :cond_1
    const/16 v0, 0xc0

    if-gt p1, v0, :cond_2

    .line 464
    const-string v0, "WMVMED_BASE"

    goto :goto_0

    .line 465
    :cond_2
    if-lt p1, v1, :cond_3

    .line 466
    const-string v0, "WMVMED_PRO"

    goto :goto_0

    .line 468
    :cond_3
    const-string v0, "WMVMED_FULL"

    goto :goto_0
.end method

.method public static setConnectionManager(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;)V
    .locals 0
    .param p0, "conMgr"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    .prologue
    .line 524
    sput-object p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mConMgr:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    .line 525
    return-void
.end method


# virtual methods
.method public buildAudioProtocol(Ljava/lang/String;II)Ljava/lang/String;
    .locals 2
    .param p1, "mime"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "audioMaxBitRate"    # I

    .prologue
    .line 487
    invoke-direct {p0, p1, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildAudioDLNAProfileName(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 488
    .local v0, "profileName":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildProtocol(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public buildImageProtocol(Ljava/lang/String;III)Ljava/lang/String;
    .locals 2
    .param p1, "mime"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 482
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildImageDLNAProfileName(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v0

    .line 483
    .local v0, "profileName":Ljava/lang/String;
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildProtocol(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public buildVideoProtocol(Ljava/lang/String;IIII)Ljava/lang/String;
    .locals 2
    .param p1, "mime"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "audioBitRate"    # I

    .prologue
    .line 492
    invoke-direct {p0, p1, p3, p4, p5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildVideoDLNAProfileName(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v0

    .line 493
    .local v0, "profileName":Ljava/lang/String;
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildProtocol(Ljava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public convertToValidateProtocol(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x3a

    .line 544
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 545
    .local v2, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->build1stField()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 546
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 547
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->bulid2ndField()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 549
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 550
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 552
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->get4thField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 553
    .local v0, "feature":Ljava/lang/String;
    const/16 v3, 0x3b

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v1

    .line 554
    .local v1, "pos":I
    if-lez v1, :cond_0

    .line 555
    invoke-virtual {v0, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 556
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 557
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->attachAdditionalFlags(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public convertToValidateProtocolWithout4thField(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x3a

    .line 561
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 562
    .local v0, "sb":Ljava/lang/StringBuilder;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->build1stField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 563
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 564
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->bulid2ndField()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 565
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 566
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 568
    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 569
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public get4thField(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 300
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 301
    :cond_0
    const-string v0, "*"

    .line 315
    :cond_1
    :goto_0
    return-object v0

    .line 303
    :cond_2
    const-string v0, "*"

    .line 305
    .local v0, "field":Ljava/lang/String;
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, ":"

    invoke-direct {v1, p1, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    .local v1, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 307
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 308
    :cond_3
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 309
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 310
    :cond_4
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 311
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 312
    :cond_5
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 313
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentProtocolInfo()Ljava/lang/String;
    .locals 4

    .prologue
    .line 574
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 575
    .local v2, "sb":Ljava/lang/StringBuilder;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 576
    .local v1, "protocol":Ljava/lang/String;
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 579
    .end local v1    # "protocol":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 580
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getFileExtensionByMimetype(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 260
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 261
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getDefaultFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 269
    :goto_0
    return-object v1

    .line 263
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    .line 265
    .local v0, "type":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
    if-nez v0, :cond_2

    .line 266
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getDefaultFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 269
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mPostfixDictionary:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getFileExtension(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 319
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 320
    :cond_0
    const-string v1, "*/*"

    .line 329
    :goto_0
    return-object v1

    .line 322
    :cond_1
    new-instance v0, Ljava/util/StringTokenizer;

    const-string v1, ":"

    invoke-direct {v0, p1, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .local v0, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 324
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 325
    :cond_2
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 326
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    .line 327
    :cond_3
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 328
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 329
    :cond_4
    const-string v1, "*/*"

    goto :goto_0
.end method

.method public getMimeTypeByPN(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "pn"    # Ljava/lang/String;

    .prologue
    .line 333
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 334
    :cond_0
    const-string v2, "*/*"

    .line 340
    :goto_0
    return-object v2

    .line 336
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 337
    .local v1, "pt":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 338
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 340
    .end local v1    # "pt":Ljava/lang/String;
    :cond_3
    const-string v2, "*/*"

    goto :goto_0
.end method

.method public getUPnPClassByMimetype(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 288
    if-nez p1, :cond_0

    .line 289
    const-string v0, "object.item"

    .line 296
    :goto_0
    return-object v0

    .line 290
    :cond_0
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    const-string v0, "object.item.audioItem"

    goto :goto_0

    .line 292
    :cond_1
    const-string v0, "video"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 293
    const-string v0, "object.item.videoItem"

    goto :goto_0

    .line 294
    :cond_2
    const-string v0, "image"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 295
    const-string v0, "object.item.imageItem"

    goto :goto_0

    .line 296
    :cond_3
    const-string v0, "object.item"

    goto :goto_0
.end method

.method public isSupportedProtocol(Ljava/lang/String;)Z
    .locals 4
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 589
    if-nez p1, :cond_1

    .line 596
    :cond_0
    :goto_0
    return v2

    .line 592
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->mProtocols:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 593
    .local v1, "prot":Ljava/lang/String;
    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 594
    const/4 v2, 0x1

    goto :goto_0
.end method
