.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
.source "ContentRequestAction.java"


# static fields
.field private static final FILTER:Ljava/lang/String; = "Filter"

.field private static final NUMBER_RETURNED:Ljava/lang/String; = "NumberReturned"

.field private static final REQUESTED_COUNT:Ljava/lang/String; = "RequestedCount"

.field private static final RESULT:Ljava/lang/String; = "Result"

.field private static final SORT_CRITERIA:Ljava/lang/String; = "SortCriteria"

.field private static final STARTING_INDEX:Ljava/lang/String; = "StartingIndex"

.field private static final TOTAL_MACHES:Ljava/lang/String; = "TotalMatches"

.field private static final UPDATE_ID:Ljava/lang/String; = "UpdateID"


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V
    .locals 0
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 36
    return-void
.end method


# virtual methods
.method public getFilter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "Filter"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestedCount()I
    .locals 2

    .prologue
    .line 48
    const-string v1, "RequestedCount"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public getSortCriteria()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    const-string v0, "SortCriteria"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStartingIndex()I
    .locals 2

    .prologue
    .line 43
    const-string v1, "StartingIndex"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "value":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method public setNumberReturned(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 65
    const-string v0, "NumberReturned"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;->setArgumentValue(Ljava/lang/String;I)V

    .line 66
    return-void
.end method

.method public setResult(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 61
    const-string v0, "Result"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public setTotalMaches(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 69
    const-string v0, "TotalMatches"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;->setArgumentValue(Ljava/lang/String;I)V

    .line 70
    return-void
.end method

.method public setUpdateID(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 73
    const-string v0, "UpdateID"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;->setArgumentValue(Ljava/lang/String;I)V

    .line 74
    return-void
.end method
