.class public Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
.source "MediaStoreItemObject.java"


# instance fields
.field private mArleadyUpdated:Z

.field private mContentUri:Landroid/net/Uri;

.field private mUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "upnpClass"    # Ljava/lang/String;
    .param p4, "contentUri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mContentUri:Landroid/net/Uri;

    .line 31
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mArleadyUpdated:Z

    .line 24
    iput-object p4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mContentUri:Landroid/net/Uri;

    .line 25
    return-void
.end method


# virtual methods
.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mContentUri:Landroid/net/Uri;

    return-object v0
.end method

.method public declared-synchronized setContentResourceUpdater(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;)V
    .locals 1
    .param p1, "updater"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;

    .prologue
    .line 34
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 35
    monitor-exit p0

    return-void

    .line 34
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized update()V
    .locals 1

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mArleadyUpdated:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 45
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 44
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->update(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->mArleadyUpdated:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
