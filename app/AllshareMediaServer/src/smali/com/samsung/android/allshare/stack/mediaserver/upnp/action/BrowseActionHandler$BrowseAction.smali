.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;
.source "BrowseActionHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BrowseAction"
.end annotation


# static fields
.field public static final BROWSE_DIRECT_CHILDREN:Ljava/lang/String; = "BrowseDirectChildren"

.field public static final BROWSE_FLAG:Ljava/lang/String; = "BrowseFlag"

.field public static final BROWSE_METADATA:Ljava/lang/String; = "BrowseMetadata"

.field public static final OBJECT_ID:Ljava/lang/String; = "ObjectID"


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V
    .locals 0
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ContentRequestAction;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 60
    return-void
.end method


# virtual methods
.method public getBrowseFlag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string v0, "BrowseFlag"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getObjectID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    const-string v0, "ObjectID"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDirectChildren()Z
    .locals 2

    .prologue
    .line 74
    const-string v0, "BrowseDirectChildren"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getBrowseFlag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isMetadata()Z
    .locals 2

    .prologue
    .line 70
    const-string v0, "BrowseMetadata"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/BrowseActionHandler$BrowseAction;->getBrowseFlag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
