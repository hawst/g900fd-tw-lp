.class public Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
.source "ActionCamera.java"


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;)V
    .locals 3
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;

    .prologue
    .line 53
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;)V

    .line 54
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->clear()V

    .line 55
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    .line 56
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V
    .locals 3
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 59
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->clear()V

    .line 61
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    .line 62
    return-void
.end method


# virtual methods
.method public performActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;)Z
    .locals 7
    .param p1, "actionReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 65
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->mActionReq:Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    .line 66
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getActionListener()Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    move-result-object v2

    .line 67
    .local v2, "listener":Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;
    if-nez v2, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v4

    .line 70
    :cond_1
    new-instance v0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;-><init>()V

    .line 71
    .local v0, "actionRes":Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;
    const/16 v6, 0x191

    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->setStatus(I)V

    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->clearOutputAgumentValues()V

    .line 74
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getRequestAddress()Ljava/net/InetAddress;

    move-result-object v1

    .line 76
    .local v1, "clientAddr":Ljava/net/InetAddress;
    if-eqz v1, :cond_0

    .line 79
    invoke-interface {v2, p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;->actionControlReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z

    move-result v4

    if-ne v4, v5, :cond_4

    .line 80
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "GetIP"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "GetInfomation"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "SetResolution"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "ZoomIN"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "ZoomOUT"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "MULTIAF"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "AF"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "AFRELEASE"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "Shot"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "ShotWithGPS"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "SetLED"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "SetFlash"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "GetZoom"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "SetZoom"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "ReleaseSelfTimer"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v6, "ShutterUp"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 89
    :cond_2
    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;->setResponseCamera(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 98
    :goto_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;->print()V

    .line 99
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    .line 103
    invoke-interface {v2, p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;->actionControlHandled(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z

    move v4, v5

    .line 105
    goto/16 :goto_0

    .line 91
    :cond_3
    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;->setResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    goto :goto_1

    .line 94
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    move-result-object v3

    .line 95
    .local v3, "upnpStatus":Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->getCode()I

    move-result v4

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionResponseCamera;->setFaultResponse(ILjava/lang/String;)V

    goto :goto_1
.end method
