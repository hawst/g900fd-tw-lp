.class Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;
.super Ljava/lang/Object;
.source "MediaStoreDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaStoreUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;
    .param p2, "x1"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$1;

    .prologue
    .line 126
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)V

    return-void
.end method


# virtual methods
.method public update()V
    .locals 33

    .prologue
    .line 128
    const-string v4, "MediaStoreDirectory"

    const-string v5, "MediaStoreUpdater"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Update "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getMimeType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v4

    if-nez v4, :cond_1

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    const/4 v10, 0x0

    .line 133
    .local v10, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 134
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mContentUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$500(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mWhere:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$600(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 137
    :cond_2
    if-eqz v10, :cond_0

    .line 142
    new-instance v29, Ljava/util/HashMap;

    invoke-direct/range {v29 .. v29}, Ljava/util/HashMap;-><init>()V

    .line 143
    .local v29, "tempItemMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getID()Ljava/lang/String;

    move-result-object v23

    .line 145
    .local v23, "parentId":Ljava/lang/String;
    :cond_3
    :goto_1
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 146
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mCurrentUpdater:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$700(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;

    move-result-object v4

    move-object/from16 v0, p0

    if-eq v4, v0, :cond_7

    .line 148
    if-eqz v10, :cond_0

    .line 149
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 212
    :catch_0
    move-exception v12

    .line 213
    .local v12, "ex":Ljava/lang/IllegalStateException;
    const-string v4, "MediaStoreDirectory"

    const-string v5, "MediaStoreUpdater"

    const-string v6, "Caught IllegalStateException- ignore"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    .end local v12    # "ex":Ljava/lang/IllegalStateException;
    :cond_4
    if-eqz v10, :cond_5

    .line 217
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 223
    :cond_5
    :goto_2
    invoke-virtual/range {v29 .. v29}, Ljava/util/HashMap;->size()I

    move-result v4

    if-gtz v4, :cond_6

    invoke-virtual/range {v29 .. v29}, Ljava/util/HashMap;->size()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getItemMap()Ljava/util/TreeMap;
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$1000(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/util/TreeMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/TreeMap;->size()I

    move-result v5

    if-eq v4, v5, :cond_0

    .line 224
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getItemMap()Ljava/util/TreeMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$1100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/util/TreeMap;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/TreeMap;->clear()V

    .line 225
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getItemMap()Ljava/util/TreeMap;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$1200(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/util/TreeMap;

    move-result-object v4

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/util/TreeMap;->putAll(Ljava/util/Map;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getItemMap()Ljava/util/TreeMap;
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$1300(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Ljava/util/TreeMap;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/TreeMap;->size()I

    move-result v5

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->setChildCount(I)V
    invoke-static {v4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$1400(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;I)V

    .line 227
    const-string v4, "MediaStoreDirectory"

    const-string v5, "MediaStoreUpdater"

    const-string v6, "Notifying DirectoryChanged"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->notifiyDirectoryChanged()V
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$1500(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)V

    goto/16 :goto_0

    .line 153
    :cond_7
    const/4 v4, 0x0

    :try_start_2
    invoke-interface {v10, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 154
    .local v14, "id":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    .line 156
    .local v22, "media_id":Ljava/lang/String;
    const-string v4, "title"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 157
    .local v18, "idxTitle":I
    move/from16 v0, v18

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v30

    .line 159
    .local v30, "title":Ljava/lang/String;
    const-string v4, "date_modified"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 160
    .local v17, "idxModifyDate":I
    const-string v4, "date_added"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 161
    .local v15, "idxCreateDate":I
    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-interface {v10, v15}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v20

    .line 163
    .local v20, "lMaxDate":J
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-wide/from16 v0, v20

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getDateTimeStr(J)Ljava/lang/String;
    invoke-static {v4, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$800(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;J)Ljava/lang/String;

    move-result-object v27

    .line 164
    .local v27, "strDate":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->getItemByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    move-result-object v19

    .line 166
    .local v19, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    const-string v31, ""

    .line 167
    .local v31, "titleProp":Ljava/lang/String;
    const-string v11, ""

    .line 168
    .local v11, "dateProp":Ljava/lang/String;
    if-eqz v19, :cond_9

    .line 169
    const-string v4, "dc:title"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v24

    .line 170
    .local v24, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v24, :cond_8

    .line 171
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getValue()Ljava/lang/String;

    move-result-object v31

    .line 173
    :cond_8
    const-string v4, "dc:date"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v25

    .line 174
    .local v25, "propDate":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v25, :cond_9

    .line 175
    invoke-virtual/range {v25 .. v25}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getValue()Ljava/lang/String;

    move-result-object v11

    .line 178
    .end local v24    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v25    # "propDate":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    :cond_9
    if-eqz v19, :cond_a

    invoke-virtual/range {v30 .. v31}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 179
    move-object/from16 v0, v29

    move-object/from16 v1, v22

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 183
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory$MediaStoreUpdater;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->mContentUri:Landroid/net/Uri;
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$500(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;)Landroid/net/Uri;

    move-result-object v5

    int-to-long v6, v14

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v22

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->convertCursorToItemNode(Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    invoke-static {v4, v0, v5, v10}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;->access$900(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreDirectory;Ljava/lang/String;Landroid/net/Uri;Landroid/database/Cursor;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    move-result-object v19

    .line 185
    const-string v4, "mime_type"

    invoke-interface {v10, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 186
    .local v16, "idxMime":I
    move/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v32

    .line 189
    .local v32, "type":Ljava/lang/String;
    if-eqz v19, :cond_d

    const-string v4, "image"

    move-object/from16 v0, v32

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_d

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_d

    .line 190
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getResoureList()Ljava/util/ArrayList;

    move-result-object v26

    .line 191
    .local v26, "propList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;>;"
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 194
    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_b
    :goto_3
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    .line 195
    .local v24, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    if-eqz v24, :cond_c

    .line 196
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getProtocol()Ljava/lang/String;

    move-result-object v28

    .line 197
    .local v28, "strProtocol":Ljava/lang/String;
    const-string v4, "_TN"

    move-object/from16 v0, v28

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 198
    invoke-virtual/range {v24 .. v24}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->deleteContent()V

    goto :goto_3

    .line 201
    .end local v28    # "strProtocol":Ljava/lang/String;
    :cond_c
    const-string v4, "MediaStoreDirectory"

    const-string v5, "MediaStoreUpdater"

    const-string v6, "prop is null"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 206
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v24    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v26    # "propList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;>;"
    :cond_d
    if-eqz v19, :cond_3

    .line 207
    move-object/from16 v0, v19

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setParentID(Ljava/lang/String;)V

    .line 208
    move-object/from16 v0, v29

    move-object/from16 v1, v22

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_1

    .line 219
    .end local v11    # "dateProp":Ljava/lang/String;
    .end local v14    # "id":I
    .end local v15    # "idxCreateDate":I
    .end local v16    # "idxMime":I
    .end local v17    # "idxModifyDate":I
    .end local v18    # "idxTitle":I
    .end local v19    # "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .end local v20    # "lMaxDate":J
    .end local v22    # "media_id":Ljava/lang/String;
    .end local v27    # "strDate":Ljava/lang/String;
    .end local v30    # "title":Ljava/lang/String;
    .end local v31    # "titleProp":Ljava/lang/String;
    .end local v32    # "type":Ljava/lang/String;
    :catch_1
    move-exception v12

    .line 220
    .local v12, "ex":Ljava/lang/Exception;
    const-string v4, "MediaStoreDirectory"

    const-string v5, "MediaStoreUpdater"

    const-string v6, "Cursor Close Exception- ignore"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method
