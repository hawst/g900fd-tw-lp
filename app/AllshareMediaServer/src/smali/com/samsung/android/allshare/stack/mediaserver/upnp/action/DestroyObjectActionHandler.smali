.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/DestroyObjectActionHandler;
.super Ljava/lang/Object;
.source "DestroyObjectActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# static fields
.field public static final CONTAINER_ID:Ljava/lang/String; = "ContainerID"

.field public static final CONTAINER_ID_DLNA:Ljava/lang/String; = "DLNA.ORG_AnyContainer"

.field public static final ELEMENTS:Ljava/lang/String; = "Elements"

.field public static final OBJECT_ID:Ljava/lang/String; = "ObjectID"

.field public static final RESULT:Ljava/lang/String; = "Result"


# instance fields
.field private mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)V
    .locals 1
    .param p1, "cds"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/DestroyObjectActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .line 53
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/DestroyObjectActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    .line 54
    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 8
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/4 v5, 0x0

    .line 58
    const-string v6, "ObjectID"

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 59
    .local v1, "id":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/DestroyObjectActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v6, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->findObjectByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v2

    .line 60
    .local v2, "media":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    if-nez v2, :cond_0

    .line 61
    const/16 v6, 0x192

    const-string v7, "No such object"

    invoke-virtual {p1, v6, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 80
    .end local v2    # "media":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :goto_0
    return v5

    .line 64
    .restart local v2    # "media":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getRestricted()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 65
    const/16 v6, 0x2c7

    const-string v7, "Restricted object"

    invoke-virtual {p1, v6, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(ILjava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_1
    instance-of v5, v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    if-eqz v5, :cond_3

    .line 70
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getParentID()Ljava/lang/String;

    move-result-object v3

    .line 71
    .local v3, "parentId":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/DestroyObjectActionHandler;->mCDS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v5, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v0

    .line 73
    .local v0, "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-eqz v0, :cond_2

    move-object v5, v2

    .line 74
    check-cast v5, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    invoke-virtual {v0, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->removeItemObject(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)V

    .line 76
    :cond_2
    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .end local v2    # "media":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getDefaultResource()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;

    move-result-object v4

    .line 77
    .local v4, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    if-eqz v4, :cond_3

    .line 78
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->deleteContent()V

    .line 80
    .end local v0    # "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .end local v3    # "parentId":Ljava/lang/String;
    .end local v4    # "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    :cond_3
    const/4 v5, 0x1

    goto :goto_0
.end method
