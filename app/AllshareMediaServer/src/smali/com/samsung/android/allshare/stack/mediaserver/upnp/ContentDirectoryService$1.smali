.class Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;
.super Landroid/os/Handler;
.source "ContentDirectoryService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 290
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v10, 0x3e9

    .line 293
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 318
    :goto_0
    return-void

    .line 295
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getSystemUpdateID()I

    move-result v0

    .line 296
    .local v0, "currSystemUpdateID":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 298
    .local v2, "currSystemUpdateTime":J
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateTime:J
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)J

    move-result-wide v6

    sub-long v6, v2, v6

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getSystemUpdateIDInterval()J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-gez v1, :cond_0

    .line 302
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getSystemUpdateIDInterval()J

    move-result-wide v6

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateTime:J
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)J

    move-result-wide v8

    sub-long v8, v2, v8

    sub-long v4, v6, v8

    .line 304
    .local v4, "diff":J
    invoke-virtual {p0, v10, v4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 308
    .end local v4    # "diff":J
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateID:I
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 309
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mVarSystemUpdateID:Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->access$200(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setValue(I)V

    .line 310
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    # setter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateID:I
    invoke-static {v1, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->access$102(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;I)I

    .line 311
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    # setter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->mLastSystemUpdateTime:J
    invoke-static {v1, v6, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->access$002(Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;J)J

    .line 313
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->getSystemUpdateIDInterval()J

    move-result-wide v6

    invoke-virtual {p0, v10, v6, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService$1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 293
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method
