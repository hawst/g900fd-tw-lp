.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;
.source "SubtitleProperty.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;


# static fields
.field private static final PROTOCOL:Ljava/lang/String; = "http-get:*:text/*:*"


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/io/File;Ljava/lang/String;)V
    .locals 2
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p2, "file"    # Ljava/io/File;
    .param p3, "extension"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getContentSubtitleExportURL(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "http-get:*:text/*:*"

    invoke-direct {p0, v0, v1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 27
    return-void
.end method
