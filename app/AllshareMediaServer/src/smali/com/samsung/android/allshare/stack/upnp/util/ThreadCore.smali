.class public Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;
.super Ljava/lang/Object;
.source "ThreadCore.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mName:Ljava/lang/String;

.field private mThreadObject:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->mThreadObject:Ljava/lang/Thread;

    .line 26
    const-string v0, "Unnamed Thread"

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->mName:Ljava/lang/String;

    .line 19
    return-void
.end method

.method private getThreadObject()Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->mThreadObject:Ljava/lang/Thread;

    return-object v0
.end method

.method private setThreadObject(Ljava/lang/Thread;)V
    .locals 0
    .param p1, "obj"    # Ljava/lang/Thread;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->mThreadObject:Ljava/lang/Thread;

    .line 30
    return-void
.end method


# virtual methods
.method public isRunnable()Z
    .locals 2

    .prologue
    .line 55
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    .line 56
    .local v0, "thisThread":Ljava/lang/Thread;
    if-eqz v0, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->getThreadObject()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 59
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public restart()V
    .locals 0

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->stop()V

    .line 73
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->start()V

    .line 74
    return-void
.end method

.method public run()V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->mName:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->getThreadObject()Ljava/lang/Thread;

    move-result-object v0

    .line 42
    .local v0, "threadObject":Ljava/lang/Thread;
    if-nez v0, :cond_1

    .line 43
    new-instance v0, Ljava/lang/Thread;

    .end local v0    # "threadObject":Ljava/lang/Thread;
    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 44
    .restart local v0    # "threadObject":Ljava/lang/Thread;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->mName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 45
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 46
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->setThreadObject(Ljava/lang/Thread;)V

    .line 47
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 49
    :cond_1
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->getThreadObject()Ljava/lang/Thread;

    move-result-object v0

    .line 65
    .local v0, "threadObject":Ljava/lang/Thread;
    if-eqz v0, :cond_0

    .line 66
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;->setThreadObject(Ljava/lang/Thread;)V

    .line 67
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 69
    :cond_0
    return-void
.end method
