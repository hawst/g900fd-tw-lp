.class public Lcom/samsung/android/allshare/stack/upnp/xml/XML;
.super Ljava/lang/Object;
.source "XML.java"


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String; = "text/xml; charset=\"utf-8\""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 123
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->escapeXMLChars(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static escapeXMLChars(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 7
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "quote"    # Z

    .prologue
    const/4 v6, 0x0

    .line 38
    if-nez p0, :cond_1

    .line 39
    const/4 p0, 0x0

    .line 81
    .end local p0    # "input":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 40
    .restart local p0    # "input":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    .line 41
    .local v4, "out":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 42
    .local v3, "oldsize":I
    new-array v2, v3, [C

    .line 43
    .local v2, "old":[C
    invoke-virtual {p0, v6, v3, v2, v6}, Ljava/lang/String;->getChars(II[CI)V

    .line 44
    const/4 v5, 0x0

    .line 45
    .local v5, "selstart":I
    const/4 v0, 0x0

    .line 46
    .local v0, "entity":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_5

    .line 47
    aget-char v6, v2, v1

    sparse-switch v6, :sswitch_data_0

    .line 71
    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 72
    sub-int v6, v1, v5

    invoke-virtual {v4, v2, v5, v6}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 73
    invoke-virtual {v4, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 74
    add-int/lit8 v5, v1, 0x1

    .line 75
    const/4 v0, 0x0

    .line 46
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 49
    :sswitch_0
    const-string v0, "&amp;"

    .line 50
    goto :goto_2

    .line 52
    :sswitch_1
    const-string v0, "&lt;"

    .line 53
    goto :goto_2

    .line 55
    :sswitch_2
    const-string v0, "&gt;"

    .line 56
    goto :goto_2

    .line 58
    :sswitch_3
    const-string v0, "&#92;"

    .line 59
    goto :goto_2

    .line 61
    :sswitch_4
    if-eqz p1, :cond_4

    .line 62
    const-string v0, "&apos;"

    .line 63
    goto :goto_2

    .line 66
    :cond_4
    :sswitch_5
    if-eqz p1, :cond_2

    .line 67
    const-string v0, "&quot;"

    goto :goto_2

    .line 78
    :cond_5
    if-eqz v5, :cond_0

    .line 80
    sub-int v6, v3, v5

    invoke-virtual {v4, v2, v5, v6}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    .line 81
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 47
    nop

    :sswitch_data_0
    .sparse-switch
        0x22 -> :sswitch_5
        0x26 -> :sswitch_0
        0x27 -> :sswitch_4
        0x3c -> :sswitch_1
        0x3e -> :sswitch_2
        0x5c -> :sswitch_3
    .end sparse-switch
.end method

.method public static final unEscapeXMLChars(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .prologue
    .line 127
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->unEscapeXMLChars(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static unEscapeXMLChars(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 6
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "quote"    # Z

    .prologue
    .line 85
    if-nez p0, :cond_1

    .line 86
    const/4 p0, 0x0

    .line 119
    .end local p0    # "input":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 87
    .restart local p0    # "input":Ljava/lang/String;
    :cond_1
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "&"

    invoke-direct {v3, p0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .local v3, "st":Ljava/util/StringTokenizer;
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 90
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 91
    .local v1, "out":Ljava/lang/StringBuffer;
    const/4 v4, 0x0

    .line 92
    .local v4, "temp":Ljava/lang/String;
    const/4 v2, 0x0

    .line 94
    .local v2, "size":I
    :goto_1
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 95
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    .line 96
    const-string v5, "amp;"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 97
    const/16 v0, 0x26

    .line 98
    .local v0, "newChar":C
    const/4 v2, 0x4

    .line 115
    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 116
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 99
    .end local v0    # "newChar":C
    :cond_2
    const-string v5, "lt;"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 100
    const/16 v0, 0x3c

    .line 101
    .restart local v0    # "newChar":C
    const/4 v2, 0x3

    goto :goto_2

    .line 102
    .end local v0    # "newChar":C
    :cond_3
    const-string v5, "gt;"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 103
    const/16 v0, 0x3e

    .line 104
    .restart local v0    # "newChar":C
    const/4 v2, 0x3

    goto :goto_2

    .line 105
    .end local v0    # "newChar":C
    :cond_4
    const-string v5, "apos;"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 106
    const/16 v0, 0x27

    .line 107
    .restart local v0    # "newChar":C
    const/4 v2, 0x5

    goto :goto_2

    .line 108
    .end local v0    # "newChar":C
    :cond_5
    const-string v5, "quot;"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 109
    const/16 v0, 0x22

    .line 110
    .restart local v0    # "newChar":C
    const/4 v2, 0x5

    goto :goto_2

    .line 112
    .end local v0    # "newChar":C
    :cond_6
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_1

    .line 119
    :cond_7
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
