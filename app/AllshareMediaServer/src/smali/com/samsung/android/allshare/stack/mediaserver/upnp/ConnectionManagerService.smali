.class public final Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;
.super Ljava/lang/Object;
.source "ConnectionManagerService.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;


# static fields
.field public static final SERVICE_TYPE:Ljava/lang/String; = "urn:schemas-upnp-org:service:ConnectionManager:1"


# instance fields
.field private mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

.field private final mSoapActionHandlers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V
    .locals 1
    .param p1, "server"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mSoapActionHandlers:Ljava/util/HashMap;

    .line 61
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 62
    const-string v0, "urn:schemas-upnp-org:service:ConnectionManager:1"

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getService(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .line 63
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->initActionHandlers()V

    .line 64
    return-void
.end method

.method private initActionHandlers()V
    .locals 4

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "GetProtocolInfo"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ProtocolInfoActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ProtocolInfoActionHandler;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "PrepareForConnection"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/PrepareForConnectionActionHandler;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/PrepareForConnectionActionHandler;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "ConnectionComplete"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ConnectionCompleteActionHandler;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ConnectionCompleteActionHandler;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "GetCurrentConnectionIDs"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CurrentConnectionIdsActionHandler;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CurrentConnectionIdsActionHandler;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mSoapActionHandlers:Ljava/util/HashMap;

    const-string v1, "GetCurrentConnectionInfo"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CurrentConnectionInfoActionHandler;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/CurrentConnectionInfoActionHandler;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    return-void
.end method


# virtual methods
.method public actionControlHandled(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 1
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public actionControlReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 4
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/4 v2, 0x0

    .line 88
    if-nez p1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v2

    .line 91
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v0

    .line 92
    .local v0, "actionName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mSoapActionHandlers:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;

    .line 94
    .local v1, "handler":Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;
    if-eqz v1, :cond_0

    .line 95
    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;->handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z

    move-result v2

    goto :goto_0
.end method

.method public start()V
    .locals 3

    .prologue
    .line 67
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    if-eqz v1, :cond_0

    .line 68
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    invoke-virtual {v1, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    .line 70
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->updateSoruceProtocolInfo()V

    .line 71
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    const-string v2, "CurrentConnectionIDs"

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getStateVariable(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v0

    .line 72
    .local v0, "cids":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    if-eqz v0, :cond_0

    .line 73
    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    .line 75
    .end local v0    # "cids":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    :cond_0
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->clearSubscriber()V

    .line 81
    :cond_0
    return-void
.end method

.method public updateSoruceProtocolInfo()V
    .locals 3

    .prologue
    .line 102
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;->mUPnPService:Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    const-string v2, "SourceProtocolInfo"

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getStateVariable(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v0

    .line 103
    .local v0, "src":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    if-eqz v0, :cond_0

    .line 104
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getCurrentProtocolInfo()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->setValue(Ljava/lang/String;)V

    .line 105
    :cond_0
    return-void
.end method
