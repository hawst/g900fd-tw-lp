.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;
.implements Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;
.implements Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;,
        Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;,
        Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;,
        Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;,
        Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;,
        Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$ManageDevice;
    }
.end annotation


# static fields
.field private static final DEFAULT_CONNECTIVITY_REQUEST_TIME:I = 0x1388

.field private static final DEFAULT_EVENTSUB_PORT:I = 0x1f7a

.field private static final DEFAULT_EVENTSUB_URI:Ljava/lang/String; = "/evetSub"

.field private static final DEFAULT_EXPIRED_DEVICE_MONITORING_INTERVAL:I = 0x3c

.field private static final DEFAULT_REFRESH_EXPIRED_TIME:I = 0x1388

.field private static final DEFAULT_SSDP_PORT:I = 0x1f48

.field private static final TAG:Ljava/lang/String; = "ControlPoint"


# instance fields
.field private deviceSnifferThreadCount:Ljava/lang/Integer;

.field private deviceSnifferThreadCountLock:[B

.field private mActivityList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityListCopy:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mActivityListCopyLock:Ljava/lang/Object;

.field private mCnt:I

.field private mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

.field private mDeviceChangeListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

.field private mDeviceDisposer:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

.field private mDeviceHandler:Landroid/os/Handler;

.field private mDeviceLock:Ljava/lang/Object;

.field private mDeviceManager:Landroid/os/HandlerThread;

.field private mDeviceNotifyListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

.field private mDeviceSearchResponseListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

.field private mEventListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

.field private mEventSubURI:Ljava/lang/String;

.field private mExeutor:Ljava/util/concurrent/ExecutorService;

.field private mExpiredDeviceMonitoringInterval:J

.field private mHandlerCheckExipredDevice:Landroid/os/Handler;

.field private mHttpPort:I

.field private mHttpServerList:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

.field private mRefreshState:Z

.field private mRenewSubscriber:Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;

.field private mRunnableCheckExipredDevice:Ljava/lang/Runnable;

.field private mSearchMx:I

.field private mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

.field private mSsdpPort:I


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide v6, 0x40c3880000000000L    # 10000.0

    const-wide v4, 0x40bf7a0000000000L    # 8058.0

    .line 183
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    mul-double/2addr v0, v6

    add-double/2addr v0, v4

    double-to-int v0, v0

    add-int/lit16 v0, v0, 0x1f48

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    mul-double/2addr v2, v6

    add-double/2addr v2, v4

    double-to-int v1, v2

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;-><init>(II)V

    .line 185
    return-void
.end method

.method public constructor <init>(II)V
    .locals 3
    .param p1, "ssdpPort"    # I
    .param p2, "httpPort"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    .line 156
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    .line 158
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExeutor:Ljava/util/concurrent/ExecutorService;

    .line 160
    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    .line 162
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceLock:Ljava/lang/Object;

    .line 164
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopyLock:Ljava/lang/Object;

    .line 191
    iput v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSsdpPort:I

    .line 216
    iput v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHttpPort:I

    .line 230
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    .line 245
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopy:Ljava/util/HashMap;

    .line 247
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;

    .line 250
    new-array v0, v1, [B

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCountLock:[B

    .line 252
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityList:Ljava/util/ArrayList;

    .line 647
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceNotifyListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    .line 673
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceSearchResponseListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    .line 709
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceChangeListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    .line 952
    const/4 v0, 0x5

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSearchMx:I

    .line 1043
    iput-boolean v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRefreshState:Z

    .line 1053
    iput v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mCnt:I

    .line 1055
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHandlerCheckExipredDevice:Landroid/os/Handler;

    .line 1057
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRunnableCheckExipredDevice:Ljava/lang/Runnable;

    .line 1250
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHttpServerList:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    .line 1288
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    .line 1431
    const-string v0, "/evetSub"

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventSubURI:Ljava/lang/String;

    .line 171
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setSSDPPort(I)V

    .line 172
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setHTTPPort(I)V

    .line 174
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setDeviceDisposer(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;)V

    .line 175
    const-wide/16 v0, 0x3c

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setExpiredDeviceMonitoringInterval(J)V

    .line 177
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setRenewSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;)V

    .line 179
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    .line 180
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeActivityDevice(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeActivityList()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRunnableCheckExipredDevice:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mCnt:I

    return v0
.end method

.method static synthetic access$1502(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mCnt:I

    return p1
.end method

.method static synthetic access$1508(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mCnt:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mCnt:I

    return v0
.end method

.method static synthetic access$1600(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHandlerCheckExipredDevice:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setRefreshMode(Z)V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->checkDeviceAvailable(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceDisposer:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)[B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCountLock:[B

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Integer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Ljava/lang/Integer;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic access$2300(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopyLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopy:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->addDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getRefreshMode()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevice(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->isActivityDevice(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->addActivityDevice(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/util/concurrent/ExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExeutor:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic access$802(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/util/concurrent/ExecutorService;)Ljava/util/concurrent/ExecutorService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p1, "x1"    # Ljava/util/concurrent/ExecutorService;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExeutor:Ljava/util/concurrent/ExecutorService;

    return-object p1
.end method

.method static synthetic access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addActivityDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 259
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    :cond_0
    return-void
.end method

.method private addDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 2
    .param p1, "rootNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 237
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceLock:Ljava/lang/Object;

    monitor-enter v1

    .line 239
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->add(Ljava/lang/Object;)Z

    .line 241
    :cond_0
    monitor-exit v1

    .line 242
    return-void

    .line 241
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private checkDeviceAvailable(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)Z
    .locals 8
    .param p1, "dev"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    const/4 v1, 0x0

    .line 1190
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v3

    .line 1191
    .local v3, "packet":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    if-nez v3, :cond_0

    .line 1192
    const-string v4, "ControlPoint"

    const-string v5, "checkDeviceAvailable"

    const-string v6, "checkDeviceAvailable: SSDPPacket of device is null!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1218
    :goto_0
    return v1

    .line 1197
    :cond_0
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v0

    .line 1198
    .local v0, "addr":Ljava/net/InetAddress;
    if-nez v0, :cond_1

    .line 1199
    const-string v4, "ControlPoint"

    const-string v5, "checkDeviceAvailable"

    const-string v6, "checkDeviceAvailable: InetAddress of device is null!"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1204
    :cond_1
    const/4 v1, 0x0

    .line 1207
    .local v1, "available":Z
    const/16 v4, 0x1388

    :try_start_0
    invoke-virtual {v0, v4}, Ljava/net/InetAddress;->isReachable(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1208
    const/4 v1, 0x1

    goto :goto_0

    .line 1210
    :cond_2
    const-string v4, "ControlPoint"

    const-string v5, "checkDeviceAvailable"

    const-string v6, "checkDeviceAvailable: device is unreachable!( TIME-OUT: 5000 )"

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1214
    :catch_0
    move-exception v2

    .line 1215
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "ControlPoint"

    const-string v5, "checkDeviceAvailable"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "check device reachable raising an exception: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 3
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    const/4 v1, 0x0

    .line 352
    if-nez p1, :cond_1

    .line 357
    :cond_0
    :goto_0
    return-object v1

    .line 354
    :cond_1
    const-string v2, "device"

    invoke-virtual {p1, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 355
    .local v0, "devNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 357
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    invoke-direct {v1, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    goto :goto_0
.end method

.method private getEventSubCallbackURL(Ljava/net/InetAddress;)Ljava/lang/String;
    .locals 3
    .param p1, "ifAddress"    # Ljava/net/InetAddress;

    .prologue
    .line 1443
    if-nez p1, :cond_0

    .line 1444
    const-string v0, ""

    .line 1446
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getHTTPPort()I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getEventSubURI()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getHostURL(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getHTTPServerList()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;
    .locals 1

    .prologue
    .line 1253
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHttpServerList:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    return-object v0
.end method

.method private getRefreshMode()Z
    .locals 1

    .prologue
    .line 1050
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRefreshState:Z

    return v0
.end method

.method private handleNotifyPakcet(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    const/4 v2, 0x1

    .line 831
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 849
    :cond_0
    :goto_0
    return-void

    .line 834
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDatagramPacket()Ljava/net/DatagramPacket;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    .line 837
    .local v0, "addr":Ljava/net/InetAddress;
    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 840
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->isAlive()Z

    move-result v1

    if-ne v1, v2, :cond_2

    .line 841
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 842
    :cond_2
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->isByeBye()Z

    move-result v1

    if-ne v1, v2, :cond_0

    .line 843
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$1;

    invoke-direct {v2, p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$1;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private handleSearchResponsePacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 818
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    if-nez v1, :cond_1

    .line 828
    :cond_0
    :goto_0
    return-void

    .line 821
    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDatagramPacket()Ljava/net/DatagramPacket;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    .line 824
    .local v0, "addr":Ljava/net/InetAddress;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v1

    if-nez v1, :cond_0

    .line 827
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private isActivityDevice(Ljava/lang/String;)Z
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isTargetDevice(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)Z
    .locals 1
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "dev"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    .line 727
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 728
    :cond_0
    const/4 v0, 0x0

    .line 730
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->isSearchTarget(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private isValidNotifyPacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)Z
    .locals 2
    .param p1, "discoveryInfo"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 789
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getNT()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->isTarget(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private isValidResponsePacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)Z
    .locals 2
    .param p1, "discoveryInfo"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    const/4 v0, 0x1

    .line 794
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->isDiscover()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getST()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->isTarget(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isValidTarget(Ljava/lang/String;)Z
    .locals 2
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 959
    const-string v1, "upnp:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    const-string v1, "urn:"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private parseLastChangEvent(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 7
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 1414
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v2

    .line 1415
    .local v2, "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    const/4 v1, 0x0

    .line 1417
    .local v1, "instanceID":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_start_0
    invoke-virtual {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    .line 1418
    .local v3, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v3, :cond_0

    .line 1419
    const/4 v4, 0x0

    .line 1424
    .end local v3    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :goto_0
    return-object v4

    .line 1420
    .restart local v3    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_0
    const-string v4, "InstanceID"

    invoke-virtual {v3, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_0
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .end local v3    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :goto_1
    move-object v4, v1

    .line 1424
    goto :goto_0

    .line 1421
    :catch_0
    move-exception v0

    .line 1422
    .local v0, "e":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    const-string v4, "ControlPoint"

    const-string v5, "parseLastChangEvent"

    const-string v6, " parseLastChangEvent ParserException "

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private removeActivityDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 264
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 265
    return-void
.end method

.method private removeActivityList()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 269
    return-void
.end method

.method private removeDevice(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    .locals 1
    .param p1, "dev"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    .line 500
    if-nez p1, :cond_0

    .line 503
    :goto_0
    return-void

    .line 502
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    goto :goto_0
.end method

.method private removeDevice(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 4
    .param p1, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 517
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->isByeBye()Z

    move-result v3

    if-nez v3, :cond_0

    .line 527
    :goto_0
    return-void

    .line 519
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v1

    .line 520
    .local v1, "usn":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/USN;->getUUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 525
    .local v2, "uuid":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getNetworkInterface()Ljava/net/NetworkInterface;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->generateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 526
    .local v0, "id":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevice(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private removeDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 5
    .param p1, "rootNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 459
    if-nez p1, :cond_1

    .line 475
    :cond_0
    return-void

    .line 462
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 464
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRootDevice()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 466
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v4, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->remove(Ljava/lang/Object;)Z

    .line 467
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->performRemoveDeviceListener(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    .line 469
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 471
    .local v2, "embDeviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 472
    .local v1, "embDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->performRemoveDeviceListener(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    goto :goto_0
.end method

.method private removeDevice(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 512
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 513
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevice(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    .line 514
    return-void
.end method

.method private saveSearchedDeviceUUID(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 3
    .param p1, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 1183
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v0

    .line 1184
    .local v0, "usn":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopyLock:Ljava/lang/Object;

    monitor-enter v2

    .line 1185
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopy:Ljava/util/HashMap;

    invoke-virtual {v1, v0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1186
    monitor-exit v2

    .line 1187
    return-void

    .line 1186
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private setDeviceDisposer(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;)V
    .locals 0
    .param p1, "disposer"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    .prologue
    .line 632
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceDisposer:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    .line 633
    return-void
.end method

.method private setExpiredDeviceMonitoringInterval(J)V
    .locals 1
    .param p1, "interval"    # J

    .prologue
    .line 624
    iput-wide p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExpiredDeviceMonitoringInterval:J

    .line 625
    return-void
.end method

.method private setHTTPPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 223
    iput p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHttpPort:I

    .line 224
    return-void
.end method

.method private setRefreshMode(Z)V
    .locals 0
    .param p1, "state"    # Z

    .prologue
    .line 1046
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRefreshState:Z

    .line 1047
    return-void
.end method

.method private setRenewSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;)V
    .locals 0
    .param p1, "sub"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;

    .prologue
    .line 1632
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRenewSubscriber:Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;

    .line 1633
    return-void
.end method

.method private setSSDPPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 198
    iput p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSsdpPort:I

    .line 199
    return-void
.end method


# virtual methods
.method public addDeviceChangeListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/IDeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/IDeviceChangeListener;

    .prologue
    .line 712
    if-nez p1, :cond_0

    .line 713
    const/4 v0, 0x0

    .line 716
    :goto_0
    return v0

    .line 715
    :cond_0
    monitor-enter p0

    .line 716
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceChangeListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->add(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit p0

    goto :goto_0

    .line 717
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public addEventListener(Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;

    .prologue
    .line 1291
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public addNotifyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .prologue
    .line 650
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceNotifyListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 651
    return-void
.end method

.method public addSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISearchResponseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISearchResponseListener;

    .prologue
    .line 676
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceSearchResponseListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->add(Ljava/lang/Object;)Z

    .line 677
    return-void
.end method

.method public closeSSDPListenSockets()V
    .locals 1

    .prologue
    .line 1773
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->closeSSDPListenSockets()V

    .line 1774
    return-void
.end method

.method public getDevNodeList()Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    return-object v0
.end method

.method public getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 7
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 420
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 421
    .local v2, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/samsung/android/allshare/stack/upnp/xml/Node;>;"
    if-nez v2, :cond_1

    move-object v1, v4

    .line 435
    :cond_0
    :goto_0
    return-object v1

    .line 424
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 425
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 426
    .local v3, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v1

    .line 427
    .local v1, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v1, :cond_1

    .line 429
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isDevice(Ljava/lang/String;)Z

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v6, :cond_0

    .line 431
    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 432
    .local v0, "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 433
    goto :goto_0

    .end local v0    # "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v1    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v3    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_2
    move-object v1, v4

    .line 435
    goto :goto_0
.end method

.method public getDeviceBySID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 6
    .param p1, "sid"    # Ljava/lang/String;

    .prologue
    .line 439
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v2

    .line 441
    .local v2, "nRoots":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 442
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v5, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    .line 443
    .local v3, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 444
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-nez v0, :cond_1

    .line 441
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 446
    :cond_1
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceBySID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 448
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-eqz v4, :cond_0

    .line 451
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v3    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getDeviceDisposer()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceDisposer:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    return-object v0
.end method

.method public getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    .locals 9

    .prologue
    .line 361
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;-><init>()V

    .line 362
    .local v1, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v6

    .line 363
    .local v6, "nRoots":I
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_0
    if-ge v5, v6, :cond_3

    .line 364
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v8, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v7

    .line 365
    .local v7, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 366
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-nez v0, :cond_1

    .line 363
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 368
    :cond_1
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    .line 370
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v3

    .line 371
    .local v3, "emDeviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 372
    .local v2, "emDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v2, :cond_2

    .line 373
    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 377
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v2    # "emDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v3    # "emDeviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_3
    return-object v1
.end method

.method public getDeviceList(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    .locals 9
    .param p1, "interfaceName"    # Ljava/lang/String;

    .prologue
    .line 381
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;-><init>()V

    .line 382
    .local v1, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v6

    .line 383
    .local v6, "nRoots":I
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_0
    if-ge v5, v6, :cond_4

    .line 384
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v8, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v7

    .line 385
    .local v7, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 387
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-nez v0, :cond_1

    .line 383
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 390
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getBoundInterfaceName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 391
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    .line 394
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v3

    .line 395
    .local v3, "emDeviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 396
    .local v2, "emDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v2, :cond_3

    .line 397
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getBoundInterfaceName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 398
    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 402
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v2    # "emDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v3    # "emDeviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_4
    return-object v1
.end method

.method public getDiscoveryInfos()[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->getDiscoveryInfo()[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-result-object v0

    return-object v0
.end method

.method public getEventSubURI()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1434
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventSubURI:Ljava/lang/String;

    return-object v0
.end method

.method public getExpiredDeviceMonitoringInterval()J
    .locals 2

    .prologue
    .line 628
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExpiredDeviceMonitoringInterval:J

    return-wide v0
.end method

.method public getHTTPPort()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHttpPort:I

    return v0
.end method

.method public getRenewSubscriber()Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;
    .locals 1

    .prologue
    .line 1636
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRenewSubscriber:Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;

    return-object v0
.end method

.method public getSSDPPort()I
    .locals 1

    .prologue
    .line 194
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSsdpPort:I

    return v0
.end method

.method public getSearchMx()I
    .locals 1

    .prologue
    .line 955
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSearchMx:I

    return v0
.end method

.method public httpRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 12
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 1259
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isNotifyRequest()Z

    move-result v0

    const/4 v11, 0x1

    if-ne v0, v11, :cond_4

    .line 1260
    new-instance v7, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;

    invoke-direct {v7, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 1261
    .local v7, "notifyReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;
    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->isValidNTS()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->hasNT()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1262
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    .line 1282
    .end local v7    # "notifyReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;
    :cond_1
    :goto_0
    return-void

    .line 1266
    .restart local v7    # "notifyReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;
    :cond_2
    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getSID()Ljava/lang/String;

    move-result-object v1

    .line 1267
    .local v1, "sid":Ljava/lang/String;
    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getSEQ()J

    move-result-wide v2

    .line 1268
    .local v2, "seq":J
    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getPropertyList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;

    move-result-object v10

    .line 1269
    .local v10, "props":Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;
    invoke-virtual {v10}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;->size()I

    move-result v9

    .line 1270
    .local v9, "propCnt":I
    const/4 v6, 0x0

    .local v6, "n":I
    :goto_1
    if-ge v6, v9, :cond_3

    .line 1271
    invoke-virtual {v10, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;->getProperty(I)Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;

    move-result-object v8

    .line 1272
    .local v8, "prop":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1273
    .local v4, "varName":Ljava/lang/String;
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;->getValue()Ljava/lang/String;

    move-result-object v5

    .local v5, "varValue":Ljava/lang/String;
    move-object v0, p0

    .line 1274
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->performEventListener(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 1270
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1276
    .end local v4    # "varName":Ljava/lang/String;
    .end local v5    # "varValue":Ljava/lang/String;
    .end local v8    # "prop":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnOK()Z

    goto :goto_0

    .line 1280
    .end local v1    # "sid":Ljava/lang/String;
    .end local v2    # "seq":J
    .end local v6    # "n":I
    .end local v7    # "notifyReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;
    .end local v9    # "propCnt":I
    .end local v10    # "props":Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;
    :cond_4
    if-eqz p1, :cond_1

    .line 1281
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnBadRequest()Z

    goto :goto_0
.end method

.method public isTarget(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "discoveryInfo"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "candinate"    # Ljava/lang/String;
    .param p3, "includeRoot"    # Z

    .prologue
    const/4 v0, 0x1

    .line 963
    if-nez p2, :cond_1

    .line 964
    const/4 v0, 0x0

    .line 970
    :cond_0
    :goto_0
    return v0

    .line 965
    :cond_1
    if-eqz p3, :cond_2

    const-string v1, "upnp:rootdevice"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 967
    :cond_2
    if-eqz p1, :cond_0

    .line 970
    invoke-virtual {p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->isSearchTarget(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public declared-synchronized onSSDPReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 800
    monitor-enter p0

    if-nez p2, :cond_0

    .line 801
    :try_start_0
    const-string v0, "ControlPoint"

    const-string v1, "onSSDPReceived"

    const-string v2, "packet is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 815
    :goto_0
    monitor-exit p0

    return-void

    .line 806
    :cond_0
    :try_start_1
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->saveSearchedDeviceUUID(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    .line 808
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->isValidNotifyPacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 809
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->handleNotifyPakcet(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    .line 814
    :cond_1
    :goto_1
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->performNotifyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 800
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 810
    :cond_2
    :try_start_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->isValidResponsePacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 811
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->handleSearchResponsePacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public openSSDPListenSockets()V
    .locals 1

    .prologue
    .line 1769
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->openSSDPListenSockets(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    .line 1770
    return-void
.end method

.method protected performAddDeviceListener(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    .locals 7
    .param p1, "discoveryInfo"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "dev"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    .line 735
    monitor-enter p0

    .line 736
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->isTargetDevice(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 737
    monitor-exit p0

    .line 758
    :goto_0
    return-void

    .line 739
    :cond_0
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceChangeListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 740
    .local v2, "listenerSize":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_1
    if-ge v3, v2, :cond_1

    .line 742
    :try_start_1
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceChangeListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 746
    .local v1, "listener":Ljava/lang/Object;
    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/device/IDeviceChangeListener;

    .end local v1    # "listener":Ljava/lang/Object;
    invoke-interface {v1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/IDeviceChangeListener;->deviceAdded(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 740
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 748
    :catch_0
    move-exception v0

    .line 749
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "ControlPoint"

    const-string v5, "performAddDeviceListener"

    const-string v6, "performAddDeviceListener  Exception"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 757
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "listenerSize":I
    .end local v3    # "n":I
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 752
    .restart local v2    # "listenerSize":I
    .restart local v3    # "n":I
    :catch_1
    move-exception v0

    .line 753
    .local v0, "e":Ljava/lang/Error;
    :try_start_3
    const-string v4, "ControlPoint"

    const-string v5, "performAddDeviceListener"

    const-string v6, "performAddDeviceListener Error"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_2

    .line 757
    .end local v0    # "e":Ljava/lang/Error;
    :cond_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public performEventListener(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 20
    .param p1, "sid"    # Ljava/lang/String;
    .param p2, "seq"    # J
    .param p4, "name"    # Ljava/lang/String;
    .param p5, "value"    # Ljava/lang/String;

    .prologue
    .line 1299
    const/4 v12, 0x0

    .line 1301
    .local v12, "lastChangeNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDeviceBySID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v8

    .line 1302
    .local v8, "device":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-nez v8, :cond_1

    .line 1411
    :cond_0
    return-void

    .line 1304
    :cond_1
    const-string v2, "MainTVAgent2"

    invoke-virtual {v8, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getService(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v18

    .line 1307
    .local v18, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-eqz v18, :cond_2

    .line 1308
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .line 1310
    .local v13, "listener":Ljava/lang/Object;
    :try_start_0
    move-object v0, v13

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;

    move-object v2, v0

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1311
    :catch_0
    move-exception v9

    .line 1312
    .local v9, "e":Ljava/lang/Exception;
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    const-string v4, "MainTVAgent2 performEventListener (eventNotifyReceived)- Exception"

    invoke-static {v2, v3, v4, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 1315
    .end local v9    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v9

    .line 1316
    .local v9, "e":Ljava/lang/Error;
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    const-string v4, "MainTVAgent2 performEventListener -ERROR"

    invoke-static {v2, v3, v4, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_0

    .line 1325
    .end local v9    # "e":Ljava/lang/Error;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "listener":Ljava/lang/Object;
    :cond_2
    const-string v2, "A_ARG_TYPE_LastChange"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1326
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v16

    .line 1328
    .local v16, "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :try_start_1
    move-object/from16 v0, v16

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v17

    .line 1329
    .local v17, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    move-object/from16 v19, v17

    .line 1330
    .local v19, "stateEvent":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I
    :try_end_1
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_1 .. :try_end_1} :catch_5

    move-result v15

    .line 1331
    .local v15, "nodeCount":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-ge v10, v15, :cond_4

    .line 1333
    :try_start_2
    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v14

    .line 1334
    .local v14, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v14}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object p4

    .line 1335
    invoke-virtual {v14}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object p5

    .line 1336
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11    # "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_2 .. :try_end_2} :catch_5

    move-result-object v13

    .line 1338
    .restart local v13    # "listener":Ljava/lang/Object;
    :try_start_3
    move-object v0, v13

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;

    move-object v2, v0

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Error; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_3 .. :try_end_3} :catch_5

    goto :goto_2

    .line 1340
    :catch_2
    move-exception v9

    .line 1341
    .local v9, "e":Ljava/lang/Exception;
    :try_start_4
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    const-string v4, "performEventListener (eventNotifyReceived)- Exception"

    invoke-static {v2, v3, v4, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_4 .. :try_end_4} :catch_5

    goto :goto_2

    .line 1350
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "listener":Ljava/lang/Object;
    .end local v14    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catch_3
    move-exception v9

    .line 1351
    .restart local v9    # "e":Ljava/lang/Exception;
    :try_start_5
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    const-string v4, "performEventListener - node  Exception"

    invoke-static {v2, v3, v4, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_5 .. :try_end_5} :catch_5

    .line 1331
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 1344
    .restart local v11    # "i$":Ljava/util/Iterator;
    .restart local v13    # "listener":Ljava/lang/Object;
    .restart local v14    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catch_4
    move-exception v9

    .line 1345
    .local v9, "e":Ljava/lang/Error;
    :try_start_6
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    const-string v4, "performEventListener -ERROR"

    invoke-static {v2, v3, v4, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_2

    .line 1356
    .end local v9    # "e":Ljava/lang/Error;
    .end local v10    # "i":I
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "listener":Ljava/lang/Object;
    .end local v14    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v15    # "nodeCount":I
    .end local v17    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v19    # "stateEvent":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catch_5
    move-exception v9

    .line 1357
    .local v9, "e":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    const-string v4, "performEventListener - node ParserException "

    invoke-static {v2, v3, v4, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1363
    .end local v9    # "e":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    .end local v16    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :cond_4
    const-string v2, "LastChange"

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1364
    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->parseLastChangEvent(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v12

    .line 1365
    if-eqz v12, :cond_0

    .line 1368
    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object p4

    .line 1369
    const-string v2, "val"

    invoke-virtual {v12, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 1370
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .restart local v13    # "listener":Ljava/lang/Object;
    move-object v2, v13

    .line 1371
    check-cast v2, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1374
    .end local v13    # "listener":Ljava/lang/Object;
    :cond_5
    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v15

    .line 1375
    .restart local v15    # "nodeCount":I
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_4
    if-ge v10, v15, :cond_0

    .line 1377
    :try_start_7
    invoke-virtual {v12, v10}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v14

    .line 1378
    .restart local v14    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v14}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object p4

    .line 1379
    const-string v2, "val"

    invoke-virtual {v14, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p5

    .line 1380
    const-string v2, "TransportState"

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1381
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "performEventListener - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getFriendlyName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1384
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_5
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7

    move-result-object v13

    .line 1387
    .restart local v13    # "listener":Ljava/lang/Object;
    :try_start_8
    move-object v0, v13

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;

    move-object v2, v0

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/Error; {:try_start_8 .. :try_end_8} :catch_8

    goto :goto_5

    .line 1388
    :catch_6
    move-exception v9

    .line 1389
    .local v9, "e":Ljava/lang/Exception;
    :try_start_9
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    const-string v4, "performEventListener mEventListenerList Exception"

    invoke-static {v2, v3, v4, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_7

    goto :goto_5

    .line 1399
    .end local v9    # "e":Ljava/lang/Exception;
    .end local v13    # "listener":Ljava/lang/Object;
    .end local v14    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catch_7
    move-exception v9

    .line 1400
    .restart local v9    # "e":Ljava/lang/Exception;
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    const-string v4, "performEventListener Exception - LastChange Exception"

    invoke-static {v2, v3, v4, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1375
    .end local v9    # "e":Ljava/lang/Exception;
    :cond_7
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 1392
    .restart local v13    # "listener":Ljava/lang/Object;
    .restart local v14    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catch_8
    move-exception v9

    .line 1393
    .local v9, "e":Ljava/lang/Error;
    :try_start_a
    const-string v2, "ControlPoint"

    const-string v3, "performEventListener"

    const-string v4, "performEventListener  mEventListenerList Error"

    invoke-static {v2, v3, v4, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7

    goto :goto_5

    .line 1406
    .end local v9    # "e":Ljava/lang/Error;
    .end local v10    # "i":I
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "listener":Ljava/lang/Object;
    .end local v14    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v15    # "nodeCount":I
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .restart local v11    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    .restart local v13    # "listener":Ljava/lang/Object;
    move-object v2, v13

    .line 1408
    check-cast v2, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-interface/range {v2 .. v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;->eventNotifyReceived(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_6
.end method

.method protected performNotifyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 7
    .param p1, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p2, "ssdpPacket"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 654
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceNotifyListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->size()I

    move-result v2

    .line 655
    .local v2, "listenerSize":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v2, :cond_0

    .line 657
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceNotifyListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;

    .line 658
    .local v1, "listener":Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;
    invoke-interface {v1, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;->onSSDPReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 655
    .end local v1    # "listener":Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 659
    :catch_0
    move-exception v0

    .line 660
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "ControlPoint"

    const-string v5, "performNotifyListener"

    const-string v6, "performNotifyListener  : Exception"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 662
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 663
    .local v0, "e":Ljava/lang/Error;
    const-string v4, "ControlPoint"

    const-string v5, "performNotifyListener"

    const-string v6, "performNotifyListener : Error"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 667
    .end local v0    # "e":Ljava/lang/Error;
    :cond_0
    return-void
.end method

.method public performRemoveDeviceListener(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    .locals 7
    .param p1, "dev"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    .line 762
    monitor-enter p0

    .line 763
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceChangeListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 764
    .local v2, "listenerSize":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v2, :cond_0

    .line 766
    :try_start_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->cleanActionMap()V

    .line 768
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceChangeListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 769
    .local v1, "listener":Ljava/lang/Object;
    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/device/IDeviceChangeListener;

    .end local v1    # "listener":Ljava/lang/Object;
    invoke-interface {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/IDeviceChangeListener;->deviceRemoved(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Error; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 764
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 770
    :catch_0
    move-exception v0

    .line 771
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v4, "ControlPoint"

    const-string v5, "performRemoveDeviceListener"

    const-string v6, "performRemoveDeviceListener Exception"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 780
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "listenerSize":I
    .end local v3    # "n":I
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 774
    .restart local v2    # "listenerSize":I
    .restart local v3    # "n":I
    :catch_1
    move-exception v0

    .line 775
    .local v0, "e":Ljava/lang/Error;
    :try_start_3
    const-string v4, "ControlPoint"

    const-string v5, "performRemoveDeviceListener"

    const-string v6, "performRemoveDeviceListener Error"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 780
    .end local v0    # "e":Ljava/lang/Error;
    :cond_0
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 781
    return-void
.end method

.method public performSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 7
    .param p1, "ssdpPacket"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    .line 684
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceSearchResponseListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->size()I

    move-result v2

    .line 685
    .local v2, "listenerSize":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v2, :cond_0

    .line 687
    :try_start_0
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceSearchResponseListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISearchResponseListener;

    .line 689
    .local v1, "listener":Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISearchResponseListener;
    invoke-interface {v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISearchResponseListener;->deviceSearchResponseReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_1

    .line 685
    .end local v1    # "listener":Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISearchResponseListener;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 690
    :catch_0
    move-exception v0

    .line 691
    .local v0, "e":Ljava/lang/Exception;
    const-string v4, "ControlPoint"

    const-string v5, "performSearchResponseListener"

    const-string v6, "performSearchResponseListener Exception "

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 694
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 695
    .local v0, "e":Ljava/lang/Error;
    const-string v4, "ControlPoint"

    const-string v5, "performSearchResponseListener"

    const-string v6, "performSearchResponseListener  Error"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Error;)V

    goto :goto_1

    .line 700
    .end local v0    # "e":Ljava/lang/Error;
    :cond_0
    return-void
.end method

.method public refreshAll()V
    .locals 6

    .prologue
    .line 1075
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lez v3, :cond_1

    .line 1115
    :cond_0
    :goto_0
    return-void

    .line 1081
    :cond_1
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;

    .line 1082
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopyLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1083
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopy:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 1084
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1086
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->search()V

    .line 1088
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    if-eqz v3, :cond_3

    .line 1089
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 1090
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v3, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 1091
    .local v2, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v2, :cond_2

    .line 1092
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 1094
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_2

    .line 1095
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCountLock:[B

    monitor-enter v4

    .line 1096
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;

    .line 1097
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExeutor:Ljava/util/concurrent/ExecutorService;

    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;

    invoke-direct {v5, p0, v2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DeviceSniffer;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    invoke-interface {v3, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 1098
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1089
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1084
    .end local v1    # "n":I
    .end local v2    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catchall_0
    move-exception v3

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    .line 1098
    .restart local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .restart local v1    # "n":I
    .restart local v2    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :catchall_1
    move-exception v3

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3

    .line 1105
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v1    # "n":I
    .end local v2    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->deviceSnifferThreadCount:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gtz v3, :cond_0

    .line 1107
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopyLock:Ljava/lang/Object;

    monitor-enter v4

    .line 1108
    :try_start_4
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityListCopy:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 1109
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1111
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mActivityList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    .line 1112
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeActivityList()V

    goto :goto_0

    .line 1109
    :catchall_2
    move-exception v3

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v3
.end method

.method public refreshSsdpNotifyListenSocket()V
    .locals 3

    .prologue
    .line 1037
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    const/16 v2, 0x76c

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->getNotifyListenSocketList(I)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;

    move-result-object v0

    .line 1039
    .local v0, "ssdpNotifySocketList":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->setNotifiyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    .line 1040
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->start()V

    .line 1041
    return-void
.end method

.method public refreshTarget(Ljava/lang/String;)V
    .locals 8
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 1222
    const/4 v3, 0x1

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setRefreshMode(Z)V

    .line 1225
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    if-eqz v3, :cond_1

    .line 1226
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1227
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v3, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 1228
    .local v2, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v2, :cond_0

    .line 1229
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 1230
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1231
    const/16 v3, 0x1388

    invoke-virtual {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setExpiredTime(I)V

    .line 1233
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHandlerCheckExipredDevice:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRunnableCheckExipredDevice:Ljava/lang/Runnable;

    const-wide/16 v6, 0x1388

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1226
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1240
    .end local v1    # "n":I
    .end local v2    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_1
    if-eqz p1, :cond_2

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->isValidTarget(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1244
    :cond_2
    :goto_1
    return-void

    .line 1243
    :cond_3
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->searchTarget(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public removeDevNode()V
    .locals 4

    .prologue
    .line 1586
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevNodeList()Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    move-result-object v2

    .line 1587
    .local v2, "nodeList":Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 1588
    .local v1, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    goto :goto_0

    .line 1589
    .end local v1    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_0
    return-void
.end method

.method public removeDeviceChangeListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/IDeviceChangeListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/IDeviceChangeListener;

    .prologue
    .line 721
    monitor-enter p0

    .line 722
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceChangeListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->remove(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit p0

    return v0

    .line 723
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removeDeviceItem(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 530
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevice(Ljava/lang/String;)V

    .line 531
    return-void
.end method

.method public removeDeviceList(Ljava/lang/String;)V
    .locals 6
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 406
    if-nez p1, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_0

    .line 410
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 411
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 412
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 409
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 414
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 415
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "ControlPoint"

    const-string v4, "removeDeviceList"

    const-string v5, "removeDeviceList exception catched"

    invoke-static {v3, v4, v5, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public removeEventListener(Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/IEventListener;

    .prologue
    .line 1295
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mEventListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public removeExpiredDevices()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 542
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v1

    .line 543
    .local v1, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v0

    .line 545
    .local v0, "devCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v0, :cond_2

    .line 546
    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 547
    .local v2, "device":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isExpired()Z

    move-result v5

    if-ne v5, v6, :cond_0

    .line 549
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setForceExpired(Z)V

    .line 550
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;

    invoke-direct {v5, p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$TaskCheckExpiredDevice;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 551
    .local v4, "t":Ljava/lang/Thread;
    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 554
    .end local v4    # "t":Ljava/lang/Thread;
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->isRealExpired()Z

    move-result v5

    if-ne v5, v6, :cond_1

    .line 555
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevice(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    .line 545
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 559
    .end local v2    # "device":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_2
    return-void
.end method

.method public removeSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISearchResponseListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISearchResponseListener;

    .prologue
    .line 680
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceSearchResponseListenerList:Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/util/ListenerList;->remove(Ljava/lang/Object;)Z

    .line 681
    return-void
.end method

.method public renewSubscriberService()V
    .locals 2

    .prologue
    .line 1622
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->renewSubscriberService(J)V

    .line 1623
    return-void
.end method

.method public renewSubscriberService(J)V
    .locals 5
    .param p1, "timeout"    # J

    .prologue
    .line 1613
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 1614
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 1615
    .local v1, "devCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 1616
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 1617
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->renewSubscriberService(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;J)V

    .line 1615
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1619
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_0
    return-void
.end method

.method public renewSubscriberService(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;J)V
    .locals 10
    .param p1, "dev"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .param p2, "timeout"    # J

    .prologue
    .line 1592
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v7

    .line 1593
    .local v7, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v6

    .line 1594
    .local v6, "serviceCnt":I
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-ge v4, v6, :cond_2

    .line 1595
    invoke-virtual {v7, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v5

    .line 1596
    .local v5, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isSubscribed()Z

    move-result v9

    if-nez v9, :cond_1

    .line 1594
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1598
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v8

    .line 1599
    .local v8, "sid":Ljava/lang/String;
    invoke-virtual {p0, v5, v8, p2, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->subscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Ljava/lang/String;J)Z

    move-result v3

    .line 1600
    .local v3, "isRenewed":Z
    if-nez v3, :cond_0

    .line 1601
    invoke-virtual {p0, v5, p2, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->subscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;J)Z

    goto :goto_1

    .line 1604
    .end local v3    # "isRenewed":Z
    .end local v5    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .end local v8    # "sid":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 1605
    .local v2, "cdevList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 1606
    .local v1, "cdevCnt":I
    const/4 v4, 0x0

    :goto_2
    if-ge v4, v1, :cond_3

    .line 1607
    invoke-virtual {v2, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 1608
    .local v0, "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {p0, v0, p2, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->renewSubscriberService(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;J)V

    .line 1606
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1610
    .end local v0    # "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_3
    return-void
.end method

.method public search()V
    .locals 6

    .prologue
    .line 1008
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    if-nez v2, :cond_1

    .line 1023
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExeutor:Ljava/util/concurrent/ExecutorService;

    if-eqz v2, :cond_0

    .line 1016
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExeutor:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$1;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 1017
    :catch_0
    move-exception v1

    .line 1018
    .local v1, "re":Ljava/util/concurrent/RejectedExecutionException;
    const-string v2, "ControlPoint"

    const-string v3, "search"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Search: RejectedExecutionException"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1019
    .end local v1    # "re":Ljava/util/concurrent/RejectedExecutionException;
    :catch_1
    move-exception v0

    .line 1020
    .local v0, "ex":Ljava/lang/Exception;
    const-string v2, "ControlPoint"

    const-string v3, "search"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Search: Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public searchTarget(Ljava/lang/String;)V
    .locals 4
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 1026
    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->isValidTarget(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1034
    :cond_0
    :goto_0
    return-void

    .line 1029
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 1032
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;

    invoke-direct {v1, p0, p1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1033
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;

    invoke-direct {v1, p0, p1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$1;)V

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getSearchMx()I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public setDiscoveryInfos([Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V
    .locals 1
    .param p1, "infos"    # [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .prologue
    .line 205
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->setDiscoveryInfo([Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    .line 206
    return-void
.end method

.method public start()Z
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 1644
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->stop()Z

    .line 1652
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1653
    :cond_0
    new-instance v7, Landroid/os/HandlerThread;

    const-string v8, "Device Manager"

    invoke-direct {v7, v8}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    .line 1654
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    invoke-virtual {v7}, Landroid/os/HandlerThread;->start()V

    .line 1655
    new-instance v7, Landroid/os/Handler;

    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    invoke-virtual {v8}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    .line 1656
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;

    new-instance v8, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$3;

    invoke-direct {v8, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$3;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1667
    :cond_1
    const/4 v4, 0x0

    .line 1668
    .local v4, "retryCnt":I
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getHTTPPort()I

    move-result v0

    .line 1669
    .local v0, "bindPort":I
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getHTTPServerList()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    move-result-object v2

    .line 1671
    .local v2, "httpServerList":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;
    :goto_0
    invoke-virtual {v2, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->open(I)Z

    move-result v7

    if-nez v7, :cond_4

    .line 1672
    add-int/lit8 v4, v4, 0x1

    .line 1673
    const/16 v7, 0x64

    if-ge v7, v4, :cond_3

    .line 1715
    :cond_2
    :goto_1
    return v6

    .line 1675
    :cond_3
    add-int/lit8 v7, v0, 0x1

    invoke-direct {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setHTTPPort(I)V

    .line 1676
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getHTTPPort()I

    move-result v0

    goto :goto_0

    .line 1679
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->size()I

    move-result v7

    if-eqz v7, :cond_2

    .line 1682
    invoke-virtual {v2, p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->addRequestListener(Lcom/samsung/android/allshare/stack/upnp/http/IHTTPRequestListener;)V

    .line 1683
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->start()V

    .line 1690
    iget-object v6, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    const/16 v7, 0x76c

    invoke-virtual {v6, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->getNotifyListenSocketList(I)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;

    move-result-object v5

    .line 1692
    .local v5, "ssdpNotifySocketList":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;
    invoke-virtual {v5, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->setNotifiyListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    .line 1693
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifyListenSocketList;->start()V

    .line 1696
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->search()V

    .line 1702
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;)V

    .line 1703
    .local v1, "disposer":Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;
    const-string v6, "Device Disposer"

    invoke-virtual {v1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;->setName(Ljava/lang/String;)V

    .line 1704
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setDeviceDisposer(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;)V

    .line 1705
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;->start()V

    .line 1711
    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;

    invoke-direct {v3, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;)V

    .line 1712
    .local v3, "renewSub":Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;
    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setRenewSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;)V

    .line 1713
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;->start()V

    .line 1715
    const/4 v6, 0x1

    goto :goto_1
.end method

.method public stop()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1719
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->unsubscribe()V

    .line 1720
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeDevNode()V

    .line 1722
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->cleanup()V

    .line 1724
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getHTTPServerList()Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;

    move-result-object v1

    .line 1725
    .local v1, "httpServerList":Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->stop()V

    .line 1726
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->close()Z

    .line 1727
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerList;->clear()V

    .line 1730
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDevNodeList:Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/xml/NodeList;->clear()V

    .line 1736
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDeviceDisposer()Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;

    move-result-object v0

    .line 1737
    .local v0, "disposer":Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;
    if-eqz v0, :cond_0

    .line 1738
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;->stop()V

    .line 1739
    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setDeviceDisposer(Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;)V

    .line 1746
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getRenewSubscriber()Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;

    move-result-object v2

    .line 1747
    .local v2, "renewSub":Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;
    if-eqz v2, :cond_1

    .line 1748
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;->stop()V

    .line 1749
    invoke-direct {p0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setRenewSubscriber(Lcom/samsung/android/allshare/stack/upnp/upnp/control/RenewSubscriber;)V

    .line 1755
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    if-eqz v3, :cond_3

    .line 1756
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->quit()Z

    .line 1757
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v3

    if-ne v3, v5, :cond_2

    .line 1758
    invoke-static {}, Ljava/lang/Thread;->yield()V

    goto :goto_0

    .line 1759
    :cond_2
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceManager:Landroid/os/HandlerThread;

    .line 1762
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExeutor:Ljava/util/concurrent/ExecutorService;

    if-eqz v3, :cond_4

    .line 1763
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExeutor:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 1765
    :cond_4
    return v5
.end method

.method public subscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)Z
    .locals 2
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .prologue
    .line 1501
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->subscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;J)Z

    move-result v0

    return v0
.end method

.method public subscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;J)Z
    .locals 12
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .param p2, "timeout"    # J

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1450
    if-nez p1, :cond_1

    .line 1497
    :cond_0
    :goto_0
    return v7

    .line 1453
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->isSubscribed()Z

    move-result v9

    if-ne v9, v8, :cond_2

    .line 1454
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v3

    .line 1455
    .local v3, "sid":Ljava/lang/String;
    invoke-virtual {p0, p1, v3, p2, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->subscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Ljava/lang/String;J)Z

    move-result v7

    goto :goto_0

    .line 1458
    .end local v3    # "sid":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v2

    .line 1459
    .local v2, "rootDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v2, :cond_0

    .line 1463
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getSSDPPacket()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    move-result-object v4

    .line 1464
    .local v4, "ssdpPacket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    if-eqz v4, :cond_0

    .line 1467
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getRemoteAddress()Ljava/net/InetAddress;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v1

    .line 1469
    .local v1, "ifAddress":Ljava/net/InetAddress;
    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;

    invoke-direct {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;-><init>()V

    .line 1470
    .local v5, "subReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getEventSubCallbackURL(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, p1, v9, p2, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setSubscribeRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Ljava/lang/String;J)V

    .line 1471
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->post()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;

    move-result-object v6

    .line 1473
    .local v6, "subRes":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->isSuccessful()Z

    move-result v9

    if-ne v9, v8, :cond_3

    .line 1474
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->getSID()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setSID(Ljava/lang/String;)V

    .line 1475
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->getTimeout()J

    move-result-wide v10

    invoke-virtual {p1, v10, v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setTimeout(J)V

    .line 1476
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->resetRetryCount()V

    move v7, v8

    .line 1477
    goto :goto_0

    .line 1479
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRetryCount()I

    move-result v8

    const/4 v9, 0x3

    if-ge v8, v9, :cond_4

    .line 1480
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->increaseRetryCount()V

    .line 1482
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->unsubscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)Z

    .line 1485
    const-wide/16 v8, 0x64

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1490
    :goto_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->subscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;J)Z

    .line 1496
    :goto_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->clearSID()V

    goto :goto_0

    .line 1486
    :catch_0
    move-exception v0

    .line 1487
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 1492
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->resetRetryCount()V

    goto :goto_2
.end method

.method public subscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Ljava/lang/String;J)Z
    .locals 7
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .param p2, "sid"    # Ljava/lang/String;
    .param p3, "timeout"    # J

    .prologue
    const/4 v3, 0x1

    .line 1506
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;-><init>()V

    .line 1507
    .local v1, "subReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setRenewRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Ljava/lang/String;J)V

    .line 1508
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->print()V

    .line 1509
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->post()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;

    move-result-object v2

    .line 1510
    .local v2, "subRes":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->print()V

    .line 1515
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->getStatusCode()I

    move-result v4

    const/16 v5, 0x19a

    if-ne v4, v5, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1516
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setForceExpired(Z)V

    .line 1517
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->requestDisposer()V

    .line 1520
    :cond_0
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->isSuccessful()Z

    move-result v4

    if-ne v4, v3, :cond_1

    .line 1522
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->getSID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setSID(Ljava/lang/String;)V

    .line 1523
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->getTimeout()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->setTimeout(J)V

    .line 1524
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->resetRetryCount()V

    .line 1545
    :goto_0
    return v3

    .line 1528
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRetryCount()I

    move-result v3

    const/4 v4, 0x3

    if-ge v3, v4, :cond_2

    .line 1529
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->increaseRetryCount()V

    .line 1533
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1538
    :goto_1
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->subscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Ljava/lang/String;J)Z

    .line 1544
    :goto_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->clearSID()V

    .line 1545
    const/4 v3, 0x0

    goto :goto_0

    .line 1534
    :catch_0
    move-exception v0

    .line 1535
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 1540
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->resetRetryCount()V

    goto :goto_2
.end method

.method public unsubscribe()V
    .locals 4

    .prologue
    .line 1577
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 1578
    .local v2, "devList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 1579
    .local v1, "devCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v1, :cond_0

    .line 1580
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 1581
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->unsubscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    .line 1579
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1583
    .end local v0    # "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_0
    return-void
.end method

.method public unsubscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    .locals 9
    .param p1, "device"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    .line 1560
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getServiceList()Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;

    move-result-object v6

    .line 1561
    .local v6, "serviceList":Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->size()I

    move-result v5

    .line 1562
    .local v5, "serviceCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v5, :cond_1

    .line 1563
    invoke-virtual {v6, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceList;->getService(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v4

    .line 1564
    .local v4, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->hasSID()Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 1565
    invoke-virtual {p0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->unsubscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)Z

    .line 1562
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1568
    .end local v4    # "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getDeviceList()Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;

    move-result-object v2

    .line 1569
    .local v2, "childDevList":Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->size()I

    move-result v1

    .line 1570
    .local v1, "childDevCnt":I
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_2

    .line 1571
    invoke-virtual {v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/DeviceList;->getDevice(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 1572
    .local v0, "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->unsubscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    .line 1570
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1574
    .end local v0    # "cdev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_2
    return-void
.end method

.method public unsubscribe(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)Z
    .locals 4
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .prologue
    const/4 v2, 0x1

    .line 1549
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;-><init>()V

    .line 1550
    .local v0, "subReq":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;
    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setUnsubscribeRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V

    .line 1551
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->post()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;

    move-result-object v1

    .line 1552
    .local v1, "subRes":Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;->isSuccessful()Z

    move-result v3

    if-ne v3, v2, :cond_0

    .line 1553
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->clearSID()V

    .line 1556
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public updateName(Ljava/lang/String;)V
    .locals 0
    .param p1, "newName"    # Ljava/lang/String;

    .prologue
    .line 640
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->setDeviceName(Ljava/lang/String;)Z

    .line 641
    return-void
.end method
