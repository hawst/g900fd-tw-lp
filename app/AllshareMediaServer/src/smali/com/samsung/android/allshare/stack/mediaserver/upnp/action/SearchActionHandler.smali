.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;
.super Ljava/lang/Object;
.source "SearchActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$1;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;
    }
.end annotation


# instance fields
.field private mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V
    .locals 1
    .param p1, "mediaServer"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 82
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 83
    return-void
.end method

.method private collectAllItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Ljava/util/List;
    .locals 7
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 229
    .local v0, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    if-eqz p1, :cond_0

    instance-of v6, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    if-eqz v6, :cond_1

    .line 249
    :cond_0
    return-object v0

    .line 233
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getItems()Ljava/util/Collection;

    move-result-object v5

    .line 234
    .local v5, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;>;"
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .line 235
    .local v4, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    instance-of v6, v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    if-nez v6, :cond_2

    .line 237
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 240
    .end local v4    # "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getDirectories()Ljava/util/ArrayList;

    move-result-object v2

    .line 242
    .local v2, "dirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .line 243
    .local v1, "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-eqz v1, :cond_4

    instance-of v6, v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    if-nez v6, :cond_4

    .line 244
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->toContentNode()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->collectAllItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method private collectAllItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .param p2, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 203
    .local v0, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    if-eqz p1, :cond_0

    instance-of v6, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    if-eqz v6, :cond_1

    .line 223
    :cond_0
    return-object v0

    .line 206
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getItems()Ljava/util/Collection;

    move-result-object v5

    .line 207
    .local v5, "items":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;>;"
    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .line 208
    .local v4, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    instance-of v6, v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/IHidden;

    if-nez v6, :cond_2

    .line 210
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 213
    .end local v4    # "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v2, "dirs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;>;"
    invoke-virtual {p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;->getDirectoriesByMimeType(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 217
    if-eqz v2, :cond_0

    .line 218
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    .line 219
    .local v1, "d":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    invoke-direct {p0, v1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->collectAllItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;Ljava/lang/String;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method

.method private collectClassMatchingItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;Ljava/util/ArrayList;)Ljava/util/List;
    .locals 6
    .param p1, "dir"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    .local p2, "postfixSearchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 174
    .local v0, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    invoke-static {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->getSearchItemTypes(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 177
    .local v2, "itemTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;

    .line 178
    .local v3, "type":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$1;->$SwitchMap$com$samsung$android$allshare$stack$mediaserver$upnp$object$ContentSearcher$SearchContentType:[I

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 180
    :pswitch_0
    const-string v4, "video/*"

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->collectAllItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 184
    :pswitch_1
    const-string v4, "audio/*"

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->collectAllItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 188
    :pswitch_2
    const-string v4, "image/*"

    invoke-direct {p0, p1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->collectAllItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 192
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->collectAllItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 197
    .end local v3    # "type":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$SearchContentType;
    :cond_0
    return-object v0

    .line 178
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private handleSearchAction(Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;Ljava/net/InetAddress;)Z
    .locals 17
    .param p1, "search"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 92
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->getContainerID()Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "contaierID":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v14, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->findDirectoryByID(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;

    move-result-object v5

    .line 94
    .local v5, "dir":Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;
    if-nez v5, :cond_0

    .line 95
    const/16 v14, 0x192

    const-string v15, "No such container"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->setStatus(ILjava/lang/String;)V

    .line 96
    const/4 v14, 0x0

    .line 167
    :goto_0
    return v14

    .line 100
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->getSearchCriteria()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->checkSearchCriteriaSyntax(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v8

    .line 102
    .local v8, "postfixSearchList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/search/SearchCriteria;>;"
    if-nez v8, :cond_1

    .line 104
    const/16 v14, 0x2c4

    const-string v15, "invalid search criteria"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->setStatus(ILjava/lang/String;)V

    .line 105
    const/4 v14, 0x0

    goto :goto_0

    .line 109
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v5, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->collectClassMatchingItems(Lcom/samsung/android/allshare/stack/mediaserver/upnp/Directory;Ljava/util/ArrayList;)Ljava/util/List;

    move-result-object v4

    .line 112
    .local v4, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    invoke-static {v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->needSearch(Ljava/util/ArrayList;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 113
    invoke-static {v4, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;->search(Ljava/util/List;Ljava/util/ArrayList;)Ljava/util/List;

    move-result-object v4

    .line 115
    :cond_2
    if-nez v4, :cond_3

    .line 119
    const/16 v14, 0x2c4

    const-string v15, "invalid search criteria"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->setStatus(ILjava/lang/String;)V

    .line 120
    const/4 v14, 0x0

    goto :goto_0

    .line 123
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v13

    .line 126
    .local v13, "totalMatch":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->getSortCriteria()Ljava/lang/String;

    move-result-object v11

    .line 127
    .local v11, "sortCriteria":Ljava/lang/String;
    invoke-static {v4, v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->sort(Ljava/util/List;Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_4

    .line 131
    const/16 v14, 0x2c5

    const-string v15, "invalid sort criteria"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->setStatus(ILjava/lang/String;)V

    .line 132
    const/4 v14, 0x0

    goto :goto_0

    .line 136
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->getStartingIndex()I

    move-result v12

    .line 137
    .local v12, "startingIndex":I
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->getRequestedCount()I

    move-result v9

    .line 138
    .local v9, "requestCount":I
    invoke-static {v4, v12, v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentListSpliter;->split(Ljava/util/List;II)Ljava/util/List;

    move-result-object v4

    .line 140
    if-nez v4, :cond_5

    .line 141
    const/16 v14, 0x192

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->setStatus(I)V

    .line 142
    const/4 v14, 0x0

    goto :goto_0

    .line 146
    :cond_5
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .line 147
    .local v3, "content":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->update()V

    goto :goto_1

    .line 151
    .end local v3    # "content":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->getFilter()Ljava/lang/String;

    move-result-object v6

    .line 152
    .local v6, "filterCondition":Ljava/lang/String;
    invoke-static {v4, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/FilterApplier;->apply(Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 155
    invoke-static/range {p2 .. p2}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getBoundInetAddress(Ljava/net/InetAddress;)Ljava/net/InetAddress;

    move-result-object v1

    .line 156
    .local v1, "addr":Ljava/net/InetAddress;
    const-string v10, ""

    .line 157
    .local v10, "result":Ljava/lang/String;
    if-eqz v1, :cond_7

    .line 158
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getHTTPPort()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getStreamingPort(Ljava/lang/String;)I

    move-result v15

    move-object/from16 v0, p2

    invoke-static {v4, v0, v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentXMLPrinter;->print(Ljava/util/Collection;Ljava/net/InetAddress;II)Ljava/lang/String;

    move-result-object v10

    .line 163
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->setResult(Ljava/lang/String;)V

    .line 164
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->setNumberReturned(I)V

    .line 165
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->setTotalMaches(I)V

    .line 166
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->getSystemUpdateID()I

    move-result v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;->setUpdateID(I)V

    .line 167
    const/4 v14, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 2
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 87
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;

    invoke-direct {v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 88
    .local v0, "search":Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;
    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler;->handleSearchAction(Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/SearchActionHandler$SearchAction;Ljava/net/InetAddress;)Z

    move-result v1

    return v1
.end method
