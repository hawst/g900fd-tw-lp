.class public Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;
.source "MediaStoreMediaServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer$1;
    }
.end annotation


# static fields
.field private static CACHE_DIR:Ljava/lang/String; = null

.field private static final CACHE_MEDIA_SERVER_PATH:Ljava/lang/String; = "/Android/data/com.samsung.android.allshare/MediaServer/"

.field private static final CACHE_SDCARD_PATH:Ljava/lang/String;

.field private static final FRIENDLY_NAME:Ljava/lang/String; = "friendlyName"

.field private static final TAG:Ljava/lang/String; = "MediaStoreMediaServer"

.field private static final WORKER_NAME:Ljava/lang/String; = "Media Update Worker"


# instance fields
.field private mContextRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private mDrmClient:Landroid/drm/DrmManagerClient;

.field private mUpdateWorker:Landroid/os/HandlerThread;

.field private mUpdater:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->CACHE_DIR:Ljava/lang/String;

    .line 48
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->CACHE_SDCARD_PATH:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 61
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;-><init>()V

    .line 53
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    .line 55
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    .line 57
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdater:Landroid/os/Handler;

    .line 59
    iput-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 62
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    .line 65
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setUploadPath(Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$Factory;

    invoke-direct {v1, p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$Factory;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setImportResourceFactory(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;)V

    .line 73
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/xml/parser/DoaXMLParser;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/xml/parser/DoaXMLParser;-><init>()V

    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnP;->setXMLParser(Lcom/samsung/android/allshare/stack/upnp/xml/Parser;)V

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->CACHE_SDCARD_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Android/data/com.samsung.android.allshare/MediaServer/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->CACHE_DIR:Ljava/lang/String;

    .line 77
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    .local v0, "ex":Ljava/lang/NullPointerException;
    const-string v1, "MediaStoreMediaServer"

    const-string v2, "MediaStoreMediaServer"

    const-string v3, "Problem in sd card. So use the default path"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setUploadPath(Ljava/io/File;)Z

    goto :goto_0
.end method

.method private closeStream(Ljava/io/InputStream;)V
    .locals 5
    .param p1, "stream"    # Ljava/io/InputStream;

    .prologue
    .line 355
    if-eqz p1, :cond_0

    .line 357
    :try_start_0
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 358
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MediaStoreMediaServer"

    const-string v2, "closeStream"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "closeStream catch IOException : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private opneResource(I)Ljava/io/InputStream;
    .locals 2
    .param p1, "res"    # I

    .prologue
    .line 342
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 343
    .local v0, "ctx":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 344
    const/4 v1, 0x0

    .line 346
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final getCacheDirectory()Ljava/io/File;
    .locals 4

    .prologue
    .line 306
    sget-object v3, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->CACHE_DIR:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/android/allshare/stack/util/PathUtil;->createCachePath(Ljava/lang/String;)Z

    move-result v2

    .line 308
    .local v2, "result":Z
    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 309
    new-instance v1, Ljava/io/File;

    sget-object v3, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->CACHE_DIR:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 320
    :goto_0
    return-object v1

    .line 313
    :cond_0
    const/4 v0, 0x0

    .line 314
    .local v0, "ctx":Landroid/content/Context;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    if-eqz v3, :cond_1

    .line 315
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "ctx":Landroid/content/Context;
    check-cast v0, Landroid/content/Context;

    .line 317
    .restart local v0    # "ctx":Landroid/content/Context;
    :cond_1
    if-eqz v0, :cond_2

    .line 318
    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    goto :goto_0

    .line 320
    :cond_2
    invoke-static {}, Landroid/os/Environment;->getDownloadCacheDirectory()Ljava/io/File;

    move-result-object v1

    goto :goto_0
.end method

.method public final getContentResolver()Landroid/content/ContentResolver;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 286
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    if-nez v2, :cond_1

    .line 294
    :cond_0
    :goto_0
    return-object v1

    .line 289
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 291
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_0

    .line 294
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    goto :goto_0
.end method

.method public final getDrmManagerClient()Landroid/drm/DrmManagerClient;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 258
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-nez v3, :cond_0

    .line 261
    :try_start_0
    const-string v3, "android.drm.DrmManagerClient"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 262
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 263
    .local v1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/drm/DrmManagerClient;

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 276
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    return-object v3

    .line 264
    :catch_0
    move-exception v2

    .line 265
    .local v2, "e":Ljava/lang/RuntimeException;
    const-string v3, "MediaStoreMediaServer"

    const-string v4, "MediaStoreMediaServer"

    const-string v5, "MediaStoreMediaServer RuntimeException android.drm.DrmManagerClient is not supported."

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    iput-object v6, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    goto :goto_0

    .line 268
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v2

    .line 270
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "MediaStoreMediaServer"

    const-string v4, "MediaStoreMediaServer"

    const-string v5, "MediaStoreMediaServer Exception android.drm.DrmManagerClient is not supported."

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    iput-object v6, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    goto :goto_0
.end method

.method public getMediaUpdateHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdater:Landroid/os/Handler;

    return-object v0
.end method

.method public initailize(IIILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "descRes"    # I
    .param p2, "cdsRes"    # I
    .param p3, "cmsRes"    # I
    .param p4, "friendlyName"    # Ljava/lang/String;
    .param p5, "udn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;
        }
    .end annotation

    .prologue
    .line 173
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->opneResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 174
    .local v1, "desc":Ljava/io/InputStream;
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->opneResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 175
    .local v2, "cds":Ljava/io/InputStream;
    invoke-direct {p0, p3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->opneResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 177
    .local v3, "cms":Ljava/io/InputStream;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    move-object v0, p0

    move-object v4, p4

    move-object v5, p5

    .line 178
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->initailize(Ljava/io/InputStream;Ljava/io/InputStream;Ljava/io/InputStream;Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_0
    if-eqz v1, :cond_1

    .line 182
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->closeStream(Ljava/io/InputStream;)V

    .line 184
    :cond_1
    if-eqz v2, :cond_2

    .line 185
    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->closeStream(Ljava/io/InputStream;)V

    .line 187
    :cond_2
    if-eqz v3, :cond_3

    .line 188
    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->closeStream(Ljava/io/InputStream;)V

    .line 190
    :cond_3
    return-void
.end method

.method public isUpdateWorkerAlive()Z
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    .line 329
    const/4 v0, 0x0

    .line 331
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isAlive()Z

    move-result v0

    goto :goto_0
.end method

.method public setFriendlyName(Ljava/lang/String;Z)Z
    .locals 2
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "needAnnounce"    # Z

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    const-string v1, "friendlyName"

    invoke-static {v0, v1, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/config/ConfigSetter;->updateDeviceConfig(Ljava/lang/ref/WeakReference;Ljava/lang/String;Ljava/lang/String;)Z

    .line 84
    invoke-super {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->setFriendlyName(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setIcon(ILcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V
    .locals 8
    .param p1, "iconRes"    # I
    .param p2, "type"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    .prologue
    .line 201
    const/4 v3, 0x0

    .line 202
    .local v3, "iconByte":[B
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 203
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v4, 0x0

    .line 205
    .local v4, "scale":Landroid/graphics/Bitmap;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v5, 0x4e20

    invoke-direct {v1, v5}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 206
    .local v1, "bos":Ljava/io/ByteArrayOutputStream;
    if-nez p2, :cond_0

    .line 249
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "scale":Landroid/graphics/Bitmap;
    :goto_0
    return-void

    .line 208
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "scale":Landroid/graphics/Bitmap;
    :cond_0
    sget-object v5, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer$1;->$SwitchMap$com$samsung$android$allshare$stack$mediaserver$upnp$MediaServer$ICON_TYPE:[I

    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 234
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 246
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "scale":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v2

    .line 247
    .local v2, "e":Ljava/io/IOException;
    const-string v5, "MediaStoreMediaServer"

    const-string v6, "setIcon"

    const-string v7, "setIcon - IOException"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 210
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v1    # "bos":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "scale":Landroid/graphics/Bitmap;
    :pswitch_0
    if-eqz v0, :cond_1

    .line 211
    const/16 v5, 0x30

    const/16 v6, 0x30

    const/4 v7, 0x0

    :try_start_1
    invoke-static {v0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 212
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {v4, v5, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 238
    :cond_1
    :goto_1
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 239
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 240
    if-eqz v0, :cond_2

    .line 241
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 242
    :cond_2
    if-eqz v4, :cond_3

    .line 243
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 245
    :cond_3
    invoke-virtual {p0, v3, p2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->setIcon([BLcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;)V

    goto :goto_0

    .line 216
    :pswitch_1
    if-eqz v0, :cond_1

    .line 217
    const/16 v5, 0x78

    const/16 v6, 0x78

    const/4 v7, 0x0

    invoke-static {v0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 218
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {v4, v5, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    goto :goto_1

    .line 222
    :pswitch_2
    if-eqz v0, :cond_1

    .line 223
    const/16 v5, 0x30

    const/16 v6, 0x30

    const/4 v7, 0x0

    invoke-static {v0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 224
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {v4, v5, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    goto :goto_1

    .line 228
    :pswitch_3
    if-eqz v0, :cond_1

    .line 229
    const/16 v5, 0x78

    const/16 v6, 0x78

    const/4 v7, 0x0

    invoke-static {v0, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 230
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {v4, v5, v6, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 208
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public start()Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 91
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v3

    if-nez v3, :cond_0

    .line 92
    new-instance v3, Landroid/os/HandlerThread;

    const-string v4, "Media Update Worker"

    invoke-direct {v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    .line 93
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->start()V

    .line 94
    new-instance v3, Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdater:Landroid/os/Handler;

    .line 96
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-nez v3, :cond_0

    .line 99
    :try_start_0
    const-string v3, "android.drm.DrmManagerClient"

    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 100
    .local v0, "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/content/Context;

    aput-object v5, v3, v4

    invoke-virtual {v0, v3}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 101
    .local v1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/drm/DrmManagerClient;

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 115
    .end local v0    # "c":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v1    # "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->start()Z

    move-result v3

    return v3

    .line 102
    :catch_0
    move-exception v2

    .line 103
    .local v2, "e":Ljava/lang/RuntimeException;
    const-string v3, "MediaStoreMediaServer"

    const-string v4, "MediaStoreMediaServer"

    const-string v5, "MediaStoreMediaServer RuntimeException android.drm.DrmManagerClient is not supported."

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    iput-object v6, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    goto :goto_0

    .line 106
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v2

    .line 108
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "MediaStoreMediaServer"

    const-string v4, "MediaStoreMediaServer"

    const-string v5, "MediaStoreMediaServer Exception android.drm.DrmManagerClient is not supported."

    invoke-static {v3, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iput-object v6, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    goto :goto_0
.end method

.method public stop(Z)Z
    .locals 2
    .param p1, "doByeBye"    # Z

    .prologue
    const/4 v1, 0x1

    .line 121
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v0

    if-ne v0, v1, :cond_1

    .line 123
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0}, Landroid/drm/DrmManagerClient;->release()V

    .line 127
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 129
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    .line 130
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 134
    :cond_1
    invoke-virtual {p0, p1, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->stop(ZZ)Z

    move-result v0

    return v0
.end method

.method public stop(ZZ)Z
    .locals 2
    .param p1, "doByeBye"    # Z
    .param p2, "removeCache"    # Z

    .prologue
    .line 140
    if-eqz p2, :cond_0

    .line 141
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->CACHE_DIR:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/util/PathUtil;->clearCachePath(Ljava/lang/String;)V

    .line 143
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->isServerStarted()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 145
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0}, Landroid/drm/DrmManagerClient;->release()V

    .line 149
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mDrmClient:Landroid/drm/DrmManagerClient;

    .line 151
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    if-eqz v0, :cond_2

    .line 152
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->mUpdateWorker:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 156
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->stop(ZZ)Z

    move-result v0

    return v0
.end method
