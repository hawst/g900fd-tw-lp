.class public Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;
.super Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
.source "SOAPResponse.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SOAPResponse"


# instance fields
.field private mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private final mTAG:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;-><init>()V

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->mTAG:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAP;->createEnvelopeBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setRootNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 51
    const-string v0, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setContentType(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V
    .locals 5
    .param p1, "httpRes"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->mTAG:Ljava/lang/String;

    .line 57
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 58
    .local v1, "is":Ljava/io/InputStream;
    invoke-direct {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setEnvelopeNode(Ljava/io/InputStream;)V

    .line 59
    const-string v2, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setContentType(Ljava/lang/String;)V

    .line 60
    if-eqz v1, :cond_0

    .line 62
    :try_start_0
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "SOAPResponse"

    const-string v3, "SOAPResponse"

    const-string v4, "SOAPResponse-IOException "

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;)V
    .locals 1
    .param p1, "soapRes"    # Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->mTAG:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setEnvelopeNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 72
    const-string v0, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setContentType(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method private getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method private setEnvelopeNode(Ljava/io/InputStream;)V
    .locals 6
    .param p1, "is"    # Ljava/io/InputStream;

    .prologue
    .line 93
    if-nez p1, :cond_0

    .line 94
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAP;->createEnvelopeBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setRootNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 107
    :goto_0
    return-void

    .line 99
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v2

    .line 100
    .local v2, "xmlParser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    invoke-virtual {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 101
    .local v1, "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setEnvelopeNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 102
    .end local v1    # "rootNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v2    # "xmlParser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->mTAG:Ljava/lang/String;

    const-string v4, "setEnvelopeNode"

    const-string v5, "setEnvelpoeNode : Exception"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 104
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAP;->createEnvelopeBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setRootNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    goto :goto_0
.end method

.method private setRootNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 83
    return-void
.end method


# virtual methods
.method public getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 120
    .local v0, "envNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 121
    const/4 v1, 0x0

    .line 122
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "Body"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method public getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    return-object v0
.end method

.method public getFaultDetailNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->getFaultNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 167
    .local v0, "faultNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 168
    const/4 v1, 0x0

    .line 169
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "detail"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method public getFaultNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2

    .prologue
    .line 135
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 136
    .local v0, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    .line 137
    const/4 v1, 0x0

    .line 138
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "Fault"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeEndsWith(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method public print()V
    .locals 0

    .prologue
    .line 223
    return-void
.end method

.method public setContent(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 3
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 203
    const-string v0, ""

    .line 204
    .local v0, "conStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 205
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setContent(Ljava/lang/String;)V

    .line 208
    return-void
.end method

.method public setEnvelopeNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;->setRootNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 112
    return-void
.end method
