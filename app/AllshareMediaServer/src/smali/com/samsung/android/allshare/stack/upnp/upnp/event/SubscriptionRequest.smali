.class public Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;
.super Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
.source "SubscriptionRequest.java"


# static fields
.field private static final CALLBACK_END_WITH:Ljava/lang/String; = ">"

.field private static final CALLBACK_START_WITH:Ljava/lang/String; = "<"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;-><init>()V

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 0
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;-><init>()V

    .line 58
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 59
    return-void
.end method

.method private setService(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V
    .locals 7
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .prologue
    .line 66
    if-nez p1, :cond_1

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getEventSubURL()Ljava/lang/String;

    move-result-object v1

    .line 72
    .local v1, "eventSubURL":Ljava/lang/String;
    const/4 v6, 0x1

    invoke-virtual {p0, v1, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setURI(Ljava/lang/String;Z)V

    .line 74
    const-string v5, ""

    .line 75
    .local v5, "urlBaseStr":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 76
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_2

    .line 77
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getURLBase()Ljava/lang/String;

    move-result-object v5

    .line 79
    :cond_2
    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_4

    .line 80
    :cond_3
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v4

    .line 81
    .local v4, "rootDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v4, :cond_4

    .line 82
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getURLBase()Ljava/lang/String;

    move-result-object v5

    .line 87
    .end local v4    # "rootDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_4
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_6

    .line 88
    :cond_5
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v4

    .line 89
    .restart local v4    # "rootDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v4, :cond_6

    .line 90
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v5

    .line 94
    .end local v4    # "rootDev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    :cond_6
    if-eqz v5, :cond_7

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-gtz v6, :cond_8

    .line 95
    :cond_7
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->isAbsoluteURL(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 96
    move-object v5, v1

    .line 98
    :cond_8
    if-eqz v5, :cond_0

    .line 100
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getHost(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "reqHost":Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getPort(Ljava/lang/String;)I

    move-result v3

    .line 103
    .local v3, "reqPort":I
    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setHost(Ljava/lang/String;I)V

    .line 104
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setRequestHost(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setRequestPort(I)V

    goto :goto_0
.end method


# virtual methods
.method public getCallback()Ljava/lang/String;
    .locals 3

    .prologue
    .line 161
    const-string v0, "CALLBACK"

    const-string v1, "<"

    const-string v2, ">"

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getStringHeaderValue(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNT()Ljava/lang/String;
    .locals 2

    .prologue
    .line 201
    const-string v1, "NT"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 202
    .local v0, "nt":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 203
    const-string v0, ""

    .line 204
    .end local v0    # "nt":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getSID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 179
    const-string v1, "SID"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "sid":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 181
    const-string v0, ""

    .line 182
    .end local v0    # "sid":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getTimeout()J
    .locals 2

    .prologue
    .line 216
    const-string v0, "TIMEOUT"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscription;->getTimeout(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public hasCallback()Z
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getCallback()Ljava/lang/String;

    move-result-object v0

    .line 166
    .local v0, "callback":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasNT()Z
    .locals 2

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getNT()Ljava/lang/String;

    move-result-object v0

    .line 196
    .local v0, "nt":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasSID()Z
    .locals 2

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getSID()Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "sid":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public post()Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;
    .locals 3

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getRequestHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->getRequestPort()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->post(Ljava/lang/String;I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v0

    .line 233
    .local v0, "httpRes":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;

    invoke-direct {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V

    return-object v1
.end method

.method public post(Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;)V
    .locals 0
    .param p1, "subRes"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionResponse;

    .prologue
    .line 224
    invoke-super {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    .line 225
    return-void
.end method

.method public setCallback(Ljava/lang/String;)V
    .locals 3
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 157
    const-string v0, "CALLBACK"

    const-string v1, "<"

    const-string v2, ">"

    invoke-virtual {p0, v0, p1, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setStringHeader(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    return-void
.end method

.method public setNT(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 134
    const-string v0, "NT"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    return-void
.end method

.method public setRenewRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Ljava/lang/String;J)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .param p2, "sid"    # Ljava/lang/String;
    .param p3, "timeout"    # J

    .prologue
    .line 117
    const-string v0, "SUBSCRIBE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setMethod(Ljava/lang/String;)V

    .line 118
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setService(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V

    .line 119
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setSID(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p0, p3, p4}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setTimeout(J)V

    .line 121
    return-void
.end method

.method public setSID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 174
    const-string v0, "SID"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    return-void
.end method

.method public setSubscribeRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;Ljava/lang/String;J)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .param p2, "callback"    # Ljava/lang/String;
    .param p3, "timeout"    # J

    .prologue
    .line 109
    const-string v0, "SUBSCRIBE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setMethod(Ljava/lang/String;)V

    .line 110
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setService(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V

    .line 111
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setCallback(Ljava/lang/String;)V

    .line 112
    const-string v0, "upnp:event"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setNT(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p0, p3, p4}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setTimeout(J)V

    .line 114
    return-void
.end method

.method public final setTimeout(J)V
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 212
    const-string v0, "TIMEOUT"

    invoke-static {p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscription;->toTimeoutHeaderString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    return-void
.end method

.method public setUnsubscribeRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .prologue
    .line 124
    const-string v0, "UNSUBSCRIBE"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setMethod(Ljava/lang/String;)V

    .line 125
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setService(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V

    .line 126
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getSID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/SubscriptionRequest;->setSID(Ljava/lang/String;)V

    .line 127
    return-void
.end method
