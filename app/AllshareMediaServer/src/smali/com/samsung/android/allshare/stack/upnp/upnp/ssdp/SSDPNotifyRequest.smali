.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;
.source "SSDPNotifyRequest.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;-><init>()V

    .line 33
    const-string v0, "NOTIFY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setMethod(Ljava/lang/String;)V

    .line 34
    const-string v0, "*"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setURI(Ljava/lang/String;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;-><init>()V

    .line 44
    const-string v0, "NOTIFY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setMethod(Ljava/lang/String;)V

    .line 45
    const-string v0, "*"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setURI(Ljava/lang/String;)V

    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setHost(Ljava/lang/String;I)V

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IZ)V
    .locals 1
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "addUserAgent"    # Z

    .prologue
    .line 50
    invoke-direct {p0, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;-><init>(Z)V

    .line 51
    const-string v0, "NOTIFY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setMethod(Ljava/lang/String;)V

    .line 52
    const-string v0, "*"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setURI(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setHost(Ljava/lang/String;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "addUserAgent"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;-><init>(Z)V

    .line 39
    const-string v0, "NOTIFY"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setMethod(Ljava/lang/String;)V

    .line 40
    const-string v0, "*"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setURI(Ljava/lang/String;)V

    .line 41
    return-void
.end method
