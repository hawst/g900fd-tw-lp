.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
.source "DefaultUploadRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;
    }
.end annotation


# instance fields
.field private mDeviceName:Ljava/lang/String;

.field private final mItemObject:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

.field private mUploadRequestSate:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

.field private mWaitObject:Ljava/lang/Object;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/Object;)V
    .locals 1
    .param p1, "deviceName"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p3, "waitObj"    # Ljava/lang/Object;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;-><init>()V

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mDeviceName:Ljava/lang/String;

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mWaitObject:Ljava/lang/Object;

    .line 30
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->UNDEFINED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mUploadRequestSate:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    .line 63
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mDeviceName:Ljava/lang/String;

    .line 64
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mItemObject:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .line 65
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mWaitObject:Ljava/lang/Object;

    .line 66
    return-void
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/Object;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    .locals 1
    .param p0, "deviceName"    # Ljava/lang/String;
    .param p1, "objectId"    # Ljava/lang/String;
    .param p2, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p3, "waitObj"    # Ljava/lang/Object;

    .prologue
    .line 34
    if-eqz p0, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 35
    :cond_0
    const/4 v0, 0x0

    .line 38
    :goto_0
    return-object v0

    .line 37
    :cond_1
    invoke-virtual {p2, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->setID(Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    invoke-direct {v0, p0, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;-><init>(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    if-ne p0, p1, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v1

    .line 54
    :cond_1
    if-eqz p1, :cond_2

    instance-of v3, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    if-eqz v3, :cond_3

    :cond_2
    move v1, v2

    .line 55
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 57
    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;

    .line 59
    .local v0, "req":Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->hashCode()I

    move-result v3

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->hashCode()I

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mDeviceName:Ljava/lang/String;

    return-object v0
.end method

.method getImportItemObject()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mItemObject:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    return-object v0
.end method

.method public getState()Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mUploadRequestSate:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mItemObject:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mItemObject:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    if-nez v0, :cond_0

    .line 44
    const/4 v0, 0x0

    .line 46
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mItemObject:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method notifyEvent()V
    .locals 2

    .prologue
    .line 99
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mWaitObject:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mWaitObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 101
    monitor-exit v1

    .line 102
    return-void

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setState(Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;)V
    .locals 0
    .param p1, "state"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mUploadRequestSate:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    .line 96
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mItemObject:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "From: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mItemObject:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mUploadRequestSate:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 73
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "From: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mDeviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public waitEvent(J)V
    .locals 5
    .param p1, "timeout"    # J

    .prologue
    .line 105
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mUploadRequestSate:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->UNDEFINED:Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest$UploadRequestSate;->compareTo(Ljava/lang/Enum;)I

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    :goto_0
    return-void

    .line 108
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mWaitObject:Ljava/lang/Object;

    monitor-enter v2

    .line 110
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/DefaultUploadRequest;->mWaitObject:Ljava/lang/Object;

    invoke-virtual {v1, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    :goto_1
    :try_start_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v1, "DefaultUploadRequest"

    const-string v3, "waitEvent"

    const-string v4, "Got interruptedException : "

    invoke-static {v1, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
