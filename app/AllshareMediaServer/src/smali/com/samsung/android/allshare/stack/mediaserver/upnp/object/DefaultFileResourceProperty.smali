.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/DefaultFileResourceProperty;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;
.source "DefaultFileResourceProperty.java"


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Ljava/io/File;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;

    .prologue
    .line 22
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getContentURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 2
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p2, "exInfo"    # Ljava/lang/String;
    .param p3, "protocol"    # Ljava/lang/String;
    .param p4, "file"    # Ljava/io/File;

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getContentURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p3, p4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/AbstractFileResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 27
    return-void
.end method
