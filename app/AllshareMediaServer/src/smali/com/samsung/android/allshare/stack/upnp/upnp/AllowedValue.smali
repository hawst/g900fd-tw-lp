.class public Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;
.super Ljava/lang/Object;
.source "AllowedValue.java"


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "allowedValue"


# instance fields
.field private mAllowedValueNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;->mAllowedValueNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 52
    return-void
.end method

.method public static isAllowedValueNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 59
    const-string v0, "allowedValue"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getAllowedValueNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;->mAllowedValueNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;->getAllowedValueNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
