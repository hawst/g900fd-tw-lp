.class public final Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;
.super Ljava/lang/Object;
.source "ContentSorter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$UPnPClassSortCap;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;
    }
.end annotation


# static fields
.field private static final SORTCAP_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Comparator",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final SORT_CAP_STR:Ljava/lang/String; = "dc:title,dc:date,upnp:class"


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->SORTCAP_MAP:Ljava/util/HashMap;

    .line 43
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->SORTCAP_MAP:Ljava/util/HashMap;

    const-string v1, "+dc:date"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;

    invoke-direct {v2, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->SORTCAP_MAP:Ljava/util/HashMap;

    const-string v1, "-dc:date"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCDateSortComparator;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->SORTCAP_MAP:Ljava/util/HashMap;

    const-string v1, "+dc:title"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;

    invoke-direct {v2, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->SORTCAP_MAP:Ljava/util/HashMap;

    const-string v1, "-dc:title"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->SORTCAP_MAP:Ljava/util/HashMap;

    const-string v1, "+upnp:class"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$UPnPClassSortCap;

    invoke-direct {v2, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$UPnPClassSortCap;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->SORTCAP_MAP:Ljava/util/HashMap;

    const-string v1, "-upnp:class"

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$UPnPClassSortCap;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$UPnPClassSortCap;-><init>(Z)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method

.method public static getSortCapabilities()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    const-string v0, "dc:title,dc:date,upnp:class"

    return-object v0
.end method

.method private static getSortCriteriaList(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .param p0, "sortCriteria"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 88
    .local v0, "sortCriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v3, ", "

    invoke-direct {v1, p0, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .local v1, "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 90
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    .line 91
    .local v2, "token":Ljava/lang/String;
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 93
    .end local v2    # "token":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static sort(Ljava/util/List;Ljava/lang/String;)Z
    .locals 8
    .param p1, "sortCriteria"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p0, "contents":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    const/4 v6, 0x1

    .line 60
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->getSortCriteriaList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 63
    .local v5, "sortcapList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 64
    .local v2, "comparatorList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/Comparator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 65
    .local v4, "sortcap":Ljava/lang/String;
    sget-object v7, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter;->SORTCAP_MAP:Ljava/util/HashMap;

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Comparator;

    .line 66
    .local v1, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    if-nez v1, :cond_1

    .line 67
    const/4 v6, 0x0

    .line 83
    .end local v1    # "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    .end local v4    # "sortcap":Ljava/lang/String;
    :cond_0
    return v6

    .line 69
    .restart local v1    # "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    .restart local v4    # "sortcap":Ljava/lang/String;
    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 75
    .end local v1    # "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    .end local v4    # "sortcap":Ljava/lang/String;
    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 76
    new-instance v7, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;

    invoke-direct {v7, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSorter$DCTitleSortComparator;-><init>(Z)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Comparator;

    .line 80
    .local v0, "comp":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;>;"
    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1
.end method
