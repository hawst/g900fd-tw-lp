.class Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;
.super Ljava/io/DataInputStream;
.source "HTTPPacket.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DualReader"
.end annotation


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0
    .param p1, "arg0"    # Ljava/io/InputStream;

    .prologue
    .line 173
    invoke-direct {p0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 174
    return-void
.end method


# virtual methods
.method public readALine()Ljava/lang/String;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/net/SocketTimeoutException;
        }
    .end annotation

    .prologue
    .line 202
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 206
    .local v4, "sb":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Byte;>;"
    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket$DualReader;->in:Ljava/io/InputStream;

    invoke-virtual {v5}, Ljava/io/InputStream;->read()I

    move-result v1

    .local v1, "d":I
    const/4 v5, -0x1

    if-eq v1, v5, :cond_1

    .line 207
    int-to-char v5, v1

    const/16 v6, 0xd

    if-eq v5, v6, :cond_0

    .line 209
    int-to-char v5, v1

    const/16 v6, 0xa

    if-ne v5, v6, :cond_2

    .line 216
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v0, v5, [B

    .line 217
    .local v0, "bytes":[B
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v5, v0

    if-ge v2, v5, :cond_3

    .line 218
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Byte;

    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    move-result v5

    aput-byte v5, v0, v2

    .line 217
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 212
    .end local v0    # "bytes":[B
    .end local v2    # "i":I
    :cond_2
    int-to-byte v5, v1

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 221
    .restart local v0    # "bytes":[B
    .restart local v2    # "i":I
    :cond_3
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    .line 222
    .local v3, "ret":Ljava/lang/String;
    return-object v3
.end method
