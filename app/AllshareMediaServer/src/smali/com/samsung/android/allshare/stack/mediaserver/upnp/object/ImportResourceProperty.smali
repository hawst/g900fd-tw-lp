.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
.source "ImportResourceProperty.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$DefaultImportResourcePropertyFactory;,
        Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;
    }
.end annotation


# static fields
.field private static final INVALID_FILE_CHARS:Ljava/lang/String; = "[/\\?%*:|\"<>]"

.field private static final PARSER:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

.field private static final TAG:Ljava/lang/String; = "ImportResourceProperty"


# instance fields
.field private mMediaFile:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->PARSER:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    return-void
.end method

.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "file"    # Ljava/io/File;

    .prologue
    .line 235
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->mMediaFile:Ljava/io/File;

    .line 236
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->mMediaFile:Ljava/io/File;

    .line 237
    return-void
.end method

.method static synthetic access$000()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->PARSER:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    return-object v0
.end method

.method static synthetic access$100(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Ljava/io/File;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 28
    invoke-static {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->createDestFileName(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Ljava/io/File;Ljava/io/File;)V
    .locals 0
    .param p0, "x0"    # Ljava/io/File;
    .param p1, "x1"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    invoke-static {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->copyFile(Ljava/io/File;Ljava/io/File;)V

    return-void
.end method

.method private static copyFile(Ljava/io/File;Ljava/io/File;)V
    .locals 11
    .param p0, "tempFile"    # Ljava/io/File;
    .param p1, "destFile"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 187
    const/4 v1, 0x0

    .line 188
    .local v1, "inChannel":Ljava/nio/channels/FileChannel;
    const/4 v6, 0x0

    .line 189
    .local v6, "outChannel":Ljava/nio/channels/FileChannel;
    const/4 v7, 0x0

    .line 190
    .local v7, "fInputStream":Ljava/io/FileInputStream;
    const/4 v9, 0x0

    .line 192
    .local v9, "fOutputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v8, Ljava/io/FileInputStream;

    invoke-direct {v8, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 193
    .end local v7    # "fInputStream":Ljava/io/FileInputStream;
    .local v8, "fInputStream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_5

    .line 194
    .end local v9    # "fOutputStream":Ljava/io/FileOutputStream;
    .local v10, "fOutputStream":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {v8}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 195
    invoke-virtual {v10}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v6

    .line 197
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v1 .. v6}, Ljava/nio/channels/FileChannel;->transferTo(JJLjava/nio/channels/WritableByteChannel;)J
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    move-object v9, v10

    .end local v10    # "fOutputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "fOutputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .line 203
    .end local v8    # "fInputStream":Ljava/io/FileInputStream;
    .restart local v7    # "fInputStream":Ljava/io/FileInputStream;
    :goto_0
    if-eqz v1, :cond_0

    .line 204
    :try_start_3
    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 210
    :cond_0
    :goto_1
    if-eqz v6, :cond_1

    .line 211
    :try_start_4
    invoke-virtual {v6}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    .line 217
    :cond_1
    :goto_2
    if-eqz v7, :cond_2

    .line 218
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    .line 224
    :cond_2
    :goto_3
    if-eqz v9, :cond_3

    .line 225
    :try_start_6
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4

    .line 230
    :cond_3
    :goto_4
    return-void

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    :goto_5
    const-string v2, "ImportResourceProperty"

    const-string v3, "copyFile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException on copyFile create Stream"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 205
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    .line 206
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v2, "ImportResourceProperty"

    const-string v3, "copyFile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException on copyFile inChannel.close();"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 212
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 213
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v2, "ImportResourceProperty"

    const-string v3, "copyFile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException on copyFile outChannel.close();"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 219
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_3
    move-exception v0

    .line 220
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v2, "ImportResourceProperty"

    const-string v3, "copyFile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException on copyFile fInputStream.close();"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 226
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_4
    move-exception v0

    .line 227
    .restart local v0    # "e":Ljava/lang/Exception;
    const-string v2, "ImportResourceProperty"

    const-string v3, "copyFile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IOException on copyFile fOutputStream.close();"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 198
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v7    # "fInputStream":Ljava/io/FileInputStream;
    .restart local v8    # "fInputStream":Ljava/io/FileInputStream;
    :catch_5
    move-exception v0

    move-object v7, v8

    .end local v8    # "fInputStream":Ljava/io/FileInputStream;
    .restart local v7    # "fInputStream":Ljava/io/FileInputStream;
    goto/16 :goto_5

    .end local v7    # "fInputStream":Ljava/io/FileInputStream;
    .end local v9    # "fOutputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "fInputStream":Ljava/io/FileInputStream;
    .restart local v10    # "fOutputStream":Ljava/io/FileOutputStream;
    :catch_6
    move-exception v0

    move-object v9, v10

    .end local v10    # "fOutputStream":Ljava/io/FileOutputStream;
    .restart local v9    # "fOutputStream":Ljava/io/FileOutputStream;
    move-object v7, v8

    .end local v8    # "fInputStream":Ljava/io/FileInputStream;
    .restart local v7    # "fInputStream":Ljava/io/FileInputStream;
    goto/16 :goto_5
.end method

.method private static createDestFileName(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 5
    .param p0, "path"    # Ljava/io/File;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "mime"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->replaceTitleToFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 47
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getFileExtensionByMimetype(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "fileExtension":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v3, ".jpeg"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 50
    const-string v1, ".jpg"

    .line 52
    :cond_0
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 54
    .local v0, "destFile":Ljava/io/File;
    const/4 v2, 0x0

    .line 55
    .local v2, "index":I
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 56
    new-instance v0, Ljava/io/File;

    .end local v0    # "destFile":Ljava/io/File;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, p0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .restart local v0    # "destFile":Ljava/io/File;
    goto :goto_0

    .line 58
    :cond_1
    return-object v0
.end method

.method private static replaceTitleToFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "title"    # Ljava/lang/String;

    .prologue
    .line 64
    if-nez p0, :cond_0

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FILE_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 68
    :cond_0
    const-string v0, "[/\\?%*:|\"<>]"

    const-string v1, "_"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public deleteContent()V
    .locals 4

    .prologue
    .line 260
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->mMediaFile:Ljava/io/File;

    if-eqz v1, :cond_0

    .line 262
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->mMediaFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 263
    :catch_0
    move-exception v0

    .line 264
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "ImportResourceProperty"

    const-string v2, "deleteContent"

    const-string v3, "fail to deleteContent"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getContentLength()J
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->mMediaFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    return-wide v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->mMediaFile:Ljava/io/File;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->mMediaFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 251
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->mMediaFile:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    :goto_0
    return-object v1

    .line 252
    :catch_0
    move-exception v0

    .line 253
    .local v0, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    .line 254
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMediaFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->mMediaFile:Ljava/io/File;

    return-object v0
.end method
