.class public abstract Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
.source "ResourceProperty.java"


# static fields
.field static parser:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;


# instance fields
.field mFeature:Ljava/lang/String;

.field mMime:Ljava/lang/String;

.field mProtocol:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->parser:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "protocol"    # Ljava/lang/String;

    .prologue
    .line 29
    const-string v0, "res"

    invoke-direct {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mProtocol:Ljava/lang/String;

    .line 24
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mMime:Ljava/lang/String;

    .line 26
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mFeature:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mProtocol:Ljava/lang/String;

    .line 32
    const-string v0, "protocolInfo"

    invoke-virtual {p0, v0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public abstract deleteContent()V
.end method

.method public get4thfield()Ljava/lang/String;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mFeature:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mFeature:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mFeature:Ljava/lang/String;

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->parser:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->get4thField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mFeature:Ljava/lang/String;

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mFeature:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getContentLength()J
.end method

.method public abstract getFilePath()Ljava/lang/String;
.end method

.method public abstract getInputStream()Ljava/io/InputStream;
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mMime:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mMime:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->parser:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mMime:Ljava/lang/String;

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mMime:Ljava/lang/String;

    return-object v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->mProtocol:Ljava/lang/String;

    return-object v0
.end method

.method public setUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setValue(Ljava/lang/String;)V

    .line 41
    return-void
.end method
