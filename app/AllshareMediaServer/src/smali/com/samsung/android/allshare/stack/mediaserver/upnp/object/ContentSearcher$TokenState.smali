.class final enum Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;
.super Ljava/lang/Enum;
.source "ContentSearcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TokenState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

.field public static final enum ESCAPECHAR:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

.field public static final enum PAREN:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

.field public static final enum QUOTWORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

.field public static final enum START:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

.field public static final enum WORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 133
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    const-string v1, "START"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->START:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    const-string v1, "PAREN"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->PAREN:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    const-string v1, "WORD"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->WORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    const-string v1, "QUOTWORD"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->QUOTWORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    const-string v1, "ESCAPECHAR"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->ESCAPECHAR:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    .line 132
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->START:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->PAREN:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->WORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->QUOTWORD:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->ESCAPECHAR:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->$VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 132
    const-class v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->$VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContentSearcher$TokenState;

    return-object v0
.end method
