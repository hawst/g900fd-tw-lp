.class public Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;
.super Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
.source "SOAPRequest.java"


# static fields
.field private static final SOAPACTION:Ljava/lang/String; = "SOAPACTION"


# instance fields
.field private mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;-><init>()V

    .line 44
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mTag:Ljava/lang/String;

    .line 51
    const-string v0, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->setContentType(Ljava/lang/String;)V

    .line 52
    const-string v0, "POST"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->setMethod(Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method private declared-synchronized getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 7

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    if-eqz v3, :cond_0

    .line 124
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :goto_0
    monitor-exit p0

    return-object v3

    .line 126
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->getContent()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 131
    .local v1, "is":Ljava/io/InputStream;
    :try_start_2
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAP;->getXMLParser()Lcom/samsung/android/allshare/stack/upnp/xml/Parser;

    move-result-object v2

    .line 132
    .local v2, "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Parser;->parse(Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_2
    .catch Lcom/samsung/android/allshare/stack/upnp/xml/ParserException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 137
    if-eqz v1, :cond_1

    .line 138
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 146
    .end local v2    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :cond_1
    :goto_1
    :try_start_4
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    goto :goto_0

    .line 139
    .restart local v2    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :catch_0
    move-exception v0

    .line 140
    .local v0, "e":Ljava/io/IOException;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mTag:Ljava/lang/String;

    const-string v4, "getRootNode"

    const-string v5, "getRootNode- IOException"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 123
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v2    # "parser":Lcom/samsung/android/allshare/stack/upnp/xml/Parser;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3

    .line 133
    .restart local v1    # "is":Ljava/io/InputStream;
    :catch_1
    move-exception v0

    .line 134
    .local v0, "e":Lcom/samsung/android/allshare/stack/upnp/xml/ParserException;
    :try_start_5
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mTag:Ljava/lang/String;

    const-string v4, "getRootNode"

    const-string v5, "getRootNode - ParserException"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 137
    if-eqz v1, :cond_1

    .line 138
    :try_start_6
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 139
    :catch_2
    move-exception v0

    .line 140
    .local v0, "e":Ljava/io/IOException;
    :try_start_7
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mTag:Ljava/lang/String;

    const-string v4, "getRootNode"

    const-string v5, "getRootNode- IOException"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 136
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_1
    move-exception v3

    .line 137
    if-eqz v1, :cond_2

    .line 138
    :try_start_8
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 141
    :cond_2
    :goto_2
    :try_start_9
    throw v3

    .line 139
    :catch_3
    move-exception v0

    .line 140
    .restart local v0    # "e":Ljava/io/IOException;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mTag:Ljava/lang/String;

    const-string v5, "getRootNode"

    const-string v6, "getRootNode- IOException"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_2
.end method

.method private declared-synchronized setRootNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 1
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->mRootNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    monitor-exit p0

    return-void

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 162
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 163
    .local v0, "envNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-object v1

    .line 165
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->hasNodes()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 167
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method

.method public getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 158
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    return-object v0
.end method

.method public getSOAPAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const-string v0, "SOAPACTION"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->getStringHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isQuerySOAPAction(Ljava/lang/String;)Z
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 73
    const-string v4, "SOAPACTION"

    invoke-virtual {p0, v4}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    .local v0, "headerValue":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v2

    .line 76
    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-ne v4, v3, :cond_2

    move v2, v3

    .line 77
    goto :goto_0

    .line 78
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->getSOAPAction()Ljava/lang/String;

    move-result-object v1

    .line 79
    .local v1, "soapAction":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 81
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method

.method public postMessage(Ljava/lang/String;I)Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;
    .locals 2
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 92
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->post(Ljava/lang/String;I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    move-result-object v0

    .line 93
    .local v0, "httpRes":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;

    invoke-direct {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V

    .line 109
    .local v1, "soapRes":Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;
    return-object v1
.end method

.method public print()V
    .locals 3

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->hasContent()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->getRootNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 195
    .local v0, "rootElem":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v0, :cond_0

    goto :goto_0
.end method

.method public setContent(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 3
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 177
    const-string v0, ""

    .line 178
    .local v0, "conStr":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<?xml version=\"1.0\" encoding=\"utf-8\"?>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 180
    if-eqz p1, :cond_0

    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->setContent(Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method public setEnvelopeNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 154
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->setRootNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 155
    return-void
.end method

.method public setSOAPAction(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 65
    const-string v0, "SOAPACTION"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;->setStringHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    return-void
.end method
