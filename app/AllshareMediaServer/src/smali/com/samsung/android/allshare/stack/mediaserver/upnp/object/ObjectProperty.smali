.class public abstract Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
.source "ObjectProperty.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;",
        ">;"
    }
.end annotation


# static fields
.field public static final ID:Ljava/lang/String; = "id"

.field public static final PARENT_ID:Ljava/lang/String; = "parentID"

.field public static final RESTRICTED:Ljava/lang/String; = "restricted"

.field public static final UNKNOWN:Ljava/lang/String; = "UNKNOWN"


# instance fields
.field private mPropList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46
    const-string v0, "object.item"

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->mPropList:Ljava/util/ArrayList;

    .line 47
    const-string v0, "0"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setID(Ljava/lang/String;)V

    .line 48
    const-string v0, "-1"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setParentID(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setRestricted(Z)V

    .line 50
    return-void
.end method


# virtual methods
.method public addProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z
    .locals 1
    .param p1, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .prologue
    .line 104
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    const/4 v0, 0x0

    .line 107
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->mPropList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public compareTo(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)I
    .locals 2
    .param p1, "another"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .prologue
    .line 339
    if-nez p1, :cond_0

    .line 340
    const/4 v0, 0x1

    .line 344
    :goto_0
    return v0

    .line 341
    :cond_0
    if-ne p0, p1, :cond_1

    .line 342
    const/4 v0, 0x0

    goto :goto_0

    .line 344
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->compareTo(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 319
    if-eqz p1, :cond_0

    instance-of v2, p1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    if-nez v2, :cond_1

    .line 322
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 321
    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .line 322
    .local v0, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->hashCode()I

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->hashCode()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    const-string v0, "id"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    const-string v0, "parentID"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .locals 4
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->mPropList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 68
    .local v1, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 71
    .end local v1    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPropertyList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->mPropList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPropertyList(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;",
            ">;"
        }
    .end annotation

    .prologue
    .line 90
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;>;"
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->mPropList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 92
    .local v2, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 93
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 95
    .end local v2    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    :cond_1
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    return-object v3
.end method

.method public getPropertyValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 142
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v0

    .line 143
    .local v0, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 145
    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getRestricted()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 229
    const-string v2, "restricted"

    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "restricted":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 233
    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-eq v1, v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    const-string v0, "dc:title"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUPnPClass()Ljava/lang/String;
    .locals 1

    .prologue
    .line 274
    const-string v0, "upnp:class"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWriteStatus()Ljava/lang/String;
    .locals 1

    .prologue
    .line 294
    const-string v0, "upnp:writeStatus"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getPropertyValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasProperties()Z
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->mPropList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getID()Ljava/lang/String;

    move-result-object v0

    .line 328
    .local v0, "id":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 329
    const/4 v1, 0x0

    .line 331
    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0
.end method

.method public removeProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z
    .locals 1
    .param p1, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->mPropList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final setID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 175
    const-string v0, "id"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 176
    return-void
.end method

.method public setParentID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 196
    const-string v0, "parentID"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 197
    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 120
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v0

    .line 125
    .local v0, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v0, :cond_2

    .line 126
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setValue(Ljava/lang/String;)V

    goto :goto_0

    .line 131
    :cond_2
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .end local v0    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-direct {v0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .restart local v0    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->mPropList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setRestricted(Z)V
    .locals 2
    .param p1, "restrict"    # Z

    .prologue
    .line 220
    const-string v1, "restricted"

    if-eqz p1, :cond_0

    const-string v0, "1"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 221
    return-void

    .line 220
    :cond_0
    const-string v0, "0"

    goto :goto_0
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 244
    const-string v0, "dc:title"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    return-void
.end method

.method public final setUPnPClass(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 265
    const-string v0, "upnp:class"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method public setWriteStatus(Ljava/lang/String;)V
    .locals 1
    .param p1, "status"    # Ljava/lang/String;

    .prologue
    .line 287
    const-string v0, "upnp:writeStatus"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    return-void
.end method

.method public abstract update()V
.end method
