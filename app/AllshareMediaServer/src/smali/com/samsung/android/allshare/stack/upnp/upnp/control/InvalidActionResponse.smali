.class public final Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;
.source "InvalidActionResponse.java"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;-><init>()V

    .line 36
    const/16 v3, 0x191

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;->setFaultResponse(I)V

    .line 38
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 39
    .local v0, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;->createResponseNode(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 40
    .local v2, "resNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 44
    .local v1, "envNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;->setContent(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 45
    return-void
.end method

.method private createResponseNode(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 3
    .param p1, "actionName"    # Ljava/lang/String;
    .param p2, "serviceType"    # Ljava/lang/String;

    .prologue
    .line 48
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50
    :cond_0
    const/4 v0, 0x0

    .line 54
    :goto_0
    return-object v0

    .line 52
    :cond_1
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "u:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Response"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 53
    .local v0, "actionNameResNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const-string v1, "xmlns:u"

    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
