.class public Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
.super Ljava/lang/Object;
.source "Action.java"


# static fields
.field public static final DMR_AUTHENTICATION_FAIL:Ljava/lang/String; = "HTTP/1.1 401 Unauthorised"

.field public static final ELEM_NAME:Ljava/lang/String; = "action"

.field private static final NAME:Ljava/lang/String; = "name"


# instance fields
.field private mActionNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field protected mActionReq:Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

.field protected mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

.field private mIsAuthenticationFailed:Z

.field private mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private final mTag:Ljava/lang/String;

.field private mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V
    .locals 3
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 132
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;)V

    .line 133
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->clear()V

    .line 134
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    .line 135
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 1
    .param p1, "serviceNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .param p2, "actionNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 128
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;)V

    .line 129
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;)V
    .locals 9
    .param p1, "serviceNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .param p2, "actionNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .param p3, "status"    # Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mTag:Ljava/lang/String;

    .line 68
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mIsAuthenticationFailed:Z

    .line 311
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mActionReq:Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    .line 404
    new-instance v7, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-direct {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;-><init>()V

    iput-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .line 87
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 88
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mActionNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 89
    if-eqz p3, :cond_0

    .line 90
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    .line 93
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v1

    .line 94
    .local v1, "argumentListCnt":I
    new-instance v7, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    invoke-direct {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;-><init>()V

    iput-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    .line 95
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_4

    .line 96
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v7

    invoke-virtual {v7, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 97
    .local v2, "argumentListNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 98
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v7

    const-string v8, "argumentList"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 95
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 101
    :cond_2
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v6

    .line 102
    .local v6, "nodeCnt":I
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_1
    if-ge v4, v6, :cond_1

    .line 103
    invoke-virtual {v2, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    .line 104
    .local v5, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->isArgumentNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 102
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 106
    :cond_3
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v7

    invoke-direct {v0, v5, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 107
    .local v0, "argument":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    invoke-virtual {v7, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 125
    .end local v0    # "argument":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .end local v2    # "argumentListNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v4    # "n":I
    .end local v5    # "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v6    # "nodeCnt":I
    :cond_4
    return-void
.end method

.method private getActionData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;
    .locals 2

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 258
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;

    .line 259
    .local v1, "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;
    if-nez v1, :cond_0

    .line 260
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;

    .end local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;
    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;-><init>()V

    .line 261
    .restart local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 262
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->setNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 264
    :cond_0
    return-object v1
.end method

.method public static isActionNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 142
    const-string v0, "action"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private setControlResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;)V
    .locals 1
    .param p1, "res"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;

    .prologue
    .line 329
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->setControlResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;)V

    .line 330
    return-void
.end method


# virtual methods
.method protected clearOutputAgumentValues()V
    .locals 5

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 228
    .local v1, "allArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->size()I

    move-result v0

    .line 229
    .local v0, "allArgCnt":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v0, :cond_1

    .line 230
    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->getArgument(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 231
    .local v2, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->isOutDirection()Z

    move-result v4

    if-nez v4, :cond_0

    .line 229
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 233
    :cond_0
    const-string v4, ""

    invoke-virtual {v2, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    goto :goto_1

    .line 235
    .end local v2    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :cond_1
    return-void
.end method

.method public getActionListener()Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;
    .locals 1

    .prologue
    .line 272
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->getActionListener()Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    move-result-object v0

    return-object v0
.end method

.method public getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mActionNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getActionRequest()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mActionReq:Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    return-object v0
.end method

.method public getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .locals 7
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 198
    .local v1, "argList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->size()I

    move-result v4

    .line 199
    .local v4, "nArgs":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v4, :cond_2

    .line 200
    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->getArgument(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 201
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v2

    .line 202
    .local v2, "argName":Ljava/lang/String;
    if-nez v2, :cond_1

    .line 199
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 204
    :cond_1
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 207
    .end local v0    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .end local v2    # "argName":Ljava/lang/String;
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mArgumentList:Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    return-object v0
.end method

.method public getArgumentValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 238
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 239
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-nez v0, :cond_0

    .line 240
    const-string v1, ""

    .line 241
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getControlResponse()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;
    .locals 1

    .prologue
    .line 325
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->getControlResponse()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;

    move-result-object v0

    return-object v0
.end method

.method public getControlStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;
    .locals 2

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getControlResponse()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;

    move-result-object v0

    .line 334
    .local v0, "controlResponse":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;
    if-eqz v0, :cond_0

    .line 335
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getUPnPError()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    move-result-object v1

    .line 337
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;-><init>()V

    goto :goto_0
.end method

.method public getInputArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    .locals 6

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 171
    .local v1, "allArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->size()I

    move-result v0

    .line 172
    .local v0, "allArgCnt":I
    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    invoke-direct {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;-><init>()V

    .line 173
    .local v3, "argList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    const/4 v4, 0x0

    .local v4, "n":I
    :goto_0
    if-ge v4, v0, :cond_1

    .line 174
    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->getArgument(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 175
    .local v2, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->isInDirection()Z

    move-result v5

    if-nez v5, :cond_0

    .line 173
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 177
    :cond_0
    invoke-virtual {v3, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 179
    .end local v2    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :cond_1
    return-object v3
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    return-object v0
.end method

.method public getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    return-object v0
.end method

.method public getStatusByString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 420
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->getCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAuthenticationFailed()Z
    .locals 1

    .prologue
    .line 424
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mIsAuthenticationFailed:Z

    return v0
.end method

.method public performActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;)Z
    .locals 7
    .param p1, "actionReq"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 280
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mActionReq:Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    .line 281
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionListener()Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    move-result-object v2

    .line 282
    .local v2, "listener":Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;
    if-nez v2, :cond_1

    .line 308
    :cond_0
    :goto_0
    return v4

    .line 285
    :cond_1
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;-><init>()V

    .line 286
    .local v0, "actionRes":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;
    const/16 v6, 0x191

    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    .line 287
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->clearOutputAgumentValues()V

    .line 289
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getRequestAddress()Ljava/net/InetAddress;

    move-result-object v1

    .line 291
    .local v1, "clientAddr":Ljava/net/InetAddress;
    if-eqz v1, :cond_0

    .line 294
    invoke-interface {v2, p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;->actionControlReceived(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z

    move-result v4

    if-ne v4, v5, :cond_2

    .line 295
    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->setResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 301
    :goto_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->print()V

    .line 302
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    .line 306
    invoke-interface {v2, p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;->actionControlHandled(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z

    move v4, v5

    .line 308
    goto :goto_0

    .line 297
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getStatus()Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    move-result-object v3

    .line 298
    .local v3, "upnpStatus":Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->getCode()I

    move-result v4

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->getDescription()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v4, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->setFaultResponse(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public postControlAction()Z
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 346
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v0

    .line 347
    .local v0, "actionArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getInputArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 348
    .local v1, "actionInputArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    invoke-direct {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;-><init>()V

    .line 349
    .local v2, "ctrlReq":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    invoke-virtual {v2, p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->setRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;)V

    .line 350
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->print()V

    .line 351
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->post()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;

    move-result-object v3

    .line 352
    .local v3, "ctrlRes":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->print()V

    .line 353
    invoke-direct {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setControlResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;)V

    .line 356
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->getFirstLine()Ljava/lang/String;

    move-result-object v6

    .line 357
    .local v6, "strFirstLine":Ljava/lang/String;
    const-string v8, "HTTP/1.1 401 Unauthorised"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 358
    iput-boolean v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mIsAuthenticationFailed:Z

    .line 363
    :cond_0
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->getStatusCode()I

    move-result v5

    .line 364
    .local v5, "statCode":I
    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    .line 369
    const/16 v8, 0x19a

    if-ne v5, v8, :cond_1

    .line 370
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 371
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setForceExpired(Z)V

    .line 372
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->requestDisposer()V

    .line 376
    :cond_1
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->isSuccessful()Z

    move-result v8

    if-nez v8, :cond_2

    .line 377
    const/4 v7, 0x0

    .line 380
    :goto_0
    return v7

    .line 378
    :cond_2
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->getResponse()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v4

    .line 379
    .local v4, "outArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->setArgumentList(Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;)V

    goto :goto_0
.end method

.method public print()V
    .locals 11

    .prologue
    .line 388
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mTag:Ljava/lang/String;

    const-string v8, "print"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Action : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 390
    .local v1, "argList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->size()I

    move-result v4

    .line 391
    .local v4, "nArgs":I
    const/4 v3, 0x0

    .local v3, "n":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 392
    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->getArgument(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 393
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v5

    .line 394
    .local v5, "name":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v6

    .line 395
    .local v6, "value":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getDirection()Ljava/lang/String;

    move-result-object v2

    .line 396
    .local v2, "dir":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mTag:Ljava/lang/String;

    const-string v8, "print"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, " ["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "] = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 398
    .end local v0    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    .end local v2    # "dir":Ljava/lang/String;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "value":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ActionData;->setActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    .line 277
    return-void
.end method

.method public setArgumentValue(Ljava/lang/String;I)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 223
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    return-void
.end method

.method public setArgumentValue(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 216
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 217
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-nez v0, :cond_0

    .line 220
    :goto_0
    return-void

    .line 219
    :cond_0
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setStatus(I)V
    .locals 1
    .param p1, "code"    # I

    .prologue
    .line 412
    invoke-static {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(ILjava/lang/String;)V

    .line 413
    return-void
.end method

.method public setStatus(ILjava/lang/String;)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "descr"    # Ljava/lang/String;

    .prologue
    .line 407
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->setCode(I)V

    .line 408
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->mUpnpStatus:Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;

    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/UPnPStatus;->setDescription(Ljava/lang/String;)V

    .line 409
    return-void
.end method
