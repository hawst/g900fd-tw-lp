.class public Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;
.super Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;
.source "NotifyRequest.java"


# static fields
.field private static final PROPERTY:Ljava/lang/String; = "property"

.field private static final PROPERTYSET:Ljava/lang/String; = "propertyset"

.field private static final XMLNS:Ljava/lang/String; = "e"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;-><init>()V

    .line 71
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 0
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;-><init>()V

    .line 74
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 75
    return-void
.end method

.method private createPropertySetNode(Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 7
    .param p1, "stateTable"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;

    .prologue
    .line 209
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v5, "e:propertyset"

    invoke-direct {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 211
    .local v2, "propSetNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const-string v5, "e"

    const-string v6, "urn:schemas-upnp-org:event-1-0"

    invoke-virtual {v2, v5, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNameSpace(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    const/4 v0, 0x0

    .local v0, "n":I
    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;->size()I

    move-result v5

    if-ge v0, v5, :cond_1

    .line 219
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v5, "e:property"

    invoke-direct {v1, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 221
    .local v1, "propNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;->getStateVariable(I)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v3

    .line 222
    .local v3, "var":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->isSendEvents()Z

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 223
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 224
    .local v4, "varNameNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 225
    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 226
    invoke-virtual {v2, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 218
    .end local v4    # "varNameNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 232
    .end local v1    # "propNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .end local v3    # "var":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    :cond_1
    return-object v2
.end method

.method private createPropertySetNode(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 5
    .param p1, "varName"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 165
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v3, "e:propertyset"

    invoke-direct {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 167
    .local v1, "propSetNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    const-string v3, "e"

    const-string v4, "urn:schemas-upnp-org:event-1-0"

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNameSpace(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v3, "e:property"

    invoke-direct {v0, v3}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 170
    .local v0, "propNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 174
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {v2, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 175
    .local v2, "varNameNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v2, p2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 176
    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 178
    return-object v1
.end method

.method private getProperty(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;
    .locals 4
    .param p1, "varNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 254
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;-><init>()V

    .line 255
    .local v1, "prop":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;
    if-nez p1, :cond_0

    .line 264
    :goto_0
    return-object v1

    .line 258
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v2

    .line 259
    .local v2, "variableName":Ljava/lang/String;
    const/16 v3, 0x3a

    invoke-virtual {v2, v3}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 260
    .local v0, "index":I
    const/4 v3, -0x1

    if-eq v0, v3, :cond_1

    .line 261
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 262
    :cond_1
    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;->setName(Ljava/lang/String;)V

    .line 263
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;->setValue(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getNT()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    const-string v1, "NT"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "nt":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 96
    const-string v0, ""

    .line 97
    .end local v0    # "nt":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getPropertyList()Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;
    .locals 6

    .prologue
    .line 269
    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;

    invoke-direct {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;-><init>()V

    .line 270
    .local v3, "properties":Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v4

    .line 271
    .local v4, "varSetNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v4, :cond_1

    .line 280
    :cond_0
    return-object v3

    .line 273
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 274
    invoke-virtual {v4, v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 275
    .local v2, "propNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v2, :cond_2

    .line 273
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 277
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getProperty(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;

    move-result-object v1

    .line 278
    .local v1, "prop":Lcom/samsung/android/allshare/stack/upnp/upnp/event/Property;
    invoke-virtual {v3, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/PropertyList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getSEQ()J
    .locals 2

    .prologue
    .line 134
    const-string v0, "SEQ"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getLongHeaderValue(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    const-string v0, "SID"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasNT()Z
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getNT()Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "nt":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isValidNTS()Z
    .locals 1

    .prologue
    .line 110
    const-string v0, "NTS"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/NTS;->isEvent(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public setNT(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 82
    const-string v0, "NT"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public setNTS(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 105
    const-string v0, "NTS"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method public setRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;)Z
    .locals 8
    .param p1, "sub"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    .param p2, "stateTable"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;

    .prologue
    .line 187
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getSID()Ljava/lang/String;

    move-result-object v6

    .line 188
    .local v6, "sid":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getNotifyCount()J

    move-result-wide v2

    .line 189
    .local v2, "notifyCnt":J
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryHost()Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, "host":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryPath()Ljava/lang/String;

    move-result-object v1

    .line 191
    .local v1, "path":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryPort()I

    move-result v4

    .line 193
    .local v4, "port":I
    const-string v7, "NOTIFY"

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setMethod(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setURI(Ljava/lang/String;)V

    .line 195
    invoke-virtual {p0, v0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setHost(Ljava/lang/String;I)V

    .line 196
    const-string v7, "upnp:event"

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setNT(Ljava/lang/String;)V

    .line 197
    const-string v7, "upnp:propchange"

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 198
    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setSID(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setSEQ(J)V

    .line 201
    const-string v7, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setContentType(Ljava/lang/String;)V

    .line 202
    invoke-direct {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->createPropertySetNode(Lcom/samsung/android/allshare/stack/upnp/upnp/ServiceStateTable;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    .line 203
    .local v5, "propSetNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setContent(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 205
    const/4 v7, 0x1

    return v7
.end method

.method public setRequest(Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "sub"    # Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
    .param p2, "varName"    # Ljava/lang/String;
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 143
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getSID()Ljava/lang/String;

    move-result-object v6

    .line 144
    .local v6, "sid":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getNotifyCount()J

    move-result-wide v2

    .line 145
    .local v2, "notifyCnt":J
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryHost()Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "host":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryPath()Ljava/lang/String;

    move-result-object v1

    .line 147
    .local v1, "path":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getDeliveryPort()I

    move-result v4

    .line 149
    .local v4, "port":I
    const-string v7, "NOTIFY"

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setMethod(Ljava/lang/String;)V

    .line 150
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setURI(Ljava/lang/String;)V

    .line 151
    invoke-virtual {p0, v0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setHost(Ljava/lang/String;I)V

    .line 152
    const-string v7, "upnp:event"

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setNT(Ljava/lang/String;)V

    .line 153
    const-string v7, "upnp:propchange"

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setSID(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setSEQ(J)V

    .line 157
    const-string v7, "text/xml; charset=\"utf-8\""

    invoke-virtual {p0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setContentType(Ljava/lang/String;)V

    .line 158
    invoke-direct {p0, p2, p3}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->createPropertySetNode(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    .line 159
    .local v5, "propSetNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setContent(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 161
    const/4 v7, 0x1

    return v7
.end method

.method public setSEQ(J)V
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 130
    const-string v0, "SEQ"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    return-void
.end method

.method public setSID(Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 118
    const-string v0, "SID"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/NotifyRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    return-void
.end method
