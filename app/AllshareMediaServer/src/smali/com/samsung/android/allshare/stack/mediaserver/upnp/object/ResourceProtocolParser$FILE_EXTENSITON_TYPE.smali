.class final enum Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
.super Ljava/lang/Enum;
.source "ResourceProtocolParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FILE_EXTENSITON_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum AAC:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum ADTS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum ASF:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum AVI:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum GP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum GPP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum LPCM:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum MP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum MP4:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum MPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum RAW:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum WMA:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum WMV:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

.field public static final enum YUV:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 152
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "JPEG"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "PNG"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "YUV"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->YUV:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "RAW"

    invoke-direct {v0, v1, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->RAW:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "GPP3"

    invoke-direct {v0, v1, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->GPP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "GP3"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->GP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "MP4"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP4:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "LPCM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->LPCM:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "MPEG"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "ADTS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ADTS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "WMA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->WMA:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "ASF"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ASF:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "WMV"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->WMV:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "MP3"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "AVI"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AVI:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    const-string v1, "AAC"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AAC:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    .line 151
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->YUV:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->RAW:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->GPP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->GP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP4:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->LPCM:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ADTS:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->WMA:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->ASF:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->WMV:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->MP3:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AVI:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->AAC:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->$VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 151
    const-class v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;
    .locals 1

    .prologue
    .line 151
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->$VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser$FILE_EXTENSITON_TYPE;

    return-object v0
.end method
