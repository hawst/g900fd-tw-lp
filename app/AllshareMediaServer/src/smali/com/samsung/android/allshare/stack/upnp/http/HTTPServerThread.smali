.class public Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;
.super Ljava/lang/Thread;
.source "HTTPServerThread.java"


# static fields
.field static final MAX_SERVER_SOCKET:I = 0x64

.field static usedServerSocket:I


# instance fields
.field private mHttpServer:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

.field private mSocket:Ljava/net/Socket;

.field private final mTag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->usedServerSocket:I

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;Ljava/net/Socket;)V
    .locals 2
    .param p1, "httpServer"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;
    .param p2, "sock"    # Ljava/net/Socket;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 29
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mTag:Ljava/lang/String;

    .line 40
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mHttpServer:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    .line 41
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mSocket:Ljava/net/Socket;

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HTTP Thread for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/net/Socket;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->setName(Ljava/lang/String;)V

    .line 44
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 55
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mSocket:Ljava/net/Socket;

    invoke-direct {v2, v4}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;-><init>(Ljava/net/Socket;)V

    .line 56
    .local v2, "httpSock":Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;-><init>()V

    .line 57
    .local v1, "httpReq":Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->setSocket(Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;)V

    .line 58
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mHttpServer:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->setHttpServer(Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;)V

    .line 61
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->readData()Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 62
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mHttpServer:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    invoke-virtual {v4, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->performRequestListener(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 63
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isKeepAlive()Z
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_2

    move-result v4

    if-nez v4, :cond_0

    .line 75
    :cond_1
    :goto_0
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v3

    .line 76
    .local v3, "uriStr":Ljava/lang/String;
    const-string v4, "/media"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isPostRequest()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 77
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mHttpServer:Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;

    invoke-virtual {v4, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->performRequestListener(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 79
    :cond_2
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->hasFileContent()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 80
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->removeTempFile()V

    .line 83
    :cond_3
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPSocket;->close()Z

    .line 87
    sget v4, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->usedServerSocket:I

    add-int/lit8 v4, v4, -0x1

    sput v4, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->usedServerSocket:I

    .line 88
    return-void

    .line 66
    .end local v3    # "uriStr":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/net/SocketTimeoutException;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mTag:Ljava/lang/String;

    const-string v5, "run"

    const-string v6, "run SocketTimeoutException"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 69
    .end local v0    # "e":Ljava/net/SocketTimeoutException;
    :catch_1
    move-exception v0

    .line 70
    .local v0, "e":Ljava/lang/Exception;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mTag:Ljava/lang/String;

    const-string v5, "run"

    const-string v6, "run Exception"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 71
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 72
    .local v0, "e":Ljava/lang/Error;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServerThread;->mTag:Ljava/lang/String;

    const-string v5, "run"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "run Error : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/Error;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
