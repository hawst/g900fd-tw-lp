.class public final Lcom/samsung/android/allshare/stack/upnp/util/TimerUtil;
.super Ljava/lang/Object;
.source "TimerUtil.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TimerUtil"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final waitRandom(I)V
    .locals 6
    .param p0, "time"    # I

    .prologue
    .line 26
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    int-to-double v4, p0

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 28
    .local v1, "waitTime":I
    int-to-long v2, v1

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 30
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "TimerUtil"

    const-string v3, "waitRandom"

    const-string v4, "waitRandom Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method
