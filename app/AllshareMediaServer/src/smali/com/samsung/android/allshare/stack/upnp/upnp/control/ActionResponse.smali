.class public Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;
.source "ActionResponse.java"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;-><init>()V

    .line 45
    const-string v0, "EXT"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;)V
    .locals 2
    .param p1, "soapRes"    # Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;-><init>(Lcom/samsung/android/allshare/stack/upnp/soap/SOAPResponse;)V

    .line 50
    const-string v0, "EXT"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method private getActionResponseNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 101
    .local v0, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->hasNodes()Z

    move-result v1

    if-nez v1, :cond_1

    .line 102
    :cond_0
    const/4 v1, 0x0

    .line 103
    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method protected createResponseNode(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 10
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 70
    if-nez p1, :cond_1

    .line 71
    const/4 v1, 0x0

    .line 92
    :cond_0
    return-object v1

    .line 72
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getName()Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "actionName":Ljava/lang/String;
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "u:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "Response"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>(Ljava/lang/String;)V

    .line 75
    .local v1, "actionNameResNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v7

    .line 76
    .local v7, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-eqz v7, :cond_2

    .line 77
    const-string v8, "xmlns:u"

    invoke-virtual {v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getServiceType()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1, v8, v9}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_2
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v3

    .line 81
    .local v3, "argList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->size()I

    move-result v6

    .line 82
    .local v6, "nArgs":I
    const/4 v5, 0x0

    .local v5, "n":I
    :goto_0
    if-ge v5, v6, :cond_0

    .line 83
    invoke-virtual {v3, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->getArgument(I)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 84
    .local v2, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->isOutDirection()Z

    move-result v8

    if-nez v8, :cond_3

    .line 82
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 86
    :cond_3
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>()V

    .line 87
    .local v4, "argNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setName(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setValue(Ljava/lang/String;)V

    .line 89
    invoke-virtual {v1, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    goto :goto_1
.end method

.method public getFirstLine()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    invoke-super {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlResponse;->getFirstLine()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponse()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    .locals 8

    .prologue
    .line 107
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;-><init>()V

    .line 109
    .local v1, "argList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->getActionResponseNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v6

    .line 110
    .local v6, "resNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-nez v6, :cond_1

    .line 122
    :cond_0
    return-object v1

    .line 113
    :cond_1
    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v3

    .line 114
    .local v3, "nArgs":I
    const/4 v2, 0x0

    .local v2, "n":I
    :goto_0
    if-ge v2, v3, :cond_0

    .line 115
    invoke-virtual {v6, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v5

    .line 116
    .local v5, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v4

    .line 117
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getValue()Ljava/lang/String;

    move-result-object v7

    .line 118
    .local v7, "value":Ljava/lang/String;
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    invoke-direct {v0, v4, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public setResponse(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V
    .locals 4
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    .prologue
    .line 58
    const/16 v3, 0xc8

    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->setStatusCode(I)V

    .line 60
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->getBodyNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 61
    .local v0, "bodyNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->createResponseNode(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 62
    .local v2, "resNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->addNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->getEnvelopeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    .line 66
    .local v1, "envNode":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;->setContent(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 67
    return-void
.end method
