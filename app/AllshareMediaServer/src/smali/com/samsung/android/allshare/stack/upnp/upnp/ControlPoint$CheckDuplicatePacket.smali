.class Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckDuplicatePacket"
.end annotation


# instance fields
.field private mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

.field private mLoopCount:I

.field private mPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

.field final synthetic this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V
    .locals 2
    .param p2, "info"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .param p3, "packet"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .prologue
    const/4 v1, 0x0

    .line 859
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 853
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mLoopCount:I

    .line 855
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .line 857
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 860
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    .line 861
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 862
    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 866
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getUSN()Ljava/lang/String;

    move-result-object v2

    .line 867
    .local v2, "usn":Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/USN;->getUUID(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 872
    .local v3, "uuid":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 875
    :cond_1
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getNetworkInterface()Ljava/net/NetworkInterface;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->generateID(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 877
    .local v1, "id":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v4, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getDevice(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 879
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    if-eqz v0, :cond_2

    .line 881
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setSSDPPacket(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;)V

    .line 882
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->setForceExpired(Z)V

    goto :goto_0

    .line 887
    :cond_2
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->isActivityDevice(Ljava/lang/String;)Z
    invoke-static {v4, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$600(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 889
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->addActivityDevice(Ljava/lang/String;)V
    invoke-static {v4, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$700(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;)V

    .line 890
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mExeutor:Ljava/util/concurrent/ExecutorService;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$800(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/util/concurrent/ExecutorService;

    move-result-object v4

    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;

    iget-object v6, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mPacket:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mDiscoveryInfo:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    invoke-direct {v5, v6, v7, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$DescriptionWorker;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 892
    :cond_3
    iget v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mLoopCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mLoopCount:I

    .line 893
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mDeviceHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$900(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$CheckDuplicatePacket;->mLoopCount:I

    mul-int/lit8 v5, v5, 0x32

    add-int/lit8 v5, v5, 0x64

    int-to-long v6, v5

    invoke-virtual {v4, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
