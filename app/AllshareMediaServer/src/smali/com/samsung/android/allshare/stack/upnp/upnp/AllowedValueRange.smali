.class public Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;
.super Ljava/lang/Object;
.source "AllowedValueRange.java"


# static fields
.field public static final ELEM_NAME:Ljava/lang/String; = "allowedValueRange"

.field private static final MAXIMUM:Ljava/lang/String; = "maximum"

.field private static final MINIMUM:Ljava/lang/String; = "minimum"

.field private static final STEP:Ljava/lang/String; = "step"


# instance fields
.field private mAllowedValueRangeNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 0
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;->mAllowedValueRangeNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 50
    return-void
.end method


# virtual methods
.method public getAllowedValueRangeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;->mAllowedValueRangeNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method public getMaximum()Ljava/lang/String;
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;->getAllowedValueRangeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "maximum"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMinimum()Ljava/lang/String;
    .locals 2

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;->getAllowedValueRangeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "minimum"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStep()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;->getAllowedValueRangeNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "step"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
