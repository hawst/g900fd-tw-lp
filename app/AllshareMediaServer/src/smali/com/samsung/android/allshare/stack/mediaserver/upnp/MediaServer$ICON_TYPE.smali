.class public final enum Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;
.super Ljava/lang/Enum;
.source "MediaServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ICON_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

.field public static final enum MACROICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

.field public static final enum MACROICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

.field public static final enum SMALLICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

.field public static final enum SMALLICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 693
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    const-string v1, "MACROICON_JPEG"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->MACROICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    .line 694
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    const-string v1, "MACROICON_PNG"

    invoke-direct {v0, v1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->MACROICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    .line 695
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    const-string v1, "SMALLICON_JPEG"

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->SMALLICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    .line 696
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    const-string v1, "SMALLICON_PNG"

    invoke-direct {v0, v1, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->SMALLICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    .line 692
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->MACROICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->MACROICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->SMALLICON_JPEG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->SMALLICON_PNG:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    aput-object v1, v0, v5

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->$VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 692
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 692
    const-class v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;
    .locals 1

    .prologue
    .line 692
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->$VALUES:[Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    invoke-virtual {v0}, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$ICON_TYPE;

    return-object v0
.end method
