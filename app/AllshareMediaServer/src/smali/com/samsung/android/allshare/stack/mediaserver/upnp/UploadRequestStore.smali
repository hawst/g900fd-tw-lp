.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;
.super Ljava/lang/Object;
.source "UploadRequestStore.java"


# instance fields
.field private final mStore:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;->mStore:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public put(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    .locals 2
    .param p1, "key"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p2, "value"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    .prologue
    .line 24
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 27
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;->mStore:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    goto :goto_0
.end method

.method public remove(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;
    .locals 2
    .param p1, "key"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;

    .prologue
    .line 31
    if-nez p1, :cond_0

    .line 32
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/UploadRequestStore;->mStore:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IUploadItemListener$UploadRequest;

    goto :goto_0
.end method
