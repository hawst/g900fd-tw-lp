.class public Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/AFReleaseActionHandler;
.super Ljava/lang/Object;
.source "AFReleaseActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# instance fields
.field private mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V
    .locals 1
    .param p1, "cameraListener"    # Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/AFReleaseActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 27
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/AFReleaseActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 28
    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 4
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/4 v2, 0x0

    .line 32
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/AFReleaseActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onAFReleaseRequest()I

    move-result v0

    .line 33
    .local v0, "nResult":I
    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    .line 34
    const/16 v3, 0x1f5

    invoke-virtual {p1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    .line 46
    :goto_0
    return v2

    .line 38
    :cond_0
    const-string v3, "AFRELEASERESULT"

    invoke-virtual {p1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v1

    .line 39
    .local v1, "resultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v1, :cond_1

    .line 40
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 46
    const/4 v2, 0x1

    goto :goto_0

    .line 42
    :cond_1
    const/16 v3, 0x192

    invoke-virtual {p1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0
.end method
