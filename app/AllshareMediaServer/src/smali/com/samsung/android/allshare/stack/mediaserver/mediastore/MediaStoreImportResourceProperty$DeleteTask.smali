.class Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;
.super Ljava/lang/Object;
.source "MediaStoreImportResourceProperty.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeleteTask"
.end annotation


# instance fields
.field private mDelayTime:I

.field private mUri:Landroid/net/Uri;

.field private mWhere:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 1
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "where"    # Ljava/lang/String;
    .param p4, "delayTime"    # I

    .prologue
    .line 135
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mUri:Landroid/net/Uri;

    .line 131
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mWhere:Ljava/lang/String;

    .line 133
    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->DELAY_TIME:I
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->access$000()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mDelayTime:I

    .line 136
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mUri:Landroid/net/Uri;

    .line 137
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mWhere:Ljava/lang/String;

    .line 138
    mul-int/lit8 v0, p4, 0x2

    iput v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mDelayTime:I

    .line 139
    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 142
    const-string v1, "MediaStoreImportResourceProperty"

    const-string v2, "run"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "*********where : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mWhere:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const/4 v0, 0x0

    .line 145
    .local v0, "nDeletedCount":I
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mMediaServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->access$100(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;)Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mWhere:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 149
    :cond_0
    const-string v1, "MediaStoreImportResourceProperty"

    const-string v2, "run"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "*********nDeletedCount : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mDelayTime:I

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->MAX_DELAY_TIME:I
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->access$200()I

    move-result v2

    if-le v1, v2, :cond_2

    .line 152
    const-string v1, "MediaStoreImportResourceProperty"

    const-string v2, "run"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "*********Delete Faild : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mWhere:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_1
    :goto_0
    return-void

    .line 156
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mDelayDeleteHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->access$300(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_1

    if-gtz v0, :cond_1

    .line 157
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->mDelayDeleteHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;->access$300(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->this$0:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mUri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mWhere:Ljava/lang/String;

    iget v6, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mDelayTime:I

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty;Landroid/net/Uri;Ljava/lang/String;I)V

    iget v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreImportResourceProperty$DeleteTask;->mDelayTime:I

    int-to-long v4, v3

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
