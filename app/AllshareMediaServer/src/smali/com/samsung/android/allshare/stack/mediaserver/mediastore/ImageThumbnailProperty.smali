.class public Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
.source "ImageThumbnailProperty.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty$ThumbnailCache;
    }
.end annotation


# static fields
.field private static final IMAGE_THUMB_SIZE:I = 0xa0

.field public static final ORIENTATION:Ljava/lang/String; = "orientation"

.field private static THUMB_CACHE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "[B>;"
        }
    .end annotation
.end field


# instance fields
.field private mMedia_store_uri:Landroid/net/Uri;

.field private mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty$ThumbnailCache;

    const/16 v1, 0xc8

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty$ThumbnailCache;-><init>(I)V

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->THUMB_CACHE:Ljava/util/Map;

    .line 68
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->THUMB_CACHE:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->THUMB_CACHE:Ljava/util/Map;

    .line 69
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V
    .locals 2
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
    .param p2, "protocol"    # Ljava/lang/String;
    .param p3, "media_store_url"    # Landroid/net/Uri;
    .param p4, "server"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getContentMicroThumbnailExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 60
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    .line 74
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    .line 75
    iput-object p4, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 76
    return-void
.end method

.method private declared-synchronized buildThumbnail()[B
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 138
    monitor-enter p0

    :try_start_0
    sget-object v6, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->THUMB_CACHE:Ljava/util/Map;

    iget-object v7, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    .local v0, "array":[B
    if-eqz v0, :cond_0

    .line 168
    .end local v0    # "array":[B
    :goto_0
    monitor-exit p0

    return-object v0

    .line 142
    .restart local v0    # "array":[B
    :cond_0
    :try_start_1
    iget-object v6, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    invoke-direct {p0, v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->getThumbnail(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 143
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-nez v1, :cond_1

    move-object v0, v5

    .line 144
    goto :goto_0

    .line 146
    :cond_1
    const-string v6, "orientation"

    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 149
    .local v2, "oriStr":Ljava/lang/String;
    const/16 v6, 0xa0

    const/16 v7, 0xa0

    invoke-direct {p0, v1, v6, v7}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->resizeBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 151
    .local v4, "scaled":Landroid/graphics/Bitmap;
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 153
    const/4 v3, 0x0

    .line 155
    .local v3, "orientation":I
    if-eqz v2, :cond_2

    .line 156
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 159
    :cond_2
    if-eqz v3, :cond_4

    .line 160
    invoke-direct {p0, v4, v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 161
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 163
    if-nez v1, :cond_3

    move-object v0, v5

    .line 164
    goto :goto_0

    .line 166
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    invoke-direct {p0, v5, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->putThumbnailToCache(Landroid/net/Uri;Landroid/graphics/Bitmap;)[B

    move-result-object v0

    goto :goto_0

    .line 168
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    invoke-direct {p0, v5, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->putThumbnailToCache(Landroid/net/Uri;Landroid/graphics/Bitmap;)[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 138
    .end local v0    # "array":[B
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "oriStr":Ljava/lang/String;
    .end local v3    # "orientation":I
    .end local v4    # "scaled":Landroid/graphics/Bitmap;
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method private getThumbnail(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "media_store_uri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    .line 211
    if-nez p1, :cond_1

    move-object v0, v4

    .line 220
    :cond_0
    :goto_0
    return-object v0

    .line 213
    :cond_1
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    .line 214
    .local v2, "id":J
    const/4 v0, 0x0

    .line 215
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 216
    .local v1, "resolver":Landroid/content/ContentResolver;
    if-eqz v1, :cond_0

    .line 217
    const/4 v5, 0x1

    invoke-static {v1, v2, v3, v5, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method private putThumbnailToCache(Landroid/net/Uri;Landroid/graphics/Bitmap;)[B
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v0, 0x0

    .line 172
    const/4 v2, 0x0

    .line 173
    .local v2, "os":Ljava/io/ByteArrayOutputStream;
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-object v0

    .line 177
    :cond_1
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    const/16 v4, 0x2710

    invoke-direct {v3, v4}, Ljava/io/ByteArrayOutputStream;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    .end local v2    # "os":Ljava/io/ByteArrayOutputStream;
    .local v3, "os":Ljava/io/ByteArrayOutputStream;
    :try_start_1
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x50

    invoke-virtual {p2, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .line 185
    .end local v3    # "os":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "os":Ljava/io/ByteArrayOutputStream;
    :goto_1
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->recycle()V

    .line 187
    if-eqz v2, :cond_0

    .line 188
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 189
    .local v0, "array":[B
    sget-object v4, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->THUMB_CACHE:Ljava/util/Map;

    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 180
    .end local v0    # "array":[B
    :catch_0
    move-exception v1

    .line 181
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    const-string v4, "ImageThumbnailProperty"

    const-string v5, "putThumbnailToCache"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "putThumbnailToCache -catch exception :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 180
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "os":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "os":Ljava/io/ByteArrayOutputStream;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "os":Ljava/io/ByteArrayOutputStream;
    .restart local v2    # "os":Ljava/io/ByteArrayOutputStream;
    goto :goto_2
.end method

.method private resizeBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "nWidthThreshold"    # I
    .param p3, "nHeightThreshold"    # I

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    .line 110
    if-nez p1, :cond_0

    .line 111
    const/4 v4, 0x0

    .line 133
    :goto_0
    return-object v4

    .line 113
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 114
    .local v3, "nWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 116
    .local v0, "nHeight":I
    move v2, v3

    .line 117
    .local v2, "nScaledWidth":I
    move v1, v0

    .line 119
    .local v1, "nScaledHeight":I
    if-le v3, v0, :cond_4

    if-le v3, p2, :cond_4

    .line 120
    move v2, p2

    .line 124
    :cond_1
    :goto_1
    if-gt v3, p2, :cond_2

    if-le v0, p3, :cond_3

    .line 125
    :cond_2
    if-le v3, v0, :cond_5

    .line 126
    mul-int v5, v0, v2

    int-to-float v5, v5

    int-to-float v6, v3

    div-float/2addr v5, v6

    add-float/2addr v5, v7

    float-to-int v1, v5

    .line 132
    :cond_3
    :goto_2
    const/4 v5, 0x1

    invoke-static {p1, v2, v1, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 133
    .local v4, "scaled":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 121
    .end local v4    # "scaled":Landroid/graphics/Bitmap;
    :cond_4
    if-lt v0, v3, :cond_1

    if-le v0, p3, :cond_1

    .line 122
    move v1, p3

    goto :goto_1

    .line 128
    :cond_5
    mul-int v5, v3, v1

    int-to-float v5, v5

    int-to-float v6, v0

    div-float/2addr v5, v6

    add-float/2addr v5, v7

    float-to-int v2, v5

    goto :goto_2
.end method

.method private rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "origin"    # Landroid/graphics/Bitmap;
    .param p2, "orientation"    # I

    .prologue
    const/4 v1, 0x0

    .line 197
    if-nez p1, :cond_1

    .line 198
    const/4 p1, 0x0

    .line 204
    .end local p1    # "origin":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p1

    .line 199
    .restart local p1    # "origin":Landroid/graphics/Bitmap;
    :cond_1
    if-eqz p2, :cond_0

    .line 202
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 203
    .local v5, "mat":Landroid/graphics/Matrix;
    int-to-float v0, p2

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 204
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public deleteContent()V
    .locals 2

    .prologue
    .line 106
    sget-object v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->THUMB_CACHE:Ljava/util/Map;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    return-void
.end method

.method public getContentLength()J
    .locals 4

    .prologue
    .line 80
    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->THUMB_CACHE:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 82
    .local v0, "array":[B
    if-nez v0, :cond_0

    .line 83
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->buildThumbnail()[B

    move-result-object v0

    .line 85
    :cond_0
    if-nez v0, :cond_1

    .line 86
    const-wide/16 v2, 0x0

    .line 88
    :goto_0
    return-wide v2

    :cond_1
    array-length v1, v0

    int-to-long v2, v1

    goto :goto_0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 3

    .prologue
    .line 93
    sget-object v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->THUMB_CACHE:Ljava/util/Map;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->mMedia_store_uri:Landroid/net/Uri;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 95
    .local v0, "array":[B
    if-nez v0, :cond_0

    .line 96
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;->buildThumbnail()[B

    move-result-object v0

    .line 98
    :cond_0
    if-nez v0, :cond_1

    .line 99
    const/4 v1, 0x0

    .line 101
    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0
.end method
