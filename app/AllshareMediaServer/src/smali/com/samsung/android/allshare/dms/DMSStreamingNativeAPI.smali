.class public Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;
.super Ljava/lang/Object;
.source "DMSStreamingNativeAPI.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native DoesRvfSessionExist()Z
.end method

.method public static native GetStreamingPort(Ljava/lang/String;Lcom/samsung/android/allshare/dms/PortID;)I
.end method

.method public static native InitializeCore()I
.end method

.method public static native OnStartRecording()V
.end method

.method public static native OnStopRecording()V
.end method

.method public static native SetDownloadUri(Ljava/lang/String;Ljava/lang/String;J)I
.end method

.method public static native StartRvfSessionListener()V
.end method

.method public static native StartStreamingServer()I
.end method

.method public static native StopRvfSessionListener()V
.end method

.method public static native StopStreamingServer()I
.end method

.method public static native TerminateCore()I
.end method
