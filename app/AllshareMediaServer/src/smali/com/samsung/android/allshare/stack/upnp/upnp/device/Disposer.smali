.class public Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;
.super Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;
.source "Disposer.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Disposer"


# instance fields
.field private mCtrlPoint:Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;)V
    .locals 0
    .param p1, "ctrlp"    # Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;-><init>()V

    .line 24
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;->setControlPoint(Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;)V

    .line 25
    return-void
.end method


# virtual methods
.method public getControlPoint()Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;->mCtrlPoint:Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    return-object v0
.end method

.method public interrupted()V
    .locals 0

    .prologue
    .line 78
    return-void
.end method

.method public run()V
    .locals 14

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;->getControlPoint()Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    move-result-object v0

    .line 48
    .local v0, "ctrlp":Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;
    invoke-interface {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;->getExpiredDeviceMonitoringInterval()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long v6, v10, v12

    .line 50
    .local v6, "monitorInterval":J
    const-wide/16 v8, 0x0

    .line 51
    .local v8, "startTime":J
    const-wide/16 v2, 0x0

    .line 52
    .local v2, "endTime":J
    const-wide/16 v4, 0x0

    .line 54
    .local v4, "interval":J
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;->isRunnable()Z

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1

    .line 56
    sub-long v10, v2, v8

    sub-long v4, v6, v10

    .line 57
    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-gtz v10, :cond_0

    const-wide/16 v4, 0x1

    .line 59
    :cond_0
    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 65
    invoke-interface {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;->removeExpiredDevices()V

    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    goto :goto_0

    .line 60
    :catch_0
    move-exception v1

    .line 61
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v10, "Disposer"

    const-string v11, "run"

    const-string v12, "run InterruptedException"

    invoke-static {v10, v11, v12, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 68
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return-void
.end method

.method public setControlPoint(Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;)V
    .locals 0
    .param p1, "ctrlp"    # Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Disposer;->mCtrlPoint:Lcom/samsung/android/allshare/stack/upnp/upnp/IControlPoint;

    .line 35
    return-void
.end method
