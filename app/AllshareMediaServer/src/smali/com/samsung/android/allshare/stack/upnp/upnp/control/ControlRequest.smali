.class public Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;
.super Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;
.source "ControlRequest.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/soap/SOAPRequest;-><init>()V

    .line 63
    return-void
.end method


# virtual methods
.method public isQueryControl()Z
    .locals 1

    .prologue
    .line 75
    const-string v0, "urn:schemas-upnp-org:control-1-0#QueryStateVariable"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;->isQuerySOAPAction(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected setRequestHostFromService(Lcom/samsung/android/allshare/stack/upnp/upnp/Service;)V
    .locals 7
    .param p1, "service"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    .prologue
    const/4 v6, 0x1

    .line 88
    if-nez p1, :cond_0

    .line 169
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getControlURL()Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "ctrlURL":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getRootDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v4

    .line 131
    .local v4, "rootDevice":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;->setURI(Ljava/lang/String;Z)V

    .line 136
    const-string v1, ""

    .line 137
    .local v1, "postURL":Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->isAbsoluteURL(Ljava/lang/String;)Z

    move-result v5

    if-ne v5, v6, :cond_1

    .line 138
    move-object v1, v0

    .line 140
    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_2

    .line 141
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getURLBase()Ljava/lang/String;

    move-result-object v1

    .line 147
    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-gtz v5, :cond_3

    .line 148
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 150
    :cond_3
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getHost(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 151
    .local v2, "reqHost":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTP;->getPort(Ljava/lang/String;)I

    move-result v3

    .line 166
    .local v3, "reqPort":I
    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;->setHost(Ljava/lang/String;I)V

    .line 167
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;->setRequestHost(Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ControlRequest;->setRequestPort(I)V

    goto :goto_0
.end method
