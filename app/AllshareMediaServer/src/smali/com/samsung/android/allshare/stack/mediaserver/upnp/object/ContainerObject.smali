.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
.source "ContainerObject.java"


# static fields
.field public static final CHILD_COUNT:Ljava/lang/String; = "childCount"

.field public static final NAME:Ljava/lang/String; = "container"

.field public static final OBJECT_CONTAINER:Ljava/lang/String; = "object.container"

.field public static final SEARCHABLE:Ljava/lang/String; = "searchable"


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;-><init>()V

    .line 33
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setID(Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setTitle(Ljava/lang/String;)V

    .line 35
    const-string v0, "container"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setName(Ljava/lang/String;)V

    .line 36
    const-string v0, "object.container"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setUPnPClass(Ljava/lang/String;)V

    .line 37
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setSearchable(I)V

    .line 38
    return-void
.end method


# virtual methods
.method public getChildCount()I
    .locals 2

    .prologue
    .line 49
    const-string v1, "childCount"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "count":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51
    :cond_0
    const/4 v1, 0x0

    .line 53
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getSearchable()I
    .locals 2

    .prologue
    .line 65
    const-string v1, "searchable"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 66
    .local v0, "searchable":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    :cond_0
    const/4 v1, 0x0

    .line 69
    :goto_0
    return v1

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public setChildCount(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 45
    const-string v0, "childCount"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 46
    return-void
.end method

.method public setSearchable(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 61
    const-string v0, "searchable"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ContainerObject;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 62
    return-void
.end method

.method public update()V
    .locals 0

    .prologue
    .line 74
    return-void
.end method
