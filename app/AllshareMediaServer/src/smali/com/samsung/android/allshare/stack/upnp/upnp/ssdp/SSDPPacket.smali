.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
.super Ljava/lang/Object;
.source "SSDPPacket.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SSDPPacket"


# instance fields
.field private mDgmPacket:Ljava/net/DatagramPacket;

.field private mHeaderDataMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected mLocalBoundInterface:Ljava/net/NetworkInterface;

.field private mPacketString:Ljava/lang/String;

.field protected mTimeStamp:J


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mDgmPacket:Ljava/net/DatagramPacket;

    .line 67
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mDgmPacket:Ljava/net/DatagramPacket;

    .line 68
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "buf"    # [B
    .param p2, "length"    # I

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mDgmPacket:Ljava/net/DatagramPacket;

    .line 62
    new-instance v0, Ljava/net/DatagramPacket;

    invoke-direct {v0, p1, p2}, Ljava/net/DatagramPacket;-><init>([BI)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mDgmPacket:Ljava/net/DatagramPacket;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    .line 64
    return-void
.end method


# virtual methods
.method public getCacheControl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 234
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    const-string v2, "Cache-Control"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 235
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 237
    .end local v0    # "value":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Cache-Control"

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDataString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 153
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mPacketString:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 154
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mPacketString:Ljava/lang/String;

    .line 165
    :goto_0
    return-object v3

    .line 156
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDatagramPacket()Ljava/net/DatagramPacket;

    move-result-object v1

    .line 157
    .local v1, "packet":Ljava/net/DatagramPacket;
    if-nez v1, :cond_1

    .line 158
    const/4 v3, 0x0

    goto :goto_0

    .line 160
    :cond_1
    :try_start_0
    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getLength()I

    move-result v2

    .line 161
    .local v2, "packetLen":I
    new-instance v3, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5, v2}, Ljava/lang/String;-><init>([BII)V

    iput-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mPacketString:Ljava/lang/String;

    .line 162
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mPacketString:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 163
    .end local v2    # "packetLen":I
    :catch_0
    move-exception v0

    .line 164
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v3, "SSDPPacket"

    const-string v4, "getDataString"

    const-string v5, "getDataString - Nullpointer"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 165
    const-string v3, ""

    goto :goto_0
.end method

.method public getDatagramPacket()Ljava/net/DatagramPacket;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mDgmPacket:Ljava/net/DatagramPacket;

    return-object v0
.end method

.method public getHost()Ljava/lang/String;
    .locals 3

    .prologue
    .line 227
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    const-string v2, "HOST"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 228
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 230
    .end local v0    # "value":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "HOST"

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHostInetAddress()Ljava/net/InetAddress;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 328
    const-string v0, "127.0.0.1"

    .line 329
    .local v0, "addrStr":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getHost()Ljava/lang/String;

    move-result-object v2

    .line 330
    .local v2, "host":Ljava/lang/String;
    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 331
    .local v1, "canmaIdx":I
    if-ltz v1, :cond_1

    .line 332
    invoke-virtual {v2, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 333
    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5b

    if-ne v4, v5, :cond_0

    .line 334
    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 335
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x5d

    if-ne v4, v5, :cond_1

    .line 336
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 338
    :cond_1
    new-instance v3, Ljava/net/InetSocketAddress;

    invoke-direct {v3, v0, v6}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 339
    .local v3, "isockaddr":Ljava/net/InetSocketAddress;
    invoke-virtual {v3}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v4

    return-object v4
.end method

.method public getIntegerValue(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 185
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 188
    :goto_0
    return v1

    .line 186
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getLeaseTime()I
    .locals 1

    .prologue
    .line 377
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getCacheControl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDP;->getLeaseTime(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 3

    .prologue
    .line 241
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    const-string v2, "LOCATION"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 242
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 244
    .end local v0    # "value":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "LOCATION"

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMAN()Ljava/lang/String;
    .locals 3

    .prologue
    .line 249
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    const-string v2, "MAN"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 252
    .end local v0    # "value":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MAN"

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getMSearch()Ljava/lang/String;
    .locals 7

    .prologue
    .line 302
    new-instance v2, Ljava/io/StringReader;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 303
    .local v2, "strReader":Ljava/io/StringReader;
    new-instance v1, Ljava/io/LineNumberReader;

    const/16 v3, 0x400

    invoke-direct {v1, v2, v3}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;I)V

    .line 305
    .local v1, "lineReader":Ljava/io/LineNumberReader;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 312
    if-eqz v2, :cond_0

    .line 313
    :try_start_1
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 314
    :cond_0
    if-eqz v1, :cond_1

    .line 315
    invoke-virtual {v1}, Ljava/io/LineNumberReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 318
    :cond_1
    :goto_0
    return-object v3

    .line 316
    :catch_0
    move-exception v0

    .line 317
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "SSDPPacket"

    const-string v5, "getMSearch"

    const-string v6, " getMSearch - IOException(2)"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 306
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 308
    .restart local v0    # "e":Ljava/io/IOException;
    :try_start_2
    const-string v3, "SSDPPacket"

    const-string v4, "getMSearch"

    const-string v5, " getMSearch - IOException(1)"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 309
    const-string v3, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 312
    if-eqz v2, :cond_2

    .line 313
    :try_start_3
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 314
    :cond_2
    if-eqz v1, :cond_1

    .line 315
    invoke-virtual {v1}, Ljava/io/LineNumberReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 316
    :catch_2
    move-exception v0

    .line 317
    const-string v4, "SSDPPacket"

    const-string v5, "getMSearch"

    const-string v6, " getMSearch - IOException(2)"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0

    .line 311
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 312
    if-eqz v2, :cond_3

    .line 313
    :try_start_4
    invoke-virtual {v2}, Ljava/io/StringReader;->close()V

    .line 314
    :cond_3
    if-eqz v1, :cond_4

    .line 315
    invoke-virtual {v1}, Ljava/io/LineNumberReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 318
    :cond_4
    :goto_1
    throw v3

    .line 316
    :catch_3
    move-exception v0

    .line 317
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v4, "SSDPPacket"

    const-string v5, "getMSearch"

    const-string v6, " getMSearch - IOException(2)"

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public getMX()I
    .locals 4

    .prologue
    .line 289
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    const-string v3, "MX"

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 291
    .local v1, "value":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 292
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 297
    :goto_0
    return v2

    .line 293
    :catch_0
    move-exception v0

    .line 295
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, -0x1

    goto :goto_0

    .line 297
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "MX"

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getIntegerValue(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method public getNT()Ljava/lang/String;
    .locals 3

    .prologue
    .line 263
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    const-string v2, "NT"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 264
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 266
    .end local v0    # "value":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NT"

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNTS()Ljava/lang/String;
    .locals 3

    .prologue
    .line 270
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    const-string v2, "NTS"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 271
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 273
    .end local v0    # "value":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NTS"

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getNetworkInterface()Ljava/net/NetworkInterface;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mLocalBoundInterface:Ljava/net/NetworkInterface;

    return-object v0
.end method

.method public getRemoteAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDatagramPacket()Ljava/net/DatagramPacket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/DatagramPacket;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method public getRemotePort()I
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDatagramPacket()Ljava/net/DatagramPacket;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/DatagramPacket;->getPort()I

    move-result v0

    return v0
.end method

.method public getST()Ljava/lang/String;
    .locals 3

    .prologue
    .line 256
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    const-string v2, "ST"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 257
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 259
    .end local v0    # "value":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ST"

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTimeStamp()J
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mTimeStamp:J

    return-wide v0
.end method

.method public getUSN()Ljava/lang/String;
    .locals 3

    .prologue
    .line 282
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    const-string v2, "USN"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 283
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 285
    .end local v0    # "value":Ljava/lang/String;
    :goto_0
    return-object v0

    .restart local v0    # "value":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "USN"

    invoke-virtual {p0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getValue(Ljava/io/LineNumberReader;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1, "reader"    # Ljava/io/LineNumberReader;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 193
    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {p2, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    .line 194
    .local v1, "bigName":Ljava/lang/String;
    const-string v7, ""

    .line 196
    .local v7, "value":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p1}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 197
    .local v6, "lineStr":Ljava/lang/String;
    :goto_0
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 198
    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    invoke-direct {v3, v6}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;-><init>(Ljava/lang/String;)V

    .line 199
    .local v3, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->hasName()Z

    move-result v8

    if-nez v8, :cond_0

    .line 200
    invoke-virtual {p1}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 201
    goto :goto_0

    .line 203
    :cond_0
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v4

    .line 204
    .local v4, "headerName":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 205
    .local v5, "headerValue":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mHeaderDataMap:Ljava/util/HashMap;

    invoke-virtual {v8, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v8, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v4, v8}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 210
    .local v0, "bigLineHeaderName":Ljava/lang/String;
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 211
    move-object v7, v5

    .line 213
    :cond_1
    invoke-virtual {p1}, Ljava/io/LineNumberReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 214
    goto :goto_0

    .line 215
    .end local v0    # "bigLineHeaderName":Ljava/lang/String;
    .end local v3    # "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    .end local v4    # "headerName":Ljava/lang/String;
    .end local v5    # "headerValue":Ljava/lang/String;
    .end local v6    # "lineStr":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 216
    .local v2, "e":Ljava/io/IOException;
    const-string v8, "SSDPPacket"

    const-string v9, "getValue"

    const-string v10, "gatValue - IOException"

    invoke-static {v8, v9, v10, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 217
    const-string v8, ""

    .line 219
    .end local v2    # "e":Ljava/io/IOException;
    :goto_1
    return-object v8

    .restart local v6    # "lineStr":Ljava/lang/String;
    :cond_2
    move-object v8, v7

    goto :goto_1
.end method

.method public getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "data"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 170
    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 171
    .local v2, "strReader":Ljava/io/StringReader;
    new-instance v1, Ljava/io/LineNumberReader;

    const/16 v4, 0x400

    invoke-direct {v1, v2, v4}, Ljava/io/LineNumberReader;-><init>(Ljava/io/Reader;I)V

    .line 172
    .local v1, "lineReader":Ljava/io/LineNumberReader;
    invoke-virtual {p0, v1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getValue(Ljava/io/LineNumberReader;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 174
    .local v3, "strResult":Ljava/lang/String;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/LineNumberReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    .end local v3    # "strResult":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 175
    .restart local v3    # "strResult":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "SSDPPacket"

    const-string v5, "getValue"

    const-string v6, "getValue  : IOException "

    invoke-static {v4, v5, v6, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 178
    const-string v3, ""

    goto :goto_0
.end method

.method public isAlive()Z
    .locals 1

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getNTS()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/NTS;->isAlive(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isByeBye()Z
    .locals 1

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getNTS()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/NTS;->isByeBye(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isDiscover()Z
    .locals 1

    .prologue
    .line 365
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getMAN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/MAN;->isDiscover(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isPMRDevice()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 357
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getNT()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/NT;->isPMRDevice(Ljava/lang/String;)Z

    move-result v1

    if-ne v1, v0, :cond_1

    .line 361
    :cond_0
    :goto_0
    return v0

    .line 359
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getST()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/ST;->isPMRDevice(Ljava/lang/String;)Z

    move-result v1

    if-eq v1, v0, :cond_0

    .line 361
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 382
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getMSearch()Ljava/lang/String;

    move-result-object v0

    .line 383
    .local v0, "mSearch":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 395
    :cond_0
    :goto_0
    return v1

    .line 386
    :cond_1
    const-string v2, "*"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 389
    const-string v2, "1.1"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 392
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getMX()I

    move-result v2

    if-ltz v2, :cond_0

    .line 395
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setNetworkInterface(Ljava/net/NetworkInterface;)V
    .locals 0
    .param p1, "ni"    # Ljava/net/NetworkInterface;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mLocalBoundInterface:Ljava/net/NetworkInterface;

    .line 106
    return-void
.end method

.method public setTimeStamp(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 87
    iput-wide p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->mTimeStamp:J

    .line 88
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 405
    .local v0, "data":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 406
    const/4 v0, 0x0

    .line 407
    .end local v0    # "data":Ljava/lang/String;
    :cond_0
    return-object v0
.end method
