.class public Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;
.super Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;
.source "Advertiser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser$AdvertiserTimerTask;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Advertiser"


# instance fields
.field private mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

.field private mTimer:Ljava/util/Timer;

.field private mTimerTask:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser$AdvertiserTimerTask;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    .locals 1
    .param p1, "dev"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/util/ThreadCore;-><init>()V

    .line 68
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mTimer:Ljava/util/Timer;

    .line 70
    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mTimerTask:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser$AdvertiserTimerTask;

    .line 27
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->setDevice(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    return-object v0
.end method

.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x1

    .line 50
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v0

    .line 51
    .local v0, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLeaseTime()I

    move-result v6

    int-to-long v2, v6

    .line 53
    .local v2, "leaseTime":J
    const-wide/16 v6, 0x4

    div-long v6, v2, v6

    long-to-double v8, v2

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v10

    const-wide/high16 v12, 0x3fd0000000000000L    # 0.25

    mul-double/2addr v10, v12

    mul-double/2addr v8, v10

    double-to-long v8, v8

    add-long v4, v6, v8

    .line 55
    .local v4, "notifyInterval":J
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->isRunnable()Z

    move-result v6

    if-ne v6, v14, :cond_1

    .line 57
    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, v4

    const-wide/16 v8, 0x0

    mul-long/2addr v6, v8

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->isRunnable()Z

    move-result v6

    if-ne v6, v14, :cond_0

    .line 63
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->announce()V

    goto :goto_0

    .line 58
    :catch_0
    move-exception v1

    .line 59
    .local v1, "e":Ljava/lang/InterruptedException;
    const-string v6, "Advertiser"

    const-string v7, "run"

    const-string v8, "run InterruptedException"

    invoke-static {v6, v7, v8, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 66
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return-void
.end method

.method public setDevice(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V
    .locals 0
    .param p1, "dev"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mDevice:Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    .line 38
    return-void
.end method

.method public start()V
    .locals 18

    .prologue
    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->getDevice()Lcom/samsung/android/allshare/stack/upnp/upnp/Device;

    move-result-object v8

    .line 76
    .local v8, "dev":Lcom/samsung/android/allshare/stack/upnp/upnp/Device;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->stop()V

    .line 78
    invoke-virtual {v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/Device;->getLeaseTime()I

    move-result v2

    int-to-long v10, v2

    .line 79
    .local v10, "leaseTime":J
    const-wide/16 v2, 0x4

    div-long v2, v10, v2

    long-to-double v6, v10

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v14

    const-wide/high16 v16, 0x3fd0000000000000L    # 0.25

    mul-double v14, v14, v16

    mul-double/2addr v6, v14

    double-to-long v6, v6

    add-long v12, v2, v6

    .line 80
    .local v12, "notifyInterval":J
    const-wide/16 v2, 0x3e8

    mul-long v4, v12, v2

    .line 82
    .local v4, "notifyMilli":J
    new-instance v2, Ljava/util/Timer;

    const-string v3, "Advertiser Timer"

    const/4 v6, 0x1

    invoke-direct {v2, v3, v6}, Ljava/util/Timer;-><init>(Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mTimer:Ljava/util/Timer;

    .line 83
    new-instance v2, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser$AdvertiserTimerTask;

    invoke-direct {v2, v8}, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser$AdvertiserTimerTask;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Device;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mTimerTask:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser$AdvertiserTimerTask;

    .line 86
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mTimer:Ljava/util/Timer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mTimerTask:Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser$AdvertiserTimerTask;

    move-wide v6, v4

    invoke-virtual/range {v2 .. v7}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-void

    .line 87
    :catch_0
    move-exception v9

    .line 88
    .local v9, "e":Ljava/lang/Exception;
    const-string v2, "Advertiser"

    const-string v3, "advertiser"

    const-string v6, "advertiser timer exception"

    invoke-static {v2, v3, v6, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 95
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 97
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/device/Advertiser;->mTimer:Ljava/util/Timer;

    .line 98
    return-void
.end method
