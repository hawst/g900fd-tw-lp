.class Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;
.super Ljava/lang/Object;
.source "ContentResourceInfoUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$1;,
        Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    }
.end annotation


# static fields
.field private static final IMAGE_THUMB_SIZE:I = 0xa0

.field private static final TAG:Ljava/lang/String; = "ContentResourceInfoUpdater"

.field private static final VIDEO_THUMB_SIZE:I = 0xa0

.field private static mNonSupportedMimeType:Z


# instance fields
.field private mNonSupportMimeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportedMimeType:Z

    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V
    .locals 3
    .param p1, "server"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    .line 68
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "video/pyv"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "audio/pyv"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "audio/dcf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "audio/imelody"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "video/pvv"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "audio/pvv"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "audio/midi"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "audio/amr"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "audio/ogg"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "audio/application"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "video/application"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "image/application"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "image/gif"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "image/bmp"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    const-string v1, "image/webp"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    if-eqz p1, :cond_0

    .line 89
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    .line 93
    :goto_0
    return-void

    .line 91
    :cond_0
    const-string v0, "ContentResourceInfoUpdater"

    const-string v1, "ContentResourceInfoUpdater"

    const-string v2, "ContentResourceInfoUpdater server is null"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private calculateThumbnailResolution(III)Landroid/util/Pair;
    .locals 7
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "orientation"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    const/16 v5, 0xa0

    .line 826
    const/16 v4, 0x5a

    if-eq p3, v4, :cond_0

    const/16 v4, 0x10e

    if-ne p3, v4, :cond_1

    .line 828
    :cond_0
    move v2, p1

    .line 829
    .local v2, "nTmp":I
    move p1, p2

    .line 830
    move p2, v2

    .line 833
    .end local v2    # "nTmp":I
    :cond_1
    move v1, p1

    .line 834
    .local v1, "nScaledWidth":I
    move v0, p2

    .line 836
    .local v0, "nScaledHeight":I
    if-le p1, p2, :cond_5

    if-le p1, v5, :cond_5

    .line 837
    const/16 v1, 0xa0

    .line 841
    :cond_2
    :goto_0
    if-gt p1, v5, :cond_3

    if-le p2, v5, :cond_4

    .line 842
    :cond_3
    if-le p1, p2, :cond_6

    .line 843
    mul-int v4, p2, v1

    int-to-float v4, v4

    int-to-float v5, p1

    div-float/2addr v4, v5

    add-float/2addr v4, v6

    float-to-int v0, v4

    .line 848
    :cond_4
    :goto_1
    new-instance v3, Landroid/util/Pair;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 849
    .local v3, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    return-object v3

    .line 838
    .end local v3    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_5
    if-lt p2, p1, :cond_2

    if-le p2, v5, :cond_2

    .line 839
    const/16 v0, 0xa0

    goto :goto_0

    .line 845
    :cond_6
    mul-int v4, p1, v0

    int-to-float v4, v4

    int-to-float v5, p2

    div-float/2addr v4, v5

    add-float/2addr v4, v6

    float-to-int v1, v4

    goto :goto_1
.end method

.method private createThumbnailFile(Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 10
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 786
    const/4 v4, 0x1

    .line 787
    .local v4, "returnValue":Z
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 788
    .local v1, "fileCacheItem":Ljava/io/File;
    const/4 v2, 0x0

    .line 791
    .local v2, "out":Ljava/io/OutputStream;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    .line 792
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794
    .end local v2    # "out":Ljava/io/OutputStream;
    .local v3, "out":Ljava/io/OutputStream;
    :try_start_1
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x50

    invoke-virtual {p2, v5, v6, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 801
    if-eqz v3, :cond_0

    .line 802
    :try_start_2
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v2, v3

    .line 810
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    :cond_1
    :goto_0
    return v4

    .line 803
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_0
    move-exception v0

    .line 804
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "ContentResourceInfoUpdater"

    const-string v6, "createThumbnailFile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 806
    const/4 v4, 0x0

    move-object v2, v3

    .line 808
    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_0

    .line 795
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 796
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    const-string v5, "ContentResourceInfoUpdater"

    const-string v6, "createThumbnailFile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Exception: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 797
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 798
    const/4 v4, 0x0

    .line 801
    if-eqz v2, :cond_1

    .line 802
    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 803
    :catch_2
    move-exception v0

    .line 804
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "ContentResourceInfoUpdater"

    const-string v6, "createThumbnailFile"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 806
    const/4 v4, 0x0

    .line 808
    goto :goto_0

    .line 800
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 801
    :goto_2
    if-eqz v2, :cond_2

    .line 802
    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 807
    :cond_2
    :goto_3
    throw v5

    .line 803
    :catch_3
    move-exception v0

    .line 804
    .restart local v0    # "e":Ljava/io/IOException;
    const-string v6, "ContentResourceInfoUpdater"

    const-string v7, "createThumbnailFile"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "IOException: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 805
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 806
    const/4 v4, 0x0

    goto :goto_3

    .line 800
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catchall_1
    move-exception v5

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_2

    .line 795
    .end local v2    # "out":Ljava/io/OutputStream;
    .restart local v3    # "out":Ljava/io/OutputStream;
    :catch_4
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/OutputStream;
    .restart local v2    # "out":Ljava/io/OutputStream;
    goto :goto_1
.end method

.method private findSubBytes([B[B)I
    .locals 5
    .param p1, "sourceBytes"    # [B
    .param p2, "subBytes"    # [B

    .prologue
    const/4 v2, -0x1

    .line 331
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v3, p1

    array-length v4, p2

    if-ge v3, v4, :cond_2

    :cond_0
    move v0, v2

    .line 348
    :cond_1
    :goto_0
    return v0

    .line 335
    :cond_2
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_1
    array-length v3, p1

    array-length v4, p2

    sub-int/2addr v3, v4

    if-ge v0, v3, :cond_5

    .line 336
    const/4 v1, 0x0

    .line 337
    .local v1, "subIndex":I
    :goto_2
    array-length v3, p2

    if-ge v1, v3, :cond_3

    .line 338
    add-int v3, v0, v1

    aget-byte v3, p1, v3

    aget-byte v4, p2, v1

    if-eq v3, v4, :cond_4

    .line 343
    :cond_3
    array-length v3, p2

    if-eq v1, v3, :cond_1

    .line 335
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 341
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v1    # "subIndex":I
    :cond_5
    move v0, v2

    .line 348
    goto :goto_0
.end method

.method private formatTime(J)Ljava/lang/String;
    .locals 11
    .param p1, "time"    # J

    .prologue
    const-wide/16 v8, 0xe10

    const-wide/16 v6, 0x3c

    .line 1142
    const-wide/16 v4, 0x3e8

    div-long/2addr p1, v4

    .line 1143
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-nez v3, :cond_0

    .line 1144
    const-string v3, "00:00:00"

    .line 1148
    :goto_0
    return-object v3

    .line 1145
    :cond_0
    rem-long v4, p1, v6

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->toDateString(J)Ljava/lang/String;

    move-result-object v2

    .line 1146
    .local v2, "seconds":Ljava/lang/String;
    rem-long v4, p1, v8

    div-long/2addr v4, v6

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->toDateString(J)Ljava/lang/String;

    move-result-object v1

    .line 1147
    .local v1, "minutes":Ljava/lang/String;
    div-long v4, p1, v8

    invoke-direct {p0, v4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->toDateString(J)Ljava/lang/String;

    move-result-object v0

    .line 1148
    .local v0, "hours":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private getLeftToRight([B)Z
    .locals 5
    .param p1, "sourceBytes"    # [B

    .prologue
    const/4 v1, 0x0

    .line 352
    if-nez p1, :cond_0

    .line 353
    const-string v2, "ContentResourceInfoUpdater"

    const-string v3, "getLeftToRight"

    const-string v4, "read file header failed!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    :goto_0
    return v1

    .line 357
    :cond_0
    const/16 v0, 0x75

    .line 358
    .local v0, "filePropertyMarkL2R":B
    aget-byte v2, p1, v1

    if-ne v0, v2, :cond_1

    .line 359
    const-string v1, "ContentResourceInfoUpdater"

    const-string v2, "getLeftToRight"

    const-string v3, "getLeftToRight = true"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const/4 v1, 0x1

    goto :goto_0

    .line 362
    :cond_1
    const-string v2, "ContentResourceInfoUpdater"

    const-string v3, "getLeftToRight"

    const-string v4, "getLeftToRight = false"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getRotateThumbnailPath(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "orientation"    # I

    .prologue
    .line 771
    const/4 v1, 0x0

    .line 774
    .local v1, "newPath":Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    const-string v4, "."

    invoke-virtual {p1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 780
    :goto_0
    return-object v1

    .line 775
    :catch_0
    move-exception v0

    .line 776
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "ContentResourceInfoUpdater"

    const-string v3, "changeImageFile"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Exception: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 777
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_rotated.jpg"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getSubTitleFileList(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 9
    .param p0, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "formatList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 1112
    if-nez p0, :cond_1

    move-object v6, v7

    .line 1137
    :cond_0
    :goto_0
    return-object v6

    .line 1114
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1116
    .local v6, "subFileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    const-string v8, "."

    invoke-virtual {p0, v8}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 1117
    .local v3, "index":I
    if-gtz v3, :cond_2

    move-object v6, v7

    .line 1118
    goto :goto_0

    .line 1119
    :cond_2
    const/4 v7, 0x0

    invoke-virtual {p0, v7, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 1130
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1131
    .local v1, "format":Ljava/lang/String;
    const-string v7, "."

    invoke-virtual {v4, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1132
    .local v5, "sub":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1133
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_3

    .line 1134
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private loadAudioResource(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;)Z
    .locals 50
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    .prologue
    .line 133
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    if-nez v4, :cond_1

    .line 134
    :cond_0
    const/4 v4, 0x0

    .line 298
    :goto_0
    return v4

    .line 136
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->getUri()Landroid/net/Uri;

    move-result-object v5

    .line 137
    .local v5, "uri":Landroid/net/Uri;
    const/16 v25, 0x0

    .line 139
    .local v25, "c":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 140
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v25

    .line 143
    :cond_2
    if-eqz v25, :cond_f

    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_f

    .line 144
    const-string v4, "_data"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 145
    .local v26, "dataIdx":I
    invoke-interface/range {v25 .. v26}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v40

    .line 147
    .local v40, "path":Ljava/lang/String;
    const-string v4, "album_id"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 148
    .local v17, "albumIdx":I
    move-object/from16 v0, v25

    move/from16 v1, v17

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 150
    .local v16, "albumId":I
    const-string v4, "mime_type"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v46

    .line 151
    .local v46, "typeIdx":I
    move-object/from16 v0, v25

    move/from16 v1, v46

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v45

    .line 153
    .local v45, "type":Ljava/lang/String;
    const-string v4, "duration"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v30

    .line 154
    .local v30, "durationIdx":I
    move-object/from16 v0, v25

    move/from16 v1, v30

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v28

    .line 156
    .local v28, "duration":J
    const-string v4, "artist"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 157
    .local v23, "artistIdx":I
    move-object/from16 v0, v25

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 158
    .local v22, "artist":Ljava/lang/String;
    if-eqz v22, :cond_3

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 159
    const-string v4, "upnp:artist"

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_3
    const-string v4, "album"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 162
    .local v18, "albumInfoIdx":I
    move-object/from16 v0, v25

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 163
    .local v14, "album":Ljava/lang/String;
    if-eqz v14, :cond_4

    invoke-virtual {v14}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_4

    .line 164
    const-string v4, "upnp:album"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    :cond_4
    const-string v4, "year"

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v47

    .line 167
    .local v47, "yearIdx":I
    move-object/from16 v0, v25

    move/from16 v1, v47

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v48

    .line 168
    .local v48, "year":J
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-wide/from16 v0, v48

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ""

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    .line 170
    .local v43, "strDate":Ljava/lang/String;
    invoke-virtual/range {v43 .. v43}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v6, 0x8

    if-ne v4, v6, :cond_5

    .line 171
    const/4 v4, 0x0

    const/4 v6, 0x4

    move-object/from16 v0, v43

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v44

    .line 172
    .local v44, "strYear":Ljava/lang/String;
    const/4 v4, 0x4

    const/4 v6, 0x6

    move-object/from16 v0, v43

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v37

    .line 173
    .local v37, "month":Ljava/lang/String;
    const/4 v4, 0x6

    const/16 v6, 0x8

    move-object/from16 v0, v43

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v27

    .line 174
    .local v27, "date":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v44

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "-"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v37

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "-"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    .line 176
    const-string v4, "dc:date"

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    .end local v27    # "date":Ljava/lang/String;
    .end local v37    # "month":Ljava/lang/String;
    .end local v44    # "strYear":Ljava/lang/String;
    :cond_5
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 180
    const/16 v25, 0x0

    .line 182
    if-eqz v45, :cond_6

    if-nez v40, :cond_7

    .line 183
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 187
    :cond_7
    const-string v4, "genres"

    invoke-static {v5, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 188
    .local v7, "genreUri":Landroid/net/Uri;
    const/16 v35, 0x0

    .line 189
    .local v35, "genreCursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v4, 0x1

    new-array v8, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v10, "name"

    aput-object v10, v8, v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v35

    .line 196
    :cond_8
    const-string v4, "audio/x-wav"

    move-object/from16 v0, v45

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 197
    const-string v45, "audio/wav"

    .line 199
    :cond_9
    if-nez v35, :cond_a

    .line 200
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 202
    :cond_a
    invoke-interface/range {v35 .. v35}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 203
    const/4 v4, 0x0

    move-object/from16 v0, v35

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v34

    .line 204
    .local v34, "genre":Ljava/lang/String;
    if-eqz v34, :cond_b

    invoke-virtual/range {v34 .. v34}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_b

    .line 205
    const-string v4, "upnp:genre"

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    .end local v34    # "genre":Ljava/lang/String;
    :cond_b
    invoke-interface/range {v35 .. v35}, Landroid/database/Cursor;->close()V

    .line 212
    const-string v41, ""

    .line 213
    .local v41, "protocol":Ljava/lang/String;
    const/16 v42, 0x0

    .line 215
    .local v42, "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v39

    .line 218
    .local v39, "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    new-instance v24, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;

    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-direct {v0, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$1;)V

    .line 219
    .local v24, "audioInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    move-object/from16 v0, p0

    move-object/from16 v1, v40

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->parseWMAFileInfo(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;)Z

    .line 220
    const/4 v4, 0x0

    move-object/from16 v0, v24

    iget v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;->audioMaxBitRate:I

    move-object/from16 v0, v39

    move-object/from16 v1, v45

    invoke-virtual {v0, v1, v4, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildAudioProtocol(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v41

    .line 222
    new-instance v42, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/DefaultFileResourceProperty;

    .end local v42    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, v40

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v42

    move-object/from16 v1, p1

    move-object/from16 v2, v41

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/DefaultFileResourceProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Ljava/io/File;)V

    .line 223
    .restart local v42    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    const-string v4, "duration"

    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->formatTime(J)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v42

    invoke-virtual {v0, v4, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 224
    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V

    .line 227
    if-lez v16, :cond_f

    .line 228
    sget-object v4, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move/from16 v0, v16

    int-to-long v10, v0

    invoke-static {v4, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v9

    .line 230
    .local v9, "albumUri":Landroid/net/Uri;
    const/4 v15, 0x0

    .line 231
    .local v15, "albumCur":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 232
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 235
    :cond_c
    if-eqz v15, :cond_e

    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_e

    .line 237
    const-string v4, "album_art"

    invoke-interface {v15, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v32

    .line 238
    .local v32, "fileIdx":I
    move/from16 v0, v32

    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v33

    .line 239
    .local v33, "filePath":Ljava/lang/String;
    if-eqz v33, :cond_e

    invoke-virtual/range {v33 .. v33}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_e

    .line 241
    new-instance v38, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v38 .. v38}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 242
    .local v38, "opts":Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x1

    move-object/from16 v0, v38

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 243
    move-object/from16 v0, v33

    move-object/from16 v1, v38

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 244
    move-object/from16 v0, v38

    iget-object v0, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    move-object/from16 v36, v0

    .line 250
    .local v36, "mimeType":Ljava/lang/String;
    if-nez v36, :cond_d

    .line 251
    const-string v4, "ContentResourceInfoUpdater"

    const-string v6, "loadAudioResource"

    const-string v8, "mimeType value is null. Set default mimeType value"

    invoke-static {v4, v6, v8}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    const-string v36, "image/jpeg"

    .line 256
    :cond_d
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, v39

    move-object/from16 v1, v36

    invoke-virtual {v0, v1, v4, v6, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildImageProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v21

    .line 258
    .local v21, "albumartProtocol":Ljava/lang/String;
    new-instance v42, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/AlbumartProperty;

    .end local v42    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, v33

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v42

    move-object/from16 v1, p1

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/AlbumartProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Ljava/io/File;)V

    .line 263
    .restart local v42    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V

    .line 266
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getContentAlbumArtExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 268
    .local v19, "albumUriStr":Ljava/lang/String;
    new-instance v20, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    const-string v4, "upnp:albumArtURI"

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-direct {v0, v4, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    .local v20, "albumartCP":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    const-string v4, "image/png"

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_11

    .line 271
    const-string v4, "dlna:profileID"

    const-string v6, "PNG_TN"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 278
    :goto_1
    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    .line 282
    .end local v19    # "albumUriStr":Ljava/lang/String;
    .end local v20    # "albumartCP":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v21    # "albumartProtocol":Ljava/lang/String;
    .end local v32    # "fileIdx":I
    .end local v33    # "filePath":Ljava/lang/String;
    .end local v36    # "mimeType":Ljava/lang/String;
    .end local v38    # "opts":Landroid/graphics/BitmapFactory$Options;
    :cond_e
    if-eqz v15, :cond_f

    .line 283
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    .line 288
    .end local v7    # "genreUri":Landroid/net/Uri;
    .end local v9    # "albumUri":Landroid/net/Uri;
    .end local v14    # "album":Ljava/lang/String;
    .end local v15    # "albumCur":Landroid/database/Cursor;
    .end local v16    # "albumId":I
    .end local v17    # "albumIdx":I
    .end local v18    # "albumInfoIdx":I
    .end local v22    # "artist":Ljava/lang/String;
    .end local v23    # "artistIdx":I
    .end local v24    # "audioInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    .end local v26    # "dataIdx":I
    .end local v28    # "duration":J
    .end local v30    # "durationIdx":I
    .end local v35    # "genreCursor":Landroid/database/Cursor;
    .end local v39    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .end local v40    # "path":Ljava/lang/String;
    .end local v41    # "protocol":Ljava/lang/String;
    .end local v42    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v43    # "strDate":Ljava/lang/String;
    .end local v45    # "type":Ljava/lang/String;
    .end local v46    # "typeIdx":I
    .end local v47    # "yearIdx":I
    .end local v48    # "year":J
    :cond_f
    if-eqz v25, :cond_10

    .line 289
    invoke-interface/range {v25 .. v25}, Landroid/database/Cursor;->close()V

    .line 290
    const/16 v25, 0x0

    .line 298
    :cond_10
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 274
    .restart local v7    # "genreUri":Landroid/net/Uri;
    .restart local v9    # "albumUri":Landroid/net/Uri;
    .restart local v14    # "album":Ljava/lang/String;
    .restart local v15    # "albumCur":Landroid/database/Cursor;
    .restart local v16    # "albumId":I
    .restart local v17    # "albumIdx":I
    .restart local v18    # "albumInfoIdx":I
    .restart local v19    # "albumUriStr":Ljava/lang/String;
    .restart local v20    # "albumartCP":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .restart local v21    # "albumartProtocol":Ljava/lang/String;
    .restart local v22    # "artist":Ljava/lang/String;
    .restart local v23    # "artistIdx":I
    .restart local v24    # "audioInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    .restart local v26    # "dataIdx":I
    .restart local v28    # "duration":J
    .restart local v30    # "durationIdx":I
    .restart local v32    # "fileIdx":I
    .restart local v33    # "filePath":Ljava/lang/String;
    .restart local v35    # "genreCursor":Landroid/database/Cursor;
    .restart local v36    # "mimeType":Ljava/lang/String;
    .restart local v38    # "opts":Landroid/graphics/BitmapFactory$Options;
    .restart local v39    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .restart local v40    # "path":Ljava/lang/String;
    .restart local v41    # "protocol":Ljava/lang/String;
    .restart local v42    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v43    # "strDate":Ljava/lang/String;
    .restart local v45    # "type":Ljava/lang/String;
    .restart local v46    # "typeIdx":I
    .restart local v47    # "yearIdx":I
    .restart local v48    # "year":J
    :cond_11
    const-string v4, "dlna:profileID"

    const-string v6, "JPEG_TN"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 292
    .end local v7    # "genreUri":Landroid/net/Uri;
    .end local v9    # "albumUri":Landroid/net/Uri;
    .end local v14    # "album":Ljava/lang/String;
    .end local v15    # "albumCur":Landroid/database/Cursor;
    .end local v16    # "albumId":I
    .end local v17    # "albumIdx":I
    .end local v18    # "albumInfoIdx":I
    .end local v19    # "albumUriStr":Ljava/lang/String;
    .end local v20    # "albumartCP":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v21    # "albumartProtocol":Ljava/lang/String;
    .end local v22    # "artist":Ljava/lang/String;
    .end local v23    # "artistIdx":I
    .end local v24    # "audioInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    .end local v26    # "dataIdx":I
    .end local v28    # "duration":J
    .end local v30    # "durationIdx":I
    .end local v32    # "fileIdx":I
    .end local v33    # "filePath":Ljava/lang/String;
    .end local v35    # "genreCursor":Landroid/database/Cursor;
    .end local v36    # "mimeType":Ljava/lang/String;
    .end local v38    # "opts":Landroid/graphics/BitmapFactory$Options;
    .end local v39    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .end local v40    # "path":Ljava/lang/String;
    .end local v41    # "protocol":Ljava/lang/String;
    .end local v42    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v43    # "strDate":Ljava/lang/String;
    .end local v45    # "type":Ljava/lang/String;
    .end local v46    # "typeIdx":I
    .end local v47    # "yearIdx":I
    .end local v48    # "year":J
    :catch_0
    move-exception v31

    .line 293
    .local v31, "e":Landroid/database/CursorWindowAllocationException;
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 294
    .end local v31    # "e":Landroid/database/CursorWindowAllocationException;
    :catch_1
    move-exception v31

    .line 295
    .local v31, "e":Ljava/lang/Exception;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Exception;->printStackTrace()V

    .line 296
    const/4 v4, 0x0

    goto/16 :goto_0
.end method

.method private loadImageResource(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;)Z
    .locals 34
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    .prologue
    .line 549
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    if-nez v6, :cond_1

    .line 550
    :cond_0
    const-string v6, "ContentResourceInfoUpdater"

    const-string v8, "loadImageResource"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "item="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " , mServer="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    const/4 v6, 0x0

    .line 709
    :goto_0
    return v6

    .line 554
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->getUri()Landroid/net/Uri;

    move-result-object v7

    .line 556
    .local v7, "uri":Landroid/net/Uri;
    const/4 v14, 0x0

    .line 557
    .local v14, "c":Landroid/database/Cursor;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 558
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 561
    :cond_2
    if-eqz v14, :cond_c

    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_c

    .line 562
    const-string v6, "_id"

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 563
    .local v20, "idIdx":I
    move/from16 v0, v20

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    .line 565
    .local v19, "id":I
    const-string v6, "_data"

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 566
    .local v15, "dataIdx":I
    invoke-interface {v14, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v25

    .line 568
    .local v25, "path":Ljava/lang/String;
    const/16 v23, 0x0

    .line 569
    .local v23, "orientation":I
    const-string v6, "orientation"

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v22

    .line 572
    .local v22, "oriIdx":I
    :try_start_1
    move/from16 v0, v22

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v23

    .line 578
    :goto_1
    :try_start_2
    new-instance v21, Landroid/graphics/BitmapFactory$Options;

    invoke-direct/range {v21 .. v21}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 579
    .local v21, "opts":Landroid/graphics/BitmapFactory$Options;
    const/4 v6, 0x1

    move-object/from16 v0, v21

    iput-boolean v6, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 580
    move-object/from16 v0, v25

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 582
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move/from16 v18, v0

    .line 583
    .local v18, "height":I
    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    move/from16 v33, v0

    .line 584
    .local v33, "width":I
    move-object/from16 v0, v21

    iget-object v0, v0, Landroid/graphics/BitmapFactory$Options;->outMimeType:Ljava/lang/String;

    move-object/from16 v32, v0

    .line 585
    .local v32, "type":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportMimeList:Ljava/util/ArrayList;

    move-object/from16 v0, v32

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    sput-boolean v6, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportedMimeType:Z

    .line 586
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 587
    const/4 v14, 0x0

    .line 589
    if-eqz v32, :cond_3

    if-eqz v25, :cond_3

    sget-boolean v6, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mNonSupportedMimeType:Z

    if-eqz v6, :cond_4

    .line 590
    :cond_3
    const-string v6, "ContentResourceInfoUpdater"

    const-string v8, "loadImageResource"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "type="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v32

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " , path="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v25

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 573
    .end local v18    # "height":I
    .end local v21    # "opts":Landroid/graphics/BitmapFactory$Options;
    .end local v32    # "type":Ljava/lang/String;
    .end local v33    # "width":I
    :catch_0
    move-exception v16

    .line 574
    .local v16, "e":Ljava/lang/Exception;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 703
    .end local v15    # "dataIdx":I
    .end local v16    # "e":Ljava/lang/Exception;
    .end local v19    # "id":I
    .end local v20    # "idIdx":I
    .end local v22    # "oriIdx":I
    .end local v23    # "orientation":I
    .end local v25    # "path":Ljava/lang/String;
    :catch_1
    move-exception v16

    .line 704
    .local v16, "e":Landroid/database/CursorWindowAllocationException;
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 596
    .end local v16    # "e":Landroid/database/CursorWindowAllocationException;
    .restart local v15    # "dataIdx":I
    .restart local v18    # "height":I
    .restart local v19    # "id":I
    .restart local v20    # "idIdx":I
    .restart local v21    # "opts":Landroid/graphics/BitmapFactory$Options;
    .restart local v22    # "oriIdx":I
    .restart local v23    # "orientation":I
    .restart local v25    # "path":Ljava/lang/String;
    .restart local v32    # "type":Ljava/lang/String;
    .restart local v33    # "width":I
    :cond_4
    const-string v6, "image/x-ms-bmp"

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 597
    const-string v32, "image/bmp"

    .line 600
    :cond_5
    const-string v6, "image/jpg"

    move-object/from16 v0, v32

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 601
    const-string v32, "image/jpeg"

    .line 604
    :cond_6
    const-string v26, ""

    .line 605
    .local v26, "protocol":Ljava/lang/String;
    const/16 v27, 0x0

    .line 607
    .local v27, "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v24

    .line 610
    .local v24, "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    const/4 v6, 0x0

    move-object/from16 v0, v24

    move-object/from16 v1, v32

    move/from16 v2, v33

    move/from16 v3, v18

    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildImageProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v26

    .line 613
    if-lez v23, :cond_e

    .line 614
    new-instance v27, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/DefaultFileResourceProperty;

    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "?rtt="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v8, Ljava/io/File;

    move-object/from16 v0, v25

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v6, v2, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/DefaultFileResourceProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 620
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    :goto_2
    const-string v6, "resolution"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, v33

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v6, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 621
    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V

    .line 623
    const/16 v6, 0xa0

    move/from16 v0, v33

    if-lt v6, v0, :cond_7

    const/16 v6, 0xa0

    move/from16 v0, v18

    if-ge v6, v0, :cond_8

    .line 626
    :cond_7
    const-string v6, "image/jpeg"

    const/4 v8, 0x2

    const/16 v9, 0xa0

    const/16 v10, 0xa0

    move-object/from16 v0, v24

    invoke-virtual {v0, v6, v8, v9, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildImageProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v26

    .line 629
    new-instance v27, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;

    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2, v7, v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ImageThumbnailProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;)V

    .line 631
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    const/4 v6, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v33

    move/from16 v2, v18

    invoke-direct {v0, v1, v2, v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->calculateThumbnailResolution(III)Landroid/util/Pair;

    move-result-object v28

    .line 633
    .local v28, "rol":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    const-string v8, "resolution"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v28

    iget-object v6, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "x"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v28

    iget-object v6, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v27

    invoke-virtual {v0, v8, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 635
    const-string v6, "orientation"

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v6, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 637
    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V

    .line 640
    .end local v28    # "rol":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_8
    const/16 v6, 0x280

    move/from16 v0, v33

    if-lt v6, v0, :cond_9

    const/16 v6, 0x1e0

    move/from16 v0, v18

    if-ge v6, v0, :cond_c

    .line 642
    :cond_9
    const/16 v30, 0x0

    .line 643
    .local v30, "thumbCur":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    if-eqz v6, :cond_a

    .line 644
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v10, 0x0

    const-string v11, "image_id = ?"

    const/4 v6, 0x1

    new-array v12, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v6

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v30

    .line 652
    :cond_a
    if-eqz v30, :cond_f

    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_f

    .line 654
    const-string v6, "_data"

    move-object/from16 v0, v30

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v29

    .line 655
    .local v29, "tDataIdx":I
    move-object/from16 v0, v30

    move/from16 v1, v29

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v31

    .line 656
    .local v31, "thumbPath":Ljava/lang/String;
    new-instance v17, Ljava/io/File;

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 658
    .local v17, "file":Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->isFile()Z

    move-result v6

    if-nez v6, :cond_b

    .line 659
    const-string v6, "ContentResourceInfoUpdater"

    const-string v8, "loadImageResource"

    const-string v9, "No File on URL - create small Thumnail!!"

    invoke-static {v6, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 660
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v7}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-static {v6, v8, v9, v10, v11}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 664
    :cond_b
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    move-object/from16 v3, v30

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->updateItemThumbnailResource(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;Landroid/database/Cursor;I)V

    .line 692
    .end local v17    # "file":Ljava/io/File;
    .end local v29    # "tDataIdx":I
    .end local v31    # "thumbPath":Ljava/lang/String;
    :goto_3
    if-eqz v30, :cond_c

    .line 693
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->close()V

    .line 699
    .end local v15    # "dataIdx":I
    .end local v18    # "height":I
    .end local v19    # "id":I
    .end local v20    # "idIdx":I
    .end local v21    # "opts":Landroid/graphics/BitmapFactory$Options;
    .end local v22    # "oriIdx":I
    .end local v23    # "orientation":I
    .end local v24    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .end local v25    # "path":Ljava/lang/String;
    .end local v26    # "protocol":Ljava/lang/String;
    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v30    # "thumbCur":Landroid/database/Cursor;
    .end local v32    # "type":Ljava/lang/String;
    .end local v33    # "width":I
    :cond_c
    if-eqz v14, :cond_d

    .line 700
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 709
    :cond_d
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 617
    .restart local v15    # "dataIdx":I
    .restart local v18    # "height":I
    .restart local v19    # "id":I
    .restart local v20    # "idIdx":I
    .restart local v21    # "opts":Landroid/graphics/BitmapFactory$Options;
    .restart local v22    # "oriIdx":I
    .restart local v23    # "orientation":I
    .restart local v24    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .restart local v25    # "path":Ljava/lang/String;
    .restart local v26    # "protocol":Ljava/lang/String;
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v32    # "type":Ljava/lang/String;
    .restart local v33    # "width":I
    :cond_e
    new-instance v27, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/DefaultFileResourceProperty;

    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    new-instance v6, Ljava/io/File;

    move-object/from16 v0, v25

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-direct {v0, v1, v2, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/DefaultFileResourceProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Ljava/io/File;)V

    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    goto/16 :goto_2

    .line 667
    .restart local v30    # "thumbCur":Landroid/database/Cursor;
    :cond_f
    if-eqz v30, :cond_10

    .line 668
    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->close()V

    .line 669
    const/16 v30, 0x0

    .line 672
    :cond_10
    const-string v6, "ContentResourceInfoUpdater"

    const-string v8, "loadImageResource"

    const-string v9, "Thumbnail Not Found - Try to get thumbnail from DB "

    invoke-static {v6, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v7}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-static {v6, v8, v9, v10, v11}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 676
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    if-eqz v6, :cond_11

    .line 677
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    sget-object v9, Landroid/provider/MediaStore$Images$Thumbnails;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v10, 0x0

    const-string v11, "image_id = ?"

    const/4 v6, 0x1

    new-array v12, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v12, v6

    const/4 v13, 0x0

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v30

    .line 684
    :cond_11
    if-eqz v30, :cond_12

    invoke-interface/range {v30 .. v30}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_12

    .line 685
    const-string v6, "ContentResourceInfoUpdater"

    const-string v8, "loadImageResource"

    const-string v9, "retry SUCCESS"

    invoke-static {v6, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v24

    move-object/from16 v3, v30

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->updateItemThumbnailResource(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;Landroid/database/Cursor;I)V
    :try_end_2
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_3

    .line 705
    .end local v15    # "dataIdx":I
    .end local v18    # "height":I
    .end local v19    # "id":I
    .end local v20    # "idIdx":I
    .end local v21    # "opts":Landroid/graphics/BitmapFactory$Options;
    .end local v22    # "oriIdx":I
    .end local v23    # "orientation":I
    .end local v24    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .end local v25    # "path":Ljava/lang/String;
    .end local v26    # "protocol":Ljava/lang/String;
    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v30    # "thumbCur":Landroid/database/Cursor;
    .end local v32    # "type":Ljava/lang/String;
    .end local v33    # "width":I
    :catch_2
    move-exception v16

    .line 706
    .local v16, "e":Ljava/lang/Exception;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Exception;->printStackTrace()V

    .line 707
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 688
    .end local v16    # "e":Ljava/lang/Exception;
    .restart local v15    # "dataIdx":I
    .restart local v18    # "height":I
    .restart local v19    # "id":I
    .restart local v20    # "idIdx":I
    .restart local v21    # "opts":Landroid/graphics/BitmapFactory$Options;
    .restart local v22    # "oriIdx":I
    .restart local v23    # "orientation":I
    .restart local v24    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .restart local v25    # "path":Ljava/lang/String;
    .restart local v26    # "protocol":Ljava/lang/String;
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v30    # "thumbCur":Landroid/database/Cursor;
    .restart local v32    # "type":Ljava/lang/String;
    .restart local v33    # "width":I
    :cond_12
    :try_start_3
    const-string v6, "ContentResourceInfoUpdater"

    const-string v8, "loadImageResource"

    const-string v9, "Thumbnail Not Found"

    invoke-static {v6, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_3
.end method

.method private loadVideoResource(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;)Z
    .locals 38
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    .prologue
    .line 853
    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    if-nez v4, :cond_1

    .line 854
    :cond_0
    const-string v4, "ContentResourceInfoUpdater"

    const-string v8, "loadVideoResource"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "item="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " , mServer="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 855
    const/4 v4, 0x0

    .line 1018
    :goto_0
    return v4

    .line 859
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->getUri()Landroid/net/Uri;

    move-result-object v5

    .line 860
    .local v5, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    if-nez v4, :cond_2

    .line 861
    const/4 v4, 0x0

    goto :goto_0

    .line 864
    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 866
    .local v14, "c":Landroid/database/Cursor;
    if-eqz v14, :cond_d

    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_d

    .line 867
    const-string v4, "_data"

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 868
    .local v21, "fileIdx":I
    move/from16 v0, v21

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v22

    .line 870
    .local v22, "filePath":Ljava/lang/String;
    const-string v4, "resolution"

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v26

    .line 871
    .local v26, "resIdx":I
    move/from16 v0, v26

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 873
    .local v29, "resolution":Ljava/lang/String;
    const-string v4, "duration"

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 874
    .local v16, "durIdx":I
    move/from16 v0, v16

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 876
    .local v18, "duration":J
    const-string v4, "mime_type"

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v35

    .line 877
    .local v35, "typeIdx":I
    move/from16 v0, v35

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 879
    .local v7, "type":Ljava/lang/String;
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 880
    const/4 v14, 0x0

    .line 882
    if-eqz v7, :cond_3

    if-nez v22, :cond_4

    .line 883
    :cond_3
    const-string v4, "ContentResourceInfoUpdater"

    const-string v8, "loadVideoResource"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "type="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " , filePath="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 884
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 889
    :cond_4
    const-string v4, "video/x-mkv"

    invoke-virtual {v7, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "video/mkv"

    invoke-virtual {v7, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    const-string v4, "video/x-matroska"

    invoke-virtual {v7, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 891
    :cond_5
    const-string v7, "video/x-mkv"

    .line 893
    :cond_6
    const-string v4, "video/flv"

    invoke-virtual {v7, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 894
    const-string v7, "video/x-flv"

    .line 896
    :cond_7
    const/16 v27, 0x0

    .line 897
    .local v27, "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v6

    .line 900
    .local v6, "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    new-instance v37, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;

    const/4 v4, 0x0

    move-object/from16 v0, v37

    invoke-direct {v0, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$1;)V

    .line 901
    .local v37, "videoInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v37

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->parseWMVFileInfo(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;)Z

    .line 902
    const/4 v8, 0x0

    move-object/from16 v0, v37

    iget v9, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;->videoWidth:I

    move-object/from16 v0, v37

    iget v10, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;->videoHeight:I

    move-object/from16 v0, v37

    iget v11, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;->audioAvgBitRate:I

    invoke-virtual/range {v6 .. v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildVideoProtocol(Ljava/lang/String;IIII)Ljava/lang/String;

    move-result-object v25

    .line 906
    .local v25, "protocol":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v15

    .line 908
    .local v15, "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    if-nez v15, :cond_8

    .line 909
    const-string v4, "ContentResourceInfoUpdater"

    const-string v8, "loadVideoResource"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "cds="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 913
    :cond_8
    new-instance v27, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/DefaultFileResourceProperty;

    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    new-instance v4, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    move-object/from16 v2, v25

    invoke-direct {v0, v1, v2, v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/DefaultFileResourceProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Ljava/io/File;)V

    .line 914
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    const-string v4, "resolution"

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 915
    const-string v4, "duration"

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->formatTime(J)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 916
    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V

    .line 929
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->updateSubtitleProperty(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;Ljava/lang/String;)V

    .line 932
    const/4 v13, 0x0

    .line 933
    .local v13, "bitmap":Landroid/graphics/Bitmap;
    new-instance v30, Landroid/media/MediaMetadataRetriever;

    invoke-direct/range {v30 .. v30}, Landroid/media/MediaMetadataRetriever;-><init>()V
    :try_end_0
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 935
    .local v30, "retriever":Landroid/media/MediaMetadataRetriever;
    :try_start_1
    move-object/from16 v0, v30

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    .line 937
    const-wide/32 v8, 0x8f0d180

    move-object/from16 v0, v30

    invoke-virtual {v0, v8, v9}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v13

    .line 941
    :try_start_2
    invoke-virtual/range {v30 .. v30}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 945
    :goto_1
    if-eqz v13, :cond_d

    .line 946
    const/16 v4, 0xa0

    const/16 v8, 0xa0

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v4, v8}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->resizeBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v31

    .line 947
    .local v31, "scaled":Landroid/graphics/Bitmap;
    const/16 v33, 0xa0

    .line 948
    .local v33, "scaledWidth":I
    const/16 v32, 0xa0

    .line 950
    .local v32, "scaledHeight":I
    if-eqz v31, :cond_9

    .line 951
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v33

    .line 952
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v32

    .line 955
    :cond_9
    new-instance v34, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getCacheDirectory()Ljava/io/File;

    move-result-object v4

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ".thumb"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v34

    invoke-direct {v0, v4, v8}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 957
    .local v34, "tempFile":Ljava/io/File;
    const/16 v23, 0x0

    .line 959
    .local v23, "fos":Ljava/io/FileOutputStream;
    :try_start_3
    new-instance v24, Ljava/io/FileOutputStream;

    move-object/from16 v0, v24

    move-object/from16 v1, v34

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 962
    .end local v23    # "fos":Ljava/io/FileOutputStream;
    .local v24, "fos":Ljava/io/FileOutputStream;
    if-eqz v31, :cond_a

    .line 963
    :try_start_4
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x50

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v8, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_4
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 976
    :cond_a
    :goto_2
    :try_start_5
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileOutputStream;->flush()V

    .line 978
    invoke-virtual/range {v34 .. v34}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v22

    .line 979
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->getContentAlbumArtExportURL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v36

    .line 981
    .local v36, "url":Ljava/lang/String;
    const-string v4, "image/jpeg"

    const/4 v8, 0x2

    move/from16 v0, v33

    move/from16 v1, v32

    invoke-virtual {v6, v4, v8, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildImageProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v12

    .line 986
    .local v12, "albumartProtocol":Ljava/lang/String;
    new-instance v28, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/VideoAlbumartProperty;

    new-instance v4, Ljava/io/File;

    move-object/from16 v0, v22

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/VideoAlbumartProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Ljava/io/File;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 987
    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .local v28, "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    :try_start_6
    const-string v4, "resolution"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v33 .. v33}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "x"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static/range {v32 .. v32}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v28

    invoke-virtual {v0, v4, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 989
    const-string v4, "upnp:albumArtURI"

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 990
    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_a
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 995
    if-eqz v13, :cond_b

    :try_start_7
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_b

    .line 996
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 997
    :cond_b
    if-eqz v31, :cond_c

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_c

    .line 998
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->recycle()V

    .line 999
    :cond_c
    if-eqz v24, :cond_d

    .line 1000
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    .line 1008
    .end local v6    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .end local v7    # "type":Ljava/lang/String;
    .end local v12    # "albumartProtocol":Ljava/lang/String;
    .end local v13    # "bitmap":Landroid/graphics/Bitmap;
    .end local v15    # "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .end local v16    # "durIdx":I
    .end local v18    # "duration":J
    .end local v21    # "fileIdx":I
    .end local v22    # "filePath":Ljava/lang/String;
    .end local v24    # "fos":Ljava/io/FileOutputStream;
    .end local v25    # "protocol":Ljava/lang/String;
    .end local v26    # "resIdx":I
    .end local v28    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v29    # "resolution":Ljava/lang/String;
    .end local v30    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v31    # "scaled":Landroid/graphics/Bitmap;
    .end local v32    # "scaledHeight":I
    .end local v33    # "scaledWidth":I
    .end local v34    # "tempFile":Ljava/io/File;
    .end local v35    # "typeIdx":I
    .end local v36    # "url":Ljava/lang/String;
    .end local v37    # "videoInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    :cond_d
    :goto_3
    if-eqz v14, :cond_e

    .line 1009
    :try_start_8
    invoke-interface {v14}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_2

    .line 1018
    :cond_e
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 938
    .restart local v6    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .restart local v7    # "type":Ljava/lang/String;
    .restart local v13    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v15    # "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .restart local v16    # "durIdx":I
    .restart local v18    # "duration":J
    .restart local v21    # "fileIdx":I
    .restart local v22    # "filePath":Ljava/lang/String;
    .restart local v25    # "protocol":Ljava/lang/String;
    .restart local v26    # "resIdx":I
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v29    # "resolution":Ljava/lang/String;
    .restart local v30    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v35    # "typeIdx":I
    .restart local v37    # "videoInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    :catch_0
    move-exception v17

    .line 939
    .local v17, "e":Ljava/lang/Exception;
    :try_start_9
    const-string v4, "ContentResourceInfoUpdater"

    const-string v8, "loadVideoResource"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Got exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 941
    :try_start_a
    invoke-virtual/range {v30 .. v30}, Landroid/media/MediaMetadataRetriever;->release()V

    goto/16 :goto_1

    .line 1012
    .end local v6    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .end local v7    # "type":Ljava/lang/String;
    .end local v13    # "bitmap":Landroid/graphics/Bitmap;
    .end local v14    # "c":Landroid/database/Cursor;
    .end local v15    # "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .end local v16    # "durIdx":I
    .end local v17    # "e":Ljava/lang/Exception;
    .end local v18    # "duration":J
    .end local v21    # "fileIdx":I
    .end local v22    # "filePath":Ljava/lang/String;
    .end local v25    # "protocol":Ljava/lang/String;
    .end local v26    # "resIdx":I
    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v29    # "resolution":Ljava/lang/String;
    .end local v30    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v35    # "typeIdx":I
    .end local v37    # "videoInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    :catch_1
    move-exception v17

    .line 1013
    .local v17, "e":Landroid/database/CursorWindowAllocationException;
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 941
    .end local v17    # "e":Landroid/database/CursorWindowAllocationException;
    .restart local v6    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .restart local v7    # "type":Ljava/lang/String;
    .restart local v13    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v14    # "c":Landroid/database/Cursor;
    .restart local v15    # "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .restart local v16    # "durIdx":I
    .restart local v18    # "duration":J
    .restart local v21    # "fileIdx":I
    .restart local v22    # "filePath":Ljava/lang/String;
    .restart local v25    # "protocol":Ljava/lang/String;
    .restart local v26    # "resIdx":I
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v29    # "resolution":Ljava/lang/String;
    .restart local v30    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v35    # "typeIdx":I
    .restart local v37    # "videoInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    :catchall_0
    move-exception v4

    invoke-virtual/range {v30 .. v30}, Landroid/media/MediaMetadataRetriever;->release()V

    throw v4
    :try_end_a
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_2

    .line 1014
    .end local v6    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .end local v7    # "type":Ljava/lang/String;
    .end local v13    # "bitmap":Landroid/graphics/Bitmap;
    .end local v14    # "c":Landroid/database/Cursor;
    .end local v15    # "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .end local v16    # "durIdx":I
    .end local v18    # "duration":J
    .end local v21    # "fileIdx":I
    .end local v22    # "filePath":Ljava/lang/String;
    .end local v25    # "protocol":Ljava/lang/String;
    .end local v26    # "resIdx":I
    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v29    # "resolution":Ljava/lang/String;
    .end local v30    # "retriever":Landroid/media/MediaMetadataRetriever;
    .end local v35    # "typeIdx":I
    .end local v37    # "videoInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    :catch_2
    move-exception v17

    .line 1015
    .local v17, "e":Ljava/lang/Exception;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V

    .line 1016
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 965
    .end local v17    # "e":Ljava/lang/Exception;
    .restart local v6    # "parser":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .restart local v7    # "type":Ljava/lang/String;
    .restart local v13    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v14    # "c":Landroid/database/Cursor;
    .restart local v15    # "cds":Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;
    .restart local v16    # "durIdx":I
    .restart local v18    # "duration":J
    .restart local v21    # "fileIdx":I
    .restart local v22    # "filePath":Ljava/lang/String;
    .restart local v24    # "fos":Ljava/io/FileOutputStream;
    .restart local v25    # "protocol":Ljava/lang/String;
    .restart local v26    # "resIdx":I
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v29    # "resolution":Ljava/lang/String;
    .restart local v30    # "retriever":Landroid/media/MediaMetadataRetriever;
    .restart local v31    # "scaled":Landroid/graphics/Bitmap;
    .restart local v32    # "scaledHeight":I
    .restart local v33    # "scaledWidth":I
    .restart local v34    # "tempFile":Ljava/io/File;
    .restart local v35    # "typeIdx":I
    .restart local v37    # "videoInfo":Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;
    :catch_3
    move-exception v17

    .line 966
    .local v17, "e":Ljava/lang/IllegalStateException;
    :try_start_b
    const-string v4, "ContentResourceInfoUpdater"

    const-string v8, "loadVideoResource"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "retry to compress: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v4, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 967
    invoke-virtual/range {v17 .. v17}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_5
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 969
    :try_start_c
    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v8, 0x1

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v8}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v31

    .line 970
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v8, 0x50

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v4, v8, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_c
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto/16 :goto_2

    .line 971
    :catch_4
    move-exception v20

    .line 972
    .local v20, "e2":Ljava/lang/IllegalStateException;
    :try_start_d
    const-string v4, "ContentResourceInfoUpdater"

    const-string v8, "loadVideoResource"

    const-string v9, "retry FAIL"

    invoke-static {v4, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 973
    invoke-virtual/range {v20 .. v20}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_5
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    goto/16 :goto_2

    .line 991
    .end local v17    # "e":Ljava/lang/IllegalStateException;
    .end local v20    # "e2":Ljava/lang/IllegalStateException;
    :catch_5
    move-exception v17

    move-object/from16 v23, v24

    .line 992
    .end local v24    # "fos":Ljava/io/FileOutputStream;
    .local v17, "e":Ljava/lang/Exception;
    .restart local v23    # "fos":Ljava/io/FileOutputStream;
    :goto_4
    :try_start_e
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    .line 995
    if-eqz v13, :cond_f

    :try_start_f
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_f

    .line 996
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 997
    :cond_f
    if-eqz v31, :cond_10

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_10

    .line 998
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->recycle()V

    .line 999
    :cond_10
    if-eqz v23, :cond_d

    .line 1000
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_6
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_f .. :try_end_f} :catch_1
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_2

    goto/16 :goto_3

    .line 1001
    :catch_6
    move-exception v17

    .line 1002
    .local v17, "e":Ljava/io/IOException;
    :try_start_10
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    .line 1001
    .end local v17    # "e":Ljava/io/IOException;
    .end local v23    # "fos":Ljava/io/FileOutputStream;
    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v12    # "albumartProtocol":Ljava/lang/String;
    .restart local v24    # "fos":Ljava/io/FileOutputStream;
    .restart local v28    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v36    # "url":Ljava/lang/String;
    :catch_7
    move-exception v17

    .line 1002
    .restart local v17    # "e":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V
    :try_end_10
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_10 .. :try_end_10} :catch_1
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_2

    goto/16 :goto_3

    .line 994
    .end local v12    # "albumartProtocol":Ljava/lang/String;
    .end local v17    # "e":Ljava/io/IOException;
    .end local v24    # "fos":Ljava/io/FileOutputStream;
    .end local v28    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .end local v36    # "url":Ljava/lang/String;
    .restart local v23    # "fos":Ljava/io/FileOutputStream;
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    :catchall_1
    move-exception v4

    .line 995
    :goto_5
    if-eqz v13, :cond_11

    :try_start_11
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_11

    .line 996
    invoke-virtual {v13}, Landroid/graphics/Bitmap;->recycle()V

    .line 997
    :cond_11
    if-eqz v31, :cond_12

    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v8

    if-nez v8, :cond_12

    .line 998
    invoke-virtual/range {v31 .. v31}, Landroid/graphics/Bitmap;->recycle()V

    .line 999
    :cond_12
    if-eqz v23, :cond_13

    .line 1000
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_8
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_11 .. :try_end_11} :catch_1
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_2

    .line 1003
    :cond_13
    :goto_6
    :try_start_12
    throw v4

    .line 1001
    :catch_8
    move-exception v17

    .line 1002
    .restart local v17    # "e":Ljava/io/IOException;
    invoke-virtual/range {v17 .. v17}, Ljava/io/IOException;->printStackTrace()V
    :try_end_12
    .catch Landroid/database/CursorWindowAllocationException; {:try_start_12 .. :try_end_12} :catch_1
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_2

    goto :goto_6

    .line 994
    .end local v17    # "e":Ljava/io/IOException;
    .end local v23    # "fos":Ljava/io/FileOutputStream;
    .restart local v24    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v4

    move-object/from16 v23, v24

    .end local v24    # "fos":Ljava/io/FileOutputStream;
    .restart local v23    # "fos":Ljava/io/FileOutputStream;
    goto :goto_5

    .end local v23    # "fos":Ljava/io/FileOutputStream;
    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v12    # "albumartProtocol":Ljava/lang/String;
    .restart local v24    # "fos":Ljava/io/FileOutputStream;
    .restart local v28    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v36    # "url":Ljava/lang/String;
    :catchall_3
    move-exception v4

    move-object/from16 v23, v24

    .end local v24    # "fos":Ljava/io/FileOutputStream;
    .restart local v23    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v27, v28

    .end local v28    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    goto :goto_5

    .line 991
    .end local v12    # "albumartProtocol":Ljava/lang/String;
    .end local v36    # "url":Ljava/lang/String;
    :catch_9
    move-exception v17

    goto :goto_4

    .end local v23    # "fos":Ljava/io/FileOutputStream;
    .end local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v12    # "albumartProtocol":Ljava/lang/String;
    .restart local v24    # "fos":Ljava/io/FileOutputStream;
    .restart local v28    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v36    # "url":Ljava/lang/String;
    :catch_a
    move-exception v17

    move-object/from16 v23, v24

    .end local v24    # "fos":Ljava/io/FileOutputStream;
    .restart local v23    # "fos":Ljava/io/FileOutputStream;
    move-object/from16 v27, v28

    .end local v28    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v27    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    goto :goto_4
.end method

.method private parseWMAFileInfo(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;)Z
    .locals 31
    .param p1, "wmaFilePath"    # Ljava/lang/String;
    .param p2, "wmaMaxBitRate"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;

    .prologue
    .line 380
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v26

    const-string v27, "wma"

    invoke-virtual/range {v26 .. v27}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v26

    if-nez v26, :cond_2

    .line 382
    :cond_0
    const/16 v26, 0x0

    .line 499
    :cond_1
    :goto_0
    return v26

    .line 385
    :cond_2
    const/16 v22, 0x0

    .line 386
    .local v22, "wmaFileHandle":Ljava/io/FileInputStream;
    const/16 v18, 0x0

    .line 387
    .local v18, "isLeftToRight":Z
    const-wide/16 v14, 0x300

    .line 388
    .local v14, "headerSize":J
    const/16 v26, 0x8

    move/from16 v0, v26

    new-array v8, v0, [B

    .line 393
    .local v8, "filePropertyMark":[B
    const/16 v26, 0x8

    move/from16 v0, v26

    new-array v9, v0, [B

    .line 394
    .local v9, "filePropertyMarkL2R":[B
    const/16 v26, 0x0

    const/16 v27, -0x74

    aput-byte v27, v9, v26

    .line 395
    const/16 v26, 0x1

    const/16 v27, -0x55

    aput-byte v27, v9, v26

    .line 396
    const/16 v26, 0x2

    const/16 v27, -0x24

    aput-byte v27, v9, v26

    .line 397
    const/16 v26, 0x3

    const/16 v27, -0x5f

    aput-byte v27, v9, v26

    .line 398
    const/16 v26, 0x4

    const/16 v27, -0x57

    aput-byte v27, v9, v26

    .line 399
    const/16 v26, 0x5

    const/16 v27, 0x47

    aput-byte v27, v9, v26

    .line 400
    const/16 v26, 0x6

    const/16 v27, 0x11

    aput-byte v27, v9, v26

    .line 401
    const/16 v26, 0x7

    const/16 v27, -0x31

    aput-byte v27, v9, v26

    .line 403
    const/16 v26, 0x8

    move/from16 v0, v26

    new-array v10, v0, [B

    .line 404
    .local v10, "filePropertyMarkR2L":[B
    const/16 v26, 0x0

    const/16 v27, -0x5f

    aput-byte v27, v10, v26

    .line 405
    const/16 v26, 0x1

    const/16 v27, -0x24

    aput-byte v27, v10, v26

    .line 406
    const/16 v26, 0x2

    const/16 v27, -0x55

    aput-byte v27, v10, v26

    .line 407
    const/16 v26, 0x3

    const/16 v27, -0x74

    aput-byte v27, v10, v26

    .line 408
    const/16 v26, 0x4

    const/16 v27, 0x47

    aput-byte v27, v10, v26

    .line 409
    const/16 v26, 0x5

    const/16 v27, -0x57

    aput-byte v27, v10, v26

    .line 410
    const/16 v26, 0x6

    const/16 v27, -0x31

    aput-byte v27, v10, v26

    .line 411
    const/16 v26, 0x7

    const/16 v27, 0x11

    aput-byte v27, v10, v26

    .line 412
    const/16 v21, 0x100

    .line 413
    .local v21, "sectionSize":I
    const/16 v26, 0x8

    move/from16 v0, v26

    new-array v0, v0, [B

    move-object/from16 v20, v0

    .line 417
    .local v20, "sectionCache":[B
    :try_start_0
    new-instance v23, Ljava/io/FileInputStream;

    new-instance v26, Ljava/io/File;

    move-object/from16 v0, v26

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    .end local v22    # "wmaFileHandle":Ljava/io/FileInputStream;
    .local v23, "wmaFileHandle":Ljava/io/FileInputStream;
    const/16 v26, 0x108

    :try_start_1
    move/from16 v0, v26

    new-array v0, v0, [B

    move-object/from16 v19, v0

    .line 419
    .local v19, "sectionBytes":[B
    const/16 v26, 0x0

    const/16 v27, 0x17

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    move/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/FileInputStream;->read([BII)I

    .line 420
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->getLeftToRight([B)Z

    move-result v18

    .line 422
    invoke-static/range {v19 .. v19}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 423
    .local v5, "buf":Ljava/nio/ByteBuffer;
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    .line 424
    .local v11, "guid1":Ljava/lang/Long;
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .line 426
    .local v12, "guid2":Ljava/lang/Long;
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v13

    .line 427
    .local v13, "hex1":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/Long;->longValue()J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v16

    .line 429
    .local v16, "hex2":Ljava/lang/String;
    const-string v26, "ContentResourceInfoUpdater"

    const-string v27, "parseWMAFileInfo"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "UID = "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string v26, "3026B2758E66CF11"

    move-object/from16 v0, v26

    invoke-virtual {v13, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v26

    if-nez v26, :cond_3

    const-string v26, "A6D900AA0062CE6C"

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v26

    if-eqz v26, :cond_4

    .line 432
    :cond_3
    const-string v26, "ContentResourceInfoUpdater"

    const-string v27, "parseWMAFileInfo"

    const-string v28, "Not a WMA file"

    invoke-static/range {v26 .. v28}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 433
    const/16 v26, 0x0

    .line 490
    if-eqz v23, :cond_1

    .line 492
    :try_start_2
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 493
    :catch_0
    move-exception v6

    .line 494
    .local v6, "e":Ljava/io/IOException;
    const-string v27, "ContentResourceInfoUpdater"

    const-string v28, "parseWMAFileInfo"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "wmaFileHandle close fail, "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 435
    .end local v6    # "e":Ljava/io/IOException;
    :cond_4
    if-nez v18, :cond_6

    .line 436
    :try_start_3
    sget-object v26, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 437
    move-object v8, v10

    .line 442
    :goto_1
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v14

    .line 443
    const-string v26, "ContentResourceInfoUpdater"

    const-string v27, "parseWMAFileInfo"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "the header size of "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    const-string v29, " is: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    const/4 v7, -0x1

    .line 447
    .local v7, "filePropertyFrameIndex":I
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_2
    mul-int v26, v17, v21

    move/from16 v0, v26

    int-to-long v0, v0

    move-wide/from16 v26, v0

    cmp-long v26, v26, v14

    if-gez v26, :cond_5

    .line 448
    const/16 v26, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    move/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->setCacheByte([B[BI)V

    .line 449
    const/16 v26, 0x8

    move-object/from16 v0, v23

    move-object/from16 v1, v19

    move/from16 v2, v26

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/FileInputStream;->read([BII)I

    .line 450
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v8}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->findSubBytes([B[B)I

    move-result v7

    .line 451
    if-ltz v7, :cond_7

    .line 452
    const-string v26, "ContentResourceInfoUpdater"

    const-string v27, "parseWMAFileInfo"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "the index of file property frame: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 453
    const/16 v26, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    move/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->setCacheByte([B[BI)V

    .line 460
    :cond_5
    const/16 v26, -0x1

    move/from16 v0, v26

    if-ne v0, v7, :cond_8

    .line 461
    const-string v26, "ContentResourceInfoUpdater"

    const-string v27, "parseWMAFileInfo"

    const-string v28, "can NOT file file property frame in wma file"

    invoke-static/range {v26 .. v28}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 462
    const/16 v26, 0x0

    .line 490
    if-eqz v23, :cond_1

    .line 492
    :try_start_4
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0

    .line 493
    :catch_1
    move-exception v6

    .line 494
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v27, "ContentResourceInfoUpdater"

    const-string v28, "parseWMAFileInfo"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "wmaFileHandle close fail, "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 439
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "filePropertyFrameIndex":I
    .end local v17    # "i":I
    :cond_6
    :try_start_5
    sget-object v26, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    move-object/from16 v0, v26

    invoke-virtual {v5, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 440
    move-object v8, v9

    goto/16 :goto_1

    .line 457
    .restart local v7    # "filePropertyFrameIndex":I
    .restart local v17    # "i":I
    :cond_7
    const/16 v26, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move-object/from16 v2, v19

    move/from16 v3, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->setCacheByte([B[BI)V

    .line 447
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_2

    .line 465
    :cond_8
    const/16 v26, 0x4

    move/from16 v0, v26

    new-array v4, v0, [B

    .line 466
    .local v4, "bitRateBytes":[B
    add-int/lit8 v26, v7, 0x63

    const/16 v27, 0x103

    move/from16 v0, v26

    move/from16 v1, v27

    if-gt v0, v1, :cond_a

    .line 467
    const/16 v26, 0x0

    add-int/lit8 v27, v7, 0x64

    aget-byte v27, v19, v27

    aput-byte v27, v4, v26

    .line 468
    const/16 v26, 0x1

    add-int/lit8 v27, v7, 0x65

    aget-byte v27, v19, v27

    aput-byte v27, v4, v26

    .line 469
    const/16 v26, 0x2

    add-int/lit8 v27, v7, 0x66

    aget-byte v27, v19, v27

    aput-byte v27, v4, v26

    .line 470
    const/16 v26, 0x3

    add-int/lit8 v27, v7, 0x67

    aget-byte v27, v19, v27

    aput-byte v27, v4, v26

    .line 480
    :goto_3
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v0, v1, v4}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->trans4BytesToInt(Z[B)J

    move-result-wide v26

    const-wide/16 v28, 0x3e8

    div-long v24, v26, v28

    .line 481
    .local v24, "wmaMaxBitRateMid":J
    move-wide/from16 v0, v24

    long-to-int v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p2

    iput v0, v1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;->audioMaxBitRate:I

    .line 482
    const-string v26, "ContentResourceInfoUpdater"

    const-string v27, "parseWMAFileInfo"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "wmaMaxBitRate.audioMaxBitRate = "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p2

    iget v0, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;->audioMaxBitRate:I

    move/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_8
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 490
    if-eqz v23, :cond_9

    .line 492
    :try_start_6
    invoke-virtual/range {v23 .. v23}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 499
    :cond_9
    :goto_4
    const/16 v26, 0x1

    goto/16 :goto_0

    .line 472
    .end local v24    # "wmaMaxBitRateMid":J
    :cond_a
    const/16 v26, 0x8

    :try_start_7
    move-object/from16 v0, v23

    move-object/from16 v1, v19

    move/from16 v2, v26

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/FileInputStream;->read([BII)I

    .line 473
    const/16 v26, 0x0

    add-int/lit8 v27, v7, 0x6c

    sub-int v27, v27, v21

    add-int/lit8 v27, v27, -0x8

    add-int/lit8 v27, v27, 0x0

    aget-byte v27, v19, v27

    aput-byte v27, v4, v26

    .line 474
    const/16 v26, 0x1

    add-int/lit8 v27, v7, 0x6c

    sub-int v27, v27, v21

    add-int/lit8 v27, v27, -0x8

    add-int/lit8 v27, v27, 0x1

    aget-byte v27, v19, v27

    aput-byte v27, v4, v26

    .line 475
    const/16 v26, 0x2

    add-int/lit8 v27, v7, 0x6c

    sub-int v27, v27, v21

    add-int/lit8 v27, v27, -0x8

    add-int/lit8 v27, v27, 0x2

    aget-byte v27, v19, v27

    aput-byte v27, v4, v26

    .line 476
    const/16 v26, 0x3

    add-int/lit8 v27, v7, 0x6c

    sub-int v27, v27, v21

    add-int/lit8 v27, v27, -0x8

    add-int/lit8 v27, v27, 0x3

    aget-byte v27, v19, v27

    aput-byte v27, v4, v26
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_8
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_3

    .line 483
    .end local v4    # "bitRateBytes":[B
    .end local v5    # "buf":Ljava/nio/ByteBuffer;
    .end local v7    # "filePropertyFrameIndex":I
    .end local v11    # "guid1":Ljava/lang/Long;
    .end local v12    # "guid2":Ljava/lang/Long;
    .end local v13    # "hex1":Ljava/lang/String;
    .end local v16    # "hex2":Ljava/lang/String;
    .end local v17    # "i":I
    .end local v19    # "sectionBytes":[B
    :catch_2
    move-exception v6

    move-object/from16 v22, v23

    .line 484
    .end local v23    # "wmaFileHandle":Ljava/io/FileInputStream;
    .local v6, "e":Ljava/io/FileNotFoundException;
    .restart local v22    # "wmaFileHandle":Ljava/io/FileInputStream;
    :goto_5
    :try_start_8
    const-string v26, "ContentResourceInfoUpdater"

    const-string v27, "parseWMAFileInfo"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "new DataInputStream fail! Exception: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 485
    const/16 v26, 0x0

    .line 490
    if-eqz v22, :cond_1

    .line 492
    :try_start_9
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    goto/16 :goto_0

    .line 493
    :catch_3
    move-exception v6

    .line 494
    .local v6, "e":Ljava/io/IOException;
    const-string v27, "ContentResourceInfoUpdater"

    const-string v28, "parseWMAFileInfo"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "wmaFileHandle close fail, "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 493
    .end local v6    # "e":Ljava/io/IOException;
    .end local v22    # "wmaFileHandle":Ljava/io/FileInputStream;
    .restart local v4    # "bitRateBytes":[B
    .restart local v5    # "buf":Ljava/nio/ByteBuffer;
    .restart local v7    # "filePropertyFrameIndex":I
    .restart local v11    # "guid1":Ljava/lang/Long;
    .restart local v12    # "guid2":Ljava/lang/Long;
    .restart local v13    # "hex1":Ljava/lang/String;
    .restart local v16    # "hex2":Ljava/lang/String;
    .restart local v17    # "i":I
    .restart local v19    # "sectionBytes":[B
    .restart local v23    # "wmaFileHandle":Ljava/io/FileInputStream;
    .restart local v24    # "wmaMaxBitRateMid":J
    :catch_4
    move-exception v6

    .line 494
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v26, "ContentResourceInfoUpdater"

    const-string v27, "parseWMAFileInfo"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "wmaFileHandle close fail, "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 486
    .end local v4    # "bitRateBytes":[B
    .end local v5    # "buf":Ljava/nio/ByteBuffer;
    .end local v6    # "e":Ljava/io/IOException;
    .end local v7    # "filePropertyFrameIndex":I
    .end local v11    # "guid1":Ljava/lang/Long;
    .end local v12    # "guid2":Ljava/lang/Long;
    .end local v13    # "hex1":Ljava/lang/String;
    .end local v16    # "hex2":Ljava/lang/String;
    .end local v17    # "i":I
    .end local v19    # "sectionBytes":[B
    .end local v23    # "wmaFileHandle":Ljava/io/FileInputStream;
    .end local v24    # "wmaMaxBitRateMid":J
    .restart local v22    # "wmaFileHandle":Ljava/io/FileInputStream;
    :catch_5
    move-exception v6

    .line 487
    .local v6, "e":Ljava/lang/Exception;
    :goto_6
    :try_start_a
    const-string v26, "ContentResourceInfoUpdater"

    const-string v27, "parseWMAFileInfo"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "parse WMA File fail! Exception: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v26 .. v28}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 488
    const/16 v26, 0x0

    .line 490
    if-eqz v22, :cond_1

    .line 492
    :try_start_b
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_6

    goto/16 :goto_0

    .line 493
    :catch_6
    move-exception v6

    .line 494
    .local v6, "e":Ljava/io/IOException;
    const-string v27, "ContentResourceInfoUpdater"

    const-string v28, "parseWMAFileInfo"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "wmaFileHandle close fail, "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 490
    .end local v6    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v26

    :goto_7
    if-eqz v22, :cond_b

    .line 492
    :try_start_c
    invoke-virtual/range {v22 .. v22}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_7

    .line 495
    :cond_b
    :goto_8
    throw v26

    .line 493
    :catch_7
    move-exception v6

    .line 494
    .restart local v6    # "e":Ljava/io/IOException;
    const-string v27, "ContentResourceInfoUpdater"

    const-string v28, "parseWMAFileInfo"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "wmaFileHandle close fail, "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual {v6}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v27 .. v29}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8

    .line 490
    .end local v6    # "e":Ljava/io/IOException;
    .end local v22    # "wmaFileHandle":Ljava/io/FileInputStream;
    .restart local v23    # "wmaFileHandle":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v26

    move-object/from16 v22, v23

    .end local v23    # "wmaFileHandle":Ljava/io/FileInputStream;
    .restart local v22    # "wmaFileHandle":Ljava/io/FileInputStream;
    goto :goto_7

    .line 486
    .end local v22    # "wmaFileHandle":Ljava/io/FileInputStream;
    .restart local v23    # "wmaFileHandle":Ljava/io/FileInputStream;
    :catch_8
    move-exception v6

    move-object/from16 v22, v23

    .end local v23    # "wmaFileHandle":Ljava/io/FileInputStream;
    .restart local v22    # "wmaFileHandle":Ljava/io/FileInputStream;
    goto :goto_6

    .line 483
    :catch_9
    move-exception v6

    goto/16 :goto_5
.end method

.method private parseWMVFileInfo(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;)Z
    .locals 9
    .param p1, "wavFilePath"    # Ljava/lang/String;
    .param p2, "videoInfo"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;

    .prologue
    const/4 v5, 0x0

    .line 503
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    const-string v7, "wmv"

    invoke-virtual {v6, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 545
    :cond_0
    :goto_0
    return v5

    .line 508
    :cond_1
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 510
    .local v1, "mediaRetriever":Landroid/media/MediaMetadataRetriever;
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 519
    const/16 v6, 0x14

    invoke-virtual {v1, v6}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    .line 521
    .local v2, "wmvBitRateStr":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 522
    :cond_2
    const-string v6, "ContentResourceInfoUpdater"

    const-string v7, "parseWMVFileInfo"

    const-string v8, "wmvBitRateStr is null or empty!"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 512
    .end local v2    # "wmvBitRateStr":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 513
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "ContentResourceInfoUpdater"

    const-string v7, "parseWMVFileInfo"

    const-string v8, "Exception at setDataSource"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 514
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 525
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v2    # "wmvBitRateStr":Ljava/lang/String;
    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    div-int/lit16 v6, v6, 0x3e8

    iput v6, p2, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;->audioAvgBitRate:I

    .line 528
    const/16 v6, 0x12

    invoke-virtual {v1, v6}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v4

    .line 530
    .local v4, "wmvWidthStr":Ljava/lang/String;
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 531
    :cond_4
    const-string v6, "ContentResourceInfoUpdater"

    const-string v7, "parseWMVFileInfo"

    const-string v8, "wmvWidthStr is null or empty!"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 534
    :cond_5
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p2, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;->videoWidth:I

    .line 537
    const/16 v6, 0x13

    invoke-virtual {v1, v6}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    .line 539
    .local v3, "wmvHeightStr":Ljava/lang/String;
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 540
    :cond_6
    const-string v6, "ContentResourceInfoUpdater"

    const-string v7, "parseWMVFileInfo"

    const-string v8, "wmvHeightStr is null or empty!"

    invoke-static {v6, v7, v8}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 543
    :cond_7
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, p2, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater$AVQualityInfo;->videoHeight:I

    .line 545
    const/4 v5, 0x1

    goto :goto_0
.end method

.method private resizeBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 8
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "nWidthThreshold"    # I
    .param p3, "nHeightThreshold"    # I

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    .line 1062
    if-nez p1, :cond_1

    .line 1063
    const/4 p1, 0x0

    .line 1091
    .end local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p1

    .line 1065
    .restart local p1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 1066
    .local v3, "nWidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 1069
    .local v0, "nHeight":I
    if-gt v3, p2, :cond_2

    if-le v0, p3, :cond_0

    .line 1072
    :cond_2
    move v2, v3

    .line 1073
    .local v2, "nScaledWidth":I
    move v1, v0

    .line 1076
    .local v1, "nScaledHeight":I
    if-lt v3, v0, :cond_4

    .line 1077
    if-lt v3, p2, :cond_3

    .line 1078
    move v2, p2

    .line 1080
    :cond_3
    mul-int v5, v0, v2

    int-to-float v5, v5

    int-to-float v6, v3

    div-float/2addr v5, v6

    add-float/2addr v5, v7

    float-to-int v1, v5

    .line 1090
    :goto_1
    const/4 v5, 0x1

    invoke-static {p1, v2, v1, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .local v4, "scaled":Landroid/graphics/Bitmap;
    move-object p1, v4

    .line 1091
    goto :goto_0

    .line 1084
    .end local v4    # "scaled":Landroid/graphics/Bitmap;
    :cond_4
    if-lt v0, p3, :cond_5

    .line 1085
    move v1, p3

    .line 1087
    :cond_5
    mul-int v5, v3, v1

    int-to-float v5, v5

    int-to-float v6, v0

    div-float/2addr v5, v6

    add-float/2addr v5, v7

    float-to-int v2, v5

    goto :goto_1
.end method

.method private rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "origin"    # Landroid/graphics/Bitmap;
    .param p2, "orientation"    # I

    .prologue
    const/4 v1, 0x0

    .line 814
    if-nez p1, :cond_1

    .line 815
    const/4 p1, 0x0

    .line 821
    .end local p1    # "origin":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-object p1

    .line 816
    .restart local p1    # "origin":Landroid/graphics/Bitmap;
    :cond_1
    if-eqz p2, :cond_0

    .line 819
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 820
    .local v5, "mat":Landroid/graphics/Matrix;
    int-to-float v0, p2

    invoke-virtual {v5, v0}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 821
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0
.end method

.method private setCacheByte([B[BI)V
    .locals 3
    .param p1, "cacheBytes"    # [B
    .param p2, "sourceBytes"    # [B
    .param p3, "flag"    # I

    .prologue
    const/16 v2, 0x8

    .line 368
    if-nez p3, :cond_0

    .line 369
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 370
    aget-byte v1, p1, v0

    aput-byte v1, p2, v0

    .line 369
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 373
    .end local v0    # "i":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 374
    array-length v1, p2

    add-int/lit8 v1, v1, -0x8

    add-int/2addr v1, v0

    aget-byte v1, p2, v1

    aput-byte v1, p1, v0

    .line 373
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 377
    :cond_1
    return-void
.end method

.method private toDateString(J)Ljava/lang/String;
    .locals 3
    .param p1, "value"    # J

    .prologue
    .line 1152
    const-wide/16 v0, 0xa

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 1153
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1154
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private trans1ByteToInt(B)J
    .locals 4
    .param p1, "byteValue"    # B

    .prologue
    .line 302
    int-to-long v0, p1

    .line 303
    .local v0, "temp":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 304
    const-wide/16 v2, 0x100

    add-long/2addr v0, v2

    .line 306
    .end local v0    # "temp":J
    :cond_0
    return-wide v0
.end method

.method private trans4BytesToInt(Z[B)J
    .locals 10
    .param p1, "isLeft2Right"    # Z
    .param p2, "dataInBytes"    # [B

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-wide/16 v4, 0x100

    .line 310
    if-eqz p2, :cond_0

    array-length v2, p2

    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    .line 311
    :cond_0
    const-wide/16 v0, 0x0

    .line 327
    :goto_0
    return-wide v0

    .line 314
    :cond_1
    const-wide/16 v0, 0x0

    .line 315
    .local v0, "result":J
    if-eqz p1, :cond_2

    .line 316
    aget-byte v2, p2, v6

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->trans1ByteToInt(B)J

    move-result-wide v2

    mul-long/2addr v2, v4

    mul-long/2addr v2, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 317
    aget-byte v2, p2, v7

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->trans1ByteToInt(B)J

    move-result-wide v2

    mul-long/2addr v2, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 318
    aget-byte v2, p2, v8

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->trans1ByteToInt(B)J

    move-result-wide v2

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 319
    aget-byte v2, p2, v9

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->trans1ByteToInt(B)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0

    .line 321
    :cond_2
    aget-byte v2, p2, v9

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->trans1ByteToInt(B)J

    move-result-wide v2

    mul-long/2addr v2, v4

    mul-long/2addr v2, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 322
    aget-byte v2, p2, v8

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->trans1ByteToInt(B)J

    move-result-wide v2

    mul-long/2addr v2, v4

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 323
    aget-byte v2, p2, v7

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->trans1ByteToInt(B)J

    move-result-wide v2

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 324
    aget-byte v2, p2, v6

    invoke-direct {p0, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->trans1ByteToInt(B)J

    move-result-wide v2

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method private updateItemThumbnailResource(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;Landroid/database/Cursor;I)V
    .locals 23
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    .param p2, "parser"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    .param p3, "thumbCur"    # Landroid/database/Cursor;
    .param p4, "orientation"    # I

    .prologue
    .line 716
    const-string v18, "width"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 717
    .local v14, "tWidthIdx":I
    const-string v18, "height"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 718
    .local v13, "tHeightIdx":I
    const-string v18, "_data"

    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 720
    .local v12, "tDataIdx":I
    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 721
    .local v17, "w":I
    move-object/from16 v0, p3

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 722
    .local v7, "h":I
    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 723
    .local v15, "thumbPath":Ljava/lang/String;
    const-string v18, "image/jpeg"

    const/16 v19, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->buildImageProtocol(Ljava/lang/String;III)Ljava/lang/String;

    move-result-object v9

    .line 725
    .local v9, "protocol":Ljava/lang/String;
    if-eqz v15, :cond_0

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v18

    if-nez v18, :cond_1

    .line 726
    :cond_0
    const-string v18, "ContentResourceInfoUpdater"

    const-string v19, "updateItemThumbnailResource"

    const-string v20, "updateItemThumbnailResource is null or empty"

    invoke-static/range {v18 .. v20}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    :goto_0
    return-void

    .line 731
    :cond_1
    if-lez p4, :cond_4

    .line 735
    :try_start_0
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-direct {v0, v15, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->getRotateThumbnailPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v8

    .line 737
    .local v8, "newPath":Ljava/lang/String;
    if-eqz v8, :cond_4

    .line 738
    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v0, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v18}, Ljava/io/File;->isFile()Z

    move-result v18

    if-nez v18, :cond_5

    .line 741
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->getUri()Landroid/net/Uri;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v20

    const/16 v19, 0x1

    const/16 v22, 0x0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    move/from16 v3, v19

    move-object/from16 v4, v22

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 745
    .local v16, "thumbnail":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->rotateBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 747
    .local v11, "rotatedThumb":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v11}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->createThumbnailFile(Ljava/lang/String;Landroid/graphics/Bitmap;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 748
    move-object v15, v8

    .line 750
    :cond_2
    if-eqz v16, :cond_3

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v18

    if-nez v18, :cond_3

    .line 751
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->recycle()V

    .line 752
    :cond_3
    if-eqz v11, :cond_4

    invoke-virtual {v11}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v18

    if-nez v18, :cond_4

    .line 753
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 764
    .end local v8    # "newPath":Ljava/lang/String;
    .end local v11    # "rotatedThumb":Landroid/graphics/Bitmap;
    .end local v16    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_4
    :goto_1
    new-instance v10, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/SmallImageProperty;

    new-instance v18, Ljava/io/File;

    move-object/from16 v0, v18

    invoke-direct {v0, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-direct {v10, v0, v9, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/SmallImageProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/lang/String;Ljava/io/File;)V

    .line 765
    .local v10, "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    const-string v18, "resolution"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v17 .. v17}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "x"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 767
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V

    goto/16 :goto_0

    .line 755
    .end local v10    # "resProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;
    .restart local v8    # "newPath":Ljava/lang/String;
    :cond_5
    move-object v15, v8

    goto :goto_1

    .line 758
    .end local v8    # "newPath":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 759
    .local v6, "e":Ljava/lang/Exception;
    const-string v18, "ContentResourceInfoUpdater"

    const-string v19, "updateItemThumbnailResource"

    const-string v20, "Exception on rotate image: "

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 760
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private updateResourceInfo(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)Z
    .locals 5
    .param p1, "node"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .prologue
    const/4 v1, 0x0

    .line 100
    if-nez p1, :cond_1

    .line 101
    const-string v2, "ContentResourceInfoUpdater"

    const-string v3, "updateResourceInfo"

    const-string v4, "updateResourceInfo server is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    .end local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_0
    :goto_0
    return v1

    .line 104
    .restart local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;->getUPnPClass()Ljava/lang/String;

    move-result-object v0

    .line 105
    .local v0, "upnpClass":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 106
    const-string v2, "ContentResourceInfoUpdater"

    const-string v3, "updateResourceInfo"

    const-string v4, "server is null"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 109
    :cond_2
    const-string v2, "object.item.audioItem"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 110
    check-cast p1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    .end local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->loadAudioResource(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;)Z

    move-result v1

    goto :goto_0

    .line 111
    .restart local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_3
    const-string v2, "object.item.imageItem"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 112
    check-cast p1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    .end local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->loadImageResource(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;)Z

    move-result v1

    goto :goto_0

    .line 113
    .restart local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    :cond_4
    const-string v2, "object.item.videoItem"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    check-cast p1, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;

    .end local p1    # "node":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->loadVideoResource(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;)Z

    move-result v1

    goto :goto_0
.end method

.method private updateSubtitleProperty(Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;Ljava/lang/String;)V
    .locals 12
    .param p1, "item"    # Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1022
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1023
    .local v7, "subtitleFormatList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v10, "txt"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1024
    const-string v10, "ttxt"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1025
    const-string v10, "idx"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1026
    const-string v10, "sub"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1027
    const-string v10, "srt"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1028
    const-string v10, "smi"

    invoke-virtual {v7, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1030
    invoke-static {p2}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->unEscapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v7}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->getSubTitleFileList(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v8

    .line 1032
    .local v8, "subtitleList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    if-eqz v8, :cond_2

    .line 1033
    const-string v0, ""

    .line 1034
    .local v0, "extension":Ljava/lang/String;
    const-string v9, ""

    .line 1036
    .local v9, "subtitleURL":Ljava/lang/String;
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/io/File;

    .line 1037
    .local v6, "subtitle":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 1038
    .local v4, "subFilePath":Ljava/lang/String;
    if-eqz v4, :cond_0

    .line 1040
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v2

    .line 1041
    .local v2, "length":I
    add-int/lit8 v10, v2, -0x3

    invoke-virtual {v4, v10, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 1043
    new-instance v5, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;

    invoke-direct {v5, p1, v6, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;-><init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;Ljava/io/File;Ljava/lang/String;)V

    .line 1044
    .local v5, "subProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;
    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addResource(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProperty;)V

    .line 1046
    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;->getValue()Ljava/lang/String;

    move-result-object v9

    .line 1047
    if-eqz v9, :cond_0

    .line 1049
    new-instance v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    const-string v10, "sec:CaptionInfoEx"

    invoke-direct {v3, v10, v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1050
    .local v3, "property":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    const-string v10, "sec:type"

    sget-object v11, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v0, v11}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v10, v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 1051
    invoke-virtual {p1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    goto :goto_0

    .line 1054
    .end local v2    # "length":I
    .end local v3    # "property":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v4    # "subFilePath":Ljava/lang/String;
    .end local v5    # "subProp":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/SubtitleProperty;
    .end local v6    # "subtitle":Ljava/io/File;
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_2

    .line 1055
    new-instance v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    const-string v10, "sec:CaptionInfo"

    invoke-direct {v3, v10, v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    .restart local v3    # "property":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    invoke-virtual {p1, v3}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreItemObject;->addProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    .line 1059
    .end local v0    # "extension":Ljava/lang/String;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "property":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v9    # "subtitleURL":Ljava/lang/String;
    :cond_2
    return-void
.end method


# virtual methods
.method public update(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)Z
    .locals 1
    .param p1, "content"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/ContentResourceInfoUpdater;->updateResourceInfo(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ObjectProperty;)Z

    move-result v0

    return v0
.end method
