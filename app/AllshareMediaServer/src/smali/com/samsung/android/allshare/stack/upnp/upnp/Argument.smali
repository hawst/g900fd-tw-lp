.class public Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
.super Ljava/lang/Object;
.source "Argument.java"


# static fields
.field private static final DIRECTION:Ljava/lang/String; = "direction"

.field public static final ELEM_NAME:Ljava/lang/String; = "argument"

.field public static final IN:Ljava/lang/String; = "in"

.field private static final NAME:Ljava/lang/String; = "name"

.field public static final OUT:Ljava/lang/String; = "out"

.field private static final RELATED_STATE_VARIABLE:Ljava/lang/String; = "relatedStateVariable"

.field private static final TAG:Ljava/lang/String; = "Argument"


# instance fields
.field private mArgumentNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

.field private mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    invoke-direct {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->mArgumentNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 101
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->mArgumentNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setName(Ljava/lang/String;)V

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 103
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V
    .locals 7
    .param p1, "argNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .param p2, "servNode"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;-><init>()V

    .line 113
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .line 114
    const-string v6, "name"

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, "name":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v2

    .line 117
    .local v2, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;

    .line 119
    .local v4, "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;
    if-nez v4, :cond_0

    .line 120
    new-instance v4, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;

    .end local v4    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;
    invoke-direct {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;-><init>()V

    .line 121
    .restart local v4    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;
    invoke-virtual {v2, v4}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 122
    invoke-virtual {v4, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;->setNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 124
    :cond_0
    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;->getValue()Ljava/lang/String;

    move-result-object v5

    .line 125
    .local v5, "value":Ljava/lang/String;
    const-string v6, "direction"

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    .local v0, "direction":Ljava/lang/String;
    const-string v6, "relatedStateVariable"

    invoke-virtual {p1, v6}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 128
    .local v3, "related":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setName(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setDirection(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setRelatedStateVariableName(Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 106
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;-><init>()V

    .line 107
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setName(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method private getArgumentData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;
    .locals 2

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    .line 209
    .local v0, "node":Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getUserData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;

    .line 210
    .local v1, "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;
    if-nez v1, :cond_0

    .line 211
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;

    .end local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;
    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;-><init>()V

    .line 212
    .restart local v1    # "userData":Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setUserData(Ljava/lang/Object;)V

    .line 213
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;->setNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    .line 215
    :cond_0
    return-object v1
.end method

.method private getArgumentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->mArgumentNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method private getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;-><init>(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)V

    return-object v0
.end method

.method private getServiceNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->mServiceNode:Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    return-object v0
.end method

.method private isAllowedValue(Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;Ljava/lang/String;)Z
    .locals 13
    .param p1, "range"    # Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v10, 0x0

    .line 296
    if-nez p1, :cond_1

    .line 311
    :cond_0
    :goto_0
    return v7

    .line 299
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;->getMaximum()Ljava/lang/String;

    move-result-object v1

    .line 300
    .local v1, "str1":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;->getMinimum()Ljava/lang/String;

    move-result-object v6

    .line 303
    .local v6, "str2":Ljava/lang/String;
    :try_start_0
    invoke-static {p2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 304
    .local v8, "val":D
    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 305
    .local v4, "min":D
    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 307
    .local v2, "max":D
    cmpl-double v11, v8, v4

    if-ltz v11, :cond_2

    cmpg-double v11, v8, v2

    if-lez v11, :cond_0

    :cond_2
    move v7, v10

    goto :goto_0

    .line 309
    .end local v2    # "max":D
    .end local v4    # "min":D
    .end local v8    # "val":D
    :catch_0
    move-exception v0

    .line 310
    .local v0, "e":Ljava/lang/NumberFormatException;
    const-string v7, "Argument"

    const-string v11, "isAllowedValue"

    const-string v12, "isAllowedValue : NumberFormatException"

    invoke-static {v7, v11, v12, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move v7, v10

    .line 311
    goto :goto_0
.end method

.method private isAllowsedValue(Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;Ljava/lang/String;)Z
    .locals 3
    .param p1, "list"    # Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 283
    if-nez p1, :cond_1

    .line 292
    :cond_0
    :goto_0
    return v1

    .line 286
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 287
    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;->getAllowedValue(I)Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;->getAllowedValue(I)Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;->getValue()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;->getAllowedValue(I)Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValue;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eq v2, v1, :cond_0

    .line 286
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 292
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isArgumentNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Z
    .locals 2
    .param p0, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 139
    const-string v0, "argument"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getDirection()Ljava/lang/String;
    .locals 2

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "direction"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIntegerValue()I
    .locals 5

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v1

    .line 237
    .local v1, "value":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 241
    :goto_0
    return v2

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Argument"

    const-string v3, "getIntegerValue"

    const-string v4, "getIntegerValue - Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 241
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRelatedStateVariable()Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    .locals 3

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getService()Lcom/samsung/android/allshare/stack/upnp/upnp/Service;

    move-result-object v1

    .line 197
    .local v1, "service":Lcom/samsung/android/allshare/stack/upnp/upnp/Service;
    if-nez v1, :cond_0

    .line 198
    const/4 v2, 0x0

    .line 200
    :goto_0
    return-object v2

    .line 199
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getRelatedStateVariableName()Ljava/lang/String;

    move-result-object v0

    .line 200
    .local v0, "relatedStatVarName":Ljava/lang/String;
    invoke-virtual {v1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Service;->getStateVariable(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v2

    goto :goto_0
.end method

.method public getRelatedStateVariableName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "relatedStateVariable"

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNodeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAllowedValue(I)Z
    .locals 5
    .param p1, "value"    # I

    .prologue
    const/4 v3, 0x1

    .line 268
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getRelatedStateVariable()Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v2

    .line 269
    .local v2, "sv":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    if-eqz v2, :cond_0

    .line 270
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->hasAllowedValueList()Z

    move-result v4

    if-ne v4, v3, :cond_1

    .line 271
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getAllowedValueList()Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;

    move-result-object v0

    .line 272
    .local v0, "list":Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->isAllowsedValue(Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;Ljava/lang/String;)Z

    move-result v3

    .line 279
    .end local v0    # "list":Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;
    :cond_0
    :goto_0
    return v3

    .line 273
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->hasAllowedValueRange()Z

    move-result v4

    if-ne v4, v3, :cond_0

    .line 274
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getAllowedValueRange()Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;

    move-result-object v1

    .line 275
    .local v1, "range":Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->isAllowedValue(Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;Ljava/lang/String;)Z

    move-result v3

    goto :goto_0
.end method

.method public isAllowedValue(Ljava/lang/String;)Z
    .locals 5
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 253
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getRelatedStateVariable()Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;

    move-result-object v2

    .line 254
    .local v2, "sv":Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;
    if-eqz v2, :cond_0

    .line 255
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->hasAllowedValueList()Z

    move-result v4

    if-ne v4, v3, :cond_1

    .line 256
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getAllowedValueList()Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;

    move-result-object v0

    .line 257
    .local v0, "list":Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;
    invoke-direct {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->isAllowsedValue(Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;Ljava/lang/String;)Z

    move-result v3

    .line 264
    .end local v0    # "list":Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueList;
    :cond_0
    :goto_0
    return v3

    .line 258
    :cond_1
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->hasAllowedValueRange()Z

    move-result v4

    if-ne v4, v3, :cond_0

    .line 259
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/StateVariable;->getAllowedValueRange()Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;

    move-result-object v1

    .line 260
    .local v1, "range":Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;
    invoke-direct {p0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->isAllowedValue(Lcom/samsung/android/allshare/stack/upnp/upnp/AllowedValueRange;Ljava/lang/String;)Z

    move-result v3

    goto :goto_0
.end method

.method public isInDirection()Z
    .locals 2

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getDirection()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "dir":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 173
    const/4 v1, 0x0

    .line 174
    :goto_0
    return v1

    :cond_0
    const-string v1, "in"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isOutDirection()Z
    .locals 1

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->isInDirection()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDirection(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "direction"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method public setRelatedStateVariableName(Ljava/lang/String;)V
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 188
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentNode()Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v0

    const-string v1, "relatedStateVariable"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->setNode(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    return-void
.end method

.method public setValue(I)V
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 227
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 228
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 223
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getArgumentData()Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/xml/ArgumentData;->setValue(Ljava/lang/String;)V

    .line 224
    return-void
.end method
