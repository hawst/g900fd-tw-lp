.class public Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetIPActionHandler;
.super Ljava/lang/Object;
.source "GetIPActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# instance fields
.field private mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V
    .locals 1
    .param p1, "cameraListener"    # Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetIPActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 27
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetIPActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 28
    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 3
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    .line 32
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetIPActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v2}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onGetIPRequest()Ljava/lang/String;

    move-result-object v1

    .line 34
    .local v1, "strResult":Ljava/lang/String;
    const-string v2, "GETIPRESULT"

    invoke-virtual {p1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 35
    .local v0, "idArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v0, :cond_0

    .line 36
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 42
    const/4 v2, 0x1

    :goto_0
    return v2

    .line 38
    :cond_0
    const/16 v2, 0x192

    invoke-virtual {p1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    .line 39
    const/4 v2, 0x0

    goto :goto_0
.end method
