.class public interface abstract Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;
.super Ljava/lang/Object;
.source "ICameraListener.java"


# virtual methods
.method public abstract onAFReleaseRequest()I
.end method

.method public abstract onAFRequest()I
.end method

.method public abstract onGetIPRequest()Ljava/lang/String;
.end method

.method public abstract onGetInformation_GetIPResult_Request()Ljava/lang/String;
.end method

.method public abstract onGetInformation_GetinformationResult_Request()Ljava/lang/String;
.end method

.method public abstract onGetZoom()I
.end method

.method public abstract onMultiAFRequest()Ljava/lang/String;
.end method

.method public abstract onReleaseSelfTimer(I)Z
.end method

.method public abstract onRvfSessionClosed()V
.end method

.method public abstract onRvfSessionCreated()V
.end method

.method public abstract onSetFlashRequest(Ljava/lang/String;)Z
.end method

.method public abstract onSetLEDRequest(I)Z
.end method

.method public abstract onSetResolutionRequest(Ljava/lang/String;)Z
.end method

.method public abstract onSetZoom(I)I
.end method

.method public abstract onShotRequest_GetAFShotResult_Request()Ljava/lang/String;
.end method

.method public abstract onShotRequest_GetAvailShots_Request()I
.end method

.method public abstract onShotWithGPSRequest_GetAFShotResult_Request(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract onShotWithGPSRequest_GetAvailShots_Request(Ljava/lang/String;)I
.end method

.method public abstract onShutterUp()Z
.end method

.method public abstract onStreamingServerStarted(I)V
.end method

.method public abstract onZoomInRequest()I
.end method

.method public abstract onZoomOutRequest()I
.end method
