.class Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchSender"
.end annotation


# instance fields
.field private mTarget:Ljava/lang/String;

.field final synthetic this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;


# direct methods
.method private constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)V
    .locals 1

    .prologue
    .line 978
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 974
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->mTarget:Ljava/lang/String;

    .line 979
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p2, "x1"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$1;

    .prologue
    .line 973
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)V

    return-void
.end method

.method private constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;)V
    .locals 1
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 981
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 974
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->mTarget:Ljava/lang/String;

    .line 982
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->mTarget:Ljava/lang/String;

    .line 983
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$1;

    .prologue
    .line 973
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 988
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeActivityList()V
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1000(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)V

    .line 990
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->mTarget:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 991
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1100(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->createSearchSendSocketList()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;

    move-result-object v0

    .line 992
    .local v0, "list":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->setSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    .line 993
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getSearchMx()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->start(I)V

    .line 994
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->mTarget:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getSearchMx()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;-><init>(Ljava/lang/String;I)V

    .line 995
    .local v1, "msReq":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->setSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    .line 996
    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;)Z

    .line 1004
    .end local v1    # "msReq":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;
    :goto_0
    return-void

    .line 998
    .end local v0    # "list":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1100(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->createSearchSendSocketList()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;

    move-result-object v0

    .line 999
    .restart local v0    # "list":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->setSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    .line 1000
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->getSearchMx()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->start(I)V

    .line 1001
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$SearchSender;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v0, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->setSearchResponseListener(Lcom/samsung/android/allshare/stack/upnp/upnp/device/ISSDPListener;)V

    .line 1002
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchResponseSocketList;->postAllSearchTarget()Z

    goto :goto_0
.end method
