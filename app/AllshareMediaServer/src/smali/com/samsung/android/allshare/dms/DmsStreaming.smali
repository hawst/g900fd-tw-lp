.class public Lcom/samsung/android/allshare/dms/DmsStreaming;
.super Ljava/lang/Object;
.source "DmsStreaming.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;,
        Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;,
        Lcom/samsung/android/allshare/dms/DmsStreaming$ErrorCode;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "DmsStreaming"

.field private static instance:Lcom/samsung/android/allshare/dms/DmsStreaming;

.field private static requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

.field private static rvfSessionListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;


# instance fields
.field private isInitialized:Z

.field private isServerStarted:Z

.field private libraryLoaded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    sput-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->instance:Lcom/samsung/android/allshare/dms/DmsStreaming;

    .line 31
    sput-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    .line 33
    sput-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->rvfSessionListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean v2, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->isInitialized:Z

    .line 29
    iput-boolean v2, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->libraryLoaded:Z

    .line 37
    iput-boolean v2, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->isServerStarted:Z

    .line 55
    :try_start_0
    const-string v1, "asf_mediaserver"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 57
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->libraryLoaded:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "ex":Ljava/lang/Exception;
    iput-boolean v2, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->libraryLoaded:Z

    .line 60
    const-string v1, "DmsStreaming"

    const-string v2, "DmsStreaming"

    const-string v3, "loadLibrary failure!"

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;
    .locals 2

    .prologue
    .line 45
    const-class v1, Lcom/samsung/android/allshare/dms/DmsStreaming;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->instance:Lcom/samsung/android/allshare/dms/DmsStreaming;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/samsung/android/allshare/dms/DmsStreaming;

    invoke-direct {v0}, Lcom/samsung/android/allshare/dms/DmsStreaming;-><init>()V

    sput-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->instance:Lcom/samsung/android/allshare/dms/DmsStreaming;

    .line 48
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->instance:Lcom/samsung/android/allshare/dms/DmsStreaming;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static onFileReceiveCompleted(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 202
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    if-nez v0, :cond_0

    .line 203
    const-string v0, "DmsStreaming"

    const-string v1, "onFileReceiveCompleted"

    const-string v2, "SetRequestReceivedListener is not done"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :goto_0
    return-void

    .line 207
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;->onFileReceiveCompleted(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static onFileReceiveFailed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "filepath"    # Ljava/lang/String;

    .prologue
    .line 211
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    if-nez v0, :cond_0

    .line 212
    const-string v0, "DmsStreaming"

    const-string v1, "onFileReceiveFailed"

    const-string v2, "SetRequestReceivedListener is not done"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    :goto_0
    return-void

    .line 216
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;->onFileReceiveFailed(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static onFileReceiveProgressUpdated(Ljava/lang/String;Ljava/lang/String;JJ)V
    .locals 8
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "filepath"    # Ljava/lang/String;
    .param p2, "received_size"    # J
    .param p4, "total_size"    # J

    .prologue
    .line 221
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    if-nez v0, :cond_0

    .line 222
    const-string v0, "DmsStreaming"

    const-string v1, "onFileReceiveProgressUpdated"

    const-string v2, "SetRequestReceivedListener is not done"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    :goto_0
    return-void

    .line 227
    :cond_0
    sget-object v1, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-interface/range {v1 .. v7}, Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;->onFileReceiveProgressUpdated(Ljava/lang/String;Ljava/lang/String;JJ)V

    goto :goto_0
.end method

.method public static onRequestReceived(Ljava/lang/String;)Lcom/samsung/android/allshare/dms/DMSItem;
    .locals 5
    .param p0, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 183
    sget-object v2, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    if-nez v2, :cond_1

    .line 184
    const-string v2, "DmsStreaming"

    const-string v3, "onRequestReceived"

    const-string v4, "SetRequestReceivedListener is not done"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 197
    :cond_0
    :goto_0
    return-object v0

    .line 188
    :cond_1
    sget-object v2, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    invoke-interface {v2, p0}, Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;->onRequestReceived(Ljava/lang/String;)Lcom/samsung/android/allshare/dms/DMSItem;

    move-result-object v0

    .line 189
    .local v0, "item_value":Lcom/samsung/android/allshare/dms/DMSItem;
    if-nez v0, :cond_2

    .line 190
    const-string v2, "DmsStreaming"

    const-string v3, "onRequestReceived"

    const-string v4, "item_value is null!!!!!!"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 191
    goto :goto_0

    .line 193
    :cond_2
    iget-object v1, v0, Lcom/samsung/android/allshare/dms/DMSItem;->file_path:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/samsung/android/allshare/dms/DMSItem;->file_path:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 194
    :cond_3
    const-string v1, "DmsStreaming"

    const-string v2, "onRequestReceived"

    const-string v3, "Filepath is empty!!!!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static onRvfSessionClosed()V
    .locals 3

    .prologue
    .line 310
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->rvfSessionListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;

    if-nez v0, :cond_0

    .line 311
    const-string v0, "DmsStreaming"

    const-string v1, "onRvfSessionClosed"

    const-string v2, "SetRvfSessionListener is not done"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :goto_0
    return-void

    .line 315
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->rvfSessionListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;

    invoke-interface {v0}, Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;->onRvfSessionClosed()V

    goto :goto_0
.end method

.method public static onRvfSessionCreated()V
    .locals 3

    .prologue
    .line 301
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->rvfSessionListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;

    if-nez v0, :cond_0

    .line 302
    const-string v0, "DmsStreaming"

    const-string v1, "onRvfSessionCreated"

    const-string v2, "SetRvfSessionListener is not done"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    :goto_0
    return-void

    .line 306
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->rvfSessionListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;

    invoke-interface {v0}, Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;->onRvfSessionCreated()V

    goto :goto_0
.end method


# virtual methods
.method public doesRvfSessionExist()Z
    .locals 5

    .prologue
    .line 319
    invoke-static {}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->DoesRvfSessionExist()Z

    move-result v0

    .line 320
    .local v0, "result":Z
    const-string v1, "DmsStreaming"

    const-string v2, "DoesRvfSessionExist"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "dmsStreamingDoesRvfSessionExist result!!! = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    return v0
.end method

.method public getStreamingPort(Ljava/lang/String;Lcom/samsung/android/allshare/dms/PortID;)I
    .locals 5
    .param p1, "ipaddress"    # Ljava/lang/String;
    .param p2, "port_number"    # Lcom/samsung/android/allshare/dms/PortID;

    .prologue
    .line 152
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 153
    :cond_0
    const-string v1, "DmsStreaming"

    const-string v2, "getStreamingPort"

    const-string v3, "GetStreamingPort failed!!! Argument null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v0, -0x1

    .line 162
    :cond_1
    :goto_0
    return v0

    .line 157
    :cond_2
    invoke-static {p1, p2}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->GetStreamingPort(Ljava/lang/String;Lcom/samsung/android/allshare/dms/PortID;)I

    move-result v0

    .line 158
    .local v0, "result":I
    if-eqz v0, :cond_1

    .line 159
    const-string v1, "DmsStreaming"

    const-string v2, "getStreamingPort"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GetStreamingPort failed!!! Error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public initialize()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 82
    iget-boolean v2, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->isInitialized:Z

    if-nez v2, :cond_0

    .line 83
    iget-boolean v2, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->libraryLoaded:Z

    if-nez v2, :cond_1

    move v0, v1

    .line 99
    :goto_0
    return v0

    .line 88
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    .line 91
    :cond_1
    const/4 v0, 0x0

    .line 92
    .local v0, "result":I
    invoke-static {}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->InitializeCore()I

    move-result v0

    .line 93
    if-eqz v0, :cond_2

    .line 94
    const-string v1, "DmsStreaming"

    const-string v2, "Initialize"

    const-string v3, "Initialize AllShareFrameworkCore failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 96
    :cond_2
    iput-boolean v1, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->isInitialized:Z

    .line 97
    const-string v1, "DmsStreaming"

    const-string v2, "Initialize"

    const-string v3, "Initialize the AllShareFrameworkCore successfully!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onStartRecording()V
    .locals 0

    .prologue
    .line 326
    invoke-static {}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->OnStartRecording()V

    .line 327
    return-void
.end method

.method public onStopRecording()V
    .locals 0

    .prologue
    .line 330
    invoke-static {}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->OnStopRecording()V

    .line 331
    return-void
.end method

.method public setDownloadUri(Ljava/lang/String;Ljava/lang/String;J)I
    .locals 5
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "filepath"    # Ljava/lang/String;
    .param p3, "filesize"    # J

    .prologue
    .line 166
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 167
    :cond_0
    const/4 v0, -0x1

    .line 175
    :cond_1
    :goto_0
    return v0

    .line 170
    :cond_2
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->SetDownloadUri(Ljava/lang/String;Ljava/lang/String;J)I

    move-result v0

    .line 171
    .local v0, "result":I
    if-eqz v0, :cond_1

    .line 172
    const-string v1, "DmsStreaming"

    const-string v2, "setDownloadUri"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SetDownloadUri failed!!! Error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setRequestReceivedListener(Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    .prologue
    .line 179
    sput-object p1, Lcom/samsung/android/allshare/dms/DmsStreaming;->requestReceivedListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;

    .line 180
    return-void
.end method

.method public startRvfSessionListener(Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;

    .prologue
    .line 291
    sput-object p1, Lcom/samsung/android/allshare/dms/DmsStreaming;->rvfSessionListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;

    .line 292
    invoke-static {}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->StartRvfSessionListener()V

    .line 293
    return-void
.end method

.method public startServer()I
    .locals 5

    .prologue
    .line 125
    invoke-static {}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->StartStreamingServer()I

    move-result v0

    .line 126
    .local v0, "result":I
    if-eqz v0, :cond_0

    .line 127
    const-string v1, "DmsStreaming"

    const-string v2, "StartServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "StartStreamingServer failed!!! Error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    :goto_0
    return v0

    .line 129
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->isServerStarted:Z

    .line 130
    const-string v1, "DmsStreaming"

    const-string v2, "StartServer"

    const-string v3, "StartStreamingServer successfully!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopRvfSessionListener()V
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/allshare/dms/DmsStreaming;->rvfSessionListener:Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;

    .line 297
    invoke-static {}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->StopRvfSessionListener()V

    .line 298
    return-void
.end method

.method public stopServer()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 137
    iget-boolean v2, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->isServerStarted:Z

    if-nez v2, :cond_0

    move v0, v1

    .line 148
    :goto_0
    return v0

    .line 141
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->StopStreamingServer()I

    move-result v0

    .line 142
    .local v0, "result":I
    if-eqz v0, :cond_1

    .line 143
    const-string v1, "DmsStreaming"

    const-string v2, "stopServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "StopStreamingServer failed!!! Error code = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 145
    :cond_1
    iput-boolean v1, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->isServerStarted:Z

    .line 146
    const-string v1, "DmsStreaming"

    const-string v2, "stopServer"

    const-string v3, "StopStreamingServer successfully!!!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public terminate()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-boolean v2, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->isInitialized:Z

    if-nez v2, :cond_0

    move v0, v1

    .line 121
    :goto_0
    return v0

    .line 113
    :cond_0
    invoke-static {}, Lcom/samsung/android/allshare/dms/DMSStreamingNativeAPI;->TerminateCore()I

    move-result v0

    .line 114
    .local v0, "result":I
    if-eqz v0, :cond_1

    .line 115
    const-string v1, "DmsStreaming"

    const-string v2, "Terminate"

    const-string v3, "Terminate AllShareFrameworkCore failed!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 117
    :cond_1
    iput-boolean v1, p0, Lcom/samsung/android/allshare/dms/DmsStreaming;->isInitialized:Z

    .line 118
    const-string v1, "DmsStreaming"

    const-string v2, "Terminate"

    const-string v3, "Terminate AllShareFrameworkCore successfully!"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
