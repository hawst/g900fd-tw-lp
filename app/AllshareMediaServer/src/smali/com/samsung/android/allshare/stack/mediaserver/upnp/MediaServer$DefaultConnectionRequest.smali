.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;
.source "MediaServer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DefaultConnectionRequest"
.end annotation


# instance fields
.field public mAllowed:Z

.field private mMacAddress:Ljava/lang/String;

.field private mTargetHost:Ljava/lang/String;

.field private mTargetInetAddr:Ljava/net/InetAddress;

.field private mTargetPort:I

.field private mUserAgent:Ljava/lang/String;

.field private mWaitObject:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "mac"    # Ljava/lang/String;
    .param p2, "userAgent"    # Ljava/lang/String;
    .param p3, "host"    # Ljava/lang/String;
    .param p4, "targetPort"    # I

    .prologue
    .line 460
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;-><init>()V

    .line 446
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    .line 448
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mAllowed:Z

    .line 450
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mMacAddress:Ljava/lang/String;

    .line 452
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mUserAgent:Ljava/lang/String;

    .line 454
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetHost:Ljava/lang/String;

    .line 456
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetPort:I

    .line 458
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetInetAddr:Ljava/net/InetAddress;

    .line 461
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mMacAddress:Ljava/lang/String;

    .line 462
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mUserAgent:Ljava/lang/String;

    .line 463
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetHost:Ljava/lang/String;

    .line 464
    iput p4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetPort:I

    .line 465
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/net/InetAddress;)V
    .locals 4
    .param p1, "mac"    # Ljava/lang/String;
    .param p2, "userAgent"    # Ljava/lang/String;
    .param p3, "host"    # Ljava/lang/String;
    .param p4, "targetPort"    # I
    .param p5, "targetInetAddr"    # Ljava/net/InetAddress;

    .prologue
    .line 468
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;-><init>()V

    .line 446
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    .line 448
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mAllowed:Z

    .line 450
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mMacAddress:Ljava/lang/String;

    .line 452
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mUserAgent:Ljava/lang/String;

    .line 454
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetHost:Ljava/lang/String;

    .line 456
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetPort:I

    .line 458
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetInetAddr:Ljava/net/InetAddress;

    .line 469
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mMacAddress:Ljava/lang/String;

    .line 470
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mUserAgent:Ljava/lang/String;

    .line 471
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetHost:Ljava/lang/String;

    .line 472
    iput p4, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetPort:I

    .line 473
    iput-object p5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetInetAddr:Ljava/net/InetAddress;

    .line 475
    const-string v0, "MediaServer"

    const-string v1, "stop"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Host - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetInetAddr:Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 477
    return-void
.end method

.method private accept()V
    .locals 2

    .prologue
    .line 506
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 516
    :goto_0
    return-void

    .line 509
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mAllowed:Z

    .line 511
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    monitor-enter v1

    .line 512
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 513
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 515
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    goto :goto_0

    .line 513
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->reject()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->accept()V

    return-void
.end method

.method private reject()V
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 503
    :goto_0
    return-void

    .line 496
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mAllowed:Z

    .line 498
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    monitor-enter v1

    .line 499
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 500
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    goto :goto_0

    .line 500
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public getMacAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mMacAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetHost:Ljava/lang/String;

    return-object v0
.end method

.method public getTargetInetAddr()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetInetAddr:Ljava/net/InetAddress;

    return-object v0
.end method

.method public getTargetPort()I
    .locals 1

    .prologue
    .line 531
    iget v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mTargetPort:I

    return v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 523
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mUserAgent:Ljava/lang/String;

    return-object v0
.end method

.method public waitEvent(J)V
    .locals 5
    .param p1, "ms"    # J

    .prologue
    .line 480
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    if-nez v1, :cond_0

    .line 490
    :goto_0
    return-void

    .line 483
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    monitor-enter v2

    .line 485
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mWaitObject:Ljava/lang/Object;

    invoke-virtual {v1, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 489
    :goto_1
    :try_start_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 486
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v1, "MediaServer"

    const-string v3, "waitEvent"

    const-string v4, "waitEvent -Interrupted success."

    invoke-static {v1, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method
