.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
.super Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;
.source "ImportItemObject.java"


# instance fields
.field private mIsRemovable:Z

.field public final mLIFE_TIME:I

.field private mLastAutodestroyCheckTime:J

.field private mLifeTime:I


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    const v3, 0x88b8

    .line 109
    const-string v0, ""

    const-string v1, ""

    const-string v2, ""

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ItemObject;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    iput v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLIFE_TIME:I

    .line 25
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLifeTime:I

    .line 27
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLastAutodestroyCheckTime:J

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mIsRemovable:Z

    .line 110
    const-string v0, "item"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->setName(Ljava/lang/String;)V

    .line 111
    iput v3, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLifeTime:I

    .line 112
    return-void
.end method

.method public static createImportItemNode(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .locals 14
    .param p0, "node"    # Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    .prologue
    .line 33
    if-nez p0, :cond_0

    .line 34
    const/4 v3, 0x0

    .line 105
    :goto_0
    return-object v3

    .line 35
    :cond_0
    :try_start_0
    new-instance v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;

    invoke-direct {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;-><init>()V

    .line 38
    .local v3, "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNAttributes()I

    move-result v4

    .line 39
    .local v4, "nAttr":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_1

    .line 40
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttribute(I)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getAttribute(I)Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;

    move-result-object v13

    invoke-virtual {v13}, Lcom/samsung/android/allshare/stack/upnp/xml/Attribute;->getValue()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v3, v12, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 39
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 43
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNNodes()I

    move-result v5

    .line 44
    .local v5, "nProp":I
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v5, :cond_3

    .line 45
    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/xml/Node;->getNode(I)Lcom/samsung/android/allshare/stack/upnp/xml/Node;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->createProperty(Lcom/samsung/android/allshare/stack/upnp/xml/Node;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v7

    .line 46
    .local v7, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v7, :cond_2

    .line 47
    invoke-direct {v3, v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->setProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)V

    .line 44
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 51
    .end local v7    # "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    :cond_3
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getTitle()Ljava/lang/String;

    move-result-object v12

    if-eqz v12, :cond_4

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    if-nez v12, :cond_5

    .line 52
    :cond_4
    const/4 v3, 0x0

    goto :goto_0

    .line 54
    :cond_5
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getTitle()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->unEscapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->escapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->setTitle(Ljava/lang/String;)V

    .line 59
    const-string v12, "restricted"

    invoke-virtual {v3, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->hasAttribute(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 60
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getRestricted()Z

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_6

    .line 61
    const/4 v3, 0x0

    goto :goto_0

    .line 62
    :cond_6
    const/4 v12, 0x0

    invoke-virtual {v3, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->setRestricted(Z)V

    .line 66
    :cond_7
    const-string v12, "dlna:dlnaManaged"

    invoke-virtual {v3, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->hasAttribute(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 67
    const-string v12, "dlna:dlnaManaged"

    const-string v13, "00000004"

    invoke-virtual {v3, v12, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 70
    :cond_8
    const-string v12, "res"

    invoke-virtual {v3, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getPropertyList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 71
    .local v10, "resList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    .line 74
    .local v0, "ctRes":I
    const/4 v12, 0x1

    if-eq v0, v12, :cond_9

    .line 75
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 78
    :cond_9
    const/4 v12, 0x0

    invoke-interface {v10, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .line 79
    .local v9, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    const-string v12, "protocolInfo"

    invoke-virtual {v9, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 80
    .local v8, "protocol":Ljava/lang/String;
    const-string v6, ""

    .line 81
    .local v6, "newProtocol":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v12

    invoke-virtual {v12, v8}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->convertToValidateProtocol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 82
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->isSupportedProtocol(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_a

    .line 83
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->convertToValidateProtocolWithout4thField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 85
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v12

    invoke-virtual {v12, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->isSupportedProtocol(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_a

    .line 86
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 90
    :cond_a
    const-string v12, "protocolInfo"

    invoke-virtual {v9, v12, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 93
    const-string v12, "dlna:resumeUpload"

    invoke-virtual {v9, v12}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 94
    .local v11, "resume":Ljava/lang/String;
    if-eqz v11, :cond_b

    .line 96
    const-string v12, "dlna:resumeUpload"

    const-string v13, "0"

    invoke-virtual {v9, v12, v13}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 99
    :cond_b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    iput-wide v12, v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLastAutodestroyCheckTime:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 101
    .end local v0    # "ctRes":I
    .end local v2    # "i":I
    .end local v3    # "item":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .end local v4    # "nAttr":I
    .end local v5    # "nProp":I
    .end local v6    # "newProtocol":Ljava/lang/String;
    .end local v8    # "protocol":Ljava/lang/String;
    .end local v9    # "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .end local v10    # "resList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;>;"
    .end local v11    # "resume":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 105
    const/4 v3, 0x0

    goto/16 :goto_0
.end method

.method private setProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)V
    .locals 2
    .param p1, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .prologue
    .line 115
    if-nez p1, :cond_0

    .line 121
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v0

    .line 118
    .local v0, "old":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v0, :cond_1

    .line 119
    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->removeProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    .line 120
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->addProperty(Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Z

    goto :goto_0
.end method


# virtual methods
.method public isPostAble()Z
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLifeTime:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemovable()Z
    .locals 1

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mIsRemovable:Z

    return v0
.end method

.method public rebirth()V
    .locals 1

    .prologue
    .line 143
    const v0, 0x88b8

    iput v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLifeTime:I

    .line 144
    return-void
.end method

.method public reduceLifeTime(J)V
    .locals 5
    .param p1, "currTime"    # J

    .prologue
    .line 136
    iget-wide v2, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLastAutodestroyCheckTime:J

    sub-long v2, p1, v2

    long-to-int v0, v2

    .line 137
    .local v0, "theta":I
    iget v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLifeTime:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLifeTime:I

    .line 138
    iput-wide p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mLastAutodestroyCheckTime:J

    .line 140
    return-void
.end method

.method public setImportResourceUri(Ljava/lang/String;)V
    .locals 2
    .param p1, "contentImportURL"    # Ljava/lang/String;

    .prologue
    .line 147
    const-string v1, "res"

    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getProperty(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    move-result-object v0

    .line 148
    .local v0, "prop":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    if-eqz v0, :cond_0

    .line 149
    const-string v1, "importUri"

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->setAttribute(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 150
    :cond_0
    return-void
.end method

.method public setRemovable(Z)V
    .locals 0
    .param p1, "isRemovable"    # Z

    .prologue
    .line 128
    iput-boolean p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->mIsRemovable:Z

    .line 129
    return-void
.end method
