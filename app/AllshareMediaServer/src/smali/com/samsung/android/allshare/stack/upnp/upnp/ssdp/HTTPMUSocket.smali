.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;
.super Ljava/lang/Object;
.source "HTTPMUSocket.java"


# instance fields
.field protected mBindAddress:Ljava/net/InetAddress;

.field private mNetworkInterface:Ljava/net/NetworkInterface;

.field private mSsdpMultiGroup:Ljava/net/InetSocketAddress;

.field private mSsdpMultiIf:Ljava/net/NetworkInterface;

.field private mSsdpMultiSock:Ljava/net/MulticastSocket;

.field private final mTag:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    .line 55
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiGroup:Ljava/net/InetSocketAddress;

    .line 57
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    .line 59
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiIf:Ljava/net/NetworkInterface;

    .line 61
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mBindAddress:Ljava/net/InetAddress;

    .line 79
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mNetworkInterface:Ljava/net/NetworkInterface;

    .line 68
    return-void
.end method


# virtual methods
.method public close()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 141
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    if-nez v2, :cond_0

    .line 155
    :goto_0
    return v1

    .line 145
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiGroup:Ljava/net/InetSocketAddress;

    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiIf:Ljava/net/NetworkInterface;

    invoke-virtual {v2, v3, v4}, Ljava/net/MulticastSocket;->leaveGroup(Ljava/net/SocketAddress;Ljava/net/NetworkInterface;)V

    .line 146
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v3, "close"

    const-string v4, "Call: Datagram.close()"

    invoke-static {v2, v3, v4}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    invoke-virtual {v2}, Ljava/net/MulticastSocket;->close()V

    .line 148
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v2, "close"

    const-string v3, "close Exception "

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 152
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMulticastInetAddress()Ljava/net/InetAddress;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiGroup:Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    return-object v0
.end method

.method public getNetworkInterface()Ljava/net/NetworkInterface;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mNetworkInterface:Ljava/net/NetworkInterface;

    return-object v0
.end method

.method public open(Ljava/lang/String;ILjava/net/InetAddress;)Z
    .locals 7
    .param p1, "joinAddress"    # Ljava/lang/String;
    .param p2, "port"    # I
    .param p3, "bindAddr"    # Ljava/net/InetAddress;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 108
    :try_start_0
    new-instance v4, Ljava/net/MulticastSocket;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Ljava/net/MulticastSocket;-><init>(Ljava/net/SocketAddress;)V

    iput-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    .line 109
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/net/MulticastSocket;->setReuseAddress(Z)V

    .line 110
    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p2}, Ljava/net/InetSocketAddress;-><init>(I)V

    .line 111
    .local v0, "bindSockAddr":Ljava/net/InetSocketAddress;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    invoke-virtual {v4, v0}, Ljava/net/MulticastSocket;->bind(Ljava/net/SocketAddress;)V

    .line 112
    new-instance v4, Ljava/net/InetSocketAddress;

    invoke-direct {v4, p1, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiGroup:Ljava/net/InetSocketAddress;

    .line 117
    invoke-static {p3}, Ljava/net/NetworkInterface;->getByInetAddress(Ljava/net/InetAddress;)Ljava/net/NetworkInterface;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiIf:Ljava/net/NetworkInterface;

    .line 119
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiGroup:Ljava/net/InetSocketAddress;

    iget-object v6, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiIf:Ljava/net/NetworkInterface;

    invoke-virtual {v4, v5, v6}, Ljava/net/MulticastSocket;->joinGroup(Ljava/net/SocketAddress;Ljava/net/NetworkInterface;)V

    .line 120
    iput-object p3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mBindAddress:Ljava/net/InetAddress;
    :try_end_0
    .catch Ljava/net/BindException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 137
    .end local v0    # "bindSockAddr":Ljava/net/InetSocketAddress;
    :goto_0
    return v2

    .line 121
    :catch_0
    move-exception v1

    .line 123
    .local v1, "e":Ljava/net/BindException;
    if-eqz p3, :cond_0

    .line 124
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v4, "open"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fail to bind to: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    move v2, v3

    .line 130
    goto :goto_0

    .line 125
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mBindAddress:Ljava/net/InetAddress;

    if-eqz v2, :cond_1

    .line 126
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v4, "open"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fail to bind to: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mBindAddress:Ljava/net/InetAddress;

    invoke-virtual {v6}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 128
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v4, "open"

    const-string v5, "fail to bind invalid argument."

    invoke-static {v2, v4, v5}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 131
    .end local v1    # "e":Ljava/net/BindException;
    :catch_1
    move-exception v1

    .line 133
    .local v1, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v4, "open"

    const-string v5, "open Exception "

    invoke-static {v2, v4, v5, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move v2, v3

    .line 134
    goto :goto_0
.end method

.method public post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)Z
    .locals 3
    .param p1, "req"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 229
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->send(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public receive()Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    .locals 6

    .prologue
    .line 237
    const/16 v3, 0x400

    new-array v2, v3, [B

    .line 238
    .local v2, "ssdvRecvBuf":[B
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;

    array-length v3, v2

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;-><init>([BI)V

    .line 241
    .local v1, "recvPacket":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    invoke-virtual {v3}, Ljava/net/MulticastSocket;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 242
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->getDatagramPacket()Ljava/net/DatagramPacket;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 243
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->setTimeStamp(J)V

    .line 244
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiSock:Ljava/net/MulticastSocket;

    invoke-virtual {v3}, Ljava/net/MulticastSocket;->getNetworkInterface()Ljava/net/NetworkInterface;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPPacket;->setNetworkInterface(Ljava/net/NetworkInterface;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :cond_0
    :goto_0
    return-object v1

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v4, "receive"

    const-string v5, "receive Exception"

    invoke-static {v3, v4, v5, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public send(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 9
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "bindAddr"    # Ljava/lang/String;
    .param p3, "bindPort"    # I

    .prologue
    const/4 v4, 0x0

    .line 163
    const/4 v2, 0x0

    .line 165
    .local v2, "msock":Ljava/net/MulticastSocket;
    if-eqz p2, :cond_0

    if-lez p3, :cond_0

    .line 166
    :try_start_0
    new-instance v3, Ljava/net/MulticastSocket;

    const/4 v5, 0x0

    invoke-direct {v3, v5}, Ljava/net/MulticastSocket;-><init>(Ljava/net/SocketAddress;)V
    :try_end_0
    .catch Ljava/net/BindException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 167
    .end local v2    # "msock":Ljava/net/MulticastSocket;
    .local v3, "msock":Ljava/net/MulticastSocket;
    :try_start_1
    new-instance v5, Ljava/net/InetSocketAddress;

    invoke-direct {v5, p2, p3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v3, v5}, Ljava/net/MulticastSocket;->bind(Ljava/net/SocketAddress;)V
    :try_end_1
    .catch Ljava/net/BindException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-object v2, v3

    .line 184
    .end local v3    # "msock":Ljava/net/MulticastSocket;
    .restart local v2    # "msock":Ljava/net/MulticastSocket;
    :goto_0
    :try_start_2
    new-instance v0, Ljava/net/DatagramPacket;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mSsdpMultiGroup:Ljava/net/InetSocketAddress;

    invoke-direct {v0, v5, v6, v7}, Ljava/net/DatagramPacket;-><init>([BILjava/net/SocketAddress;)V

    .line 187
    .local v0, "dgmPacket":Ljava/net/DatagramPacket;
    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Ljava/net/MulticastSocket;->setTimeToLive(I)V

    .line 188
    invoke-virtual {v2, v0}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V

    .line 189
    invoke-virtual {v2}, Ljava/net/MulticastSocket;->close()V

    .line 211
    const/4 v4, 0x1

    .end local v0    # "dgmPacket":Ljava/net/DatagramPacket;
    :goto_1
    return v4

    .line 169
    :cond_0
    new-instance v3, Ljava/net/MulticastSocket;

    invoke-direct {v3}, Ljava/net/MulticastSocket;-><init>()V
    :try_end_2
    .catch Ljava/net/BindException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 170
    .end local v2    # "msock":Ljava/net/MulticastSocket;
    .restart local v3    # "msock":Ljava/net/MulticastSocket;
    :try_start_3
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mBindAddress:Ljava/net/InetAddress;

    if-eqz v5, :cond_1

    .line 171
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mBindAddress:Ljava/net/InetAddress;

    invoke-virtual {v3, v5}, Ljava/net/MulticastSocket;->setInterface(Ljava/net/InetAddress;)V
    :try_end_3
    .catch Ljava/net/BindException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    :cond_1
    move-object v2, v3

    .end local v3    # "msock":Ljava/net/MulticastSocket;
    .restart local v2    # "msock":Ljava/net/MulticastSocket;
    goto :goto_0

    .line 190
    :catch_0
    move-exception v1

    .line 191
    .local v1, "e":Ljava/net/BindException;
    :goto_2
    if-eqz v2, :cond_2

    .line 192
    invoke-virtual {v2}, Ljava/net/MulticastSocket;->close()V

    .line 196
    :cond_2
    if-eqz p2, :cond_3

    .line 197
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v6, "send"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fail to bind to: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 198
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mBindAddress:Ljava/net/InetAddress;

    if-eqz v5, :cond_4

    .line 199
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v6, "send"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "fail to bind to: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mBindAddress:Ljava/net/InetAddress;

    invoke-virtual {v8}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 201
    :cond_4
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v6, "send"

    const-string v7, "fail to bind invalid argument."

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 204
    .end local v1    # "e":Ljava/net/BindException;
    :catch_1
    move-exception v1

    .line 205
    .local v1, "e":Ljava/lang/Exception;
    :goto_3
    if-eqz v2, :cond_5

    .line 206
    invoke-virtual {v2}, Ljava/net/MulticastSocket;->close()V

    .line 208
    :cond_5
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mTag:Ljava/lang/String;

    const-string v6, "send"

    const-string v7, "send Exception "

    invoke-static {v5, v6, v7, v1}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_1

    .line 204
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "msock":Ljava/net/MulticastSocket;
    .restart local v3    # "msock":Ljava/net/MulticastSocket;
    :catch_2
    move-exception v1

    move-object v2, v3

    .end local v3    # "msock":Ljava/net/MulticastSocket;
    .restart local v2    # "msock":Ljava/net/MulticastSocket;
    goto :goto_3

    .line 190
    .end local v2    # "msock":Ljava/net/MulticastSocket;
    .restart local v3    # "msock":Ljava/net/MulticastSocket;
    :catch_3
    move-exception v1

    move-object v2, v3

    .end local v3    # "msock":Ljava/net/MulticastSocket;
    .restart local v2    # "msock":Ljava/net/MulticastSocket;
    goto :goto_2
.end method

.method public setNetworkInterface(Ljava/net/NetworkInterface;)V
    .locals 0
    .param p1, "ni"    # Ljava/net/NetworkInterface;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/HTTPMUSocket;->mNetworkInterface:Ljava/net/NetworkInterface;

    .line 87
    return-void
.end method
