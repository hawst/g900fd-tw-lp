.class public Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;
.super Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;
.source "SSDPSearchRequest.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "upnp:rootdevice"

    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;-><init>(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "serachTarget"    # Ljava/lang/String;

    .prologue
    .line 44
    const/4 v0, 0x5

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;-><init>(Ljava/lang/String;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2
    .param p1, "serachTarget"    # Ljava/lang/String;
    .param p2, "mx"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPRequest;-><init>()V

    .line 35
    const-string v0, "M-SEARCH"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;->setMethod(Ljava/lang/String;)V

    .line 36
    const-string v0, "*"

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;->setURI(Ljava/lang/String;)V

    .line 38
    const-string v0, "ST"

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    const-string v0, "MX"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v0, "MAN"

    const-string v1, "\"ssdp:discover\""

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPSearchRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method
