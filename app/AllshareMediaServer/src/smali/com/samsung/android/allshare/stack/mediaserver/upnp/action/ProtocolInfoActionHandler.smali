.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ProtocolInfoActionHandler;
.super Ljava/lang/Object;
.source "ProtocolInfoActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# static fields
.field private static final SINK:Ljava/lang/String; = "Sink"

.field private static final SOURCE:Ljava/lang/String; = "Source"


# instance fields
.field private mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;)V
    .locals 1
    .param p1, "mediaServer"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ProtocolInfoActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 44
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ProtocolInfoActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    .line 45
    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 8
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/16 v7, 0x192

    const/4 v4, 0x0

    .line 49
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionRequest()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v1

    .line 50
    .local v1, "args":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    .line 53
    .local v0, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Source"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Sink"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 56
    :cond_1
    invoke-virtual {p1, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    .line 82
    .end local v0    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :goto_0
    return v4

    .line 61
    :cond_2
    const-string v5, "Source"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 62
    .local v2, "argument":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v2, :cond_3

    .line 64
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getParser()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getCurrentProtocolInfo()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 65
    iget-object v5, p0, Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/ProtocolInfoActionHandler;->mServer:Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;

    invoke-virtual {v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer;->updateSoruceProtocolInfo()V

    .line 73
    const-string v5, "Sink"

    invoke-virtual {p1, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v2

    .line 74
    if-eqz v2, :cond_4

    .line 75
    const-string v4, ""

    invoke-virtual {v2, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 82
    const/4 v4, 0x1

    goto :goto_0

    .line 67
    :cond_3
    invoke-virtual {p1, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 77
    :cond_4
    invoke-virtual {p1, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0
.end method
