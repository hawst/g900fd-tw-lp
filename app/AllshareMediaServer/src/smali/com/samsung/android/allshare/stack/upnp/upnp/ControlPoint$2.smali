.class Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;
.super Ljava/lang/Object;
.source "ControlPoint.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;


# direct methods
.method constructor <init>(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)V
    .locals 0

    .prologue
    .line 1057
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1060
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->removeExpiredDevices()V

    .line 1061
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHandlerCheckExipredDevice:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1600(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRunnableCheckExipredDevice:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1400(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Runnable;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mCnt:I
    invoke-static {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1500(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1063
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # operator++ for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mCnt:I
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1508(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)I

    .line 1065
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mCnt:I
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1500(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)I

    move-result v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_0

    .line 1066
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # setter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mCnt:I
    invoke-static {v0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1502(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;I)I

    .line 1067
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # invokes: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->setRefreshMode(Z)V
    invoke-static {v0, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1700(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;Z)V

    .line 1068
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mHandlerCheckExipredDevice:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1600(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint$2;->this$0:Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;

    # getter for: Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->mRunnableCheckExipredDevice:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;->access$1400(Lcom/samsung/android/allshare/stack/upnp/upnp/ControlPoint;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1071
    :cond_0
    return-void
.end method
