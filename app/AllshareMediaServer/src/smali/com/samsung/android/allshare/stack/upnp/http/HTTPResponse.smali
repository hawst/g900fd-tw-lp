.class public Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
.super Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;
.source "HTTPResponse.java"


# instance fields
.field private mStatusCode:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->mStatusCode:I

    .line 36
    const-string v0, "text/html; charset=\"utf-8\""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContentType(Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPServer;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setServer(Ljava/lang/String;)V

    .line 38
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setContent(Ljava/lang/String;)V

    .line 41
    const-string v0, "Connection"

    const-string v1, "close"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)V
    .locals 1
    .param p1, "httpRes"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->mStatusCode:I

    .line 45
    invoke-virtual {p0, p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->set(Lcom/samsung/android/allshare/stack/upnp/http/HTTPPacket;)V

    .line 49
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getStatusCode()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setStatusCode(I)V

    .line 50
    return-void
.end method

.method public static buildHTTPResponse(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    .locals 4
    .param p0, "req"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 53
    new-instance v1, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;

    invoke-direct {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;-><init>()V

    .line 54
    .local v1, "resp":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    if-nez p0, :cond_1

    .line 55
    const/4 v1, 0x0

    .line 62
    .end local v1    # "resp":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    :cond_0
    :goto_0
    return-object v1

    .line 58
    .restart local v1    # "resp":Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;
    :cond_1
    const-string v2, "Accept-Language"

    invoke-virtual {p0, v2}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeader(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 59
    .local v0, "acceptLanguage":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 60
    const-string v2, "Content-Language"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 110
    .local v0, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getStatusLineString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getHeaderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public getHeaderString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 93
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 94
    .local v3, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getNHeaders()I

    move-result v2

    .line 95
    .local v2, "nHeaders":I
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 96
    invoke-virtual {p0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getHeader(I)Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;

    move-result-object v0

    .line 97
    .local v0, "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    if-eqz v0, :cond_0

    .line 98
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\r\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 95
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 100
    .end local v0    # "header":Lcom/samsung/android/allshare/stack/upnp/http/HTTPHeader;
    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public getStatusCode()I
    .locals 2

    .prologue
    .line 76
    iget v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->mStatusCode:I

    if-eqz v1, :cond_0

    .line 77
    iget v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->mStatusCode:I

    .line 79
    :goto_0
    return v1

    .line 78
    :cond_0
    new-instance v0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPStatus;

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getFirstLine()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPStatus;-><init>(Ljava/lang/String;)V

    .line 79
    .local v0, "httpStatus":Lcom/samsung/android/allshare/stack/upnp/http/HTTPStatus;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPStatus;->getStatusCode()I

    move-result v1

    goto :goto_0
.end method

.method public getStatusLineString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HTTP/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getVersion()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getStatusCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->mStatusCode:I

    invoke-static {v1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPStatus;->code2String(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isSuccessful()Z
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getStatusCode()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPStatus;->isSuccessful(I)Z

    move-result v0

    return v0
.end method

.method public print()V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method public setStatusCode(I)V
    .locals 0
    .param p1, "code"    # I

    .prologue
    .line 72
    iput p1, p0, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->mStatusCode:I

    .line 73
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 124
    .local v0, "str":Ljava/lang/StringBuffer;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getStatusLineString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 125
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getHeaderString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 126
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 127
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;->getContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 129
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
