.class public interface abstract Lcom/samsung/android/allshare/dms/DmsStreaming$IRequestReceivedListener;
.super Ljava/lang/Object;
.source "DmsStreaming.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/dms/DmsStreaming;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IRequestReceivedListener"
.end annotation


# virtual methods
.method public abstract onFileReceiveCompleted(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onFileReceiveFailed(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onFileReceiveProgressUpdated(Ljava/lang/String;Ljava/lang/String;JJ)V
.end method

.method public abstract onRequestReceived(Ljava/lang/String;)Lcom/samsung/android/allshare/dms/DMSItem;
.end method
