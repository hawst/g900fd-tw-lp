.class public Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotWithGPSActionHandler;
.super Ljava/lang/Object;
.source "ShotWithGPSActionHandler.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;


# instance fields
.field private mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;


# direct methods
.method public constructor <init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V
    .locals 1
    .param p1, "cameraListener"    # Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotWithGPSActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 28
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotWithGPSActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 29
    return-void
.end method


# virtual methods
.method public handleAction(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;Ljava/net/InetAddress;)Z
    .locals 12
    .param p1, "action"    # Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .param p2, "client"    # Ljava/net/InetAddress;

    .prologue
    const/16 v11, 0x192

    const/4 v8, 0x0

    .line 33
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getActionRequest()Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    move-result-object v9

    invoke-virtual {v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v2

    .line 34
    .local v2, "args":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    .line 37
    .local v1, "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getName()Ljava/lang/String;

    move-result-object v9

    const-string v10, "GPSINFO"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 38
    :cond_1
    invoke-virtual {p1, v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    .line 74
    .end local v1    # "arg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :goto_0
    return v8

    .line 43
    :cond_2
    const-string v9, "GPSINFO"

    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v4

    .line 44
    .local v4, "gpsInfoArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    const/4 v7, 0x0

    .line 45
    .local v7, "strAFShotResult":Ljava/lang/String;
    const/4 v6, 0x0

    .line 46
    .local v6, "nAvailShots":I
    if-eqz v4, :cond_3

    .line 47
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotWithGPSActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onShotWithGPSRequest_GetAFShotResult_Request(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 49
    iget-object v9, p0, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotWithGPSActionHandler;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->getValue()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onShotWithGPSRequest_GetAvailShots_Request(Ljava/lang/String;)I

    move-result v6

    .line 53
    :cond_3
    const/4 v9, -0x1

    if-ne v6, v9, :cond_4

    .line 54
    const/16 v9, 0x1f5

    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 58
    :cond_4
    const-string v9, "AFSHOTRESULT"

    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v0

    .line 59
    .local v0, "afShotResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v0, :cond_5

    .line 60
    invoke-virtual {v0, v7}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(Ljava/lang/String;)V

    .line 66
    const-string v9, "AVAILSHOTS"

    invoke-virtual {p1, v9}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->getArgument(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;

    move-result-object v3

    .line 67
    .local v3, "availShotsResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    if-eqz v3, :cond_6

    .line 68
    invoke-virtual {v3, v6}, Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;->setValue(I)V

    .line 74
    const/4 v8, 0x1

    goto :goto_0

    .line 62
    .end local v3    # "availShotsResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :cond_5
    invoke-virtual {p1, v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0

    .line 70
    .restart local v3    # "availShotsResultArg":Lcom/samsung/android/allshare/stack/upnp/upnp/Argument;
    :cond_6
    invoke-virtual {p1, v11}, Lcom/samsung/android/allshare/stack/upnp/upnp/Action;->setStatus(I)V

    goto :goto_0
.end method
