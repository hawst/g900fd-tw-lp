.class public Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;
.super Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;
.source "CameraMediaServer.java"

# interfaces
.implements Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$3;,
        Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$ROTATION_TYPE;
    }
.end annotation


# static fields
.field private static final CHECK_CLIENT_STATUS:I = 0x7d1

.field private static final CLIENT_CHECK_DELAY_TIME:I = 0x3e8

.field private static final PING_TIME_OUT:I = 0x3e8

.field private static final ROTATION_DELAY_TIME:I = 0x3e8

.field private static final SEND_ROTATION_INFO:I = 0x3e9

.field private static final TAG:Ljava/lang/String; = "CameraMediaServer"


# instance fields
.field private CLIENT_MAX_CHECK_COUNT:I

.field private PING_THREAD_NAME:Ljava/lang/String;

.field private ROTATION_THREAD_NAME:Ljava/lang/String;

.field private mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

.field private mCameraMimeType:Ljava/lang/String;

.field private mCameraProtocolInfo:Ljava/lang/String;

.field private mClientInetAddr:Ljava/net/InetAddress;

.field private mFileDirectory:Ljava/lang/String;

.field private mPingHandler:Landroid/os/Handler;

.field private mPingThread:Landroid/os/HandlerThread;

.field private mRotationHandler:Landroid/os/Handler;

.field private mRotationThread:Landroid/os/HandlerThread;

.field private mRotationType:Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$ROTATION_TYPE;

.field private volatile mServerRunning:Z

.field private mStarted:Z

.field private mUrlDirectory:Ljava/lang/String;

.field private nPingFailedCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;-><init>(Landroid/content/Context;)V

    .line 67
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 69
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationType:Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$ROTATION_TYPE;

    .line 71
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mStarted:Z

    .line 73
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mUrlDirectory:Ljava/lang/String;

    .line 75
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mFileDirectory:Ljava/lang/String;

    .line 77
    const-string v0, "image/jpeg"

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraMimeType:Ljava/lang/String;

    .line 79
    const-string v0, "protocol_info: http-get:*:image/jpeg:DLNA.ORG_PN=JPEG_LRG;DLNA.ORG_OP=01;DLNA.ORG_CI=0"

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraProtocolInfo:Ljava/lang/String;

    .line 81
    iput-boolean v2, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    .line 87
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationThread:Landroid/os/HandlerThread;

    .line 89
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    .line 91
    const-string v0, "Rotation Noti Thread"

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->ROTATION_THREAD_NAME:Ljava/lang/String;

    .line 93
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    .line 95
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingHandler:Landroid/os/Handler;

    .line 97
    const-string v0, "Ping Check Thread"

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->PING_THREAD_NAME:Ljava/lang/String;

    .line 105
    iput-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mClientInetAddr:Ljava/net/InetAddress;

    .line 107
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->CLIENT_MAX_CHECK_COUNT:I

    .line 109
    iput v2, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->nPingFailedCount:I

    .line 117
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->handleRotationMessage(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->handlePingMessage(Landroid/os/Message;)V

    return-void
.end method

.method private announceCameraInfo()V
    .locals 9

    .prologue
    .line 385
    invoke-static {}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->notifyWait()V

    .line 387
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAllAddress()Ljava/util/Collection;

    move-result-object v1

    .line 389
    .local v1, "addressList":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/net/InetAddress;>;"
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getSSDPAnnounceCount()I

    move-result v5

    .line 390
    .local v5, "ssdpCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_2

    .line 391
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InetAddress;

    .line 392
    .local v0, "addr":Ljava/net/InetAddress;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_0

    .line 394
    invoke-direct {p0, v0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->announceCameraInfo(Ljava/net/InetAddress;)V

    goto :goto_1

    .line 397
    .end local v0    # "addr":Ljava/net/InetAddress;
    :cond_1
    const-wide/16 v6, 0xc8

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 398
    :catch_0
    move-exception v2

    .line 399
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v6, "CameraMediaServer"

    const-string v7, "announceCameraInfo"

    const-string v8, ""

    invoke-static {v6, v7, v8, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    .line 402
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void
.end method

.method private announceCameraInfo(Ljava/net/InetAddress;)V
    .locals 25
    .param p1, "bindAddr"    # Ljava/net/InetAddress;

    .prologue
    .line 302
    const/16 v22, 0x0

    move/from16 v0, v22

    new-array v7, v0, [Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    .line 303
    .local v7, "discoveryInfoList":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mSocketFactory:Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SocketFactory;->getDiscoveryInfo()[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;

    move-result-object v7

    .line 305
    if-nez v7, :cond_1

    .line 306
    const-string v22, "CameraMediaServer"

    const-string v23, "announceCameraInfo"

    const-string v24, "DiscoveryInfo is not set correctly"

    invoke-static/range {v22 .. v24}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    move-object v4, v7

    .local v4, "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    array-length v14, v4

    .local v14, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    move v11, v9

    .end local v4    # "arr$":[Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    .end local v9    # "i$":I
    .end local v14    # "len$":I
    .local v11, "i$":I
    :goto_1
    if-ge v11, v14, :cond_0

    aget-object v12, v4, v11

    .line 311
    .local v12, "info":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;
    const/16 v17, 0x0

    .line 312
    .local v17, "ni":Ljava/net/NetworkInterface;
    const/16 v19, 0x0

    .line 314
    .local v19, "pairs":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;>;"
    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getNetworkInterfaceName()Ljava/lang/String;

    move-result-object v16

    .line 315
    .local v16, "name":Ljava/lang/String;
    const-string v22, "*"

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 316
    invoke-static {}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAllAddressNetworkInterfacePair()Ljava/util/Collection;

    move-result-object v19

    .line 327
    :goto_2
    invoke-interface/range {v19 .. v19}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .end local v11    # "i$":I
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/util/Pair;

    .line 329
    .local v18, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/net/InetAddress;

    invoke-virtual/range {v22 .. v22}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v22

    if-eqz v22, :cond_3

    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->supportLoopback()Z

    move-result v22

    if-eqz v22, :cond_2

    .line 332
    :cond_3
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v22, v0

    check-cast v22, Ljava/net/InetAddress;

    invoke-virtual/range {v22 .. v22}, Ljava/net/InetAddress;->isLinkLocalAddress()Z

    move-result v22

    if-eqz v22, :cond_4

    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->supportLinkLocal()Z

    move-result v22

    if-eqz v22, :cond_2

    .line 335
    :cond_4
    invoke-virtual {v12}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/DiscoveryInfo;->getMulticastAddressList()[Ljava/lang/String;

    move-result-object v5

    .local v5, "arr$":[Ljava/lang/String;
    array-length v15, v5

    .local v15, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_3
    if-ge v10, v15, :cond_2

    aget-object v13, v5, v10

    .line 336
    .local v13, "joinAddr":Ljava/lang/String;
    invoke-virtual/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getLocationURL(Ljava/net/InetAddress;)Ljava/lang/String;

    move-result-object v6

    .line 338
    .local v6, "devLocation":Ljava/lang/String;
    const/16 v22, 0x76c

    move/from16 v0, v22

    move-object/from16 v1, p1

    invoke-static {v13, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->createSSDPNotifySocket(Ljava/lang/String;ILjava/net/InetAddress;)Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;

    move-result-object v21

    .line 340
    .local v21, "ssdpSock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    if-nez v21, :cond_7

    .line 335
    :goto_4
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    .line 319
    .end local v5    # "arr$":[Ljava/lang/String;
    .end local v6    # "devLocation":Ljava/lang/String;
    .end local v10    # "i$":I
    .end local v13    # "joinAddr":Ljava/lang/String;
    .end local v15    # "len$":I
    .end local v18    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    .end local v21    # "ssdpSock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    .restart local v11    # "i$":I
    :cond_5
    :try_start_0
    invoke-static/range {v16 .. v16}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v17

    .line 323
    invoke-static/range {v17 .. v17}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getAddressByNIC(Ljava/net/NetworkInterface;)Ljava/util/Collection;

    move-result-object v19

    goto :goto_2

    .line 320
    :catch_0
    move-exception v8

    .line 310
    .end local v11    # "i$":I
    :cond_6
    add-int/lit8 v9, v11, 0x1

    .restart local v9    # "i$":I
    move v11, v9

    .end local v9    # "i$":I
    .restart local v11    # "i$":I
    goto :goto_1

    .line 343
    .end local v11    # "i$":I
    .restart local v5    # "arr$":[Ljava/lang/String;
    .restart local v6    # "devLocation":Ljava/lang/String;
    .restart local v10    # "i$":I
    .restart local v13    # "joinAddr":Ljava/lang/String;
    .restart local v15    # "len$":I
    .restart local v18    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/net/InetAddress;Ljava/net/NetworkInterface;>;"
    .restart local v21    # "ssdpSock":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;
    :cond_7
    new-instance v20, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;

    const/16 v22, 0x76c

    const/16 v23, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-direct {v0, v13, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;-><init>(Ljava/lang/String;IZ)V

    .line 348
    .local v20, "ssdpReq":Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;
    const-string v22, "urn:schemas-upnp-org:service:ConnectionManager:1"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNT(Ljava/lang/String;)V

    .line 350
    sget-object v22, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$3;->$SwitchMap$com$samsung$android$allshare$stack$smartcamera$CameraMediaServer$ROTATION_TYPE:[I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationType:Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$ROTATION_TYPE;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$ROTATION_TYPE;->ordinal()I

    move-result v23

    aget v22, v22, v23

    packed-switch v22, :pswitch_data_0

    goto/16 :goto_0

    .line 352
    :pswitch_0
    const-string v22, "ssdp:rotationL"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    .line 368
    :goto_5
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getUDN()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "::urn:schemas-upnp-org:service:ConnectionManager:1"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setUSN(Ljava/lang/String;)V

    .line 369
    const-wide/16 v22, 0x0

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setContentLength(J)V

    .line 371
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->post(Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;)Z

    .line 372
    invoke-virtual/range {v21 .. v21}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/NotifySendSocket;->close()Z

    goto :goto_4

    .line 355
    :pswitch_1
    const-string v22, "ssdp:rotationR"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    goto :goto_5

    .line 358
    :pswitch_2
    const-string v22, "ssdp:rotationU"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    goto :goto_5

    .line 361
    :pswitch_3
    const-string v22, "ssdp:rotationD"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/ssdp/SSDPNotifyRequest;->setNTS(Ljava/lang/String;)V

    goto :goto_5

    .line 350
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getIpAddress()Ljava/lang/String;
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 406
    :try_start_0
    const-string v7, "wlan0"

    invoke-static {v7}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v5

    .line 408
    .local v5, "ni":Ljava/net/NetworkInterface;
    if-nez v5, :cond_1

    .line 409
    const-string v7, "eth0"

    invoke-static {v7}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v5

    .line 410
    if-nez v5, :cond_1

    .line 411
    const-string v7, "CameraMediaServer"

    const-string v8, "getIpAddress"

    const-string v9, "NetworkInterface found"

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_0
    :goto_0
    return-object v6

    .line 416
    :cond_1
    invoke-virtual {v5}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v1

    .line 417
    .local v1, "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InterfaceAddress;

    .line 418
    .local v0, "addr":Ljava/net/InterfaceAddress;
    invoke-virtual {v0}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v4

    .line 419
    .local v4, "iAddr":Ljava/net/InetAddress;
    if-eqz v4, :cond_2

    .line 422
    invoke-virtual {v4}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v7

    const-string v8, "wlan0"

    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 423
    :cond_3
    const-string v7, "CameraMediaServer"

    const-string v8, "getIpAddress"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getIpAddress: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    invoke-virtual {v4}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    goto :goto_0

    .line 427
    .end local v0    # "addr":Ljava/net/InterfaceAddress;
    .end local v1    # "addrs":Ljava/util/List;, "Ljava/util/List<Ljava/net/InterfaceAddress;>;"
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "iAddr":Ljava/net/InetAddress;
    :catch_0
    move-exception v2

    .line 428
    .local v2, "e":Ljava/lang/Exception;
    const-string v7, "CameraMediaServer"

    const-string v8, "getIpAddress"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getIpAddress Exception : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Lcom/samsung/android/allshare/stack/util/DLog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handlePingMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 231
    iget-boolean v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    if-nez v1, :cond_1

    .line 232
    const-string v1, "CameraMediaServer"

    const-string v2, "handlePingMessage"

    const-string v3, "mServerRunning is false"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 239
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mClientInetAddr:Ljava/net/InetAddress;

    if-eqz v1, :cond_4

    .line 241
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mClientInetAddr:Ljava/net/InetAddress;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Ljava/net/InetAddress;->isReachable(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 242
    iget v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->nPingFailedCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->nPingFailedCount:I

    .line 243
    const-string v1, "CameraMediaServer"

    const-string v2, "handlePingMessage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isReachable failed - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->nPingFailedCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    iget v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->nPingFailedCount:I

    iget v2, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->CLIENT_MAX_CHECK_COUNT:I

    if-lt v1, v2, :cond_2

    .line 247
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v1}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onRvfSessionClosed()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 259
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "CameraMediaServer"

    const-string v2, "handlePingMessage"

    const-string v3, "IOException is occurred"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    if-eqz v1, :cond_0

    .line 263
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v1}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onRvfSessionClosed()V

    goto :goto_0

    .line 250
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingHandler:Landroid/os/Handler;

    const/16 v2, 0x7d1

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 254
    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->nPingFailedCount:I

    .line 255
    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingHandler:Landroid/os/Handler;

    const/16 v2, 0x7d1

    const-wide/16 v4, 0x3e8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 266
    :cond_4
    const-string v1, "CameraMediaServer"

    const-string v2, "handlePingMessage"

    const-string v3, "mClientInetAddr is null"

    invoke-static {v1, v2, v3}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x7d1
        :pswitch_0
    .end packed-switch
.end method

.method private handleRotationMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    if-nez v0, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-void

    .line 189
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 191
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->announceCameraInfo()V

    .line 193
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 189
    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V
    .locals 4
    .param p1, "request"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;

    .prologue
    .line 277
    invoke-super {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->acceptConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 279
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetInetAddr()Ljava/net/InetAddress;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    .line 281
    const-string v0, "CameraMediaServer"

    const-string v1, "acceptConnectionRequest"

    const-string v2, "start ping check thread"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    new-instance v0, Landroid/os/HandlerThread;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->PING_THREAD_NAME:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    .line 283
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 285
    new-instance v0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$2;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$2;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingHandler:Landroid/os/Handler;

    .line 291
    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;->getTargetInetAddr()Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mClientInetAddr:Ljava/net/InetAddress;

    .line 292
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->nPingFailedCount:I

    .line 293
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingHandler:Landroid/os/Handler;

    const/16 v1, 0x7d1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 297
    :cond_0
    return-void
.end method

.method public doesRvfSessionExist()Z
    .locals 1

    .prologue
    .line 627
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/dms/DmsStreaming;->doesRvfSessionExist()Z

    move-result v0

    return v0
.end method

.method protected getDMSItem(Ljava/lang/String;)Lcom/samsung/android/allshare/dms/DMSItem;
    .locals 8
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 551
    if-nez p1, :cond_0

    .line 552
    const/4 v2, 0x0

    .line 574
    :goto_0
    return-object v2

    .line 554
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mUrlDirectory:Ljava/lang/String;

    .line 556
    .local v3, "urlDirectory":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 557
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mUrlDirectory:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    add-int v1, v4, v5

    .line 558
    .local v1, "fileNameIndex":I
    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 559
    .local v0, "fileName":Ljava/lang/String;
    const-string v4, "_SM"

    invoke-virtual {v0, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 560
    const/4 v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x3

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 562
    :cond_1
    new-instance v2, Lcom/samsung/android/allshare/dms/DMSItem;

    invoke-direct {v2}, Lcom/samsung/android/allshare/dms/DMSItem;-><init>()V

    .line 563
    .local v2, "mDMSItem":Lcom/samsung/android/allshare/dms/DMSItem;
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraMimeType:Ljava/lang/String;

    iput-object v4, v2, Lcom/samsung/android/allshare/dms/DMSItem;->mime_type:Ljava/lang/String;

    .line 564
    iget-object v4, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraProtocolInfo:Ljava/lang/String;

    iput-object v4, v2, Lcom/samsung/android/allshare/dms/DMSItem;->protocol_info:Ljava/lang/String;

    .line 565
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mFileDirectory:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/samsung/android/allshare/dms/DMSItem;->file_path:Ljava/lang/String;

    .line 567
    const-string v4, "CameraMediaServer"

    const-string v5, "getDMSItem"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mime: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/samsung/android/allshare/dms/DMSItem;->mime_type:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 568
    const-string v4, "CameraMediaServer"

    const-string v5, "getDMSItem"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "protocol_info: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/samsung/android/allshare/dms/DMSItem;->protocol_info:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    const-string v4, "CameraMediaServer"

    const-string v5, "getDMSItem"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "file path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/samsung/android/allshare/dms/DMSItem;->file_path:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const-string v4, "CameraMediaServer"

    const-string v5, "getDMSItem"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "subtitle path: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/samsung/android/allshare/dms/DMSItem;->subtitle_path:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 574
    .end local v0    # "fileName":Ljava/lang/String;
    .end local v1    # "fileNameIndex":I
    .end local v2    # "mDMSItem":Lcom/samsung/android/allshare/dms/DMSItem;
    :cond_2
    invoke-super {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getDMSItem(Ljava/lang/String;)Lcom/samsung/android/allshare/dms/DMSItem;

    move-result-object v2

    goto/16 :goto_0
.end method

.method protected handleSoapAction(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V
    .locals 7
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;
    .param p2, "serviceType"    # Ljava/lang/String;
    .param p3, "listener"    # Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;

    .prologue
    .line 522
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isPostRequest()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isSOAPAction()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 523
    new-instance v3, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;

    invoke-direct {v3, p1}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;-><init>(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    .line 524
    .local v3, "req":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getActionName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getAction(Ljava/lang/String;)Lcom/samsung/android/allshare/stack/upnp/upnp/Action;

    move-result-object v0

    .line 525
    .local v0, "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    if-eqz v0, :cond_1

    .line 526
    new-instance v1, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;

    invoke-direct {v1, v0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;-><init>(Lcom/samsung/android/allshare/stack/upnp/upnp/Action;)V

    .line 527
    .local v1, "actionCamera":Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;
    invoke-virtual {v1}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v2

    .line 528
    .local v2, "originalArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getArgumentList()Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;

    move-result-object v4

    .line 529
    .local v4, "reqArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    invoke-virtual {v2, v4}, Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;->setArgumentList(Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;)V

    .line 530
    invoke-virtual {v1, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;->performActionListener(Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;)Z

    .line 537
    .end local v0    # "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .end local v1    # "actionCamera":Lcom/samsung/android/allshare/stack/smartcamera/upnp/ActionCamera;
    .end local v2    # "originalArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    .end local v3    # "req":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    .end local v4    # "reqArgList":Lcom/samsung/android/allshare/stack/upnp/upnp/ArgumentList;
    :cond_0
    :goto_0
    return-void

    .line 533
    .restart local v0    # "action":Lcom/samsung/android/allshare/stack/upnp/upnp/Action;
    .restart local v3    # "req":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;
    :cond_1
    new-instance v5, Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;

    invoke-virtual {v3}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->getActionName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6, p2}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/InvalidActionResponse;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    .local v5, "res":Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionResponse;
    invoke-virtual {v3, v5}, Lcom/samsung/android/allshare/stack/upnp/upnp/control/ActionRequest;->post(Lcom/samsung/android/allshare/stack/upnp/http/HTTPResponse;)Z

    goto :goto_0
.end method

.method public httpRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V
    .locals 17
    .param p1, "httpReq"    # Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;

    .prologue
    .line 435
    if-nez p1, :cond_1

    .line 516
    :cond_0
    :goto_0
    return-void

    .line 438
    :cond_1
    const/4 v11, 0x0

    .line 439
    .local v11, "uri":Landroid/net/Uri;
    const-string v12, ""

    .line 442
    .local v12, "uriStr":Ljava/lang/String;
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getURI()Ljava/lang/String;

    move-result-object v12

    .line 443
    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 449
    const-string v13, "CameraMediaServer"

    const-string v14, "httpRequestRecieved"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "httpRequestRecieved: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v11}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    invoke-static {v11}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServerUris;->match(Landroid/net/Uri;)I

    move-result v10

    .line 454
    .local v10, "match":I
    sparse-switch v10, :sswitch_data_0

    .line 482
    const/4 v9, 0x1

    .line 484
    .local v9, "isAllow":Z
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isGetRequest()Z

    move-result v13

    if-nez v13, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isHeadRequest()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 485
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mConnectionListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;

    if-eqz v13, :cond_6

    const-string v13, "description.xml"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 487
    const-string v13, "User-Agent"

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getHeaderValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 489
    .local v4, "userAgent":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getRequestAddress()Ljava/net/InetAddress;

    move-result-object v7

    .line 490
    .local v7, "addr":Ljava/net/InetAddress;
    if-nez v7, :cond_5

    .line 491
    const-string v13, "CameraMediaServer"

    const-string v14, "httpRequestRecieved"

    const-string v15, "InetAddress is null!"

    invoke-static {v13, v14, v15}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 444
    .end local v4    # "userAgent":Ljava/lang/String;
    .end local v7    # "addr":Ljava/net/InetAddress;
    .end local v9    # "isAllow":Z
    .end local v10    # "match":I
    :catch_0
    move-exception v8

    .line 446
    .local v8, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 457
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v10    # "match":I
    :sswitch_0
    const-string v13, "urn:schemas-upnp-org:service:ContentDirectory"

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13, v14}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->handleSoapAction(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    goto :goto_0

    .line 462
    :sswitch_1
    const-string v13, "urn:schemas-upnp-org:service:ConnectionManager:1"

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getConnectionManager()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ConnectionManagerService;

    move-result-object v14

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13, v14}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->handleSoapAction(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Ljava/lang/String;Lcom/samsung/android/allshare/stack/upnp/upnp/control/IActionListener;)V

    goto/16 :goto_0

    .line 468
    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isGetRequest()Z

    move-result v13

    if-nez v13, :cond_3

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isHeadRequest()Z

    move-result v13

    if-eqz v13, :cond_4

    .line 469
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v11}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->sendContent(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 470
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->isPostRequest()Z

    move-result v13

    if-eqz v13, :cond_0

    .line 471
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v11}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->receiveContent(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 476
    :sswitch_3
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v11}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->sendIcon(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 495
    .restart local v4    # "userAgent":Ljava/lang/String;
    .restart local v7    # "addr":Ljava/net/InetAddress;
    .restart local v9    # "isAllow":Z
    :cond_5
    invoke-virtual {v7}, Ljava/net/InetAddress;->toString()Ljava/lang/String;

    move-result-object v5

    .line 497
    .local v5, "host":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->getTargetHostPort()I

    move-result v6

    .line 499
    .local v6, "port":I
    invoke-static {v5}, Lcom/samsung/android/allshare/stack/upnp/util/NetworkUtils;->getMacAddrFromArpTable(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 500
    .local v3, "macAddress":Ljava/lang/String;
    if-eqz v3, :cond_7

    .line 502
    :goto_1
    new-instance v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/net/InetAddress;)V

    .line 504
    .local v2, "req":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mConnectionListener:Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;

    invoke-interface {v13, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener;->onConnectionRequest(Lcom/samsung/android/allshare/stack/mediaserver/upnp/IConnectionListener$ConnectionRequest;)V

    .line 505
    const-wide/16 v14, 0x7530

    invoke-virtual {v2, v14, v15}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->waitEvent(J)V

    .line 507
    iget-boolean v9, v2, Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;->mAllowed:Z

    .line 511
    .end local v2    # "req":Lcom/samsung/android/allshare/stack/mediaserver/upnp/MediaServer$DefaultConnectionRequest;
    .end local v3    # "macAddress":Ljava/lang/String;
    .end local v4    # "userAgent":Ljava/lang/String;
    .end local v5    # "host":Ljava/lang/String;
    .end local v6    # "port":I
    .end local v7    # "addr":Ljava/net/InetAddress;
    :cond_6
    if-eqz v9, :cond_8

    .line 512
    invoke-super/range {p0 .. p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->httpRequestRecieved(Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;)V

    goto/16 :goto_0

    .line 500
    .restart local v3    # "macAddress":Ljava/lang/String;
    .restart local v4    # "userAgent":Ljava/lang/String;
    .restart local v5    # "host":Ljava/lang/String;
    .restart local v6    # "port":I
    .restart local v7    # "addr":Ljava/net/InetAddress;
    :cond_7
    const-string v3, ""

    goto :goto_1

    .line 514
    .end local v3    # "macAddress":Ljava/lang/String;
    .end local v4    # "userAgent":Ljava/lang/String;
    .end local v5    # "host":Ljava/lang/String;
    .end local v6    # "port":I
    .end local v7    # "addr":Ljava/net/InetAddress;
    :cond_8
    const/16 v13, 0x193

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lcom/samsung/android/allshare/stack/upnp/http/HTTPRequest;->returnResponse(I)Z

    goto/16 :goto_0

    .line 454
    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_3
        0xc9 -> :sswitch_2
        0x12d -> :sswitch_0
        0x12e -> :sswitch_1
    .end sparse-switch
.end method

.method public initailize(IIILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "descRes"    # I
    .param p2, "cdsRes"    # I
    .param p3, "cmsRes"    # I
    .param p4, "friendlyName"    # Ljava/lang/String;
    .param p5, "udn"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/samsung/android/allshare/stack/upnp/upnp/device/InvalidDescriptionException;
        }
    .end annotation

    .prologue
    .line 146
    invoke-super/range {p0 .. p5}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->initailize(IIILjava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "GetIP"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetIPActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetIPActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 150
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "GetInfomation"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetInfomationActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetInfomationActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 152
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "SetResolution"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/SetResolutionActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/SetResolutionActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 154
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "ZoomIN"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ZoomInActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ZoomInActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 156
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "ZoomOUT"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ZoomOutActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ZoomOutActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 158
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "MULTIAF"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/MulltiAFActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/MulltiAFActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 160
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "AF"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/AFActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/AFActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 162
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "AFRELEASE"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/AFReleaseActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/AFReleaseActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 164
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "Shot"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 166
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "ShotWithGPS"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotWithGPSActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShotWithGPSActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 168
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "SetLED"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/SetLedActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/SetLedActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 170
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "SetFlash"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/SetFlashActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/SetFlashActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 172
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "SetZoom"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/SetZoomActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/SetZoomActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 174
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "GetZoom"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetZoomActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/GetZoomActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 176
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "ReleaseSelfTimer"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ReleaseSelfTimerActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ReleaseSelfTimerActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 178
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getContentDirectory()Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;

    move-result-object v0

    const-string v1, "ShutterUp"

    new-instance v2, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShutterUpActionHandler;

    iget-object v3, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-direct {v2, v3}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/action/ShutterUpActionHandler;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/ContentDirectoryService;->putActionHandler(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/action/IActionHandler;)V

    .line 181
    const-string v0, "/RVFIMG/"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/Android/data/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->setImageUrlDirectory(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    return-void
.end method

.method public onRvfSessionClosed()V
    .locals 3

    .prologue
    .line 639
    const-string v0, "CameraMediaServer"

    const-string v1, "onRvfSessionClosed"

    const-string v2, " onRvfSessionClosed"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onRvfSessionClosed()V

    .line 642
    :cond_0
    return-void
.end method

.method public onRvfSessionCreated()V
    .locals 3

    .prologue
    .line 632
    const-string v0, "CameraMediaServer"

    const-string v1, "onRvfSessionCreated"

    const-string v2, " onRvfSessionCreated"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-interface {v0}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onRvfSessionCreated()V

    .line 635
    :cond_0
    return-void
.end method

.method public onStartRecording()V
    .locals 3

    .prologue
    .line 645
    const-string v0, "CameraMediaServer"

    const-string v1, "onStartRecording"

    const-string v2, " onStartRecording"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 646
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/dms/DmsStreaming;->onStartRecording()V

    .line 647
    return-void
.end method

.method public onStopRecording()V
    .locals 3

    .prologue
    .line 650
    const-string v0, "CameraMediaServer"

    const-string v1, "onStopRecording"

    const-string v2, " onStopRecording"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/dms/DmsStreaming;->onStopRecording()V

    .line 652
    return-void
.end method

.method public setCameraListener(Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    .line 121
    return-void
.end method

.method public setImageUrlDirectory(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "urlDirectory"    # Ljava/lang/String;
    .param p2, "fileDirectory"    # Ljava/lang/String;

    .prologue
    .line 540
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mUrlDirectory:Ljava/lang/String;

    .line 541
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mUrlDirectory:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mUrlDirectory:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mUrlDirectory:Ljava/lang/String;

    .line 544
    :cond_0
    iput-object p2, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mFileDirectory:Ljava/lang/String;

    .line 545
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mFileDirectory:Ljava/lang/String;

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 546
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mFileDirectory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mFileDirectory:Ljava/lang/String;

    .line 547
    :cond_1
    return-void
.end method

.method public setRotationInfo(Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$ROTATION_TYPE;)V
    .locals 5
    .param p1, "rotationType"    # Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$ROTATION_TYPE;

    .prologue
    const/16 v4, 0x3e9

    .line 124
    const-string v0, "CameraMediaServer"

    const-string v1, "setRotationInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "rotationInfo: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationType:Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$ROTATION_TYPE;

    .line 126
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 128
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 130
    :cond_0
    return-void
.end method

.method public start()Z
    .locals 2

    .prologue
    .line 206
    invoke-super {p0}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->start()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mStarted:Z

    .line 208
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    if-nez v0, :cond_1

    .line 209
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mStarted:Z

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    .line 211
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/android/allshare/dms/DmsStreaming;->startRvfSessionListener(Lcom/samsung/android/allshare/dms/DmsStreaming$IRvfSessionListener;)V

    .line 212
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mCameraListener:Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;

    invoke-static {}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->getIpAddress()Ljava/lang/String;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->getStreamingPort(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/samsung/android/allshare/stack/smartcamera/upnp/ICameraListener;->onStreamingServerStarted(I)V

    .line 216
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->ROTATION_THREAD_NAME:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationThread:Landroid/os/HandlerThread;

    .line 217
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 219
    new-instance v0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$1;

    iget-object v1, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer$1;-><init>(Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    .line 226
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mStarted:Z

    return v0
.end method

.method public stop(Z)Z
    .locals 2
    .param p1, "doByeBye"    # Z

    .prologue
    .line 580
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 581
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    .line 583
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/dms/DmsStreaming;->stopRvfSessionListener()V

    .line 585
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 586
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 587
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    .line 588
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 590
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 591
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingHandler:Landroid/os/Handler;

    const/16 v1, 0x7d1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 592
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_3

    .line 593
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 594
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    .line 599
    :cond_3
    invoke-super {p0, p1}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->stop(Z)Z

    move-result v0

    return v0
.end method

.method public stop(ZZ)Z
    .locals 2
    .param p1, "doByeBye"    # Z
    .param p2, "removeCache"    # Z

    .prologue
    .line 604
    iget-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 605
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mServerRunning:Z

    .line 607
    invoke-static {}, Lcom/samsung/android/allshare/dms/DmsStreaming;->getInstance()Lcom/samsung/android/allshare/dms/DmsStreaming;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/allshare/dms/DmsStreaming;->stopRvfSessionListener()V

    .line 609
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationHandler:Landroid/os/Handler;

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 611
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    .line 612
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mRotationThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 614
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    .line 615
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingHandler:Landroid/os/Handler;

    const/16 v1, 0x7d1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 616
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    if-eqz v0, :cond_3

    .line 617
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 618
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/smartcamera/CameraMediaServer;->mPingThread:Landroid/os/HandlerThread;

    .line 623
    :cond_3
    invoke-super {p0, p1, p2}, Lcom/samsung/android/allshare/stack/mediaserver/mediastore/MediaStoreMediaServer;->stop(ZZ)Z

    move-result v0

    return v0
.end method
