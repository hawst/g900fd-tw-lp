.class public Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$DefaultImportResourcePropertyFactory;
.super Ljava/lang/Object;
.source "ImportResourceProperty.java"

# interfaces
.implements Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty$IImportResourceFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DefaultImportResourcePropertyFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    .locals 9
    .param p1, "path"    # Ljava/io/File;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "parent"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .param p4, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .param p5, "tempFile"    # Ljava/io/File;

    .prologue
    .line 122
    const-string v2, ""

    .line 123
    .local v2, "protocol":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 124
    const-string v5, "protocolInfo"

    invoke-virtual {p4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 126
    :cond_0
    const/4 v0, 0x0

    .line 127
    .local v0, "destFile":Ljava/io/File;
    invoke-virtual {p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->unEscapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 129
    .local v4, "title":Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 130
    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->PARSER:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->access$000()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->createDestFileName(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    invoke-static {p1, v4, v5}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->access$100(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 132
    :cond_1
    if-nez v0, :cond_2

    .line 133
    const/4 v3, 0x0

    .line 155
    :goto_0
    return-object v3

    .line 137
    :cond_2
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :goto_1
    :try_start_1
    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->copyFile(Ljava/io/File;Ljava/io/File;)V
    invoke-static {p5, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->access$200(Ljava/io/File;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 152
    :goto_2
    invoke-virtual {p5}, Ljava/io/File;->delete()Z

    .line 154
    new-instance v3, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;

    invoke-direct {v3, p2, v2, v0}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 155
    .local v3, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    goto :goto_0

    .line 138
    .end local v3    # "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    :catch_0
    move-exception v1

    .line 139
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "ImportResourceProperty"

    const-string v6, "create"

    const-string v7, "fail to destFile.delete()"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 148
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    .line 149
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "ImportResourceProperty"

    const-string v6, "create"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "IOException on ImportResourceProperty create"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public create(Ljava/io/File;Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/InputStream;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    .locals 14
    .param p1, "path"    # Ljava/io/File;
    .param p2, "uri"    # Ljava/lang/String;
    .param p3, "parent"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .param p4, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .param p5, "is"    # Ljava/io/InputStream;

    .prologue
    .line 78
    const-string v6, ""

    .line 79
    .local v6, "protocol":Ljava/lang/String;
    if-eqz p4, :cond_0

    .line 80
    const-string v9, "protocolInfo"

    move-object/from16 v0, p4

    invoke-virtual {v0, v9}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 81
    :cond_0
    invoke-virtual/range {p3 .. p3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getTitle()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->unEscapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->PARSER:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->access$000()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v10

    invoke-virtual {v10, v6}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->createDestFileName(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    invoke-static {p1, v9, v10}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->access$100(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 84
    .local v2, "destFile":Ljava/io/File;
    const/4 v4, 0x0

    .line 86
    .local v4, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .local v5, "fos":Ljava/io/FileOutputStream;
    const/4 v7, 0x0

    .line 88
    .local v7, "read":I
    const v9, 0x4b000

    :try_start_1
    new-array v1, v9, [B

    .line 89
    .local v1, "buffer":[B
    if-eqz p5, :cond_3

    .line 90
    :goto_0
    move-object/from16 v0, p5

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v7

    const/4 v9, -0x1

    if-eq v7, v9, :cond_3

    .line 91
    const/4 v9, 0x0

    invoke-virtual {v5, v1, v9, v7}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_a
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 93
    .end local v1    # "buffer":[B
    :catch_0
    move-exception v3

    move-object v4, v5

    .line 94
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .end local v7    # "read":I
    .local v3, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :goto_1
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 95
    const/4 v8, 0x0

    .line 101
    if-eqz p5, :cond_1

    .line 102
    :try_start_3
    invoke-virtual/range {p5 .. p5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 107
    .end local v3    # "e":Ljava/io/FileNotFoundException;
    :cond_1
    :goto_2
    if-eqz v4, :cond_2

    .line 108
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 115
    :cond_2
    :goto_3
    return-object v8

    .line 101
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "buffer":[B
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "read":I
    :cond_3
    if-eqz p5, :cond_4

    .line 102
    :try_start_5
    invoke-virtual/range {p5 .. p5}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 107
    :cond_4
    :goto_4
    if-eqz v5, :cond_5

    .line 108
    :try_start_6
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 114
    :cond_5
    :goto_5
    new-instance v8, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;

    move-object/from16 v0, p2

    invoke-direct {v8, v0, v6, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .local v8, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    move-object v4, v5

    .line 115
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_3

    .line 103
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .end local v8    # "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v3

    .line 104
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 109
    .end local v3    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v3

    .line 110
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v9, "ImportResourceProperty"

    const-string v10, "create"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IOException on ImportResourceProperty"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    .line 103
    .end local v1    # "buffer":[B
    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .end local v7    # "read":I
    .local v3, "e":Ljava/io/FileNotFoundException;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v3

    .line 104
    .local v3, "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 109
    .end local v3    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v3

    .line 110
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v9, "ImportResourceProperty"

    const-string v10, "create"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IOException on ImportResourceProperty"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 96
    .end local v3    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v3

    .line 97
    .restart local v3    # "e":Ljava/io/IOException;
    :goto_6
    :try_start_7
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 98
    const/4 v8, 0x0

    .line 101
    if-eqz p5, :cond_6

    .line 102
    :try_start_8
    invoke-virtual/range {p5 .. p5}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 107
    :cond_6
    :goto_7
    if-eqz v4, :cond_2

    .line 108
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto :goto_3

    .line 109
    :catch_6
    move-exception v3

    .line 110
    const-string v9, "ImportResourceProperty"

    const-string v10, "create"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IOException on ImportResourceProperty"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v10, v11}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 103
    :catch_7
    move-exception v3

    .line 104
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 100
    .end local v3    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    .line 101
    :goto_8
    if-eqz p5, :cond_7

    .line 102
    :try_start_a
    invoke-virtual/range {p5 .. p5}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 107
    :cond_7
    :goto_9
    if-eqz v4, :cond_8

    .line 108
    :try_start_b
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    .line 111
    :cond_8
    :goto_a
    throw v9

    .line 103
    :catch_8
    move-exception v3

    .line 104
    .restart local v3    # "e":Ljava/io/IOException;
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 109
    .end local v3    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v3

    .line 110
    .restart local v3    # "e":Ljava/io/IOException;
    const-string v10, "ImportResourceProperty"

    const-string v11, "create"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "IOException on ImportResourceProperty"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v11, v12}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_a

    .line 100
    .end local v3    # "e":Ljava/io/IOException;
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "read":I
    :catchall_1
    move-exception v9

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_8

    .line 96
    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v5    # "fos":Ljava/io/FileOutputStream;
    :catch_a
    move-exception v3

    move-object v4, v5

    .end local v5    # "fos":Ljava/io/FileOutputStream;
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 93
    .end local v7    # "read":I
    :catch_b
    move-exception v3

    goto/16 :goto_1
.end method

.method public create(Ljava/lang/String;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;Ljava/io/File;)Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "parent"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .param p3, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;
    .param p4, "file"    # Ljava/io/File;

    .prologue
    .line 177
    const-string v0, ""

    .line 178
    .local v0, "protocol":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 179
    const-string v2, "protocolInfo"

    invoke-virtual {p3, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    :cond_0
    new-instance v1, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;

    invoke-direct {v1, p1, v0, p4}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)V

    .line 182
    .local v1, "res":Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;
    return-object v1
.end method

.method public createFileName(Ljava/io/File;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;)Ljava/lang/String;
    .locals 4
    .param p1, "path"    # Ljava/io/File;
    .param p2, "parent"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;
    .param p3, "prop"    # Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;

    .prologue
    .line 161
    const-string v1, ""

    .line 162
    .local v1, "protocol":Ljava/lang/String;
    if-eqz p3, :cond_0

    .line 163
    const-string v2, "protocolInfo"

    invoke-virtual {p3, v2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/Property;->getAttributeValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 165
    :cond_0
    invoke-virtual {p2}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportItemObject;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/allshare/stack/upnp/xml/XML;->unEscapeXMLChars(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->PARSER:Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;
    invoke-static {}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->access$000()Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ResourceProtocolParser;->getMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->createDestFileName(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    invoke-static {p1, v2, v3}, Lcom/samsung/android/allshare/stack/mediaserver/upnp/object/ImportResourceProperty;->access$100(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 168
    .local v0, "destFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
