.class public Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;
.super Ljava/lang/Object;
.source "Subscriber.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "Subscriber"


# instance fields
.field private mDeliveryHost:Ljava/lang/String;

.field private mDeliveryPath:Ljava/lang/String;

.field private mDeliveryPort:I

.field private mDeliveryURL:Ljava/lang/String;

.field private mIfAddr:Ljava/lang/String;

.field private mNotifyCount:J

.field private mSID:Ljava/lang/String;

.field private mSubscriptionTime:J

.field private mTimeOut:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mSID:Ljava/lang/String;

    .line 63
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mIfAddr:Ljava/lang/String;

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryURL:Ljava/lang/String;

    .line 96
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryHost:Ljava/lang/String;

    .line 98
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryPath:Ljava/lang/String;

    .line 100
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryPort:I

    .line 118
    iput-wide v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mTimeOut:J

    .line 147
    iput-wide v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mSubscriptionTime:J

    .line 161
    iput-wide v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mNotifyCount:J

    .line 42
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->renew()V

    .line 43
    return-void
.end method


# virtual methods
.method public getDeliveryHost()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryHost:Ljava/lang/String;

    return-object v0
.end method

.method public getDeliveryPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryPath:Ljava/lang/String;

    return-object v0
.end method

.method public getDeliveryPort()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryPort:I

    return v0
.end method

.method public getDeliveryURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryURL:Ljava/lang/String;

    return-object v0
.end method

.method public getInterfaceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mIfAddr:Ljava/lang/String;

    return-object v0
.end method

.method public getNotifyCount()J
    .locals 2

    .prologue
    .line 164
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mNotifyCount:J

    return-wide v0
.end method

.method public getSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mSID:Ljava/lang/String;

    return-object v0
.end method

.method public getSubscriptionTime()J
    .locals 2

    .prologue
    .line 150
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mSubscriptionTime:J

    return-wide v0
.end method

.method public getTimeOut()J
    .locals 2

    .prologue
    .line 121
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mTimeOut:J

    return-wide v0
.end method

.method public incrementNotifyCount()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 172
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mNotifyCount:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 173
    iput-wide v4, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mNotifyCount:J

    .line 177
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mNotifyCount:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mNotifyCount:J

    goto :goto_0
.end method

.method public isExpired()Z
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 129
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 132
    .local v0, "currTime":J
    iget-wide v6, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mTimeOut:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-nez v5, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v4

    .line 136
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getSubscriptionTime()J

    move-result-wide v6

    invoke-virtual {p0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->getTimeOut()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    add-long v2, v6, v8

    .line 137
    .local v2, "expiredTime":J
    cmp-long v5, v2, v0

    if-gez v5, :cond_0

    .line 138
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public renew()V
    .locals 2

    .prologue
    .line 184
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->setSubscriptionTime(J)V

    .line 185
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->setNotifyCount(I)V

    .line 186
    return-void
.end method

.method public setDeliveryURL(Ljava/lang/String;)V
    .locals 5
    .param p1, "deliveryURL"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryURL:Ljava/lang/String;

    .line 87
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 88
    .local v1, "url":Ljava/net/URL;
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryHost:Ljava/lang/String;

    .line 89
    invoke-virtual {v1}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryPath:Ljava/lang/String;

    .line 90
    invoke-virtual {v1}, Ljava/net/URL;->getPort()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mDeliveryPort:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .end local v1    # "url":Ljava/net/URL;
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "Subscriber"

    const-string v3, "setDeliveryURL"

    const-string v4, "setDeliveryURL Exception"

    invoke-static {v2, v3, v4, v0}, Lcom/samsung/android/allshare/stack/util/DLog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public setNotifyCount(I)V
    .locals 2
    .param p1, "cnt"    # I

    .prologue
    .line 168
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mNotifyCount:J

    .line 169
    return-void
.end method

.method public setSID(Ljava/lang/String;)V
    .locals 0
    .param p1, "sid"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mSID:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setSubscriptionTime(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 154
    iput-wide p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mSubscriptionTime:J

    .line 155
    return-void
.end method

.method public setTimeOut(J)V
    .locals 1
    .param p1, "value"    # J

    .prologue
    .line 125
    iput-wide p1, p0, Lcom/samsung/android/allshare/stack/upnp/upnp/event/Subscriber;->mTimeOut:J

    .line 126
    return-void
.end method
