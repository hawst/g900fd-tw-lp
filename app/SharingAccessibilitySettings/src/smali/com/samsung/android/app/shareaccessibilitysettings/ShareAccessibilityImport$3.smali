.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$3;
.super Ljava/lang/Object;
.source "ShareAccessibilityImport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    .line 157
    if-nez p2, :cond_0

    .line 159
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 160
    .local v1, "import_internal_intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    const-string v2, "FOLDERPATH"

    sget-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_MOST_INTERNAL_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 166
    const-string v2, "CONTENT_TYPE"

    const-string v3, "application/x-sasf"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 170
    const-string v2, "JUST_SELECT_MODE"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 172
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;

    invoke-virtual {v2, v1, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->startActivityForResult(Landroid/content/Intent;I)V

    .line 196
    .end local v1    # "import_internal_intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 178
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 179
    .local v0, "import_external_intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 181
    const-string v2, "FOLDERPATH"

    const-string v3, "/storage/extSdCard"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 185
    const-string v2, "CONTENT_TYPE"

    const-string v3, "application/x-sasf"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const-string v2, "JUST_SELECT_MODE"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 191
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;

    invoke-virtual {v2, v0, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
