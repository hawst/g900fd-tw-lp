.class public Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;
.super Landroid/app/Activity;
.source "ShareAccessibilityImport.java"


# static fields
.field private static final IMPORT:I = 0x2

.field private static final IMPORT_PENDING_REQUESTCODE:I = 0x1

.field private static final PICK_SINGLE_FILE:I = 0x1

.field private static final RESTORE:I = 0x5

.field private static RevSettingValues:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ShareAccessibilityImport"

.field private static isDialogShown:Z

.field private static isSdCardAvailable:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDialogBuilder:Landroid/app/AlertDialog$Builder;

.field private mImportNotificationManager:Landroid/app/NotificationManager;

.field private path:Ljava/lang/String;

.field private window:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    sput-boolean v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isDialogShown:Z

    .line 37
    sput-boolean v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isSdCardAvailable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 24
    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mContext:Landroid/content/Context;

    .line 30
    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->path:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->window:Landroid/view/WindowManager$LayoutParams;

    .line 38
    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    return-void
.end method

.method private ShareAccessibilityImport_Init()V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mContext:Landroid/content/Context;

    .line 124
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->RevSettingValues:Ljava/util/HashMap;

    .line 125
    const-string v0, "ShareAccessibilityImport"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 22
    sget-boolean v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isSdCardAvailable:Z

    return v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 22
    sget-boolean v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isDialogShown:Z

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 22
    sput-boolean p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isDialogShown:Z

    return p0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->path:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->RevSettingValues:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$302(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Ljava/util/HashMap;

    .prologue
    .line 22
    sput-object p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->RevSettingValues:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->importNotificationSet(Ljava/lang/String;)V

    return-void
.end method

.method private importNotificationSet(Ljava/lang/String;)V
    .locals 10
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 285
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 286
    .local v4, "noti_import_intent":Landroid/content/Intent;
    const-string v6, "android.settings.ACCESSIBILITY_SETTINGS"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 287
    const-string v6, "importNotification"

    invoke-virtual {v4, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 289
    invoke-static {p0, v8, v4, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 293
    .local v3, "intent":Landroid/app/PendingIntent;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070013

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v8, [Ljava/lang/Object;

    aput-object p1, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 295
    .local v1, "importnotiticker":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070014

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 298
    .local v2, "importnotititle":Ljava/lang/String;
    const-string v6, "notification"

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    iput-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mImportNotificationManager:Landroid/app/NotificationManager;

    .line 299
    new-instance v0, Landroid/app/Notification;

    const v6, 0x7f020001

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v0, v6, v1, v8, v9}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 302
    .local v0, "importnotification":Landroid/app/Notification;
    invoke-virtual {v0, p0, v2, p1, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 304
    iget v6, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x10

    iput v6, v0, Landroid/app/Notification;->flags:I

    .line 306
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mImportNotificationManager:Landroid/app/NotificationManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    long-to-int v7, v8

    invoke-virtual {v6, v7, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 309
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.settings.action.talkback_off"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 310
    .local v5, "tbIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 312
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 260
    const-string v0, "ShareAccessibilityImport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 262
    const-string v0, "ShareAccessibilityImport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resultCode :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-string v0, "ShareAccessibilityImport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestCode :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    if-eqz p3, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->path:Ljava/lang/String;

    .line 266
    const-string v0, "FILE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->path:Ljava/lang/String;

    .line 268
    const-string v0, "ShareAccessibilityImport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult PICK_SINGLE_FILE path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->showDialog(I)V

    .line 281
    :goto_0
    return-void

    .line 277
    :cond_0
    const-string v0, "ShareAccessibilityImport"

    const-string v1, "data is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 278
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-direct {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->ShareAccessibilityImport_Init()V

    .line 44
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isExternalMemoryAvailable(Landroid/content/Context;)Z

    move-result v2

    sput-boolean v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isSdCardAvailable:Z

    .line 45
    const-string v2, "ShareAccessibilityImport"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "savedInstanceState"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isLightTheme(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    :goto_0
    invoke-direct {v3, p0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    .line 50
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->window:Landroid/view/WindowManager$LayoutParams;

    if-nez v2, :cond_0

    .line 51
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->window:Landroid/view/WindowManager$LayoutParams;

    .line 52
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->window:Landroid/view/WindowManager$LayoutParams;

    const/4 v3, 0x2

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 53
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->window:Landroid/view/WindowManager$LayoutParams;

    const/high16 v3, 0x3f000000    # 0.5f

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 54
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->getWindow()Landroid/view/Window;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->window:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 55
    const-string v2, "ShareAccessibilityImport"

    const-string v3, "Dimming background"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$1;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 73
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$2;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 89
    sget-boolean v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isSdCardAvailable:Z

    if-eqz v2, :cond_1

    if-nez p1, :cond_1

    .line 90
    sput-boolean v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isDialogShown:Z

    .line 93
    :cond_1
    sget-boolean v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isDialogShown:Z

    if-nez v2, :cond_5

    .line 94
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isExternalMemoryAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 96
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->showDialog(I)V

    .line 116
    :goto_2
    if-eqz p1, :cond_2

    .line 117
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->finish()V

    .line 119
    :cond_2
    return-void

    .line 46
    :cond_3
    const/4 v2, 0x4

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v2, "ShareAccessibilityImport"

    const-string v3, "window is not null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 99
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_4
    const-string v2, "ShareAccessibilityImport"

    const-string v3, "shareAccSettingsSinglePicker"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 102
    .local v1, "mImportRestore_intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 104
    const-string v2, "FOLDERPATH"

    sget-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_MOST_INTERNAL_PATH:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 106
    const-string v2, "CONTENT_TYPE"

    const-string v3, "application/x-sasf"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v2, "JUST_SELECT_MODE"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 109
    invoke-virtual {p0, v1, v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_2

    .line 113
    .end local v1    # "mImportRestore_intent":Landroid/content/Intent;
    :cond_5
    sput-boolean v5, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->isDialogShown:Z

    goto :goto_2
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 144
    const-string v2, "ShareAccessibilityImport"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateDialogonCreateDialog id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const/4 v2, 0x2

    new-array v0, v2, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f070029

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f070027

    invoke-virtual {v3, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v0, v2

    .line 149
    .local v0, "mEntries":[Ljava/lang/CharSequence;
    const v1, 0x7f07002a

    .line 150
    .local v1, "titleId":I
    const-string v2, "ShareAccessibilityImport"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreateDialog id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    packed-switch p1, :pswitch_data_0

    .line 255
    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2

    .line 155
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$3;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;)V

    invoke-virtual {v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 199
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 203
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mDialogBuilder:Landroid/app/AlertDialog$Builder;

    const v3, 0x7f07001e

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f07001d

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070003

    new-instance v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$5;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$5;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f070005

    new-instance v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$4;

    invoke-direct {v4, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport$4;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 151
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 316
    const-string v0, "ShareAccessibilityImport"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->RevSettingValues:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 319
    sput-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->RevSettingValues:Ljava/util/HashMap;

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mImportNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_1

    .line 323
    iput-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->mImportNotificationManager:Landroid/app/NotificationManager;

    .line 325
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 327
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 131
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 139
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 133
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityImport;->finish()V

    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
