.class public Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;
.super Landroid/app/Activity;
.source "ShareAccessibilityShareVia.java"


# static fields
.field private static final PICK_MULTI_FILE:I = 0x2

.field private static final SHARE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "ShareAccessibilityShareVia"


# instance fields
.field private isShowDialog:Z

.field private mContext:Landroid/content/Context;

.field private path:Ljava/lang/String;

.field private window:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 26
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->mContext:Landroid/content/Context;

    .line 34
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->path:Ljava/lang/String;

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->isShowDialog:Z

    .line 36
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->window:Landroid/view/WindowManager$LayoutParams;

    return-void
.end method

.method private ShareAccessibilityShareVia_Init()V
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->mContext:Landroid/content/Context;

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->isShowDialog:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;
    .param p1, "x1"    # Z

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->isShowDialog:Z

    return p1
.end method

.method private show_share_via_multi_files([Ljava/lang/String;)V
    .locals 8
    .param p1, "filePaths"    # [Ljava/lang/String;

    .prologue
    .line 254
    const-string v5, "ShareAccessibilityShareVia"

    const-string v6, "show_share_via_multi_files is entered"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 258
    .local v0, "attachments":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p1

    if-ge v1, v5, :cond_0

    .line 259
    const/4 v4, 0x0

    .line 260
    .local v4, "uri":Landroid/net/Uri;
    new-instance v5, Ljava/io/File;

    aget-object v6, p1, v1

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v4

    .line 261
    const-string v5, "ShareAccessibilityShareVia"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Uri "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ":"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 264
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 265
    .local v3, "showshareViaMultiIntent":Landroid/content/Intent;
    const-string v5, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 266
    const-string v5, "android.intent.extra.STREAM"

    invoke-virtual {v3, v5, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 268
    const-string v5, "application/x-sasf"

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 271
    const v5, 0x7f070025

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 273
    .local v2, "intentNew":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->startActivity(Landroid/content/Intent;)V

    .line 274
    return-void
.end method

.method private show_share_via_single_file(Ljava/lang/String;)V
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 238
    const-string v3, "ShareAccessibilityShareVia"

    const-string v4, "show_share_via_single_file is entered"

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    const/4 v2, 0x0

    .line 240
    .local v2, "uri":Landroid/net/Uri;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 241
    const-string v3, "ShareAccessibilityShareVia"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 243
    .local v1, "showshareViaSingleIntent":Landroid/content/Intent;
    const-string v3, "android.intent.action.SEND"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 244
    const-string v3, "android.intent.extra.STREAM"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 245
    const-string v3, "application/x-sasf"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 248
    const v3, 0x7f070025

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    .line 250
    .local v0, "intentNew":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->startActivity(Landroid/content/Intent;)V

    .line 251
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 198
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 199
    const-string v2, "ShareAccessibilityShareVia"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resultCode :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    const-string v2, "ShareAccessibilityShareVia"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestCode :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    if-eqz p3, :cond_4

    const/4 v2, 0x2

    if-ne p1, v2, :cond_4

    .line 203
    const/4 v0, 0x0

    .line 204
    .local v0, "files":[Ljava/lang/String;
    const-string v2, "FILE"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_1

    .line 206
    array-length v2, v0

    if-le v2, v6, :cond_2

    .line 207
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 208
    const-string v2, "ShareAccessibilityShareVia"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActivityResult length is 1 more, PICK_MULTI_FILE path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 212
    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->show_share_via_multi_files([Ljava/lang/String;)V

    .line 230
    .end local v0    # "files":[Ljava/lang/String;
    .end local v1    # "i":I
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->finish()V

    .line 234
    return-void

    .line 214
    .restart local v0    # "files":[Ljava/lang/String;
    :cond_2
    array-length v2, v0

    if-ne v2, v6, :cond_3

    .line 215
    const-string v2, "ShareAccessibilityShareVia"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onActivityResult length is 1, PICK_MULTI_FILE path:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v0, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    aget-object v2, v0, v5

    invoke-direct {p0, v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->show_share_via_single_file(Ljava/lang/String;)V

    goto :goto_1

    .line 220
    :cond_3
    const-string v2, "ShareAccessibilityShareVia"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PICK_MULTI_FILE : not selected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->path:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 227
    .end local v0    # "files":[Ljava/lang/String;
    :cond_4
    const-string v2, "ShareAccessibilityShareVia"

    const-string v3, "data is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-direct {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->ShareAccessibilityShareVia_Init()V

    .line 43
    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isExternalMemoryAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->showDialog(I)V

    .line 61
    :goto_0
    return-void

    .line 49
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->isShowDialog:Z

    .line 50
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 51
    .local v0, "ShareVia_intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    const-string v1, "FOLDERPATH"

    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_MOST_INTERNAL_PATH:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    const-string v1, "CONTENT_TYPE"

    const-string v2, "application/x-sasf"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    const-string v1, "JUST_SELECT_MODE"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 58
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 9
    .param p1, "id"    # I

    .prologue
    const/4 v8, 0x2

    .line 83
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isLightTheme(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x5

    :goto_0
    invoke-direct {v1, p0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 89
    .local v1, "dlg":Landroid/app/AlertDialog$Builder;
    :try_start_0
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->window:Landroid/view/WindowManager$LayoutParams;

    if-nez v6, :cond_0

    .line 90
    new-instance v6, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v6}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->window:Landroid/view/WindowManager$LayoutParams;

    .line 91
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->window:Landroid/view/WindowManager$LayoutParams;

    const/4 v7, 0x2

    iput v7, v6, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 92
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->window:Landroid/view/WindowManager$LayoutParams;

    const/high16 v7, 0x3f000000    # 0.5f

    iput v7, v6, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 93
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->getWindow()Landroid/view/Window;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->window:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v6, v7}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 94
    const-string v6, "ShareAccessibilityShareVia"

    const-string v7, "Dimming background"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_0
    :goto_1
    new-instance v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$1;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$1;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 112
    new-instance v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$2;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$2;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;)V

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 131
    const v5, 0x7f07002c

    .line 132
    .local v5, "titleId":I
    const v4, 0x7f070028

    .line 135
    .local v4, "mEntries_internal_Id":I
    new-array v3, v8, [Ljava/lang/CharSequence;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f070027

    invoke-virtual {v7, v8}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    aput-object v7, v3, v6

    .line 139
    .local v3, "mEntries":[Ljava/lang/CharSequence;
    const-string v6, "ShareAccessibilityShareVia"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onCreateDialog id : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    packed-switch p1, :pswitch_data_0

    .line 191
    :goto_2
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 192
    .local v0, "dialog":Landroid/app/AlertDialog;
    return-object v0

    .line 83
    .end local v0    # "dialog":Landroid/app/AlertDialog;
    .end local v1    # "dlg":Landroid/app/AlertDialog$Builder;
    .end local v3    # "mEntries":[Ljava/lang/CharSequence;
    .end local v4    # "mEntries_internal_Id":I
    .end local v5    # "titleId":I
    :cond_1
    const/4 v6, 0x4

    goto :goto_0

    .line 96
    .restart local v1    # "dlg":Landroid/app/AlertDialog$Builder;
    :catch_0
    move-exception v2

    .line 98
    .local v2, "e":Ljava/lang/RuntimeException;
    const-string v6, "ShareAccessibilityShareVia"

    const-string v7, "window is not null"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 143
    .end local v2    # "e":Ljava/lang/RuntimeException;
    .restart local v3    # "mEntries":[Ljava/lang/CharSequence;
    .restart local v4    # "mEntries_internal_Id":I
    .restart local v5    # "titleId":I
    :pswitch_0
    new-instance v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$3;

    invoke-direct {v6, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia$3;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;)V

    invoke-virtual {v1, v3, v6}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 186
    invoke-virtual {p0, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 140
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 69
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 77
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 71
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityShareVia;->finish()V

    goto :goto_0

    .line 69
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
