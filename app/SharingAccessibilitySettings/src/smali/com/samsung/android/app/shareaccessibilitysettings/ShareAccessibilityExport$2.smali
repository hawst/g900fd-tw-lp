.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;
.super Ljava/lang/Object;
.source "ShareAccessibilityExport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const v9, 0x7f070011

    const/4 v8, 0x1

    const-wide/16 v6, 0x0

    const v5, 0x7f07000b

    .line 130
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->DATE_FORMAT:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 131
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 132
    .local v1, "today":Ljava/util/Calendar;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Settings of accessibility"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".sasf"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    .line 135
    const-string v2, "ShareAccessibilityExport"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "FILE NAME : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    if-nez p2, :cond_3

    .line 137
    const-string v2, "ShareAccessibilityExport"

    const-string v3, "EXPORT SD card selected"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->getAvailableInternalMemorySize()J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-lez v2, :cond_2

    .line 140
    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->createFolder(Ljava/lang/String;)V

    .line 142
    sput v8, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->SaveFlag:I

    .line 143
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 144
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 146
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$100(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValue(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v2

    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$002(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 149
    const-string v2, "ShareAccessibilityExport"

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    new-instance v2, Ljava/io/File;

    sget-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    .line 153
    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->sharingFileNameCheck(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    .line 155
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-static {v2, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValueToFile(Ljava/util/HashMap;Ljava/io/File;)V

    .line 158
    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-lez v2, :cond_1

    .line 159
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    sget-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportNotificationSet(Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v2, v3, v4, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;I)V

    .line 224
    :goto_0
    return-void

    .line 167
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    invoke-virtual {v3, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    invoke-virtual {v4, v9}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$300(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_0

    .line 171
    :cond_2
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    invoke-virtual {v3, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    const v5, 0x7f070019

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$300(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_3
    const-string v2, "ShareAccessibilityExport"

    const-string v3, "EXTERNAL card selected"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$100(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isExternalMemoryAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 178
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$100(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->getAvailableExternalMemorySize(Landroid/content/Context;)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-lez v2, :cond_6

    .line 179
    const-string v2, "/storage/extSdCard/Accessibility"

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->createFolder(Ljava/lang/String;)V

    .line 181
    sput v8, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->SaveFlag:I

    .line 184
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 185
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 188
    :cond_4
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$100(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValue(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v2

    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$002(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 190
    const-string v2, "ShareAccessibilityExport"

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    new-instance v2, Ljava/io/File;

    const-string v3, "/storage/extSdCard/Accessibility"

    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    .line 194
    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->sharingFileNameCheck(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    .line 196
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v2

    sget-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-static {v2, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValueToFile(Ljava/util/HashMap;Ljava/io/File;)V

    .line 201
    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-lez v2, :cond_5

    .line 202
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    sget-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportNotificationSet(Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v2, v3, v4, v8}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 210
    :cond_5
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    invoke-virtual {v3, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    invoke-virtual {v4, v9}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$300(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto/16 :goto_0

    .line 214
    :cond_6
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    invoke-virtual {v3, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    const v5, 0x7f070010

    invoke-virtual {v4, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$300(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 217
    :cond_7
    const-string v2, "ShareAccessibilityExport"

    const-string v3, "External memory card is not existed"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
