.class public Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;
.super Ljava/lang/Object;
.source "ShareAccVariable.java"


# static fields
.field public static final ACCESSIBILITY_CAPTIONING_WINDOW_COLOR:Ljava/lang/String; = "accessibility_captioning_window_color"

.field public static final ACC_SETTING_BT_FILE_PATH:Ljava/lang/String; = "/storage/sdcard0/Bluetooth/"

.field public static final ACC_SETTING_FILE_EXTERNAL_FOLDER_PATH:Ljava/lang/String; = "/storage/extSdCard/Accessibility"

.field public static final ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

.field public static final ACC_SETTING_FILE_MOST_EXTERNAL_PATH:Ljava/lang/String; = "/storage/extSdCard"

.field public static final ACC_SETTING_FILE_MOST_INTERNAL_PATH:Ljava/lang/String;

.field public static final ALL_SOUND_MUTE:Ljava/lang/String; = "android.settings.ALL_SOUND_MUTE"

.field public static final ANSWERING_ACCESSIBILITY_TAPPING_KEYLIST:Ljava/lang/String; = "answering_accessibility_tapping"

.field public static final ANSWERING_BRING_TO_EAR:Ljava/lang/String; = "answering_bring_to_ear"

.field public static final ANSWER_CALLS_USING_VOICE_INPUT_CONTROL_INCOMIMNG_CALLS_KEYLIST:Ljava/lang/String; = "voice_input_control_incomming_calls"

.field public static final ANSWER_CALLS_USING_VOICE_INPUT_CONTROL_KEYLIST:Ljava/lang/String; = "voice_input_control"

.field public static final AUDIO_BALANCE:Ljava/lang/String; = "audio_balance"

.field public static final AessibilitySettingFileMimeType:Ljava/lang/String; = "application/x-sasf"

.field public static DATE_FORMAT:Ljava/lang/String; = null

.field public static final ENABLED_ACCESSIBILITY_SERVICES_SEPARATOR:C = ':'

.field public static FILE_NAME:Ljava/lang/String; = null

.field public static final FILE_NAME_FORMAT:Ljava/lang/String; = ".sasf"

.field public static final FILE_NAME_STRING:Ljava/lang/String; = "Settings of accessibility"

.field public static final HEARING_AID_PREFERENCE_KEY:Ljava/lang/String; = "call_hearing_aid"

.field public static final HOME_KEY_ANSWERS_CALLS_ANYKEY_MODE_KEYLIST:Ljava/lang/String; = "anykey_mode"

.field public static final HOVER_ZOOM_MAGNIFIER_SIZE:Ljava/lang/String; = "hover_zoom_magnifier_size"

.field public static final HOVER_ZOOM_VALUE:Ljava/lang/String; = "hover_zoom_value"

.field public static final MONO_AUDIO_CHANGED:Ljava/lang/String; = "android.settings.MONO_AUDIO_CHANGED"

.field public static final MONO_AUDIO_DB_KEYLIST:Ljava/lang/String; = "mono_audio_db"

.field public static final SLASH:Ljava/lang/String; = "/"

.field public static final SOUND_BALANCE:Ljava/lang/String; = "sound_balance"

.field public static final SWITCH_ACCESS_KEY:Ljava/lang/String; = "switch_access_key"

.field public static SaveFlag:I = 0x0

.field public static final TALKBACK_KEY:Ljava/lang/String; = "talk_back"

.field public static final TTS_ENGINE_KEYLIST:Ljava/lang/String; = "tts_engine"

.field public static final TURNOFF_ALLSOUNDS_KEYLIST:Ljava/lang/String; = "all_sound_off"

.field public static final VOICE_DATA_INTEGRITY_CHECK:I = 0x7b9

.field public static export_external_file:Ljava/io/File; = null

.field public static export_internal_file:Ljava/io/File; = null

.field public static fis:Ljava/io/FileInputStream; = null

.field public static final keyList:[Ljava/lang/String;

.field public static final mCurConfig:Landroid/content/res/Configuration;

.field public static ois:Ljava/io/ObjectInputStream; = null

.field public static final sInstalledServices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public static final sStringColonSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

.field public static final shareAccSettingsMultiPicker:Ljava/lang/String; = "com.sec.android.app.myfiles.PICK_DATA_MULTIPLE"

.field public static final shareAccSettingsMyFilePackagename:Ljava/lang/String; = "com.sec.android.app.myfiles"

.field public static final shareAccSettingsSinglePicker:Ljava/lang/String; = "com.sec.android.app.myfiles.PICK_DATA"


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0x3a

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 60
    const/16 v0, 0x51

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "accelerometer_rotation"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, "accelerometer_rotation_second"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "screen_off_timeout"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "speak_password"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "anykey_mode"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "answering_bring_to_ear"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "answering_accessibility_tapping"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "voice_input_control"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "voice_input_control_incomming_calls"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "incall_power_button_behavior"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "accessibility_enabled"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "font_size"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "font_scale"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "accessibility_display_magnification_enabled"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "high_contrast"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "color_blind"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "color_blind_test"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "color_blind_cvdtype"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "color_blind_cvdseverity"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "color_blind_user_parameter"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "tts_engine"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "tts_default_rate"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "accessibility_script_injection"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "audio_balance"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "sound_balance"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "mono_audio_db"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "all_sound_off"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "switch_access_key"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "flash_notification"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "assistant_menu"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "assistant_menu_dominant_hand_type"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "assistant_menu_pointer_speed"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "assistant_menu_pointer_size"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "assistant_menu_pad_size"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "long_press_timeout"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "enable_accessibility_global_gesture_enabled"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "def_tactileassist_enable"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "access_control_use"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "lock_screen_lock_after_timeout"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "lcd_curtain"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "ultrasonic_cane"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "haptic_feedback_enabled"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "haptic_feedback_extra"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "easy_interaction"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "notification_reminder"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "time_key"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "notification_reminder_selectable"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "time_key_selectable"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "notification_reminder_vibrate"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "notification_reminder_app_list"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "hearing_aid"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "air_motion_call_accept"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "accessibility_captioning_enabled"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "accessibility_captioning_font_scale"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "accessibility_captioning_preset"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "accessibility_captioning_typeface"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "accessibility_captioning_foreground_color"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "accessibility_captioning_edge_type"

    aput-object v2, v0, v1

    const-string v1, "accessibility_captioning_edge_color"

    aput-object v1, v0, v5

    const/16 v1, 0x3b

    const-string v2, "accessibility_captioning_window_color"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "accessibility_captioning_background_color"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "accessibility_captioning_locale"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "accessibility_sec_captioning_enabled"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "rapid_key_input"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "rapid_key_input_menu_checked"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "air_motion_wake_up"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "direct_access"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "direct_accessibility"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "direct_talkback"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "direct_negative"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "smart_scroll"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "face_smart_scroll"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "smart_scroll_sensitivity"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "smart_scroll_visual_feedback_icon"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "VIB_NOTIFICATION_MAGNITUDE"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "sound_detector"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "doorbell_detector"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "accessibility_magnifier"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "hover_zoom_value"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "hover_zoom_magnifier_size"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "direct_access_control"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->keyList:[Ljava/lang/String;

    .line 160
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->mCurConfig:Landroid/content/res/Configuration;

    .line 164
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->sInstalledServices:Ljava/util/Set;

    .line 168
    new-instance v0, Landroid/text/TextUtils$SimpleStringSplitter;

    invoke-direct {v0, v5}, Landroid/text/TextUtils$SimpleStringSplitter;-><init>(C)V

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->sStringColonSplitter:Landroid/text/TextUtils$SimpleStringSplitter;

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/storage/emulated/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Accessibility"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/storage/emulated/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_MOST_INTERNAL_PATH:Ljava/lang/String;

    .line 190
    const-string v0, "yyyyMMdd"

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->DATE_FORMAT:Ljava/lang/String;

    .line 198
    sput-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    .line 200
    sput v4, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->SaveFlag:I

    .line 203
    sput-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->fis:Ljava/io/FileInputStream;

    .line 205
    sput-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ois:Ljava/io/ObjectInputStream;

    .line 207
    sput-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    .line 209
    sput-object v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
