.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;
.super Ljava/lang/Object;
.source "ShareAccessibilitySettingsReceive.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->showImportAndRestoreDialog(Ljava/util/HashMap;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;

.field final synthetic val$filename:Ljava/lang/String;

.field final synthetic val$restoreSettingValue:Ljava/util/HashMap;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 317
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;

    iput-object p2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;->val$restoreSettingValue:Ljava/util/HashMap;

    iput-object p3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;->val$filename:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 319
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->access$000(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;->val$restoreSettingValue:Ljava/util/HashMap;

    invoke-static {v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->applySettings(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 321
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;

    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;->val$filename:Ljava/lang/String;

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->importNotificationSet(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->access$100(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;

    invoke-virtual {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->finish()V

    .line 323
    return-void
.end method
