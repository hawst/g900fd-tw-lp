.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;
.super Ljava/lang/Object;
.source "ShareAccessibilityExportImport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 13
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/16 v12, 0xa

    const/4 v8, 0x5

    const/4 v11, 0x4

    const/4 v9, 0x2

    const/4 v10, 0x1

    .line 175
    new-instance v4, Ljava/text/SimpleDateFormat;

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->DATE_FORMAT:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 176
    .local v4, "sdf":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 177
    .local v5, "today":Ljava/util/Calendar;
    invoke-virtual {v5, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 178
    .local v3, "month":Ljava/lang/String;
    invoke-virtual {v5, v9}, Ljava/util/Calendar;->get(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    if-ge v6, v12, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 179
    :cond_0
    invoke-virtual {v5, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 180
    .local v0, "day":Ljava/lang/String;
    invoke-virtual {v5, v8}, Ljava/util/Calendar;->get(I)I

    move-result v6

    if-ge v6, v12, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "0"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182
    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Settings of accessibility"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v5, v10}, Ljava/util/Calendar;->get(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".sasf"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    sput-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    .line 184
    const-string v6, "ShareAccessibilityExportImport"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FILE NAME : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    if-nez p2, :cond_3

    .line 186
    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isDialogShown:Z
    invoke-static {v10}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$002(Z)Z

    .line 187
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 188
    .local v2, "import_internal_intent":Landroid/content/Intent;
    const-string v6, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 190
    const-string v6, "FOLDERPATH"

    sget-object v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_MOST_INTERNAL_PATH:Ljava/lang/String;

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string v6, "CONTENT_TYPE"

    const-string v7, "application/x-sasf"

    invoke-virtual {v2, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 194
    const-string v6, "JUST_SELECT_MODE"

    invoke-virtual {v2, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 195
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-virtual {v6, v2, v11}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->startActivityForResult(Landroid/content/Intent;I)V

    .line 256
    .end local v2    # "import_internal_intent":Landroid/content/Intent;
    :cond_2
    :goto_0
    return-void

    .line 196
    :cond_3
    if-ne p2, v10, :cond_4

    .line 197
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const/4 v7, 0x6

    invoke-virtual {v6, v7}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->showDialog(I)V

    goto :goto_0

    .line 198
    :cond_4
    if-ne p2, v9, :cond_5

    .line 199
    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isDialogShown:Z
    invoke-static {v10}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$002(Z)Z

    .line 200
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 201
    .local v1, "import_external_intent":Landroid/content/Intent;
    const-string v6, "com.sec.android.app.myfiles.PICK_DATA"

    invoke-virtual {v1, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    const-string v6, "FOLDERPATH"

    const-string v7, "/storage/extSdCard"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 205
    const-string v6, "CONTENT_TYPE"

    const-string v7, "application/x-sasf"

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 207
    const-string v6, "JUST_SELECT_MODE"

    invoke-virtual {v1, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 208
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-virtual {v6, v1, v11}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 209
    .end local v1    # "import_external_intent":Landroid/content/Intent;
    :cond_5
    const/4 v6, 0x3

    if-ne p2, v6, :cond_2

    .line 210
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isExternalMemoryAvailable(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 212
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->getAvailableExternalMemorySize(Landroid/content/Context;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_8

    .line 213
    const-string v6, "/storage/extSdCard/Accessibility"

    invoke-static {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->createFolder(Ljava/lang/String;)V

    .line 215
    sput v10, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->SaveFlag:I

    .line 218
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$300()Ljava/util/HashMap;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 219
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$300()Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashMap;->clear()V

    .line 222
    :cond_6
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValue(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v6

    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$302(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 224
    const-string v6, "ShareAccessibilityExportImport"

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$300()Ljava/util/HashMap;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    new-instance v6, Ljava/io/File;

    const-string v7, "/storage/extSdCard/Accessibility"

    sget-object v8, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    invoke-direct {v6, v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    .line 228
    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-static {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->sharingFileNameCheck(Ljava/io/File;)Ljava/io/File;

    move-result-object v6

    sput-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    .line 230
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$300()Ljava/util/HashMap;

    move-result-object v6

    sget-object v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-static {v6, v7}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValueToFile(Ljava/util/HashMap;Ljava/io/File;)V

    .line 234
    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->length()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_7

    .line 235
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    sget-object v7, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->exportNotificationSet(Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v6, v7, v8, v10}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$400(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;Ljava/lang/String;I)V

    .line 254
    :goto_1
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-virtual {v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->finish()V

    goto/16 :goto_0

    .line 243
    :cond_7
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    iget-object v7, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const v8, 0x7f07000b

    invoke-virtual {v7, v8}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const v9, 0x7f070011

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v7, v8}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$500(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_external_file:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 247
    :cond_8
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    iget-object v7, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const v8, 0x7f07000b

    invoke-virtual {v7, v8}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    const v9, 0x7f070010

    invoke-virtual {v8, v9}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v6, v7, v8}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$500(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 250
    :cond_9
    const-string v6, "ShareAccessibilityExportImport"

    const-string v7, "External memory card is not existed"

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
