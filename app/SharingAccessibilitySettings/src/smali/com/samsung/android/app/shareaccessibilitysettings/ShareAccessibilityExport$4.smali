.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;
.super Ljava/lang/Object;
.source "ShareAccessibilityExport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)V
    .locals 0

    .prologue
    .line 255
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 259
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->createFolder(Ljava/lang/String;)V

    .line 261
    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->SaveFlag:I

    .line 262
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 263
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$100(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValue(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$002(Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 267
    const-string v0, "ShareAccessibilityExport"

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$000()Ljava/util/HashMap;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-static {v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValueToFile(Ljava/util/HashMap;Ljava/io/File;)V

    .line 274
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    sget-object v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportNotificationSet(Ljava/lang/String;Ljava/lang/String;I)V
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;I)V

    .line 287
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    invoke-virtual {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->finish()V

    .line 288
    return-void

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    const v2, 0x7f07000b

    invoke-virtual {v1, v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    const v3, 0x7f070011

    invoke-virtual {v2, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->access$300(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method
