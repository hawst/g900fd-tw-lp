.class public Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;
.super Landroid/app/Activity;
.source "ShareAccessibilityExport.java"


# static fields
.field private static final EXPORT:I = 0x1

.field private static final EXPORT_NO_CARD:I = 0x4

.field private static final EXPORT_PENDING_REQUESTCODE_EXTERNAL:I = 0x2

.field private static final EXPORT_PENDING_REQUESTCODE_INTERNAL:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ShareAccessibilityExport"

.field private static settingValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mExportFailNotificationManager:Landroid/app/NotificationManager;

.field private mExportNotificationManager:Landroid/app/NotificationManager;

.field pending_noti_external_intent:Landroid/app/PendingIntent;

.field pending_noti_internal_intent:Landroid/app/PendingIntent;

.field private final selectExternalMemory:I

.field private final selectInternalMemory:I

.field private window:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 30
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->selectInternalMemory:I

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->selectExternalMemory:I

    .line 37
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->pending_noti_internal_intent:Landroid/app/PendingIntent;

    .line 38
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->pending_noti_external_intent:Landroid/app/PendingIntent;

    .line 45
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->window:Landroid/view/WindowManager$LayoutParams;

    return-void
.end method

.method private ShareAccessibilityExport_Init()V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;

    .line 69
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;

    .line 71
    const-string v0, "ShareAccessibilityExport"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    return-void
.end method

.method static synthetic access$000()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$002(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Ljava/util/HashMap;

    .prologue
    .line 26
    sput-object p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportNotificationSet(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 379
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mExportFailNotificationManager:Landroid/app/NotificationManager;

    .line 380
    new-instance v0, Landroid/app/Notification;

    const v1, 0x7f020001

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v1, p1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 383
    .local v0, "exportFailureNotification":Landroid/app/Notification;
    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, p2, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 386
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 388
    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mExportFailNotificationManager:Landroid/app/NotificationManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 390
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->finish()V

    .line 391
    return-void
.end method

.method private exportNotificationSet(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10
    .param p1, "file_name"    # Ljava/lang/String;
    .param p2, "file_path"    # Ljava/lang/String;
    .param p3, "which"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 309
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 310
    .local v4, "noti_internal_export_intent":Landroid/content/Intent;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 313
    .local v3, "noti_external_export_intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.myfiles"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 316
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.myfiles"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 319
    const-string v5, "ShareAccessibilityExport"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "exportNotificationSet which :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    if-nez p3, :cond_2

    .line 321
    const-string v5, "START_FOLDER"

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 323
    invoke-static {p0, v8, v4, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->pending_noti_internal_intent:Landroid/app/PendingIntent;

    .line 336
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 339
    .local v1, "exportnotiticker":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 342
    .local v2, "exportnotititle":Ljava/lang/String;
    const-string v5, "notification"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    iput-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mExportNotificationManager:Landroid/app/NotificationManager;

    .line 343
    new-instance v0, Landroid/app/Notification;

    const v5, 0x7f020001

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v0, v5, v1, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 346
    .local v0, "exportnotification":Landroid/app/Notification;
    if-nez p3, :cond_3

    .line 347
    iget-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->pending_noti_internal_intent:Landroid/app/PendingIntent;

    invoke-virtual {v0, p0, v2, p1, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 355
    :cond_1
    :goto_1
    iget v5, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x10

    iput v5, v0, Landroid/app/Notification;->flags:I

    .line 357
    iget-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mExportNotificationManager:Landroid/app/NotificationManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v5, v6, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 361
    iget-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v5, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 362
    return-void

    .line 327
    .end local v0    # "exportnotification":Landroid/app/Notification;
    .end local v1    # "exportnotiticker":Ljava/lang/String;
    .end local v2    # "exportnotititle":Ljava/lang/String;
    :cond_2
    if-ne p3, v8, :cond_0

    .line 328
    const-string v5, "START_FOLDER"

    const-string v6, "/storage/extSdCard/Accessibility"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 330
    const/4 v5, 0x2

    invoke-static {p0, v5, v3, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->pending_noti_external_intent:Landroid/app/PendingIntent;

    goto :goto_0

    .line 350
    .restart local v0    # "exportnotification":Landroid/app/Notification;
    .restart local v1    # "exportnotiticker":Ljava/lang/String;
    .restart local v2    # "exportnotititle":Ljava/lang/String;
    :cond_3
    if-ne p3, v8, :cond_1

    .line 351
    iget-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->pending_noti_external_intent:Landroid/app/PendingIntent;

    invoke-virtual {v0, p0, v2, p1, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_1
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    invoke-direct {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->ShareAccessibilityExport_Init()V

    .line 53
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isExternalMemoryAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->showDialog(I)V

    .line 65
    :goto_0
    return-void

    .line 58
    :cond_0
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->getAvailableInternalMemorySize()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 59
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->showDialog(I)V

    goto :goto_0

    .line 61
    :cond_1
    const v0, 0x7f07000b

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f070019

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 14
    .param p1, "id"    # I

    .prologue
    const/4 v11, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 91
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isLightTheme(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_1

    const/4 v9, 0x5

    :goto_0
    invoke-direct {v1, p0, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 95
    .local v1, "dlg":Landroid/app/AlertDialog$Builder;
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->window:Landroid/view/WindowManager$LayoutParams;

    if-nez v9, :cond_0

    .line 96
    new-instance v9, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v9}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->window:Landroid/view/WindowManager$LayoutParams;

    .line 97
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->window:Landroid/view/WindowManager$LayoutParams;

    const/4 v10, 0x2

    iput v10, v9, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 98
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->window:Landroid/view/WindowManager$LayoutParams;

    const/high16 v10, 0x3f000000    # 0.5f

    iput v10, v9, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 99
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getWindow()Landroid/view/Window;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->window:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v9, v10}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 100
    const-string v9, "ShareAccessibilityExport"

    const-string v10, "Dimming background"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :cond_0
    :goto_1
    new-instance v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$1;

    invoke-direct {v9, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$1;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)V

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 116
    new-array v5, v11, [Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f070029

    invoke-virtual {v9, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    aput-object v9, v5, v12

    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const v10, 0x7f070027

    invoke-virtual {v9, v10}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    aput-object v9, v5, v13

    .line 120
    .local v5, "mEntries":[Ljava/lang/CharSequence;
    const v7, 0x7f07002a

    .line 121
    .local v7, "titleId":I
    const-string v9, "ShareAccessibilityExport"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onCreateDialog id : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    packed-switch p1, :pswitch_data_0

    .line 302
    :goto_2
    :pswitch_0
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 304
    .local v0, "dialog":Landroid/app/AlertDialog;
    return-object v0

    .line 91
    .end local v0    # "dialog":Landroid/app/AlertDialog;
    .end local v1    # "dlg":Landroid/app/AlertDialog$Builder;
    .end local v5    # "mEntries":[Ljava/lang/CharSequence;
    .end local v7    # "titleId":I
    :cond_1
    const/4 v9, 0x4

    goto :goto_0

    .line 102
    .restart local v1    # "dlg":Landroid/app/AlertDialog$Builder;
    :catch_0
    move-exception v2

    .line 104
    .local v2, "e":Ljava/lang/RuntimeException;
    const-string v9, "ShareAccessibilityExport"

    const-string v10, "window is not null"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 125
    .end local v2    # "e":Ljava/lang/RuntimeException;
    .restart local v5    # "mEntries":[Ljava/lang/CharSequence;
    .restart local v7    # "titleId":I
    :pswitch_1
    new-instance v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;

    invoke-direct {v9, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$2;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)V

    invoke-virtual {v1, v5, v9}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 227
    invoke-virtual {p0, v7}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 233
    :pswitch_2
    new-instance v6, Ljava/text/SimpleDateFormat;

    sget-object v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->DATE_FORMAT:Ljava/lang/String;

    invoke-direct {v6, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 234
    .local v6, "sdf":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v8

    .line 235
    .local v8, "today":Ljava/util/Calendar;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Settings of accessibility"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ".sasf"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    .line 237
    new-instance v9, Ljava/io/File;

    sget-object v10, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    sget-object v11, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    invoke-direct {v9, v10, v11}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    .line 240
    sget-object v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-static {v9}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->sharingFileNameCheck(Ljava/io/File;)Ljava/io/File;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    .line 243
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 245
    .local v4, "export_path":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f070009

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-array v10, v13, [Ljava/lang/Object;

    aput-object v4, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 250
    .local v3, "export_description":Ljava/lang/String;
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f07000a

    invoke-virtual {v9, v10}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f070005

    new-instance v11, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;

    invoke-direct {v11, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$4;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f070003

    new-instance v11, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$3;

    invoke-direct {v11, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport$3;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_2

    .line 122
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 395
    const-string v0, "ShareAccessibilityExport"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 398
    sput-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->settingValue:Ljava/util/HashMap;

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mExportNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_1

    .line 402
    iput-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->mExportNotificationManager:Landroid/app/NotificationManager;

    .line 405
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 406
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 77
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 85
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 79
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->finish()V

    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 366
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 367
    packed-switch p1, :pswitch_data_0

    .line 375
    :goto_0
    :pswitch_0
    return-void

    .line 369
    :pswitch_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->removeDialog(I)V

    goto :goto_0

    .line 372
    :pswitch_2
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExport;->removeDialog(I)V

    goto :goto_0

    .line 367
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
