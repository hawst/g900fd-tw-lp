.class public Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;
.super Landroid/app/Activity;
.source "ShareAccessibilitySettingsReceive.java"


# static fields
.field private static final IMPORT_PENDING_REQUESTCODE:I = 0x1

.field private static RevSettingValues:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "ShareAccessibilitySettingsReceive"

.field private static receiveSettingValues:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final delimiterReceivePath:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mImportandRestoreNotificationManager:Landroid/app/NotificationManager;

.field private mReceiveFullPath:Ljava/lang/String;

.field mReceiveUri:Landroid/net/Uri;

.field private separationReceivePath:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->RevSettingValues:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mContext:Landroid/content/Context;

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->separationReceivePath:[Ljava/lang/String;

    .line 48
    const-string v0, "\\/"

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->delimiterReceivePath:Ljava/lang/String;

    .line 50
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveUri:Landroid/net/Uri;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->importNotificationSet(Ljava/lang/String;)V

    return-void
.end method

.method private applyPayload([B)V
    .locals 20
    .param p1, "payload"    # [B

    .prologue
    .line 212
    new-instance v15, Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Ljava/lang/String;-><init>([B)V

    sput-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->RevSettingValues:Ljava/lang/String;

    .line 213
    const-string v15, "ShareAccessibilitySettingsReceive"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "applyPayload: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->RevSettingValues:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 217
    .local v10, "mResultHashMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 218
    .local v12, "separationEachSettingValue":[Ljava/lang/String;
    const/4 v13, 0x0

    .line 219
    .local v13, "separationHashKey_HashValue":[Ljava/lang/String;
    const-string v5, ","

    .line 220
    .local v5, "delimiterComma":Ljava/lang/String;
    const-string v6, "="

    .line 221
    .local v6, "delimiterEqual":Ljava/lang/String;
    const-string v7, "\\{"

    .line 222
    .local v7, "delimiterLBracket":Ljava/lang/String;
    const-string v8, "\\}"

    .line 223
    .local v8, "delimiterRBracket":Ljava/lang/String;
    const/4 v3, 0x0

    .line 224
    .local v3, "ResultKey":Ljava/lang/String;
    const/4 v4, 0x0

    .line 225
    .local v4, "ResultValue":Ljava/lang/String;
    const/4 v2, 0x0

    .line 227
    .local v2, "RemoveValues":Ljava/lang/String;
    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->RevSettingValues:Ljava/lang/String;

    const-string v16, ""

    move-object/from16 v0, v16

    invoke-virtual {v15, v7, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 228
    const-string v15, ""

    invoke-virtual {v2, v8, v15}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 229
    const-string v15, "ShareAccessibilitySettingsReceive"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "after remove Bracket: "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 232
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    array-length v15, v12

    if-ge v9, v15, :cond_0

    .line 233
    aget-object v15, v12, v9

    invoke-virtual {v15, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 235
    const/4 v15, 0x0

    aget-object v15, v13, v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 236
    const/4 v15, 0x1

    aget-object v15, v13, v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    .line 237
    invoke-virtual {v10, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 238
    const-string v15, "ShareAccessibilitySettingsReceive"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "ResultKey"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    const-string v15, "ShareAccessibilitySettingsReceive"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "ResultValue"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 243
    :cond_0
    new-instance v11, Ljava/text/SimpleDateFormat;

    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->DATE_FORMAT:Ljava/lang/String;

    invoke-direct {v11, v15}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 244
    .local v11, "sdf":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v14

    .line 245
    .local v14, "today":Ljava/util/Calendar;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Settings of accessibility"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v14}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, ".sasf"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    sput-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    .line 248
    const-string v15, "ShareAccessibilitySettingsReceive"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "FILE NAME : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    sget-object v17, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    invoke-static {v15}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->createFolder(Ljava/lang/String;)V

    .line 252
    const/4 v15, 0x1

    sput v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->SaveFlag:I

    .line 253
    new-instance v15, Ljava/io/File;

    sget-object v16, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    sget-object v17, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    invoke-direct/range {v15 .. v17}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    .line 255
    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-static {v15}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->sharingFileNameCheck(Ljava/io/File;)Ljava/io/File;

    move-result-object v15

    sput-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    .line 258
    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-static {v10, v15}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->saveValueToFile(Ljava/util/HashMap;Ljava/io/File;)V

    .line 260
    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->length()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v15, v16, v18

    if-lez v15, :cond_1

    .line 261
    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v15}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->showImportAndRestoreDialog(Ljava/util/HashMap;Ljava/lang/String;)V

    .line 269
    :goto_1
    return-void

    .line 263
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->showImportFailureDialog()V

    .line 264
    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->delete()Z

    goto :goto_1
.end method

.method public static hasPackage(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 4
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 162
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 163
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x1

    .line 165
    .local v1, "hasPkg":Z
    const/16 v3, 0x80

    :try_start_0
    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 169
    :goto_0
    return v1

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private importNotificationSet(Ljava/lang/String;)V
    .locals 10
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 360
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 361
    .local v4, "noti_import_intent":Landroid/content/Intent;
    const-string v6, "android.settings.ACCESSIBILITY_SETTINGS"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    const-string v6, "importNotification"

    invoke-virtual {v4, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 363
    invoke-static {p0, v8, v4, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 366
    .local v3, "intent":Landroid/app/PendingIntent;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070013

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v8, [Ljava/lang/Object;

    aput-object p1, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 368
    .local v1, "importnotiticker":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070014

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 370
    .local v2, "importnotititle":Ljava/lang/String;
    const-string v6, "notification"

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    iput-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mImportandRestoreNotificationManager:Landroid/app/NotificationManager;

    .line 371
    new-instance v0, Landroid/app/Notification;

    const v6, 0x7f020001

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v0, v6, v1, v8, v9}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 373
    .local v0, "importnotification":Landroid/app/Notification;
    invoke-virtual {v0, p0, v2, p1, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 375
    iget v6, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x10

    iput v6, v0, Landroid/app/Notification;->flags:I

    .line 377
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mImportandRestoreNotificationManager:Landroid/app/NotificationManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    long-to-int v7, v8

    invoke-virtual {v6, v7, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 379
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.settings.action.talkback_off"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 380
    .local v5, "tbIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 381
    return-void
.end method

.method private shareAccessibilitySettingsReceive_Init()V
    .locals 2

    .prologue
    .line 204
    const-string v0, "ShareAccessibilitySettingsReceive"

    const-string v1, "init"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mContext:Landroid/content/Context;

    .line 207
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->receiveSettingValues:Ljava/util/HashMap;

    .line 208
    return-void
.end method

.method private showImportAndRestoreDialog(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 4
    .param p2, "filename"    # Ljava/lang/String;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 304
    .local p1, "restoreSettingValue":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isLightTheme(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    :goto_0
    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 309
    .local v0, "dlg":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07001e

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070003

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$7;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$7;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070005

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;

    invoke-direct {v3, p0, p1, p2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$6;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;Ljava/util/HashMap;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$5;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 335
    return-void

    .line 304
    .end local v0    # "dlg":Landroid/app/AlertDialog$Builder;
    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method

.method private showImportFailureDialog()V
    .locals 4

    .prologue
    .line 273
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isLightTheme(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    :goto_0
    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 277
    .local v0, "dlg":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f07001c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070019

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070005

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$3;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$3;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 284
    new-instance v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$4;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 294
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 295
    return-void

    .line 273
    .end local v0    # "dlg":Landroid/app/AlertDialog$Builder;
    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method

.method private showImportGuideDialog()V
    .locals 4

    .prologue
    .line 339
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isLightTheme(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    :goto_0
    invoke-direct {v0, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 344
    .local v0, "dlg":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f07001d

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f07001f

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f070005

    new-instance v3, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$9;

    invoke-direct {v3, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$9;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$8;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$8;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 356
    return-void

    .line 339
    .end local v0    # "dlg":Landroid/app/AlertDialog$Builder;
    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method

.method private showNotSupportFileDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 174
    const-string v1, "ShareAccessibilitySettingsReceive"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "showNotSupportFileDialog : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-static {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isLightTheme(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    :goto_0
    invoke-direct {v2, p0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 179
    .local v0, "alertDialog":Landroid/app/AlertDialog;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t open /"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 180
    const-string v1, "Please download an app that can open this file."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 182
    const-string v1, "OK"

    new-instance v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$1;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog;->setButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 189
    new-instance v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive$2;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 199
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 201
    return-void

    .line 175
    .end local v0    # "alertDialog":Landroid/app/AlertDialog;
    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    const/4 v11, 0x0

    .line 58
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 59
    const-string v9, "ShareAccessibilitySettingsReceive"

    const-string v10, "onCreate"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "com.samsung.android.app.shareaccessibilitysettings"

    invoke-static {v9, v10}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->hasPackage(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 62
    const-string v9, "ShareAccessibilitySettingsReceive"

    const-string v10, "This model is not supported to sharing accessiblity settings file"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 64
    .local v4, "intent":Landroid/content/Intent;
    const-string v9, "ShareAccessibilitySettingsReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Intent : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveUri:Landroid/net/Uri;

    .line 67
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveUri:Landroid/net/Uri;

    if-eqz v9, :cond_1

    .line 69
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    .line 70
    const-string v9, "ShareAccessibilitySettingsReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mReceiveUri: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    const-string v1, ""

    .line 73
    .local v1, "ReceivefileNameInNotSupportModel":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    iget-object v10, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    iget-object v11, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 76
    const-string v9, "ShareAccessibilitySettingsReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ReceivefileNameInNotSupportModel : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    invoke-direct {p0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->showNotSupportFileDialog(Ljava/lang/String;)V

    .line 159
    .end local v1    # "ReceivefileNameInNotSupportModel":Ljava/lang/String;
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 80
    .restart local v4    # "intent":Landroid/content/Intent;
    :cond_1
    const-string v9, "ShareAccessibilitySettingsReceive"

    const-string v10, "mReceiveUri is null"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->finish()V

    goto :goto_0

    .line 86
    .end local v4    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->shareAccessibilitySettingsReceive_Init()V

    .line 89
    const-string v9, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->getIntent()Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 91
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->getIntent()Landroid/content/Intent;

    move-result-object v9

    const-string v10, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {v9, v10}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v7

    .line 93
    .local v7, "rawMsgs":[Landroid/os/Parcelable;
    if-eqz v7, :cond_0

    .line 94
    array-length v9, v7

    new-array v5, v9, [Landroid/nfc/NdefMessage;

    .line 95
    .local v5, "msgs":[Landroid/nfc/NdefMessage;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v9, v7

    if-ge v3, v9, :cond_3

    .line 96
    aget-object v9, v7, v3

    check-cast v9, Landroid/nfc/NdefMessage;

    aput-object v9, v5, v3

    .line 95
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 99
    :cond_3
    aget-object v9, v5, v11

    invoke-virtual {v9}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v8

    .line 100
    .local v8, "recs":[Landroid/nfc/NdefRecord;
    aget-object v9, v8, v11

    invoke-virtual {v9}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v6

    .line 101
    .local v6, "payload":[B
    const-string v9, "ShareAccessibilitySettingsReceive"

    const-string v10, "call applyPayload"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-direct {p0, v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->applyPayload([B)V

    goto :goto_0

    .line 107
    .end local v3    # "i":I
    .end local v5    # "msgs":[Landroid/nfc/NdefMessage;
    .end local v6    # "payload":[B
    .end local v7    # "rawMsgs":[Landroid/os/Parcelable;
    .end local v8    # "recs":[Landroid/nfc/NdefRecord;
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 108
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string v9, "ShareAccessibilitySettingsReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Intent : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v4}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {v4}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveUri:Landroid/net/Uri;

    .line 112
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveUri:Landroid/net/Uri;

    if-nez v9, :cond_5

    .line 113
    const-string v9, "ShareAccessibilitySettingsReceive"

    const-string v10, "mReceiveUri is null "

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->finish()V

    goto/16 :goto_0

    .line 117
    :cond_5
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveUri:Landroid/net/Uri;

    invoke-virtual {v9}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    .line 118
    const-string v9, "ShareAccessibilitySettingsReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mReceiveUri: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const-string v0, ""

    .line 121
    .local v0, "ReceivefileName":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    iget-object v10, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    iget-object v11, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 124
    const-string v9, "ShareAccessibilitySettingsReceive"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "ReceivefileName : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    const-string v9, ".sasf"

    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v9

    const/4 v10, -0x1

    if-ne v9, v10, :cond_6

    .line 128
    invoke-direct {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->showNotSupportFileDialog(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 131
    :cond_6
    :try_start_0
    iget-object v9, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    invoke-static {v9}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->LoadValue(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v9

    sput-object v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->receiveSettingValues:Ljava/util/HashMap;
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_2
    sget-object v9, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->receiveSettingValues:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "{}"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 142
    const-string v9, "ShareAccessibilitySettingsReceive"

    const-string v10, "receiveSettingValues is null"

    invoke-static {v9, v10}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->finish()V

    goto/16 :goto_0

    .line 135
    :catch_0
    move-exception v2

    .line 137
    .local v2, "e":Ljavax/xml/parsers/ParserConfigurationException;
    invoke-virtual {v2}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    goto :goto_2

    .line 148
    .end local v2    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :cond_7
    invoke-direct {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->showImportGuideDialog()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 388
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->RevSettingValues:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 389
    sput-object v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->RevSettingValues:Ljava/lang/String;

    .line 392
    :cond_0
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->receiveSettingValues:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 393
    sput-object v1, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->receiveSettingValues:Ljava/util/HashMap;

    .line 396
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 397
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mContext:Landroid/content/Context;

    .line 400
    :cond_2
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 401
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveFullPath:Ljava/lang/String;

    .line 404
    :cond_3
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveUri:Landroid/net/Uri;

    if-eqz v0, :cond_4

    .line 405
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->mReceiveUri:Landroid/net/Uri;

    .line 408
    :cond_4
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->separationReceivePath:[Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 409
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsReceive;->separationReceivePath:[Ljava/lang/String;

    .line 412
    :cond_5
    const-string v0, "ShareAccessibilitySettingsReceive"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 414
    return-void
.end method
