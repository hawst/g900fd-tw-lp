.class public final Lcom/samsung/android/app/shareaccessibilitysettings/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final androidbeam_popup_description:I = 0x7f070000

.field public static final androidbeam_popup_title:I = 0x7f070001

.field public static final app_name:I = 0x7f070002

.field public static final dialog_cancle:I = 0x7f070003

.field public static final dialog_no:I = 0x7f070004

.field public static final dialog_ok:I = 0x7f070005

.field public static final dialog_yes:I = 0x7f070006

.field public static final export:I = 0x7f070007

.field public static final export_description:I = 0x7f070008

.field public static final export_dialog_description:I = 0x7f070009

.field public static final export_dialog_title:I = 0x7f07000a

.field public static final export_error:I = 0x7f07000b

.field public static final export_notify_end:I = 0x7f07000c

.field public static final export_notify_end_title:I = 0x7f07000d

.field public static final export_storage_popup_External:I = 0x7f07000e

.field public static final export_storage_popup_sdcard:I = 0x7f07000f

.field public static final external_memory_full:I = 0x7f070010

.field public static final filesize_zero_error_msg:I = 0x7f070011

.field public static final import_export:I = 0x7f070012

.field public static final import_notify_end:I = 0x7f070013

.field public static final import_notify_end_title:I = 0x7f070014

.field public static final import_storage_popup_External:I = 0x7f070015

.field public static final import_storage_popup_sdcard:I = 0x7f070016

.field public static final importandrestore:I = 0x7f070017

.field public static final importandrestore_description:I = 0x7f070018

.field public static final internal_memory_full:I = 0x7f070019

.field public static final nfc_androidbeam_popup_description:I = 0x7f07001a

.field public static final nfc_androidbeam_popup_title:I = 0x7f07001b

.field public static final nfc_error:I = 0x7f07001c

.field public static final restore_file_popup:I = 0x7f07001d

.field public static final restore_file_popup_description:I = 0x7f07001e

.field public static final restore_file_popup_description_2:I = 0x7f07001f

.field public static final share:I = 0x7f070020

.field public static final share_accessibility_settings:I = 0x7f070021

.field public static final share_accessibility_settings_description:I = 0x7f070022

.field public static final share_accessibility_settings_summary:I = 0x7f070023

.field public static final share_description:I = 0x7f070024

.field public static final share_via_popup_title:I = 0x7f070025

.field public static final stms_version:I = 0x7f070026

.field public static final storage_popup_External:I = 0x7f070027

.field public static final storage_popup_sdcard:I = 0x7f070028

.field public static final storage_popup_sdcard_H:I = 0x7f070029

.field public static final storage_popup_title:I = 0x7f07002a

.field public static final storage_popup_title_K:I = 0x7f07002b

.field public static final storage_sharevia_popup_title:I = 0x7f07002c

.field public static final use_android_beam:I = 0x7f07002d

.field public static final use_android_beam_description:I = 0x7f07002e


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
