.class public Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;
.super Landroid/app/Activity;
.source "ShareAccessibilityExportImport.java"


# static fields
.field private static final EXPORT_MYFILES_CONFIRM:I = 0x6

.field private static final EXPORT_PENDING_REQUESTCODE_EXTERNAL:I = 0x2

.field private static final EXPORT_PENDING_REQUESTCODE_INTERNAL:I = 0x1

.field private static final ExternalMemory:I = 0x1

.field private static final IMPORT:I = 0x3

.field private static final IMPORT_PENDING_REQUESTCODE:I = 0x3

.field private static final NOCARD:I = 0x2

.field private static final PICK_SINGLE_FILE:I = 0x4

.field private static final RESTORE:I = 0x5

.field private static RevSettingValues:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ShareAccessibilityExportImport"

.field private static isDialogShown:Z

.field private static isSdCardAvailable:Z

.field private static isSecondDialogShown:Z

.field private static settingValue:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;

.field private mExportFailNotificationManager:Landroid/app/NotificationManager;

.field private mExportNotificationManager:Landroid/app/NotificationManager;

.field private mImportNotificationManager:Landroid/app/NotificationManager;

.field private path:Ljava/lang/String;

.field pending_noti_external_intent:Landroid/app/PendingIntent;

.field pending_noti_internal_intent:Landroid/app/PendingIntent;

.field private final selectExternalMemory:I

.field private final selectInternalMemory:I

.field private window:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    sput-boolean v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isDialogShown:Z

    .line 53
    sput-boolean v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isSdCardAvailable:Z

    .line 54
    sput-boolean v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isSecondDialogShown:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 33
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;

    .line 37
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->selectInternalMemory:I

    .line 40
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->selectExternalMemory:I

    .line 42
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->pending_noti_internal_intent:Landroid/app/PendingIntent;

    .line 43
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->pending_noti_external_intent:Landroid/app/PendingIntent;

    .line 56
    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->window:Landroid/view/WindowManager$LayoutParams;

    return-void
.end method

.method private ShareAccessibilityExport_Init()V
    .locals 2

    .prologue
    .line 91
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;

    .line 92
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;

    .line 93
    const-string v0, "ShareAccessibilityExportImport"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    return-void
.end method

.method private ShareAccessibilityImport_Init()V
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->RevSettingValues:Ljava/util/HashMap;

    .line 87
    const-string v0, "ShareAccessibilityExportImport"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 25
    sget-boolean v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isDialogShown:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 25
    sput-boolean p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isDialogShown:Z

    return p0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 25
    sget-boolean v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isSecondDialogShown:Z

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 25
    sput-boolean p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isSecondDialogShown:Z

    return p0
.end method

.method static synthetic access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$302(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Ljava/util/HashMap;

    .prologue
    .line 25
    sput-object p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic access$400(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->exportNotificationSet(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->RevSettingValues:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$702(Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Ljava/util/HashMap;

    .prologue
    .line 25
    sput-object p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->RevSettingValues:Ljava/util/HashMap;

    return-object p0
.end method

.method static synthetic access$800(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->importNotificationSet(Ljava/lang/String;)V

    return-void
.end method

.method private exportFailureNotification(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 561
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mExportFailNotificationManager:Landroid/app/NotificationManager;

    .line 562
    new-instance v0, Landroid/app/Notification;

    const v1, 0x7f020001

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v1, p1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 565
    .local v0, "exportFailureNotification":Landroid/app/Notification;
    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, p2, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 568
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 570
    iget-object v1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mExportFailNotificationManager:Landroid/app/NotificationManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    invoke-virtual {v1, v2, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 572
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->finish()V

    .line 573
    return-void
.end method

.method private exportNotificationSet(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10
    .param p1, "file_name"    # Ljava/lang/String;
    .param p2, "file_path"    # Ljava/lang/String;
    .param p3, "which"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 436
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 437
    .local v4, "noti_internal_export_intent":Landroid/content/Intent;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 440
    .local v3, "noti_external_export_intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.myfiles"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 443
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.android.app.myfiles"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 446
    const-string v5, "ShareAccessibilityExportImport"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "exportNotificationSet which :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    if-nez p3, :cond_2

    .line 448
    const-string v5, "START_FOLDER"

    sget-object v6, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 450
    invoke-static {p0, v8, v4, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->pending_noti_internal_intent:Landroid/app/PendingIntent;

    .line 463
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07000c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    aput-object p1, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 466
    .local v1, "exportnotiticker":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07000d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 469
    .local v2, "exportnotititle":Ljava/lang/String;
    const-string v5, "notification"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/NotificationManager;

    iput-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mExportNotificationManager:Landroid/app/NotificationManager;

    .line 470
    new-instance v0, Landroid/app/Notification;

    const v5, 0x7f020001

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-direct {v0, v5, v1, v6, v7}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 473
    .local v0, "exportnotification":Landroid/app/Notification;
    if-nez p3, :cond_3

    .line 474
    iget-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->pending_noti_internal_intent:Landroid/app/PendingIntent;

    invoke-virtual {v0, p0, v2, p1, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 482
    :cond_1
    :goto_1
    iget v5, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v5, v5, 0x10

    iput v5, v0, Landroid/app/Notification;->flags:I

    .line 484
    iget-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mExportNotificationManager:Landroid/app/NotificationManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v5, v6, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 488
    iget-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "file://"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v5, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 489
    return-void

    .line 454
    .end local v0    # "exportnotification":Landroid/app/Notification;
    .end local v1    # "exportnotiticker":Ljava/lang/String;
    .end local v2    # "exportnotititle":Ljava/lang/String;
    :cond_2
    if-ne p3, v8, :cond_0

    .line 455
    const-string v5, "START_FOLDER"

    const-string v6, "/storage/extSdCard/Accessibility"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 457
    const/4 v5, 0x2

    invoke-static {p0, v5, v3, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->pending_noti_external_intent:Landroid/app/PendingIntent;

    goto :goto_0

    .line 477
    .restart local v0    # "exportnotification":Landroid/app/Notification;
    .restart local v1    # "exportnotiticker":Ljava/lang/String;
    .restart local v2    # "exportnotititle":Ljava/lang/String;
    :cond_3
    if-ne p3, v8, :cond_1

    .line 478
    iget-object v5, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->pending_noti_external_intent:Landroid/app/PendingIntent;

    invoke-virtual {v0, p0, v2, p1, v5}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    goto :goto_1
.end method

.method private importNotificationSet(Ljava/lang/String;)V
    .locals 10
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 492
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 493
    .local v4, "noti_import_intent":Landroid/content/Intent;
    const-string v6, "android.settings.ACCESSIBILITY_SETTINGS"

    invoke-virtual {v4, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 494
    const-string v6, "importNotification"

    invoke-virtual {v4, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 496
    const/4 v6, 0x3

    invoke-static {p0, v6, v4, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 500
    .local v3, "intent":Landroid/app/PendingIntent;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070013

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-array v7, v9, [Ljava/lang/Object;

    aput-object p1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 502
    .local v1, "importnotiticker":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070014

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 505
    .local v2, "importnotititle":Ljava/lang/String;
    const-string v6, "notification"

    invoke-virtual {p0, v6}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/NotificationManager;

    iput-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mImportNotificationManager:Landroid/app/NotificationManager;

    .line 506
    new-instance v0, Landroid/app/Notification;

    const v6, 0x7f020001

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-direct {v0, v6, v1, v8, v9}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    .line 509
    .local v0, "importnotification":Landroid/app/Notification;
    invoke-virtual {v0, p0, v2, p1, v3}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    .line 511
    iget v6, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v6, v6, 0x10

    iput v6, v0, Landroid/app/Notification;->flags:I

    .line 513
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mImportNotificationManager:Landroid/app/NotificationManager;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    long-to-int v7, v8

    invoke-virtual {v6, v7, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 516
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.android.settings.action.talkback_off"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 517
    .local v5, "tbIntent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 519
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 537
    const-string v0, "ShareAccessibilityExportImport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 539
    const-string v0, "ShareAccessibilityExportImport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "resultCode :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 540
    const-string v0, "ShareAccessibilityExportImport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestCode :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    if-eqz p3, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 542
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;

    .line 543
    const-string v0, "FILE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;

    .line 545
    const-string v0, "ShareAccessibilityExportImport"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onActivityResult PICK_SINGLE_FILE path:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->showDialog(I)V

    .line 559
    :goto_0
    return-void

    .line 555
    :cond_0
    const-string v0, "ShareAccessibilityExportImport"

    const-string v1, "data is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-direct {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->ShareAccessibilityExport_Init()V

    .line 63
    invoke-direct {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->ShareAccessibilityImport_Init()V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isExternalMemoryAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->showDialog(I)V

    .line 81
    :goto_0
    return-void

    .line 79
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->showDialog(I)V

    goto :goto_0
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 16
    .param p1, "id"    # I

    .prologue
    .line 112
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-static/range {p0 .. p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->isLightTheme(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_1

    const/4 v13, 0x5

    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v3, v0, v13}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 117
    .local v3, "dlg":Landroid/app/AlertDialog$Builder;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->window:Landroid/view/WindowManager$LayoutParams;

    if-nez v13, :cond_0

    .line 118
    new-instance v13, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v13}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->window:Landroid/view/WindowManager$LayoutParams;

    .line 119
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->window:Landroid/view/WindowManager$LayoutParams;

    const/4 v14, 0x2

    iput v14, v13, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 120
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->window:Landroid/view/WindowManager$LayoutParams;

    const/high16 v14, 0x3f000000    # 0.5f

    iput v14, v13, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 121
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->window:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v13, v14}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 122
    const-string v13, "ShareAccessibilityExportImport"

    const-string v14, "Dimming background"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    :cond_0
    :goto_1
    new-instance v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$1;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V

    invoke-virtual {v3, v13}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 139
    new-instance v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$2;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V

    invoke-virtual {v3, v13}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    .line 156
    const/4 v13, 0x4

    new-array v7, v13, [Ljava/lang/CharSequence;

    const/4 v13, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f070016

    invoke-virtual {v14, v15}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    aput-object v14, v7, v13

    const/4 v13, 0x1

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f07000f

    invoke-virtual {v14, v15}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    aput-object v14, v7, v13

    const/4 v13, 0x2

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f070015

    invoke-virtual {v14, v15}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    aput-object v14, v7, v13

    const/4 v13, 0x3

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f07000e

    invoke-virtual {v14, v15}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    aput-object v14, v7, v13

    .line 161
    .local v7, "mEntries":[Ljava/lang/CharSequence;
    const/4 v13, 0x2

    new-array v8, v13, [Ljava/lang/CharSequence;

    const/4 v13, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f070016

    invoke-virtual {v14, v15}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    aput-object v14, v8, v13

    const/4 v13, 0x1

    invoke-virtual/range {p0 .. p0}, Landroid/content/ContextWrapper;->getApplicationContext()Landroid/content/Context;

    move-result-object v14

    const v15, 0x7f07000f

    invoke-virtual {v14, v15}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v14

    aput-object v14, v8, v13

    .line 165
    .local v8, "mEntries_NOCARD":[Ljava/lang/CharSequence;
    const v11, 0x7f07002b

    .line 166
    .local v11, "titleId":I
    const-string v13, "ShareAccessibilityExportImport"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "onCreateDialog id : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    packed-switch p1, :pswitch_data_0

    .line 428
    :goto_2
    :pswitch_0
    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    .line 429
    .local v2, "dialog":Landroid/app/AlertDialog;
    const/4 v13, 0x1

    invoke-virtual {v2, v13}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 431
    return-object v2

    .line 112
    .end local v2    # "dialog":Landroid/app/AlertDialog;
    .end local v3    # "dlg":Landroid/app/AlertDialog$Builder;
    .end local v7    # "mEntries":[Ljava/lang/CharSequence;
    .end local v8    # "mEntries_NOCARD":[Ljava/lang/CharSequence;
    .end local v11    # "titleId":I
    :cond_1
    const/4 v13, 0x4

    goto/16 :goto_0

    .line 124
    .restart local v3    # "dlg":Landroid/app/AlertDialog$Builder;
    :catch_0
    move-exception v4

    .line 126
    .local v4, "e":Ljava/lang/RuntimeException;
    const-string v13, "ShareAccessibilityExportImport"

    const-string v14, "window is not null"

    invoke-static {v13, v14}, Landroid/util/secutil/Log;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 170
    .end local v4    # "e":Ljava/lang/RuntimeException;
    .restart local v7    # "mEntries":[Ljava/lang/CharSequence;
    .restart local v8    # "mEntries_NOCARD":[Ljava/lang/CharSequence;
    .restart local v11    # "titleId":I
    :pswitch_1
    new-instance v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$3;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V

    invoke-virtual {v3, v7, v13}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 258
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 262
    :pswitch_2
    new-instance v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$4;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$4;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V

    invoke-virtual {v3, v8, v13}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 285
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v13

    invoke-virtual {v3, v13}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 288
    :pswitch_3
    const/4 v13, 0x1

    sput-boolean v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->isSecondDialogShown:Z

    .line 291
    new-instance v10, Ljava/text/SimpleDateFormat;

    sget-object v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->DATE_FORMAT:Ljava/lang/String;

    invoke-direct {v10, v13}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 292
    .local v10, "sdf":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    .line 293
    .local v12, "today":Ljava/util/Calendar;
    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 294
    .local v9, "month":Ljava/lang/String;
    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    const/16 v14, 0xa

    if-ge v13, v14, :cond_2

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "0"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 295
    :cond_2
    const/4 v13, 0x5

    invoke-virtual {v12, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 296
    .local v1, "day":Ljava/lang/String;
    const/4 v13, 0x5

    invoke-virtual {v12, v13}, Ljava/util/Calendar;->get(I)I

    move-result v13

    const/16 v14, 0xa

    if-ge v13, v14, :cond_3

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "0"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 298
    :cond_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Settings of accessibility"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const/4 v14, 0x1

    invoke-virtual {v12, v14}, Ljava/util/Calendar;->get(I)I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ".sasf"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    sput-object v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    .line 300
    new-instance v13, Ljava/io/File;

    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    sget-object v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->FILE_NAME:Ljava/lang/String;

    invoke-direct {v13, v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    .line 303
    sget-object v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    invoke-static {v13}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->sharingFileNameCheck(Ljava/io/File;)Ljava/io/File;

    move-result-object v13

    sput-object v13, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->export_internal_file:Ljava/io/File;

    .line 306
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v14, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccVariable;->ACC_SETTING_FILE_INTERNAL_FOLDER_PATH:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 308
    .local v6, "export_path":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/view/ContextThemeWrapper;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f070009

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v6, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 313
    .local v5, "export_description":Ljava/lang/String;
    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const v14, 0x7f07000a

    invoke-virtual {v13, v14}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const v14, 0x7f070005

    new-instance v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;

    invoke-direct/range {v15 .. v16}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$6;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V

    invoke-virtual {v13, v14, v15}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const v14, 0x7f070003

    new-instance v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$5;

    invoke-direct/range {v15 .. v16}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$5;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V

    invoke-virtual {v13, v14, v15}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_2

    .line 374
    .end local v1    # "day":Ljava/lang/String;
    .end local v5    # "export_description":Ljava/lang/String;
    .end local v6    # "export_path":Ljava/lang/String;
    .end local v9    # "month":Ljava/lang/String;
    .end local v10    # "sdf":Ljava/text/SimpleDateFormat;
    .end local v12    # "today":Ljava/util/Calendar;
    :pswitch_4
    const v13, 0x7f07001e

    invoke-virtual {v3, v13}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const v14, 0x7f07001d

    invoke-virtual {v13, v14}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const v14, 0x7f070003

    new-instance v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$8;

    invoke-direct/range {v15 .. v16}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$8;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V

    invoke-virtual {v13, v14, v15}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v13

    const v14, 0x7f070005

    new-instance v15, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;

    invoke-direct/range {v15 .. v16}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;-><init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V

    invoke-virtual {v13, v14, v15}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_2

    .line 167
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 576
    const-string v0, "ShareAccessibilityExportImport"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    sget-object v0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 579
    sput-object v2, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->settingValue:Ljava/util/HashMap;

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mExportNotificationManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_1

    .line 583
    iput-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mExportNotificationManager:Landroid/app/NotificationManager;

    .line 586
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 587
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 98
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 106
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 100
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->finish()V

    goto :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;
    .param p3, "args"    # Landroid/os/Bundle;

    .prologue
    .line 523
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V

    .line 524
    packed-switch p1, :pswitch_data_0

    .line 534
    :goto_0
    return-void

    .line 526
    :pswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->removeDialog(I)V

    goto :goto_0

    .line 529
    :pswitch_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->removeDialog(I)V

    goto :goto_0

    .line 524
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
