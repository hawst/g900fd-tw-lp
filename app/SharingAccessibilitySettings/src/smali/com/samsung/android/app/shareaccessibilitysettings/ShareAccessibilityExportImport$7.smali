.class Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;
.super Ljava/lang/Object;
.source "ShareAccessibilityExportImport.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 388
    const-string v2, "ShareAccessibilityExportImport"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RESTORE path :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$600(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$600(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->LoadValue(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    # setter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->RevSettingValues:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$702(Ljava/util/HashMap;)Ljava/util/HashMap;
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    :goto_0
    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->RevSettingValues:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$700()Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "{}"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 402
    const-string v2, "ShareAccessibilityExportImport"

    const-string v3, "RevSettingValues is null"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    :goto_1
    return-void

    .line 394
    :catch_0
    move-exception v0

    .line 396
    .local v0, "e":Ljavax/xml/parsers/ParserConfigurationException;
    invoke-virtual {v0}, Ljavax/xml/parsers/ParserConfigurationException;->printStackTrace()V

    goto :goto_0

    .line 409
    .end local v0    # "e":Ljavax/xml/parsers/ParserConfigurationException;
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$200(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Landroid/content/Context;

    move-result-object v2

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->RevSettingValues:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$700()Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilitySettingsCommonFunction;->applySettings(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 413
    const-string v1, ""

    .line 414
    .local v1, "mSelectionFileName":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$600(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$600(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # getter for: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->path:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$600(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 417
    const-string v2, "ShareAccessibilityExportImport"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSelectionFileName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    # invokes: Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->importNotificationSet(Ljava/lang/String;)V
    invoke-static {v2, v1}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->access$800(Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;Ljava/lang/String;)V

    .line 420
    iget-object v2, p0, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport$7;->this$0:Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;

    invoke-virtual {v2}, Lcom/samsung/android/app/shareaccessibilitysettings/ShareAccessibilityExportImport;->finish()V

    goto :goto_1
.end method
